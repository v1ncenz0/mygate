﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
/**
 * Endpoint e variabili statiche
 */
public class StaticVariables {

	// Url del sito dov'è hostato il sito di backends
	public static string BASE_URL = "http://localhost/fabbridea/site/www/";
	//public static string BASE_URL = "http://www.fabbridea.com/";

	public static string source="web";

	public static string username="icnosman@gmail.com";
	public static string password="nirvana";
	public static int id_user=0;

	public static ItemGate gate;
	//Le variabili a seguire sono relative al CAD
	public static bool comingFromGate = false;
	public static bool comingFromFence=false;
	public static string relativeSegment="";
	public static string firstPole = "";
	public static Vector3 fencePosition;
	public static int workMode = 0; //questa variabile identifica la modalità di inserimento dei segmenti (es. 0= recinzioni, 1=balconi,2=teste di cazzi per ora);

	public static string scenePlan = "prova";
}
