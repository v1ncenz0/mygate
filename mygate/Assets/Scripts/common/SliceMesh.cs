﻿using UnityEngine;
using System.Collections;

/**
 * Classe che effettua un finto taglio della mesh.
 * In pratica schiaccia i vertici esterni alla curva spline
 * sulla curva stessa, nel punto più vicino alla curva. Il risultato non è perfetto ma sufficiente per il nostro scopo.
 * 
 * TODO NOTA: Per migliorare il risultato, bisognerebbe schiacciare i vertici esterni alla curve, non nel punto più vicono alla curva,
 * ma nel punto di intereszione del piano generato dal triangolo del vertice e la curva.
 */
public class SliceMesh : MonoBehaviour
{

		// Oggetto spline che incapsula l'equazione della curva
		public Spline spline;
		// Parametri t0 e t1 utili per selezionare un pezzo di curva
		// Di default è selezionata tutta la curva
		public float t0 = 0;
		public float t1 = 1;

		void Start ()
		{
				SliceIt ();
		}

		public void SliceIt ()
		{
				MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter> ();
				foreach (MeshFilter comp in meshFilters) {

						// Vertici della mesh
						Vector3[] vertices = comp.mesh.vertices;
						// Mesh dell'oggetto
						Mesh meshSlice = comp.mesh;

						// loop thru vertexes (of original object, but slice clone has same amount)
						for (var i = 0; i < vertices.Length; i++) {
								// Trasformo la posizone (x, y, z) da local a world space
								Vector3 tmpverts = transform.TransformPoint (vertices [i]);
								// Nuovo punto sulla curva
								Vector3 newPoint;

								// Se il vertice è sopra la curva
								if (isExternalPoint (tmpverts, out newPoint)) {
										// Schiaccio il vertice sulla curva
										vertices [i] = transform.InverseTransformPoint (newPoint) * 1.0f;
								}
						}
		
						// Ricreo la mesh con in nuovi punti
						meshSlice.vertices = vertices;
						meshSlice.RecalculateBounds ();

				}
		}

		// Controlla se un dato vertice è esterno (sulla parte superiore) della curva
		private bool isExternalPoint (Vector3 point, out Vector3 newPoint)
		{
				float t = spline.GetClosestPointParam (point, 5, t0, t1, 0.01f);
				Vector3 splinePoint = spline.GetPositionOnSpline (t);
				if (splinePoint.y < point.y) {
						// Il nuovo punto su cui spostare il vertice è dato dal vertice sulla spine, 
						// del quale mantendo la z, così facendo è come se avessimo una lama infinita che taglia verso z
						newPoint = splinePoint;
						newPoint.z = point.z;
						return true;
				}
				newPoint = point;
				return false;
		}

}


