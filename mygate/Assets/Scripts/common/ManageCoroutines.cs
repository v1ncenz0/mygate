﻿using UnityEngine;
using System.Collections;

/**
 * Classe utile per il lanciare coroutines,
 * essa visualizza una finestra di caricamento mentre è in esecuzione la coroutine
 */
public class ManageCoroutines : MonoBehaviour
{
		public static bool inProgress;
		private static ManageCoroutines instance;
		

	
		// Lancia la coroutine passata in input
		public static void lauchCoroutines (IEnumerator coroutine)
		{
				if (instance == null) {
						GameObject go = new GameObject ();
						go.AddComponent<ManageCoroutines> ();
						instance = go.GetComponent<ManageCoroutines> ();
						
				}
				inProgress = true;
				instance.StartCoroutine (coroutine);
		} 
	
}
