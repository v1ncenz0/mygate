﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageManager : MonoBehaviour {

	public static string HELP_WELCOME_TITLE= "ASSISTENTE";
	public static string HELP_WELCOME="Benvenuto in MyGate. Per iniziare seleziona un elemento dalla libreria";

	public static string HELP_EDIT_TITLE= "INSERIMENTO";
	public static string HELP_EDIT= "Seleziona un oggetto alla libreria per poterlo inserire nella griglia";

	public static string HELP_DELETE_TITLE="GOMMA";
	public static string HELP_DELETE="Clicca sull'oggetto che vuoi eliminare. Se è attiva la funzione 'speculare' con il click eliminerai anche l'oggetto nell'anta accanto";


	public static string HELP_FILL_TITLE= "BACCHETTA MAGICA";
	public static string HELP_FILL= "Posiziona in automatico il pezzo scelto su tutto il pannello";

	public static string HELP_ERASEGRID_TITLE= "CESTINO";
	public static string HELP_ERASEGRID= "Elimina tutti gli elementi";


	public static string HELP_LAYER_TITLE= "LIVELLI";
	public static string HELP_LAYERS= "Puoi scegliere di comporre il cancello su più livelli, assegnando ad ognuno un colore, così da ottenere giochi di contrasto di pieni e vuoti";

	public static string HELP_LIBRARY_TITLE= "POSIZIONA";
	public static string HELP_LIBRARY= "Seleziona un oggetto dalla libreria e trascinalo nella griglia per posizionarlo";

	public static string MESSAGE_SAVE="Ad ora il tuo cancello costa %s. Sei soddisfatto?";

	public static string BT_OK_CHECKOUT="SI. VAI AL CARRELLO";
	public static string BT_CONTINUE_DRAWING="NO. CONTINUA A DISEGNARE";

	public static string MESSAGE_ALERT_BEFORE_CLOSE="Affinchè la configurazione sia corretta, le celle del livello centrale devono essere tutte riempite. Aggiungi elementi per completare la configurazione";

	public static string BT_OK="OK";
	public static string BT_QUIT="ESCI";


	public static string text(string message,string value){
		return message.Replace ("%s", value).ToString();
	}

}
