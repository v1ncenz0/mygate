using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;

/**
 * Classe di utility per l'implementazione di funzioni statiche.
 */
public class Utility
{



		// Calcola il Massimo Comune Divisore (GCD) tra due interi
		public static int GCD (int a, int b)
		{
				int Remainder;
		
				while (b != 0) {
						Remainder = a % b;
						a = b;
						b = Remainder;
				}
		
				return a;
		}

		// Calcola il Massimo Comune Divisore (GCD) in una lista di interi
		public static int GCDofList (int[] values)
		{
				if (values == null || values.Length == 0)
						return 0;

				if (values.Length == 1)
						return values [0];

				int tmp = GCD (values [values.Length - 1], values [values.Length - 2]);
				for (int i = values.Length - 3; i >= 0; i--) {
						tmp = GCD (tmp, values [i]);
				}
				return tmp;

		}

		
	public static T getId<T>(ArrayList list,Dropdown d){

		string name = d.options [d.value].text;

		for (int i = 0; i < list.Count; i++) {
			T item = (T)list [i];

			if (item.GetType ().GetField ("name").GetValue (item).ToString () == name.ToString ()) {
				return item;
			}
		}
		return default(T);

	}
		
		// Codifica un testo semplice in base64
		public static string Base64Encode (string plainText)
		{
				byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes (plainText);
				return System.Convert.ToBase64String (plainTextBytes);
		}
		
		// Decodifica un testo in base64 in testo semplice
		public static string Base64Decode (string base64EncodedData)
		{
				byte[] base64EncodedBytes = System.Convert.FromBase64String (base64EncodedData);
				return System.Text.Encoding.UTF8.GetString (base64EncodedBytes);
		}


		public static bool convertIntToBool(string v){
			if (v == "0")
				return false;
			else
				return true;
		}


	public static Vector3 ConvertPointToWorld(Vector3 v){
		v = v;
		v.z = -Camera.main.transform.localPosition.z;
		v = Camera.main.ScreenToWorldPoint(v);
		return v;

	}


	public static string ConvertDBDate(string dbdate){

		string[] datetime = dbdate.Split (' ');
		string[] date = datetime [0].Split ('-');

		return date [2] + "/" + date [1] + "/" + date [0] + " " + datetime [1];

	}

	public static string ConvertHtmlToText(string source) {

		string result;

		// Remove HTML Development formatting
		// Replace line breaks with space
		// because browsers inserts space
		result = source.Replace("\r", " ");
		// Replace line breaks with space
		// because browsers inserts space
		result = result.Replace("\n", " ");
		// Remove step-formatting
		result = result.Replace("\t", string.Empty);
		// Remove repeating speces becuase browsers ignore them
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"( )+", " ");

		// Remove the header (prepare first by clearing attributes)
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*head([^>])*>", "<head>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"(<( )*(/)( )*head( )*>)", "</head>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(<head>).*(</head>)", string.Empty,
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// remove all scripts (prepare first by clearing attributes)
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*script([^>])*>", "<script>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"(<( )*(/)( )*script( )*>)", "</script>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		//result = System.Text.RegularExpressions.Regex.Replace(result, 
		//         @"(<script>)([^(<script>\.</script>)])*(</script>)",
		//         string.Empty, 
		//         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"(<script>).*(</script>)", string.Empty,
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// remove all styles (prepare first by clearing attributes)
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*style([^>])*>", "<style>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"(<( )*(/)( )*style( )*>)", "</style>",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(<style>).*(</style>)", string.Empty,
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// insert tabs in spaces of <td> tags
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*td([^>])*>", "\t",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// insert line breaks in places of <BR> and <LI> tags
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*br( )*>", "\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*li( )*>", "\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// insert line paragraphs (double line breaks) in place
		// if <P>, <DIV> and <TR> tags
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*div([^>])*>", "\r\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*tr([^>])*>", "\r\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<( )*p([^>])*>", "\r\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// Remove remaining tags like <a>, links, images,
		// comments etc - anything thats enclosed inside < >
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<[^>]*>", string.Empty,
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		// replace special characters:
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&nbsp;", " ",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);

		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&bull;", " * ",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&lsaquo;", "<",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&rsaquo;", ">",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&trade;", "(tm)",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&frasl;", "/",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"<", "<",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@">", ">",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&copy;", "(c)",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&reg;", "(r)",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		// Remove all others. More can be added, see
		// http://hotwired.lycos.com/webmonkey/reference/special_characters/
		result = System.Text.RegularExpressions.Regex.Replace(result,
			@"&(.{2,6});", string.Empty,
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);


		// make line breaking consistent
		result = result.Replace("\n", "\r");

		// Remove extra line breaks and tabs:
		// replace over 2 breaks with 2 and over 4 tabs with 4. 
		// Prepare first to remove any whitespaces inbetween
		// the escaped characters and remove redundant tabs inbetween linebreaks
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\r)( )+(\r)", "\r\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\t)( )+(\t)", "\t\t",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\t)( )+(\r)", "\t\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\r)( )+(\t)", "\r\t",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		// Remove redundant tabs
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\r)(\t)+(\r)", "\r\r",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		// Remove multible tabs followind a linebreak with just one tab
		result = System.Text.RegularExpressions.Regex.Replace(result,
			"(\r)(\t)+", "\r\t",
			System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		// Initial replacement target string for linebreaks
		string breaks = "\r\r\r";
		// Initial replacement target string for tabs
		string tabs = "\t\t\t\t\t";
		for (int index = 0; index < result.Length; index++) {
			result = result.Replace(breaks, "\r\r");
			result = result.Replace(tabs, "\t\t\t\t");
			breaks = breaks + "\r";
			tabs = tabs + "\t";
		}

		// Thats it.
		return result;

	}
		

}