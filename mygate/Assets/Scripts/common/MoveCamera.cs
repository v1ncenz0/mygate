﻿// Credit to damien_oconnell from http://forum.unity3d.com/threads/39513-Click-drag-camera-movement
// for using the mouse displacement for calculating the amount of camera movement and panning code.
using UnityEngine;
using System.Collections;

/**
 * La classe gestisce i moventi della telecamera usando il mouse come sorgente di input.
 * Parti della classe sono stati presi dal web.
 * 
 * Gestisce zoom, movimenti, pan e view stabilite.
 */
public class MoveCamera : MonoBehaviour
{
	public float turnSpeed = 4.0f;		// Speed of camera turning when mouse moves in along an axis
	public float panSpeed = 4.0f;		// Speed of the camera when being panned
	public float zoomSpeed = 4.0f;		// Speed of the camera going back and forth
	public float zoomSpeedManual = 20.0f;		// Speed of the camera going back and forth

	public float turnSpeed2 = 1000.0f;

	private Vector3 mouseOrigin;	// Position of cursor when mouse dragging starts
	
	public float minFov = 50f;
	public float maxFov = 200f;
	public float sensitivity = 10f;
	private float ZoomAmount = 0; //With Positive and negative values
	public float MaxToClamp = 100;
	private float ROTSpeed = 20;
	public float manualZoomFactor = 2; // Quantità zoom alla pressione del pulsante di zoom

	// Oggetto frame del cancello preso dall'inspector
	public GameObject frame;
	// Variabili per il reset della posizione
	private Transform initialTransform;
	private float lastRotation = 0f;
	private Vector3 lastTraslation = Vector3.zero;
	public Vector3 puntoPivot;
	private Vector3 lastVector = Vector3.up;

	//Tasti del mouse premuti
	private bool mouseLeftDown = false;
	private bool mouseRightDown = false;
	private bool mouseMiddleDown = false;

	private bool isRotating;

	// Posizione iniziale della camera
	public Vector3 initialCameraPos;
	// Rotazione iniziale della camera
	public Quaternion initialCameraRot;

	// Ultima Posizione della camera (prima del reset)
	public Vector3 lastCameraPos;
	// Ultima Rotazione della camera (prima del reset)
	public Quaternion lastCameraRot;


// Flag che indica che è stata effettuata una rotazione manuale della camera
	private bool manualRotation = false;
	
	public bool enableKeyboardMouse = true;
//public static Vector3 centerGate;

	void Start ()
	{

		// Centro del cancello
		//puntoPivot = new Vector3 (layer.gateDim.x / 2, layer.gateDim.y / 2, 0);
		//initialTransform = Camera.main.transform;
	}




	void Update ()
	{
		if (!enableKeyboardMouse)
			return;
					// Zoom andando avanti e indietro con la camera
					/*ZoomAmount += Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;
					ZoomAmount = Mathf.Clamp (ZoomAmount, -MaxToClamp, MaxToClamp);
					var translate = Mathf.Min (Mathf.Abs (Input.GetAxis ("Mouse ScrollWheel")), MaxToClamp - Mathf.Abs (ZoomAmount));
					*/

		//Camera.main.transform.Translate (0, 0, Input.GetAxis ("Mouse ScrollWheel")* zoomSpeed);

        if(Camera.main.orthographicSize + Input.GetAxis("Mouse ScrollWheel") * zoomSpeed>0)
            Camera.main.orthographicSize = Camera.main.orthographicSize + Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;        

		if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
			mouseOrigin = Input.mousePosition;



		// sposto la telecamera
		if (Input.GetMouseButton (2)) {
			Vector3 pos = Camera.main.ScreenToViewportPoint ((-Input.mousePosition+ mouseOrigin)*500);
			transform.Translate (pos, Space.Self);
		}

		// Effettuo la rotazione libera intorno all'asse Y
		/*if(Input.GetMouseButton (1)){
			Vector3 pos = Camera.main.ScreenToViewportPoint ((-Input.mousePosition- mouseOrigin));
			transform.RotateAround (puntoPivot, Vector3.up, pos.y * turnSpeed2);
		} */
				



		//abilito i pulsanti per muovere la camera (indipendentemente se è stato selezionato il pulsante
		if(Input.GetKey(KeyCode.LeftShift)){ // PAN 
			if(Input.GetKey(KeyCode.LeftArrow))
				pansx ();
			if(Input.GetKey(KeyCode.RightArrow))
				pandx ();
			if(Input.GetKey(KeyCode.UpArrow))
				panup ();
			if(Input.GetKey(KeyCode.DownArrow))
				pandw();	
		
		}else{

			if (Input.GetKey(KeyCode.LeftArrow))
				rotatesx();
			
			if (Input.GetKey(KeyCode.RightArrow))
				rotatedx();

			if (Input.GetKey(KeyCode.UpArrow))
				zoomIN();
			if (Input.GetKey(KeyCode.DownArrow))
				zoomOUT();
		}




		manualRotation = true;





			

			if (isRotating) {
					if (targetAngle != 0) {
							Rotate ();
					} else {
							if (targetAngle2 != 0) {
									Rotate2 ();
							} else {
									isRotating = false;
							}
					}
			}
	}

	private string lastClick = "";



	public void rotateCamera(int degree){
		
		isRotating = true;
		resetCamera();
		// Effettuo prima una trasformazione per arrivare al punto di partenza della camera
		// e poi applico la rotazione desiderata, questo mi serve per evitare somme di trasformazioni.
		targetAngle = lastRotation;
		lastRotation = degree;
		lastVector = Vector3.up;
		targetAngle2 = -lastRotation;
	Rotate ();
		//layer.cameraMoved (true);

	}

	public void goLeftFor30Degree ()
	{
			// Resetto la camera se è stata mossa manualmente
			if(manualRotation)
				resetCamera();

			if (lastClick == "30left" || isRotating)
					return;
			lastClick = "30left";
			isRotating = true;
		
			// Effettuo prima una trasformazione per arrivare al punto di partenza della camera
			// e poi applico la rotazione desiderata, questo mi serve per evitare somme di trasformazioni.
			targetAngle = lastRotation;
			lastRotation = 30;
			lastVector = Vector3.up;
			targetAngle2 = -lastRotation;

	}

	public void goRightFor30Degree ()
	{
			// Resetto la camera se è stata mossa manualmente
			if(manualRotation)
				resetCamera();

			if (lastClick == "30right" || isRotating)
					return;
			lastClick = "30right";
			isRotating = true;

			// Effettuo prima una trasformazione per arrivare al punto di partenza della camera
			// e poi applico la rotazione desiderata, questo mi serve per evitare somme di trasformazioni.
			targetAngle = lastRotation;
			lastRotation = -30;
			lastVector = Vector3.up;
			targetAngle2 = -lastRotation;

	}

	public void goInFront ()
	{
			// Resetto la camera se è stata mossa manualmente
			if(manualRotation)
				resetCamera();

			if (lastClick == "front" || isRotating)
					return;
			lastClick = "front";
			isRotating = true;

			// Effettuo prima una trasformazione per arrivare al punto di partenza della camera
			// e poi applico la rotazione desiderata, questo mi serve per evitare somme di trasformazioni.
			targetAngle = lastRotation;
			lastRotation = 0;
			lastVector = Vector3.up;
			targetAngle2 = -lastRotation;

	}

	public void goToBack ()
	{
			// Resetto la camera se è stata mossa manualmente
			if(manualRotation)
				resetCamera();

			if (lastClick == "back" || isRotating)
					return;
			lastClick = "back";
			isRotating = true;

			// Effettuo prima una trasformazione per arrivare al punto di partenza della camera
			// e poi applico la rotazione desiderata, questo mi serve per evitare somme di trasformazioni.
			targetAngle = lastRotation;
			lastRotation = -180;
			lastVector = Vector3.up;
			targetAngle2 = -lastRotation;

	}


	private float targetAngle = 0;
	private float targetAngle2 = 0;
	const float rotationAmount = 1.5f;
	public float rDistance = 1.0f;
	public float rSpeed = 1.0f;

	protected void Rotate ()
	{
			if (targetAngle > 0) {
					transform.RotateAround (puntoPivot, Vector3.up, -rotationAmount);
					targetAngle -= rotationAmount;
			} else if (targetAngle < 0) {
					transform.RotateAround (puntoPivot, Vector3.up, rotationAmount);
					targetAngle += rotationAmount;
			}
	}

	protected void Rotate2 ()
	{
			if (targetAngle2 > 0) {
					transform.RotateAround (puntoPivot, Vector3.up, -rotationAmount);
					targetAngle2 -= rotationAmount;
			} else if (targetAngle2 < 0) {
					transform.RotateAround (puntoPivot, Vector3.up, rotationAmount);
					targetAngle2 += rotationAmount;
			}
	}

	


	public void panup(float factor=1){
		Vector3 p=new Vector3(0,50*factor,0);
		transform.Translate (p, Space.Self);
	}

	public void pandw(float factor=1){
		Vector3 p=new Vector3(0,-50*factor,0);
		transform.Translate (p, Space.Self);
	}

	public void pandx(float factor=1){
		Vector3 p=new Vector3(50*factor,0,0);
		transform.Translate (p, Space.Self);
	}

	public void pansx(float factor=1){
		Vector3 p=new Vector3(-50*factor,0,0);
		transform.Translate (p, Space.Self);
	}


	public void rotatesx(float factor=1){
		transform.RotateAround (puntoPivot, Vector3.up, -0.1f * turnSpeed2*factor);
	}

	public void rotatedx(float factor=1){
		transform.RotateAround (puntoPivot, Vector3.up, 0.1f * turnSpeed2*factor);
	}


	public void zoomIN(float factor=1)
	{
		Camera.main.transform.Translate (0, 0, factor* zoomSpeedManual);
        Camera.main.orthographicSize=Camera.main.orthographicSize+factor * zoomSpeedManual;
	}

	public void zoomOUT(float factor=1)
	{
		Camera.main.transform.Translate (0, 0, -factor* zoomSpeedManual);
        Camera.main.orthographicSize = Camera.main.orthographicSize - factor * zoomSpeedManual;
	}

	public void resetCamera(bool saveLastCameraPos=true)
	{
		if (saveLastCameraPos) {
			lastCameraPos = Camera.main.transform.position;
			lastCameraRot = Camera.main.transform.rotation;
		}
		Camera.main.transform.position = initialCameraPos;
		Camera.main.transform.rotation = initialCameraRot;

	}

	public void lastCamera(){
		Camera.main.transform.position = lastCameraPos;
		Camera.main.transform.rotation = lastCameraRot;
	}
}
