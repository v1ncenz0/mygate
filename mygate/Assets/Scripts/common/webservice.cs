﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


/// <summary>
/// Scaricamento dal sito www.fabbridea.com di tutti gli elementi necessari alla configurazione (paletta colori, elenco elementi, etc)
/// </summary>

public class webservice : MonoBehaviour {


	const string URL_DOWNLOAD_COLORS=  "index.php?option=com_configuratore&view=colores&format=json&limit=1000000000";
	const string URL_DOWNLOAD_ELEMENTS="index.php?option=com_configuratore&view=elementos&format=json&idStile=";
	const string URL_DOWNLOAD_FRAME="index.php?option=com_configuratore&view=tipologium&type=telaio&format=json&id=";
	const string URL_DOWNLOAD_HAT="index.php?option=com_configuratore&view=cimasa&format=json&id=";
	const string URL_DOWNLOAD_SCENES="index.php?option=com_configuratore&view=scene&format=json";
	const string URL_DOWNLOAD_STYLES="index.php?option=com_configuratore&view=stiles&format=json";
	const string URL_DOWNLOAD_TYPES="index.php?option=com_configuratore&view=tipologia&format=json";
	const string URL_DOWNLOAD_GEOMETRIES="index.php?option=com_configuratore&view=cimasas&format=json";
	const string URL_LOGIN = "index.php?option=com_configuratore&view=utente&command=login&format=json";
	const string URL_DOWNLOAD_GATES=  "index.php?option=com_configuratore&view=cancellos&format=json&showinvoice=0";
	const string URL_DOWNLOAD_GATE = "index.php?option=com_configuratore&view=cancello&format=json&task=load";


	public static bool isDownloaded=false;

	public static webservice InitWebService(){
		webservice wb=GameObject.FindObjectOfType<webservice> ();

		if (wb == null) {
			GameObject wb_go = new GameObject ();
			wb_go.name = "webservice";
			wb=wb_go.AddComponent<webservice> ();
		}

		return wb;
	}


	public void InitInterface(Action<bool> finish){

		//verifica se le librerie sono state già scaricate
		if (webservice.isDownloaded) {
			finish (true);
			return;
		}

		StartCoroutine (DownloadStyleList ((FinishStyleList)=>{
			if(FinishStyleList)
				StartCoroutine (DownloadTypeList ((FinishTypeList)=>{
					if(FinishTypeList)
						StartCoroutine (DownloadGeometriesList ((FinishGeometriesList)=>{
							if(FinishGeometriesList)
								StartCoroutine (DownloadColorList ((FinishColorList)=>{
									PoolDownloadColor ((finishAllDownloadImages)=>{
										if(finishAllDownloadImages)
												finish(true);
												webservice.isDownloaded=true;
											
									});
								}));
						}));
				}));
		}));

	}









	#region Download Styles

	/// <summary>
	/// Downloads the styles list.
	/// </summary>
	/// <returns>The style list.</returns>
	/// <param name="www">Www.</param>
	/// <param name="finish">Finish.</param>
	private IEnumerator DownloadStyleList (Action <bool> finish){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_STYLES);

		ArrayList styles=new ArrayList();

		GUIManager.setLoadingMessage ("Scaricamento stili");

		yield return www;
		
		if (www.error  ==null) {

			JSONNode nodes = JSON.Parse (www.text);
			styles = fromJsonToStyles (nodes);
			Library.StylesAvaible = styles;
			finish (true);
		} else {
			Debug.Log ("Download Style List :"+www.error);
			GUIManager.setLoadingMessage ("Errore durante lo scaricamento degli stili",true);
			finish (false);
		}
	}


	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to styles.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToStyles (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemStyle item = new ItemStyle ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["nome"].Value;
			item.image_path =  nodes [i] ["immagine"].Value;
			item.generator = new Vector2 (nodes [i] ["mindimx"].AsInt, nodes [i] ["mindimy"].AsInt);
			item.elements = new ArrayList ();
			for (int j = 0; j < nodes [i] ["elements"].AsArray.Count; j++) {
				Item e = new Item ();
				e.id = nodes [i] ["elements"][j] ["id"].AsInt;
				e.name = nodes [i] ["elements"][j] ["nome"].Value;
				e.obj_path=System.Uri.EscapeUriString (StaticVariables.BASE_URL +nodes [i] ["elements"][j] ["oggetto3D"].Value);

				e.weight = nodes [i] ["elements"][j] ["peso"].AsDouble;
				e.dimension = new Vector3 (nodes [i] ["elements"][j] ["dimX"].AsFloat, nodes [i] ["elements"][j] ["dimY"].AsFloat, nodes [i] ["elements"][j] ["dimZ"].AsFloat);;
				e.rotate = new Vector3 (nodes [i] ["elements"][j] ["rollio"].AsFloat, nodes [i] ["elements"][j] ["beccheggio"].AsFloat, nodes [i] ["elements"][j] ["imbardata"].AsFloat);
				e.type = nodes [i] ["elements"][j] ["categoria_id"].AsInt;
				e.style = nodes [i] ["elements"][j] ["stile_id"].AsInt;
				e.iconPath = StaticVariables.BASE_URL + nodes [i] ["elements"][j] ["immagine"];
				item.elements.Add (e);
			}
			item.setCellsElements ();
			result.Add (item);
		}
		return result;
	}

	#endregion

	#region Download Types

	/// <summary>
	/// Downloads the types list.
	/// </summary>
	/// <returns>The style list.</returns>
	/// <param name="www">Www.</param>
	/// <param name="finish">Finish.</param>
	private IEnumerator DownloadTypeList (Action <bool> finish){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_TYPES);

		ArrayList types=new ArrayList();

		GUIManager.setLoadingMessage ("Scaricamento tipologie");

		yield return www;

		if (www.error == null) {

			JSONNode nodes = JSON.Parse (www.text);
			
			types = fromJsonToTypes (nodes);
			Library.TypesAvaible = types;
			finish (true);
		} else {
			Debug.Log ("Download Type List :"+www.error);
			GUIManager.setLoadingMessage ("Errore durante lo scaricamento delle tipologie",true);
			finish (false);
		}
	}


	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to styles.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToTypes (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemType item = new ItemType ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["nome"].Value;
			item.image_path =  nodes [i] ["immagine"].Value;
			item.opening =  nodes [i] ["versoApertura"].Value;
			item.description = nodes [i] ["descrizione"].Value;
			item.misure = new ArrayList ();

			for (int j = 0; j < nodes [i] ["tipologiamisure"].AsArray.Count; j++) {
				ItemMisure m = new ItemMisure ();
				m.id = nodes [i] ["tipologiamisure"][j] ["id"].AsInt;
				m.min_width = nodes [i] ["tipologiamisure"][j] ["min_larghezza"].AsInt;
				m.max_width = nodes [i]["tipologiamisure"] [j] ["max_larghezza"].AsInt;
				m.area_lateral = nodes [i]["tipologiamisure"] [j] ["area_laterale"].AsInt;
				m.width_frame = nodes [i]["tipologiamisure"] [j] ["larghezza_telaio"].AsInt;
				m.width_central_frame = nodes [i]["tipologiamisure"] [j] ["larghezza_battente"].AsInt;
				m.area_bottom = nodes [i]["tipologiamisure"] [j] ["area_inferiore"].AsInt;
				m.height_bottom_frame = nodes [i] ["tipologiamisure"][j] ["altezza_traverso_inferiore"].AsInt;
				m.height_top_frame = nodes [i] ["tipologiamisure"][j] ["altezza_traverso_superiore"].AsInt;
				m.factor = nodes [i]["tipologiamisure"] [j] ["fattore_moltiplicativo_larghezza_telaio"].AsFloat;
				m.area_top = nodes [i] ["tipologiamisure"][j] ["area_superiore"].AsInt;
				item.misure.Add (m);
			}
				



			result.Add (item);
		}
		return result;
	}

	#endregion


	#region Download Geometries

	/// <summary>
	/// Downloads the types list.
	/// </summary>
	/// <returns>The style list.</returns>
	/// <param name="www">Www.</param>
	/// <param name="finish">Finish.</param>
	private IEnumerator DownloadGeometriesList (Action <bool> finish){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_GEOMETRIES);

		ArrayList geometries=new ArrayList();

		GUIManager.setLoadingMessage ("Scaricamento cimase");

		yield return www;

		if (www.error == null) {

			JSONNode nodes = JSON.Parse (www.text);
			geometries = fromJsonToGeometries (nodes);
			Library.GeometriesAvaible = geometries;
			finish (true);
		} else {
			Debug.Log ("Download Geometries List :"+www.error);
			GUIManager.setLoadingMessage ("Errore durante lo scaricamento delle cimase",true);
			finish (false);
		}
	}


	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to styles.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToGeometries (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemGeometry item = new ItemGeometry ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["nome"].Value;
			item.image_path =  nodes [i] ["immagine"].Value;
			item.showStructure = nodes [i] ["strutturaVisibile"].AsBool;
			item.showBottomBar = Utility.convertIntToBool(nodes [i] ["conBarraInBasso"]);
			item.type_curve = nodes [i] ["tipoCurva"].Value;

			JSONArray points = nodes [i] ["punti"].AsArray;
			Vector2[] positions = new Vector2[points.Count];
			for (int j=0; j<points.Count; j++) {
				JSONNode point = points [j];
				positions [j] = new Vector2 (point ["x"].AsFloat, point ["y"].AsFloat);
			}

			item.relativePositions = positions;


			result.Add (item);
		}
		return result;
	}

	#endregion

	#region Download Colors

	int colorDownloaded=0;

	/// <summary>
	/// Pools the color of the download.
	/// </summary>
	private void PoolDownloadColor(Action <bool> finish){


		for (int i = 0; i < Library.ColorsAvaible.Count; i++) {
			ItemColor c = (ItemColor)Library.ColorsAvaible [i];
			StartCoroutine (c.downloadTextureColor ((textureDownloaded) => {
				colorDownloaded++;
				verifyDownloadTexture ((finishDownloadTexture)=>{
					finish(true);
				});
			}
			));

		}

	}

	private void verifyDownloadTexture(Action <bool> finish){
		if (Library.ColorsAvaible.Count == colorDownloaded)
			finish (true);
	}

	/// <summary>
	/// Downloads the color list.
	/// </summary>
	/// <returns>The color list.</returns>
	/// <param name="www">Www.</param>
	/// <param name="finish">Finish.</param>
	private IEnumerator DownloadColorList (Action <bool> finish){
		
		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_COLORS);

		ArrayList colors=new ArrayList();

		GUIManager.setLoadingMessage ("Configurazione colori");

		yield return www;

		if (www.error == null) {
			
			JSONNode nodes = JSON.Parse (www.text);
			colors = fromJsonToColors (nodes);
			Library.ColorsAvaible = colors;
			finish (true);
		} else {
			Debug.Log ("Download Color List :"+www.error);
			GUIManager.setLoadingMessage ("Errore durante la configurazione dei colori",true);
			finish (false);
		}



	
	

	}

	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to colors.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToColors (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemColor item = new ItemColor ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["material"].Value;
			item.color = ItemColor.HexToColor(nodes [i] ["colore"].Value);
			item.texture_path =  nodes [i] ["texture"].Value;
			item.image_path =  nodes [i] ["immagine"].Value;
			result.Add (item);
		}
		return result;
	}


	#endregion

	#region DownloadElements
	/// <summary>
	/// metodo per lo scaricamento del singolo OBJ
	/// </summary>
	/// <returns>The element.</returns>
	/// <param name="item">Item.</param>
	/// <param name="callback">Callback.</param>
	/// <param name="waitingform">If set to <c>true</c> waitingform.</param>
	public static void downloadElement(Item item,Action<GameObject> callback,bool waitingform=true){

		//verifica che l'item non ha un obj

		if (!item.obj) {
			
			//scarica l'obj
			GameObject g = new GameObject ();
			OBJ obj = g.AddComponent<OBJ> ();
			obj.objPath = item.obj_path;
		
			obj.StartLoadCoroutine ((value)=>{
				
				Destroy(g.GetComponent<OBJ>());
				callback (g);


			});

		}else	
			callback ((GameObject)item.obj);

	}



	#endregion





	#region DownloadScenes

	private IEnumerator DownloadScenes (Action <bool> finish){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_SCENES);
		ArrayList scenes=new ArrayList();


		GUIManager.setLoadingMessage ("Configurazione scene");

		yield return www;

		if (www.error == null) {
			JSONNode nodes=JSON.Parse(www.text);
			scenes = fromJsonToScenes (nodes);
		}


		Library.ScenesAvaible = scenes;
		finish (true);




	}

	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to scenes.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToScenes (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemScene item = new ItemScene ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["nome"].Value;
			item.image_bg_path =  nodes [i] ["immagine_sfondo"].Value;
			item.image_floor_path =  nodes [i] ["immagine_pavimento"].Value;
			item.texture_wall_path =  nodes [i] ["texture_muro"].Value;
			item.texture_column_path =  nodes [i] ["texture_colonna"].Value;
			item.camera_rotate =  nodes [i] ["rotazione_camera"].AsInt;
			item.lamp_rotate = new Vector3 (nodes [i] ["rotazione_luce_x"].AsInt, nodes [i] ["rotazione_luce_y"].AsInt, nodes [i] ["rotazione_luce_z"].AsInt);
			item.lamp_intensity=nodes [i] ["intesita_luce"].AsFloat;
			result.Add (item);
		}
		return result;
	}

	#endregion


	#region LOGIN

	public IEnumerator login(string username,string password,Action<bool> success){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_LOGIN+"&username="+username+"&password="+password);
	

		GUIManager.setLoadingMessage ("Accesso in corso...");

		yield return www;

		if (www.error == null) {
			JSONNode nodes=JSON.Parse(www.text);
			bool status = nodes ["status"].AsBool;
			if (status) {
				StaticVariables.id_user = nodes ["id_user"].AsInt;
				StaticVariables.username = username;
				StaticVariables.password = password;

				PlayerPrefs.SetString ("username", username);
				PlayerPrefs.SetString ("password", password);

				success (true);


			} else
				success (false);
			
		}else
			success (false);

	}


	#endregion

	#region GATES

	public IEnumerator DownloadGates (int start,int limit,Action <int> finish){

		WWW www=new WWW(StaticVariables.BASE_URL+URL_DOWNLOAD_GATES+"&limitstart="+start.ToString()+"&limit="+limit.ToString()+"&id_user="+StaticVariables.id_user.ToString());
		ArrayList gates=new ArrayList();

	
		GUIManager.setLoadingMessage ("Scaricamento configurazioni");

		yield return www;

		if (www.error == null) {
			JSONNode nodes = JSON.Parse (www.text);
			gates = fromJsonToGates (nodes);
			for (int i = 0; i < gates.Count; i++)
				Library.GatesAvaible.Add (gates [i]);
			
			finish ( gates.Count);
		} else {
			finish (0);
			Debug.Log ("Error download gates " + www.error);

		}





	}

	/// <summary>
	/// Parsing dei valori da JSON ad arraylist
	/// </summary>
	/// <returns>The json to scenes.</returns>
	/// <param name="nodes">Nodes.</param>
	private ArrayList fromJsonToGates (JSONNode nodes)
	{
		ArrayList result = new ArrayList ();
		for (int i=0; i < nodes.Count; i++) {
			ItemGate item = new ItemGate ();
			item.id = nodes [i] ["id"].AsInt;
			item.name = nodes [i] ["nome"].Value;
			item.image_path =  nodes [i] ["immagine"].Value;
			item.date =  Utility.ConvertDBDate(nodes [i] ["creazione"].Value);
			item.id_type = nodes [i] ["tipologia_id"].AsInt;
			item.id_style = nodes [i] ["stile_id"].AsInt;
			item.id_geometry = nodes [i] ["cimasa_id"].AsInt;
			item.width = nodes [i] ["larghezza"].AsInt;
			item.height = nodes [i] ["altezza"].AsInt;
			item.heightmax = nodes [i] ["altezzaMax"].AsInt;
			result.Add (item);
		}
		return result;
	}

	#endregion


	#region GATE


	public IEnumerator downloadGateFromDB(int id,Action<ItemGate> finish,Action<string> error){


		WWW www = new WWW (StaticVariables.BASE_URL + URL_DOWNLOAD_GATE+"&user="+StaticVariables.username+"&password="+StaticVariables.password+"&id="+id.ToString());

		yield return www;

		// check for errors
		if (www.error == null) {

			JSONNode nodes = JSON.Parse (www.text);
			String result=nodes["success"];

			if (result == "no") {
				error (nodes ["error"]);
			} else {


				JSONNode db = nodes ["values"];

				JSONNode gate = db ["gate"];

				ItemGate g = new ItemGate ();

				g.id = id;
				g.height = gate ["height"].AsInt;
				g.heightmax = gate ["maxheight"].AsInt;
				g.width = gate ["width"].AsInt;
				g.id_style = gate ["id_style"].AsInt;
				g.id_geometry = gate ["geometry"].AsInt;
				g.id_type = gate ["type"].AsInt;
				g.opening = gate ["opening"];
				g.configuration = JSON.Parse (gate ["layers"].ToString ()).AsArray;

				g.updateOtherParams ();
				finish (g);
			}
		}else
			error (www.error);
		



	}

	#endregion
}
