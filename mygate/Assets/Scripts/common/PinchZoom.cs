﻿using UnityEngine;

/**
 * Classe che gestisce lo zoom con il pinch su dispositivi mobile.
 * Scaricata da internet.
 */
public class PinchZoom : MonoBehaviour
{
		public float perspectiveZoomSpeed = 0.00000005f;        // The rate of change of the field of view in perspective mode.
		public float orthoZoomSpeed = 0.0000005f;        // The rate of change of the orthographic size in orthographic mode.

		void Update ()
		{
				// If there are two touches on the device...
				if (Input.touchCount == 2) {
						// Store both touches.
						Touch touchZero = Input.GetTouch (0);
						Touch touchOne = Input.GetTouch (1);
			
						// Find the position in the previous frame of each touch.
						Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
						Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
						// Find the magnitude of the vector (the distance) between the touches in each frame.
						float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
						float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
						// Find the difference in the distances between each frame.
						float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

						Camera.main.transform.Translate (0, 0, deltaMagnitudeDiff * perspectiveZoomSpeed);

						
				}
		}
}