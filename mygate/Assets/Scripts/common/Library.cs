﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Library : MonoBehaviour {

	public static ArrayList ItemsAvaible;
	public static ArrayList ColorsAvaible;
	public static ArrayList ScenesAvaible;
	public static ArrayList StylesAvaible;
	public static ArrayList TypesAvaible;
	public static ArrayList GeometriesAvaible;
	public static ArrayList GatesAvaible=new ArrayList();


	public static bool last_is_saved = true;

	public static ItemStyle searchStyle(int id){

		for (int i = 0; i < StylesAvaible.Count; i++) {
			ItemStyle item = (ItemStyle)StylesAvaible [i];
			if (item.id == id)
				return item;

		}

		return null;
			

	}


	public static ItemType searchType(int id){

		for (int i = 0; i < TypesAvaible.Count; i++) {
			ItemType item = (ItemType)TypesAvaible [i];
			if (item.id == id)
				return item;

		}

		return null;


	}


	public static ItemGeometry searchGeometry(int id){

		for (int i = 0; i < GeometriesAvaible.Count; i++) {
			ItemGeometry item = (ItemGeometry)GeometriesAvaible [i];
			if (item.id == id)
				return item;

		}

		return null;


	}
}
