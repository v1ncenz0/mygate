using System;

/**
 * Struttura facente parte di OBJ.cs.
 * Scaricata da internet e modificata da Marco Cozza
 */
public struct FaceIndices
{
	public int vi;
	public int vu;
	public int vn;
}
