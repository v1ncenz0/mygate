using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Classe facente parte di OBJ.cs.
 * Scaricata da internet e modificata da Marco Cozza
 */
public class GeometryBuffer {

	private List<ObjectData> objects;
	public List<Vector3> vertices;
	public List<Vector2> uvs;
	public List<Vector3> normals;
	public int unnamedGroupIndex = 1; // naming index for unnamed group. like "Unnamed-1"

	private ObjectData current;
	
	/// <summary>
	/// This class represents an object with faces, vertices, uvs and normals. 
	/// The object can be defined in the .obj file (a row starting with "o") or it can be created by the splitting process.
	/// </summary>
	private class ObjectData {
		
		public string name;
		public List<GroupData> groups;
		
		/// <summary>
		/// The list of faces row of the objects
		/// </summary>
		public List<FaceIndicesRow> allFacesRow;

		/// <summary>
		/// The list of vertices of the object
		/// </summary>
		public List<Vector3> vertices;
		
		/// <summary>
		/// The list of uvs of the object
		/// </summary>
		public List<Vector2> uvs;

		/// <summary>
		/// The list of normals of the object
		/// </summary>
		public List<Vector3> normals;

		/// <summary>
		/// The list of triangles of the object
		/// </summary>
		public List<int> triangles;
		
		public int normalCount;

		public ObjectData() {
			groups = new List<GroupData>();
			allFacesRow = new List<FaceIndicesRow>();
			normalCount = 0;

			vertices = new List<Vector3>();
			uvs = new List<Vector2>();
			normals = new List<Vector3>();

			triangles = new List<int>();
		}
	}
	
	private GroupData curgr;
	private class GroupData {
		public string name;
		public string materialName;
		//public List<FaceIndices> faces;
		public List<FaceIndicesRow> faces;
		
		// Aggiunte marco
		/*
		public List<Vector3> vertices;
		public List<Vector2> uvs;
		public List<Vector3> normals;
		 * */
		// Fine aggiunte marco

		public GroupData() {
			//faces = new List<FaceIndices>();
			faces = new List<FaceIndicesRow>();
			// Aggiunte marco
			/*
			vertices = new List<Vector3>();
			uvs = new List<Vector2>();
			normals = new List<Vector3>();
			 * */
			// Fine aggiunte marco
		}
		public bool isEmpty { get { return faces.Count == 0; } }
	}

	/// <summary>
	/// This class contains a list of FaceIndices
	/// </summary>
	public class FaceIndicesRow
	{
		public List<FaceIndices> faceIndices;

		public FaceIndicesRow()
		{
			faceIndices = new List<FaceIndices>();
		}
	}
	
	public GeometryBuffer() {
		objects = new List<ObjectData>();
		
		ObjectData d = new ObjectData();
		d.name = "default";
		objects.Add(d);
		current = d;
		
		GroupData g = new GroupData();
		g.name = "default";
		d.groups.Add(g);
		curgr = g;
		
		vertices = new List<Vector3>();
		uvs = new List<Vector2>();
		normals = new List<Vector3>();
	}
	
	public void PushObject(string name) {
		//Debug.Log("Adding new object " + name + ". Current is empty: " + isEmpty);
		
		//if(isEmpty) objects.Remove(current);
		
		ObjectData n = new ObjectData();
		n.name = name;
		objects.Add(n);
		
		GroupData g = new GroupData();
		g.name = "default";
		n.groups.Add(g);
		
		curgr = g;
		current = n;
	}
	
	public void PushGroup(string name) {
		if(curgr.isEmpty) current.groups.Remove(curgr);
		GroupData g = new GroupData();
		if (name == null) {
			name = "Unnamed-"+unnamedGroupIndex;
			unnamedGroupIndex++;
		}
		g.name = name;
		current.groups.Add(g);
		curgr = g;
	}
	
	public void PushMaterialName(string name) {
		//Debug.Log("Pushing new material " + name + " with curgr.empty=" + curgr.isEmpty);
		if(!curgr.isEmpty) PushGroup(name);
		if(curgr.name == "default") curgr.name = name;
		curgr.materialName = name;
	}
	
	public void PushVertex(Vector3 v) {
		vertices.Add (v);
		current.vertices.Add (v);

	}
	
	public void PushUV(Vector2 v) {
		uvs.Add(v);
		current.uvs.Add(v);
	}
	
	public void PushNormal(Vector3 v) {
		normals.Add(v);
		current.normals.Add(v);
	}
	
	/*
	public void PushFace(FaceIndices f) {
		curgr.faces.Add(f);
		current.allFaces.Add(f);
		if (f.vn >= 0) {
			current.normalCount++;
		}
	}
	*/

	public void PushFaceRow(FaceIndicesRow f)
	{
		curgr.faces.Add(f);
		current.allFacesRow.Add(f);
		foreach (FaceIndices fi in f.faceIndices)
			if (fi.vn >= 0)
				current.normalCount++;
	}
	
	public void Trace() {
		Debug.Log("OBJ has " + objects.Count + " object(s)");
		Debug.Log("OBJ has " + vertices.Count + " vertice(s)");
		Debug.Log("OBJ has " + uvs.Count + " uv(s)");
		Debug.Log("OBJ has " + normals.Count + " normal(s)");
		foreach(ObjectData od in objects) {
			Debug.Log(od.name + " has " + od.groups.Count + " group(s)");
			foreach(GroupData gd in od.groups) {
				Debug.Log(od.name + "/" + gd.name + " has " + gd.faces.Count + " faces(s)");
			}
		}
		
	}
	
	public int numObjects { get { return objects.Count; } }	
	public bool isEmpty { get { return vertices.Count == 0; } }
	public bool hasUVs { get { return uvs.Count > 0; } }
	public bool hasNormals { get { return normals.Count > 0; } }

	public static int MAX_VERTICES_LIMIT_FOR_A_MESH = 64999;
	
	public void PopulateMeshes(GameObject[] gs, Dictionary<string, Material> mats) {
		if(gs.Length != numObjects) return; // Should not happen unless obj file is corrupt...
		//Debug.Log("PopulateMeshes GameObjects count:"+gs.Length);

		for(int i = 0; i < gs.Length; i++) {
			ObjectData od = objects[i];
			
			//Debug.Log("Object: " + od.name + " - verts: " + od.vertices.Count + " - uvs: " + od.uvs.Count + " - norm: " + od.normals.Count + " - facesRow: " + od.allFacesRow.Count + " - faces: " + CountTotalObjectFaces(od));
			bool objectHasNormals = (hasNormals && od.normalCount > 0);
			
			if(od.name != "default") gs[i].name = od.name;
			//Debug.Log("PopulateMeshes object name:"+od.name);

			// If necessary, we have to split the object in many sub-objects, storing in this list splitted data.
			List<ObjectData> childObjects = new List<ObjectData>();

			/*
			List<Vector3> tvertices = new List<Vector3>();
			List<Vector2> tuvs = new List<Vector2>();
			List<Vector3> tnormals = new List<Vector3>();
			*/

			List<Vector3> temptvertices = new List<Vector3>();
			List<Vector2> temptuvs = new List<Vector2>();
			List<Vector3> temptnormals = new List<Vector3>();

			// The vertex dictionary identified by a Vector3 (vertex's x, y, z). Each vertex can have a list of position in the vertex array, because we can have duplicates with differente uvs.
			Dictionary<Vector3, List<int>> tempvertDic = new Dictionary<Vector3, List<int>>();
			
			// The vertex dictionary identified by its position in the vertex dictionary. It is useful to get directly the vertex at a current position
			Dictionary<int, Vector3> tempverticesOrderedDictionary = new Dictionary<int, Vector3>();
			
			List<int> tempttriangles = new List<int>();

			int k = 0;

			// InnerK is used to split an object (and a mesh) in sub-object (sub-meshes)
			int innerK = 0;

			int childNumber = 0;

			foreach (FaceIndicesRow fir in od.allFacesRow)
			{
				if (k + fir.faceIndices.Count >= MAX_VERTICES_LIMIT_FOR_A_MESH && childNumber == 0 || 
					childNumber > 0 && innerK + fir.faceIndices.Count >= MAX_VERTICES_LIMIT_FOR_A_MESH) // We reached the size limit of a mesh, so we have to split the object
				{
					Debug.LogWarning("maximum vertex number for a mesh exceeded for object:" + gs[i].name + " - " + childNumber + " - " + innerK);
					
					ObjectData objectData = new ObjectData();
					objectData.vertices = new List<Vector3>(tempverticesOrderedDictionary.Values);
					objectData.normals = temptnormals;
					objectData.uvs = temptuvs;
					objectData.triangles = tempttriangles;
					childObjects.Add(objectData);
					Debug.Log("Oggetto splittato: v: " + objectData.vertices.Count + " - n: " + objectData.normals.Count + " - u: " + objectData.uvs.Count);
					
					temptvertices = new List<Vector3>();
					temptuvs = new List<Vector2>();
					temptnormals = new List<Vector3>();
					tempttriangles = new List<int>();

					tempvertDic = new Dictionary<Vector3, List<int>>();
					tempverticesOrderedDictionary = new Dictionary<int, Vector3>();

					childNumber++;
					innerK = 0;
				}

				foreach (FaceIndices f in fir.faceIndices)
				{
					
					if (this.AddVertexToList(tempvertDic, tempverticesOrderedDictionary, tempttriangles, temptuvs, temptnormals, f))
					{
						
						if (HasObjectUV(od))
						{
							temptuvs.Add(uvs[f.vu]);
						}
						if (HasObjectNormals(od) && f.vn >= 0)
						{
							temptnormals.Add(normals[f.vn]);
						}
						k++;
						if (childNumber >= 0)
							innerK++;
					}
				}

			}


			// Now we assign remaining values to a new ObjectData, so we can store last splitted object or the whole object, if it was not necessary to split it.
			if (childObjects.Count >= 0)
			{
				ObjectData lastObjectData = new ObjectData();
				lastObjectData.vertices = new List<Vector3>(tempverticesOrderedDictionary.Values);//temptvertices;
				lastObjectData.normals = temptnormals;
				lastObjectData.uvs = temptuvs;
				lastObjectData.triangles = tempttriangles;
				childObjects.Add(lastObjectData);
			}


			// It counts the number of children
			int childCount = 0;

			// Processo i figli creati dinamicamente in base allo splitting della mesh, se ce ne sono
			foreach(ObjectData odata in childObjects)
			{
				// Aggiungiamo il gruppo del padre all'objectData
				odata.groups.Add(od.groups[0]);
				
				// Creiamo un GameObject che contenga la Mesh e il MeshFilter
				GameObject go = new GameObject();
				go.transform.parent = gs[i].transform;
				go.name = gs[i].name + "_default_MeshPart" + childCount++; 
				go.AddComponent(typeof(MeshRenderer));
				Mesh mesh = (go.AddComponent(typeof(MeshFilter)) as MeshFilter).mesh;
				//Debug.Log("odata v: " + odata.vertices.Count + " - u: " + odata.uvs.Count + " - groups: " + odata.groups.Count);

				Vector3[] tV = new Vector3[odata.vertices.Count];
				int vI = 0;
				foreach (Vector3 v in odata.vertices)
					tV[vI++] = v;
				
				mesh.vertices = tV;

				if (HasObjectUV(odata))
				{
					Vector2[] tU = new Vector2[odata.uvs.Count];
					int uI = 0;
					foreach (Vector2 u in odata.uvs)
						tU[uI++] = u;
					mesh.uv = tU;
				}
				if (HasObjectNormals(odata))
				{
					Vector3[] tN = new Vector3[odata.normals.Count];
					int nI = 0;
					foreach (Vector3 n in odata.normals)
						tN[nI++] = n;
					mesh.normals = tN;
				}
				if (odata.groups.Count == 1)
				{
					//Debug.Log("PopulateMeshes only one group: " + odata.groups[0].name);
					GroupData gd = odata.groups[0];
					string matName = (gd.materialName != null) ? gd.materialName : "default"; // MAYBE: "default" may not enough.
					if (mats.ContainsKey(matName))
					{
						go.GetComponent<Renderer>().material = mats[matName];
						//Debug.Log("PopulateMeshes mat:" + matName + " set.");
					}
					else
					{
						Debug.LogWarning("PopulateMeshes mat:" + matName + " not found.");
					}
					
					/*
					int[] triangles = new int[odata.vertices.Count];
					for (int j = 0; j < triangles.Length; j++) triangles[j] = j;
					*/
					
					int[] triangles = new int[odata.triangles.Count];
					int tindex = 0;
					foreach (int t in odata.triangles)
					{
						triangles[tindex] = t;
						tindex++;
					}
					
					mesh.triangles = triangles;
				}
				
				// If the object hasn't normals, we recalculate them.
				if (odata.normals.Count == 0)
					mesh.RecalculateNormals();
			}
			
			// Processo l'oggetto padre solo se questo ha meno del massimo numero di vertici nella mesh (TODO: tvertices.count > 0 ??)
			/*
			if (tvertices.Count <= MAX_VERTICES_LIMIT_FOR_A_MESH && tvertices.Count > 0) 
			{
				Mesh m = (gs[i].GetComponent(typeof(MeshFilter)) as MeshFilter).mesh;

				// Inizio test HashSet
				Vector3[] tempV = new Vector3[tvertices.Count];
				int vIndex = 0;
				foreach (Vector3 v in tvertices)
					tempV[vIndex++] = v;

				m.vertices = tempV;

				if (HasObjectUV(od))
				{
					Vector2[] tempU = new Vector2[tuvs.Count];
					int uIndex = 0;
					foreach (Vector2 u in tuvs)
						tempU[uIndex++] = u;
					m.uv = tempU;
				}
				if (HasObjectNormals(od))
				{
					Vector3[] tempN = new Vector3[tnormals.Count];
					int nIndex = 0;
					foreach (Vector3 n in tnormals)
						tempN[nIndex++] = n;
					m.normals = tempN;
				}
				Debug.Log("m.vertices: " + m.vertices.Length + " [" + tvertices.Count + "] - m.uv: " + m.uv.Length + " [" + tuvs.Count + "] - m.normals: " + m.normals.Length + "[ " + tnormals.Count + "]");

				if (od.groups.Count == 1)
				{
					Debug.Log("PopulateMeshes only one group: " + od.groups[0].name);
					GroupData gd = od.groups[0];
					string matName = (gd.materialName != null) ? gd.materialName : "default"; // MAYBE: "default" may not enough.
					if (mats.ContainsKey(matName))
					{
						gs[i].renderer.material = mats[matName];
						Debug.Log("PopulateMeshes mat:" + matName + " set.");
					}
					else
					{
						Debug.LogWarning("PopulateMeshes mat:" + matName + " not found.");
					}
					Debug.Log("total faces: " + this.CountTotalObjectFaces(od));
					int[] triangles = new int[m.vertices.Length];
					for (int j = 0; j < triangles.Length; j++) triangles[j] = j;

					m.triangles = triangles;

				}
				else
				{
					int gl = od.groups.Count;
					Material[] materials = new Material[gl];
					m.subMeshCount = gl;
					int c = 0;

					Debug.Log("PopulateMeshes group count:" + gl);
					for (int j = 0; j < gl; j++)
					{
						string matName = (od.groups[j].materialName != null) ? od.groups[j].materialName : "default"; // MAYBE: "default" may not enough.
						if (mats.ContainsKey(matName))
						{
							materials[j] = mats[matName];
							Debug.Log("PopulateMeshes mat:" + matName + " set.");
						}
						else
						{
							Debug.LogWarning("PopulateMeshes mat:" + matName + " not found.");
						}

						int[] triangles = new int[od.groups[j].faces.Count*3];
						int l = od.groups[j].faces.Count*3 + c;
						int s = 0;
						for (; c < l; c++, s++) triangles[s] = c;
						m.SetTriangles(triangles, j);
					}

					gs[i].renderer.materials = materials;
				}
				if (!objectHasNormals)
				{
					m.RecalculateNormals();
				}
			}*/
		}
	}

	private bool HasObjectUV(ObjectData od)
	{
		return od.uvs.Count > 0;
	}
	
	private bool HasObjectNormals(ObjectData od)
	{
		return od.normals.Count > 0;
	}

	private int CountTotalObjectFaces(ObjectData od)
	{
		int count = 0; // od.allFacesRow.Count;
		foreach(FaceIndicesRow fr in od.allFacesRow)
			count += fr.faceIndices.Count;
		return count;
	}

	/// <summary>
	/// This function dynamically adds a vertex in the dictionary. It first controls if it is already present (and it has same uvs), then refreshes the collections.
	/// </summary>
	/// <param name="vertDic">The vertex dictionary with a Vector3 as key</param>
	/// <param name="vertOrderedDic">The vertex dictionary with an integer as key (the position of the vertex into the vertexList)</param>
	/// <param name="tempTriangles">The list of triangles</param>
	/// <param name="tempUvs">The list of uvs</param>
	/// <param name="tempNormals">The list of normals</param>
	/// <param name="face">The current face</param>
	/// <returns>True if the vertex was not already in the dictionary and it has been added to the dictionary, False otherwise</returns>
	private bool AddVertexToList(Dictionary<Vector3, List<int>> vertDic, Dictionary<int, Vector3> vertOrderedDic, List<int> tempTriangles, List<Vector2> tempUvs, List<Vector3> tempNormals, FaceIndices face)
	{
		
		List<int> indexList;
		Vector3 vertex = vertices[face.vi];
		bool vFound = vertDic.ContainsKey(vertex);
		int vIndex = -1;
		if (!vFound) // The vertex is not present in the dictionary
		{
			
			vIndex = vertOrderedDic.Keys.Count;
			indexList = new List<int>();
			indexList.Add(vIndex);
			vertDic.Add(vertex, indexList);
			vertOrderedDic.Add(vIndex, vertex);
		}
		else // The vertex is present, but we have to check if the corresponding uv is the same
		{
			
			vertDic.TryGetValue(vertex, out indexList);
			bool vertexFacePresent = false;
			foreach (int index in indexList)
			{
				if (tempUvs.Count > 0 && tempUvs[index] == uvs[face.vu]) // We found the correspondence, so we don't add the vertex
				{
					vIndex = index;
					vertexFacePresent = true;
					break;
				}
			}
			if (!vertexFacePresent) // We didn't found the correspondence, so we have to store the vertex and its uv
			{
				vIndex = vertOrderedDic.Keys.Count;
				indexList.Add(vIndex);
				vertOrderedDic.Add(vIndex, vertex);
				vFound = false;
			}
			
		}
		tempTriangles.Add(vIndex);
		return !vFound;
	}

}



























