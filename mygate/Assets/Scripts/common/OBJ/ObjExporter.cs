﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

/**
 * Esporta un GameObject in OBJ
 */
public class ObjExporterRuntime
{
	private static int StartIndex = 0;
	public static ArrayList materialsname = new ArrayList ();
	public static ArrayList materialsncolor = new ArrayList ();

	public static int totalVertex=0;

	public static void Start ()
	{
		StartIndex = 0;
	}

	public static void End ()
	{
		StartIndex = 0;
	}



	public static string MeshToString (MeshFilter mf, Transform t)
	{	
		Vector3 s = t.localScale;
		Vector3 p = t.localPosition;
		Quaternion r = t.localRotation;


		int numVertices = 0;
		Mesh m = mf.sharedMesh;
		if (!m) {
			return "####Error####";
		}
		if (!mf.GetComponent<Renderer> ().isVisible)
			return "####Error####";

		Material[] mats = mf.GetComponent<Renderer>().sharedMaterials;

		StringBuilder sb = new StringBuilder ();
		StringBuilder sm = new StringBuilder ();




		foreach (Vector3 vv in m.vertices) {
			Vector3 v = t.TransformPoint (vv);
			numVertices++;
			sb.Append (string.Format ("v {0} {1} {2}\n", v.x, v.y, -v.z));
		}
		sb.Append ("\n");
		/*foreach (Vector3 nn in m.normals) {
				Vector3 v = r * nn;
				sb.Append (string.Format ("vn {0} {1} {2}\n", -v.x, -v.y, v.z));
			}
			sb.Append ("\n");
			

			for (int k=0;k<m.vertices.Length;k++){ //inserisci un numero di normali uguali al numero di vertici
				Vector3 v=new Vector3(0,0,0);
				if(k<m.uv.Length)
					v=m.uv[k];
				sb.Append (string.Format ("vt {0} {1}\n", v.x, v.y));
			}
*/

		foreach (Color c in m.colors) {
			sb.Append (string.Format ("vc {0} {1} {2} {3}\n", c.r, c.g, c.b, c.a));
		}


		for (int material=0; material < m.subMeshCount; material ++) {
			sb.Append ("\n");

			string materialname = "";
			Color materialcolor = new Color ();

			if (mats [material] != null) {

				//Color c=mats[material].color;
				materialname =  Regex.Replace(mats [material].name.Replace(" (Instance)",""), "[^\\w\\._]", "").ToLower() ;
				materialcolor = mats [material].color;
			}

			if (materialname.Equals (""))
				materialname = "Default";

			ObjExporterRuntime.addMaterial (materialname, materialcolor);

			sb.Append ("usemtl ").Append (materialname).Append ("\n");
			sb.Append ("usemap ").Append (materialname).Append ("\n");


			if (m.vertexCount > 0) {
				int[] triangles = m.GetTriangles (material);
				for (int i=0; i<triangles.Length; i+=3) {
					sb.Append (string.Format ("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n", 
						triangles [i] + 1 + StartIndex, triangles [i + 1] + 1 + StartIndex, triangles [i + 2] + 1 + StartIndex));
				}
			}
			//}


			StartIndex += numVertices;
		}
		return sb.ToString ();
	}

	public static void addMaterial(string name,Color color){

		for (int i=0;i<ObjExporterRuntime.materialsname.Count;i++){
			string m=(string)ObjExporterRuntime.materialsname[i];
			if(m==name)
				return;
		}

		ObjExporterRuntime.materialsname.Add (name);
		ObjExporterRuntime.materialsncolor.Add (color);



	}
}

public class ObjExporting : ScriptableObject
{
	// Punto di ingresso della classe
	public static void DoExportWSubmeshes (string fileName, GameObject go)
	{
		DoExport (true, fileName, go);
	}

	// Punto di ingresso della classe
	public static void DoExportWOSubmeshes (string fileName, GameObject go)
	{
		DoExport (false, fileName, go);
	}

	public static OBJItem getObjString (bool makeSubmeshes, GameObject go)
	{
		string meshName = go.name;

		OBJItem result = new OBJItem ();


		ObjExporterRuntime.Start ();

		StringBuilder meshString = new StringBuilder ();

		meshString.Append ("#" + meshName + ".obj"
			+ "\n#" + System.DateTime.Now.ToLongDateString () 
			+ "\n#" + System.DateTime.Now.ToLongTimeString ()
			+ "\n#-------" 
			+ "\n\n");

		Transform t = go.transform;

		Vector3 originalPosition = t.position;
		t.position = Vector3.zero;

		if (!makeSubmeshes) {
			meshString.Append ("g ").Append (t.name).Append ("\n");
		}

		string s = processTransform (t, makeSubmeshes);
		result.mtl = new ArrayList ();
		result.mtl_name = new ArrayList ();
		for (int i=0; i<ObjExporterRuntime.materialsname.Count; i++) {

			meshString.Append ("mtllib "+ObjExporterRuntime.materialsname[i]+".mtl\n");

			result.mtl.Add(ObjExporting.createMTL((string)ObjExporterRuntime.materialsname[i],(Color)ObjExporterRuntime.materialsncolor[i]));
			result.mtl_name.Add(ObjExporterRuntime.materialsname[i]);
		}



		meshString.Append (s);

		t.position = originalPosition;

		ObjExporterRuntime.End ();


		result.obj = meshString.ToString ();

		return result;
	}


	public static string createMTL(string name, Color c){
		string txt;

		txt = "newmtl "+name+"\n";
		txt = txt + "Ka "+c.r+" "+c.g+" "+c.b+"\n";
		txt = txt + "Kd "+c.r+" "+c.g+" "+c.b+"\n";

		return txt;

	}

	static void DoExport (bool makeSubmeshes, string fileName, GameObject go)
	{
		string meshName = go.name;

		ObjExporterRuntime.Start ();

		StringBuilder meshString = new StringBuilder ();

		meshString.Append ("#" + meshName + ".obj"
			+ "\n#" + System.DateTime.Now.ToLongDateString () 
			+ "\n#" + System.DateTime.Now.ToLongTimeString ()
			+ "\n#-------" 
			+ "\n\n");

		Transform t = go.transform;

		Vector3 originalPosition = t.position;
		t.position = Vector3.zero;

		if (!makeSubmeshes) {
			meshString.Append ("g ").Append (t.name).Append ("\n");
		}
		meshString.Append (processTransform (t, makeSubmeshes));

		WriteToFile (meshString.ToString (), fileName);

		t.position = originalPosition;

		ObjExporterRuntime.End ();
		Debug.Log ("Exported Mesh: " + fileName);
	}

	static string processTransform (Transform t, bool makeSubmeshes)
	{
		StringBuilder meshString = new StringBuilder ();

		meshString.Append ("#" + t.name
			+ "\n#-------" 
			+ "\n");

		if (makeSubmeshes) {
			meshString.Append ("g ").Append (t.name).Append ("\n");
		}

		MeshFilter mf = t.GetComponent<MeshFilter> ();
		if (mf) {
			meshString.Append (ObjExporterRuntime.MeshToString (mf, t));
		}

		for (int i = 0; i < t.childCount; i++) {
			meshString.Append (processTransform (t.GetChild (i), makeSubmeshes));
		}

		return meshString.ToString ();
	}

	static void WriteToFile (string s, string filename)
	{
		using (StreamWriter sw = new StreamWriter(filename)) {
			sw.Write (s);
		}
	}
}