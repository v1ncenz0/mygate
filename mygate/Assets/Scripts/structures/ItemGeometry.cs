﻿using UnityEngine;
using System;
using System.Collections;


public class ItemGeometry : MonoBehaviour {

	public int id; // Identificativo dell'item
	public string name; // Nome
	public Sprite image; //Texture del colore (se presente)
	public string image_path; //path remoto della texture


	// Vettore contente le coodinate dei punti di controllo della curva
	// Memorizzano le coordinate in termini relativi (da 0 a 1) dei punti di controllo
	public Vector2[] relativePositions;
	// Il tipo d interpolazione da utilizzare per la costruzione della curva
	public Spline.InterpolationMode interpolationMode;
	/*
		 * Flag indicante l'assenza della barra di ferro in basso alla cimasa o se vogliamo, in alto al cancello
		 * 
		 *   |---x---|
		 *   |       |
		 *   |_______|
		 */
	public bool showBottomBar;
	public bool showStructure;
	public string type_curve;


	public IEnumerator downloadImage(Action<bool> finish){


		if(this.image_path.ToString()!="null" && this.image_path.ToString()!=""){
			WWW www = new WWW (System.Uri.EscapeUriString(StaticVariables.BASE_URL +this.image_path));

			yield return www;

			if (www.texture)
				this.image =  Sprite.Create((Texture2D)www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0.5f,0.5f),10);

		}
		finish (true);
	}


	public bool isPresent(){
		if (relativePositions.Length > 0)
			return true;

		return false;
	}

}
