﻿using UnityEngine;
using System;
using System.Collections;


public class ItemStyle : MonoBehaviour {

	public int id; // Identificativo dell'item
	public string name; // Nome
	public Sprite image; //Texture del colore (se presente)
	public string image_path; //path remoto della texture
	public Vector2 generator;
	public ArrayList elements;


	public IEnumerator downloadImage(Action<bool> finish){
		

		if(this.image_path.ToString()!="null" && this.image_path.ToString()!=""){
			WWW www = new WWW (System.Uri.EscapeUriString(StaticVariables.BASE_URL +this.image_path));

			yield return www;

			if (www.texture)
				this.image =  Sprite.Create((Texture2D)www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0.5f,0.5f),10);

		}
		finish (true);
	}


	public void setCellsElements(){
		
		//imposto per ogni item la dimensione della cella
		for (int i = 0; i< elements.Count; i++) {

			Item item = (Item)elements [i];
			Vector2 dimInCellGrid;
			dimInCellGrid.x = item.dimension.x / generator.x;
			dimInCellGrid.y = item.dimension.y / generator.y;
			item.cells = dimInCellGrid;
		}

	}

	/// <summary>
	/// Corregge le dimensioni degli elementi in funzione della scalatura della griglia
	/// </summary>
	/// <param name="scale_x">Scale x.</param>
	/// <param name="scale_y">Scale y.</param>
	public void fixDimensionElements(float scale_x,float scale_y){

		for (int i = 0; i< elements.Count; i++) {

			Item item = (Item)elements [i];
			item.scale_x = scale_x;
			item.scale_y = scale_y;

			item.dimension.x=item.dimension.x * scale_x;
			item.dimension.y=item.dimension.y * scale_y;

		}

	

	}

}
