﻿using UnityEngine;
using System.Collections;

/**
 * Classe che incaplsula le infomazioni relative alla cimasa
 *
 */
public class Hat2
{
		// Id dell'elemento cappello
		public int id;
		// Vettore contente le coodinate dei punti di controllo della curva
		// Memorizzano le coordinate in termini relativi (da 0 a 1) dei punti di controllo
		public Vector2[] relativePositions;
		// Il tipo d interpolazione da utilizzare per la costruzione della curva
		public Spline.InterpolationMode interpolationMode;
		/*
		 * Flag indicante l'assenza della barra di ferro in basso alla cimasa o se vogliamo, in alto al cancello
		 * 
		 *   |---x---|
		 *   |       |
		 *   |_______|
		 */
		 public bool senzaBarra;
		 public bool strutturaVisibile;
	public string tipologia_curva;
}
