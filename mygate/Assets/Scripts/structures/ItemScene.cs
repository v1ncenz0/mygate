﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScene : MonoBehaviour {



	public int id; // Identificativo della scena
	public string name; // Nome
	public Texture image_bg; 
	public Texture image_floor; 
	public Texture texture_wall; 
	public Texture texture_column; 


	public string image_bg_path; 
	public string image_floor_path; 
	public string texture_wall_path;
	public string texture_column_path; 

	public int camera_rotate; 

	public Vector3 lamp_rotate; 
	public float lamp_intensity; 

}
