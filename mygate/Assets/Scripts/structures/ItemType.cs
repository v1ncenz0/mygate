﻿using UnityEngine;
using System;
using System.Collections;


public class ItemType : MonoBehaviour {

	public int id; // Identificativo dell'item
	public string name; // Nome
	public Sprite image; //Texture del colore (se presente)
	public string image_path; //path remoto della texture
	public string opening;
	public string description;
	public ArrayList misure;

	public IEnumerator downloadImage(Action<bool> finish){


		if(this.image_path.ToString()!="null" && this.image_path.ToString()!=""){
			WWW www = new WWW (System.Uri.EscapeUriString(StaticVariables.BASE_URL +this.image_path));

			yield return www;

			if (www.texture)
				this.image =  Sprite.Create((Texture2D)www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0.5f,0.5f),10);

		}
		finish (true);
	}


	public ItemMisure getMisure(int gross_width){
		for (int i=0;i<misure.Count;i++){
			ItemMisure item = (ItemMisure)misure [i];
			if(item.min_width<=gross_width && item.max_width>=gross_width)
				return item;

		}
		return null;
	}


    public int CalcolateNetDimension(int gross_width, int gross_height, ItemStyle style, out int net_width, out int net_height, out int net_height_max){

        return CalcolateNetDimension(gross_width, gross_height, style, out net_width, out net_height, out net_height_max, null);
    }

    public int CalcolateNetDimension(int gross_width, int gross_height,ItemStyle style,out int net_width,out int net_height,out int net_height_max,ItemGeometry geometry){
		int width=0, height=0,height_max=0,gross_height_max=0;

       

		net_width=width;
		net_height = height;
		net_height_max = height_max;

		for (int i=0;i<misure.Count;i++){

			ItemMisure item = (ItemMisure)misure [i];
			if(item.min_width<=gross_width && item.max_width>=gross_width){
				

				width=(int)(gross_width-2*item.width_frame*item.factor-item.width_central_frame-item.area_lateral);
	
				height_max=(int)(gross_height-item.height_top_frame-item.height_bottom_frame-item.area_bottom-item.area_top);

                if (geometry.id != null)
                    if (geometry.showBottomBar)
                        height_max = height_max - item.height_top_frame; //sottrai alla dimensione massima anche l'altezza della traversa della cimasa (se presente)

                    
                //se la tipologia comprende la cimasa, calcola la dimensione dell'altezza complessiva

				if(gross_width<=2600)
					height = height_max - (int)style.generator.y;

				if(gross_width>2600 && gross_width<=5200)
                    height = height_max - (int)style.generator.y*2;

				if(gross_width>5200)
                    height = height_max - (int)style.generator.y*3;



				

				net_width=width;
				net_height = height;
				net_height_max = height_max;
                gross_height_max = net_height_max + item.height_top_frame + item.height_bottom_frame + item.area_bottom + item.area_top;
                if (geometry.id != null)
                    if (geometry.showBottomBar)
                        gross_height_max = gross_height_max + item.height_top_frame;
                
				continue;
				}
			}

        //attribuisci all'altezza lorda un valore

        return gross_height_max;

	}


	public ArrayList getStandardWidth(int mindim){
		int min = 800;
		int start;
		ArrayList widths=new ArrayList();
		for (int i = 0; i < misure.Count; i++) {

			ItemMisure item = (ItemMisure)misure [i];
			if (item.min_width > min)
				min = item.min_width;

			if(mindim<min)
				start=(int)Mathf.Round(min/mindim)*mindim;
			else
				start=mindim;	

			Debug.Log (start + " " + item.max_width+" "+mindim);

			for(int j=start;j<item.max_width;j=j+mindim){
				int w=j+2*item.width_frame+item.area_lateral+item.width_central_frame;
				//verifica che nel caso di battente il numero di celle sia pari
				if(item.width_central_frame>0){
					if((j/mindim)%2==0){
						widths.Add(w);
					}
				}else
					widths.Add(w);
			}
		}

		return widths;

	}


	public ArrayList getStandardHeight(int mindim,int width){



		int max=3000;
		int min=600;
		int start=0;
		ArrayList heights=new ArrayList();




		for (int i = 0; i < misure.Count; i++) {

			ItemMisure item = (ItemMisure)misure [i];

			//per ogni riga calcola i valori
			if(width>=item.min_width && width<=item.max_width){

				if(mindim<min)
					start=(int)Mathf.Round(min/mindim)*mindim;
				else
					start=mindim;	


				for (int j=start;j<max;j=j+mindim){

					int a = j + item.height_top_frame + item.height_bottom_frame + item.area_bottom + item.area_top;

					heights.Add(a);
				}
			}


		}

		return heights;
	}





}
