﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour {

	public Vector3 drawCoordinate;
	public Item item;
	public bool is_specular;



	void OnMouseOver ()
	{
		if (Ctrl.inDeleteMode) {
			ControlsDesign l = GameObject.FindObjectOfType<ControlsDesign>();
			// Rimuovo l'oggetto 
			if (Input.GetMouseButtonDown (0)) {
				l.currentActive.deleteElement (this.transform.gameObject);
				removeSpecular ();
			}
		}
	}


	void removeSpecular(){
		ControlsDesign l = GameObject.FindObjectOfType<ControlsDesign>();
		if (l.cg.gate.misure.width_central_frame>0 && l.addspecularcell)
			l.currentActive.deleteElement (l.searchElement (drawCoordinate, true));

	}

	void Update(){
		if (Ctrl.inDeleteMode) {
			//se usi il touch e se in modalità di cancellazione elimina la cella dall'elenco
			if (Input.touchCount == 1) {
				Touch touch = Input.GetTouch (0);
				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					if (hit.transform.GetInstanceID() == this.transform.GetInstanceID() && touch.phase == TouchPhase.Began) {
						ControlsDesign l = GameObject.FindObjectOfType<ControlsDesign>();
						l.currentActive.deleteElement (this.transform.gameObject);
						removeSpecular ();
					}
				}
			}
		}

	}
}
