﻿using UnityEngine;


/**
 * Parametri caratterizzanti un elemento inseribile nella griglia
 */
public class Item 
{
		public int id; // Identificativo dell'item
		public int type; // Tabella categoria
		public Vector3 dimension; // Dimensioni
		public double price; // Prezzo 
		public double weight; // Peso
		public string name; // Nome
		public string iconPath; // Path che permette di caricare la texture dell'icona 
		public int numPuntiSaldatura; // Numero punti di saldatura
		public int style; // Stile di appartenza
		public Object obj; // Oggetto 3D
		public string obj_path; //path per lo scaricamento dell'OBJ
		public Vector3 rotate; // Rotazioni applicate
		public ItemColor color;
		public Vector2 cells;
		public float scale_x;
		public float scale_y;


		public static Item Clone(Item fromClone){
			Item item = new Item ();
			item.id = fromClone.id;
			item.type = fromClone.type;
			item.dimension = fromClone.dimension;
			item.price = fromClone.price;
			item.weight = fromClone.weight;
			item.name = fromClone.name;
			item.iconPath = fromClone.iconPath;
			item.numPuntiSaldatura = fromClone.numPuntiSaldatura;
			item.style = fromClone.style;
			item.obj = fromClone.obj;
			item.obj_path = fromClone.obj_path;
			item.rotate = fromClone.rotate;
			
			item.color = fromClone.color;
			item.cells = fromClone.cells;
			item.scale_x = fromClone.scale_x;
			item.scale_y = fromClone.scale_y;

			return item;
		}

		
}


