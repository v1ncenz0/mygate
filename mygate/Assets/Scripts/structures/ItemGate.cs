﻿using UnityEngine;
using System;
using System.Collections;
using SimpleJSON;

public class ItemGate : MonoBehaviour {

	public int id; // Identificativo dell'item
	public string name; // Nome
	public Sprite image; //Texture del colore (se presente)
	public string image_path; //path remoto della texture
	public string date;
	public int id_type;
	public int id_style;
	public int id_geometry;
	public int width; //larghezza della sola griglia
	public int height;//altezza della sola griglia
	public int heightmax;//altezza comprensiva la cimasa
    public int width_gross;
    public int height_gross;
    public string opening;


	public JSONArray configuration;
	public string json;


	public ItemType type;
	public ItemStyle style;
	public ItemGeometry geometry;

	public ItemMisure misure;

	public Vector3 gateDim;
	public Vector3 gateMaxDim;

	public Vector3 centralPoint;

	public ItemColor mat_frame;

	[Header("Dati delle due colonne")]
	public float columnWidth;
	public float columnDepth;
	public float capitalWidth;
	public float capitalHeight;
	public float capitalDepth;

	public float columnWidthL;
	public float columnDepthL;
	public float capitalWidthL;
	public float capitalHeightL;
	public float capitalDepthL;

	public float columnWidthR;
	public float columnDepthR;
	public float capitalWidthR;
	public float capitalHeightR;
	public float capitalDepthR;



	public IEnumerator downloadImage(Action<bool> finish){
		
		if(this.image_path.ToString()!="null" && this.image_path.ToString()!=""){
			WWW www = new WWW (System.Uri.EscapeUriString(StaticVariables.BASE_URL +this.image_path));

			yield return www;

			if (www.texture)
				this.image =  Sprite.Create((Texture2D)www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0.5f,0.5f),10);

		}
		finish (true);
	}


	public void loadFromLibrary(){
		this.type = Library.searchType (id_type);
		this.style = Library.searchStyle (id_style);
		this.geometry = Library.searchGeometry (id_geometry);
	}

	public void updateOtherParams(){
		
		loadFromLibrary ();

		this.misure = this.type.getMisure (this.width);

		if (this.heightmax < this.height)
			this.heightmax = this.height;

		if(!isGeometry()) //nel caso non è presente la cimasa
			this.heightmax = this.height;


		if (width > 0 && height > 0) {
			gateDim = new Vector3 (width + misure.width_central_frame, height, 0);
			gateMaxDim = new Vector3 (width + misure.width_central_frame, heightmax, 0);
			centralPoint = new Vector3 (gateDim.x / 2, gateDim.y, gateDim.z);
		}


        //Calcolo il numero di celle che entrano nella luce del cancello (approssimato)

        int elements_Y = (int)Math.Floor(gateDim.y / this.style.generator.y);
        float new_y = gateDim.y / elements_Y;

        //se presente un battente centrale effettua il calcolo solo su una anta  (per gli elementi X)
        int elements_X = 0;
        float new_x = 0;
        int width_central = this.misure.width_central_frame;
        if (width_central > 0)
        {
            elements_X = (int)Mathf.Ceil((gateDim.x/2 - this.misure.width_central_frame /2)/ this.style.generator.x);
            new_x = ((gateDim.x-this.misure.width_central_frame)/2) / elements_X;
        } else {
            elements_X = (int)Mathf.Ceil(gateDim.x / this.style.generator.x);
            new_x = gateDim.x / elements_X;
        }

       	
		//calcolo la scala di distorsione

		float scale_generator_x = new_x / this.style.generator.x;
		float scale_generator_y = new_y / this.style.generator.y;


		//applico le nuove dimensioni del generator
		this.style.generator.x = new_x;
		this.style.generator.y = new_y;

		//imposta la libreria di items
		this.style.fixDimensionElements(scale_generator_x,scale_generator_y);



	}


	public Vector2 calcolateDimensionGross(){
		Vector2 dimension = new Vector2 (gateDim.x + misure.area_lateral, gateDim.y + misure.area_bottom + misure.area_top);
		return dimension;
	}


	public bool isGeometry(){
		try{
			if(this.geometry.id>0)
				return this.geometry.isPresent ();
		}catch{
			return false;
		}
		return false;
	}












}
