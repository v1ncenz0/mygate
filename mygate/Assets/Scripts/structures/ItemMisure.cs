﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMisure : MonoBehaviour {

	public int id;
	public int min_width;
	public int max_width;
	public int area_lateral;
	public int area_bottom;
	public int area_top;
	public int width_frame;
	public int width_central_frame;
	public int height_top_frame;
	public int height_bottom_frame;
	public float factor;

}
