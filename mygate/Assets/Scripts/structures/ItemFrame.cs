﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFrame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnMouseDown()
	{
        ArrayList parameters = new ArrayList();


        parameters.Add(ItemParam.createParam("width_central", "Larghezza battente centrale", "", ItemParam.INPUTFIELD, GameObject.Find("Gate/Frame/center_right").transform.localScale.x.ToString(), ""));
        parameters.Add(ItemParam.createParam("Button", "Cancella", "", ItemParam.BUTTON, "delete", ""));

        GUIManager.OpenInspector(parameters, "Camera", "GridManager", "updateFrame");
	}

}
