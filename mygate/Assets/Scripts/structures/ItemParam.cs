﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemParam : MonoBehaviour {

    public string name;
    public string label;
    public string icon;
    public int type;
    public string value;
    public string defaultvalue;

    public const int INPUTFIELD = 0;
    public const int DROPDOWN = 1;
    public const int CHECKBOX = 2;
    public const int DIVIDER = 3;
    public const int SWITCH = 4;
    public const int BUTTON = 5;

    public const string TOGGLE_ON = "True";
    public const string TOGGLE_OFF = "False";


    /// <summary>
    /// Trova l'ItemParam dall'interno di una lista di parametri
    /// </summary>
    /// <returns>The parameter.</returns>
    /// <param name="paramlist">Paramlist.</param>
    public static ItemParam GetParam(ArrayList paramlist,string paramname){

        foreach(ItemParam p in paramlist){
            if (p.name == paramname)
                return p;
        }
        return null;
    }


    public static ItemParam createParam(string name,string label, string icon, int type, string value, string defaultvalue){

        ItemParam param = new ItemParam();
        param.name = name;
        param.label = label;
        param.type = type;
        param.icon = icon;
        param.value = value;
        param.defaultvalue = defaultvalue;
        return param;

    }

}
