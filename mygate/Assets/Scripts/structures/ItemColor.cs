﻿using UnityEngine;
using System;
using System.Collections;
/**
 * Parametri caratterizzanti un elemento inseribile nella griglia
 */
public class ItemColor: MonoBehaviour 
{
		public int id; // Identificativo dell'item
		public string name; // Nome
		public Color color; // Colore in #RRGGBB
		public Texture texture; //Texture del colore (se presente)
		public string texture_path; //path remoto della texture
		public string image_path; //path remoto della texture
		public bool selected;
		public Material material;
	

	public static string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}

	public static Color HexToColor(string hex)
	{
		int space = 0;
		//verifica se è presente il carattere #
		if (hex.Substring (0, 1) == "#")
				space = 1;

		byte r = byte.Parse(hex.Substring(space,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(space+2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(space+4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, 255);
	}


	public IEnumerator downloadTextureColor(Action<bool> finish){


		if(this.texture_path.ToString()!="null" && this.texture_path.ToString()!=""){
			WWW www = new WWW (StaticVariables.BASE_URL +this.texture_path);

			yield return www;

			if (www.texture)
				this.texture = www.texture;
			
		}
		finish (true);
	}


	public void setMaterial(){
		//material = Resources.Load ("defaultMat") as Material;

		material = new Material (Shader.Find ("Standard"));
		material.color = this.color;
		material.name = this.name;
		material.SetFloat("_Metallic", 1.0f);
		material.mainTexture = null;
		if(this.texture)
			if (this.texture.width > 10) {
				material.color = Color.white;
				material.mainTexture = this.texture;

			}
	}

}
