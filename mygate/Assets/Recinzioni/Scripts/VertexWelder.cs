﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexWelder {
	//Questo script calcola i vertici di due segmenti adiacenti e li salda tra loro. 
	//Si occupa inoltre della creazione e modifica dei segmenti.
	//Questo serve per evitare aperture tra due segmenti adiacenti che abbiano angolo diverso
	// Use this for initialization

	 MeshFilter cubeOne; //Il mesh filter del primo cubo
	 MeshFilter cubeTwo; //Il mesh filter del secondo cubo
	 Vector3 vertOffset;//IN base alla posizione del pivot, bisogna fare un offset per la posizione dei vertici.
	 float slopeAngleUp; //Pendenza del segmento
 	float slopeAngleDown; //Pendenza negativa del segmento (es. recinzione di una rampa del garage)
 	private Mesh meshOne;
	private Mesh meshTwo; //Le mesh che verranno manipolate 
	private Vector3 []verticesOne;
	private Vector3[] verticesTwo; //Queste matrici contengono le coordinate dei vertici di ciascuna mesh
	private Vector3 [] vertOneWorldPos; //Dove si trova la posizione di ciascun vertice in coordinate globali
	private Vector3[] originalMatrix; //memorizza la Posizione dei vertici nello stato inedito.
	private Mesh originalMesh;
	private bool comingFromApplyWindow=false; //nel doEditing, distingue tra quando premo applica nella finestra di editing, e quando invece modifico i nodi e basta

	/// <summary>
	/// Registra la matrice iniziale che identifica le posizioni locali dei vertici. Utile nel caso in cui bisogna resettare allo stato iniziale questi ultimi.
	/// </summary>
	public void Init(){
		GameObject originalObj = Resources.Load ("cubetto")as GameObject;
		originalMesh = originalObj.GetComponent<MeshFilter>().sharedMesh;
		originalMatrix = new Vector3[originalMesh.vertices.Length];
		originalMatrix = originalMesh.vertices;


	}

	/// <summary>
	/// Permette di modificare lunghezza e posizione di nodi già esistenti.
	/// </summary>
	/// <param name="SegmentoUno">Il segmento antecedente quello a cui appartiene il nodo</param>
	/// <param name="SegmentoDue">Il segmento a cui appartiene il nodo selezionato.</param>
	/// <param name="SegmentoZero">Se esiste, il segmento precedente al precedente.</param>">
	/// <param name="closeLoop">True se sto chiudendo  su un nodeFirst</param>
	public void doEditing(GameObject SegmentoUno, GameObject SegmentoDue, GameObject SegmentoZero,bool closeLoop){
		//Prima modifico la posizione di segmento 1 e 2

		resetSegmentToInitialState(SegmentoUno);
		if (SegmentoDue) {
			resetSegmentToInitialState (SegmentoDue);
			if(!closeLoop)
		reRotateSecondSegment (SegmentoDue, SegmentoUno);
		

			doWelding (SegmentoUno, SegmentoDue);
		}
	
		if (SegmentoZero != null) {
			resetNodeOneSegmentToInitialState (SegmentoZero);
			doWelding (SegmentoZero, SegmentoUno);
		}

		if (SegmentoZero && SegmentoZero.GetComponent<linkProperties> ().previousSegment) {
			resetNodeOneSegmentToInitialConnection (SegmentoZero, SegmentoZero.GetComponent<linkProperties> ().previousSegment);
		}

			if (SegmentoDue && SegmentoDue.GetComponent<linkProperties> ().attachedSegment) {
			float[] momProps = new float[8];
			if (SegmentoDue.GetComponent<linkProperties> ().attachedSegment.GetComponent<linkProperties>().thisSegmentProperties.Length == 8)
				momProps = SegmentoDue.GetComponent<linkProperties> ().attachedSegment.GetComponent<linkProperties>().thisSegmentProperties;
			doWelding (SegmentoDue, SegmentoDue.GetComponent<linkProperties> ().attachedSegment);
			if (SegmentoDue.GetComponent<linkProperties> ().attachedSegment.GetComponent<linkProperties>().thisSegmentProperties.Length == 8)
				
			ApplyNewProperties (SegmentoDue.GetComponent<linkProperties> ().attachedSegment, momProps);
 		}

		//A questo punto controllo se i segmenti in questione erano stati modificati in altezza, spessore ecc, e di conseguenza ne ricalcolo i parametri.

			if (SegmentoUno.GetComponent<linkProperties> ().thisSegmentProperties.Length == 8) {
				
				ApplyNewProperties (SegmentoUno, SegmentoUno.GetComponent<linkProperties> ().thisSegmentProperties);
			}

			if (SegmentoDue && SegmentoDue.GetComponent<linkProperties> ().thisSegmentProperties.Length == 8) {
				ApplyNewProperties (SegmentoDue, SegmentoDue.GetComponent<linkProperties> ().thisSegmentProperties);

			}
			if (SegmentoZero && SegmentoZero.GetComponent<linkProperties> ().thisSegmentProperties.Length == 8) {
				ApplyNewProperties (SegmentoZero, SegmentoZero.GetComponent<linkProperties> ().thisSegmentProperties);

			}


	
	}

	/// <summary>
	/// Quando si ha a che fare con la modifica di un segmento precedentemente creato, bisogna prima riportarlo allo stato indeformato.
	/// </summary>
	void resetSegmentToInitialState(GameObject segment){

		segment.GetComponent<MeshFilter>().mesh=originalMesh;
	}
	/// <summary>
	/// Resetta solo il nodo 1 del segmento allo stato iniziale;
	/// </summary>
	/// <param name="segment">Segment.</param>
	void resetNodeOneSegmentToInitialState(GameObject segment){
		Vector3[] EditedMatrix = new Vector3[originalMatrix.Length];
		for (int i = 0; i < originalMatrix.Length; i++) {				
			if (i != 0 || i != 1 || i != 10 || i != 11 || i != 12 || i != 13 || i != 14 || i != 15 || i != 17 || i != 21 || i != 22)
			//else
			EditedMatrix [i] = originalMatrix [i];

			segment.GetComponent<MeshFilter> ().mesh.vertices = EditedMatrix;
			segment.GetComponent<MeshFilter> ().mesh.RecalculateBounds ();
	}
	}
	/// <summary>
	/// Serve a ripristinare la connessione tra due segmenti a seguito del reset della mesh
	/// </summary>
	/// <param name="segment">Segment.</param>
	void resetNodeOneSegmentToInitialConnection(GameObject segmentZero, GameObject segmentPreZero){
		Vector3[] retrieveCorrectPos = new Vector3[originalMatrix.Length];
		Vector3[] worldCorrectPos = new Vector3[originalMatrix.Length];
		Vector3[] initialLocalPos = new Vector3[originalMatrix.Length];
		initialLocalPos = segmentZero.GetComponent<MeshFilter> ().mesh.vertices;
		for (int i = 0; i < originalMatrix.Length; i++) {
			retrieveCorrectPos=segmentPreZero.GetComponent<MeshFilter> ().mesh.vertices;
			worldCorrectPos[i] = segmentPreZero.transform.TransformPoint (retrieveCorrectPos [i]);
		}
		initialLocalPos [0] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [3]);
		initialLocalPos [15] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [3]);
		initialLocalPos [18] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [3]);

		initialLocalPos [11] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [7]);
		initialLocalPos [12] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [7]);
		initialLocalPos [17] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [7]);

		initialLocalPos [22] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [6]);
		initialLocalPos [13] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [6]);
		initialLocalPos [10] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [6]);

		initialLocalPos [1] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [2]);
		initialLocalPos [14] = segmentZero.transform.InverseTransformPoint(worldCorrectPos [2]);
		initialLocalPos [21] =segmentZero.transform.InverseTransformPoint(worldCorrectPos [2]);
		segmentZero.GetComponent<MeshFilter> ().mesh.vertices = initialLocalPos;
		segmentZero.GetComponent<MeshFilter> ().mesh.RecalculateBounds ();
		
	}
	///<summary> 
	/// L'editing di due segmenti esistenti prevede che la posizione venga ricalcolata. Lo faccio qui.
	/// </summary>
	void reRotateSecondSegment(GameObject segmentoDue,GameObject segmentoUno){
		GameObject Dot=segmentoDue.transform.GetChild (0).gameObject;
		//Vector3 finalPosition = Dot.transform.position;
		Vector3 finalPosition=new Vector3(Dot.transform.position.x,segmentoUno.transform.position.y,Dot.transform.position.z);
		//Vector3 startPosition = segmentoUno.transform.Find("node(Clone)").transform.position;
		Vector3 startPosition=new Vector3(segmentoUno.transform.Find("node(Clone)").transform.position.x,segmentoUno.transform.position.y,segmentoUno.transform.Find("node(Clone)").transform.position.z);
		float a=Vector3.Distance (startPosition, finalPosition);


		segmentoDue.transform.position = startPosition;
		segmentoDue.transform.localScale = new Vector3 (a, segmentoDue.transform.localScale.y, segmentoDue.transform.localScale.z);
		segmentoDue.transform.LookAt(finalPosition);
		segmentoDue.transform.eulerAngles=new Vector3(-90,segmentoDue.transform.eulerAngles.y+90,0);
		//Dot.transform.parent = segmentoDue.transform;
		Dot.transform.localEulerAngles = new Vector3 (0, Dot.transform.localEulerAngles.y, 0);
		Dot.transform.localScale = new Vector3 (1 / segmentoDue.transform.localScale.x, 1 / segmentoDue.transform.localScale.y, 1 / segmentoDue.transform.localScale.z);
	}





	/// <summary>
	/// Esegue il blending del segmento 1 col segmento 2,è possibile indicare le pendenze delle sezioni inferiori e superiori del segmento 1 in gradi.
	/// Se contigui, i vertici del segmento 2 nodo 1 vengono fatti coincidere coi vertici nodo 2 segmento 1.
	/// </summary>
	public void doWelding(GameObject SegmentoUno, GameObject SegmentoDue) {
		//Assegno i vertici alle relative matrici, dopo aver popolato le due variabili mesh
		meshOne=null;
		meshTwo = null;
		cubeOne = null;
		cubeTwo = null;
		verticesOne = null;
		verticesTwo = null;
		vertOffset= new Vector3(0,-0.5f,0);
		cubeOne=SegmentoUno.GetComponent<MeshFilter>();
		cubeTwo = SegmentoDue.GetComponent<MeshFilter> ();


		meshOne=cubeOne.mesh;
		meshOne.RecalculateBounds ();
		meshTwo = cubeTwo.mesh;
		verticesOne = new Vector3[meshOne.vertices.Length];
		vertOneWorldPos = new Vector3[meshOne.vertices.Length];
		verticesTwo = new Vector3[meshTwo.vertices.Length];
		verticesOne = meshOne.vertices;
		verticesTwo = meshTwo.vertices;
		for (int i = 0; i < verticesOne.Length; i++)
			vertOneWorldPos[i]=cubeOne.transform.TransformPoint(verticesOne[i]);

		
	

		//Individuazione dei vertici: N.B. in ogni "spigolo" convergono più vertici 
		//
		//                  A /-------------------------/|F                 z^  ^ y
		//                   /_______________________E_/ |                   | /
		//      Nodo 1    B |  D     Segmento         | /  G           x<-----/           (origine nel nodo 1) 
		//                  |_________________________|/ H    Nodo 2   
		//                C
		// Vertici in A=14,1,21 | Vertici in B=10,22,13| Vertici in C 11,12,17  | Vertici in D 0,15,8 | Vertici in E 6,9,23 | Vertici in F 2,5,20 | Vertici in G 3,4,16 | Vertici in H 7,8,19
		//La numerazione sopra riportata non è quella effettivamente utilizzata nel codice: viene infatti calcolata in codice in base alle localPosition di ciascun vertice
		//Per comodità posso far apparire un numero sui vertici per capire con quale ho a che fare.   <-- Metodo cancellato e non re-implementato al momento.

		//Noto l'angolo di inclinazione del segmento 2, bisogna ricalcolare l'angolo al nodo 2 del segmento 1, in modo da creare un raccordo a spessore costante, cosicchè lo spessore del segmento rimanga costante
		AdjustFirstNode(cubeOne.gameObject,cubeTwo.gameObject,verticesOne,verticesTwo);
		//Uso una prima volta WeldVertices: se i segmenti non sono contigui,non avrò necessità di far coincidere i vertici dopo aver messo in pendenza il segmento 1.

			//Se il segmento è in pendenza, ricalcolo l'altezza dei nodi E, F in figura, perchè si trovino ad un'altezza funzione di pendenza e altezza iniziale (quella tra i segmenti B e C)
			//ComputePositiveSlope (slopeAngleUp);
			//ComputeNegativeSlope (slopeAngleDown);
			//A questo punto è possibile congiurere il nodo del segmento 2 con il nodo del segmento 1
			WeldVertices (cubeTwo.gameObject,vertOneWorldPos,verticesOne,verticesTwo,meshOne,meshTwo);

			
	}
	/// <summary>
	/// Calcola la posizione dei vertici nodo 2 segmento 1 in base all'angolo tra il segmento 1 ed il segmento 2 
	/// </summary>
	void AdjustFirstNode(GameObject cubeOneS, GameObject cubeTwoS, Vector3[] verticesOneS, Vector3 []verticesTwoS){

		//Prendo l'angolo di inclinazione del segmento due e sottraggo 90 gradi per mettermi nell'orientazione della faccia laterale
		float angle=(cubeTwoS.transform.localEulerAngles.y-cubeOneS.transform.localEulerAngles.y)/2;
		//Prendo lo spessore del mio cubo.
		float side=cubeTwoS.transform.localScale.y;
		//Aggiusto la lunghezza dei segmenti esterni allungando. Aggiusto la lunghezza dei segmenti interni accorciando. La quantità di cui li aggiusto è l'addendum.
		float radAng=Mathf.Deg2Rad*angle; 
		float addendum= (side/2)*Mathf.Tan(radAng);
		//Ora calcolo la posizione in world coordinates dei vertici nodo 2 del segmento 1
		for (int i = 0; i < verticesOneS.Length; i++) {
			//Questi sono i vertici "interni alla curva", dove va quindi sommato il dedendum
				if (i==6 || i==7 || i==8 || i==9 || i==19 || i==23) { 
				Vector3 worldPos = cubeOne.transform.TransformPoint (verticesOneS [i]);
				worldPos = new Vector3 (worldPos.x - addendum * Mathf.Cos (cubeOneS.transform.rotation.eulerAngles.y * Mathf.Deg2Rad), worldPos.y, worldPos.z + addendum * Mathf.Sin (cubeOneS.transform.rotation.eulerAngles.y * Mathf.Deg2Rad));
				verticesOneS [i] = cubeOneS.transform.InverseTransformPoint (worldPos);
				vertOneWorldPos [i] = worldPos;
			} 
				else if (i==3 || i==4 || i==16 || i==2 || i==5 || i==20){

				Vector3 worldPos = cubeOneS.transform.TransformPoint (verticesOneS [i]);
				worldPos = new Vector3 (worldPos.x + addendum * Mathf.Cos (cubeOneS.transform.rotation.eulerAngles.y * Mathf.Deg2Rad), worldPos.y, worldPos.z - addendum * Mathf.Sin (cubeOneS.transform.rotation.eulerAngles.y * Mathf.Deg2Rad));
				verticesOneS [i] = cubeOneS.transform.InverseTransformPoint (worldPos);
				vertOneWorldPos [i] = worldPos;
				//ottenuta la nuova posizione in world coordinates, la applico al segmento uno
			} 

			}
		cubeOneS.GetComponent<MeshFilter>().mesh.vertices = verticesOneS;
		for (int i = 0; i < verticesOneS.Length; i++) {
			vertOneWorldPos [i] = cubeOneS.transform.TransformPoint (verticesOneS [i]);
		}

		cubeOneS.GetComponent<MeshFilter>().mesh.RecalculateBounds ();
		cubeOneS.GetComponent<MeshCollider> ().sharedMesh = cubeOneS.GetComponent<MeshFilter> ().mesh;
	

	}

	/// <summary>
	/// "Attacca" i vertici nodo 1 segmento 2 con i vertici nodo2 segmento1
	/// </summary>
	/// <param name="firstSegment">Segmento 2</param>
	/// <param name="segOneWorldPos">Matrice vertici segmento 1 in world position</param>
	/// <param name="segOneWorldPos">Matrice vertici segmento 1 in local position</param>
	/// <param name="segTwoLocalPos">Matrice vertici segmento 2 in local position.</param>
	void WeldVertices(GameObject secondSegment,Vector3[]segOneWorldPos,Vector3[] segOneLocalPos,Vector3[]segTwoLocalPos, Mesh meshSegOne, Mesh meshSegTwo){
		// I vertici del primo nodo del secondo segmento vengono fatti coincidere con i vertici del secondo nodo del primo segmento.
		// Non dimentichiamo che la posizione di ciascun vertice dipende anche dalla scala;

		segTwoLocalPos [0] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [3]);
		segTwoLocalPos [15] = secondSegment.transform.InverseTransformPoint (segOneWorldPos [3]);
		segTwoLocalPos [18] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [3]);

		segTwoLocalPos [11] = secondSegment.transform.InverseTransformPoint (segOneWorldPos [7]);
		segTwoLocalPos [12] = secondSegment.transform.InverseTransformPoint (segOneWorldPos [7]);
		segTwoLocalPos [17] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [7]);

		segTwoLocalPos [22] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [6]);
		segTwoLocalPos [13] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [6]);
		segTwoLocalPos [10] = secondSegment.transform.InverseTransformPoint (segOneWorldPos [6]);

		segTwoLocalPos [1] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [2]);
		segTwoLocalPos [14] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [2]);
		segTwoLocalPos [21] = secondSegment.transform.InverseTransformPoint (segOneWorldPos  [2]);
		for(int i=0;i<segOneLocalPos.Length;i++)
			segOneLocalPos[i]=cubeOne.transform.InverseTransformPoint (segOneWorldPos[i]);






		meshSegOne.vertices = segOneLocalPos;
		meshSegOne.RecalculateBounds ();
		meshSegTwo.vertices = segTwoLocalPos;
		meshSegTwo.RecalculateBounds ();
		secondSegment.GetComponent<MeshCollider> ().sharedMesh = meshSegTwo;
	
}
	/// <summary>
	/// Sistema i vertici superiori del nodo 2 segmento 1 in funzione dell'angolo pendenza 
	/// </summary>
	void ComputePositiveSlope(float pendenza){
		float angle = pendenza* Mathf.Deg2Rad;
		//Quindi noto angolo di pendenza e lunghezza del segmento, posso calcolare a quale altezza si troveranno i vertici del nodo 2, segmento 1
		float newHeight=cubeOne.transform.localScale.x*Mathf.Sin(angle);
		for (int i = 0; i < verticesOne.Length; i++) {
			//Prendo solo i vertici superiori al nodo 2
			if (verticesOne [i].z > 0.5f && verticesOne [i].x != 0f) {
				vertOneWorldPos [i] = new Vector3 (vertOneWorldPos [i].x, vertOneWorldPos [i].y + newHeight, vertOneWorldPos [i].z);
				verticesOne [i] = cubeOne.transform.InverseTransformPoint (vertOneWorldPos [i]);
			} 
		}
		meshOne.vertices = verticesOne;
		meshOne.RecalculateBounds ();
		for (int i = 0; i < verticesOne.Length; i++) {
			vertOneWorldPos [i] = cubeOne.transform.TransformPoint (verticesOne [i]);
		}

	}
	/// <summary>
	///  Sistema i vertici inferiori del nodo 2 segmento 1 in base all'angolo pendenza
	/// </summary>
	void ComputeNegativeSlope(float pendenza){
		float angle = pendenza* Mathf.Deg2Rad;
		//Quindi noto angolo di pendenza e lunghezza del segmento, posso calcolare a quale altezza si troveranno i vertici del nodo 2, segmento 1
		float newHeight=cubeOne.transform.localScale.x*Mathf.Sin(angle);
		for (int i = 0; i < verticesOne.Length; i++) {
			//Prendo solo i vertici inferiori al nodo 2
			if (verticesOne [i].z < 0.5f && verticesOne [i].x != 0f) {
				vertOneWorldPos [i] = new Vector3 (vertOneWorldPos [i].x, vertOneWorldPos [i].y + newHeight, vertOneWorldPos [i].z);
				verticesOne [i] = cubeOne.transform.InverseTransformPoint (vertOneWorldPos [i]);
			}
		}
		meshOne.vertices = verticesOne;
		for (int i = 0; i < verticesOne.Length; i++) {
			vertOneWorldPos [i] = cubeOne.transform.TransformPoint (verticesOne [i]);
		}
		meshOne.RecalculateBounds ();
	}

	/// <summary>
	/// Restituisce le grandezze legate allo specifico segmento
	/// </summary>
	/// <returns>Altezza,spessore, pendenza superiore, pendenza inferiore.</returns>
	/// <param name="segment">Segment.</param>
	public float[] QueryProperties(GameObject segment){
		float[] stats = new float[8];
		Mesh mToQuery = segment.GetComponent<MeshFilter> ().mesh;
		Vector3[] thisMatrix = new Vector3[mToQuery.vertices.Length];
		thisMatrix = mToQuery.vertices;
		float height = segment.transform.TransformPoint (thisMatrix [10]).y;
		float hfin=0;
		if (segment.GetComponent<linkProperties> ().previousSegment) {
			 hfin = segment.transform.TransformPoint(segment.GetComponent<linkProperties> ().previousSegment.GetComponent<MeshFilter> ().mesh.vertices [2]).y;
		}
		float length = segment.transform.localScale.x;
		float angle = segment.transform.eulerAngles.y;
		if (segment.GetComponent<linkProperties> ().previousSegment)
			angle = segment.transform.eulerAngles.y - segment.GetComponent<linkProperties> ().previousSegment.transform.eulerAngles.y;
		float angleTop = 0;
		float angleDown = 0;
		if (segment.GetComponent<linkProperties> ().thisSegmentProperties.Length == 8) {
			angleTop = segment.GetComponent<linkProperties> ().thisSegmentProperties [2];
		    angleDown = segment.GetComponent<linkProperties> ().thisSegmentProperties [3];
		} 
		stats [0] = height;
		stats [1] =   Mathf.Abs(((Vector3.zero) - (thisMatrix [0])).y)*ExperimCmd.defaultDepth  ;
		//stats [1] =segment.transform.localScale.y/2;
		stats [2] = angleTop;
		stats [3] = angleDown;
		stats [4] = segment.transform.localScale.y/2;
		stats [5] = length;
		stats [6] = angle;
		stats [7] = hfin;
		return stats;

	}
	public void setPropsWithEditing(GameObject segment,float[]properties){
		doEditing (segment, segment.GetComponent<linkProperties> ().attachedSegment, segment.GetComponent<linkProperties> ().previousSegment,false);
		//
		ApplyNewProperties (segment, properties);
	}
	/// <summary>
	/// Applica al segmento le nuove proprietà.
	/// </summary>
	/// <param name="segment">Segmento in editing.</param>
	/// <param name="properties">Array delle proprietà.</param>
	public void ApplyNewProperties(GameObject segment, float[]properties){
		comingFromApplyWindow = true;
		Mesh mToChange = segment.GetComponent<MeshFilter> ().mesh;			//ok
		Vector3[] thisMatrix = new Vector3[mToChange.vertices.Length];      //ok
		thisMatrix = mToChange.vertices;  									//ok
		float newLocalHeight = properties [0];
		Vector3[] worldMtx = new Vector3[thisMatrix.Length];
		for (int i = 0; i < worldMtx.Length; i++) 
			worldMtx[i] = segment.transform.TransformPoint (thisMatrix[i]);
		worldMtx=setHeight (worldMtx,properties[0]);
		worldMtx = setAngleSup (worldMtx, properties [2],properties[0]);
		worldMtx = setAngleInf (worldMtx, properties [3],properties[0]);
	 	setInnerThikness (segment,worldMtx, properties[1]);
	 	setOuterThikness (segment, worldMtx, properties [4]);
		for (int i = 0; i < worldMtx.Length; i++) {
			thisMatrix [i] = segment.transform.InverseTransformPoint (worldMtx [i]);
		}
		mToChange.vertices = thisMatrix;

		mToChange.RecalculateBounds ();
		segment.GetComponent<MeshCollider> ().sharedMesh = mToChange;
		//Inviamo le nuove caratteristiche al segmento. Che le conserverà per le successive modifiche.
		segment.GetComponent<linkProperties> ().OnUpdateProps (properties [0], properties [1], properties [2], properties [3], properties [4],properties[5],properties[6],properties[7]);




	}
	/// <summary>
	/// Cambia l'altezza del segmento
	/// </summary>
	Vector3[] setHeight(Vector3[] matriceVertici, float altezza){
		Vector3[] correctedMtx = new Vector3[matriceVertici.Length];
		for (int i = 0; i < matriceVertici.Length; i++) {
			if(i==2 || i==5 || i==20 || i==6 || i==9 || i==23 || i==14 || i==1 || i==21 || i==10 || i==22 || i==13)
			correctedMtx [i] = new Vector3 (matriceVertici [i].x, altezza, matriceVertici [i].z);
			else
				correctedMtx [i] = new Vector3 (matriceVertici [i].x, matriceVertici[i].y, matriceVertici [i].z);
				
		}
		return correctedMtx;
	}
	/// <summary>
	/// Applica la variazione angolare superiore della recensione.
	/// </summary>
	/// <returns>Il valore del vettore 3 locale</returns>
	/// <param name="matriceVertici">Matrice dei vertici del modello da variare.</param>
	/// <param name="angolo">Angolo di inclinazione.</param>
	Vector3[]setAngleSup(Vector3[]matriceVertici,float angolo,float altezza){
		Vector3[] correctedMtx = new Vector3[matriceVertici.Length];
		angolo = Mathf.Deg2Rad*angolo; 
		float lunghezza = Vector3.Distance (matriceVertici [14], matriceVertici[2]);
		float newEight = altezza + Mathf.Tan (angolo)*lunghezza;
		for (int i = 0; i < matriceVertici.Length; i++) {
			if(i==2 || i==5 || i==20 || i==6 || i==9 || i==23 )
				correctedMtx [i] = new Vector3 (matriceVertici [i].x, newEight, matriceVertici [i].z);
			else
				correctedMtx [i] = new Vector3 (matriceVertici [i].x, matriceVertici[i].y, matriceVertici [i].z);

		}
		return correctedMtx;
	}
	Vector3[] setAngleInf(Vector3[]matriceVertici,float angolo,float altezza){
		Vector3[] correctedMtx = new Vector3[matriceVertici.Length];
		angolo = Mathf.Deg2Rad*angolo; 
		float lunghezza = Vector3.Distance (matriceVertici [14], matriceVertici[2]);
		float newEight =  Mathf.Tan (angolo)*lunghezza;
		for (int i = 0; i < matriceVertici.Length; i++) {
			if(i==3 || i==4 || i==16 || i==7 || i==8 || i==19 )
				correctedMtx [i] = new Vector3 (matriceVertici [i].x,matriceVertici[i].y- newEight, matriceVertici [i].z);
			else
				correctedMtx [i] = new Vector3 (matriceVertici [i].x, matriceVertici[i].y, matriceVertici [i].z);

		}
		return correctedMtx;
	}
	void setOuterThikness(GameObject segmento,Vector3[]matriceVertici,float spessore){
		//Aggiustamento del nodo 2
		//pos è la posizione centrale al Nodo2
		Vector3 pos=new Vector3(segmento.transform.position.x+segmento.transform.localScale.x*Mathf.Cos(segmento.transform.eulerAngles.y*Mathf.Deg2Rad+180*Mathf.Deg2Rad),segmento.transform.position.y,segmento.transform.position.z+segmento.transform.localScale.x*Mathf.Sin(segmento.transform.eulerAngles.y*Mathf.Deg2Rad));
		//dir è la direzione di "smusso" al nodo 2
		Vector3 dir = ( matriceVertici[6]-matriceVertici[2]).normalized;
		Vector3 horizontal = segmento.transform.right;
		float angleForAddendumNodeTwo = Vector3.Angle (dir, horizontal); 
		float addendumNodeTwo = spessore / Mathf.Sin (angleForAddendumNodeTwo*Mathf.Deg2Rad);
		Vector3 correctedPos = pos + dir * addendumNodeTwo;
		for(int i=0;i<matriceVertici.Length ;i++)
			if(i==6 || i==7 || i==8 || i==9 || i==19 || i==23){
				matriceVertici[i]=new Vector3(correctedPos.x,matriceVertici[i].y,correctedPos.z);
			}
		Debug.DrawLine (pos, pos + dir * 1, Color.green, Mathf.Infinity);

		//Aggiustamento del nodo 1
		Vector3 nodeTwoDir=(matriceVertici[10]-matriceVertici[14]).normalized;
		float angleForAddendumNodeOne = Vector3.Angle (nodeTwoDir, horizontal);
		float addendumNodeOne = spessore/Mathf.Sin(angleForAddendumNodeOne*Mathf.Deg2Rad);
		Vector3 correctedNodeTwoPos=segmento.transform.localPosition+nodeTwoDir*addendumNodeOne;
		for (int i = 0; i < matriceVertici.Length; i++)
			if (i == 10 || i == 11 || i == 12 || i == 13 || i == 17 || i ==22) {
				matriceVertici [i] = new Vector3 (correctedNodeTwoPos.x, matriceVertici [i].y, correctedNodeTwoPos.z);
			}

	}
	void setInnerThikness(GameObject segmento,Vector3[]matriceVertici,float spessore){
		//Aggiustamento del nodo 2 
		//pos è il centro del nodo 2, visto superiormente (il punto 0 da cui poi sviluppo lo spessore")
		Vector3 pos=new Vector3(segmento.transform.position.x+segmento.transform.localScale.x*Mathf.Cos(segmento.transform.eulerAngles.y*Mathf.Deg2Rad+180*Mathf.Deg2Rad),segmento.transform.position.y,segmento.transform.position.z+segmento.transform.localScale.x*Mathf.Sin(segmento.transform.eulerAngles.y*Mathf.Deg2Rad));

		Vector3 dir = ( matriceVertici[2]-matriceVertici[6]).normalized;
		Vector3 horizontal = segmento.transform.right;
		float angleForAddendumNodeTwo = Vector3.Angle (dir, horizontal);
		float addendumNodeTwo = spessore / Mathf.Sin (angleForAddendumNodeTwo * Mathf.Deg2Rad);
		Vector3 correctedPos = pos + dir * addendumNodeTwo;

		for(int i=0;i<matriceVertici.Length ;i++)
			if(i==2 || i==5 || i==20 || i==3 || i==4 || i==16){
				matriceVertici[i]=new Vector3(correctedPos.x,matriceVertici[i].y,correctedPos.z);
			}
			Debug.DrawLine (pos, pos + dir * 1, Color.red, Mathf.Infinity);
		//Aggiustamento del nodo 1
		//qui vector3.pos è semplicemente rappresentato dalla localTransform.position
		Vector3 nodeOneDir=(matriceVertici[14]-matriceVertici[10]).normalized;
		float angleForAddendumNodeOne = Vector3.Angle (nodeOneDir, horizontal);
		float addendumNodeOne = spessore/Mathf.Sin(angleForAddendumNodeOne*Mathf.Deg2Rad);
		Vector3 correctedNodeOnePos=segmento.transform.localPosition+nodeOneDir*addendumNodeOne;

		for (int i = 0; i < matriceVertici.Length; i++)
			if (i == 14 || i == 1 || i == 21 || i == 0 || i == 15 || i == 8) {
				matriceVertici [i] = new Vector3 (correctedNodeOnePos.x, matriceVertici [i].y, correctedNodeOnePos.z);
			}


	
	}




	/// <summary>
	/// Cancella un segmento, edita i due attaccati per annullare gli smussi causati da esso.
	/// </summary>
	/// <param name="previous">Segmento Precedente.</param>
	/// <param name="next">Segmento Successivo.</param>
	/// <param name="thisOne">Segmento da cancellare.</param>
	public void onDeleteSegment(GameObject previous, GameObject next,GameObject thisOne ){
		if (previous != null) {
			previous.GetComponent<linkProperties> ().attachedSegment = null;
			previous.GetComponent<MeshFilter> ().mesh = originalMesh;
			if (previous.GetComponent<linkProperties> ().previousSegment != null) {
				//Attacco Nodo1 segmento 2 con Nodo2 segmento 1
				reweldPreviousAfterDelete(previous);
			}
		}

		if (next != null) {
			next.GetComponent<linkProperties> ().previousSegment = null;
			next.GetComponent<MeshFilter> ().mesh = originalMesh;
			if (next.GetComponent<linkProperties> ().attachedSegment != null) {
				doWelding (next, next.GetComponent<linkProperties> ().attachedSegment );
			}

			next.GetComponent<linkProperties> ().generateNodeCong ();
		}

		thisOne.GetComponent<linkProperties> ().DeleteSegment ();

	}
	/// <summary>
	/// Risalda i vertici tra i due segmenti precedenti quello cancellato
	/// </summary>
	/// <returns>WorldMatrix</returns>
	/// <param name="segment">Segmento</param>
	void reweldPreviousAfterDelete(GameObject segment){
		GameObject thisSegment = segment;
		GameObject previousSegment = segment.GetComponent<linkProperties> ().previousSegment;
		Vector3[] localMatrixPrevious = previousSegment.GetComponent<MeshFilter> ().mesh.vertices;
		Vector3[] worldMatrixPrevious=new Vector3[localMatrixPrevious.Length];
		for (int i = 0; i < worldMatrixPrevious.Length; i++) 
			worldMatrixPrevious [i] = previousSegment.transform.TransformPoint (localMatrixPrevious [i]);
		Vector3[] localMatrixThisOne = segment.GetComponent<MeshFilter> ().mesh.vertices;
		localMatrixThisOne [14] = segment.transform.InverseTransformPoint (worldMatrixPrevious [2]);
		localMatrixThisOne [1] = segment.transform.InverseTransformPoint (worldMatrixPrevious [2]);
		localMatrixThisOne [21] = segment.transform.InverseTransformPoint (worldMatrixPrevious [2]);

		localMatrixThisOne [0] = segment.transform.InverseTransformPoint (worldMatrixPrevious [3]);
		localMatrixThisOne [15] = segment.transform.InverseTransformPoint (worldMatrixPrevious [3]);
		localMatrixThisOne [8] = segment.transform.InverseTransformPoint (worldMatrixPrevious [3]);

		localMatrixThisOne [10] = segment.transform.InverseTransformPoint (worldMatrixPrevious [6]);
		localMatrixThisOne [22] = segment.transform.InverseTransformPoint (worldMatrixPrevious [6]);
		localMatrixThisOne [13] = segment.transform.InverseTransformPoint (worldMatrixPrevious [6]);

		localMatrixThisOne [11] = segment.transform.InverseTransformPoint (worldMatrixPrevious [7]);
		localMatrixThisOne [12] = segment.transform.InverseTransformPoint (worldMatrixPrevious [7]);
		localMatrixThisOne [17] = segment.transform.InverseTransformPoint (worldMatrixPrevious [7]);

		thisSegment.GetComponent<MeshFilter> ().mesh.vertices = localMatrixThisOne;
		thisSegment.GetComponent<MeshFilter> ().mesh.RecalculateBounds();
        }
	/// <summary>
	/// Trova tutti i figli di un dato parente, con un certo tag. Se reorder è true, li riordina in base al nome (es. 0-1-2...) 
	/// </summary>
	/// <returns>The all children with tag.</returns>
	/// <param name="tag">Tag.</param>
	/// <param name="reorder">If set to <c>true</c> reorder.</param>

	public GameObject[] FindAllChildrenWithTag(GameObject parent,string tag, bool reorder){
		List <GameObject> taggedChildren = new List<GameObject> ();
		for (int i = 0; i < parent.transform.childCount; i++) {
			if(parent.transform.GetChild(i).tag==tag)
				taggedChildren.Add(parent.transform.GetChild(i).gameObject);
		}
					return taggedChildren.ToArray();
	}

}





