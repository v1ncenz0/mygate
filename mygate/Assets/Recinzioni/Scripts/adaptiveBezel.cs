﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TriangleNet.Geometry;
using CatchCo;
public class adaptiveBezel : MonoBehaviour 
{
	TriangleNet.Mesh meshRepresentation;
	InputGeometry geometry;
	[Header("Variabili di progetto")]
	public float D;    //Distanza grata da parete
	public float cws;  //sporgenza cornicione superiore
	public float cwi;  //sporgenza cornicione inferiore 
	public float H;    //Altezza luce finestra
	public float L;   // Larghezza luce finestra
	public float pxl; //sporgenza lato sinsitro grata
	public float pxr; //sporgenza lato destro grata
	public float pys; //sporgenza superiore grata
	public float pyi;  //sporgenza inferiore grata
	private float thickness; 	//i cornicioni di marmo hanno spessore fisso
	[Header("Individuazione centro finestra")]
	public Vector3 diagonalPointOne;
	public Vector3 diagonalPointTwo;

	private Vector3 windowCenter;
	[Header("Dimensione finestra")]
	public Vector2 vertOne;
	public Vector2 vertTwo;
	public Vector2 vertThree;
	public Vector2 vertFour;
	public float distance = 5;
	public float verticalDistance = 2;
	public float boxDistance = 1f;
	public float circleDistance = 0.7f;

	//public UPolygon logoOutline;
	public UPolygon[] holes; 

	public float boxWidth = 1f;
	public float zOffset = 0.5f;
	public bool hasGrate;  //Se vi è associata una grata configurata, questa variabile è true
	public GameObject grateOb; // Se vi è assocuata una grata configruata la inserisco qui
	Mesh mesh;
	public bool setWin = false;
	/// <summary>
	/// CQuesto metodo crea la finestra, a partire dai dati inseriti
	/// </summary>
	[ExposeMethodInEditor]
	public void createWindowProcess () 
	{
		holes = new UPolygon[1];
		holes [0] = new UPolygon ();
		holes [0].points = new Vector2[4];

		holes [0].points [0] = new Vector2 (-L / 2, H / 2);
		holes [0].points [1] = new Vector2 (L / 2, H / 2);
		holes [0].points [2] = new Vector2 (L / 2, -H / 2);
		holes [0].points [3] = new Vector2 (-L / 2, -H / 2);
		geometry = new InputGeometry();
		windowCenter = (diagonalPointOne + diagonalPointTwo) / 2;
		transform.position = windowCenter;
		transform.eulerAngles = new Vector3 (0, -90, 0);
		//it is necessary to put a border around all the points in order to get triangulation to work correctly when holes are used
		List<Point> border = new List<Point>();
		border.Add(new Point(vertOne.x,vertOne.y));
		border.Add(new Point(vertTwo.x,vertTwo.y));
		border.Add(new Point(vertThree.x,vertThree.y));
		border.Add(new Point(vertFour.x,vertFour.y));
		geometry.AddRing(border);




		foreach(UPolygon hole in holes)
		{
			List<Point> holePoints = new List<Point>(hole.points.Length);

			foreach (Vector2 coordinates in hole.points)
				holePoints.Add(new Point(coordinates));

			geometry.AddRingAsHole(holePoints, 0);
		}

		meshRepresentation = new TriangleNet.Mesh();
		meshRepresentation.Triangulate(geometry);

		//generate mesh based on triangulation

		Dictionary<int, float> zOffsets = new Dictionary<int, float>();

	

		int triangleIndex = 0;
		List<Vector3> vertices = new List<Vector3>(meshRepresentation.triangles.Count * 3);
		List<int> triangleIndices = new List<int>(meshRepresentation.triangles.Count * 3);

		foreach(KeyValuePair<int, TriangleNet.Data.Triangle> pair in meshRepresentation.triangles)
		{
			TriangleNet.Data.Triangle triangle = pair.Value;

			TriangleNet.Data.Vertex vertex0 = triangle.GetVertex(0);
			TriangleNet.Data.Vertex vertex1 = triangle.GetVertex(1);
			TriangleNet.Data.Vertex vertex2 = triangle.GetVertex(2);

			Vector3 p0 = new Vector3( vertex0.x, vertex0.y,0);
			Vector3 p1 = new Vector3( vertex1.x, vertex1.y, 0);
			Vector3 p2 = new Vector3( vertex2.x, vertex2.y, 0);

			vertices.Add(p0);
			vertices.Add(p1);
			vertices.Add(p2);

			triangleIndices.Add(triangleIndex + 2);
			triangleIndices.Add(triangleIndex + 1);
			triangleIndices.Add(triangleIndex);

			triangleIndex += 3;
		}

		mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangleIndices.ToArray();
		mesh.RecalculateNormals();
		GetComponent<MeshFilter>().mesh = mesh;
		Extrude ();
	}

	void OnDrawGizmos()
	{
		if (holes.Length <= 0)
			return;

		Gizmos.color = Color.black;
		//logoOutline.GizmoDraw();

		Gizmos.color = Color.white;
		foreach(UPolygon hole in holes)
		{
			hole.GizmoDraw();
		}

		if (meshRepresentation == null)
			return;

		Gizmos.color = Color.cyan;

		foreach(KeyValuePair<int, TriangleNet.Data.Triangle> pair in meshRepresentation.triangles)
		{
			TriangleNet.Data.Triangle triangle = pair.Value;

			TriangleNet.Data.Vertex vertex0 = triangle.GetVertex(0);
			TriangleNet.Data.Vertex vertex1 = triangle.GetVertex(1);
			TriangleNet.Data.Vertex vertex2 = triangle.GetVertex(2);

			Vector2 p0 = new Vector2((float) vertex0.x, (float) vertex0.y);
			Vector2 p1 = new Vector2((float)vertex1.x, (float)vertex1.y);
			Vector2 p2 = new Vector2((float)vertex2.x, (float)vertex2.y);

			Gizmos.DrawLine(p0, p1);
			Gizmos.DrawLine(p1, p2);
			Gizmos.DrawLine(p2, p0);
		}
	}
	//Crea Le Pareti Del muro interne al foro finestrato
	void Extrude(){
		GameObject bottomWall = GameObject.CreatePrimitive (PrimitiveType.Quad);
		bottomWall.transform.position = new Vector3 (windowCenter.x-.15f, windowCenter.y - H/ 2, windowCenter.z);
		bottomWall.transform.localScale = new Vector3 (.3f, L, 1);
		bottomWall.transform.eulerAngles = new Vector3 (90, 0, 0);
		bottomWall.transform.parent = transform;
		bottomWall.name = "bottomWall";
		GameObject topWall = GameObject.CreatePrimitive (PrimitiveType.Quad);
		topWall.transform.position = new Vector3 (windowCenter.x-.15f, windowCenter.y + H/ 2, windowCenter.z);
		topWall.transform.localScale = new Vector3 (.3f, L, 1);
		topWall.transform.eulerAngles = new Vector3 (-90, 0, 0);
		topWall.transform.parent = transform;
		topWall.name = "topWall";
		GameObject leftWall = GameObject.CreatePrimitive (PrimitiveType.Quad);
		leftWall.transform.position = new Vector3 (windowCenter.x - .15f, windowCenter.y, windowCenter.z - L / 2);
		leftWall.transform.localScale = new Vector3 (.3f, H, 1);
		leftWall.transform.parent = transform;
		leftWall.transform.eulerAngles = new Vector3 (0, 180, 0);
		leftWall.name = "leftWall";
		GameObject rightWall = GameObject.CreatePrimitive (PrimitiveType.Quad);
		rightWall.transform.position= new Vector3 (windowCenter.x - .15f, windowCenter.y, windowCenter.z + L / 2);
		rightWall.transform.localScale = new Vector3 (.3f, H, 1);
		rightWall.transform.parent = transform;
		rightWall.name = "rightWall";
		GameObject window = GameObject.CreatePrimitive (PrimitiveType.Quad);
		window.transform.localScale = new Vector3 (L, H, 1);
		window.transform.position = new Vector3 (windowCenter.x - .3f, windowCenter.y, windowCenter.z);
		window.transform.eulerAngles = new Vector3 (0, -90, 0);
		window.transform.parent = transform;
		window.name = "window";
		GameObject bottomCornice = GameObject.CreatePrimitive (PrimitiveType.Cube);
		bottomCornice.transform.localScale = new Vector3 (.3f+cwi, .03f, L);
		bottomCornice.transform.position = new Vector3 (windowCenter.x - .15f + cwi / 2, windowCenter.y - H / 2+.015f, windowCenter.z);
		bottomCornice.transform.parent = transform;
		bottomCornice.name = "bottomCornice";
		GameObject topCornice = GameObject.CreatePrimitive (PrimitiveType.Cube);
		topCornice.transform.localScale = new Vector3 (.3f+cws, .03f, L);
		topCornice.transform.position = new Vector3 (windowCenter.x - .15f + cws / 2, windowCenter.y + H / 2-.015f, windowCenter.z);
		topCornice.transform.parent = transform;
		topCornice.name = "bottomCornice";
		GameObject leftCornice = GameObject.CreatePrimitive (PrimitiveType.Cube);
		leftCornice.transform.localScale = new Vector3 (.3f, .03f, H-0.06f);
		leftCornice.transform.position=new Vector3(windowCenter.x-.15f, windowCenter.y, windowCenter.z - L/2+.015f);
		leftCornice.transform.eulerAngles = new Vector3 (-90, 0, 0);
		leftCornice.transform.parent = transform;
		leftCornice.name = "leftCornice";

		GameObject rightCornice = GameObject.CreatePrimitive (PrimitiveType.Cube);
		rightCornice.transform.localScale = new Vector3 (.3f, .03f, H-0.06f);
		rightCornice.transform.position=new Vector3(windowCenter.x-.15f, windowCenter.y, windowCenter.z + L/2-.015f);
		rightCornice.transform.eulerAngles = new Vector3 (-90, 0, 0);
		rightCornice.transform.parent = transform;
		rightCornice.name = "rightCornice";
		makeGrateBox ();

	}
	void makeGrateBox(){
		GameObject grate = GameObject.CreatePrimitive (PrimitiveType.Cube);
		if (D > 0) {
			grate.transform.localScale =new Vector3( .02f,pyi+H+pys,pxl + L + pxr);

			grate.transform.position = new Vector3 (windowCenter.x + D, windowCenter.y + ((H + pyi + pys) / 2 - (H / 2 + pyi)), windowCenter.z - ((L + pxl + pxr) / 2 - (L / 2 + pxr)));

		} else {
			grate.transform.localScale =new Vector3( .02f,H,L);

			grate.transform.position = new Vector3 (windowCenter.x + D, windowCenter.y , windowCenter.z );
		}
		grate.GetComponent<MeshRenderer> ().material = Resources.Load ("fenceBox") as Material;
		grate.tag = "grate";
		grate.name = "GrateBox";
		grate.transform.parent = transform;
	
	}

}

