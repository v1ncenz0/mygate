﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class linkProperties : MonoBehaviour {
	[HideInInspector]
	public bool isOpening = false; //Se true, disattivo la visualizzazione di questo muro;
	public GameObject lookAtOb; //quando edito un segmento dalla coda invece che dalla testa (punto giallo) ruoto attorno a questo punto
	public GameObject previousSegment;
	public GameObject attachedSegment;
	public float[] thisSegmentProperties; //Quando un segmento viene editato, questo array viene popolato con le caratteristiche del pezzo
	public bool freezeMeasure;//Quando edito la misura direttamente dal testo, questa cosa deve farsi i cazzi suoi
	public bool freezeAngleMeasure;//Quando edito la misura angolare direttamente dal testo, questa cosa deve farsi i cazzi suoi
	public GameObject angleText;
	public Material transparentMat;
	private Material originalMat;
	private TextMesh angleTextMR;
	private GameObject angleLine;
	private LineRenderer angleLineMR;
	private GameObject lengthTextParent;
	public GameObject lengthText;
	private TextMesh lengthTextMR;
	private GameObject arrowSideOne;
	private GameObject arrowSideTwo;
	private GameObject backNode;
	private int segments=30;
	private float xradius = 1f;
	private float yradius=1f;
	private Color lerpedColor;
	private Color lerpedColorAngle;
	private bool FadeStarted=false;
	private bool hasGeneratedInitialNode=false;
	private GameObject firstNode;
	public int thickTyp=0; //Il tipo di spessore in uso. All'inizio è 0
	private MeshFilter myMf;
	private MeshCollider myMc;
	public GameObject gate;
	public bool instantiatedFromSaveFile=false;
	public GameObject columnR;
	public GameObject columnL;
	public bool isHandrail=false; //Se questa proprietà è true, il segmento è un corrimano, quindi sarà un po diversa la sua logica.
	public bool isBalcony=false; //Distinguiamo tra balcone e recinzione. Le due tipologie hanno logiche di salvataggio (e funzionamento) diverse.

	// Use this for initialization
	void Start () {
		//Se la variabile statica isBalcony è uguale a true allora siamo sicuri che questo segmento è un balcone

		if (!isHandrail) {
			if (StaticVariables.workMode == 1) {
				isBalcony = true;
			}
			myMf = GetComponent<MeshFilter> ();
			myMc = GetComponent<MeshCollider> ();
			originalMat = GetComponent<MeshRenderer> ().material;
			transparentMat = Resources.Load ("transparentSegment") as Material;

			ExperimCmd.segmentsTotal++;
			if (!instantiatedFromSaveFile)
				gameObject.name = "segment" + ExperimCmd.segmentsTotal.ToString ();
			angleText = new GameObject ();
			angleTextMR = angleText.AddComponent<TextMesh> ();
			angleTextMR.color = new Color (1, 142f / 255f, 0, 1);
			angleTextMR.alignment = TextAlignment.Center;
			angleTextMR.characterSize = 0.2f;
			angleText.transform.eulerAngles = new Vector3 (90, 0, 0);
			angleText.tag = "anglesMeasures";
			angleLine = new GameObject ();
			angleLineMR = angleLine.AddComponent<LineRenderer> ();
			angleLineMR.positionCount = segments + 1;
			angleLineMR.startColor = new Color (1, 142f / 255f, 0, 1);
			angleLineMR.endColor = new Color (1, 142f / 255f, 0, 1);
			angleLineMR.startWidth = .01f;
			angleLineMR.endWidth = .01f;
			angleLineMR.useWorldSpace = false;
			angleLine.transform.position = transform.position;
			angleLine.transform.localScale = new Vector3 (1, 1, 1);
			angleLine.transform.eulerAngles = new Vector3 (0, 0, 0);
			angleLineMR.material = Resources.Load ("AngleLine") as Material;
			angleLine.tag = "anglesLines";
			angleText.AddComponent<BoxCollider> ();
			angleText.GetComponent<BoxCollider> ().size = Vector3.one;

			lengthTextParent = new GameObject ();
			LineRenderer linerend = lengthTextParent.AddComponent<LineRenderer> ();
			linerend.startColor = Color.green;
			linerend.endColor = Color.green;
			linerend.startWidth = .01f;
			linerend.endWidth = .01f;
			linerend.SetPosition (0, new Vector3 (0, -1, 0));
			linerend.SetPosition (1, new Vector3 (-1, -1, 0));
			linerend.material = Resources.Load ("LineRendering") as Material;
			linerend.useWorldSpace = false;
			lengthTextParent.tag = "straightLinesMeasures";
			lengthText = new GameObject ();
			lengthText.transform.parent = lengthTextParent.transform;
			lengthText.tag = "straightMeasures";
			lengthText.transform.localPosition = new Vector3 (lengthTextParent.transform.localEulerAngles.x / 2, 1, 0);
			lengthTextMR = lengthText.AddComponent<TextMesh> ();
			lengthTextMR.alignment = TextAlignment.Center;
			lengthTextMR.anchor = TextAnchor.UpperCenter;
			lengthTextMR.color = Color.green;
			lengthTextMR.characterSize = 0.2f;
			lengthTextMR.gameObject.AddComponent<BoxCollider> ();
			lengthTextMR.gameObject.GetComponent<BoxCollider> ().size = Vector3.one;
			arrowSideOne = GameObject.CreatePrimitive (PrimitiveType.Quad);
			arrowSideTwo = GameObject.CreatePrimitive (PrimitiveType.Quad);
			arrowSideOne.GetComponent<MeshRenderer> ().material = Resources.Load ("Arrow") as Material;
			arrowSideTwo.GetComponent<MeshRenderer> ().material = Resources.Load ("Arrow") as Material;
			arrowSideOne.transform.localEulerAngles = new Vector3 (180, 0, 0);
			arrowSideTwo.transform.localEulerAngles = new Vector3 (180, 180, 0);
			arrowSideOne.transform.parent = lengthTextParent.transform;
			arrowSideTwo.transform.parent = lengthTextParent.transform;
			arrowSideOne.transform.localPosition = new Vector3 (0, -1, 0);
			arrowSideTwo.transform.localPosition = new Vector3 (-1, -1, 0);
			arrowSideTwo.tag = "straightMeasures";
			arrowSideOne.tag = "straightMeasures";
			lookAtOb = new GameObject ("rotationPoint" + ExperimCmd.segmentsTotal);

			angleLine.SetActive (false);
			generateNodeFirst ();

			hasGeneratedInitialNode = true;
		} else
			GetComponent<MeshRenderer> ().sharedMaterial = Resources.Load ("HandRailMat") as Material;
	}
	
	// Update is called once per frame
	void Update () {
		//Manteniamo i nodi all'altezza del segmento
		//firstNode.transform.localPosition=new Vector3(firstNode.transform.localPositio
		if(!isHandrail){
		myMc.sharedMesh = myMf.sharedMesh;

		if (Input.GetKeyDown(KeyCode.LeftShift)) {
			lookAtOb.transform.position = transform.Find ("node(Clone)").position;

		}
		if (isOpening) {
			GetComponent<MeshRenderer> ().material = transparentMat;
			if (gate != null)
				gate.SetActive (true);
		} else {
			if (gate != null) {
				gate.SetActive (false);
				Debug.Log ("CIDJIDIDHSISHIH");
			}
		}

		
		if (previousSegment) {
			angleLine.SetActive (true);
			if (hasGeneratedInitialNode) {
				Destroy (firstNode);
				hasGeneratedInitialNode = false;
			}

		}
		if (lengthTextParent.transform.localScale.x != 0) {
			arrowSideOne.transform.localScale = new Vector3 (.2f / lengthTextParent.transform.localScale.x, .2f, .2f);
			arrowSideTwo.transform.localScale = new Vector3 (.2f / lengthTextParent.transform.localScale.x, .2f, .2f);
		}

		angleText.transform.position =new Vector3( transform.position.x,transform.position.y,transform.position.z);
		lengthTextParent.transform.position = transform.position;
		lengthTextParent.transform.rotation = transform.rotation;
		lengthTextParent.transform.localScale = new Vector3(transform.localScale.x,1,1);
		lengthText.transform.localPosition=new Vector3(-0.5f,-1.3f,0);
		if (lengthTextParent.transform.localScale.x != 0 && !freezeMeasure) {
			lengthTextMR.text = string.Format ("{0:0.00}", lengthTextParent.transform.localScale.x);
			if (lengthTextParent.transform.eulerAngles.y > 90 && lengthTextParent.transform.eulerAngles.y < 270) {
					lengthText.transform.localScale = new Vector3 (-1 / lengthTextParent.transform.localScale.x, 1 / lengthTextParent.transform.localScale.y, 1 / lengthTextParent.transform.localScale.z);
			}
			else
			lengthText.transform.localScale = new Vector3 (1 / lengthTextParent.transform.localScale.x, -1 / lengthTextParent.transform.localScale.y, 1 / lengthTextParent.transform.localScale.z);
		}
	

		if (!freezeAngleMeasure) {
			if (previousSegment) {
				angleTextMR.text = string.Format ("{0:0.00}", transform.eulerAngles.y - previousSegment.transform.eulerAngles.y);
			} else {
				angleTextMR.text = string.Format ("{0:0.00}", transform.eulerAngles.y);
			}
		}
		if (previousSegment) {
			CreatePoints ();
				if ((transform.eulerAngles.y - previousSegment.transform.eulerAngles.y) < 0)
					angleLine.transform.eulerAngles = new Vector3 (0, previousSegment.transform.eulerAngles.y + 180, 0);
				else
					angleLine.transform.eulerAngles = new Vector3 (0, previousSegment.transform.eulerAngles.y, 0);

					angleLine.transform.eulerAngles = new Vector3 (0, previousSegment.transform.eulerAngles.y+180, 0);
	
			angleLine.transform.position=transform.position;
		}
		if (transform.Find ("node(Clone)")&& transform.localScale.x!=0) {
			transform.Find ("node(Clone)").localEulerAngles = new Vector3 (180, 0, 180);
			transform.Find ("node(Clone)").localScale = new Vector3 (1 / transform.localScale.x, 1 / transform.localScale.y, 1 / transform.localScale.z);
			if(firstNode)
				firstNode.transform.localScale=new Vector3 (1 / transform.localScale.x, 1 / transform.localScale.y, 1 / transform.localScale.z);
		}

	}
	}

	void CreatePoints ()
	{

		float x;
		float y;
		float z;
		Vector3 firstVertex=Vector3.zero;
		Vector3 lastVertex=Vector3.zero;
		float angle = 0f;

		for (int i = 0; i < (segments + 1); i++)
		{
			
			x = (Mathf.Sin (Mathf.Deg2Rad * angle) * xradius);
			z = (Mathf.Cos (Mathf.Deg2Rad * angle) * yradius);
			if(i==0)
				firstVertex=new Vector3(x,0,z);
			if(i==segments+1)
				lastVertex=new Vector3(x,0,z);
			angleLineMR.SetPosition (i,new Vector3(x,0,z) );


			angle+= (( transform.eulerAngles.y - previousSegment.transform.eulerAngles.y) / segments);

		}
		Vector3 dir=firstVertex-lastVertex;
		float OffsetAngle = Vector3.Angle (dir, Vector3.forward);
		Debug.DrawRay (firstVertex, dir);
	}
	/// <summary>
	/// quando si utilizza una finestra di modifica proprietà, le info vengono immagazzinate nel segmento corrispondente, per la successiva consultazione
	/// </summary>
	/// <param name="height">Altezza iniziale</param>
	/// <param name="thiknessA">Spessore lato 1</param>
	/// <param name="thiknessB">Spessore lato 2</param>
	/// <param name="topSlope">Pendenza superiore</param>
	/// <param name="botSlope">Pendenza inferiore</param>
	/// <param name="length">Lunghezza</param>
	/// <param name="gamma">AngoloInPianta</param>

	public void OnUpdateProps(float height,float thicknessA,float topSlope,float botSlope, float thicknessB,float length,float gamma,float feight){
		thisSegmentProperties = new float[8];
		thisSegmentProperties [0] = height;
		thisSegmentProperties [1] = thicknessA;
		thisSegmentProperties [2] = topSlope;
		thisSegmentProperties [3] = botSlope;
		thisSegmentProperties [4] = thicknessB;
		thisSegmentProperties [5] = length;
		thisSegmentProperties [6] = gamma;
		thisSegmentProperties [7] = feight;
		Debug.Log ("Il thickness salvato è " + thicknessA);
	}
	public void DeleteSegment(){
		Destroy (this.gameObject);
	}
	void OnDestroy(){
		Destroy(angleText);
		Destroy (lengthTextParent);
		Destroy (angleLine);
	}
	public void generateNodeCong(){
		if(!transform.Find("NodeCong")){
		GameObject nodeCong=Resources.Load ("NodeCong")as GameObject;
		GameObject instantiated=Instantiate (nodeCong, transform.position, transform.rotation);
		instantiated.transform.parent = transform;
		instantiated.transform.localScale = new Vector3 (1/transform.localScale.x, 1/transform.localScale.y, 1/transform.localScale.z);
		instantiated.transform.localEulerAngles=new Vector3(180,0,180);
			backNode = instantiated;
		}


		
	}
	public void generateNodeFirst(){
		if(!transform.Find("NodeFirst")){     //ERA NodeFirst
			GameObject nodeFirst=Resources.Load ("nodeFirst") as GameObject;
			GameObject instantiated=Instantiate (nodeFirst, transform.position, transform.rotation);
			instantiated.transform.parent = transform;
			if(transform.localScale.x!=0)
			instantiated.transform.localScale = new Vector3 (1/transform.localScale.x, 1/transform.localScale.y, 1/transform.localScale.z);
			instantiated.transform.localEulerAngles=new Vector3(180,0,180);
			firstNode = instantiated;
		}


	}

	Coroutine fadingcourotine;


	public void startFade(bool forAngle){
		
		if (!FadeStarted) {
			if (!forAngle) {
				fadingcourotine = StartCoroutine (Fading ());
				FadeStarted = true;
			} else {
				fadingcourotine = StartCoroutine (FadingAngle ());
				FadeStarted = true;
			}
		}
	}
	public void stopFade(){
		StopCoroutine (fadingcourotine);
		Debug.Log ("Proviamo");
		lengthTextMR.color = Color.green;
		FadeStarted = false;
	}
	IEnumerator Fading ()
	{
		Debug.Log ("QuanteVolte");
		while(true) {
			lerpedColor = Color.Lerp (Color.white, Color.green, Time.time);
			lengthTextMR.color = lerpedColor;
			yield return new WaitForSeconds (.3f);
			lerpedColor = Color.Lerp (Color.green, Color.white, Time.time);
			lengthTextMR.color = lerpedColor;
			yield return new  WaitForSeconds (.3f);
		}

	}
	IEnumerator FadingAngle(){

		while(true) {
			lerpedColorAngle = Color.Lerp (Color.white, new Color (1, 142f/255f, 0, 1), Time.time);
			angleTextMR.color = lerpedColorAngle;
			yield return new WaitForSeconds (.3f);
			lerpedColorAngle = Color.Lerp (new Color (1, 142f/255f, 0, 1), Color.white, Time.time);
			angleTextMR.color = lerpedColorAngle;
			yield return new  WaitForSeconds (.3f);
		}
	

	}
}
