﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Crosstales;
using Crosstales.FB;
using UnityEngine.SceneManagement;
using UnityEditor;


public class SaveLoad : MonoBehaviour {
	private GameObject ortoCam;
	private GameObject provvFenceBox;
	public GameObject fenceAlert;
	public GameObject edit_new_win;
	public ExperimCmd Ecmd;
	public stakesHandling stkhand;
	public static string segmentOfGate;
	public static float gateLength;
	public static float gateHeight;
	public bool shouldSave=false;
	public bool shouldLoad=false;
	private string gameDataFileName = "data.json";
	private JSonFile loadedData;
	[System.Serializable]
	public class JSonFile
	{
		public string version;
		public string author;
		public string creation;
		public string lastEdited;
		public recinzioni recin;
		public cancelli canc;
		public interRecinzioni intR;
		public balconi balc;
		public grate gra;


	}
	[System.Serializable]
	public class cancelli{
		public string type = "Cancelli";
		public gateProperties[] gP;
	}

	[System.Serializable]
	public class recinzioni{
		public string type="Recinzioni";
		public segmentStuff[] sS;

	}
	[System.Serializable]
	public class balconi{
		public string type="Balconi";
		public segmentStuff[] sS;



	}
	[System.Serializable]
	public class grate{
		public string type="Grate";
		public grateStuff[] gS;
	}
	[System.Serializable]
	public class proprietaGrate{
		public float D;
		public float cws;
		public float cwi;
		public float H;
		public float L;
		public float pxl;
		public float pxr;
		public float pys;
		public float pyi;
		public string grateName;
	}

	[System.Serializable]
	public class grateStuff{
		public proprietaGrate pG;
	}
	[System.Serializable]
	public class proprietaPaletti{
		public string type = "Proprietà paletto";
		public corrimano handrail;

		public bool exists = false;
		public Vector3 piastraCollegamentoPos;
		public Vector3 piastraCollegamentoRot;
		public Vector3 piastrCollegamentoScale;
		public Vector3 linkCollegamentoPos;
		public Vector3 linkCollegamentoRot;
		public Vector3 linkCollegamentoScale;

	}
	[System.Serializable]
	public class palettiCollegamento{
		public string type = "Paletti di collegamento";
		public Vector3 pos;
		public Vector3 rot;
		public Vector3 scal;
	}
	[System.Serializable]
	public class piastrineCollegamento{
		public string type = "Piastrine di collegamento";
		public Vector3 pos;
		public Vector3 rot;
		public Vector3 scal;

	}
	[System.Serializable]
	public class corrimano{
		public string type="Corrimano";
		public bool exists;
		public float height;
		public float width;
	}

	[System.Serializable]
		public class interRecinzioni{
		public string type="interRecinzioni";
		public freeFences[] fF;
	}
	[System.Serializable]
  		public class freeFences{
		public string fenceName;
		public Vector3 fencePosition;
		public Vector3 fenceRotation;
		public Vector3 fenceScale;
		public bool hasAFence;
		public string firstStack;
		public string secondStack;
		public string firstSegment;
		public string secondSegment;
		public string myFence;
		public Vector3 fenceObjPos;
		public Vector3 fenceObjRot;
		public Vector3 fenceObjScale;
		public gateScript gS;
	}
	[System.Serializable]
	public class colonna{
		public string type = "Colonna";
		public columnProperties cP;
	}
	[System.Serializable]
	public class segmentStuff{
		public string segName;
		public meshMatrix segMesh=new meshMatrix();
		public ObjPosition segPosition=new ObjPosition();
		public ObjRotation segRotation=new ObjRotation();
		public ObjScale segScale=new ObjScale();
		public string lookAtOb; //manca
		public string prevSeg;  //manca
		public string AttachedSeg;//manca
		public segmentProperties SP=new segmentProperties();
		public segmentChildren[] SC;//la scala fa schifo al cazzo
		public colonna columndx;
		public colonna columnsx;
		public paletti[] stakesHere;
		public boxRinghiere[] fbox;


	}
	[System.Serializable]
	public class boxRinghiere {
		public string name;
		public Vector3 pos;
		public Vector3 localRot;
		public Vector3 localScale;
		public string firstPole;
		public string secondPole;
		public bool hasFence;
		
	}
	[System.Serializable]
	public class paletti{
		public string type = "Paletti";
		public stakesProps stakProp;
	}

	[System.Serializable]
	public class stakesProps{
		public string stakeName;
		public ObjPosition stakePos;
		public ObjRotation stakeRot;
		public ObjScale stakeScale;
		public bool hasFence;
		public Fence thisFence;
		public proprietaPaletti pP;
	}
	[System.Serializable]
	public class Fence{
		public string fenceName;
		public Vector3 fenceAngles;
		public Vector3 fencePosition;
		public Vector3 fenceScale;
		public Quaternion fenceRealRot;
		public gateProperties fenceProps;
	}
	[System.Serializable]
	public class segmentProperties{
		public bool isOpening;
		public float H;
		public float L;
		public float W;
		public int thickType;
		public float alfa;
		public float beta;
		public float gamma;
		public float HPrec;
		public string associatedGate;

	}
	[System.Serializable]
	public class gateProperties{
		public string gateName;
		public ObjPosition gatePos;
		public ObjRotation gateRot;
		public ObjScale gateScale;
		public string filename;
		public gateScript gS;
	}
	[System.Serializable]
	public class gateScript{
		public int id;
		public string name;
		public string image_path;
		public string date;
		public int id_type;
		public int id_style;
		public int id_geometry;
		public int width;
		public int height;
		public int heightMax;
		public string opening;
		public string json;
		public ItemType it;

		public float colWidthL;
		public float colDepthL;
		public float capWidthL;
		public float capDepthL;
		public float capHeightL;

		public float colWidthR;
		public float colDepthR;
		public float capWidthR;
		public float capDepthR;
		public float capHeightR;
	

	}
	[System.Serializable]
	public class columnProperties{
		public string belongingSegment;
		public ObjPosition pos;
		public ObjRotation rot;
		public ObjScale sca;
		public ObjPosition capitalLocalPos;
		public ObjScale capitalScale;
	
	}
	[System.Serializable]
	public class meshMatrix{
		public Vector3[] vertices;
	
	}
	[System.Serializable]
	public class ObjPosition{
		public Vector3 value;
	}
	[System.Serializable]
	public class ObjRotation{
		public Vector3 value;
	}
	[System.Serializable]
	public class ObjScale{
		public Vector3 value;
	}
	[System.Serializable]
	public class segmentChildren{
		public ObjPosition nodePos=new ObjPosition();
		public ObjScale nodeScale=new ObjScale();
		public string nodeName;

		
	}
	private static int debugInt = 0;
	private bool skipAFrame = false; //Nel caricare la scena dei cancelli, ho necessità di far passare un frame per distruggere il cancello da modificare.


	/// <summary>
	/// Ogni volta che un segmento ha una recinzione collegata,viene chiamato questo script/// </summary>
	/// <param name="filePath">File path.</param>
	private void instantiateSavedFences(string filePath, stakesProps stProps, GameObject relativeStake ){
		
		string dir = Path.GetDirectoryName (filePath);
		GameObject fence = new GameObject ();
		fence.name = stProps.thisFence.fenceName;
		fence.transform.parent = relativeStake.transform;
		//fence.transform.localEulerAngles = stProps.thisFence.fenceAngles;
		fence.transform.localPosition = stProps.thisFence.fencePosition;
		fence.transform.position = stProps.thisFence.fenceScale;
		fence.transform.localEulerAngles  = Vector3.zero;
		int id = stProps.thisFence.fenceProps.gS.id;
		string sname = stProps.thisFence.fenceName;
		string image_path=stProps.thisFence.fenceProps.gS.image_path;
		string date=stProps.thisFence.fenceProps.gS.date;
		int id_type=stProps.thisFence.fenceProps.gS.id_type;
		int id_style=stProps.thisFence.fenceProps.gS.id_style;
		int id_geometry=stProps.thisFence.fenceProps.gS.id_geometry;
		int width=stProps.thisFence.fenceProps.gS.width;
		int height=stProps.thisFence.fenceProps.gS.height;
		int heightmax =stProps.thisFence.fenceProps.gS.heightMax ;
		string opening =stProps.thisFence.fenceProps.gS.opening ;
		string json=stProps.thisFence.fenceProps.gS.json;
		string filename =stProps.thisFence.fenceName;


		OBJ obj = fence.AddComponent<OBJ> ();
		obj.loadMaterial = true;
		obj.objPath =dir+"/"+filename+".obj";
		obj.StartLoadCoroutine ((value) => {

			Destroy (fence.GetComponent<OBJ> ());
			for(int q=0;q<fence.transform.childCount;q++){
				fence.transform.GetChild(q).localPosition=Vector3.zero;
				fence.transform.GetChild(q).localEulerAngles =Vector3.zero;
				fence.transform.localPosition=stProps.thisFence.fencePosition ;
				fence.transform.localEulerAngles=stProps.thisFence.fenceAngles;
				if(stProps.thisFence.fenceAngles.z!=0)
				fence.transform.localEulerAngles=new Vector3(stProps.thisFence.fenceAngles.x, 0,stProps.thisFence.fenceAngles.z);
				else
				fence.transform.localEulerAngles=new Vector3(stProps.thisFence.fenceAngles.x,180,stProps.thisFence.fenceAngles.z);
				//fence.transform.rotation=stProps.thisFence.fenceRealRot;
			//	fence.transform.localEulerAngles=new Vector3(stProps.thisFence.fenceAngles.x,stProps.thisFence.fenceAngles.y,stProps.thisFence.fenceAngles.z);


			}
			//GameObject.Find(seg).GetComponent<linkProperties>().gate=gate;
			fence.AddComponent<ItemGate>();
			fence.GetComponent<ItemGate>().id=id ;
			fence.GetComponent<ItemGate>().name=sname;
			fence.GetComponent<ItemGate>().image_path= image_path;
			fence.GetComponent<ItemGate>().date= date;
			fence.GetComponent<ItemGate>().id_type= id_type;
			fence.GetComponent<ItemGate>().id_style= id_style;
			fence.GetComponent<ItemGate>().id_geometry= id_geometry;
			fence.GetComponent<ItemGate>().width= width;
			fence.GetComponent<ItemGate>().height= height;
			fence.GetComponent<ItemGate>().heightmax = heightmax ;
			fence.GetComponent<ItemGate>().opening = opening ;
			fence.GetComponent<ItemGate>().json= json;
			//fence.GetComponent<ItemGate>().type=it;
		
		});
			}

	private void instantiateSavedGates(string filePath){

		string dir=Path.GetDirectoryName(filePath);

		for (int i = 0; i < loadedData.canc.gP.Length; i++) 
		{
			
				GameObject gate = new GameObject ();

		
			
				gate.name = loadedData.canc.gP [i].gateName;
				gate.AddComponent<MeshFilter> ();
				gate.AddComponent<MeshRenderer> ();
			Vector3 thisGatePosition = loadedData.canc.gP [i].gatePos.value;
			Vector3 thisGateEulers = new Vector3(loadedData.canc.gP [i].gateRot.value.x,0,loadedData.canc.gP [i].gateRot.value.z);
				gate.transform.localScale = Vector3.one;
			int id=loadedData.canc.gP[i].gS.id;
			string sname=loadedData.canc.gP[i].gS.name;
			string image_path=loadedData.canc.gP[i].gS.image_path;
			string date=loadedData.canc.gP[i].gS.date;
			int id_type=loadedData.canc.gP[i].gS.id_type;
			int id_style=loadedData.canc.gP[i].gS.id_style;
			int id_geometry=loadedData.canc.gP[i].gS.id_geometry;
			int width=loadedData.canc.gP[i].gS.width;
			int height=loadedData.canc.gP[i].gS.height;
			int heightmax =loadedData.canc.gP[i].gS.heightMax ;
			string opening =loadedData.canc.gP[i].gS.opening ;
			string json=loadedData.canc.gP[i].gS.json;
			float colWidthL = loadedData.canc.gP [i].gS.colWidthL;
			float colDepthL = loadedData.canc.gP [i].gS.colDepthL;
			float capWidthL = loadedData.canc.gP [i].gS.capWidthL;
			float capDepthL = loadedData.canc.gP [i].gS.capDepthL;
			float capHeightL = loadedData.canc.gP [i].gS.capHeightL;
			float colWidthR = loadedData.canc.gP [i].gS.colWidthR;
			float colDepthR = loadedData.canc.gP [i].gS.colDepthR;
			float capWidthR = loadedData.canc.gP [i].gS.capWidthR;
			float capDepthR = loadedData.canc.gP [i].gS.capDepthR;
			float capHeightR = loadedData.canc.gP [i].gS.capHeightR;
			ItemType it = loadedData.canc.gP [i].gS.it;
			gate.name = loadedData.canc.gP [i].gateName;	
				//carica il file OBJ del cancello
				string filename = loadedData.canc.gP [i].filename;

				OBJ obj = gate.AddComponent<OBJ> ();
				obj.loadMaterial = true;
				obj.objPath =dir+"/"+filename;
			obj.objPath =dir+"/"+filename;
				obj.StartLoadCoroutine ((value)=>{
				
					Destroy(gate.GetComponent<OBJ>());
				for(int q=0;q<gate.transform.childCount;q++){
					gate.transform.GetChild(q).localPosition=Vector3.zero;
					gate.transform.GetChild(q).localEulerAngles =Vector3.zero;
					gate.transform.position=thisGatePosition ;
					gate.transform.eulerAngles=new Vector3(thisGateEulers.x,thisGateEulers.y,thisGateEulers.z);
				
				
				}
				string seg=gate.name.Replace("Gate","");
				GameObject.Find(seg).GetComponent<linkProperties>().gate=gate;
				gate.AddComponent<ItemGate>();
				gate.GetComponent<ItemGate>().id=id ;
				gate.GetComponent<ItemGate>().name=sname;
				gate.GetComponent<ItemGate>().image_path= image_path;
				gate.GetComponent<ItemGate>().date= date;
				gate.GetComponent<ItemGate>().id_type= id_type;
				gate.GetComponent<ItemGate>().id_style= id_style;
				gate.GetComponent<ItemGate>().id_geometry= id_geometry;
				gate.GetComponent<ItemGate>().width= width;
				gate.GetComponent<ItemGate>().height= height;
				gate.GetComponent<ItemGate>().heightmax = heightmax ;
				gate.GetComponent<ItemGate>().opening = opening ;
				gate.GetComponent<ItemGate>().json= json;
				gate.GetComponent<ItemGate>().type=it;

				gate.GetComponent<ItemGate>().columnWidthL=colWidthL;
				gate.GetComponent<ItemGate>().columnDepthL=colDepthL;
				gate.GetComponent<ItemGate>().capitalWidthL=capWidthL;
				gate.GetComponent<ItemGate>().capitalDepthL=capDepthL;
				gate.GetComponent<ItemGate>().capitalHeightL=capHeightL;

				gate.GetComponent<ItemGate>().columnWidthR=colWidthR;
				gate.GetComponent<ItemGate>().columnDepthR=colDepthR;
				gate.GetComponent<ItemGate>().capitalWidthR=capWidthR;
				gate.GetComponent<ItemGate>().capitalDepthR=capDepthR;
				gate.GetComponent<ItemGate>().capitalHeightR=capHeightR;








				});




		}



		
	}
			private void instantiateSavedSegments(string filePath){

		for (int i = 0; i < loadedData.recin.sS.Length; i++) {
			GameObject seg=Instantiate (Resources.Load ("cubetto") as GameObject);
			seg.name = loadedData.recin.sS [i].segName;
			seg.tag = "segment";
			seg.transform.position = loadedData.recin.sS [i].segPosition.value;
			seg.transform.eulerAngles = loadedData.recin.sS [i].segRotation.value;
			seg.transform.localScale = loadedData.recin.sS [i].segScale.value;
			seg.GetComponent<MeshFilter> ().mesh.vertices = loadedData.recin.sS [i].segMesh.vertices;
			seg.GetComponent<MeshFilter> ().mesh.RecalculateBounds ();
			if (loadedData.recin.sS [i].SP.L != 0) {
				linkProperties tlp = seg.GetComponent<linkProperties> ();
				tlp.thisSegmentProperties = new float[8];
				tlp.thisSegmentProperties [0] = loadedData.recin.sS [i].SP.H;
				tlp.thisSegmentProperties [1] = loadedData.recin.sS [i].SP.W;
				tlp.thisSegmentProperties [2] = loadedData.recin.sS [i].SP.alfa;
				tlp.thisSegmentProperties [3] = loadedData.recin.sS [i].SP.beta;
				//tlp.thisSegmentProperties[4]
				tlp.thisSegmentProperties[5]=loadedData.recin.sS [i].SP.L;
				tlp.thisSegmentProperties [6] = loadedData.recin.sS [i].SP.gamma;
				tlp.thisSegmentProperties [7] = loadedData.recin.sS [i].SP.HPrec;
				tlp.isOpening = loadedData.recin.sS [i].SP.isOpening;
				tlp.thickTyp = loadedData.recin.sS [i].SP.thickType;
				//tlp.instantiatedFromSaveFile = true;
				if(loadedData.recin.sS[i].SP.associatedGate!="no gate")
				tlp.gate=GameObject.Find(loadedData.recin.sS[i].SP.associatedGate);
			
			}
	
			for (int z = 0; z < loadedData.recin.sS [i].SC.Length; z++) {
				
				if (loadedData.recin.sS [i].SC [z].nodeName == "node(Clone)") {
					string nodeToLoad = loadedData.recin.sS [i].SC [z].nodeName;
					nodeToLoad = nodeToLoad.Replace ("(Clone)", "");
					GameObject node = Instantiate (Resources.Load (nodeToLoad) as GameObject);
					GameObject rotPoint = new GameObject ();
				//	rotPoint.name = "rotationPoint";
					node.tag = nodeToLoad;
					node.transform.position = loadedData.recin.sS [i].SC [z].nodePos.value;
				//	rotPoint.transform.position = node.transform.position;
					seg.GetComponent<linkProperties> ().lookAtOb = rotPoint;

					node.transform.localScale = Vector3.one;
					node.transform.parent = seg.transform;
					node.transform.localEulerAngles = new Vector3 (180, 0, 180);

					node.transform.localScale = new Vector3 (1 / seg.transform.localScale.x, 1 / seg.transform.localScale.y, 1 / seg.transform.localScale.x);
				} else if(loadedData.recin.sS[i].SC[z].nodeName=="nodeCong(Clone)") {
					string nodeToLoad = loadedData.recin.sS [i].SC [z].nodeName;
					nodeToLoad = nodeToLoad.Replace ("(Clone)", "");
					GameObject node = Instantiate (Resources.Load (nodeToLoad) as GameObject);
					//	rotPoint.name = "rotationPoint";
					node.tag = nodeToLoad;
					node.transform.position = loadedData.recin.sS [i].SC [z].nodePos.value;
					//	rotPoint.transform.position = node.transform.position;

					node.transform.localScale = Vector3.one;
					node.transform.parent = seg.transform;
					node.transform.localEulerAngles = new Vector3 (180, 0, 180);

					node.transform.localScale = new Vector3 (1 / seg.transform.localScale.x, 1 / seg.transform.localScale.y, 1 / seg.transform.localScale.x);
				}


			}
			if (loadedData.recin.sS [i].columndx.cP.belongingSegment != "") {
				GameObject col = GameObject.CreatePrimitive (PrimitiveType.Cube);
				GameObject cap = GameObject.CreatePrimitive (PrimitiveType.Cube);
				col.transform.position = loadedData.recin.sS [i].columndx.cP.pos.value;
				col.transform.eulerAngles = loadedData.recin.sS [i].columndx.cP.rot.value;
				col.transform.localScale  = loadedData.recin.sS [i].columndx.cP.sca.value;
				cap.transform.parent = col.transform;
				cap.transform.localPosition = loadedData.recin.sS [i].columndx.cP.capitalLocalPos.value;
				cap.transform.localScale = loadedData.recin.sS [i].columndx.cP.capitalScale.value;
				seg.GetComponent<linkProperties> ().columnR = col;
			}
			if (loadedData.recin.sS [i].columnsx.cP.belongingSegment != "") {
				GameObject col = GameObject.CreatePrimitive (PrimitiveType.Cube);
				GameObject cap = GameObject.CreatePrimitive (PrimitiveType.Cube);
				col.transform.position = loadedData.recin.sS [i].columnsx.cP.pos.value;
				col.transform.eulerAngles = loadedData.recin.sS [i].columnsx.cP.rot.value;
				col.transform.localScale = loadedData.recin.sS [i].columnsx.cP.sca.value;
				cap.transform.parent = col.transform;
				cap.transform.localPosition = loadedData.recin.sS [i].columnsx.cP.capitalLocalPos.value;
				cap.transform.localScale = loadedData.recin.sS [i].columnsx.cP.capitalScale.value;
				seg.GetComponent<linkProperties> ().columnL = col;
			}
			if (loadedData.recin.sS [i].stakesHere.Length != 0) {
				for (int q = 0; q < loadedData.recin.sS [i].stakesHere.Length; q++) {
					GameObject newStake = GameObject.CreatePrimitive (PrimitiveType.Cube);
					newStake.tag = "stake";
					newStake.name = loadedData.recin.sS [i].stakesHere [q].stakProp.stakeName;
					newStake.AddComponent<BoxCollider> ();
					newStake.transform.parent = seg.transform;
					newStake.transform.position = loadedData.recin.sS [i].stakesHere [q].stakProp.stakePos.value;
					newStake.transform.eulerAngles  = loadedData.recin.sS [i].stakesHere [q].stakProp.stakeRot.value;
					newStake.transform.localScale = loadedData.recin.sS [i].stakesHere [q].stakProp.stakeScale.value;
					if (loadedData.recin.sS [i].stakesHere [q].stakProp.hasFence == true) {
						instantiateSavedFences (filePath, loadedData.recin.sS [i].stakesHere [q].stakProp,newStake);
					}
						
				}
			}
			for (int q = 0; q < loadedData.recin.sS [i].fbox.Length; q++) {
				GameObject fenceBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
				fenceBox.transform.parent = seg.transform;
				fenceBox.transform.position = loadedData.recin.sS [i].fbox [q].pos;
				fenceBox.transform.localEulerAngles = loadedData.recin.sS [i].fbox [q].localRot;
				fenceBox.transform.localScale = loadedData.recin.sS [i].fbox [q].localScale;
				fenceBox.name = loadedData.recin.sS [i].fbox [q].name;
				fenceBox.tag = "fence";
				fenceBox.GetComponent<MeshRenderer> ().material = Resources.Load ("fenceBox") as Material;
				fenceBox.AddComponent<BoxCollider> ();
				fenceBox.AddComponent<fenceBoxScript> ();
				fenceBox.GetComponent<fenceBoxScript> ().firstPole = seg.transform.Find (loadedData.recin.sS [i].fbox [q].firstPole).gameObject ;
				fenceBox.GetComponent<fenceBoxScript> ().secondPole = seg.transform.Find (loadedData.recin.sS [i].fbox [q].secondPole).gameObject ;
				fenceBox.GetComponent<fenceBoxScript> ().hasAFence = loadedData.recin.sS [i].fbox [q].hasFence;

			}
		

		}
	

		for (int i = 0; i < loadedData.recin.sS.Length; i++) {
			if (loadedData.recin.sS [i].AttachedSeg != "")
				GameObject.Find (loadedData.recin.sS [i].segName).GetComponent<linkProperties> ().attachedSegment = GameObject.Find (loadedData.recin.sS [i].AttachedSeg);
			if (loadedData.recin.sS [i].prevSeg != "")
				GameObject.Find (loadedData.recin.sS [i].segName).GetComponent<linkProperties> ().previousSegment = GameObject.Find (loadedData.recin.sS [i].prevSeg);

		}

	}
	private void instantiateSavedSegmentsBalcony(string filePath){
		//Mi prendo alcuni dati che mi torneranno utili dopo aver instanziato ipaletti 
		float larghezzaCorrimano=0;
		float altezzaCorrimano = 0;
		for (int i = 0; i < loadedData.balc.sS.Length; i++) {
			GameObject seg=Instantiate (Resources.Load ("cubetto") as GameObject);
			seg.name = loadedData.balc.sS [i].segName;
			seg.tag = "segment";
			seg.transform.position = loadedData.balc.sS [i].segPosition.value;
			seg.transform.eulerAngles = loadedData.balc.sS [i].segRotation.value;
			seg.transform.localScale = loadedData.balc.sS [i].segScale.value;
			seg.GetComponent<MeshFilter> ().mesh.vertices = loadedData.balc.sS [i].segMesh.vertices;
			seg.GetComponent<MeshFilter> ().mesh.RecalculateBounds ();
			if (loadedData.balc.sS [i].SP.L != 0) {
				linkProperties tlp = seg.GetComponent<linkProperties> ();
				tlp.isBalcony = true;
				tlp.thisSegmentProperties = new float[8];
				tlp.thisSegmentProperties [0] = loadedData.balc.sS [i].SP.H;
				tlp.thisSegmentProperties [1] = loadedData.balc.sS [i].SP.W;
				tlp.thisSegmentProperties [2] = loadedData.balc.sS [i].SP.alfa;
				tlp.thisSegmentProperties [3] = loadedData.balc.sS [i].SP.beta;
				//tlp.thisSegmentProperties[4]
				tlp.thisSegmentProperties[5]=loadedData.balc.sS [i].SP.L;
				tlp.thisSegmentProperties [6] = loadedData.balc.sS [i].SP.gamma;
				tlp.thisSegmentProperties [7] = loadedData.balc.sS [i].SP.HPrec;
				tlp.isOpening = loadedData.balc.sS [i].SP.isOpening;
				tlp.thickTyp = loadedData.balc.sS [i].SP.thickType;
				//tlp.instantiatedFromSaveFile = true;
				if(loadedData.balc.sS[i].SP.associatedGate!="no gate")
					tlp.gate=GameObject.Find(loadedData.balc.sS[i].SP.associatedGate);

			}

			for (int z = 0; z < loadedData.balc.sS [i].SC.Length; z++) {

				if (loadedData.balc.sS [i].SC [z].nodeName == "node(Clone)") {
					string nodeToLoad = loadedData.balc.sS [i].SC [z].nodeName;
					nodeToLoad = nodeToLoad.Replace ("(Clone)", "");
					GameObject node = Instantiate (Resources.Load (nodeToLoad) as GameObject);
					GameObject rotPoint = new GameObject ();
					//	rotPoint.name = "rotationPoint";
					node.tag = nodeToLoad;
					node.transform.position = loadedData.balc.sS [i].SC [z].nodePos.value;
					//	rotPoint.transform.position = node.transform.position;
					seg.GetComponent<linkProperties> ().lookAtOb = rotPoint;

					node.transform.localScale = Vector3.one;
					node.transform.parent = seg.transform;
					node.transform.localEulerAngles = new Vector3 (180, 0, 180);

					node.transform.localScale = new Vector3 (1 / seg.transform.localScale.x, 1 / seg.transform.localScale.y, 1 / seg.transform.localScale.x);
				} else if(loadedData.balc.sS[i].SC[z].nodeName=="nodeCong(Clone)") {
					string nodeToLoad = loadedData.balc.sS [i].SC [z].nodeName;
					nodeToLoad = nodeToLoad.Replace ("(Clone)", "");
					GameObject node = Instantiate (Resources.Load (nodeToLoad) as GameObject);
					//	rotPoint.name = "rotationPoint";
					node.tag = nodeToLoad;
					node.transform.position = loadedData.balc.sS [i].SC [z].nodePos.value;
					//	rotPoint.transform.position = node.transform.position;

					node.transform.localScale = Vector3.one;
					node.transform.parent = seg.transform;
					node.transform.localEulerAngles = new Vector3 (180, 0, 180);

					node.transform.localScale = new Vector3 (1 / seg.transform.localScale.x, 1 / seg.transform.localScale.y, 1 / seg.transform.localScale.x);
				}


			}
			if (loadedData.balc.sS [i].columndx.cP.belongingSegment != "") {
				GameObject col = GameObject.CreatePrimitive (PrimitiveType.Cube);
				GameObject cap = GameObject.CreatePrimitive (PrimitiveType.Cube);
				col.transform.position = loadedData.balc.sS [i].columndx.cP.pos.value;
				col.transform.eulerAngles = loadedData.balc.sS [i].columndx.cP.rot.value;
				col.transform.localScale  = loadedData.balc.sS [i].columndx.cP.sca.value;
				cap.transform.parent = col.transform;
				cap.transform.localPosition = loadedData.balc.sS [i].columndx.cP.capitalLocalPos.value;
				cap.transform.localScale = loadedData.balc.sS [i].columndx.cP.capitalScale.value;
				seg.GetComponent<linkProperties> ().columnR = col;
			}
			if (loadedData.balc.sS [i].columnsx.cP.belongingSegment != "") {
				GameObject col = GameObject.CreatePrimitive (PrimitiveType.Cube);
				GameObject cap = GameObject.CreatePrimitive (PrimitiveType.Cube);
				col.transform.position = loadedData.balc.sS [i].columnsx.cP.pos.value;
				col.transform.eulerAngles = loadedData.balc.sS [i].columnsx.cP.rot.value;
				col.transform.localScale = loadedData.balc.sS [i].columnsx.cP.sca.value;
				cap.transform.parent = col.transform;
				cap.transform.localPosition = loadedData.balc.sS [i].columnsx.cP.capitalLocalPos.value;
				cap.transform.localScale = loadedData.balc.sS [i].columnsx.cP.capitalScale.value;
				seg.GetComponent<linkProperties> ().columnL = col;
			}
			if (loadedData.balc.sS [i].stakesHere.Length != 0) {
				for (int q = 0; q < loadedData.balc.sS [i].stakesHere.Length; q++) {
					GameObject newStake = GameObject.CreatePrimitive (PrimitiveType.Cube);
					newStake.tag = "stake";
					newStake.name = loadedData.balc.sS [i].stakesHere [q].stakProp.stakeName;
					newStake.AddComponent<BoxCollider> ();
					newStake.transform.parent = seg.transform;
					newStake.transform.position = loadedData.balc.sS [i].stakesHere [q].stakProp.stakePos.value;
					newStake.transform.eulerAngles  = loadedData.balc.sS [i].stakesHere [q].stakProp.stakeRot.value;
					newStake.transform.localScale = loadedData.balc.sS [i].stakesHere [q].stakProp.stakeScale.value;
					if (loadedData.balc.sS [i].stakesHere [q].stakProp.hasFence == true) {
						instantiateSavedFences (filePath, loadedData.balc.sS [i].stakesHere [q].stakProp,newStake);
					}
					if (loadedData.balc.sS [i].stakesHere [q].stakProp.pP.handrail.exists) {
						larghezzaCorrimano = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.handrail.width;
						altezzaCorrimano = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.handrail.height;
					}
					if (loadedData.balc.sS [i].stakesHere [q].stakProp.pP.exists) {
						newStake.AddComponent<stakeProperties> ();
						if (loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastraCollegamentoPos != Vector3.zero) {
							GameObject basePlate = GameObject.CreatePrimitive (PrimitiveType.Cube);
							basePlate.transform.position = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastraCollegamentoPos;
							basePlate.transform.eulerAngles = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastraCollegamentoRot;
							basePlate.transform.localScale = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastrCollegamentoScale;
							newStake.GetComponent<stakeProperties> ().basePlate = basePlate;
							newStake.GetComponent<stakeProperties> ().basePlateHeight = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastrCollegamentoScale.y;
							newStake.GetComponent<stakeProperties> ().basePlateWidth = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.piastrCollegamentoScale.x;
						}

						if (loadedData.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoPos != Vector3.zero) {
							GameObject newLink = GameObject.CreatePrimitive (PrimitiveType.Cube);
							newLink.transform.position = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoPos;
							newLink.transform.eulerAngles = loadedData.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoRot;
							newLink.transform.parent = newStake.transform.parent;
							newLink.transform.localScale=loadedData.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoScale;
							if (!newStake.GetComponent<stakeProperties> ())
								newStake.AddComponent<stakeProperties> ();
							newStake.GetComponent<stakeProperties> ().link = newLink;

						}
						

					}

				}
			}
			for (int q = 0; q < loadedData.balc.sS [i].fbox.Length; q++) {
				GameObject fenceBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
				fenceBox.transform.parent = seg.transform;
				fenceBox.transform.position = loadedData.balc.sS [i].fbox [q].pos;
				fenceBox.transform.localEulerAngles = loadedData.balc.sS [i].fbox [q].localRot;
				fenceBox.transform.localScale = loadedData.balc.sS [i].fbox [q].localScale;
				fenceBox.name = loadedData.balc.sS [i].fbox [q].name;
				fenceBox.tag = "fence";
				fenceBox.GetComponent<MeshRenderer> ().material = Resources.Load ("fenceBox") as Material;
				fenceBox.AddComponent<BoxCollider> ();
				fenceBox.AddComponent<fenceBoxScript> ();
				fenceBox.GetComponent<fenceBoxScript> ().firstPole = seg.transform.Find (loadedData.balc.sS [i].fbox [q].firstPole).gameObject ;
				fenceBox.GetComponent<fenceBoxScript> ().secondPole = seg.transform.Find (loadedData.balc.sS [i].fbox [q].secondPole).gameObject ;
				fenceBox.GetComponent<fenceBoxScript> ().hasAFence = loadedData.balc.sS [i].fbox [q].hasFence;

			}


		}


		for (int i = 0; i < loadedData.balc.sS.Length; i++) {
			if (loadedData.balc.sS [i].AttachedSeg != "")
				GameObject.Find (loadedData.balc.sS [i].segName).GetComponent<linkProperties> ().attachedSegment = GameObject.Find (loadedData.balc.sS [i].AttachedSeg);
			if (loadedData.balc.sS [i].prevSeg != "")
				GameObject.Find (loadedData.balc.sS [i].segName).GetComponent<linkProperties> ().previousSegment = GameObject.Find (loadedData.balc.sS [i].prevSeg);

		}
		if (larghezzaCorrimano > 0 && altezzaCorrimano > 0) {
			stkhand.segment = GameObject.FindGameObjectWithTag ("segment");
			stkhand.enabled = true;
			stkhand.generateHandrailWithDim (altezzaCorrimano,larghezzaCorrimano);
			stkhand.enabled = false;
		}


	}
	void instantiateInterFences(string filePath){
		GameObject[] allsegments = GameObject.FindGameObjectsWithTag ("segment");
	
		if (loadedData.intR.fF.Length > 0) {
			for (int o = 0; o < loadedData.intR.fF.Length; o++) {
				GameObject interfence = GameObject.CreatePrimitive (PrimitiveType.Cube);
				interfence.AddComponent<fenceBoxScript> ();
				interfence.transform.position = loadedData.intR.fF [o].fencePosition;
				interfence.transform.eulerAngles = loadedData.intR.fF [o].fenceRotation;
				interfence.transform.localScale = loadedData.intR.fF [o].fenceScale;
				interfence.name = loadedData.intR.fF [o].fenceName;
				string firstToFind = loadedData.intR.fF [o].firstSegment;
				GameObject firstSegFound = GameObject.Find (firstToFind);
				interfence.GetComponent<fenceBoxScript> ().firstSegment =firstSegFound;
				interfence.GetComponent<fenceBoxScript> ().secondSegment = GameObject.Find (loadedData.intR.fF [o].secondSegment);
			
				interfence.GetComponent<fenceBoxScript> ().firstPole =firstSegFound.transform.Find(loadedData.intR.fF [o].firstStack).gameObject ;

				interfence.GetComponent<fenceBoxScript> ().secondPole = interfence.GetComponent<fenceBoxScript> ().secondSegment.transform.Find (loadedData.intR.fF [o].secondStack).gameObject ;

				interfence.GetComponent<fenceBoxScript>().hasAFence=loadedData.intR.fF[o].hasAFence;
				interfence.GetComponent<MeshRenderer> ().material = Resources.Load ("fenceBox") as Material;
				interfence.tag = "fence";
				if (loadedData.intR.fF[o].myFence!="") {
					
					string filename = loadedData.intR.fF [o].myFence + ".obj";
					GameObject fenceToBe = new GameObject ();
					string dir=Path.GetDirectoryName(filePath);
					OBJ obj = fenceToBe.AddComponent<OBJ> ();
					fenceToBe.transform.position = loadedData.intR.fF [o].fenceObjPos;
					fenceToBe.transform.eulerAngles = loadedData.intR.fF [o].fenceObjRot;
					fenceToBe.transform.localScale = loadedData.intR.fF [o].fenceObjScale;
					fenceToBe.name = loadedData.intR.fF [o].myFence;
					fenceToBe.AddComponent<ItemGate> ();
					/*
					SaveFile.intR.fF[g].gS = new gateScript ();
					SaveFile.intR.fF[g].gS.id =midFences[g].GetComponent<ItemGate> ().id;
					SaveFile.intR.fF[g].gS.name = midFences[g].GetComponent<ItemGate> ().name;
					SaveFile.intR.fF[g].gS.image_path  =midFences[g].GetComponent<ItemGate> ().image_path ;
					SaveFile.intR.fF[g].gS.date = midFences[g].GetComponent<ItemGate> ().date;
					SaveFile.intR.fF[g].gS.id_type =midFences[g].GetComponent<ItemGate> ().id_type;
					SaveFile.intR.fF[g].gS.id_style= midFences[g].GetComponent<ItemGate> ().id_style;
					SaveFile.intR.fF[g].gS.id_geometry =midFences[g].GetComponent<ItemGate> ().id_geometry;
					SaveFile.intR.fF[g].gS.width = midFences[g].GetComponent<ItemGate> ().width;
					SaveFile.intR.fF[g].gS.height = midFences[g].GetComponent<ItemGate> ().height ;
					SaveFile.intR.fF[g].gS.heightMax  = midFences[g].GetComponent<ItemGate> ().heightmax ;
					SaveFile.intR.fF[g].gS.opening = midFences[g].GetComponent<ItemGate> ().opening;
					SaveFile.intR.fF[g].gS.json = midFences[g].GetComponent<ItemGate> ().json;
					SaveFile.intR.fF[g].gS.it = midFences[g].GetComponent<ItemGate> ().type;
					*/
					fenceToBe.GetComponent<ItemGate> ().id = loadedData.intR.fF [o].gS.id;
					fenceToBe.GetComponent<ItemGate> ().name = loadedData.intR.fF [o].gS.name;
					fenceToBe.GetComponent<ItemGate> ().image_path = loadedData.intR.fF [o].gS.image_path;
					fenceToBe.GetComponent<ItemGate> ().date = loadedData.intR.fF [o].gS.date;
					fenceToBe.GetComponent<ItemGate> ().id_type = loadedData.intR.fF [o].gS.id_type;
					fenceToBe.GetComponent<ItemGate> ().id_style = loadedData.intR.fF [o].gS.id_style;
					fenceToBe.GetComponent<ItemGate> ().id_geometry = loadedData.intR.fF [o].gS.id_geometry;
					fenceToBe.GetComponent<ItemGate> ().width = loadedData.intR.fF [o].gS.width;
					fenceToBe.GetComponent<ItemGate> ().height = loadedData.intR.fF [o].gS.height;
					fenceToBe.GetComponent<ItemGate> ().heightmax = loadedData.intR.fF [o].gS.heightMax;
					fenceToBe.GetComponent<ItemGate> ().opening = loadedData.intR.fF [o].gS.opening;
					fenceToBe.GetComponent<ItemGate> ().json= loadedData.intR.fF [o].gS.json;
					fenceToBe.GetComponent<ItemGate> ().type = loadedData.intR.fF [o].gS.it;












					interfence.GetComponent<fenceBoxScript> ().myfence = fenceToBe;
					obj.loadMaterial = true;
					obj.objPath =dir+"/"+filename;
					obj.StartLoadCoroutine ((value) => {

						Destroy (fenceToBe.GetComponent<OBJ> ());

						for (int q = 0; q < fenceToBe.transform.childCount; q++) {
							fenceToBe.transform.GetChild (q).localPosition = Vector3.zero;
							fenceToBe.transform.GetChild (q).localEulerAngles = Vector3.zero;



						}
					});
			}
		}
	}
	}


	void Awake(){
		ortoCam = GameObject.Find ("Main Camera");
		Ecmd = GameObject.Find ("vertWelder").GetComponent<ExperimCmd> ();
		if (StaticVariables.comingFromGate && !StaticVariables.comingFromFence) { //verifica che dalla scena di design stanno passando un cancello
			load (Application.dataPath + "/provv.json"); //carica la scena temporenea
			GameObject gate = GameObject.Find ("Gate");
			GameObject opening = null;
			GameObject segment = GameObject.Find (segmentOfGate);
			Debug.Log (segmentOfGate);
			Debug.Log (segment);
			if (segment.GetComponent<linkProperties> ().isOpening) {
				opening = segment;
		
				
			}

			segment.GetComponent<linkProperties> ().gate = gate;

			gate.tag = "gate";
			gate.name = gate.name + segment.name;
			gate.transform.localScale = new Vector3 (0.001f, 0.001f, 0.001f);
		



			gate.transform.eulerAngles = new Vector3 (0, opening.transform.eulerAngles.y, 0);
			GameObject cazzaredo = new GameObject ();
			cazzaredo.transform.parent = opening.transform;
			cazzaredo.transform.localPosition = opening.transform.Find ("node(Clone)").localPosition;
			cazzaredo.transform.parent = null;
			gate.transform.position = cazzaredo.transform.position;
			instantiateColumns (gate);
		
		

		}
		if(StaticVariables.comingFromFence==true) {
			load (Application.dataPath + "/provv.json");
			GameObject fence = GameObject.FindWithTag ("freshFence");
			fence.transform.parent = null;
			//Qua devo prevedere due casi: il caso dei cancelli intersegmento, e i casi dei cancelli normali. Discriminante: "Parent".
			if (!StaticVariables.relativeSegment.Contains("inter_")) {
				StartCoroutine (waitAFrameParented (fence));
			} else { //qua ci va la logica dei cancelli inter 
				//Trovo il box corrispondente che si porta dietro tutte le variabili del caso.
				StartCoroutine(waitAFrame(fence));
			}
		}

	}
	IEnumerator waitAFrame(GameObject fence){
		yield return 0;
		string splitted=StaticVariables.relativeSegment.Remove(0,6); //tolgo la scritta inter. Che casino
		string boxName="BoxinterFence_"+splitted;
		GameObject relBox= GameObject.Find (boxName);

		GameObject originStake = relBox.GetComponent<fenceBoxScript> ().firstPole;
		fence.transform.position= new Vector3 (originStake.transform.position.x, originStake.transform.position.y - (originStake.transform.localScale.y * originStake.transform.parent.localScale.z) / 2, originStake.transform.position.z);
		fence.transform.localScale = Vector3.one * 0.001f;
		fence.transform.eulerAngles =new Vector3( relBox.transform.eulerAngles.x,relBox.transform.eulerAngles.y-90,relBox.transform.eulerAngles.z);
		GameObject fakeParent = new GameObject ();
		fakeParent.transform.position = fence.transform.position;
		fence.transform.parent = fakeParent.transform;
		fence.transform.parent = null;
		Destroy (fakeParent);
		fence.name = "interRecin" + originStake.name + originStake.transform.parent.name;
		relBox.GetComponent<fenceBoxScript> ().hasAFence = true;
		relBox.GetComponent<fenceBoxScript> ().myfence = fence;
		StaticVariables.relativeSegment = "";
		StaticVariables.comingFromFence = false;

	}
	IEnumerator waitAFrameParented(GameObject fence){
		yield return 0;
		GameObject parentSegment = GameObject.Find (StaticVariables.relativeSegment);
		GameObject originStake = parentSegment.transform.Find (StaticVariables.firstPole).gameObject;
		fence.transform.position = new Vector3 (originStake.transform.position.x, originStake.transform.position.y - (originStake.transform.localScale.y * originStake.transform.parent.localScale.z) / 2, originStake.transform.position.z);
		fence.transform.localScale = Vector3.one * 0.001f;
		fence.transform.localEulerAngles = new Vector3 (0, 180, 0);
		fence.transform.localEulerAngles = new Vector3 (0, 180 + parentSegment.transform.localEulerAngles.y, 0);

		fence.transform.parent = originStake.transform;
		fence.name = "recin_" + originStake.name + originStake.transform.parent.name;
		//fence.transform.eulerAngles = new Vector3 (fence.transform.eulerAngles.x, fence.transform.eulerAngles.y, fence.transform.parent.transform.parent.GetComponent<linkProperties> ().thisSegmentProperties [2]);
		fenceDistortionInterpolator (fence, fence.transform.parent.transform.parent.GetComponent<linkProperties> ().thisSegmentProperties [2]);
		StaticVariables.comingFromFence = false;
		StaticVariables.relativeSegment = "";
	}
	IEnumerator associateLookPoints(){
		yield return new WaitForSeconds (0.5f);


	}

	/// <summary>
	/// Istanzia le grate e i vari oggetti ad essa legati
	/// </summary>
	public void instantiateGrates(string filepath){
		for (int i = 0; i < loadedData.gra.gS.Length; i++) {
			string obToFind = "GrataHandler" + i.ToString ();
			GameObject thisWind=GameObject.Find (obToFind);
			thisWind.GetComponent<adaptiveBezel> ().D = loadedData.gra.gS [i].pG.D;
			thisWind.GetComponent<adaptiveBezel> ().cws = loadedData.gra.gS [i].pG.cws;
			thisWind.GetComponent<adaptiveBezel> ().cwi = loadedData.gra.gS [i].pG.cwi;
			thisWind.GetComponent<adaptiveBezel> ().H = loadedData.gra.gS [i].pG.H;
			thisWind.GetComponent<adaptiveBezel> ().L = loadedData.gra.gS [i].pG.L;
			thisWind.GetComponent<adaptiveBezel> ().pxl = loadedData.gra.gS [i].pG.pxl;
			thisWind.GetComponent<adaptiveBezel> ().pxr = loadedData.gra.gS [i].pG.pxr;
			thisWind.GetComponent<adaptiveBezel> ().pys = loadedData.gra.gS [i].pG.pys;
			thisWind.GetComponent<adaptiveBezel> ().pyi = loadedData.gra.gS [i].pG.pyi;
			thisWind.GetComponent<adaptiveBezel> ().createWindowProcess ();
		}
	}
	/// <summary>
	/// Quando un cancello viene inserito nella scena, ne instanzia le colonne corrispondenti 
	/// </summary>
	public void instantiateColumns(GameObject gate){
		GameObject dxColumn=GameObject.CreatePrimitive(PrimitiveType.Cube);
		GameObject sxColumn=GameObject.CreatePrimitive(PrimitiveType.Cube);
		dxColumn.transform.parent=gate.transform;
		sxColumn.transform.parent=gate.transform;
		float scaleComp=1;
		float divCompt = 1000;
		if (gate.transform.localScale.x < 0.1) {
			scaleComp = 1000;
			divCompt = 1;
		}
		string segmentOfThisGate=gate.name.Replace("Gate","");
		GameObject thisSeg = GameObject.Find (segmentOfThisGate);
		float ColumnRotation=thisSeg.transform.eulerAngles.y;

		float height = gate.GetComponent<ItemGate> ().height/scaleComp ;
		float columnWidthL = gate.GetComponent<ItemGate> ().columnWidthL / scaleComp ;
		float columnDepthL = gate.GetComponent<ItemGate> ().columnDepthL / scaleComp ;
		float columnWidthR = gate.GetComponent<ItemGate> ().columnWidthR /scaleComp ;
		float columnDepthR = gate.GetComponent<ItemGate> ().columnDepthR /scaleComp ;
		Vector3 columnDimensionsL=new Vector3(columnWidthL ,height,columnDepthL );
		Vector3 columnDimensionsR=new Vector3(columnWidthR ,height,columnDepthR );
		dxColumn.transform.localScale=columnDimensionsR*scaleComp/divCompt; 
		sxColumn.transform.localScale = columnDimensionsL * scaleComp/divCompt ;
		dxColumn.transform.localPosition=new Vector3(-columnDimensionsR.x/(2f*divCompt)*scaleComp ,columnDimensionsR.y/(2f*divCompt)*scaleComp ,0);
		sxColumn.transform.localPosition=new Vector3(scaleComp*(GameObject.Find(segmentOfThisGate).transform.localScale.x+columnDimensionsL.x/(2f*divCompt)),(columnDimensionsL.y/(2f*divCompt))*scaleComp ,0);
		sxColumn.transform.eulerAngles=new Vector3(0,ColumnRotation,0);
		dxColumn.transform.eulerAngles=new Vector3(0,ColumnRotation,0);
	//	if (gate.GetComponent<ItemGate> ().capitalDepth > 0 && gate.GetComponent<ItemGate> ().capitalWidth > 0 && gate.GetComponent<ItemGate> ().capitalHeight > 0) {
			GameObject dxCapital = GameObject.CreatePrimitive (PrimitiveType.Cube);
			GameObject sxCapital = GameObject.CreatePrimitive (PrimitiveType.Cube);
			dxCapital.transform.parent=gate.transform;
			sxCapital.transform.parent=gate.transform;
			float capitalHeightL = gate.GetComponent<ItemGate> ().capitalHeightL;
			float capitalWidthL = gate.GetComponent<ItemGate> ().capitalWidthL ;
			float capitalDepthL = gate.GetComponent<ItemGate> ().capitalDepthL;
			float capitalHeightR = gate.GetComponent<ItemGate> ().capitalHeightR;
			float capitalWidthR = gate.GetComponent<ItemGate> ().capitalWidthR ;
			float capitalDepthR = gate.GetComponent<ItemGate> ().capitalDepthR;
		dxCapital.transform.localScale = new Vector3 (capitalWidthR/divCompt, capitalHeightR/divCompt, capitalDepthR/divCompt);
		sxCapital.transform.localScale = new Vector3 (capitalWidthL/divCompt, capitalHeightL/divCompt, capitalDepthL/divCompt);
		dxCapital.transform.parent = dxColumn.transform;
		sxCapital.transform.parent = sxColumn.transform;
		dxCapital.transform.localPosition = new Vector3 (0, .5f+dxCapital.transform.localScale.y*.5f, 0);
		sxCapital.transform.localPosition = new Vector3 (0, .5f+sxCapital.transform.localScale.y*.5f, 0);
			sxCapital.transform.eulerAngles=new Vector3(0,ColumnRotation,0);
			dxCapital.transform.eulerAngles=new Vector3(0,ColumnRotation,0);

			dxCapital.name = "CapitelloDestro";
			dxColumn.name = "ColonnaDestra";
			
			sxCapital.name = "CapitelloSinistro";
			sxColumn.name = "ColonnaSinistra";
			sxColumn.transform.parent = null;
			dxColumn.transform.parent = null;
			thisSeg.GetComponent<linkProperties> ().columnL = sxColumn;
			thisSeg.GetComponent<linkProperties> ().columnR = dxColumn;


		//}
	}

	public void load(string  filePath){
		GameObject[] prevSegs = GameObject.FindGameObjectsWithTag ("segment");
		for (int i = 0; i < prevSegs.Length; i++)
			Destroy (prevSegs [i]);
		if(File.Exists(filePath))
		{
			StartCoroutine (waitForDestruction (filePath));
		
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}


	/// <summary>
	/// Save the specified savepath.
	/// </summary>
	/// <param name="savepath">Savepath.</param>

	public void save(string savepath){

	
		JSonFile SaveFile = new JSonFile ();
		SaveFile.version = Application.version;
		SaveFile.author = "Io";
		SaveFile.creation = System.DateTime.Today.ToString ();
		SaveFile.recin = new recinzioni ();
		SaveFile.canc = new cancelli ();
		//SaveFile.recin.type = "Recinzioni";

		GameObject[] segments =Helper.segmentsNotBalcony( GameObject.FindGameObjectsWithTag ("segment"));
		int segNumber = segments.Length;
		//QUESTA PARTE SALVA BALCONI E RECINZIONI
		SaveFile.recin.sS = new segmentStuff[segments.Length];

		for (int i = 0; i < segments.Length; i++) {
				linkProperties lP = segments [i].GetComponent<linkProperties> ();
				SaveFile.recin.sS [i] = new segmentStuff ();
				SaveFile.recin.sS [i].segName = segments [i].name;
				SaveFile.recin.sS [i].segMesh.vertices = new Vector3[segments [i].GetComponent<MeshFilter> ().mesh.vertices.Length];
				SaveFile.recin.sS [i].segMesh.vertices = storeMatrixVertices (segments [i].GetComponent<MeshFilter> ().mesh);
				if (lP.attachedSegment)
					SaveFile.recin.sS [i].AttachedSeg = lP.attachedSegment.name;
				if (lP.previousSegment)
					SaveFile.recin.sS [i].prevSeg = lP.previousSegment.name;
				SaveFile.recin.sS [i].segPosition.value = segments [i].transform.position;
				SaveFile.recin.sS [i].segRotation.value = segments [i].transform.rotation.eulerAngles;
				SaveFile.recin.sS [i].segScale.value = segments [i].transform.localScale;
				SaveFile.recin.sS [i].lookAtOb = lP.lookAtOb.name;
				SaveFile.recin.sS [i].SP.isOpening = lP.isOpening;
				if (lP.thisSegmentProperties.Length > 0) {
					SaveFile.recin.sS [i].SP.H = lP.thisSegmentProperties [0];
					SaveFile.recin.sS [i].SP.W = lP.thisSegmentProperties [1];
					SaveFile.recin.sS [i].SP.alfa = lP.thisSegmentProperties [2];
					SaveFile.recin.sS [i].SP.beta = lP.thisSegmentProperties [3];
					SaveFile.recin.sS [i].SP.L = lP.thisSegmentProperties [5];
					SaveFile.recin.sS [i].SP.gamma = lP.thisSegmentProperties [6];
					SaveFile.recin.sS [i].SP.HPrec = lP.thisSegmentProperties [7];
					if (lP.gate)
						SaveFile.recin.sS [i].SP.associatedGate = lP.gate.name;
					else
						SaveFile.recin.sS [i].SP.associatedGate = "no gate";	
				}
				SaveFile.recin.sS [i].SP.thickType = lP.thickTyp;
				int childNumb = segments [i].transform.childCount;

				SaveFile.recin.sS [i].SC = new segmentChildren [childNumb];
				for (int j = 0; j < childNumb; j++) {
					SaveFile.recin.sS [i].SC [j] = new segmentChildren ();
					GameObject childNode =	segments [i].transform.GetChild (j).gameObject;
					SaveFile.recin.sS [i].SC [j].nodePos.value = childNode.transform.position;
					SaveFile.recin.sS [i].SC [j].nodeScale.value = childNode.transform.localScale;
					SaveFile.recin.sS [i].SC [j].nodeName = childNode.name;
				}
				if (segments [i].GetComponent<linkProperties> ().columnR != null) {
					GameObject colDx = segments [i].GetComponent<linkProperties> ().columnR;
					SaveFile.recin.sS [i].columndx = new colonna ();
					SaveFile.recin.sS [i].columndx.cP = new columnProperties ();
					SaveFile.recin.sS [i].columndx.cP.belongingSegment = segments [i].name;
					SaveFile.recin.sS [i].columndx.cP.pos = new ObjPosition ();
					SaveFile.recin.sS [i].columndx.cP.rot = new ObjRotation ();
					SaveFile.recin.sS [i].columndx.cP.sca = new ObjScale ();

					SaveFile.recin.sS [i].columndx.cP.pos.value = colDx.transform.position;
					SaveFile.recin.sS [i].columndx.cP.rot.value = colDx.transform.eulerAngles;
					SaveFile.recin.sS [i].columndx.cP.sca.value = colDx.transform.localScale;
					SaveFile.recin.sS [i].columndx.cP.capitalLocalPos = new ObjPosition ();
					SaveFile.recin.sS [i].columndx.cP.capitalScale = new ObjScale ();


					SaveFile.recin.sS [i].columndx.cP.capitalLocalPos.value = colDx.transform.GetChild (0).transform.localPosition;
					SaveFile.recin.sS [i].columndx.cP.capitalScale.value = colDx.transform.GetChild (0).transform.localScale;
				}
				if (segments [i].GetComponent<linkProperties> ().columnL != null) {
					GameObject colSx = segments [i].GetComponent<linkProperties> ().columnL;
					SaveFile.recin.sS [i].columnsx = new colonna ();
					SaveFile.recin.sS [i].columnsx.cP = new columnProperties ();
					SaveFile.recin.sS [i].columnsx.cP.belongingSegment = segments [i].name;
					SaveFile.recin.sS [i].columnsx.cP.pos = new ObjPosition ();
					SaveFile.recin.sS [i].columnsx.cP.rot = new ObjRotation ();
					SaveFile.recin.sS [i].columnsx.cP.sca = new ObjScale ();
					SaveFile.recin.sS [i].columnsx.cP.pos.value = colSx.transform.position;
					SaveFile.recin.sS [i].columnsx.cP.rot.value = colSx.transform.eulerAngles;
					SaveFile.recin.sS [i].columnsx.cP.sca.value = colSx.transform.localScale;
					SaveFile.recin.sS [i].columnsx.cP.capitalLocalPos = new ObjPosition ();
					SaveFile.recin.sS [i].columnsx.cP.capitalScale = new ObjScale ();
					SaveFile.recin.sS [i].columnsx.cP.capitalLocalPos.value = colSx.transform.GetChild (0).transform.localPosition;
					SaveFile.recin.sS [i].columnsx.cP.capitalScale.value = colSx.transform.GetChild (0).transform.localScale;
				}

				int totalStakes = 0;

				for (int d = 0; d < segments [i].transform.childCount; d++) {
					if (segments [i].transform.GetChild (d).tag == "stake")
						totalStakes++;
				}
				GameObject[] totalChildren = new GameObject[totalStakes];
				int counter = 0;
				for (int f = 0; f < segments [i].transform.childCount; f++) {
					if (segments [i].transform.GetChild (f).tag == "stake") {
						totalChildren [counter] = segments [i].transform.GetChild (f).gameObject;
						counter++;
					}
				}
				SaveFile.recin.sS [i].stakesHere = new paletti[counter];
				for (int q = 0; q < counter; q++) {
					SaveFile.recin.sS [i].stakesHere [q] = new paletti ();
					SaveFile.recin.sS [i].stakesHere [q].stakProp = new stakesProps ();
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakeName = totalChildren [q].name;
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakePos = new ObjPosition ();
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakeRot = new ObjRotation ();
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakeScale = new ObjScale ();
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakePos.value = totalChildren [q].transform.position;
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakeRot.value = totalChildren [q].transform.eulerAngles;
					SaveFile.recin.sS [i].stakesHere [q].stakProp.stakeScale.value = totalChildren [q].transform.localScale;
					if (totalChildren [q].transform.childCount > 0) {
						SaveFile.recin.sS [i].stakesHere [q].stakProp.hasFence = true;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence = new Fence ();
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceName = totalChildren [q].transform.GetChild (0).name;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fencePosition = totalChildren [q].transform.GetChild (0).transform.localPosition;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceAngles = totalChildren [q].transform.GetChild (0).transform.localEulerAngles;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceScale = totalChildren [q].transform.GetChild (0).transform.localScale;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps = new gateProperties ();
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS = new gateScript ();
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.name = totalChildren [q].transform.GetChild (0).name;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.image_path = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().image_path;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.date = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().date;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_type = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_type;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_style = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_style;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_geometry = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_geometry;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.width = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().width;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.height = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().height;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.heightMax = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().heightmax;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.opening = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().opening;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.json = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().json;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.it = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().type;
						SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceRealRot = totalChildren [q].transform.GetChild (0).transform.rotation;
					    
						// Salva la recinzione su file
						string dir = Path.GetDirectoryName (savepath);
						string fenceFilename = totalChildren [q].transform.GetChild (0).name + ".obj";
						totalChildren [q].transform.GetChild (0).eulerAngles = Vector3.zero;
						OBJItem objString = ObjExporting.getObjString (false, totalChildren [q].transform.GetChild (0).gameObject);
						System.IO.File.WriteAllText (dir + "/" + fenceFilename, objString.obj);
						for (int p = 0; p < objString.mtl.Count; p++) {

							System.IO.File.WriteAllText (dir + "/" + objString.mtl_name [p] + ".mtl", objString.mtl [p].ToString ());	

						}
						totalChildren [q].transform.GetChild (0).localEulerAngles = SaveFile.recin.sS [i].stakesHere [q].stakProp.thisFence.fenceAngles;
					}
				}
				int boxNumber = 0;
				for (int x = 0; x < segments [i].transform.childCount; x++) {
					if (segments [i].transform.GetChild (x).tag == "fence") {
						boxNumber++;
					}
				
				}
				int toNextbox = 0;
				SaveFile.recin.sS [i].fbox = new boxRinghiere[boxNumber];
				for (int u = 0; u < segments [i].transform.childCount; u++) {
					if (segments [i].transform.GetChild (u).tag == "fence") {
						SaveFile.recin.sS [i].fbox [toNextbox] = new boxRinghiere ();
						SaveFile.recin.sS [i].fbox [toNextbox].name = segments [i].transform.GetChild (u).name;
						SaveFile.recin.sS [i].fbox [toNextbox].pos = segments [i].transform.GetChild (u).transform.position;
						SaveFile.recin.sS [i].fbox [toNextbox].localRot = segments [i].transform.GetChild (u).transform.localEulerAngles;
						SaveFile.recin.sS [i].fbox [toNextbox].localScale = segments [i].transform.GetChild (u).transform.localScale;
						SaveFile.recin.sS [i].fbox [toNextbox].firstPole = segments [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().firstPole.name;
						SaveFile.recin.sS [i].fbox [toNextbox].secondPole = segments [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().secondPole.name;
						SaveFile.recin.sS [i].fbox [toNextbox].hasFence = segments [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().hasAFence;
						toNextbox++;
					}
					
				}
			} 
		//Spazio dedicato al salvataggio balconi 

		GameObject []segmentsBal=Helper.segmentsOfBalcony(GameObject.FindGameObjectsWithTag("segment"));
		SaveFile.balc=new balconi();

		SaveFile.balc.sS = new segmentStuff[segmentsBal.Length];

		for(int i=0;i<segmentsBal.Length;i++){
			linkProperties lP=segmentsBal[i].GetComponent<linkProperties>();
			SaveFile.balc.sS[i]=new segmentStuff();
			SaveFile.balc.sS[i].segName=segmentsBal[i].name;
			SaveFile.balc.sS [i].segMesh.vertices = new Vector3[segmentsBal [i].GetComponent<MeshFilter> ().mesh.vertices.Length];
			SaveFile.balc.sS [i].segMesh.vertices = storeMatrixVertices (segmentsBal [i].GetComponent<MeshFilter> ().mesh);
			if (lP.attachedSegment)
				SaveFile.balc.sS [i].AttachedSeg = lP.attachedSegment.name;
			if (lP.previousSegment)
				SaveFile.balc.sS [i].prevSeg = lP.previousSegment.name;
			SaveFile.balc.sS [i].segPosition.value = segmentsBal [i].transform.position;
			SaveFile.balc.sS [i].segRotation.value = segmentsBal [i].transform.rotation.eulerAngles;
			SaveFile.balc.sS [i].segScale.value = segmentsBal [i].transform.localScale;
			SaveFile.balc.sS [i].lookAtOb = lP.lookAtOb.name;
			SaveFile.balc.sS [i].SP.isOpening = lP.isOpening;
			if (lP.thisSegmentProperties.Length > 0) {
				SaveFile.balc.sS [i].SP.H = lP.thisSegmentProperties [0];
				SaveFile.balc.sS [i].SP.W = lP.thisSegmentProperties [1];
				SaveFile.balc.sS [i].SP.alfa = lP.thisSegmentProperties [2];
				SaveFile.balc.sS [i].SP.beta = lP.thisSegmentProperties [3];
				SaveFile.balc.sS [i].SP.L = lP.thisSegmentProperties [5];
				SaveFile.balc.sS [i].SP.gamma = lP.thisSegmentProperties [6];
				SaveFile.balc.sS [i].SP.HPrec = lP.thisSegmentProperties [7];
				//Teoricamente se è un balcone sta cosa dei gate non ci dev'essere 
				if (lP.gate)
					SaveFile.balc.sS [i].SP.associatedGate = lP.gate.name;
				else
					SaveFile.balc.sS [i].SP.associatedGate = "no gate";	
			}
			SaveFile.balc.sS [i].SP.thickType = lP.thickTyp;
			int childNumb = segmentsBal [i].transform.childCount;

			SaveFile.balc.sS [i].SC = new segmentChildren [childNumb];
			for (int j = 0; j < childNumb; j++) {
				SaveFile.balc.sS [i].SC [j] = new segmentChildren ();
				GameObject childNode =	segmentsBal [i].transform.GetChild (j).gameObject;
				SaveFile.balc.sS [i].SC [j].nodePos.value = childNode.transform.position;
				SaveFile.balc.sS [i].SC [j].nodeScale.value = childNode.transform.localScale;
				SaveFile.balc.sS [i].SC [j].nodeName = childNode.name;
			}
			int totalStakes = 0;

			for (int d = 0; d < segmentsBal [i].transform.childCount; d++) {
				if (segmentsBal [i].transform.GetChild (d).tag == "stake")
					totalStakes++;
			}
			GameObject[] totalChildren = new GameObject[totalStakes];
			int counter = 0;
			for (int f = 0; f < segmentsBal [i].transform.childCount; f++) {
				if (segmentsBal [i].transform.GetChild (f).tag == "stake") {
					totalChildren [counter] = segmentsBal [i].transform.GetChild (f).gameObject;
					counter++;
				}
			}
			SaveFile.balc.sS [i].stakesHere = new paletti[counter];
			for (int q = 0; q < counter; q++) {
				SaveFile.balc.sS [i].stakesHere [q] = new paletti ();
				SaveFile.balc.sS [i].stakesHere [q].stakProp = new stakesProps ();
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakeName = totalChildren [q].name;
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakePos = new ObjPosition ();
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakeRot = new ObjRotation ();
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakeScale = new ObjScale ();
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakePos.value = totalChildren [q].transform.position;
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakeRot.value = totalChildren [q].transform.eulerAngles;
				SaveFile.balc.sS [i].stakesHere [q].stakProp.stakeScale.value = totalChildren [q].transform.localScale;
				SaveFile.balc.sS [i].stakesHere [q].stakProp.pP = new proprietaPaletti ();
				Debug.Log (totalChildren [q].GetComponent<stakeProperties> () +" "+ totalChildren [q].GetComponent<stakeProperties> ().basePlate+" sepmcdp ,epd,dpeodjemidjieji");
				if (totalChildren [q].GetComponent<stakeProperties> () && totalChildren[q].GetComponent<stakeProperties>().basePlate) {
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.exists = true;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.piastraCollegamentoPos = totalChildren [q].GetComponent<stakeProperties> ().basePlate.transform.position;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.piastraCollegamentoRot = totalChildren [q].GetComponent<stakeProperties> ().basePlate.transform.eulerAngles;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.piastrCollegamentoScale = totalChildren [q].GetComponent<stakeProperties> ().basePlate.transform.localScale;


				}
				if (totalChildren [q].GetComponent<stakeProperties> () && totalChildren [q].GetComponent<stakeProperties> ().link) {
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.exists = true;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoPos = totalChildren [q].GetComponent<stakeProperties> ().link.transform.position;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoRot = totalChildren [q].GetComponent<stakeProperties> ().link.transform.eulerAngles;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.linkCollegamentoScale = totalChildren [q].GetComponent<stakeProperties> ().link.transform.localScale;


				}
				GameObject[] existingHandrails = GameObject.FindGameObjectsWithTag ("handrail");
				if (existingHandrails.Length > 0) {
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.handrail = new corrimano ();
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.handrail.exists = true;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.handrail.height = existingHandrails [0].transform.localScale.z;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.pP.handrail.width = existingHandrails [0].transform.localScale.y;
				}
				if (totalChildren [q].transform.childCount > 0) {
					SaveFile.balc.sS [i].stakesHere [q].stakProp.hasFence = true;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence = new Fence ();
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceName = totalChildren [q].transform.GetChild (0).name;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fencePosition = totalChildren [q].transform.GetChild (0).transform.localPosition;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceAngles = totalChildren [q].transform.GetChild (0).transform.localEulerAngles;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceScale = totalChildren [q].transform.GetChild (0).transform.localScale;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps = new gateProperties ();
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS = new gateScript ();
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.name = totalChildren [q].transform.GetChild (0).name;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.image_path = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().image_path;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.date = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().date;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_type = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_type;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_style = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_style;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.id_geometry = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().id_geometry;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.width = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().width;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.height = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().height;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.heightMax = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().heightmax;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.opening = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().opening;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.json = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().json;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceProps.gS.it = totalChildren [q].transform.GetChild (0).GetComponent<ItemGate> ().type;
					SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceRealRot = totalChildren [q].transform.GetChild (0).transform.rotation;
					//Se il paletto ha lo script attaccato "stakeProps", ci salviamo l'informazione su un' eventuale piastrina di  collegamneto
			

					// Salva la recinzione su file
					string dir = Path.GetDirectoryName (savepath);
					string fenceFilename = totalChildren [q].transform.GetChild (0).name + ".obj";
					totalChildren [q].transform.GetChild (0).eulerAngles = Vector3.zero;
					OBJItem objString = ObjExporting.getObjString (false, totalChildren [q].transform.GetChild (0).gameObject);
					System.IO.File.WriteAllText (dir + "/" + fenceFilename, objString.obj);
					for (int p = 0; p < objString.mtl.Count; p++) {

						System.IO.File.WriteAllText (dir + "/" + objString.mtl_name [p] + ".mtl", objString.mtl [p].ToString ());	

					}
					totalChildren [q].transform.GetChild (0).localEulerAngles = SaveFile.balc.sS [i].stakesHere [q].stakProp.thisFence.fenceAngles;
				}
			}
			int boxNumber = 0;
			for (int x = 0; x < segmentsBal [i].transform.childCount; x++) {
				if (segmentsBal [i].transform.GetChild (x).tag == "fence") {
					boxNumber++;
				}

			}
			int toNextbox = 0;
			SaveFile.balc.sS [i].fbox = new boxRinghiere[boxNumber];
			for (int u = 0; u < segmentsBal [i].transform.childCount; u++) {
				if (segmentsBal [i].transform.GetChild (u).tag == "fence") {
					SaveFile.balc.sS [i].fbox [toNextbox] = new boxRinghiere ();
					SaveFile.balc.sS [i].fbox [toNextbox].name = segmentsBal [i].transform.GetChild (u).name;
					SaveFile.balc.sS [i].fbox [toNextbox].pos = segmentsBal [i].transform.GetChild (u).transform.position;
					SaveFile.balc.sS [i].fbox [toNextbox].localRot = segmentsBal [i].transform.GetChild (u).transform.localEulerAngles;
					SaveFile.balc.sS [i].fbox [toNextbox].localScale = segmentsBal [i].transform.GetChild (u).transform.localScale;
					SaveFile.balc.sS [i].fbox [toNextbox].firstPole = segmentsBal [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().firstPole.name;
					SaveFile.balc.sS [i].fbox [toNextbox].secondPole = segmentsBal [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().secondPole.name;
					SaveFile.balc.sS [i].fbox [toNextbox].hasFence = segmentsBal [i].transform.GetChild (u).GetComponent<fenceBoxScript> ().hasAFence;
					toNextbox++;
				}

			}
		}







			
				

				


	
			
				
			





		//Fine spazio dedicato al salvataggio baconi
		//salva ogni ringhiera condivisa tra due segmenti e salvala con tutto ciò che serve per il richiamo.
		GameObject [] allfences=GameObject.FindGameObjectsWithTag("fence");
		int efCounter=0;
		int realEfCounter=0;
		for(int j=0;j<allfences.Length;j++){
			if(allfences[j].transform.parent==null)
				efCounter++;
		}
		GameObject[] midFences=new GameObject [efCounter];
		for(int l=0;l<allfences.Length;l++)
			if(allfences[l].transform.parent==null){
				midFences[realEfCounter]=allfences[l];
				realEfCounter++;

			}
	
		SaveFile.intR = new interRecinzioni ();
		SaveFile.intR.fF = new freeFences[midFences.Length];
			for(int g=0;g<midFences.Length;g++){
			SaveFile.intR.fF [g] = new freeFences ();
			SaveFile.intR.fF [g].fenceName = midFences [g].name;
			SaveFile.intR.fF [g].fencePosition = midFences [g].transform.position;
			SaveFile.intR.fF [g].fenceRotation = midFences [g].transform.eulerAngles;
			SaveFile.intR.fF [g].fenceScale = midFences [g].transform.localScale;
			SaveFile.intR.fF [g].hasAFence = midFences [g].GetComponent<fenceBoxScript> ().hasAFence;
			SaveFile.intR.fF [g].firstStack = midFences [g].GetComponent<fenceBoxScript> ().firstPole.name;
			SaveFile.intR.fF [g].secondStack = midFences [g].GetComponent<fenceBoxScript> ().secondPole.name;

			if(midFences [g].GetComponent<fenceBoxScript> ().myfence!=null)
			SaveFile.intR.fF [g].myFence = midFences [g].GetComponent<fenceBoxScript> ().myfence.name;

			SaveFile.intR.fF [g].firstSegment = midFences [g].GetComponent<fenceBoxScript> ().firstPole.transform.parent.name;
			SaveFile.intR.fF [g].secondSegment = midFences [g].GetComponent<fenceBoxScript> ().secondPole.transform.parent.name;
			if (midFences [g].GetComponent<fenceBoxScript> ().myfence) {
			GameObject savableFence= midFences [g].GetComponent<fenceBoxScript> ().myfence;
				SaveFile.intR.fF [g].fenceObjPos = savableFence.transform.position;
				SaveFile.intR.fF [g].fenceObjRot = savableFence.transform.eulerAngles;
				SaveFile.intR.fF [g].fenceObjScale = savableFence.transform.localScale;
				string dir=Path.GetDirectoryName(savepath);
				string fenceFilename = savableFence.name+".obj";
				savableFence.transform.eulerAngles = Vector3.zero;
				OBJItem objString = ObjExporting.getObjString (false, savableFence.gameObject);
				System.IO.File.WriteAllText(dir+"/"+fenceFilename, objString.obj);
				for(int p=0;p<objString.mtl.Count;p++){

					System.IO.File.WriteAllText(dir+"/"+objString.mtl_name[p]+".mtl", objString.mtl[p].ToString());	

				}
				savableFence.transform.eulerAngles = SaveFile.intR.fF [g].fenceObjRot;
					SaveFile.intR.fF [g].gS = new gateScript ();
					SaveFile.intR.fF [g].gS.id =savableFence.GetComponent<ItemGate> ().id;
				SaveFile.intR.fF [g].gS.name = savableFence.GetComponent<ItemGate> ().name;
				SaveFile.intR.fF [g].gS.image_path = savableFence.GetComponent<ItemGate> ().image_path;
				SaveFile.intR.fF [g].gS.date =  savableFence.GetComponent<ItemGate> ().date;
				SaveFile.intR.fF [g].gS.id_type = savableFence.GetComponent<ItemGate> ().id_type;
				SaveFile.intR.fF [g].gS.id_style = savableFence.GetComponent<ItemGate> ().id_style;
				SaveFile.intR.fF [g].gS.id_geometry = savableFence.GetComponent<ItemGate> ().id_geometry;
				SaveFile.intR.fF [g].gS.width = savableFence.GetComponent<ItemGate> ().width;
				SaveFile.intR.fF [g].gS.height =  savableFence.GetComponent<ItemGate> ().height;
				SaveFile.intR.fF [g].gS.heightMax =  savableFence.GetComponent<ItemGate> ().heightmax;
				SaveFile.intR.fF [g].gS.opening =  savableFence.GetComponent<ItemGate> ().opening;
				SaveFile.intR.fF [g].gS.json =  savableFence.GetComponent<ItemGate> ().json;
				SaveFile.intR.fF [g].gS.it = savableFence.GetComponent<ItemGate> ().type;

	
			}

			}

		//Trovo quali interbox hanno una recinzione associata, e me la salvo.


		//Fine della parte che salva le inter recinzioni

		/*
		 * Inizio della parte che gestisce il salvataggio delle grate
		 */

		GameObject[] allWindows = GameObject.FindGameObjectsWithTag ("window");
		SaveFile.gra = new grate ();
		//SaveFile.gra.gS = new grateStuff ();
		SaveFile.gra.gS = new grateStuff[allWindows.Length];

		for(int gr=0;gr<allWindows.Length;gr++){
			if (allWindows [gr].transform.childCount > 0) {
				//SONO QUA 
				SaveFile.gra.gS[gr]=new grateStuff();
				SaveFile.gra.gS [gr].pG = new proprietaGrate ();
				SaveFile.gra.gS[gr].pG.D=allWindows[gr].GetComponent<adaptiveBezel>().D;
				SaveFile.gra.gS [gr].pG.cws = allWindows [gr].GetComponent<adaptiveBezel> ().cws;
				SaveFile.gra.gS [gr].pG.cwi = allWindows [gr].GetComponent<adaptiveBezel> ().cwi;
				SaveFile.gra.gS [gr].pG.H = allWindows [gr].GetComponent<adaptiveBezel> ().H;
				SaveFile.gra.gS [gr].pG.L = allWindows [gr].GetComponent<adaptiveBezel> ().L;
				SaveFile.gra.gS [gr].pG.pxl = allWindows [gr].GetComponent<adaptiveBezel> ().pxl;
				SaveFile.gra.gS [gr].pG.pxr = allWindows [gr].GetComponent<adaptiveBezel> ().pxr;
				SaveFile.gra.gS [gr].pG.pys = allWindows [gr].GetComponent<adaptiveBezel> ().pys;
				SaveFile.gra.gS [gr].pG.pyi = allWindows [gr].GetComponent<adaptiveBezel> ().pyi;
				if (allWindows [gr].GetComponent<adaptiveBezel> ().grateOb != null)
					SaveFile.gra.gS [gr].pG.grateName = allWindows [gr].GetComponent<adaptiveBezel> ().grateOb.name;
				else
					SaveFile.gra.gS [gr].pG.grateName = "none";
			//	SaveFile.gra.gS[gr].upperBezelPos=allWindows[gr].transform.Find 

			}
		}



		//salva ogni cancello che si trova nella scena
		GameObject[] gates = GameObject.FindGameObjectsWithTag ("gate");
		SaveFile.canc.gP = new gateProperties[gates.Length];
	
		for (int z = 0; z < gates.Length; z++) {
			string filename = gates [z].name + ".obj";
			SaveFile.canc.gP [z] = new gateProperties ();
			SaveFile.canc.gP [z].gateName = gates [z].name;
			SaveFile.canc.gP [z].gatePos = new ObjPosition (); 
			SaveFile.canc.gP [z].gateRot = new ObjRotation (); 
			SaveFile.canc.gP [z].gateScale = new ObjScale (); 

			SaveFile.canc.gP [z].gatePos.value  = gates [z].transform.position;
			SaveFile.canc.gP [z].gateRot.value = gates [z].transform.eulerAngles;
			SaveFile.canc.gP [z].gateScale.value = gates [z].transform.localScale;
			SaveFile.canc.gP [z].filename = filename;
			SaveFile.canc.gP [z].gS = new gateScript ();
			SaveFile.canc.gP [z].gS.id = gates [z].GetComponent<ItemGate> ().id;
			SaveFile.canc.gP [z].gS.name = gates [z].GetComponent<ItemGate> ().name;
			SaveFile.canc.gP [z].gS.image_path  = gates [z].GetComponent<ItemGate> ().image_path ;
			SaveFile.canc.gP [z].gS.date = gates [z].GetComponent<ItemGate> ().date;
			SaveFile.canc.gP [z].gS.id_type = gates [z].GetComponent<ItemGate> ().id_type;
			SaveFile.canc.gP [z].gS.id_style= gates [z].GetComponent<ItemGate> ().id_style;
			SaveFile.canc.gP [z].gS.id_geometry = gates [z].GetComponent<ItemGate> ().id_geometry;
			SaveFile.canc.gP [z].gS.width = gates [z].GetComponent<ItemGate> ().width;
			SaveFile.canc.gP [z].gS.height = gates [z].GetComponent<ItemGate> ().height ;
			SaveFile.canc.gP [z].gS.heightMax  = gates [z].GetComponent<ItemGate> ().heightmax ;
			SaveFile.canc.gP [z].gS.opening = gates [z].GetComponent<ItemGate> ().opening;
			SaveFile.canc.gP [z].gS.json = gates [z].GetComponent<ItemGate> ().json;
			SaveFile.canc.gP [z].gS.it = gates [z].GetComponent<ItemGate> ().type;

			SaveFile.canc.gP [z].gS.colWidthL  = gates [z].GetComponent<ItemGate> ().columnWidthL ;
			SaveFile.canc.gP [z].gS.colWidthR  = gates [z].GetComponent<ItemGate> ().columnWidthR ;
			SaveFile.canc.gP [z].gS.colDepthL  = gates [z].GetComponent<ItemGate> ().columnDepthL ;
			SaveFile.canc.gP [z].gS.colDepthR  = gates [z].GetComponent<ItemGate> ().columnDepthR ;
			SaveFile.canc.gP [z].gS.capWidthL  = gates [z].GetComponent<ItemGate> ().capitalWidthL ;
			SaveFile.canc.gP [z].gS.capWidthR  = gates [z].GetComponent<ItemGate> ().capitalWidthR ;
			SaveFile.canc.gP [z].gS.capDepthL  = gates [z].GetComponent<ItemGate> ().capitalDepthL ;
			SaveFile.canc.gP [z].gS.capDepthR  = gates [z].GetComponent<ItemGate> ().capitalDepthR ;
			SaveFile.canc.gP [z].gS.capHeightL = gates [z].GetComponent<ItemGate> ().capitalHeightL;
			SaveFile.canc.gP [z].gS.capHeightR = gates [z].GetComponent<ItemGate> ().capitalHeightR;















			//esporta il cancello in obj
			string dir=Path.GetDirectoryName(savepath);

			OBJItem objString = ObjExporting.getObjString (false,  gates [z]);
			System.IO.File.WriteAllText(dir+"/"+filename, objString.obj);

			for(int i=0;i<objString.mtl.Count;i++){

				System.IO.File.WriteAllText(dir+"/"+objString.mtl_name[i]+".mtl", objString.mtl[i].ToString());	
			}


		
		}



		//string filePath= FileBrowser.SaveFile("Save File", "", "MySaveFile", extensions);
		string json = JsonUtility.ToJson(SaveFile);

		System.IO.File.WriteAllText (savepath, json);
	}  
	//QUESTA PARENTESI CHIUDE IL METODO DI SALVATAGGIO





	Vector3[] storeMatrixVertices(Mesh ms){
		
		Vector3[] vertices = new Vector3[ms.vertices.Length];
		for (int i = 0; i < vertices.Length; i++) {
			vertices [i] = ms.vertices [i];


		

		}
		return vertices;

		}



	IEnumerator waitForDestruction(string filePath){
		yield return 0;
		// Read the json from the file into a string
		string dataAsJson = File.ReadAllText(filePath); 
		// Pass the json to JsonUtility, and tell it to create a GameData object from it
		loadedData = JsonUtility.FromJson<JSonFile>(dataAsJson);
		GameObject[] delSeg=GameObject.FindGameObjectsWithTag ("segment");
		for (int i = 0; i < delSeg.Length; i++)
			Destroy (delSeg [i]);
		ExperimCmd.segmentsTotal = 0;
		// Retrieve the allRoundData property of loadedData
		string version = loadedData.version;
		instantiateSavedSegments (filePath);
		instantiateSavedSegmentsBalcony (filePath);
		instantiateSavedGates (filePath);

		instantiateInterFences (filePath);
		instantiateGrates (filePath);

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (1) && ortoCam.activeInHierarchy) {
			Ray ray = GameObject.Find ("Main Camera").GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
			RaycastHit info;
			if (Physics.Raycast (ray, out info))
			if (info.collider.tag == "fence") {
				if (info.transform.GetComponent<fenceBoxScript> ().hasAFence == false) {

					StaticVariables.firstPole = info.collider.GetComponent<fenceBoxScript> ().firstPole.name;
					if (!info.collider.name.Contains ("BoxinterFence")) {
						StaticVariables.relativeSegment = info.collider.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
					}
								else
									StaticVariables.relativeSegment = "inter_"+info.collider.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
								
			
					info.collider.GetComponent<fenceBoxScript> ().hasAFence = true;
					openMyFence (info.collider.gameObject,false);
				} else {
					provvFenceBox = info.collider.gameObject;
					fenceAlert.SetActive (true);
				}
			}
			if (info.collider.tag == "grate") {
				if (info.transform.parent.GetComponent<adaptiveBezel>().hasGrate==false) {

		


					info.collider.transform.parent.GetComponent<adaptiveBezel> ().hasGrate = true;
					openMyFence (info.collider.gameObject,false);
				} else {
                 /*qua c'è da aggiungere
                  * la logica relativa al
                  * caso in cui è già presente una grata
                  * e si voglia modificare l'ambaradan
                  */

				}
			}
		}
	}
	public void CallToAction(int chose){
		switch (chose) {
		case 0:
			SaveSingleFile ();
			break;
		case 1:
			OpenSingleFile ();
			break;
		case 2:
			Debug.Log ("Case2");
			break;
		}
	
	}
	public void OpenSingleFile() {
		string extensions = "json";        
		string path = FileBrowser.OpenSingleFile("Open File", "", extensions);
		load (path);
	}
	public void SaveSingleFile(){
		string extensions = "json";
		string path = FileBrowser.SaveFile ("Save File", "","NewProject", extensions);
		save (path);
	}
	public void InsertGate(){
		
		GameObject segment = GameObject.Find (segmentOfGate);



		Ecmd.setProperties ();
		segment.GetComponent<linkProperties> ().isOpening = true;

		//verifica che sul segment c'è un cancello collegato
		GameObject gate=segment.GetComponent<linkProperties>().gate;
		if (gate != null) {
			//Qua deve aprire la finestra di alert tra nuovo e modifica
			edit_new_win.SetActive(true);

		}else
			openMyGate ("wizard");

	}
	//Qua creo un nuovo cancello. Se esiste, cancello il precedente 
	void openMyGate(string scene){
		GameObject segment = GameObject.Find (segmentOfGate);
	

			//Qua dovremmo cancellare un cancello pre esistente, se esiste. perchè fanculo noi ne vogliamo uno nuovo e più bello, Dio Caro
			if (segment.GetComponent<linkProperties> ().gate != null) {
				string gatename = segment.GetComponent<linkProperties> ().gate.name;
				GameObject oggettoProvvisorio = new GameObject ();
				oggettoProvvisorio.name = "oggettoProvvisorio";
				//GameObject.Find (gatename).transform.parent = oggettoProvvisorio.transform;
				DestroyImmediate (GameObject.Find (gatename));
			}
			
	
			
				ExperimCmd.segmentsTotal = 0;
				if (segment.GetComponent<linkProperties> ().isOpening) {
					gateLength = segment.GetComponent<linkProperties> ().thisSegmentProperties [5];
					gateHeight = segment.GetComponent<linkProperties> ().thisSegmentProperties [0];

				//	SceneManager.LoadScene (scene);
			if(scene=="wizard")
				StartCoroutine (LoadWizardScene ());
			if (scene == "design")
				StartCoroutine (LoadGateScene ());
			
				}
	
			
	}
	IEnumerator LoadGateScene(){

		yield return 0;
		string firstPart = Application.dataPath;
		save (firstPart + "/provv.json");
		SceneManager.LoadScene ("design");
	}
	IEnumerator LoadWizardScene(){
		yield return 0;
			
		string firstPart = Application.dataPath;
		save (firstPart + "/provv.json");
		SceneManager.LoadScene("wizard");
	}

	/// <summary>
	/// Se scelgo di modificare un cancello già esistente, allora procedo chiamando la funzuioe sotto 
	/// </summary>
			public  void editExistingGate(){
		GameObject segment = GameObject.Find (segmentOfGate);



		Ecmd.setProperties ();
		segment.GetComponent<linkProperties> ().isOpening = true;

		//verifica che sul segment c'è un cancello collegato
		GameObject gate=segment.GetComponent<linkProperties>().gate;
				webservice wb = webservice.InitWebService ();
				StartCoroutine (
					wb.downloadGateFromDB (gate.GetComponent<ItemGate>().id, (g) => {
						openMyGate ("design");

			
					}, (error) => {
						Debug.Log (error);
					}));
			}

	public void newGateFromAlert(){
		openMyGate ("wizard");
	}
	void openMyFence( GameObject fenceBox,bool justEdit){


		ExperimCmd.segmentsTotal = 0;
		if (!fenceBox.name.Contains("BoxinterFence")) {
			gateLength = fenceBox.transform.localScale.z * fenceBox.transform.parent.localScale.x;
			gateHeight = fenceBox.transform.localScale.y * fenceBox.transform.parent.localScale.z;
		} else {
			gateLength = fenceBox.transform.localScale.z ;
			gateHeight = fenceBox.transform.localScale.y ;
		}
		StaticVariables.firstPole = fenceBox.GetComponent<fenceBoxScript> ().firstPole.name;
		StaticVariables.fencePosition = fenceBox.transform.position;
		StaticVariables.comingFromFence = true;

//		fencePosition=

			//	SceneManager.LoadScene (scene);
		if (!justEdit)
			StartCoroutine (LoadWizardScene ());
		else
			StartCoroutine (LoadGateScene ());


		}
	/// <summary>
	/// Trova il valore di distorsione da applicare alla recinzione, basato sull'inclinazione del segmento
	/// </summary>
	public void fenceDistortionInterpolator(GameObject distFence,float targetAngle){
		bool invertDir = false;
		if (targetAngle < 0)
			invertDir = true;
		targetAngle = Mathf.Abs (targetAngle);
		float incrementalValue = 0;
		float realAngle = 0;
		GameObject pivotPoint = new GameObject ();
		pivotPoint.transform.parent = distFence.transform;
		pivotPoint.transform.localPosition = Vector3.zero;
		pivotPoint.transform.localScale = Vector3.one;
		pivotPoint.transform.localEulerAngles = Vector3.zero;
		GameObject lookPoint = new GameObject ();
		lookPoint.transform.parent = distFence.transform ;
		lookPoint.transform.localScale = Vector3.one;
		lookPoint.transform.localPosition = new Vector3 (5000, 0, 0);
		lookPoint.transform.localEulerAngles = Vector3.zero;
		pivotPoint.transform.LookAt (lookPoint.transform.position);
		GameObject pivotPointReplica = new GameObject ();
		pivotPointReplica.transform.position = pivotPoint.transform.position;
		GameObject lookPointReplica = new GameObject ();
		lookPointReplica.transform.position = lookPoint.transform.position;
	
		while (Mathf.Abs (realAngle - targetAngle) > 0.2f) {
			distFence.transform.localEulerAngles = new Vector3 (distFence.transform.localEulerAngles.x, distFence.transform.localEulerAngles.y,incrementalValue);
			lookPointReplica.transform.position = lookPoint.transform.position;
				incrementalValue += 0.001f;
		      			pivotPointReplica.transform.LookAt (lookPointReplica.transform.position);
			realAngle = pivotPointReplica.transform.transform.localEulerAngles.x;
			}
			

	
	  		realAngle = pivotPointReplica.transform.transform.localEulerAngles.x;
		if (targetAngle != 0) {
			if (!invertDir)
				distFence.transform.localScale = new Vector3 (-distFence.transform.localScale.x,	-distFence.transform.localScale.y,	distFence.transform.localScale.z);
			else {
				distFence.transform.localScale = new Vector3 (distFence.transform.localScale.x,	-distFence.transform.localScale.y,	-distFence.transform.localScale.z);
				distFence.transform.localEulerAngles = new Vector3 (distFence.transform.localEulerAngles.x, 0, distFence.transform.localEulerAngles.z);
			}
		} else {
			if (distFence.transform.localEulerAngles.z > 50 || distFence.transform.localEulerAngles.z < -50)
				distFence.transform.localEulerAngles = new Vector3 (distFence.transform.localEulerAngles.x, distFence.transform.localEulerAngles.y, 180);
		}
		}
	    
	public void editExistingFence(){
		
	//	fence.name = "recin_" + originStake.name + originStake.transform.parent.name;
		string fenceName="";
		if (provvFenceBox.transform.parent != null)
			fenceName = "recin_" + provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.name + provvFenceBox.transform.parent.name;
		else
			fenceName = provvFenceBox.GetComponent<fenceBoxScript> ().myfence.name;
		StaticVariables.gate = GameObject.Find (fenceName).GetComponent<ItemGate> ();
		GameObject toDestroy=null;
		if (provvFenceBox.transform.parent != null) {
			for (int i = 0; i < provvFenceBox.transform.parent.childCount; i++) {
				if (provvFenceBox.transform.parent.GetChild (i).childCount > 0 && provvFenceBox.transform.parent.GetChild (i).tag == "stake")
				if (provvFenceBox.transform.parent.GetChild (i).transform.GetChild (0).name == fenceName)
					toDestroy = provvFenceBox.transform.parent.GetChild (i).transform.GetChild (0).gameObject;
				
				
			}
		} else {
			toDestroy = GameObject.Find ("fenceName");  //Molto piu semplice in questo caso, perchè non devo cercare tra figli e figliastri
		}
		
		Destroy (toDestroy);
		openMyFence (provvFenceBox,true);
		StaticVariables.firstPole = provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.name;
		if (!provvFenceBox.name.Contains  ( "BoxinterFence"))
			StaticVariables.relativeSegment = provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
		else
						StaticVariables.relativeSegment = "inter_"+provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
		provvFenceBox.GetComponent<fenceBoxScript> ().hasAFence = true;

	}
	public void createNewFence(){
		string fenceName="recin_"+provvFenceBox.GetComponent<fenceBoxScript>().firstPole.name+provvFenceBox.transform.parent.name;
		GameObject toDestroy = provvFenceBox.transform.Find (fenceName).gameObject ;
		Destroy (toDestroy);
		openMyFence (provvFenceBox,false);
		StaticVariables.firstPole = provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.name;
		if (!provvFenceBox.name.Contains ( "BoxinterFence"))
			StaticVariables.relativeSegment = provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
		else
						StaticVariables.relativeSegment =  "inter_"+provvFenceBox.GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject.name;
		provvFenceBox.GetComponent<fenceBoxScript> ().hasAFence = true;

	}
	public void undoNewFence(){
		fenceAlert.SetActive (false);
	}
	public void deleteFence(){
		string fenceName="recin_"+provvFenceBox.GetComponent<fenceBoxScript>().firstPole.name+provvFenceBox.transform.parent.name;
		GameObject toDestroy=null;
		for (int i = 0; i < provvFenceBox.transform.parent.childCount; i++) {
			if (provvFenceBox.transform.parent.GetChild (i).childCount > 0 && provvFenceBox.transform.parent.GetChild (i).tag == "stake")
			if (provvFenceBox.transform.parent.GetChild (i).transform.GetChild (0).name == fenceName) {
				provvFenceBox.GetComponent<fenceBoxScript> ().hasAFence = false;
				toDestroy = provvFenceBox.transform.parent.GetChild (i).transform.GetChild (0).gameObject;
			}


		}

		Destroy (toDestroy);
		fenceAlert.SetActive (false);

		
	}

	}
	




