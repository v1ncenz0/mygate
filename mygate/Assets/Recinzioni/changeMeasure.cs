﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeMeasure : MonoBehaviour {
	 TextMesh thisLength;
	Transform parentTr;
	// Use this for initialization
	void Start () {
		thisLength = GetComponent<TextMesh> ();
		parentTr = transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
		if (parentTr.localScale.x != 0) {
			thisLength.text = string.Format ("{0:0.00}", parentTr.localScale.x);
			if (parentTr.eulerAngles.y > 90 && parentTr.eulerAngles.y < 270) {
				transform.localScale = new Vector3 (1 / parentTr.localScale.x, 1 / parentTr.localScale.y, 1 / parentTr.localScale.z);
				Debug.Log ("thisscase");
			}
			else
				transform.localScale = new Vector3 (-1 / parentTr.localScale.x, -1 / parentTr.localScale.y, 1 / parentTr.localScale.z);
		}
	}
}
