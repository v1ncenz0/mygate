﻿using UnityEngine;
using System.Collections.Generic;
public static class Helper {
	public static GameObject [] FindComponentsInChildrenWithTag<T>(this GameObject parent, string tag, bool forceActive = false) where T : Component
	{
		if(parent == null) { throw new System.ArgumentNullException(); }
		if(string.IsNullOrEmpty(tag) == true) { throw new System.ArgumentNullException(); }
		List<T> list = new List<T>(parent.GetComponentsInChildren<T>(forceActive));
		if(list.Count == 0) { return null; }

		for(int i = list.Count - 1; i >= 0; i--) 
		{
			if (list[i].CompareTag(tag) == false)
			{
				list.RemoveAt(i);
			}
		}
		//return list.ToArray();
		GameObject[] returnables = new GameObject[list.Count];
		for (int i = 0; i < list.Count; i++)
			returnables [i] = list [i].gameObject;
		return returnables;
	}
	public static GameObject[] reOrderedChildrens(GameObject[] unordered){
		GameObject[] ordered = new GameObject[unordered.Length];
		for (int i = 0; i < ordered.Length; i++) {
			for (int j = 0; j < ordered.Length; j++) {
				if (unordered [j].name == i.ToString())
					ordered [i] = unordered [j];
			}
		}

		return ordered;
	}
	public static GameObject[] segmentsNotBalcony(GameObject []allSegments){
		List<GameObject> addable = new List<GameObject> ();
		for (int i = 0; i < allSegments.Length; i++) {
			if (!allSegments [i].GetComponent<linkProperties> ().isBalcony)
				addable.Add (allSegments [i]);
		}
		return addable.ToArray ();
	}
	public static GameObject[] segmentsOfBalcony(GameObject[]allSegments){
		List<GameObject> addable = new List<GameObject> ();
		for (int i = 0; i < allSegments.Length; i++) {
			if (allSegments [i].GetComponent<linkProperties> ().isBalcony)
				addable.Add (allSegments [i]);
		}
		return addable.ToArray ();
	}
	
}