﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MaterialUI;
using TriangleNet.Geometry;


public class ExperimCmd : MonoBehaviour {
	public static int segmentsTotal=0; //Quanti segmenti sono stati instanziati 

	private float defaultHeight=0.5f;
	public static float defaultDepth=0.3f;

	[Header("GiàRecintatoAlert")]
	public GameObject stakeAlert;
	[Header("Le camere utilizzate")]
	public GameObject ortoCam;
	public GameObject perspCam;
	private Camera Cameramain;
	[Header("Elementi UI Principali")]
	public Text suggestionLabel;
	public GameObject[] createSuggestionCommands;
	public GameObject[] editSuggestionCommands;
	public GameObject propWindowCluster; //Il parent che contiene tutto ciò correlato alla finestra proprietà
	public GameObject propWindowClusterBalcony;//Finestra proprietà relativa ai balconi.
	public float scrollingSpeed=.01f; //Velocità con cui il mouse scrolla nell'editor.

	[Header("Finestra proprietà Recinzione")]
	public Text heightTag;
	public Text widthOneTag;
	public Text supAngleTag;
	public Text infAngleTag;
	public Text lengthTag,absAngleTag; //Lunghezza e angolo (in pianta) del segmento
	public Text prevHeightTag;
	public Text h, wOne, alfa, beta,l,gamma,hfin;
	public GameObject isGateButt;
	public GameObject isFenceButt;
	public GameObject sxColumnButt;
	public GameObject dxColumnButt;
	public GameObject sxColumnWin;
	public GameObject dxColumnWin;
	public Text[] hintTextSx;
	public Text[] hintTextDx;
	public Text[] impTextSx;
	public Text[] impTextDx;
	[Header("Finestrà proprietà Balcone")]
	public Text heightTagBal;
	public Text widthOneTagBal;
	public Text supAngleTagBal;
	public Text infAngleTagBal;
	public Text lengthTagBal, absAngleTagBal;
	public Text prevHeightTagBal;
	public Text hBal, wOneBal, alfaBal, betaBal, lBal, gammaBal, hfinBal;
	[Header("Materiali per segmenti")]
	public Material outlinedMat;
	public Material normalMat;
	int workPhase; //0 Creazione nuovi segmenti, 1 modifica di segmenti, 2 modalità showcase
	[Header("Stato switch Spessore")]
	public MaterialDropdown MDd;
	[Header("Caselle di testo")] //le prendo per resettarle
	public MaterialInputField[] MIF;
	[Header("Sprites Finestre proprietà")]
	public Sprite[] wall_gate;
	public Image UIExplanatory;
	public Toggle gateOnOff;
	[Header("Stakes Handler")]
	public stakesHandling stakHand;
	[HideInInspector]
	public GameObject segOne;
	[HideInInspector]

	public GameObject segTwo;
	[HideInInspector]

	public float supSlop;
	[HideInInspector]
    public float infSlop;
	private bool instantiated=false; //Controlla se siamo nell'istante della creazione del nuovo segmento, o nella fase di drag.
	GameObject inEditingSegment; //Il segmento che si sta modificando attualmente
	GameObject inEditingNode; //Questo è il nodo 2 che "segue il mouse" quando il segmento viene trascinato;
	private Vector3 segmentOrigin; //Il punto in cui il segmento viene istanziato
	private Vector3 originMousePosition;
	bool createFromNode=false; //Questo segmento, ha origine nel nodo di un altro segmento? Se sì, richiamo weldVertices. Se no, salto la procedura di fusione .
	GameObject snappedNode; //Se per fare un segmento partiamo da un nodo, questo nodo viene memorizzato qui per richiamare il segmento di cui è figlio nella fase di welding.
	VertexWelder VW;
	[HideInInspector]

	public List <Transform> segPositions= new List<Transform>(); //Questo vettore viene po
	[HideInInspector]

	public List <Transform> nodePositions=new List<Transform>();
	bool editingExisting;
	GameObject secondSeg; //Viene utilizzato quando si sta rieditando un nodo tra due segmenti esistenti. memorizza il segmento successivo al nodo preso in considerazione
	private bool cicleOnce=false;

	private float oldX; //Utilizzati per la funzione scroll in editor;
	private float oldZ;
	private GameObject selectedSegmentForEditing; //il segmento che si sta editando 
	private float NoEditPercentageInf;
	private float NoEditPercentageSup; //Questi due valori sono i valori per i quali, ad una certa altezza dello schermo, cliccare col mouse non inserirà nuovi segmenti.
	//bool finishOnNodeCong=false; // Questa booleana è true se il segmento che si sta creando, va a congiungersi col nodo 1 di un segmento preesistente;
	private bool hideAngleDims=true;
	private bool hideStraightDims =true;
	private bool hideNodes=false;
	private float editPercentageOrig; //Qui salvo la variabile relativa alla porzione inferiore dello schermo in cui non è possibile interagire con i segmenti (per la presenza della finestra inferiore);
	private int precWorkPhase; //quando attivo la visuale prospettica, salvo la modalità di utilizzo immediatamente precedente per ripristinarla in un secondo tempo 
	private bool canWriteMeasure; //booleana per capire se posso modificare direttamente la misura dalla textmesh
	private bool canWriteAngle;   // //booleana per capire se posso modificare direttamente l'angolo dalla textmesh
	private Vector3 endPoint; //vettore che mi serve per l'editing del primo segmento
	// Use this for initialization
	private int thicknessBias=0; //0 Simmetrico, 1 bias sinistro, 2 bias destro
	private bool isGate_state=false;
	private bool hasShiftedForDragging=false; //Controllore per evitare che il sisetema fallisca quando un utente rilascia il tasto shift prima di aver rilasciato il segmento trascinato
	private SaveLoad SLScript;


	//Variabili a seguire relative all'estrusione 
	public int segments = 1;
	public bool invertFaces = false;

	public float extrusionLength = 1.0f;

	public bool autoCalculateOrientation = true;

	private GameObject buildingPlane;
	private Mesh srcMesh;
	private MeshExtrusion.Edge[] precomputedEdges;
	void Start () {
		buildingPlane = GameObject.Find ("BuildingPlane");
		SLScript = GameObject.Find ("SaveLoadHandler").GetComponent<SaveLoad> ();

		editPercentageOrig = NoEditPercentageInf;
		VW = new VertexWelder ();
		VW.Init ();
		Cameramain = ortoCam.GetComponent<Camera> ();
	}
	public void selectCreateMode(){
		workPhase = 0;
		SetAllSegmentsInitialMat ();
	}
	public void selectEditMode(){

		workPhase = 1;
	}
	/// <summary>
	/// Qui scelgo se utilizzare la camera prospettica o quella ortografica
	/// </summary>
	public void selectCamMode(){

		//if (workPhase != 2) {
		if(ortoCam.activeInHierarchy){
			//precWorkPhase = workPhase;
			//workPhase = 2;
			Cameramain=perspCam.GetComponent<Camera>();
			perspCam.SetActive (true);
			ortoCam.SetActive (false);

		} else {
			//workPhase = precWorkPhase;
			Cameramain=ortoCam.GetComponent<Camera>();
			perspCam.SetActive (false);
			ortoCam.SetActive (true);
		}
	}

	public void DiscardChanges(){
		GameObject[] segments=GameObject.FindGameObjectsWithTag("segment");
		for (int i = 0; i < segments.Length; i++) {
			segments [i].GetComponent<MeshRenderer> ().material = normalMat;
		}
		selectedSegmentForEditing = null;
		workPhase = 0;
		resetInputFields ();
	}
	/// <summary>
	/// Quando un segmento viene selezionato per l'editing, diventando verde, giro in questa funzione per vedere se s1ono state richiamate queste operazioni;
	/// </summary>
	void EditingOperations(){
		
		if (Input.GetMouseButton (2)) {
			switch(StaticVariables.workMode){
			case 0:
				propWindowCluster.SetActive (true);
				break;
			case 1:
				propWindowClusterBalcony.SetActive (true);
				break;
			}
		}
		if (Input.GetMouseButton (0) && workPhase!=2) {
			Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);

			RaycastHit[] info;
			info = (Physics.RaycastAll (ray));
			for (int i = 0; i < info.Length; i++) {
				if (info [i].collider.gameObject.tag == "straightMeasures" && info [i].collider.transform.gameObject == selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText) {
					canWriteMeasure = true;
					selectedSegmentForEditing.GetComponent<linkProperties> ().startFade (false);
				} else if (info [i].collider.gameObject.tag == "anglesMeasures" && info [i].collider.transform.gameObject == selectedSegmentForEditing.GetComponent<linkProperties> ().angleText) {
					canWriteAngle = true;
					selectedSegmentForEditing.GetComponent<linkProperties> ().startFade (true);
				}
       
			}
		}
		if (canWriteMeasure) {
			selectedSegmentForEditing.GetComponent<linkProperties> ().freezeMeasure = true;
			foreach (char c in Input.inputString)
			{
				if (c == '\b') // has backspace/delete been pressed?
				{
					if (selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text.Length != 0)
					{
						selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text = selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text.Substring(0, selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text.Length - 1);
					}
				}
				else if ((c == '\n') || (c == '\r')) // enter/return
				{

					print("New measure is: " + selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text);
					l.text =  (selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text);
					setProperties ();
					selectedSegmentForEditing.GetComponent<linkProperties> ().freezeMeasure = false;
					selectedSegmentForEditing.GetComponent<linkProperties> ().stopFade ();
					canWriteMeasure = false;
				}
				else
				{
					if(char.IsDigit(c) || char.IsPunctuation(c))
					selectedSegmentForEditing.GetComponent<linkProperties> ().lengthText.GetComponent<TextMesh> ().text += c;
				}
			}
		}
		if (canWriteAngle) {
			selectedSegmentForEditing.GetComponent<linkProperties> ().freezeAngleMeasure = true;
			foreach (char c in Input.inputString)
			{
				if (c == '\b') // has backspace/delete been pressed?
				{
					if (selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text.Length != 0)
					{
						selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text = selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text.Substring(0, selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text.Length - 1);
					}
				}
				else if ((c == '\n') || (c == '\r')) // enter/return
				{

					gamma.text =  (selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text);
					setProperties ();
					selectedSegmentForEditing.GetComponent<linkProperties> ().freezeMeasure = false;
					selectedSegmentForEditing.GetComponent<linkProperties> ().stopFade ();
					canWriteAngle = false;
				}
				else
				{
					if (selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text.Length == 0) {
						if(char.IsDigit(c) || c=='-')
							selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text += c;
					}
					else if(char.IsDigit(c) || char.IsPunctuation(c))
						selectedSegmentForEditing.GetComponent<linkProperties> ().angleText.GetComponent<TextMesh> ().text += c;
				}
			}
		}
	
		
	}
	// Update is called once per frame
	void Update () {
		if (StaticVariables.workMode == 1)
			buildingPlane.transform.position = new Vector3 (9.17f, 4.13f, 0);
		else
			buildingPlane.transform.position = new Vector3 (9.17f, 0, 0);
		//questo mi serve per regolare per bene le fasi di drag di un segmento 
	
		//Se un segmento è selezionato per l'editing, verifichiamo quali operazioni ci vanno fatte//
		if (selectedSegmentForEditing) {
			EditingOperations ();
		}


		//Esecuzione di alcune funzioni relative alle fasi di creazione e modifica
	    if (!perspCam.activeInHierarchy) {
			if (Input.GetMouseButtonDown (1)) {
				workPhase = 1;
			}
			 checkPropertyWindow ();
			//GestioneScroll
			NoEditPercentageInf = Screen.height * 0.12f;
			NoEditPercentageSup = Screen.height - NoEditPercentageInf;
		}
       switch(workPhase){
		case 0:
			if (ortoCam.activeInHierarchy) {
				ortoCamHandling ();
				creatingPhaseCode ();
			}
			break;
		case 1:
			if (ortoCam.activeInHierarchy) {
				ortoCamHandling ();
				EditPhaseCode ();
			}
			break;
			
		case 5:  //Questo caso è quando edito un segmento non intermedio trascinando da nodo 1.
			dragSegmentFromNodeOne ();
			break;
			

	}
			
	}
	/// <summary>
	/// Controlla se il mouse clicca dentro la finestra proprietà del segmento. Previene creazioni di segmenti accidentali
	/// </summary>
	void checkPropertyWindow(){
		if (Input.GetMouseButton (0)) {
			Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);

			RaycastHit[] info;
			info = (Physics.RaycastAll (ray));
			for (int i = 0; i < info.Length; i++) {
				if (info [i].collider.gameObject.layer == 10)
					workPhase = 4;
			}
		}
	}

	/// <summary>
	/// Caso in cui trascino un segmento dal nodo 1
	/// </summary>
	void dragSegmentFromNodeOne(){
		
		if (Input.GetMouseButton (0)) {
			Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);
			float newSegPosX = 0;
			float newSegPosZ = 0;
			RaycastHit info;
			if (Physics.Raycast (ray, out info)) {
				newSegPosX = info.point.x;
				newSegPosZ = info.point.z;
			}
			inEditingSegment.transform.position = new Vector3 (info.point.x, inEditingSegment.transform.position.y, info.point.z);

			inEditingSegment.transform.LookAt (inEditingSegment.GetComponent<linkProperties> ().lookAtOb.transform);
			inEditingSegment.transform.eulerAngles = new Vector3 (-90, inEditingSegment.transform.eulerAngles.y+90, 0);
			inEditingSegment.transform.localScale = new Vector3 (Vector3.Distance(endPoint, inEditingSegment.transform.position), inEditingSegment.transform.localScale.y, inEditingSegment.transform.localScale.z);
		} else {
			if (inEditingSegment.GetComponent<linkProperties> ().attachedSegment) {
				VW.doEditing (inEditingSegment, inEditingSegment.GetComponent<linkProperties> ().attachedSegment, null, false);
				createFloor ();
			}
				workPhase = 0;
		}
	}
	/// <summary>
	/// Codice per la gestione della camera ortografica
	/// </summary>
	void ortoCamHandling(){
		if (Input.GetKey (KeyCode.Mouse2)) {
			Cameramain.transform.position = new Vector3 (Cameramain.transform.position.x - (Input.mousePosition.x - oldX) * scrollingSpeed, Cameramain.transform.position.y, Cameramain.transform.position.z - (Input.mousePosition.y - oldZ) * scrollingSpeed);

		}
		oldX = Input.mousePosition.x;
		oldZ = Input.mousePosition.y;
		//GestioneZoom
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) { // back
			Cameramain.orthographicSize--;

		}
		if (Input.GetAxis ("Mouse ScrollWheel") > 0) { // forward
			Cameramain.orthographicSize++;
		}
		Cameramain.orthographicSize = Mathf.Clamp (Cameramain.orthographicSize, 1, 10000);
		//Ogni qual volta rilascio il pulsante, la fase di drag viene interrotta. 
	}

	/// <summary>
	/// Questo codice viene eseguito quando si è in fase di creazione
	/// </summary>
	void creatingPhaseCode(){
		suggestionLabel.text = "Create mode-Right click on segments for edit mode";
		suggestionLabel.color = Color.green;
		suggestionLabel.color = new Color (0, 150f / 255f, 136f / 255f, 1);
		for(int i=0;i<createSuggestionCommands.Length;i++){
			createSuggestionCommands[i].SetActive(true);
			editSuggestionCommands [i].SetActive (false);

		}
		NoEditPercentageInf = 0;
		switch(StaticVariables.workMode){
		case 0:
		propWindowCluster.SetActive (false); //In questo caso chiudo la finestra proprietà
			break;
		case 1:
			propWindowClusterBalcony.SetActive (false);
			break;
		}
	
		if (Input.mousePosition.y < NoEditPercentageSup && Input.mousePosition.y > NoEditPercentageInf) {
			if (!Input.GetMouseButton (0)) {
				completeNewSegmentInstantiation ();
				hasShiftedForDragging = false;
			}

			//Il primo click, con il quale definisco da dove ha origine il segmento
			if (Input.GetMouseButton (0) && !instantiated) {
				instantiateNewSegment ();
			}

			//Se continuo a tenere premuto, visto che ora "instantiated==true", posso fare drag
			if (Input.GetMouseButton (0) && instantiated && Input.mousePosition != originMousePosition) {

				dragInstantiatedSegment ();

			}
		}
	}
	/// <summary>
	/// Dopo aver instanziato e dimensionato il segmento, qui si procede al completamento dell'operazion
	/// </summary>
	void completeNewSegmentInstantiation(){
		bool closingLoop = false; //Se chiudo un segmento su un nodo iniziale, nodeFirst, allra sto chiduendo le mura
		if (cicleOnce) {
			//Se il segmento va a finire su un nodo blu, allora deve collegarsi al segmento successivo preesistente!
			if(!Input.GetKey(KeyCode.LeftShift) && !hasShiftedForDragging ){ 
			if (verifyRayCast ("nodeCong") || verifyRayCast ("nodeFirst")) {
				string landingTag = " ";
					if (verifyRayCast ("nodeCong"))
						landingTag = "nodeCong";
					else {
						landingTag = "nodeFirst";
						closingLoop = true;
					}

				//quando cancello il primo segmento e lo reinstanzio per collegarlo, devo ricreare un falso "snappedNode"
				bool fakeSnappedNode = false;
				if (!snappedNode) {
					fakeSnappedNode = true;
					snappedNode = GameObject.CreatePrimitive (PrimitiveType.Quad);
					snappedNode.transform.position = inEditingSegment.transform.position;
				}
				inEditingSegment.transform.localScale = new Vector3 (Vector3.Distance (verifyRayCast (landingTag).transform.position, snappedNode.transform.position), inEditingSegment.transform.localScale.y, inEditingSegment.transform.localScale.z);
				float c = -(snappedNode.transform.position.z - verifyRayCast (landingTag).transform.position.z);
				float b = (snappedNode.transform.position.x - verifyRayCast (landingTag).transform.position.x);
				float angle = Mathf.Atan (c / b) * Mathf.Rad2Deg;
				if (b < 0)
					angle += 180;
				inEditingSegment.transform.eulerAngles = new Vector3 (inEditingSegment.transform.eulerAngles.x, angle, inEditingSegment.transform.eulerAngles.z);
				inEditingSegment.GetComponent<linkProperties> ().attachedSegment = verifyRayCast (landingTag).transform.parent.gameObject;
				verifyRayCast (landingTag).transform.parent.gameObject.GetComponent<linkProperties> ().previousSegment = inEditingSegment;
				Destroy (verifyRayCast (landingTag));
				secondSeg = inEditingSegment.GetComponent<linkProperties> ().attachedSegment;
				createFromNode = true;
				editingExisting = true;
				if (fakeSnappedNode)
					Destroy (snappedNode);
			}
		}
			instantiated = false;

			if (inEditingNode != null) {
				//Il nodo diventa figlio del segmento. Questo ci permette- noto il nodo- di richiamare il segmento corrispondente

				inEditingNode.transform.parent = inEditingSegment.transform;
				inEditingNode.transform.localEulerAngles = new Vector3 (180, 0, 180);
				if (1 / inEditingSegment.transform.localScale.x != Mathf.Infinity) 
					inEditingNode.transform.localScale = new Vector3 (1 / inEditingSegment.transform.localScale.x, 1 / inEditingSegment.transform.localScale.y, 1 / inEditingSegment.transform.localScale.z);
				else{
					Destroy (inEditingNode);
					Destroy (inEditingSegment);
				}
				inEditingNode.transform.position = new Vector3 (inEditingNode.transform.position.x, 0, inEditingNode.transform.position.z);
				inEditingSegment.GetComponent<linkProperties> ().lookAtOb.transform.position = inEditingNode.transform.position;
				if (createFromNode && !editingExisting) {
					VW.doWelding (snappedNode.transform.parent.gameObject, inEditingSegment);
					createFloor ();
				}
				else if (createFromNode && editingExisting) {
					//Trovo lo zeroSegment sapendo che se c'è, il suo nodo è inEditingNode. Zero Segment è il segmento precedente a quello che stiamo modificando
					GameObject zeroSeg = null;
					if (inEditingSegment.GetComponent<linkProperties> ().previousSegment) 
						zeroSeg = inEditingSegment.GetComponent<linkProperties> ().previousSegment;



					VW.doEditing (inEditingSegment, secondSeg, zeroSeg,closingLoop);
					createFloor ();
						
				}
				createFromNode = false;
				inEditingNode = null;
				inEditingSegment = null;

			}
			cicleOnce = false;

		}
	}
	/// <summary>
	/// Instanzia il nuovo segmento, a seguito del click sul piano di costruzione, in modalità create
	/// </summary>
	void instantiateNewSegment(){
		cicleOnce = true;
		editingExisting = false;
		Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);

		RaycastHit[] info;
		snappedNode = null;
		int freePositionInfo = 0; //Nel caso in cui non interagiamo con un node, il segmento lo creiamo in info[freePositionInfo].point;
		info = (Physics.RaycastAll (ray));
		createFromNode = false;
		originMousePosition = Input.mousePosition;
		//controllo se il mouse sta interagendo con un nodo (e quindi snap sul nodo e il nuovo segmento nasce da qui) oppure dal piano di costruzione (e quindi creo segmento dal punto in cui si clicca)
		for (int i = 0; i < info.Length; i++) {
			if(info[i].collider.tag=="stake"){
				editingExisting=false;
				createFromNode=false;
			}

			if (info [i].collider.tag == "node") {
				if (info [i].collider.gameObject.transform.parent.GetComponent<linkProperties> ().attachedSegment == null && !Input.GetKey (KeyCode.LeftShift)) {
					editingExisting = false;
					createFromNode = true;
				} else if (Input.GetKey (KeyCode.LeftShift)) { 
					editingExisting = true;
					createFromNode = true;

				}else 
					return;

				snappedNode = info [i].collider.gameObject;




			}

			else if( Input.GetKey(KeyCode.LeftShift) || hasShiftedForDragging){ 
				hasShiftedForDragging = true;
				if(info [i].collider.tag == "nodeFirst") {
				inEditingSegment = info [i].collider.transform.parent.gameObject;
				endPoint = inEditingSegment.transform.TransformPoint (-1, 0, 0);
				workPhase = 5;
				return;
			}
			else if (info [i].collider.tag == "nodeCong") {
				inEditingSegment = info [i].collider.transform.parent.gameObject;
				endPoint = inEditingSegment.transform.TransformPoint (-1, 0, 0);
				workPhase = 5;
				return;
			}
			}
		}
		if (editingExisting) {
			//Esiste già un segmento in questa posizione? Se esite, allora stiamo editando
			inEditingSegment = snappedNode.transform.parent.gameObject;

			segmentOrigin = inEditingSegment.transform.position;
			if (snappedNode.transform.parent.gameObject.GetComponent<linkProperties> ().attachedSegment) {
				secondSeg = snappedNode.transform.parent.gameObject.GetComponent<linkProperties> ().attachedSegment;

			} else
				secondSeg = null;
			inEditingNode = snappedNode;
			editingExisting = true;
			instantiated = true;



		}

		if (!editingExisting) {

			//N.B. I segmenti non sono cubi qualsiasi, nativi di Unity, ma cubi creati con Rhinoceros per avere pivot nel nodo 1 (Vedi VertexWelder per schema)
			GameObject segment = Instantiate (Resources.Load ("cubetto")as GameObject);
			if (snappedNode)
				snappedNode.transform.parent.gameObject.GetComponent<linkProperties> ().attachedSegment = segment;
			try {
				segment.GetComponent<linkProperties> ().previousSegment = snappedNode.transform.parent.gameObject;
			} catch {
				//Debug.Log ("first segment");
			}

			GameObject node = Instantiate (Resources.Load ("node") as GameObject);
			segment.transform.localEulerAngles = new Vector3 (-90, 0, 0);
			//Aggiorno la lista aggiungendo il segmento attuale
			segPositions.Add (segment.transform);
			nodePositions.Add (node.transform);
			if (!snappedNode)
				segment.transform.position = new Vector3 (info [freePositionInfo].point.x, info [freePositionInfo].point.y, info [freePositionInfo].point.z);
			else {

				//Qualora il nuovo segmento parta da un nodo, automaticamente sappiamo che dobbiamo unire il nuovo segmento con il precedente.
				segment.transform.position = snappedNode.transform.position;
				segment.transform.position = new Vector3 (snappedNode.transform.position.x, info [freePositionInfo].point.y, snappedNode.transform.position.z);
				Debug.Log (snappedNode.transform.position.x +" "+ snappedNode.transform.position.y +" "+ snappedNode.transform.position.z+" Questa è la possizione di partrrnzsa!");
			}
			//segment.transform.position = Input.mousePosition;

			segment.transform.localScale = new Vector3 (0, defaultDepth, defaultHeight);

			segmentOrigin = segment.transform.position;
			segment.tag = "segment";

			segPositions.Add (segment.transform);
			inEditingNode = node;
			inEditingNode.tag = "node";
			inEditingNode.transform.rotation = segment.transform.rotation;
			inEditingNode.transform.position = segment.transform.position;
			inEditingNode.transform.position = new Vector3 (inEditingNode.transform.position.x, segment.transform.position.y, inEditingNode.transform.position.z);
			nodePositions.Add (node.transform);

			inEditingSegment = segment;
			instantiated = true;

		}
	}
	/// <summary>
	/// Determina l'allungamento e la rotazione del segmento per effetto del trascinamento del mouse
	/// </summary>
	void dragInstantiatedSegment(){
		
		Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);


		float newSegmentPositionX = 0;
		float newSegmentPositionZ = 0;
		float angleOffset; //A causa dell'uso dell'arcotangente, bisogna aggiustare il valore dell'angolo a seconda che il risultato sia negativo o positivo
		RaycastHit info;
		if (Physics.Raycast (ray, out info)) {
			newSegmentPositionX = info.point.x;
			newSegmentPositionZ = info.point.z;
		}

		inEditingNode.transform.position = info.point;
		//trovo rotazione e lunghezza del segmento sotto trascinamento, con le leggi trigonometriche del triangolo rettangolo e con Pitagora
		float b = newSegmentPositionX - segmentOrigin.x;
		float c = newSegmentPositionZ - segmentOrigin.z;

		float segmentRotation = Mathf.Atan (b / c);
		//Perchè il puntatore mouse si trovi al centro del nodo 2 del segmento, devo calcolare un offset angolare
		if (c > 0)
			angleOffset = 90;
		else
			angleOffset = -90;
		inEditingSegment.transform.eulerAngles = new Vector3 (inEditingSegment.transform.eulerAngles.x, segmentRotation * Mathf.Rad2Deg + angleOffset, inEditingSegment.transform.eulerAngles.z);
		float a = Mathf.Sqrt (b * b + c * c);
		inEditingSegment.transform.localScale = new Vector3 (a, inEditingSegment.transform.localScale.y, inEditingSegment.transform.localScale.z);
	}

	/// <summary>
	/// Questo codice viene richiamato nella fase di modifica
	/// </summary>
	void EditPhaseCode(){
		suggestionLabel.text = "Edit mode-Right click on the grid for create mode";
		suggestionLabel.color = new Color (0, 150f / 255f, 136f / 255f, 1);
		for(int i=0;i<createSuggestionCommands.Length;i++){
			createSuggestionCommands[i].SetActive(false);
			editSuggestionCommands [i].SetActive (true);

		}
		bool goToCreateMode = false;
		if (Input.GetMouseButtonDown (1) && Input.mousePosition.y < NoEditPercentageSup && Input.mousePosition.y > NoEditPercentageInf) {
			goToCreateMode = true;
			//Troviamo Tutti i segmenti e riportiamoli allo stato iniziale
			GameObject[] segments = GameObject.FindGameObjectsWithTag ("segment");
			for (int i = 0; i < segments.Length; i++) {
				segments [i].GetComponent<MeshRenderer> ().material = normalMat;
			}
			Ray rayS = Cameramain.ScreenPointToRay (Input.mousePosition);

			RaycastHit[] infoS;
			infoS = (Physics.RaycastAll (rayS));
			createFromNode = false;
			originMousePosition = Input.mousePosition;
			//controllo se il mouse sta interagendo con un nodo (e quindi snap sul nodo e il nuovo segmento nasce da qui) oppure dal piano di costruzione (e quindi creo segmento dal punto in cui si clicca)

			for (int i = 0; i < infoS.Length; i++) {

				if (infoS [i].collider.tag == "segment") {
					goToCreateMode = false;
					NoEditPercentageInf = editPercentageOrig;
			
				
					setWindowValues (infoS [i].collider.gameObject);
				} 
			}

		}
		if(selectedSegmentForEditing){
			if (!selectedSegmentForEditing.GetComponent<linkProperties> ().attachedSegment && !selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment) {
				if (Input.GetKey (KeyCode.DownArrow))
					selectedSegmentForEditing.transform.position = new Vector3 (selectedSegmentForEditing.transform.position.x, selectedSegmentForEditing.transform.position.y, selectedSegmentForEditing.transform.position.z - 0.01f);
				if (Input.GetKey (KeyCode.UpArrow))
					selectedSegmentForEditing.transform.position = new Vector3 (selectedSegmentForEditing.transform.position.x, selectedSegmentForEditing.transform.position.y, selectedSegmentForEditing.transform.position.z + 0.01f);
				if (Input.GetKey (KeyCode.LeftArrow))
					selectedSegmentForEditing.transform.position = new Vector3 (selectedSegmentForEditing.transform.position.x - 0.01f, selectedSegmentForEditing.transform.position.y, selectedSegmentForEditing.transform.position.z);
				if (Input.GetKey (KeyCode.RightArrow))
					selectedSegmentForEditing.transform.position = new Vector3 (selectedSegmentForEditing.transform.position.x + 0.01f, selectedSegmentForEditing.transform.position.y, selectedSegmentForEditing.transform.position.z);

			}
		}
		if (goToCreateMode) {
			DiscardChanges ();
			selectCreateMode ();
		}
	}
	/// <summary>
	/// Inserisce i valori del segmento nella finestra proprietà
	/// </summary>
	/// <param name="segment">Segment.</param>
	void setWindowValues(GameObject segment){
		segment.GetComponent<MeshRenderer> ().material = outlinedMat;
		SaveLoad.segmentOfGate = segment.name;


		float[] statistics = new float[8];
		statistics = VW.QueryProperties (segment);
		heightTag.text = string.Format ("{0:0.00}", statistics [0]);
		widthOneTag.text = string.Format ("{0:0.00}", statistics[1]*2);
		supAngleTag.text = string.Format ("{0:0.00}", statistics [2]);
		infAngleTag.text = string.Format ("{0:0.00}", statistics [3]);
		lengthTag.text = string.Format ("{0:0.00}", statistics [5]);
		absAngleTag.text = string.Format ("{0:0.00}", statistics [6]);
		prevHeightTag.text = string.Format ("{0:0.00}", statistics [7]);
 		h.text = string.Format ("{0:0.00}", statistics [0]);
		wOne.text = string.Format ("{0:0.00}",statistics[1]*2);
		alfa.text = string.Format ("{0:0.00}", statistics [2]);
		beta.text = string.Format ("{0:0.00}", statistics [3]);
		l.text = string.Format ("{0:0.00}", statistics [5]);
		gamma.text = string.Format ("{0:0.00}", statistics [6]);
		hfin.text = string.Format ("{0:0.00}", statistics [7]);
		MDd.currentlySelected = segment.GetComponent<linkProperties> ().thickTyp;
		thicknessType (segment.GetComponent<linkProperties> ().thickTyp);
		if (segment.GetComponent<linkProperties> ().isOpening) {
			makeGate ();
			gateOnOff.isOn = true;
			if (segment.GetComponent<linkProperties> ().columnL != null) {
				sxColumnButt.SetActive (true);
				Debug.Log (segment.GetComponent<linkProperties> ().columnL + " " + segment.name);
			}
		if (segment.GetComponent<linkProperties> ().columnR != null)
				dxColumnButt.SetActive (true);
			
		} else {
			sxColumnButt.SetActive (false);
			//dxColumnButt.SetActive (false);
			makeWall ();
			gateOnOff.isOn = false;
		
		}
			
		selectedSegmentForEditing =segment;
	}


	public void applyNewLeftColumnSettings(){
		GameObject col = selectedSegmentForEditing.GetComponent<linkProperties> ().columnL;
		GameObject gate = selectedSegmentForEditing.GetComponent<linkProperties> ().gate;
		if (impTextSx [0].text != "") {
			gate.GetComponent<ItemGate>().columnWidthL   = float.Parse (impTextSx [0].text);
			Debug.Log ("IMPTEXTIMPTEXT" + impTextSx [0].text);

		}
	

		if (impTextSx [1].text != "") {
			gate.GetComponent<ItemGate>().columnDepthL  = float.Parse (impTextSx [1].text);
		}
		if (impTextSx [2].text != "") {
			gate.GetComponent<ItemGate>().capitalWidthL  = float.Parse (impTextSx [2].text);
		}
		if (impTextSx [3].text != "") {
			gate.GetComponent<ItemGate>().capitalDepthL = float.Parse (impTextSx [3].text);
		}
		if (impTextSx [4].text != "") {
			gate.GetComponent<ItemGate>().capitalHeightL = float.Parse (impTextSx [4].text);
		}

		//Roprendo
		Destroy(col);
		Destroy (selectedSegmentForEditing.GetComponent<linkProperties> ().columnR);
		StartCoroutine (waitForNewColumns ());


		
	}
	public void applyNewRightColumnSettings(){
		GameObject col = selectedSegmentForEditing.GetComponent<linkProperties> ().columnR;
		GameObject gate = selectedSegmentForEditing.GetComponent<linkProperties> ().gate;
	
		if (impTextDx [0].text != "") {
			gate.GetComponent<ItemGate>().columnWidthR  = float.Parse (impTextDx [0].text);
		}


		if (impTextDx [1].text != "") {
			gate.GetComponent<ItemGate>().columnDepthR  = float.Parse (impTextDx [1].text);
		}
		if (impTextDx [2].text != "") {
			gate.GetComponent<ItemGate>().capitalWidthR  = float.Parse (impTextDx [2].text);
		}
		if (impTextDx [3].text != "") {
			gate.GetComponent<ItemGate>().capitalDepthR = float.Parse (impTextDx [3].text);
		}
		if (impTextDx [4].text != "") {
			gate.GetComponent<ItemGate>().capitalHeightR = float.Parse (impTextDx [4].text);
		}

		//Roprendo
		Destroy(col);
		Destroy (selectedSegmentForEditing.GetComponent<linkProperties> ().columnL);
		StartCoroutine (waitForNewColumns ());



	}
	IEnumerator waitForNewColumns(){
		yield return 0;
		SLScript.instantiateColumns(selectedSegmentForEditing.GetComponent<linkProperties>().gate);
		sxColumnWin.SetActive (false);
		dxColumnWin.SetActive (false);
	}
	public void cancelColumnMods(){
		sxColumnWin.SetActive (false);
		dxColumnWin.SetActive (false);
	}
	public void onOpenLeftColumnWindow(){
		Debug.Log (selectedSegmentForEditing.name);
		hintTextSx [0].text = "Column Width (mm) -" + (selectedSegmentForEditing.GetComponent<linkProperties> ().columnL.transform.localScale.x*1000f).ToString ();
		hintTextSx [1].text  = "Column Depth (mm) -" + (selectedSegmentForEditing.GetComponent<linkProperties> ().columnL.transform.localScale.z*1000f).ToString ();
		GameObject capital = selectedSegmentForEditing.GetComponent<linkProperties> ().columnL.transform.GetChild (0).gameObject ;
		capital.transform.parent = null;
		hintTextSx [2].text  = "Capital Width (mm) -" + (capital.transform.localScale.x*1000f).ToString ();
		hintTextSx[3].text ="Capital Depth (mm) -" + (capital.transform.localScale.z*1000f).ToString ();
		hintTextSx[4].text ="Capital Height (mm) -" + (capital.transform.localScale.y*1000f).ToString ();
		capital.transform.parent =	selectedSegmentForEditing.GetComponent<linkProperties> ().columnL.transform;

		sxColumnWin.SetActive (true);


		
	}
	public void onOpenRightColumnWindow(){
		hintTextDx [0].text = "Column Width (mm) -" + (selectedSegmentForEditing.GetComponent<linkProperties> ().columnR.transform.localScale.x*1000f).ToString ();
		hintTextDx [1].text  = "Column Depth (mm) -" + (selectedSegmentForEditing.GetComponent<linkProperties> ().columnR.transform.localScale.z*1000f).ToString ();
		GameObject capital = selectedSegmentForEditing.GetComponent<linkProperties> ().columnR.transform.GetChild (0).gameObject ;
		capital.transform.parent = null;
		hintTextDx [2].text  = "Capital Width (mm) -" + (capital.transform.localScale.x*1000f).ToString ();
		hintTextDx[3].text ="Capital Depth (mm) -" + (capital.transform.localScale.z*1000f).ToString ();
		hintTextDx[4].text ="Capital Height (mm) -" + (capital.transform.localScale.y*1000f).ToString ();
		capital.transform.parent =	selectedSegmentForEditing.GetComponent<linkProperties> ().columnR.transform;

		dxColumnWin.SetActive (true);
	}
	/// <summary>
	/// Se il campo input di una proprietà del segmento è vuoto, allora ci riapplico il valore precedente.
	/// </summary>
	/// <param name="segment">Segment.</param>
	float checkEmptyValues(GameObject segment, int statNum, string parsedFloat){ //0h,1+4width,2 supangle,3infangle,5L,6gamma,7prevheight
		float thisvalue=0;
		string toCheck=parsedFloat;
		if (toCheck =="") {
			float[] statistics = new float[8];
			statistics = VW.QueryProperties (segment);
			toCheck = string.Format ("{0:0.00}", statistics [statNum]);
		}
		thisvalue = float.Parse (toCheck);
	
		return thisvalue;
		
	}
	float checkThicknessValues(GameObject segment, string parsedFloat){ //0h,1+4width,2 supangle,3infangle,5L,6gamma,7prevheight
		float thisvalue=0;
		string toCheck=parsedFloat;
		if (toCheck =="") {
			float[] statistics = new float[8];
			statistics = VW.QueryProperties (segment);
			toCheck = string.Format ("{0:0.00}", statistics [1]+statistics[4]);
		}
		thisvalue = float.Parse (toCheck);

		return thisvalue;

	}

	/// <summary>
	/// Riporta i segmenti al materiale originale. Utile dopo la face di editing.
	/// </summary>
	void SetAllSegmentsInitialMat(){
		GameObject[] allSegs=GameObject.FindGameObjectsWithTag ("segment");
		for (int i = 0; i < allSegs.Length; i++) {
			allSegs [i].GetComponent<MeshRenderer> ().material = normalMat;
		}
	}
	/// <summary>
	/// Conferma e apporta le modifiche
	/// </summary>
	public void setProperties(){
		switch (StaticVariables.workMode){
		case 0:
			setPropertiesFences ();
			break;
		case 1:
			setPropertiesBalcony ();
			break;
		}
	}
	private void setPropertiesFences(){
		float thicknessLeft=0;
		float thicknessRight=0;
		SetAllSegmentsInitialMat ();
		//checkEmptyValues (selectedSegmentForEditing);
		selectedSegmentForEditing.transform.localScale = new Vector3 (checkEmptyValues(selectedSegmentForEditing,5,l.text), selectedSegmentForEditing.transform.localScale.y, selectedSegmentForEditing.transform.localScale.z);
		//Debug.Log (gamma.text);
		//float angleBetween = float.Parse (gamma.text);
		float angleBetween=checkEmptyValues(selectedSegmentForEditing,6,gamma.text);
		float angleAbsolute = 0;
		if (selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment) 
			angleAbsolute = angleBetween + selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment.transform.eulerAngles.y;
		else
			angleAbsolute = angleBetween;
		selectedSegmentForEditing.transform.eulerAngles = new Vector3 (selectedSegmentForEditing.transform.eulerAngles.x, angleAbsolute, selectedSegmentForEditing.transform.eulerAngles.z);
		switch (thicknessBias){
		case 0:
			thicknessLeft = checkThicknessValues (selectedSegmentForEditing, wOne.text)/2;
			thicknessRight = checkThicknessValues (selectedSegmentForEditing, wOne.text)/2;
			break;
		case 1:
			thicknessLeft = checkThicknessValues(selectedSegmentForEditing,wOne.text );
			thicknessRight = 0;
			break;
		case 2:
			thicknessLeft = 0;
			thicknessRight = checkThicknessValues(selectedSegmentForEditing,wOne.text );
			break;
		}
		float[] props = { checkEmptyValues(selectedSegmentForEditing,0,h.text),thicknessLeft,checkEmptyValues(selectedSegmentForEditing,2,alfa.text), checkEmptyValues(selectedSegmentForEditing,3,beta.text),thicknessRight,checkEmptyValues(selectedSegmentForEditing,5,l.text),angleBetween,checkEmptyValues(selectedSegmentForEditing,7,hfin.text)};
		VW.setPropsWithEditing (selectedSegmentForEditing, props);
		selectedSegmentForEditing.GetComponent<linkProperties> ().thickTyp = thicknessBias;
		selectedSegmentForEditing.GetComponent<linkProperties> ().isOpening = isGate_state;
		workPhase = 0;
		findAndResetStakes (selectedSegmentForEditing);
		resetInputFields ();
		checkForStakesAndFences(selectedSegmentForEditing,selectedSegmentForEditing.GetComponent<linkProperties>().thisSegmentProperties[2]);
		switch(StaticVariables.workMode){
		case 0:
			propWindowCluster.SetActive (false);
			break;
		case 1:
			propWindowClusterBalcony.SetActive (false);
			break;
		}

	}
	private void setPropertiesBalcony(){
		float thicknessLeft=0;
		float thicknessRight=0;
		SetAllSegmentsInitialMat ();
		//checkEmptyValues (selectedSegmentForEditing);
		selectedSegmentForEditing.transform.localScale = new Vector3 (checkEmptyValues(selectedSegmentForEditing,5,lBal.text), selectedSegmentForEditing.transform.localScale.y, selectedSegmentForEditing.transform.localScale.z);
		//Debug.Log (gamma.text);
		//float angleBetween = float.Parse (gamma.text);
		float angleBetween=checkEmptyValues(selectedSegmentForEditing,6,gammaBal.text);
		float angleAbsolute = 0;
		if (selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment) 
			angleAbsolute = angleBetween + selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment.transform.eulerAngles.y;
		else
			angleAbsolute = angleBetween;
		selectedSegmentForEditing.transform.eulerAngles = new Vector3 (selectedSegmentForEditing.transform.eulerAngles.x, angleAbsolute, selectedSegmentForEditing.transform.eulerAngles.z);
		switch (thicknessBias){
		case 0:
			thicknessLeft = checkThicknessValues (selectedSegmentForEditing, wOneBal.text)/2;
			thicknessRight = checkThicknessValues (selectedSegmentForEditing, wOneBal.text)/2;
			break;
		case 1:
			thicknessLeft = checkThicknessValues(selectedSegmentForEditing,wOneBal.text );
			thicknessRight = 0;
			break;
		case 2:
			thicknessLeft = 0;
			thicknessRight = checkThicknessValues(selectedSegmentForEditing,wOneBal.text );
			break;
		}
		float[] props = { checkEmptyValues(selectedSegmentForEditing,0,hBal.text),thicknessLeft,checkEmptyValues(selectedSegmentForEditing,2,alfaBal.text), checkEmptyValues(selectedSegmentForEditing,3,betaBal.text),thicknessRight,checkEmptyValues(selectedSegmentForEditing,5,lBal.text),angleBetween,checkEmptyValues(selectedSegmentForEditing,7,hfinBal.text)};
		VW.setPropsWithEditing (selectedSegmentForEditing, props);
		selectedSegmentForEditing.GetComponent<linkProperties> ().thickTyp = thicknessBias;
		workPhase = 0;
		findAndResetStakes (selectedSegmentForEditing);
		resetInputFields ();
		checkForStakesAndFences(selectedSegmentForEditing,selectedSegmentForEditing.GetComponent<linkProperties>().thisSegmentProperties[2]);
		switch(StaticVariables.workMode){
		case 0:
			propWindowCluster.SetActive (false);
			break;
		case 1:
			propWindowClusterBalcony.SetActive (false);
			break;
		}

	}
	/// <summary>
	/// Cancella il segmento selezionato
	/// </summary>
	public void trashSelectedSegment(){
		if (selectedSegmentForEditing != null)
			VW.onDeleteSegment (selectedSegmentForEditing.GetComponent<linkProperties> ().previousSegment, selectedSegmentForEditing.GetComponent<linkProperties> ().attachedSegment,selectedSegmentForEditing);
		SetAllSegmentsInitialMat ();
		switch(StaticVariables.workMode){
		case 0:
			propWindowCluster.SetActive (false);
			break;
		case 1:
			propWindowClusterBalcony.SetActive (false);
			break;
		}		resetInputFields ();
		selectCreateMode ();
	}
	/// <summary>
	/// Nasconde/Mostra le infografiche relative agli angoli
	/// </summary>
	public void onHideAnglesDimensions(){
		GameObject[] hidable = GameObject.FindGameObjectsWithTag ("anglesMeasures");
		GameObject[] linehidable = GameObject.FindGameObjectsWithTag ("anglesLines");
		if (hideAngleDims == true) {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled = false;
			}
			for (int z = 0; z < linehidable.Length; z++) {
				linehidable [z].GetComponent<LineRenderer> ().enabled = false;
			}
			hideAngleDims = false;
		} else {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled = true;
			}
			for (int z = 0; z < linehidable.Length; z++) {
				linehidable [z].GetComponent<LineRenderer> ().enabled = true;
			}
			hideAngleDims = true;
		}



	}

	/// <summary>
	/// Nasconde/Mostra le infografiche relative alle misure
	/// </summary>
	public void onHideStraightDimensions(){
		GameObject[] hidable = GameObject.FindGameObjectsWithTag ("straightMeasures");
		GameObject[] linesHidable = GameObject.FindGameObjectsWithTag ("straightLinesMeasures");
		if (hideStraightDims == true) {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled = false;

			}
			for (int i = 0; i < linesHidable.Length; i++) {
				linesHidable [i].GetComponent<LineRenderer> ().enabled = false;

			}
			hideStraightDims = false;
		} else {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled = true;
			}
			for (int i = 0; i < linesHidable.Length; i++) {
				linesHidable [i].GetComponent<LineRenderer> ().enabled = true;
			}
			hideStraightDims = true;
		}
	}
	/// <summary>
	/// Nasconde i nodi
	/// </summary>
	public void onHideNodes(){
		GameObject[] hidable = GameObject.FindGameObjectsWithTag ("node");
		GameObject[] linesHidable = GameObject.FindGameObjectsWithTag ("nodeCong");
		if (hideNodes == false) {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled = false;
			}
			for (int i = 0; i < linesHidable.Length; i++) {
				linesHidable [i].GetComponent<MeshRenderer> ().enabled = false;
			}
			hideNodes = true;
		} else {
			for (int i = 0; i < hidable.Length; i++) {
				hidable [i].GetComponent<MeshRenderer> ().enabled =true;
			}
			for (int i = 0; i < linesHidable.Length; i++) {
				linesHidable [i].GetComponent<MeshRenderer> ().enabled = true;
			}
			hideNodes = false;
		}
		}

	/// <summary>
	/// Verifica se il mouse sta toccando uno oggetto con uno specifico tag
	/// </summary>
	/// <returns><c>true</c>, if ray cast was verifyed <c>false</c> otherwise.</returns>
	/// <param name="tag">Tag da controllare.</param>
	private GameObject verifyRayCast(string tag){
		GameObject nodeCong = null;
		//bool tocca = false;
		RaycastHit[] info;
		Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);
        info = (Physics.RaycastAll (ray));
		originMousePosition = Input.mousePosition;
		//controllo se il mouse sta interagendo con un nodo (e quindi snap sul nodo e il nuovo segmento nasce da qui) oppure dal piano di costruzione (e quindi creo segmento dal punto in cui si clicca)
		for (int i = 0; i < info.Length; i++)
			if (info [i].collider.tag == tag)
				nodeCong = info [i].collider.gameObject;
		return nodeCong;

	

		
		
	}
	/// <summary>
	/// 0 simmetrico, 1 bias sinistro, 2 bias destro
	/// </summary>
	/// <param name="tipo">Tipo.</param>
	public void  thicknessType(int tipo){
		thicknessBias=MDd.currentlySelected;


	}
	void resetInputFields(){
		for (int i = 0; i < MIF.Length; i++) {
			MIF [i].ClearText ();
		}
	}
	void makeGate(){
		
		isGateButt.SetActive (true);
		isFenceButt.SetActive (false);
		GameObject.Find (SaveLoad.segmentOfGate).GetComponent<linkProperties> ().isOpening = true;
		isGate_state = true;
		for (int i = 0; i < MIF.Length; i++)
			MIF [i].interactable = false;
		MIF [1].interactable = true;
		MIF [2].interactable = true;
		UIExplanatory.SetImage (wall_gate [1]);
		GameObject mySeg = GameObject.Find (SaveLoad.segmentOfGate);
		for (int i = 0; i < mySeg.transform.childCount; i++) {
			if (mySeg.transform.GetChild (i).tag == "stake" || mySeg.transform.GetChild (i).tag == "fence")
				Destroy (mySeg.transform.GetChild (i).gameObject);
				
		}
	}
	void makeWall(){
		isGateButt.SetActive (false);
		isFenceButt.SetActive (true);
		GameObject.Find (SaveLoad.segmentOfGate).GetComponent<linkProperties> ().isOpening = false;

		isGate_state = false;
		for (int i = 0; i < MIF.Length; i++)
			MIF [i].interactable = true;
		MIF [3].interactable = false;
		UIExplanatory.SetImage (wall_gate [0]);

	}
	public void checkboxWallGate(bool isOpening){
		if (!gateOnOff.isOn)
			makeWall();
		else
			makeGate ();
	}
	public void enableStakesPlacement(){
		bool noFencesHere = true;
		for (int i = 0; i < selectedSegmentForEditing.transform.childCount; i++) {
			if (selectedSegmentForEditing.transform.GetChild (i).tag == "stake" && selectedSegmentForEditing.transform.GetChild (i).childCount > 0)
				noFencesHere = false;
		}
		if (noFencesHere) {
			setProperties ();
			stakHand.segment = selectedSegmentForEditing;

			stakHand.enabled = true;
			switch(StaticVariables.workMode){
			case 0:
				propWindowCluster.SetActive (false);
				break;
			case 1:
				propWindowClusterBalcony.SetActive (false);
				break;
			}			this.enabled = false;
		} else {
			stakeAlert.SetActive (true);
			//resetStakePlacementWithoutFences (selectedSegmentForEditing);

			
		}
	}

	/// <summary>
	/// Trova e riposiziona gli stakes sul dorso del segmento ad ogni modifica
	/// </summary>
	void findAndResetStakes(GameObject theSegment){
		int totalStakes = 0;
		for (int i = 0; i < theSegment.transform.childCount; i++) {
			if (theSegment.transform.GetChild (i).tag == "stake")
				totalStakes++;
		}
		GameObject[] totalChildren = new GameObject[totalStakes];
		int counter=0;
		for (int i = 0; i < theSegment.transform.childCount; i++) {
			if (theSegment.transform.GetChild (i).tag == "stake") {
				totalChildren [counter] = theSegment.transform.GetChild (i).gameObject;
				counter++;
			}
		}
		for (int i = 0; i < totalChildren.Length; i++)
			recalculateStakeHeightPosition (totalChildren [i], totalChildren [i].transform.localScale.y * theSegment.transform.localScale.z);
	}


	/// <summary>
	/// Ricalcola posizione dei paletti quando si modifica inclinazione o altezza segmento
	/// </summary>
	/// <param name="stke">Stke.</param>
	/// <param name="rightHeight">Right height.</param>
	void recalculateStakeHeightPosition(GameObject stke,float rightHeight){

		Vector3 absolutePosition=stke.transform.position;
		Debug.Log (stke.transform.position);
		absolutePosition=new Vector3(absolutePosition.x,100,absolutePosition.z);
		float stakeStartingH=0;

		Ray ray=new Ray(absolutePosition ,Vector3.down);
		Debug.DrawRay (absolutePosition, Vector3.down);
		RaycastHit[] info;
		Debug.Log ("Ma da qua passiamo certo");
		info = Physics.RaycastAll (ray);
		for (int i = 0; i < info.Length; i++) {
			Debug.Log ("E qui anche...");
			if (info [i].collider.tag=="segment") {
				Vector3 localPoint = info [i].point;
				stakeStartingH = localPoint.y;
				Debug.Log ("Entriamo qua e troviamo che " + stakeStartingH);		       	
			}
		}
		Debug.Log (stakeStartingH + " e l'altezza del pezzo dovrebbe essrere " + rightHeight);
		stke.transform.position=new Vector3(stke.transform.position.x,stakeStartingH+(rightHeight*.5f) ,stke.transform.position.z);

	}
	private void checkForStakesAndFences(GameObject theSeg,float targetAngle){
		for (int i = 0; i < theSeg.transform.childCount; i++) {
			if (theSeg.transform.GetChild (i).tag == "stake" && theSeg.transform.GetChild (i).childCount > 0)
				SLScript.fenceDistortionInterpolator (theSeg.transform.GetChild (i).GetChild (0).gameObject, targetAngle);
				//(theSeg.transform.GetChild (i).transform.GetChild (0);
		}
	}
	private void resetStakePlacementWithoutFences(GameObject theSeg){
		for (int i = 0; i < theSeg.transform.childCount; i++) {
			if (theSeg.transform.GetChild (i).tag == "stake" && theSeg.transform.GetChild (i).childCount > 0)
				Destroy (theSeg.transform.GetChild (i).GetChild (0).gameObject );
			else if (theSeg.transform.GetChild (i).tag == "fence")
				theSeg.transform.GetChild (i).GetComponent<fenceBoxScript> ().hasAFence = false;
		}
	}
	public void dismissStakeAlert(){
		stakeAlert.SetActive (false);
	}
	public void reEditStakes(){
		resetStakePlacementWithoutFences (selectedSegmentForEditing);
		setProperties ();
		stakHand.enabled = true;
		switch(StaticVariables.workMode){
		case 0:
			propWindowCluster.SetActive (false);
			break;
		case 1:
			propWindowClusterBalcony.SetActive (false);
			break;
		}		stakHand.segment = selectedSegmentForEditing;
		this.enabled = false;

	}
	/// <summary>
	/// Consente lo switch tra una modalità ed un'altra. es. tra recinzioni e balcon9
	/// </summary>
	/// <param name="modalità">Modalità.</param>
	public void setWorkMode(int modalita){
		StaticVariables.workMode = modalita;
	}
	/// <summary>
	/// Crea il piano di calpestio del balcone o il piano di riferimento delle recinzioni
	/// </summary>
	public void createFloor(){
		TriangleNet.Mesh meshRepresentation; //relativo alla generazione del piano di calpestio
		InputGeometry geometry; //relativo alla generazione del piano di caòpestio
		if(StaticVariables.workMode==0){
			if (GameObject.Find ("floor")) {
				Destroy (GameObject.Find ("floor"));
				Destroy (GameObject.Find ("extrusion"));
			} else {
				if (GameObject.Find ("floorBalcony")) {
					Destroy (GameObject.Find ("floorBalcony"));
					Destroy (GameObject.Find ("extrusionBalcony"));
				}
			}
		}
		geometry = new InputGeometry();
		int onlyOne = 0; //questo contatore mi permettere di conteggiare quante volte incontro segmenti che non hanno un segmento precedente: se sono più di uno, allora vuol dire che i segmnenti non sono chiusi.
		GameObject [] allSegs = GameObject.FindGameObjectsWithTag("segment");

		List <Point> boarderPoints=new List <Point>();
		for (int i = 0; i < allSegs.Length; i++) {
			if(onlyOne>1)
				return;  //Interrompo perchè non c'è una curva chiusa;
			//Per ciascun segmento esaminato, devo capire se prendere il vertice finale o iniziale. Prenderò per tutti il vertice finale, ma per il primo segmento anche il vertice iniziale
			if(allSegs[i].GetComponent<linkProperties>().previousSegment==null){ //Siamo sul primo segmento....
				Vector3 firstPoint=allSegs[i].GetComponent<MeshFilter>().mesh.vertices[11];
				boarderPoints.Add(new Point(allSegs[i].transform.TransformPoint(firstPoint).x,allSegs[i].transform.TransformPoint(firstPoint).z));
				Vector3 lastPoint=allSegs[i].GetComponent<MeshFilter>().mesh.vertices[8];
				boarderPoints.Add(new Point(allSegs[i].transform.TransformPoint(lastPoint).x, allSegs[i].transform.TransformPoint(lastPoint).z));
				//Qua devo metterci il primo vertice
				//Qua devo metterci anche il secondo
				onlyOne++;
				Debug.Log ("MI aspetto di passare una volta da qua");
			}
			else{ //non siamo piu sul primo segmento, prendiamo solo il punto finale
				//Qua prendo il punto finale
				Vector3 lastPoint=allSegs[i].GetComponent<MeshFilter>().mesh.vertices[8];
				boarderPoints.Add(new Point(allSegs[i].transform.TransformPoint(lastPoint).x, allSegs[i].transform.TransformPoint(lastPoint).z));
				Debug.Log ("E tot altre volte da qua");
			}
		}
		Debug.Log (boarderPoints [2].coordinates);
			geometry.AddRing(boarderPoints,0);
			meshRepresentation = new TriangleNet.Mesh();
			meshRepresentation.Triangulate(geometry);

		//generate mesh based on triangulation
		Dictionary<int, float> zOffsets = new Dictionary<int, float>();

		foreach(KeyValuePair<int, TriangleNet.Data.Vertex> pair in meshRepresentation.vertices)
		{
			zOffsets.Add(pair.Key, Random.RandomRange(0, 0));
		}

		int triangleIndex = 0;
		List<Vector3> vertices = new List<Vector3>(meshRepresentation.triangles.Count * 3);
		List<int> triangleIndices = new List<int>(meshRepresentation.triangles.Count * 3);

		foreach(KeyValuePair<int, TriangleNet.Data.Triangle> pair in meshRepresentation.triangles)
		{
			TriangleNet.Data.Triangle triangle = pair.Value;

			TriangleNet.Data.Vertex vertex0 = triangle.GetVertex(0);
			TriangleNet.Data.Vertex vertex1 = triangle.GetVertex(1);
			TriangleNet.Data.Vertex vertex2 = triangle.GetVertex(2);

			Vector3 p0 = new Vector3( vertex0.x, vertex0.y, zOffsets[vertex0.id]);
			Vector3 p1 = new Vector3( vertex1.x, vertex1.y, zOffsets[vertex1.id]);
			Vector3 p2 = new Vector3( vertex2.x, vertex2.y, zOffsets[vertex2.id]);

			vertices.Add(p0);
			vertices.Add(p1);
			vertices.Add(p2);

			triangleIndices.Add(triangleIndex + 2);
			triangleIndices.Add(triangleIndex + 1);
			triangleIndices.Add(triangleIndex);

			triangleIndex += 3;
		}

		Mesh mesh;
		Mesh extrusion;
		mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangleIndices.ToArray();
		mesh.RecalculateNormals();

		GameObject floor = new GameObject ();
		if (StaticVariables.workMode == 0)
			floor.name = "floor";
		else
			floor.name = "floorBalcony";
		floor.AddComponent<MeshFilter> ();
		floor.AddComponent<MeshRenderer> ();
		floor.GetComponent<MeshFilter>().mesh = mesh;
		floor.transform.eulerAngles = new Vector3 (90, 0, 0);
		floor.GetComponent<MeshRenderer> ().material = Resources.Load ("floor") as Material;

		srcMesh =floor.GetComponent<MeshFilter>().sharedMesh;
		precomputedEdges = MeshExtrusion.BuildManifoldEdges(srcMesh);
		Extrude ();

	}


	/// <summary>
	/// Estrude il piano di calpestìo, creando la sezione definita con S nelle tavole di riferimento	
	/// </summary>
	void Extrude()
	{
		GameObject extrusionOb = new GameObject ();
		if (StaticVariables.workMode == 0)
			extrusionOb.name = "extrusion";
		else
			extrusionOb.name = "extrusionBalcony";
		extrusionOb.AddComponent<MeshFilter> ();
		extrusionOb.AddComponent<MeshRenderer> ();
		extrusionOb.GetComponent<MeshRenderer>().material=Resources.Load ("floor") as Material;
		Vector3 extrusionNormal = -transform.up;
		List<ExtrudedSection> sections = new List<ExtrudedSection>();


		ExtrudedSection section = new ExtrudedSection();
		section.point = extrusionOb.transform.position;
		section.matrix = extrusionOb.transform.localToWorldMatrix;
		sections.Insert(0,section);

		//for(int i = 0; i<segments; i++)
		//{
			extrusionOb.transform.position = extrusionOb.transform.position + extrusionNormal * 1.0f / segments;
			section = new ExtrudedSection();
			section.point = extrusionOb.transform.position;
			section.matrix = extrusionOb.transform.localToWorldMatrix;
			sections.Insert(0, section);
		//}

		Matrix4x4 worldToLocal = extrusionOb.transform.worldToLocalMatrix;
		Quaternion rotation = Quaternion.LookRotation(-extrusionNormal, Vector3.up);
		Matrix4x4[] finalSections = new Matrix4x4[sections.Count];
		for (int i = 0; i < sections.Count; i++)
		{
			finalSections[i] = worldToLocal * Matrix4x4.TRS(sections[i].point, rotation, Vector3.one);
		}

		MeshExtrusion.ExtrudeMesh(srcMesh,extrusionOb.GetComponent<MeshFilter>().mesh, finalSections, precomputedEdges, invertFaces);
		extrusionOb.transform.eulerAngles = new Vector3 (0, 180, 180);
		extrusionOb.transform.position = new Vector3 (0, buildingPlane.transform.position.y, 0);
	}
	private   class ExtrudedSection
	{
		public Vector3 point;
		public Matrix4x4 matrix;
	}
}

