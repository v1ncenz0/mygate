﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MaterialUI;

public class stakesHandling : MonoBehaviour {
	int constructionCase=0; //Se 0, stiamo costruendo recinzioni, se è 1 stiamo costruendo balconi 
	private bool isEmbossed=false; // Se true, la ringhiera del balcone sarà a sbalzo. Si devono quindi definire i vari cazzi e mazzi.
	public Color editingBackgroundColor;
	private Color standardBackgroundColor;
	[Header("Finestra di editing Recinzioni")]
	public GameObject propertyWindow;

	public Text stakeWidthLb;
	public Text stakeLengthLb;
	public Text stakeHeightLb;
	public Text stakeOffsetLb;
	public Text stakeInterDistLb;

	public Text stakeWidthHint;
	public Text stakeLengthHint;
	public Text stakeHeighthint;
	public Text stakeOffsetHint;
	public Text stakeInterDistHint;
	public Text stakeOffsetMin;
	public Text stakeOffsetMax;
	[Header("Finestra di editing Balconi")]
	public GameObject propertyWindowBal;
	public Image embossedImg;
	public Image standardImg;
	public Text stakeWidthLbBal;
	public Text stakeLengthLbBal;
	public Text stakeHeightLbBal;
	public Text stakeOffsetLbBal;
	public Text stakeInterDistLbBal;
	public Text stakeZInfLbBal;
	public Text basePlateLbBalWidth;
	public Text basePlateLbBalHeight;
	public Text stakeBetaRotLb;
	public Text stakeHandraiLblWidth;
	public Text stakeHandrailLbHeight;
	//public Text basePlateLbBalLength;   //Quadrata in prima approssimazione

	public Text stakeWidthHintBal;
	public Text stakeLengthHintBal;
	public Text stakeHeighthintBal;
	public Text stakeOffsetHintBal;
	public Text stakeInterDistHintBal;
	public Text stakeOffsetMinBal;
	public Text stakeOffsetMaxBal;
	public Text stakeZInfHintBal;
	public Text basePlateHintBalWidth;
	public Text basePlateHintBalHeight;
	public Text stakeBetaRotHint;
	private float basePlateWidth;
	private float basePlateHeight;
	public Text stakeHandrailHintWidth;
	public Text stakeHandrailHintHeight;
	private float handrailHeight;
	private float handrailWidth;


	[Header("Intervallo ideale tra i paletti in m")]
	public float autoSeparation;
	public GameObject segment;
	public bool hasAttachedSegment;
	public bool hasPreviousSegment;
	public Vector3 autoStakeDim;
	public float manualOffset;
	int stakeWorkingPhase=0;
	private Camera Cameramain;
	private GameObject lengthTextParent;
	public GameObject lengthText;
	private TextMesh lengthTextMR;
	private GameObject arrowSideOne;
	private GameObject arrowSideTwo;
	private bool canWriteMeasure;
	[Header("MaterialePerPaletto")]
	public Material stakeMat;
	private float oldX; //Utilizzati per la funzione scroll in editor;
	private float oldZ;
	public float scrollingSpeed=.01f; //Velocità con cui il mouse scrolla nell'editor.
	private bool FadeStarted;
	private Color lerpedColor;
	GameObject measureEditable=null;
	private bool setNewStakeDistance=false;
	private GameObject stakeInEditing;
	[Header("NormalMode")]
	public ExperimCmd exCm;
	private MaterialInputField []MIF;
	private Material originalMat;
	private float zInf=0;


	/*
	 * 
	 * 
	 *    Lo spazio tra i paletti, dove vengono eventualmente inserite le ringhiere lo chiameremo da ora in poi porzione per comodità
	 * 
	 *      __|_________|_________|___
	 *         porzione  porzione
	 * 
	 * */

	void Start(){
		switch(StaticVariables.workMode){
		case 0:
			MIF = new MaterialInputField[5];
			MIF [0] = stakeWidthLb.transform.parent.GetComponent<MaterialInputField> ();
			MIF [1] = stakeHeightLb.transform.parent.GetComponent<MaterialInputField> ();
			MIF [2] = stakeLengthLb.transform.parent.GetComponent<MaterialInputField> ();
			MIF [3] = stakeOffsetLb.transform.parent.GetComponent<MaterialInputField> ();
			MIF [4] = stakeInterDistLb.transform.parent.GetComponent<MaterialInputField> ();
			break;
		case 1:
			MIF = new MaterialInputField[5];
			MIF [0] = stakeWidthLbBal.transform.parent.GetComponent<MaterialInputField> ();
			MIF [1] = stakeHeightLbBal.transform.parent.GetComponent<MaterialInputField> ();
			MIF [2] = stakeLengthLbBal.transform.parent.GetComponent<MaterialInputField> ();
			MIF [3] = stakeOffsetLbBal.transform.parent.GetComponent<MaterialInputField> ();
			MIF [4] = stakeInterDistLbBal.transform.parent.GetComponent<MaterialInputField> ();
			break;
		}
		
	}

	// Use this for initialization
	void OnEnable () {
		originalMat = segment.GetComponent<MeshRenderer> ().material;
		segment.GetComponent<MeshRenderer> ().material = Resources.Load ("stakePlacementMat") as Material;
		//automaticPlacement ();
		Cameramain=GameObject.Find("Main Camera").GetComponent<Camera>();
		standardBackgroundColor = Cameramain.backgroundColor;
		Cameramain.backgroundColor = editingBackgroundColor;
		deletePreviousJob ();
	}
	void ortoCamHandling(){
		if (Input.GetKey (KeyCode.Mouse2)) {
			Cameramain.transform.position = new Vector3 (Cameramain.transform.position.x - (Input.mousePosition.x - oldX) * scrollingSpeed, Cameramain.transform.position.y, Cameramain.transform.position.z - (Input.mousePosition.y - oldZ) * scrollingSpeed);

		}
		oldX = Input.mousePosition.x;
		oldZ = Input.mousePosition.y;
		//GestioneZoom
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) { // back
			Cameramain.orthographicSize--;

		}
		if (Input.GetAxis ("Mouse ScrollWheel") > 0) { // forward
			Cameramain.orthographicSize++;
		}
		Cameramain.orthographicSize = Mathf.Clamp (Cameramain.orthographicSize, 1, 10000);
		//Ogni qual volta rilascio il pulsante, la fase di drag viene interrotta. 
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			generateHandrail ();
			exCm.enabled = true;
			Cameramain.backgroundColor = standardBackgroundColor;
			this.enabled = false;


		}
			
		ortoCamHandling ();
		switch (stakeWorkingPhase) {
		case 0:

			manualPlacement ();
			EditingOperations ();
			break;
			/*
		case 1:
			Debug.Log ("NoInstantiation");
			break;
			*/
		}


	}
	void automaticPlacement(){
		//Quando il segmento è più piccolo del valore di intervallo dei paletti, si avrà un unico intervallo di paletti   |_____|
		if (segment.transform.localScale.x < autoSeparation) {
			instantiateAStake (segment, autoStakeDim, 0,0,false);
			instantiateAStake (segment, autoStakeDim, 1,0,false);

				
		} else {
			//Definiamo il numero di porzioni, come approssimazione all'intero più vicino del rapporto tra lunghezza segmento e valore di autoseparazione ideale

			int porzioni = (Mathf.RoundToInt (segment.transform.localScale.x / autoSeparation));
			float floatedPorzioni = (float)porzioni-1;
			float  stakeLocalPos = 0;
			//Otteniamo TOT porzioni. Ora bisogna ragionare in coordinate locali, dove la lunghezza di ciascun segmento è unitaria.
			for (int i = 0; i < porzioni; i++) {
				stakeLocalPos = (1 /floatedPorzioni)*i;
				instantiateAStake (segment, autoStakeDim, stakeLocalPos,0,false);
			}
		}
	
	}
	/// <summary>
	/// Piazza un paletto nella posizione desiderata, lo assegna come figlio al segmento corrispondente
	/// </summary>
	/// <param name="seg">Segmento padre.</param>
	/// <param name="portions">Porzioni di ringhiera .</param>
	/// <param name="stakeDimension">Dimensione paletto.</param>
	/// <param name="localPos">Posizione locale.</param>
	private void instantiateAStake(GameObject seg, Vector3 stakeDimension,float localPos,float localheight,bool isManual){

		GameObject stake = GameObject.CreatePrimitive (PrimitiveType.Cube);
		//Settiamo l'altezza dello stake, in prima approssimazione facciamo che si trovi sulla faccia superiore del cubo. 
		stake.name="stake";
		stake.GetComponent<MeshRenderer> ().material = stakeMat;
		stake.AddComponent<MeshCollider> ();
		stake.tag = "stake";
		stake.transform.localScale=stakeDimension ;
		stake.transform.eulerAngles = new Vector3 (0, seg.transform.eulerAngles.y, 0);
		//stake.transform.position = seg.transform.TransformPoint (new Vector3 (-localPos, manualOffset, stakeDimension.y));
		stake.transform.parent = seg.transform;
		if (isManual)
			localPos = -localPos;
		//stake.transform.localEulerAngles = new Vector3(90,0,0);
		//stake.transform.localScale = new Vector3 (stakeDimension.x/seg.transform.localScale.x, stakeDimension.y / seg.transform.localScale.y, stakeDimension.z / seg.transform.localScale.z);

		stake.transform.localPosition = new Vector3 (-localPos, manualOffset, stakeDimension.y);	
		if (localheight != 0) {
			stake.transform.localPosition = new Vector3 (-localPos, manualOffset, localheight + stake.transform.localScale.y / 2);
		} else {
			recalculateStakeHeightPosition (stake, stakeDimension.y);
		}
		measureRenderer (seg);
		if(StaticVariables.workMode==1 && isEmbossed)
			moveFenceOut (manualOffset, zInf, stake);
		//QUA ISTANZIO BASETTA		
		if (StaticVariables.workMode == 1 && !isEmbossed) {
			Vector3 basePos = new Vector3 (stake.transform.position.x,stake.transform.position.y-  (stake.transform.localScale.y * stake.transform.parent.transform.localScale.z)/2+basePlateHeight/2, stake.transform.position.z);
			instantiateBasePlate (basePos,stake);
		}
	}


	/// <summary>
	/// Istanzia i picchetti manualmente sul segmento
	/// </summary>
	void manualPlacement(){
		if (Input.GetMouseButtonDown (0)) {
			
			Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);

			RaycastHit[] info;
	
			info = Physics.RaycastAll (ray);
			for (int i = 0; i < info.Length; i++) {
				
				if (info [i].collider.gameObject==segment && info [i].normal.y> 0) {
					//segment = info [i].collider.gameObject;
					Vector3 instPoint = info[i].collider.transform.InverseTransformPoint (info[i].point); // info.point
					instantiateAStake (info[i].collider.transform.gameObject, autoStakeDim, instPoint.x,instPoint.z, true);
				}
			}
		}
		}

	/// <summary>
	/// Istanzia un line renderer con la misura relativa tra due paletti, o tra il paletto e l'inizio del segmento; 
	/// </summary>
	void measureRenderer(GameObject segmr){

		//Innanzitutto mi riordino i paletti dal più vicino all'origine del segmento al più lontano
		int theseStakesLength=0;
		int counted = 0;
		GameObject[] totalTagged = GameObject.FindGameObjectsWithTag ("stake");
		for (int i = 0; i < segmr.transform.childCount; i++) {
			if (segmr.transform.GetChild (i).tag == "stake")
				theseStakesLength++;
		}
		GameObject[] stakes=new GameObject[theseStakesLength];

		for (int z = 0; z < totalTagged.Length; z++) {
			if (totalTagged [z].transform.parent == segmr.transform) {
				stakes [counted] = totalTagged [z];
				stakes [counted].name = counted.ToString ();
				counted++;

			}
		}

		int totalStakes = theseStakesLength;



		GameObject[] arrangedStakes = new GameObject[totalStakes];

		for (int i = 0; i < totalStakes; i++) {
	
			stakes [i] = segmr.transform.Find (i.ToString ()).gameObject;
		}
		for (int i = 0; i < totalStakes; i++) {
			int position = 0;
			for (int j = 0; j < totalStakes; j++) {
				if (stakes [i].transform.localPosition.x < stakes [j].transform.localPosition.x)
					position++;
			}
			arrangedStakes [position] = stakes [i];
			stakes [i].name = i.ToString ();

		}

		GameObject []deletable=GameObject.FindGameObjectsWithTag("stakeMeasures");
		for (int i = 0; i < deletable.Length; i++)
			Destroy (deletable[i]);
		if (arrangedStakes.Length>0) {
			GameObject fake = new GameObject ();

			fake.transform.parent = arrangedStakes [0].transform.parent;
			fake.transform.localPosition = arrangedStakes [0].transform.localPosition;
			fake.transform.localPosition = new Vector3 (0, arrangedStakes [0].transform.localPosition.y, arrangedStakes [0].transform.localPosition.z);
			lineRendererInstantiator (fake, arrangedStakes [0], 0);
			arrangedStakes [0].name = "0";
	   
			for (int i = 1; i < totalStakes; i++) {
				lineRendererInstantiator (arrangedStakes [i], arrangedStakes [i - 1], i);
				arrangedStakes [i].name = i.ToString ();
			}
		}
	}
	void lineRendererInstantiator(GameObject pointOne,GameObject pointTwo, int stakeNumber){
		GameObject lineOb = new GameObject ();
		lineOb.tag = "stakeMeasures";
		LineRenderer linerend= lineOb.AddComponent<LineRenderer> ();
		linerend.startColor = Color.red;
		linerend.endColor = Color.red;
		linerend.startWidth = .01f;
		linerend.endWidth = .01f;
		linerend.SetPosition (0, new Vector3(pointOne.transform.position.x,pointTwo.transform.position.y,pointOne.transform.position.z));
		linerend.SetPosition(1,pointTwo.transform.position);
		linerend.material = Resources.Load ("LineRendering") as Material;
		linerend.useWorldSpace = false;
		lengthTextParent = new GameObject ();

		lengthTextParent.tag="stakeMeasures";
		lengthText = new GameObject ();
		lengthText.name=stakeNumber.ToString();
		lengthText.transform.parent = lengthTextParent.transform;
		lengthText.transform.eulerAngles = new Vector3 (-90, 0, 0);
		lengthTextParent.transform.eulerAngles = new Vector3 (0, segment.transform.eulerAngles.y, 0);
		lengthTextParent.transform.localScale = new Vector3 (-1, 1, 1);
		lengthText.tag = "stakeMeasures";
		lengthText.transform.position = Vector3.Lerp (pointOne.transform.position, pointTwo.transform.position, 0.5f);
		lengthTextMR = lengthText.AddComponent<TextMesh> ();
		Vector3 correctedPOne = new Vector3 (pointOne.transform.position.x, pointTwo.transform.position.y, pointOne.transform.position.z);
		float  rawMeasure=Mathf.Abs(Vector3.Distance(correctedPOne,pointTwo.transform.position));
		string measureToShow = string.Format ("{0:0.00}", rawMeasure);  
		lengthTextMR.text =measureToShow;
		lengthTextMR.alignment = TextAlignment.Center;
		lengthTextMR.anchor = TextAnchor.UpperCenter;
		lengthTextMR.color = Color.red;
		lengthTextMR.characterSize = 0.1f;
		lengthTextMR.gameObject.AddComponent<BoxCollider> ();
		lengthTextMR.gameObject.GetComponent<BoxCollider> ().size = Vector3.one*0.1f;
	}

	void EditingOperations(){

		if (Input.GetMouseButton (1)) {
			Ray ray = Cameramain.ScreenPointToRay (Input.mousePosition);

			RaycastHit[] info;
			info = (Physics.RaycastAll (ray));
			for (int i = 0; i < info.Length; i++) {
				if (info [i].collider.gameObject.tag == "stakeMeasures") {
					canWriteMeasure = true;
					measureEditable = info [i].transform.gameObject;

					startFade (measureEditable.GetComponent<TextMesh>());
				} 
				if (info [i].collider.gameObject.tag == "stake") {
					//segment = info [i].collider.transform.parent.gameObject;
					canWriteMeasure = false;
					OpenPropertyWindow (info [i].collider.gameObject,info[i].collider.gameObject.transform.parent.gameObject);
				}

			}
		}
		if (canWriteMeasure) {
			stakeDistanceInEditor ();
	     }



	}

	/// <summary>
	/// Modifica distanza tra paletti direttamente dall'editor
	/// </summary>
	void stakeDistanceInEditor(){
		foreach (char c in Input.inputString)
		{
			if (c == '\b') // has backspace/delete been pressed?
			{
				if (measureEditable.GetComponent<TextMesh> ().text.Length != 0)
				{
					measureEditable.GetComponent<TextMesh> ().text = measureEditable.GetComponent<TextMesh> ().text.Substring(0, measureEditable.GetComponent<TextMesh> ().text.Length - 1);
				}
			}
			else if ((c == '\n') || (c == '\r')) // enter/return
			{

				print("New measure is: " + measureEditable.GetComponent<TextMesh> ().text);
				setNewStakePosition (measureEditable);
				//freezeMeasure = false;
				stopFade (measureEditable.GetComponent<TextMesh>());
				canWriteMeasure = false;
			}
			else
			{
				if (char.IsDigit (c) || char.IsPunctuation (c))
					measureEditable.GetComponent<TextMesh> ().text += c;
			}
		}
	}


	Coroutine fadingcourotine;


	public void startFade(TextMesh textm){

		if (!FadeStarted) {
			fadingcourotine = StartCoroutine (Fading (textm));
				FadeStarted = true;
			} 
		}

	public void stopFade(TextMesh textm){
		StopCoroutine (fadingcourotine);
		textm.color = Color.red;
		FadeStarted = false;
	}
	IEnumerator Fading (TextMesh texm)
	{
		while(true) {
			lerpedColor = Color.Lerp (Color.white, Color.red, Time.time);
			texm.color = lerpedColor;
			yield return new WaitForSeconds (.3f);
			lerpedColor = Color.Lerp (Color.red, Color.white, Time.time);
			texm.color = lerpedColor;
			yield return new  WaitForSeconds (.3f);
		}

	}

	void setNewStakePosition(GameObject measureEditable){
		int stakeNo = 0;
		int.TryParse (measureEditable.name,out stakeNo);
		float distance = 0;
		float.TryParse (measureEditable.GetComponent<TextMesh> ().text, out distance);
					//Per il primo stake la distanza viene calcolata rispetto al segmento
		if (stakeNo == 0) {
			GameObject stake = segment.transform.Find (measureEditable.name).gameObject;
			Destroy (stake);
			StartCoroutine (yieldDestruction (segment, autoStakeDim, -distance / segment.transform.localScale.x, true));
			//
	
		} else { //La distanza viene calcolata come incremento rispetto al palo precedente
			GameObject stake = segment.transform.Find (measureEditable.name).gameObject;
			int prev = int.Parse (measureEditable.name) - 1;
			GameObject prevStake = segment.transform.Find (prev.ToString ()).gameObject ;
			Destroy (stake);
			StartCoroutine (yieldDestruction (segment, autoStakeDim, -distance / segment.transform.localScale.x+prevStake.transform.localPosition.x, true));
		}
	   
	}
	IEnumerator yieldDestruction(GameObject segmt, Vector3 dims,float pos,bool manual){
		yield return 0;
					instantiateAStake (segmt,dims,pos,0,manual);
	

	}

	IEnumerator yieldDestruction(GameObject segmt){
		yield return 0;
		measureRenderer (segmt);
	}
	/// <summary>
	/// Apre la finestra di modifica del segmento selezionato
	/// </summary>
	/// <param name="inEditingStake">In editing stake.</param>
	public void OpenPropertyWindow (GameObject inEditingStake,GameObject inEditingSegment){
		switch (StaticVariables.workMode) {
		case 0:
			OpenPropertyWindowRecinz (inEditingStake, inEditingSegment);
			break;
		case 1:
			OpenPropertyWindowBalcony (inEditingStake, inEditingSegment);
			break;
	
		}
	}
	private void OpenPropertyWindowRecinz(GameObject inEditingStake,GameObject inEditingSegment){
		stakeWorkingPhase = 1;
		propertyWindow.SetActive (true);
		stakeInEditing = inEditingStake;
		stakeHeighthint.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.y * inEditingSegment.transform.localScale.z);
		stakeLengthHint.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.x * inEditingSegment.transform.localScale.x);
		stakeWidthHint.text  = string.Format ("{0:0.00}", inEditingStake.transform.localScale.z * inEditingSegment.transform.localScale.y);
		//float localToWorldOffset=(inEditingStake.transform.localPosition.y-.5f)*inEditingStake.transform.parent.GetComponent<linkProperties>().thisSegmentProperties[1]/.5f;
		stakeOffsetHint.text = string.Format ("{0:0.000}", inEditingStake.transform.localPosition.y);

		//stakeOffsetHint.text=string.Format("{0:0.00}",localToWorldOffset);
		stakeHeightLb.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.y * inEditingSegment.transform.localScale.z);
		stakeLengthLb.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.x * inEditingSegment.transform.localScale.x);
		stakeWidthLb.text  = string.Format ("{0:0.00}", inEditingStake.transform.localScale.z * inEditingSegment.transform.localScale.y);
		float offsetBounds = segment.GetComponent<linkProperties> ().thisSegmentProperties [1] / 2;
		stakeOffsetMin.text = string.Format ("{0:0.000}", -offsetBounds);
		stakeOffsetMax.text = string.Format ("{0:0.000}", offsetBounds);
		if (inEditingStake.name == "0") {
			float absDist = Mathf.Abs(inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x);
			stakeInterDistHint.text = string.Format ("{0:0.00}",absDist);
			stakeInterDistLb.text = string.Format ("{0:0.00}", absDist );

		} else {
			int thisStk = int.Parse (inEditingStake.name);

			int prevStk = thisStk - 1;
			string pstkSt = prevStk.ToString ();
			GameObject prevStake = inEditingSegment.transform.Find (pstkSt).gameObject ;
			stakeInterDistLb.text = string.Format ("{0:0.00}",Mathf.Abs( inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x - prevStake.transform.localPosition.x * inEditingSegment.transform.localScale.x));

			stakeInterDistHint.text = string.Format ("{0:0.00}",Mathf.Abs( inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x - prevStake.transform.localPosition.x * inEditingSegment.transform.localScale.x));
		}
	}
	private void OpenPropertyWindowBalcony(GameObject inEditingStake,GameObject inEditingSegment){
		stakeWorkingPhase = 1;
		propertyWindowBal.SetActive (true);
		stakeInEditing = inEditingStake;
		stakeHeighthintBal.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.y * inEditingSegment.transform.localScale.z);
		stakeLengthHintBal.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.x * inEditingSegment.transform.localScale.x);
		stakeWidthHintBal.text  = string.Format ("{0:0.00}", inEditingStake.transform.localScale.z * inEditingSegment.transform.localScale.y);
		           
		float localToWorldOffset=(inEditingStake.transform.localPosition.y-.5f)*inEditingStake.transform.parent.GetComponent<linkProperties>().thisSegmentProperties[1]/.5f;
		if (isEmbossed)
			stakeOffsetHintBal.text = string.Format ("{0:0.000}", localToWorldOffset);
		else
			stakeOffsetHintBal.text = string.Format ("{0:0.00}", inEditingStake.transform.localPosition.y);
		stakeHeightLbBal.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.y * inEditingSegment.transform.localScale.z);
		stakeLengthLbBal.text = string.Format ("{0:0.00}", inEditingStake.transform.localScale.x * inEditingSegment.transform.localScale.x);
		stakeWidthLbBal.text  = string.Format ("{0:0.00}", inEditingStake.transform.localScale.z * inEditingSegment.transform.localScale.y);

		float offsetBounds = segment.GetComponent<linkProperties> ().thisSegmentProperties [1] / 2;
		stakeOffsetMinBal.text = string.Format ("{0:0.000}", -offsetBounds);
		stakeOffsetMaxBal.text = string.Format ("{0:0.000}", offsetBounds);

		if (inEditingStake.name == "0") {
			float absDist = Mathf.Abs (inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x);
			stakeInterDistHintBal.text = string.Format ("{0:0.00}", absDist);
			stakeInterDistLbBal.text = string.Format ("{0:0.00}", absDist);
		}else {
			int thisStk = int.Parse (inEditingStake.name);

			int prevStk = thisStk - 1;
			string pstkSt = prevStk.ToString ();
			GameObject prevStake = inEditingSegment.transform.Find (pstkSt).gameObject ;
			stakeInterDistLbBal.text = string.Format ("{0:0.00}",Mathf.Abs( inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x - prevStake.transform.localPosition.x * inEditingSegment.transform.localScale.x));

			stakeInterDistHintBal.text = string.Format ("{0:0.00}",Mathf.Abs( inEditingStake.transform.localPosition.x * inEditingSegment.transform.localScale.x - prevStake.transform.localPosition.x * inEditingSegment.transform.localScale.x));
		}
		//Se è la prima volta che apriamo la finestra per il componente, allora non avrà uno script stakeProperties: alcune variabili saranno settate a valori standard
		if (!inEditingStake.GetComponent<stakeProperties> ()) {
			basePlateHintBalHeight.text = "0";
			basePlateHintBalWidth.text = "0";
			basePlateLbBalHeight.text = "0";
			basePlateHintBalWidth.text = "0";
			stakeHandraiLblWidth.text = ".08";
			stakeHandrailLbHeight.text  = ".05";
			stakeHandrailHintWidth.text  = ".08f";
			stakeHandrailHintHeight.text  = ".05f"; 
			basePlateHeight = 0;
			basePlateWidth = 0;
			handrailWidth=.08f;
			handrailHeight=.05f;


		} else {
			basePlateHintBalHeight.text = inEditingStake.GetComponent<stakeProperties>().basePlateHeight.ToString();
			basePlateHintBalWidth.text =inEditingStake.GetComponent<stakeProperties>().basePlateWidth.ToString();
			basePlateLbBalHeight.text =inEditingStake.GetComponent<stakeProperties>().basePlateHeight.ToString();
			basePlateHintBalWidth.text = inEditingStake.GetComponent<stakeProperties> ().basePlateWidth.ToString();
			basePlateHeight =inEditingStake.GetComponent<stakeProperties>().basePlateHeight;
			basePlateWidth =  inEditingStake.GetComponent<stakeProperties> ().basePlateWidth;
			stakeHandraiLblWidth.text  =  inEditingStake.GetComponent<stakeProperties>().handrailw.ToString();
			stakeHandrailLbHeight.text  =  inEditingStake.GetComponent<stakeProperties>().handrailh.ToString();
			stakeHandrailHintWidth.text  =  inEditingStake.GetComponent<stakeProperties>().handrailw.ToString ();
			stakeHandrailHintHeight.text = inEditingStake.GetComponent<stakeProperties>().handrailh.ToString();
			handrailWidth= inEditingStake.GetComponent<stakeProperties>().handrailw;
			handrailHeight= inEditingStake.GetComponent<stakeProperties>().handrailh;

		}
			
	
	}
	public void DismissChanges(){
		resetInputFields ();
		stakeWorkingPhase = 0;
			
		propertyWindow.SetActive (false);
		propertyWindowBal.SetActive (false);
	}
	public void DeleteAStake(){
		resetInputFields ();
		stakeWorkingPhase = 0;
		Destroy (stakeInEditing);
		Debug.Log (stakeInEditing + " dovrebbe essere in corso di distruzione");
		StartCoroutine (yieldDestruction (segment));

		propertyWindow.SetActive (false);
		propertyWindowBal.SetActive (false);
	}
	public void ApplyStakeChanges(){
		switch (StaticVariables.workMode) {
		case 0:
			applyStakeChangesRecinzoni ();
			break;

		case 1:
			applyStakeChangesBalconi ();
			break;
		}

	}


	private void applyStakeChangesRecinzoni(){
		segment = stakeInEditing.transform.parent.gameObject;
		int stakeNo = int.Parse (stakeInEditing.name);
		float newStakeWidth;
		float newStakeLength;
		float newStakeHeight;
		float newXPos = 0;
		if(stakeWidthLb.text!="")
			newStakeWidth = float.Parse (stakeWidthLb.text);
		else
			newStakeWidth = float.Parse (stakeWidthHint.text);

		if(stakeLengthLb.text!="")
			newStakeLength = float.Parse (stakeLengthLb.text);
		else
			newStakeLength = float.Parse (stakeLengthHint.text);

		if(stakeHeightLb.text!="")
			newStakeHeight = float.Parse (stakeHeightLb.text);
		else
			newStakeHeight = float.Parse (stakeHeighthint.text);

		if(stakeInterDistLb.text!="")
			newXPos = float.Parse (stakeInterDistLb.text);// / segment.transform.localScale.x;
		else
			newXPos=float.Parse(stakeInterDistHint.text);
		float instantiatingPosition;
		if (stakeNo == 0) {

			//	Destroy (stake);
			//	StartCoroutine (yieldDestruction (segment, autoStakeDim, -newXPos / segment.transform.localScale.x, true));
			instantiatingPosition=-newXPos / segment.transform.localScale.x;


		} else { //La distanza viene calcolata come incremento rispetto al palo precedente
			GameObject stake =stakeInEditing;
			int prev = int.Parse (stakeInEditing.name) - 1;
			GameObject prevStake = segment.transform.Find (prev.ToString ()).gameObject ;
			//StartCoroutine (yieldDestruction (segment, autoStakeDim, -newXPos / segment.transform.localScale.x+prevStake.transform.localPosition.x, true));
			instantiatingPosition=-newXPos / segment.transform.localScale.x+prevStake.transform.localPosition.x;
		}
		autoStakeDim = new Vector3 (newStakeLength, newStakeHeight, newStakeWidth);
		if (stakeOffsetLb.text != "")
			manualOffset  = Mathf.Clamp (float.Parse (stakeOffsetLb.text), float.Parse (stakeOffsetMin.text), float.Parse (stakeOffsetMax.text));
		else
			manualOffset  = Mathf.Clamp (float.Parse (stakeOffsetHint.text), float.Parse (stakeOffsetMin.text), float.Parse (stakeOffsetMax.text));
		manualOffset = manualOffset  / float.Parse (stakeOffsetMax.text)*segment.transform.localScale.z;

		Destroy (stakeInEditing );
		StartCoroutine (yieldDestruction (segment, autoStakeDim, instantiatingPosition, true));
		resetInputFields ();
		propertyWindow.SetActive (false);
		stakeWorkingPhase = 0;

	}
	private void applyStakeChangesBalconi(){
		//Innanzitutto aggiungo stakeProperties al paletto, così ci immagazzino delle informazioni utili
		stakeInEditing.AddComponent<stakeProperties>();
		segment = stakeInEditing.transform.parent.gameObject;
		int stakeNo = int.Parse (stakeInEditing.name);
		float newStakeWidth;
		float newStakeLength;
		float newStakeHeight;
		float newXPos = 0;
		if(stakeWidthLbBal.text!="")
			newStakeWidth = float.Parse (stakeWidthLbBal.text);
		else
			newStakeWidth = float.Parse (stakeWidthHintBal.text);

		if(stakeLengthLbBal.text!="")
			newStakeLength = float.Parse (stakeLengthLbBal.text);
		else
			newStakeLength = float.Parse (stakeLengthHintBal.text);

		if(stakeHeightLbBal.text!="")
			newStakeHeight = float.Parse (stakeHeightLbBal.text);
		else
			newStakeHeight = float.Parse (stakeHeighthintBal.text);

		if(stakeInterDistLbBal.text!="")
			newXPos = float.Parse (stakeInterDistLbBal.text);// / segment.transform.localScale.x;
		else
			newXPos=float.Parse(stakeInterDistHintBal.text);
		if (basePlateLbBalHeight.text != "")
			basePlateHeight = float.Parse (basePlateLbBalHeight.text);
		else
			basePlateHeight = float.Parse (basePlateHintBalHeight.text);
		if (basePlateLbBalWidth.text != "")
			basePlateWidth = float.Parse (basePlateLbBalWidth.text);
		else
			basePlateWidth = float.Parse (basePlateHintBalWidth.text);
		if (stakeHandraiLblWidth.text != "")
			handrailWidth = float.Parse (stakeHandraiLblWidth.text);
		else
			handrailWidth = float.Parse (stakeHandrailHintWidth.text);
		if (stakeHandrailLbHeight.text != "")
			handrailHeight = float.Parse (stakeHandrailLbHeight.text);
		else
			handrailHeight = float.Parse (stakeHandrailHintHeight.text);
		float instantiatingPosition;
		if (stakeNo == 0) {

			//	Destroy (stake);
			//	StartCoroutine (yieldDestruction (segment, autoStakeDim, -newXPos / segment.transform.localScale.x, true));
			instantiatingPosition=-newXPos / segment.transform.localScale.x;


		} else { //La distanza viene calcolata come incremento rispetto al palo precedente
			GameObject stake =stakeInEditing;
			int prev = int.Parse (stakeInEditing.name) - 1;
			GameObject prevStake = segment.transform.Find (prev.ToString ()).gameObject ;
			//StartCoroutine (yieldDestruction (segment, autoStakeDim, -newXPos / segment.transform.localScale.x+prevStake.transform.localPosition.x, true));
			instantiatingPosition=-newXPos / segment.transform.localScale.x+prevStake.transform.localPosition.x;
		}
		autoStakeDim = new Vector3 (newStakeLength, newStakeHeight, newStakeWidth);
		if (!isEmbossed) {
			if (stakeOffsetLbBal.text != "")
				manualOffset = Mathf.Clamp (float.Parse (stakeOffsetLbBal.text), float.Parse (stakeOffsetMinBal.text), float.Parse (stakeOffsetMaxBal.text));
			else
				manualOffset = Mathf.Clamp (float.Parse (stakeOffsetHintBal.text), float.Parse (stakeOffsetMinBal.text), float.Parse (stakeOffsetMaxBal.text));
			manualOffset = manualOffset / float.Parse (stakeOffsetMaxBal.text) * segment.transform.localScale.z;
		} else {
			if (stakeOffsetLbBal.text != "") {
				manualOffset = float.Parse (stakeOffsetLbBal.text);
				zInf = float.Parse (stakeZInfLbBal.text);
			} else {
				manualOffset = float.Parse (stakeOffsetHintBal.text);
				zInf = float.Parse (stakeZInfHintBal.text );
			}
		}

	
		Destroy (stakeInEditing );
		StartCoroutine (yieldDestruction (segment, autoStakeDim, instantiatingPosition, true));
		resetInputFields ();

		propertyWindowBal.SetActive (false);
		stakeWorkingPhase = 0;

	}

	void resetInputFields(){
		for (int i = 0; i < MIF.Length; i++) {
			MIF [i].ClearText ();
		}
	}

	void OnDisable(){
		segment.GetComponent<MeshRenderer> ().material = originalMat;
		GameObject[] MisureSchif = GameObject.FindGameObjectsWithTag ("stakeMeasures");
		for(int i=0;i<MisureSchif.Length;i++)
			Destroy(MisureSchif[i]);
		//GameObject.FindGameObjects ("stakeMeasures");
		createFenceSpaces();
	
	}
		
	/// <summary>
	/// Riposiziona correttamente il paletto sul dorso superiore del segmento 
	/// </summary>
	void recalculateStakeHeightPosition(GameObject stke,float rightHeight){
		
		Vector3 absolutePosition=stke.transform.position;
		absolutePosition=new Vector3(absolutePosition.x,100,absolutePosition.z);
		float stakeStartingH=0;

		Ray ray=new Ray(absolutePosition ,Vector3.down);
		RaycastHit[] info;
		info = Physics.RaycastAll (ray);
		for (int i = 0; i < info.Length; i++) {
			if (info [i].collider.tag == "segment") {
				Vector3 localPoint = info [i].point;
				stakeStartingH = localPoint.y;
			}
		}
		stke.transform.position=new Vector3(stke.transform.position.x,stakeStartingH+(rightHeight*.5f) ,stke.transform.position.z);

	}
	/// <summary>
	/// Crea gli spazi in cui creare le ringhiere della recinzione
	/// </summary>
	void createFenceSpaces(){
		int theseStakesLength=0;
		int counted = 0;
		GameObject[] totalTagged = GameObject.FindGameObjectsWithTag ("stake");
		for (int i = 0; i < segment.transform.childCount; i++) {
			if (segment.transform.GetChild (i).tag == "stake")
				theseStakesLength++;
		}
		GameObject[] stakes=new GameObject[theseStakesLength];

		for (int z = 0; z < totalTagged.Length; z++) {
			if (totalTagged [z].transform.parent == segment.transform) {
				stakes [counted] = totalTagged [z];
				//stakes [counted].name = counted.ToString ();
				counted++;

			}
		}
		GameObject[] orderedStakes = new GameObject [stakes.Length];
		for (int s = 0; s < stakes.Length; s++) {
			for (int j = 0; j < stakes.Length; j++)
				if (float.Parse (stakes [j].name) == s)
					orderedStakes [s] = stakes [j];
			
		}

		for (int i = 0; i < counted-1; i++) {
			fenceBoxCreator (orderedStakes [i], orderedStakes [i + 1], i.ToString (),true);


		}



		if (segment.GetComponent<linkProperties> ().previousSegment) {
			int otherStakesLength=0;
			int countedOther = 0;

			for (int z = 0; z < totalTagged.Length; z++) {

				if (totalTagged [z].transform.parent == segment.GetComponent<linkProperties> ().previousSegment.transform) {
					countedOther++;

				}

			}
			int assigner = 0;
			GameObject [] stakesOther= new GameObject[countedOther];
			for (int f = 0; f < totalTagged.Length ; f++) {
				if (totalTagged [f].transform.parent == segment.GetComponent<linkProperties> ().previousSegment.transform) {

					stakesOther [assigner] = totalTagged [f];
					assigner++;
				}
			}
			GameObject[] stakesOtherOrdered = new GameObject[stakesOther.Length];
			for (int s = 0; s < stakesOther.Length; s++) {
				for (int j = 0; j < stakesOther.Length; j++)
					if (float.Parse (stakesOther [j].name) == s)
						stakesOtherOrdered [s] = stakesOther [j];

			}
			GameObject lastStake=null;
		
			if (stakesOtherOrdered.Length > 0) {
		
				lastStake = stakesOtherOrdered [0];
				for (int d = 0; d < stakesOtherOrdered.Length; d++) {
					if (float.Parse (stakesOtherOrdered [d].name) > float.Parse (lastStake.name))
						lastStake = stakesOtherOrdered [d];
				}
				if (countedOther > 0) {
					fenceBoxCreator (lastStake, orderedStakes  [0], "interFence",false);
				}
			}
		}
		if (segment.GetComponent<linkProperties> ().attachedSegment) {
			if (segment.GetComponent<linkProperties> ().attachedSegment.transform.Find ("0")) {
				int theseStakesLengthZ=0;
				int countedZ = 0;
				GameObject[] totalTaggedZ = GameObject.FindGameObjectsWithTag ("stake");
				for (int i = 0; i < segment.transform.childCount; i++) {
					if (segment.transform.GetChild (i).tag == "stake")
						theseStakesLengthZ++;
				}
				GameObject[] stakesZ=new GameObject[theseStakesLength];

				for (int z = 0; z < totalTaggedZ.Length; z++) {
					if (totalTaggedZ [z].transform.parent == segment.transform) {
						stakesZ [countedZ] = totalTaggedZ [z];
						//stakes [counted].name = counted.ToString ();
						countedZ++;

					}
				}
				GameObject[] orderedStakesZ = new GameObject [stakesZ.Length];
				for (int s = 0; s < stakesZ.Length; s++) {
					for (int j = 0; j < stakesZ.Length; j++)
						if (float.Parse (stakesZ [j].name) == s)
							orderedStakesZ [s] = stakesZ [j];

				}
				fenceBoxCreator (orderedStakesZ[stakes.Length-1], segment.GetComponent<linkProperties> ().attachedSegment.transform.Find ("0").gameObject, "interFence",false);
			}
		}
	}
	void fenceBoxCreator(GameObject firstStake,GameObject secondstake,string fenceBoxName,bool parentToSegment){
			float centreOfInstantiationX = (secondstake.transform.position.x + firstStake.transform.position.x) / 2;
			float centreOfInstantiatonY = 0;
			float fenceHeight = 0;
		if (firstStake.transform.localScale.y >= secondstake.transform.localScale.y) {
				centreOfInstantiatonY =firstStake.transform.position.y;
			fenceHeight =firstStake.transform.localScale.y * firstStake.transform.parent.transform.localScale.z;
			} else {
			centreOfInstantiatonY =secondstake.transform.position.y;
			fenceHeight =secondstake.transform.parent.GetComponent<linkProperties>().thisSegmentProperties[0]+secondstake.transform.localScale.y *secondstake.transform.parent.transform.localScale.z;
			}
		float centreOfInstantiationZ =  (secondstake.transform.position.z + firstStake.transform.position.z) / 2;
			GameObject looker = new GameObject ();
			looker.name = "looker";
			looker.transform.position =firstStake.transform.position;
			looker.transform.LookAt(secondstake.transform );
			float angle = Vector3.Angle (looker.transform.eulerAngles, Vector3.zero );
			GameObject fenceSBox=GameObject.CreatePrimitive (PrimitiveType.Cube);
			fenceSBox.GetComponent<MeshRenderer> ().material = Resources.Load ("fenceBox") as Material;
			fenceSBox.name = "Box" + fenceBoxName;
			fenceSBox.tag = "fence";
		fenceSBox.AddComponent<fenceBoxScript> ();
			fenceSBox.transform.eulerAngles = looker.transform.eulerAngles;
		float effDist=Mathf.Abs(firstStake.transform.localPosition.x*segment.transform.localScale.x-secondstake.transform.localPosition.x*segment.transform.localScale.x);

		fenceSBox.transform.localScale = new Vector3 (0.05f, fenceHeight,effDist);
		fenceSBox.GetComponent<fenceBoxScript> ().firstPole = firstStake;
		fenceSBox.GetComponent<fenceBoxScript> ().secondPole = secondstake;
		if (parentToSegment) {
			fenceSBox.transform.eulerAngles = new Vector3 (0, fenceSBox.transform.eulerAngles.y, fenceSBox.transform.eulerAngles.z);
			fenceSBox.transform.parent = segment.transform;
		}
			fenceSBox.transform.position = new Vector3 (centreOfInstantiationX, centreOfInstantiatonY, centreOfInstantiationZ);
		/*
		if (fenceBoxName == "interFence") {
			Vector3 orginalScale = fenceSBox.transform.localScale;
			Vector3 originalRot = fenceSBox.transform.eulerAngles;
			fenceSBox.transform.parent = firstStake.transform.parent;
			fenceSBox.transform.eulerAngles = originalRot;

		}
		*/
		if (fenceBoxName == "interFence") {
			fenceSBox.transform.localScale = new Vector3 (fenceSBox.transform.localScale.x, fenceSBox.transform.localScale.y, Vector3.Distance (firstStake.transform.position, secondstake.transform.position));
			fenceSBox.name = "BoxinterFence_" + firstStake.transform.parent.name;

		}
			Destroy (looker);
		}
	/// <summary>
	/// Sposta la ringhiera a sbalzo fuori dal muretto
	/// </summary>
	/// <param name="offset">L'Offset di sbalzo.</param>
	/// <param name="infPart">La lunghezza che sporge in giu dal piano di calpestìo.</param>
	/// <param name="stakes">gli stakes.</param>
	void moveFenceOut(float offset, float infPart, GameObject stakeInEd){
		
		//Il totalOffset non va calcolato nuovamente come giù, perchè sennò lo addiziona, bisogna trovare un'altra via

	float totalOffset=offset + stakeInEd.transform.parent.GetComponent<linkProperties> ().thisSegmentProperties [1] / 2;

		float offsetTransposed = offset * .5f / (stakeInEd.transform.parent.GetComponent<linkProperties> ().thisSegmentProperties [1])+.5f;


		float zInf = 0;
		if(stakeZInfLbBal.text!="")
		    zInf=float.Parse (stakeZInfLbBal.text);
			else
			zInf=float.Parse(stakeZInfHintBal.text);

		float stakeTotalheight = 0;
		if (stakeHeightLbBal.text != "")
			stakeTotalheight = float.Parse (stakeHeightLbBal.text)+zInf;
		else
			stakeTotalheight = float.Parse (stakeHeighthintBal.text)+zInf;

		float 	yPos = (stakeTotalheight / 2 - zInf)/stakeInEd.transform.parent.transform.localScale.z ;   
		stakeInEd.transform.localPosition = new Vector3 (stakeInEd.transform.localPosition.x, offsetTransposed , yPos); 
		stakeInEd.transform.localScale = new Vector3 (stakeInEd.transform.localScale.x, stakeTotalheight / stakeInEd.transform.parent.localScale.z, stakeInEd.transform.localScale.z);
			addLinkPiece (infPart, stakeInEd,stakeTotalheight );
		stakeOffsetLbBal.text = offsetTransposed.ToString ();
		stakeOffsetHintBal.text = offsetTransposed.ToString ();
	}


	/// <summary>
	/// Aggiunge i tondini di collegamento col balcone per una ringhiera a sbalzo
	/// </summary>
	void addLinkPiece(float infLength,GameObject parent,float absHeight){
		if (!parent.GetComponent<stakeProperties> ())
			parent.AddComponent<stakeProperties> ();
		if (parent.GetComponent<stakeProperties> ().link != null)
			Destroy (parent.GetComponent<stakeProperties> ().link);
		
		GameObject link = GameObject.CreatePrimitive (PrimitiveType.Cube);

		link.transform.localScale = new Vector3 (.01f, .01f, .50f);
	
		link.transform.parent = parent.transform;
		link.transform.localEulerAngles = Vector3.zero;

		link.transform.localPosition = new Vector3 (0, -0.5f+infLength/(0.5f*absHeight) , link.transform.localScale.z/2);
		link.transform.parent = parent.transform.parent;
		parent.GetComponent<stakeProperties> ().link = link;
	}


	/// <summary>
	/// Permette lo switch tra balcone con ringhiera a sbalzo e balcone con ringhiera on board
	/// </summary>
   public void switchBalconyType(){
		isEmbossed=(isEmbossed) ? isEmbossed = false : isEmbossed = true;
		if (isEmbossed) {
			embossedImg.gameObject.SetActive (true);
			standardImg.gameObject.SetActive (false);
		} else {
			embossedImg.gameObject.SetActive (false);
			standardImg.gameObject.SetActive (true);
		}
		OpenPropertyWindowBalcony (stakeInEditing, stakeInEditing.transform.parent.gameObject);
	}

   //Istanzia la basetta della ringhiera
	void instantiateBasePlate(Vector3 basePos,GameObject thisStake){
		if (!thisStake.GetComponent<stakeProperties> ())
			thisStake.AddComponent<stakeProperties> ();
		if (thisStake.GetComponent<stakeProperties> ().basePlate != null)
			Destroy (thisStake.GetComponent<stakeProperties> ().basePlate);
		GameObject basePlate = GameObject.CreatePrimitive (PrimitiveType.Cube);
		basePlate.transform.localScale = new Vector3 (basePlateWidth,basePlateHeight,basePlateWidth);
		basePlate.transform.position = basePos;
		basePlate.transform.eulerAngles = thisStake.transform.eulerAngles;
		thisStake.GetComponent<stakeProperties> ().basePlate = basePlate;
	}


	public void generateHandrailWithDim(float width, float height){
		handrailWidth = width;
		handrailHeight = height;
		generateHandrail ();
	}
	/// <summary>
	/// Questa funzione si occupa di generare automaticamente il corrimano per un dato balcone, trovando in automatico tutti i paletti
	/// </summary>
	public void generateHandrail(){
		//La gneerazione del corrimano avviene ad ogni modifica dei paletti. Questo per garantire il funzionamento in tutti i casi. Quindi il primo step è eliminare eventuali corrimano già presenti
		GameObject[] previousHandrail=GameObject.FindGameObjectsWithTag("handrail");
		for (int i = 0; i < previousHandrail.Length; i++)
			Destroy (previousHandrail[i]);
		if (handrailWidth != 0 && handrailHeight != 0) {
			VertexWelder vWInst = new VertexWelder ();
			List<GameObject> stakesInter = new List<GameObject> (); // In questa lista ci metto primo e ultimo stake di ciascun segmento. Mi serviranno dopo per i collegamenti "intersegmentali"



			//FindAllChildrenWithTag
			GameObject[] allTheSegments = GameObject.FindGameObjectsWithTag ("segment");
			for (int i = 0; i < allTheSegments.Length; i++) {
				List <GameObject> stakesInHere = new List<GameObject> ();
				for (int j = 0; j < allTheSegments [i].transform.childCount; j++)
					if (allTheSegments [i].transform.GetChild (j).tag == "stake") {
						stakesInHere.Add (allTheSegments [i].transform.GetChild (j).gameObject);
					}
				//ORDERED STAKES ERA QUI
				GameObject[] orderedStakesInHere = new GameObject[stakesInHere.Count];
				GameObject firstStakeIn = null;
				GameObject lastStakeIn = null;
				for (int d = 0; d < stakesInHere.Count; d++) {
					for (int s = 0; s < stakesInHere.Count; s++){
						if (int.Parse (stakesInHere [s].name) == d)
							orderedStakesInHere [d] = stakesInHere [s];
					//Non molto pulito
						if (d == 0)
							firstStakeIn = orderedStakesInHere [d];
						if (d == stakesInHere.Count-1)
							lastStakeIn = orderedStakesInHere [d];
					}
				}
				if (orderedStakesInHere.Length > 0) {
					stakesInter.Add (orderedStakesInHere [0]);
					stakesInter.Add (orderedStakesInHere [orderedStakesInHere.Length - 1]);
				}
				List <GameObject> segToConnect = new List<GameObject> ();
				for (int k = 0; k < stakesInHere.Count - 1; k++) {
					//Qua dobbiamo instanziare un cubetto tra i paletti
					GameObject handRailPiece = Instantiate (Resources.Load ("Cubetto") as GameObject);
					handRailPiece.name = "handRailSeg";
					handRailPiece.tag = "handrail";
					handRailPiece.GetComponent<linkProperties> ().isHandrail = true;
					Destroy(handRailPiece.GetComponent<MeshCollider>());
					Vector3 firstStakeTip = new Vector3 (orderedStakesInHere [k].transform.position.x, orderedStakesInHere [k].transform.position.y + orderedStakesInHere [k].transform.lossyScale.y/2, orderedStakesInHere [k].transform.position.z);
					Vector3 secondStakeTip = new Vector3 (orderedStakesInHere [k + 1].transform.position.x, orderedStakesInHere [k + 1].transform.position.y + orderedStakesInHere [k + 1].transform.lossyScale.y/2, orderedStakesInHere [k + 1].transform.position.z);
					handRailPiece.transform.position = firstStakeTip;
					handRailPiece.transform.LookAt (secondStakeTip);
					handRailPiece.transform.eulerAngles = new Vector3 (-90, handRailPiece.transform.eulerAngles.y + 90, 0);
				//	if (handRailPiece.transform.eulerAngles.x >0) {
				//		handRailPiece.transform.eulerAngles = new Vector3 (-90 + handRailPiece.transform.eulerAngles.x, -90, 90);
				//	}
					for (int child = 0; child < handRailPiece.transform.childCount; child++)
						Destroy (handRailPiece.transform.GetChild (child));
					float stakesDist = Vector3.Distance (firstStakeTip, secondStakeTip);
					handRailPiece.transform.localScale = new Vector3 (stakesDist,handrailHeight, handrailWidth);
					segToConnect.Add (handRailPiece);
			     
			
				}
				for (int q = 0; q < segToConnect.Count - 1; q++) {
					vWInst.doWelding (segToConnect [q], segToConnect [q + 1]);
				}	

				
			}
			//Creiamo un corrimano tra due segmenti adiacenti. Il paletto 0 va collegato con l'ultimo paletto del segmento precedente
			for (int i = 0; i < allTheSegments.Length-1; i++) {
				if (allTheSegments [i].GetComponent<linkProperties> ().attachedSegment) {
					GameObject[] myStakes = Helper.FindComponentsInChildrenWithTag<Transform> (allTheSegments [i], "stake", false);
					myStakes = Helper.reOrderedChildrens (myStakes);
					GameObject[] nextStakes = Helper.FindComponentsInChildrenWithTag<Transform> (allTheSegments [i + 1], "stake", false);
					nextStakes = Helper.reOrderedChildrens (nextStakes);
					if (myStakes.Length > 0 && nextStakes.Length > 0) {
						GameObject handRailINter = Instantiate (Resources.Load ("Cubetto") as GameObject);
						handRailINter.name = "handRailSeg";
						handRailINter.tag = "handrail";
						handRailINter.GetComponent<linkProperties> ().isHandrail = true;
						Vector3 firstStakeTip = new Vector3 (myStakes [myStakes.Length - 1].transform.position.x, myStakes [myStakes.Length - 1].transform.position.y + myStakes [myStakes.Length - 1].transform.lossyScale.y / 2, myStakes [myStakes.Length - 1].transform.position.z);
						Vector3 secondStakeTip = new Vector3 (nextStakes [0].transform.position.x, nextStakes [0].transform.position.y + nextStakes [0].transform.lossyScale.y / 2, nextStakes [0].transform.position.z);
						handRailINter.transform.position = firstStakeTip;
						handRailINter.transform.LookAt (secondStakeTip);
						handRailINter.transform.eulerAngles = new Vector3 (-90, handRailINter.transform.localEulerAngles.y + 90, 0);
						float interStakeDist = Vector3.Distance (firstStakeTip, secondStakeTip);
						handRailINter.transform.localScale = new Vector3 (interStakeDist, handrailHeight, handrailWidth);
					}
					
				}
			}





		}
	}

	/// <summary>
	/// Quando su un segmento sono già presenti paletti, ringhiere e box per ringhiere, è necessario cancellarli per far spazio alla roba nuova.
	/// </summary>
	void deletePreviousJob(){
		GameObject []allBoxes = GameObject.FindGameObjectsWithTag ("fence");
		for (int i = 0; i < allBoxes.Length; i++) {
			if (allBoxes [i].GetComponent<fenceBoxScript> ().firstPole.transform.parent.gameObject  == segment || allBoxes [i].GetComponent<fenceBoxScript> ().secondPole.transform.parent.gameObject == segment) {
				if (allBoxes [i].GetComponent<fenceBoxScript> ().myfence != null)
					Destroy (allBoxes [i].GetComponent<fenceBoxScript> ().myfence);
				Destroy (allBoxes [i]);
					
					

			}
		}
	}

	}



	

