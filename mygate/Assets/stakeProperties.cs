﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stakeProperties : MonoBehaviour {
	public GameObject link;
	public GameObject basePlate;
	public GameObject handrail;
	public float basePlateWidth;
	public float basePlateHeight;
	public float handrailw=0.08f;
	public float handrailh=.05f;

	void OnDestroy(){ 
		Destroy (link);
		Destroy (basePlate);
	}
}
