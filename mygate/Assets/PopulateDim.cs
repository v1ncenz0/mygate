﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateDim : MonoBehaviour {
	public Text GateHeight;
	public InputField h;
	public InputField w;
	public Text GateHeightpLh;
	public Text GateWidth;
	public Text GateWidthpLh;

	// Use this for initialization
	void Start () {
		if (StaticVariables.comingFromFence == false) {
			GateHeight.text = (1000 * SaveLoad.gateHeight).ToString ();
			GateHeightpLh.text = (1000 * SaveLoad.gateHeight).ToString ();
			GateWidth.text = (1000 * SaveLoad.gateLength).ToString ();

			GateWidthpLh.text = (1000 * SaveLoad.gateLength).ToString ();
			w.text = (1000 * SaveLoad.gateLength).ToString ();
			h.text = (1000 * SaveLoad.gateHeight).ToString ();
		} else {
			SaveLoad.gateHeight = 1000*Mathf.Round (SaveLoad.gateHeight * 100) / 100;
			SaveLoad.gateLength = 1000*Mathf.Round (SaveLoad.gateLength * 100) / 100;
			GateHeight.text = (SaveLoad.gateHeight).ToString ();
			GateHeightpLh.text = ( SaveLoad.gateHeight).ToString ();
			GateWidth.text = ( SaveLoad.gateLength).ToString ();

			GateWidthpLh.text = ( SaveLoad.gateLength).ToString ();
			w.text = ( SaveLoad.gateLength).ToString ();
			h.text = ( SaveLoad.gateHeight).ToString ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
