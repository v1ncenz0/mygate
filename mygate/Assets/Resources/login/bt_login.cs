﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_login : MonoBehaviour {


	public GameObject message;



	public void login(){

		string username = GameObject.Find ("Canvas/fieldset/username").GetComponent<InputField> ().text;
		string password = GameObject.Find ("Canvas/fieldset/password").GetComponent<InputField> ().text;

		webservice wb = webservice.InitWebService ();


		message.SetActive (true);
		message.GetComponent<Text>().text="Accesso in corso...";
		StartCoroutine(wb.login(username,password,(s)=>{
			if(s){
				message.SetActive (false);
				Application.LoadLevel ("gates");
			}else{
				message.SetActive (true);
				message.GetComponent<Text>().text="Username e Password non coincidono o non hai ancora un account";
			}
		}));
			

	}
}
