﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_enable_layer : MonoBehaviour {


	public ItemColor layer_color;



	public void setEnableLayer(){

        int id=this.GetComponent<MaterialUI.MaterialDropdown>().currentlySelected;

		GameObject.FindObjectOfType<ControlsDesign>().activateLayer(id);
		GameObject.FindObjectOfType<ControlsDesign>().setColorLayer(layer_color.id,id);

	}
}
