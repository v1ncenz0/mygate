﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bt_show_colorlibrary : MonoBehaviour {


	public bool is_frame = false;

	public void showColorLibrary(){
		GUIManager.showColorLibrary (true);
        GameObject.Find ("color_library(Clone)").GetComponent<params_colorlibrary> ().id_layer = GameObject.FindObjectOfType<ControlsDesign>().currentActive.numLayer;
		GameObject.Find ("color_library(Clone)").GetComponent<params_colorlibrary> ().is_frame = is_frame;

	}

}
