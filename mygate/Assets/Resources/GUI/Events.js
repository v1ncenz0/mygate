﻿#pragma strict

 
 function Start () 
{
    Application.ExternalCall("GetMessage");
   
	
}

function setAddMode(){
    
    GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setAddMode");
}


function setEditMode(){
	
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setEditMode");
}


function setDeleteMode(){
	
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setDeleteMode");
}



function commandLayer(cmd){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage(cmd);
}




function commandAction(cmd){
	GameObject.Find("Camera").GetComponent("Actions").SendMessage(cmd);


}



function changeLayer(id:int){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("activateLayer",id);
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("modifyDepth",false);
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("itemSelectionChanged");
	
}




function setElement(id:int){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setElement",id);

}

function setColor(id:int){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setColorByCursor",id);

}
function setColorHex(hex:String){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setColorByHex",hex);

}
function setColorFrame(id:int){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setColorFrame",id);

}

function setColorLayer(cmd:String){

	var prs=new ArrayList();
	 var split = new Array();  
	 var separator : char[] = [";"[0]];
	 
	split=cmd.Split(separator);
	
		
	for(var i=0;i<split.length;i++){
		prs.Add(split[i]);
	}

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setColorLayer",prs);
}


function resetColorLayer(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("resetColorLayer");
}


function getColorLayer(id_layer:int){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("getColorLayer",id_layer);
}


function getColorFrame(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("getColorFrame");
}

function moveCamera(degree:int){

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("cameraMove",degree);


}

function resetCamera(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("resetCamera");

}

function navigation(txt:String){

	var data=txt.ToString().Split(";"[0]);

	GameObject.Find("Camera").GetComponent("MoveCamera").SendMessage(data[0],parseInt(data[1]));
}



function setSceneVisible(visible:int){
	var v=false;
	if(visible==1)
		GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("showSceneForRender");
	else
		GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("ShowCAD");
	
		
	
}


function setSpecularMode(visible:int){
	var v=false;
	if(visible==1)
		v=true;

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setSpecularMode",v);
}


function invertSpecularMode(){
	

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("invertSpecularMode");
}


function setVisibilityLayer(txt:String){
	
	
	var data=txt.ToString().Split(";"[0]);
    
    var arr = new ArrayList(); 
    
    
    var v=false;
	if(parseInt(data[1])==1)
		v=true;
    
    arr.Add(parseInt(data[0]));
    arr.Add(v);
    
    

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setVisibilityLayer",arr);

}

function noGrid(visible:int){
	var v=false;
	if(visible==1)
		v=true;

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("noGrid",v);



}


function invertGrid(){
	
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("invertGridShow");

}


function fillGrid(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("fillGrid");
}

function eraseGrid(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("eraseGrid");
}

function captureScreen(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("CaptureScreen");
}

function saveGate(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("saveGate");
}

function keepGate(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("keepingGateFromDestr");
}

function exportToOBJ(){
	
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("exportToOBJ");
	
	
}

function setSaveWithScreen(visible:int){
	
	
	var v=false;
	if(visible==1)
		v=true;

    

	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("setSaveWithScreen",v);

}



function invertSceneForRender(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("invertSceneForRender");

}


function showSceneForRender(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("showSceneForRender");

}


function showCAD(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("showCAD");

}

function init(cmd:String){

	var prs=new ArrayList();
	 var split = new Array();  
	 var separator : char[] = ["&"[0]];
	 
	split=cmd.Split(separator);
	
		
	for(var i=0;i<split.length;i++){
		prs.Add(split[i]);
	}
	
	
	
	
	GameObject.Find("Intro").GetComponent("intro").SendMessage("setParameters",prs);
	GameObject.Find("Intro").GetComponent("intro").SendMessage("startWebGL");
	
	
}

function showPrice(){
	
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("showPrice");
	
	
}


function back(){
	GameObject.Find("Camera").GetComponent("ControlsDesign").SendMessage("cancelGate");
}