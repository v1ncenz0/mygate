﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Text;
using SimpleJSON;


/**
 * Classe che gestisce le modifiche alla gui.
 */
public class GUIManager : MonoBehaviour
{



	public static GameObject cameracontrols;
	public static GameObject library;
	public static GameObject layermanager;
	public static GameObject colorlibrary;
	
	public static GameObject toolbar;
	public static GameObject statusbar;
	
	public static GameObject help;
	
	public static GameObject confirmBox;
	public static string confirmBoxObjectName;
	public static string confirmBoxComponentName;
	public static string confirmBoxCommandName;

	public static GameObject dialogBox;
	public static GameObject waitingBox;
	public static GameObject loadingPanel;
	public static GameObject detailBox;



	public static void InitDialogs(){


        //instatiate dialogs

        GUIManager.LoadGUIWindow(ref GUIManager.confirmBox, "GUI/confirmbox");
        GUIManager.LoadGUIWindow(ref GUIManager.dialogBox, "GUI/dialogbox");
        GUIManager.LoadGUIWindow(ref GUIManager.waitingBox, "GUI/waitingbox");
        GUIManager.LoadGUIWindow(ref GUIManager.loadingPanel, "GUI/loading");
        GUIManager.LoadGUIWindow(ref GUIManager.detailBox, "GUI/detailBox");

		//set library and layer_manager to GUIManager
        GUIManager.layermanager=GameObject.Find("layer_manager");
		GUIManager.cameracontrols=GameObject.Find("camera_controls");
		GUIManager.toolbar=GameObject.Find("Toolbar");
		GUIManager.statusbar=GameObject.Find("StatusBar");
		GUIManager.help=GameObject.Find("help");

		

		//adjust loadingPanel
		GUIManager.loadingPanel.transform.Find("bt_back").gameObject.SetActive(false);

       
	}





	public static void LoadGUIWindow(ref GameObject obj,String file){

		obj=Instantiate(Resources.Load(file)) as GameObject;
		if (GameObject.Find ("Canvas") != null) {
			obj.transform.SetParent (GameObject.Find ("Canvas").transform);
			obj.GetComponent<RectTransform> ().offsetMin = new Vector2 (0, 0);
			obj.GetComponent<RectTransform> ().offsetMax = new Vector2 (0, 0);
			obj.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			obj.SetActive (false);
		}

	}


	/// <summary>
	/// Inizializza la GUI
	/// </summary>
	public static void createGUI(){





		//add color libray
		LoadGUIWindow (ref GUIManager.colorlibrary, "color_library/color_library");

		//add color to color library
		GameObject colorlibrarycontent=GUIManager.colorlibrary.transform.Find("Panel/Scroll View/Viewport/Content").gameObject;


		if (Library.ColorsAvaible.Count > 0) {
			for (int i = 0; i < Library.ColorsAvaible.Count; i++) {
				ItemColor c = (ItemColor)Library.ColorsAvaible [i];

				//crea il pulsante nella scrollview Library
				GameObject bt=Instantiate(Resources.Load("color_library/bt_color")) as GameObject;
				bt.transform.SetParent (colorlibrarycontent.transform);
				bt.transform.localScale = new Vector3 (1, 1, 1);
				bt.transform.Find ("title").GetComponent<Text> ().text = c.name;
				bt.GetComponent<bt_color> ().colorItem = c;


				if (c.image_path == "")
					bt.GetComponent<bt_color> ().setColorBackground (c.color);
				else
					bt.GetComponent<bt_color> ().setImage(c.texture);	
				
			}
		
		}

		GUIManager.colorlibrary.SetActive (false);

        GUIManager.LoadGUIWindow(ref GUIManager.library, "library/library");
		
        //add element to  library 

        GameObject librarycontent = GUIManager.library.transform.Find("Panel/ScrollView/Viewport/Content").gameObject;

		if (librarycontent != null) {
			if (Library.ItemsAvaible.Count > 0) {
				for (int i = 0; i < Library.ItemsAvaible.Count; i++) {
					Item item = (Item)Library.ItemsAvaible [i];
					//crea il pulsante nella scrollview Library
					GameObject bt = Instantiate (Resources.Load ("library/bt_element")) as GameObject;
					bt.transform.SetParent (librarycontent.transform);
					bt.transform.localScale = new Vector3 (1, 1, 1);
					bt.transform.Find ("id").GetComponent<Text> ().text = item.id.ToString ();
					bt.transform.Find ("cells").GetComponent<Text> ().text = item.cells.x.ToString () + " x " + item.cells.y.ToString ();

					bt.GetComponent<bt_element> ().id_element = item.id;
					ManageCoroutines.lauchCoroutines (bt.GetComponent<bt_element> ().setImage (item.iconPath));

				}
			}
		}
        		
        GUIManager.library.SetActive(false);


		GUIManager.closeHelp ();


		//imposta la statusbar
		/*Vector2 dimensionGross = GateDimUtility.calcolateDimensionGross ();
		if(statusbar!=null)
			statusbar.transform.Find ("message").GetComponent<Text> ().text = "Dimensioni spazio disponibile: " + dimensionGross.x.ToString () + " x " + dimensionGross.y.ToString () + " mm";
		*/
	
	}






	/***** COLOR BOX ******/


	public static void showColorLibrary(bool value){

		if (value)
			GUIManager.disableCursorElement ();
		else
			GUIManager.activeCursorElement ();

		GUIManager.colorlibrary.SetActive (value);
	}


	public void hideColorLibrary(){
		GUIManager.showColorLibrary (false);
	}

	public static void changeColorLayer(int id_layer,ItemColor colorItem){
		

			GameObject.Find ("App Bar/bt_color_layer").GetComponentInChildren<RawImage> ().texture = null;
			if (colorItem.texture) {
            GameObject.Find ("App Bar/bt_color_layer").GetComponentInChildren<RawImage> ().texture = colorItem.texture;
            GameObject.Find ("App Bar/bt_color_layer").GetComponentInChildren<RawImage> ().color = Color.white;
			} else
            GameObject.Find ("App Bar/bt_color_layer").GetComponentInChildren<RawImage> ().color = colorItem.color;


            GameObject.Find ("App Bar/Select_level").GetComponent<bt_enable_layer> ().layer_color = colorItem;
		
	}


    public static void changeColorFrame(ItemColor colorItem)
    {
        GameObject.Find("App Bar/bt_color_frame").GetComponentInChildren<RawImage>().texture = null;
        if (colorItem.texture)
        {
            GameObject.Find("App Bar/bt_color_frame").GetComponentInChildren<RawImage>().texture = colorItem.texture;
            GameObject.Find("App Bar/bt_color_frame").GetComponentInChildren<RawImage>().color = Color.white;
        }
        else
            GameObject.Find("App Bar/bt_color_frame").GetComponentInChildren<RawImage>().color = colorItem.color;

	}


	/***** END COLOR BOX ******/


    public static void showLibrary(bool value)
    {

        if (value)
            GUIManager.disableCursorElement();
        else
            GUIManager.activeCursorElement();

        GUIManager.library.SetActive(value);
    }



    public void hideLibrary()
    {
        GUIManager.showLibrary(false);
    }

   

    public void showLibrary()
    {
        GUIManager.showLibrary(true);

    }

	/***** CONFIRM BOX ******/

	public static void showConfirmBox(string title,string message,string objectname,string componentname,string commandname,string lbl_ok_button="OK",string lbl_cancel_button="ANNULLA"){
		GUIManager.disableCursorElement ();
		GUIManager.confirmBoxObjectName = objectname;
		GUIManager.confirmBoxComponentName = componentname;
		GUIManager.confirmBoxCommandName = commandname;
		GUIManager.confirmBox.transform.Find ("Panel/lbl_header").GetComponent<Text> ().text = title;
		GUIManager.confirmBox.transform.Find ("Panel/message").GetComponent<Text> ().text = message;
		GUIManager.confirmBox.transform.Find ("Panel/bt_ok").GetComponentInChildren<Text> ().text = lbl_ok_button;
		GUIManager.confirmBox.transform.Find ("Panel/bt_cancel").GetComponentInChildren<Text> ().text = lbl_cancel_button;

		GUIManager.confirmBox.SetActive (true);
	}

	public  void hideConfirmBox(){
		GUIManager.confirmBox.SetActive (false);
		GUIManager.activeCursorElement ();
	}

	public void okConfirmBox(){
		hideConfirmBox ();
		GameObject.Find(GUIManager.confirmBoxObjectName).GetComponent(GUIManager.confirmBoxComponentName).SendMessage(GUIManager.confirmBoxCommandName);

	}
	 
	/***** END CONFIRM BOX ******/


	/***** DIALOG BOX ******/

	public static void showDialogBox(string title,string message){
		GUIManager.disableCursorElement ();
		GUIManager.dialogBox.transform.Find ("Panel/lbl_header").GetComponent<Text> ().text = title;
		GUIManager.dialogBox.transform.Find ("Panel/message").GetComponent<Text> ().text = message;

		GUIManager.dialogBox.SetActive (true);
	}

	public void okDialogBox(){
		GUIManager.dialogBox.SetActive (false);
		GUIManager.activeCursorElement ();
	}

	/***** END DIALOG BOX ******/


	/***** WAITING BOX ******/

	public static void showWaitingBox(string title,string message,bool hideCursor=true){
		if(hideCursor)
			GUIManager.disableCursorElement ();
		GUIManager.waitingBox.transform.Find ("Panel/lbl_header").GetComponent<Text> ().text = title;
		GUIManager.waitingBox.transform.Find ("Panel/message").GetComponent<Text> ().text = message;

		GUIManager.waitingBox.SetActive (true);
	}


	public static void closeWaitingBox(){
		GUIManager.waitingBox.SetActive (false);
		//GUIManager.activeCursorElement ();
	}

	/***** END WAITING BOX ******/




	
	public static void disableCursorElement(){

		if(GameObject.FindObjectOfType<ControlsDesign> ())
			GameObject.FindObjectOfType<ControlsDesign> ().cursorObject.SetActive(false);
		if(GameObject.Find ("Camera/Camera cursor"))
			GameObject.Find ("Camera/Camera cursor").GetComponent<Camera> ().enabled = false;

	}

	public static void activeCursorElement(){
		if(GameObject.FindObjectOfType<ControlsDesign> ())
			GameObject.FindObjectOfType<ControlsDesign> ().cursorObject.SetActive(true);
		if(GameObject.Find ("Camera/Camera cursor"))
			GameObject.Find ("Camera/Camera cursor").GetComponent<Camera> ().enabled = true;
	}


	public static void hidePalettes(){
		GUIManager.library.SetActive (false);
		GUIManager.layermanager.SetActive (false);
		GUIManager.toolbar.SetActive (false);
	}

	public static void showPalettes(){
		GUIManager.library.SetActive (true);
		GUIManager.layermanager.SetActive (true);
		GUIManager.toolbar.SetActive (true);
	}




	public static void hideCameraControl(){
		GUIManager.cameracontrols.SetActive (false);
	}

	public static void showCameraControl(){
		GUIManager.cameracontrols.SetActive (true);
	}


	/// <summary>
	/// Imposta il pulsante edit della toolbar in enable (disabilita eventualmente il tasto delete)
	/// </summary>
	public static void setEditMode(){
		Button bt_edit = GameObject.Find ("Toolbar/bt_edit").GetComponent<Button> ();

		Image img_edit = GameObject.Find ("Toolbar/bt_edit").GetComponent<Image> ();
		Image img_delete = GameObject.Find ("Toolbar/bt_delete").GetComponent<Image> ();

		img_edit.color = bt_edit.colors.pressedColor;
		img_delete.color = Color.white;
	}


	/// <summary>
	/// Imposta il pulsante delete della toolbar in enable (disabilita eventualmente il tasto edit)
	/// </summary>
	public static void setDeleteMode(){
		Button bt_delete = GameObject.Find ("Toolbar/bt_delete").GetComponent<Button> ();

		Image img_edit = GameObject.Find ("Toolbar/bt_edit").GetComponent<Image> ();
		Image img_delete = GameObject.Find ("Toolbar/bt_delete").GetComponent<Image> ();

		img_delete.color = bt_delete.colors.pressedColor;
		img_edit.color = Color.white;
	}


	/// <summary>
	/// Imposta il pulsante griglia della toolbar in enable 
	/// </summary>
	public static void showGrid(){
		Button bt_grid = GameObject.Find ("Toolbar/bt_grid").GetComponent<Button> ();
		Image img_grid = GameObject.Find ("Toolbar/bt_grid").GetComponent<Image> ();
		img_grid.color = bt_grid.colors.pressedColor;

	}


	/// <summary>
	/// Imposta il pulsante griglia della toolbar in disable 
	/// </summary>
	public static void hideGrid(){
		GameObject bt_grid = GameObject.Find ("Toolbar/bt_grid");
		if (bt_grid) {
			Image img_grid = bt_grid.GetComponent<Image> ();
			img_grid.color = Color.white;
		}

	}

	

	
	/// <summary>
	/// Apre la schermate di loading
	/// </summary>
	public static void showLoading(){
		GUIManager.loadingPanel.SetActive (true);
	}

	/// <summary>
	/// Chiude la schermate di loading
	/// </summary>
	public static void hideLoading(){
		GUIManager.loadingPanel.SetActive (false);
	}

	/// <summary>
	/// Imposta il messaggio del pannello di loading
	/// </summary>
	/// <param name="message">Message.</param>
	public static void setLoadingMessage(string message,bool isError=false){
		if (GUIManager.loadingPanel) {
			GUIManager.loadingPanel.transform.Find ("message").GetComponent<Text> ().text = message;
			if (isError)
				GUIManager.loadingPanel.GetComponent<loadingPanel> ().showError ();
		}
	}

	/// <summary>
	/// Aggiorna il testo del prezzo
	/// </summary>
	/// <param name="price">Price.</param>
	public static void updateInvoice(double price){
		//GameObject.Find ("total").GetComponent<Text> ().text = price.ToString()+" €";
	}


	public static void showHelp(string title,string message){
		if (GUIManager.help != null) {
			GUIManager.help.transform.Find ("title").GetComponent<Text> ().text = title;
			GUIManager.help.transform.Find ("message").GetComponent<Text> ().text = message;
			GUIManager.help.SetActive (true);
		}
	}

	public static void closeHelp(){
		if(GUIManager.help!=null)
			GUIManager.help.SetActive (false);
	}


	public static void openDetailBox(ItemGate gate){
		detailBox.SetActive (true);
		detailBox.transform.Find ("Panel/title").GetComponent<Text> ().text = "CONFIGURAZIONE #" + gate.id.ToString ();

		detailBox.transform.Find ("Panel/ImageContainer/Image").GetComponent<Image> ().sprite = gate.image;
		detailBox.transform.Find ("Panel/ImageContainer/Image").GetComponent<Image> ().type = Image.Type.Simple;
		detailBox.transform.Find ("Panel/ImageContainer/Image").GetComponent<Image> ().preserveAspect = true;

        detailBox.transform.Find ("Panel/description").GetComponent<Text> ().text = "Tipologia :"+gate.type.name+"\nStile:"+gate.style.name+"\nForma:"+gate.geometry.name+"\nUltima modifica:"+gate.date;

		detailBox.transform.GetComponentInChildren<bt_mygate>().gate=gate;
		detailBox.transform.GetComponentInChildren<bt_buy>().id=gate.id;



	}

	public static void closeDetailBox(){
		detailBox.SetActive (false);
	}




    /****** INSPECTOR ******/

    public static void OpenInspector(ArrayList paramlist,string objectName, string componentName, string commandName){

        GameObject inspector = GameObject.Find("Canvas/Inspector");
        inspector inspector_class= inspector.GetComponent<inspector>();
        inspector_class.setWindow(paramlist,objectName,componentName,commandName);
    }

}
