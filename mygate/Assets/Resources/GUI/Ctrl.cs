﻿using UnityEngine;
using System.Collections;

/**
 * Gestisce l'input da tastiera
 */
public class Ctrl : MonoBehaviour
{
		// Flag indicante se siamo in "Modalità cancellazione"
		public static bool inDeleteMode = false;
		
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.A)) {
					inDeleteMode = true;
				}
				if (Input.GetKeyUp (KeyCode.A)) {
					inDeleteMode = false;
				}

				if (Input.GetKeyDown (KeyCode.LeftControl)) {
						inDeleteMode = true;
				}
				if (Input.GetKeyUp (KeyCode.LeftControl)) {
						inDeleteMode = false;
				}
				
		}


	/// <summary>
	/// Sets the edit mode.
	/// </summary>
	public static void setEditMode(){
		Ctrl.inDeleteMode = false;
		GUIManager.setEditMode ();
	}

	/// <summary>
	/// Sets the delete mode.
	/// </summary>
	public static void setDeleteMode(){
		Ctrl.inDeleteMode = true;
		GUIManager.setDeleteMode ();
	}

}
