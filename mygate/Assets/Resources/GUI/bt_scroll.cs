﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_scroll : MonoBehaviour {

	public GameObject scrollview;
	public enum ModeList{ScrollLeft=0,ScrollRight=1};
	public enum TypeList{Horizontal=0,Vertical=1};

	public float delta = 10f;
	public ModeList mode;
	public TypeList type=0;


	public void scroll(){
		//prendi il componente scrollview
		ScrollRect scroll=scrollview.GetComponent<ScrollRect>();


		if (type == TypeList.Horizontal) {
			if (mode == ModeList.ScrollLeft) {
				if (scroll.horizontalNormalizedPosition  > 0)
					scroll.horizontalNormalizedPosition = scroll.horizontalNormalizedPosition - delta;
			} else {
				if(scroll.horizontalNormalizedPosition <1)
					scroll.horizontalNormalizedPosition = scroll.horizontalNormalizedPosition + delta;
			}
		} else {
			if (mode == ModeList.ScrollLeft) {
				if (scroll.verticalNormalizedPosition  > 0)
					scroll.verticalNormalizedPosition = scroll.verticalNormalizedPosition - delta;
			} else {
				if (scroll.verticalNormalizedPosition  < 1)
					scroll.verticalNormalizedPosition = scroll.verticalNormalizedPosition + delta;
			}
		}


	}


}
