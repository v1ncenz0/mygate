﻿using UnityEngine;
using System.Collections;

public class wireframe : MonoBehaviour {

	GameObject root;

	void Start ()
	{
		root= transform.gameObject;
		SetDeactive ();

	}



	public void SetActive(){
		this.GetComponentInParent<MeshRenderer> ().enabled = true;

	}

	public void SetDeactive(){
		root.GetComponent<MeshRenderer> ().enabled = false;
	}

}