﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class inspector : MonoBehaviour {


    public GameObject inspector_panel;

    public string objectName;
    public string componentName;
    public string commandName;



	
    /// <summary>
    /// Crea la finestra con i parametri e imposta quale funzione richiamare quando viene cliccato il tasto Apply
    /// </summary>
    /// <param name="paramlist">Paramlist.</param>
    /// <param name="objectName">Object name.</param>
    /// <param name="componentName">Component name.</param>
    /// <param name="commandName">Command name.</param>
    public void setWindow(ArrayList paramlist,string objectName,string componentName,string commandName){

        this.objectName = objectName;
        this.componentName = componentName;
        this.commandName = commandName;


        //cancella tutto
        inspector_panel.transform.DestroyChildren();


        foreach(ItemParam p in paramlist){
            createParam(p);
        }

    }


    /// <summary>
    /// Applica le modifiche richiamando la funziona all'interno del gameobject specificato nella funzione setWindow.
    /// </summary>
    public void Apply(){
        
        GameObject.Find(objectName).GetComponent(componentName).SendMessage(commandName,getParams());
    }




	
    void createParam(ItemParam p){

        switch(p.type){
            case ItemParam.INPUTFIELD:
                createInputField(p);
                break;
            case ItemParam.DROPDOWN:
                createDropDown(p);
                break;
            case ItemParam.CHECKBOX:
                createCheckbox(p);
                break;
            case ItemParam.DIVIDER:
                createDivider(p);
                break;
            case ItemParam.SWITCH:
                createSwitch(p);
                break;
            case ItemParam.BUTTON:
                createButton(p);
                break;
        }

    }


    private void createInputField(ItemParam p){
        //carica il prefab dell'input field
        GameObject input = loadPrefab("Input Field");

        input.name = p.name;
        input.GetComponentInChildren<MaterialUI.VectorImage>().vectorImageData.glyph = new MaterialUI.Glyph("add_box", "utf8", false);
        input.transform.Find("Hint Text").GetComponent<Text>().text = p.label;
        input.GetComponentInChildren<InputField>().text = p.value;
        input.GetComponentInChildren<InputField>().onEndEdit.AddListener(delegate { Apply(); });

        Param2Input(p, input);

    }


    private void createDropDown(ItemParam p)
    {
        GameObject input = loadPrefab("Dropdown");



        input.name = p.name;
        //azzera la lista
        input.GetComponent<MaterialUI.MaterialDropdown>().optionDataList = new MaterialUI.OptionDataList();
        input.GetComponent<MaterialUI.MaterialDropdown>().onItemSelected.AddListener(delegate { Apply(); });
        //splitta i valori da visualizzare
        string[] values = p.defaultvalue.Split(';');
        for (int i = 0; i < values.Length;i++){
            MaterialUI.OptionData option = new MaterialUI.OptionData();
            option.text = values[i];
            input.GetComponent<MaterialUI.MaterialDropdown>().AddData(option);
        }
        input.GetComponent<MaterialUI.MaterialButton>().text.text=p.label;
        input.GetComponent<MaterialUI.MaterialDropdown>().currentlySelected = int.Parse(p.value);

        Param2Input(p, input);
    }


    private void createCheckbox(ItemParam p)
    {

        GameObject input = loadPrefab("Checkbox");
  

        input.name = p.name;

        input.GetComponent<MaterialUI.MaterialCheckbox>().labelText = p.label;



        input.GetComponent<MaterialUI.MaterialCheckbox>().toggleOnLabel = p.label;
        input.GetComponent<MaterialUI.MaterialCheckbox>().toggleOffLabel = p.label;
        input.GetComponent<MaterialUI.MaterialCheckbox>().toggle.onValueChanged.AddListener(delegate { Apply(); });

        if (p.defaultvalue == ItemParam.TOGGLE_ON)
            input.GetComponent<MaterialUI.MaterialCheckbox>().toggle.isOn=true;
        else
            input.GetComponent<MaterialUI.MaterialCheckbox>().toggle.isOn=false;

        if (p.value ==ItemParam.TOGGLE_ON)
            input.GetComponent<MaterialUI.MaterialCheckbox>().toggle.isOn = true;
        else if(p.value == ItemParam.TOGGLE_OFF)
            input.GetComponent<MaterialUI.MaterialCheckbox>().toggle.isOn = false;
        
        Param2Input(p, input);
    }

    private void createSwitch(ItemParam p)
    {

        GameObject input = loadPrefab("Switch");
       
        input.name = p.name;

        input.GetComponent<MaterialUI.MaterialSwitch>().labelText = p.label;

        input.GetComponent<MaterialUI.MaterialSwitch>().toggleOnLabel = p.label;
        input.GetComponent<MaterialUI.MaterialSwitch>().toggleOffLabel = p.label;

        input.GetComponent<MaterialUI.MaterialSwitch>().toggle.onValueChanged.AddListener(delegate { Apply(); });

        if (p.defaultvalue == ItemParam.TOGGLE_ON)
            input.GetComponent<MaterialUI.MaterialSwitch>().toggle.isOn = true;
        else
            input.GetComponent<MaterialUI.MaterialSwitch>().toggle.isOn = false;

        if (p.value == ItemParam.TOGGLE_ON)
            input.GetComponent<MaterialUI.MaterialSwitch>().toggle.isOn = true;
        else if (p.value == ItemParam.TOGGLE_OFF)
            input.GetComponent<MaterialUI.MaterialSwitch>().toggle.isOn = false;


        Param2Input(p, input);
    }

    private void createDivider(ItemParam p)
    {

        if(p.label!=""){

            GameObject header = loadPrefab("Text");
           
            header.name = "header";
            header.GetComponentInChildren<Text>().text = p.label;
            header.GetComponentInChildren<Text>().fontStyle = FontStyle.Bold;

        }

        GameObject input = loadPrefab("Divider");
        input.name = "divider";




    }

    private void createButton(ItemParam p)
    {
        //carica il prefab dell'input field
        GameObject input = loadPrefab("Button - Normal");
        input.name = p.name;
        input.GetComponentInChildren<MaterialUI.MaterialButton>().text.text = p.label;
        input.GetComponent<Button>().onClick.AddListener(delegate { Apply(); });

        Param2Input(p, input);

    }


    private GameObject loadPrefab(string name){
        GameObject input = Instantiate((GameObject)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/MaterialUI/Prefabs/Components/"+name+".prefab", typeof(GameObject)));
        input.transform.SetParent(inspector_panel.transform);
        input.transform.localScale = new Vector3(1, 1, 1);
        return input;
    }

    private ArrayList getParams(){
        ArrayList array = new ArrayList();

        for (int i = 0; i < inspector_panel.transform.childCount;i++){
            GameObject g = inspector_panel.transform.GetChild(i).gameObject;

            ItemParam param = g.GetComponent<ItemParam>();
            if (param != null)
            {
                switch (param.type)
                {
                    case ItemParam.INPUTFIELD:
                        param.value = g.GetComponent<InputField>().text;
                        break;
                    case ItemParam.DROPDOWN:
                        param.value = g.GetComponent<MaterialUI.MaterialDropdown>().optionDataList.options[g.GetComponent<MaterialUI.MaterialDropdown>().currentlySelected].text;
                        break;
                    case ItemParam.CHECKBOX:
                        param.value = g.GetComponent<MaterialUI.MaterialCheckbox>().toggle.isOn.ToString();
                        break;
                    case ItemParam.SWITCH:
                        param.value = g.GetComponent<MaterialUI.MaterialSwitch>().toggle.isOn.ToString();
                        break;

                }
            }


            array.Add(param);



        }

        return array;
    }


    private void Param2Input(ItemParam param, GameObject input){
        input.AddComponent<ItemParam>();
        input.GetComponent<ItemParam>().name = param.name;
        input.GetComponent<ItemParam>().label = param.label;
        input.GetComponent<ItemParam>().value = param.value;
        input.GetComponent<ItemParam>().defaultvalue = param.defaultvalue;
        input.GetComponent<ItemParam>().type = param.type;
    }


}
