﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class bt_wizard : MonoBehaviour {

	public int id;

	public GameObject textureReplica;


	public void setParameter(string param_name){


		var type = typeof(wizard);
		var field = type.GetField(param_name);
		field.SetValue(null, id);

		if (textureReplica)
			textureReplica.GetComponent<Image> ().sprite = this.transform.Find("Image").GetComponent<Image> ().sprite;

		GameObject.Find ("Canvas").GetComponent<init_start> ().NextPanel ();

	}
}
