﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_standarddimension : MonoBehaviour {


	public GameObject textfield;


	public void changeWidth(){
		//calcola le misure standard
		ItemType item_type = Library.searchType(wizard.id_type);
		ItemStyle item_style = Library.searchStyle(wizard.id_style);

		ArrayList list = item_type.getStandardWidth ((int)item_style.generator.x);

		Dropdown standardWidth=GameObject.Find("Canvas/pnl_dimension//standardWidth").GetComponent<Dropdown>();
		Dropdown standardHeight=GameObject.Find("Canvas/pnl_dimension//standardHeight").GetComponent<Dropdown>();
		standardWidth.options.Clear ();
		for (int i = 0; i < list.Count; i++) {
			int s = (int)list[i];
			standardWidth.options.Add(new Dropdown.OptionData () { text = s.ToString() });
		}
		
		standardWidth.RefreshShownValue ();
		standardWidth.GetComponentInParent<bt_standarddimension> ().updateTextField ();
		standardHeight.GetComponentInParent<bt_standarddimension> ().changeHeight ();
		standardHeight.GetComponentInParent<bt_standarddimension> ().updateTextField ();




	}


	public void changeHeight(){
		
		ItemType item_type = Library.searchType(wizard.id_type);
		ItemStyle item_style = Library.searchStyle(wizard.id_style);
		Dropdown standardWidth=GameObject.Find("Canvas/pnl_dimension/standardWidth").GetComponent<Dropdown>();

	


		ArrayList list = item_type.getStandardHeight ((int)item_style.generator.y,int.Parse(	standardWidth.options [standardWidth.value].text));

		Dropdown standardHeight=GameObject.Find("Canvas/pnl_dimension/standardHeight").GetComponent<Dropdown>();
		standardHeight.options.Clear ();
		for (int i = 0; i < list.Count; i++) {
			int s = (int)list[i];
			standardHeight.options.Add(new Dropdown.OptionData () { text = s.ToString() });
		}
		standardHeight.RefreshShownValue ();

	}


	public void updateTextField(){

		Dropdown d = this.GetComponent<Dropdown> ();
		textfield.GetComponent<InputField> ().text = d.options [d.value].text;



	}




}
