﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class init_start : MonoBehaviour {


	int countStylesImage=0;
	public GameObject[] pnl;
	public GameObject opening_dropdown;

	int currentIndex=0;


	// Use this for initialization
	void Start () {

		GUIManager.InitDialogs ();
		GUIManager.showLoading ();

		//Definizione webservice
		webservice wb = webservice.InitWebService();

		wb.InitInterface ((finish) => {
			InitGUI ();
			GUIManager.hideLoading();
		});


	}



	void InitGUI(){



		setPanel<ItemStyle> (Library.StylesAvaible, "wizard/bt_style", pnl[0],GameObject.Find("Canvas/Paging/Image_step1"));
		setPanel<ItemType> (Library.TypesAvaible, "wizard/bt_type", pnl[1],GameObject.Find("Canvas/Paging/Image_step2"));
		setPanel<ItemGeometry> (Library.GeometriesAvaible, "wizard/bt_geometry", pnl[2],GameObject.Find("Canvas/Paging/Image_step3"));



	}
	

	int searchPosArray<T>(ref ArrayList list,int id){
		for (int i = 0; i < list.Count; i++) {
			T item = (T)list [i];


			if (item.GetType ().GetField("id").GetValue(item).ToString() == id.ToString())
				return i;

		}
		return -1;

	}



	void setPanel<T>(ArrayList list,string prefab,GameObject pnl,GameObject textureReplica){
		if (list.Count > 0) {
			for (int i = 0; i < list.Count; i++) {
				T c = (T)list [i];

				//crea il pulsante nella scrollview Library
				GameObject bt=Instantiate(Resources.Load(prefab)) as GameObject;


				bt.transform.SetParent (pnl.GetComponentInChildren<GridLayoutGroup> ().gameObject.transform);
				bt.transform.localScale = new Vector3 (1, 1, 1);
				bt.AddComponent<bt_wizard> ();
				bt.GetComponent<bt_wizard> ().id = (int)c.GetType ().GetField ("id").GetValue (c);
				bt.GetComponent<bt_wizard> ().textureReplica = textureReplica; 
				bt.transform.GetComponentInChildren<Text> ().text = c.GetType ().GetField ("name").GetValue (c).ToString ();
				Sprite img = (Sprite)c.GetType ().GetField ("image").GetValue (c);
				if (img) {
					bt.transform.Find ("Image").GetComponent<Image> ().sprite = img;
				} else {
					StartCoroutine (downloadImage(c.GetType ().GetField ("image_path").GetValue (c).ToString(), ((img_downloaded) => {
						bt.transform.Find ("Image").GetComponent<Image> ().sprite = img_downloaded;
						c.GetType().GetField("image").SetValue(c,img_downloaded);
					})));
				}
			}
		}

	}


	IEnumerator downloadImage(string image_path,Action<Sprite> finish){
        Sprite image=Sprite.Create(null,new Rect(),new Vector2());
		
        if(image_path.ToString()!="null" && image_path.ToString()!=""){
			WWW www = new WWW (System.Uri.EscapeUriString(StaticVariables.BASE_URL +image_path));

			yield return www;

			if (www.texture)
				image =  Sprite.Create((Texture2D)www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0.5f,0.5f),10);

		}
		finish (image);
	}





	public void NextPanel(){
		if (currentIndex < (pnl.Length-1)) {
			
			currentIndex++;
			GameObject.Find ("Paging/bt_step" + (currentIndex+1).ToString ()).GetComponent<Toggle> ().isOn = true;

		}

	}

	public void PrevPanel(){
		if (currentIndex >0 ) {
			currentIndex--;
			GameObject.Find ("Paging/bt_step" + (currentIndex+1).ToString ()).GetComponent<Toggle> ().isOn = true;



		}

	}

	public void MoveToPanel(int index){
		pnl [currentIndex].SetActive (false);
		currentIndex = index;
		pnl [index].SetActive (true);

		if (index == 3) { //se ci si trova nella definizione delle dimensioni
			ItemType item_type = Library.searchType (wizard.id_type);
			ItemStyle item_style = Library.searchStyle (wizard.id_style);

			ArrayList widths=item_type.getStandardWidth ((int)item_style.generator.x);
			Dropdown dw = GameObject.Find ("Canvas/pnl_dimension/standardWidth").GetComponent<Dropdown> ();
			dw.ClearOptions ();
			for (int i = 0; i < widths.Count; i++) {
				int w = (int)widths [i];
				dw.options.Add (new Dropdown.OptionData (){text=w.ToString()});
			}
			dw.value = 0;
			dw.RefreshShownValue ();


			ArrayList heights=item_type.getStandardHeight ((int)item_style.generator.y,int.Parse(dw.options[0].text));
			Dropdown dh = GameObject.Find ("Canvas/pnl_dimension/standardHeight").GetComponent<Dropdown> ();
			dh.ClearOptions ();
			for (int i = 0; i < heights.Count; i++) {
				int h = (int)heights [i];
				dh.options.Add (new Dropdown.OptionData (){text=h.ToString()});
			}
			dh.value = 0;
			dh.RefreshShownValue ();


			//aggiorna la descrizione della tipologia
			GameObject.Find ("Canvas/pnl_dimension/description").GetComponent<Text> ().text = Utility.ConvertHtmlToText(item_type.description);

			//aggiorna la dropdown per la scelta del verso
			if (item_type.opening == "entrambe") {
				opening_dropdown.SetActive (false);
				wizard.opening = "entrambe";
			} else {
				opening_dropdown.SetActive (true);
				wizard.opening = "destra";
			}

		}

	}


}
