﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_start : MonoBehaviour {

	public void goToMain(){







		//crea il nuovo gate

		ItemGate gate = new ItemGate ();

		gate.id = 0;
		gate.id_type = wizard.id_type;
		gate.id_style = wizard.id_style;
		gate.id_geometry = wizard.id_geometry;

		gate.opening = wizard.opening;



		gate.loadFromLibrary ();
		if (GameObject.Find ("Canvas/pnl_dimension/dimension_w").GetComponent<InputField> ().text == "")
			wizard.width_gross = 0;
		else
			wizard.width_gross = int.Parse(GameObject.Find ("Canvas/pnl_dimension/dimension_w").GetComponent<InputField> ().text);


		if (GameObject.Find ("Canvas/pnl_dimension/dimension_h").GetComponent<InputField> ().text == "")
			wizard.height_gross = 0;
		else
			wizard.height_gross = int.Parse(GameObject.Find ("Canvas/pnl_dimension/dimension_h").GetComponent<InputField> ().text);

		if (GameObject.Find ("Canvas/pnl_dimension/dimension_B").GetComponent<InputField> ().text == "") {
			gate.columnWidth = 0;
			gate.columnWidthL = 0;
			gate.columnWidthR = 0;
		} else {
			gate.columnWidth = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_B").GetComponent<InputField> ().text);
			gate.columnWidthL = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_B").GetComponent<InputField> ().text);
			gate.columnWidthR = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_B").GetComponent<InputField> ().text);

		}
		if (GameObject.Find ("Canvas/pnl_dimension/dimension_E").GetComponent<InputField> ().text == "") {
			gate.columnDepth = 0;
			gate.columnDepthL = 0;
			gate.columnDepthR = 0;
		} else {
			gate.columnDepth = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_E").GetComponent<InputField> ().text);
			gate.columnDepthL = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_E").GetComponent<InputField> ().text);
			gate.columnDepthR = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_E").GetComponent<InputField> ().text);
		}	
		if (GameObject.Find ("Canvas/pnl_dimension/dimension_D").GetComponent<InputField> ().text == "") {
			gate.capitalWidth = 0;
			gate.capitalWidthL = 0;
			gate.capitalWidthR = 0;
		} else {
			gate.capitalWidth = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_D").GetComponent<InputField> ().text);
			gate.capitalWidthL = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_D").GetComponent<InputField> ().text);
			gate.capitalWidthR = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_D").GetComponent<InputField> ().text);


		}
		if (GameObject.Find ("Canvas/pnl_dimension/dimension_F").GetComponent<InputField> ().text == "") {
			gate.capitalDepth = 0;
			gate.capitalDepthL = 0;
			gate.capitalDepthR = 0;


		} else {
			gate.capitalDepth = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_F").GetComponent<InputField> ().text);
			gate.capitalDepthR = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_F").GetComponent<InputField> ().text);
			gate.capitalDepthL = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_F").GetComponent<InputField> ().text);


		}
		if (GameObject.Find ("Canvas/pnl_dimension/dimension_K").GetComponent<InputField> ().text == "") {
			gate.capitalHeight = 0;
			gate.capitalHeightL = 0;
			gate.capitalHeightR = 0;


		} else {
			gate.capitalHeight = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_K").GetComponent<InputField> ().text);
			gate.capitalHeightL = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_K").GetComponent<InputField> ().text);
			gate.capitalHeightR = int.Parse (GameObject.Find ("Canvas/pnl_dimension/dimension_K").GetComponent<InputField> ().text);


		}
	
		
		
		



		gate.type.CalcolateNetDimension (wizard.width_gross,wizard.height_gross, gate.style,out gate.width, out gate.height,out gate.heightmax);

		gate.updateOtherParams ();

		StaticVariables.gate = gate;


		//verifica che sono stati passati tutti i valori

		if (wizard.width_gross <= 0) {
			GUIManager.showDialogBox ("Errore", "Inserisci la dimensione L");
			return;
		}
			

		if (wizard.height_gross <= 0) {
			GUIManager.showDialogBox ("Errore", "Inserisci la dimensione H");
			return;
		}

		Application.LoadLevel ("design");
	}




}
