﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class init_gates : MonoBehaviour {


	int start=0;
	int limit=1000000;


	webservice wb;

	// Use this for initialization
	void Start () {
		GUIManager.InitDialogs ();
		GUIManager.showLoading ();


		//Definizione webservice
		wb = webservice.InitWebService();

		//verifica che le librerie sono state già scaricate
		wb.InitInterface ((finish) => {
			InitDownloadGates ();
		});



	}



	 void InitDownloadGates(){


		if (Library.last_is_saved) {
			Library.GatesAvaible.Clear ();

		}
		if (Library.GatesAvaible.Count == 0) {
			downloadGates ();
		}
		else {
			populateList (Library.GatesAvaible.Count);
		}





	}


	public void downloadGates(){

		StartCoroutine (wb.DownloadGates (start, limit, (downloaded) => {

			populateList (downloaded);
		}));

	}


	void populateList(int downloaded){
		if(downloaded>0){
			GameObject scroll=GameObject.Find("Canvas/Scroll View/Viewport/Content");

			if (Library.GatesAvaible.Count > 0) {
				for (int i = start; i < Library.GatesAvaible.Count; i++) {
					ItemGate c = (ItemGate)Library.GatesAvaible [i];
					c.updateOtherParams ();
					//crea il pulsante nella scrollview Library
					GameObject bt=Instantiate(Resources.Load("gates/bt_gate")) as GameObject;

					bt.transform.SetParent (scroll.transform);
					bt.transform.localScale = new Vector3 (1, 1, 1);
					bt.GetComponent<bt_detail> ().gate= c;


					bt.transform.Find ("title").GetComponent<Text> ().text = c.id.ToString ();
					bt.transform.Find ("date").GetComponent<Text> ().text = c.date;
					bt.transform.Find ("style").GetComponent<Text> ().text = c.style.name;
					bt.transform.Find ("type").GetComponent<Text> ().text = c.type.name;


				}
			}


		}

		GUIManager.hideLoading ();

		Library.last_is_saved = false;

	}



}
