﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bt_detail : MonoBehaviour {

	public ItemGate gate=new ItemGate();


	public void openDetail(){
		GUIManager.showWaitingBox ("Scheda di dettaglio", "Apertura in corso");

		if (!gate.image) {
			StartCoroutine (gate.downloadImage ((img_downloaded) => {
				GUIManager.closeWaitingBox();
				GUIManager.openDetailBox (gate);


			}));
		} else {
			GUIManager.closeWaitingBox();
			GUIManager.openDetailBox (gate);
		}



	}

	public void closeDetail(){
		GUIManager.closeDetailBox ();
	}


}

