﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class toolbar : MonoBehaviour {




	public void EditMode(){
		
		GameObject.Find("Gate/Layer").GetComponent("Layer").SendMessage("actionEdit");
		setToggle ("bt_edit",true);
		setToggle ("bt_delete",false);

	}


	public void DeleteMode(){
		GameObject.Find("Gate/Layer").GetComponent("Layer").SendMessage("actionDelete");
		setToggle ("bt_delete",true);
		setToggle ("bt_edit",false);
	}



	void setToggle(string bt_name,bool enable){

		Color c;

		if (enable)
			c = Color.red;
		else
			c = Color.white;


		Button b = this.transform.Find(bt_name).GetComponent<Button> ();
		ColorBlock cb = b.colors;
		cb.normalColor = c;
		b.colors = cb;



	}


}
