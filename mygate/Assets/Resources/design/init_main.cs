﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class init_main : MonoBehaviour {




	void Start () {

		//Definizione webservice
		webservice wb = webservice.InitWebService ();
		wb.InitInterface ((finish) => {

			//verifica se bisogna caricare un nuovo cancello
			//Debug.Log(StaticVariables.gate.id+ " probabilmente è qui che sbaglio qualcosa");	
			if(StaticVariables.gate!=null){
				if (StaticVariables.gate != null || StaticVariables.gate.id > 0) {
					//verifica che le librerie sono state già scaricate
						StartCoroutine (
							wb.downloadGateFromDB (StaticVariables.gate.id, (g) => {
								InitGate (g);
							}, (error) => {
								Debug.Log (error);
							}));
					}
					
				} else {

					StaticVariables.gate=new ItemGate();

					StaticVariables.gate.id=0;
					StaticVariables.gate.id_type=3;
					StaticVariables.gate.id_style=7;
					StaticVariables.gate.id_geometry=5;
					StaticVariables.gate.loadFromLibrary ();
                    StaticVariables.gate.width_gross = 1820;
                    StaticVariables.gate.height_gross = 2000;
                StaticVariables.gate.height_gross=StaticVariables.gate.type.CalcolateNetDimension (StaticVariables.gate.width_gross, StaticVariables.gate.height_gross,StaticVariables.gate.style,out StaticVariables.gate.width, out StaticVariables.gate.height,out StaticVariables.gate.heightmax,StaticVariables.gate.geometry);
					
					InitGate (StaticVariables.gate);
				}
		});


	}

	void InitGate(ItemGate g){
		g.updateOtherParams ();

		Library.ItemsAvaible=g.style.elements;

		this.GetComponentInParent<GridManager> ().Init(g);
		this.GetComponentInParent<ControlsDesign> ().setInitialCamera (this.GetComponentInParent<GridManager> ().setCameraToSeeGate (new Vector3 (0, 0, 4000f)));



	}


}
