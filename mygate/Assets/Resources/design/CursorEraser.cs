﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorEraser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 pos=Utility.ConvertPointToWorld (Input.mousePosition);
		this.transform.localPosition =pos;
		
	}
}
