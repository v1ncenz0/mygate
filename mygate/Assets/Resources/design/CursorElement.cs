﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;


/**
 * Classe che memorizza le informazioni necessarie a gestire il cursore e le intersezioni con la griglia
 */
public class CursorElement: MonoBehaviour
{
	// Dimensione della cella che attualmente sta sul cursore, 
	// le diemensioni sono intese in numero di celle (CellGrid) occupate
	public static Vector2 dimCellGridTouched;
	// Dimensione della cella che forma la griglia (CellGrid)
	//public static Vector2 generatorDim;
	// dimensioni dell'elemento
	public static Vector3 elementDim;

	public enum ModeList{None=0,MoveLeft=1,MoveRight=2,MoveTop=3,MoveDown=4,AddElement=5,FreeMove=6,AddLeft=7,AddRight=8,AddTop=9,AddDown=10};
	
	public ModeList mode;

	public static Item currentItem;
	public static bool currentCellToCut = false;

	public bool isInit=true;
	private bool isMouseMove = false;
	private bool isTouch=false;
	private Vector3 firstPositionMouse;
	private Vector3 initialPositionCursor;
	private bool isDragged=false;
	private Vector3 oldPosition;

	//for double click
	float clicked = 0;
	float clicktime = 0;
	float clickdelay = 0.7f;

   
	void Start()
	{
		Input.simulateMouseWithTouches = false;
	}


	bool DoubleClick(){

		bool touchBegan = false;
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);

			if (Input.touchCount == 1 && touch.phase == TouchPhase.Began)
				touchBegan = true;

		}

		if (Input.GetMouseButtonDown (0) || (touchBegan) ) {
			clicked++;
			if (clicked == 1) clicktime = Time.time;
		}         
		if (clicked > 1 && Time.time - clicktime < clickdelay) {
			clicked = 0;
			clicktime = 0;
			return true;
		} else if (clicked > 2 || Time.time - clicktime > 1) clicked = 0;         
		return false;
	}

	void Update(){
		if (isMouseMove) {

			//calcalate delta
			Vector3 delta = Utility.ConvertPointToWorld(Input.mousePosition)-firstPositionMouse;

			float stepX = CursorElement.elementDim.x / CursorElement.dimCellGridTouched.x;
			float stepY = CursorElement.elementDim.y / CursorElement.dimCellGridTouched.y;

			float dx = delta.x/stepX ;
			float dy = delta.y/stepY ;

			float x = initialPositionCursor.x+ (stepX * Mathf.Round(dx));
			float y = initialPositionCursor.y+(stepY * Mathf.Round(dy));

			//verify camera
			Vector3 rot_camera=Camera.main.transform.localRotation.eulerAngles;
			int sign = 1;
			if (rot_camera.y >= 90.0F && rot_camera.y <= 270.0F)
				sign = -1;

			Vector3 pos = new Vector3 (Mathf.Round(x*10*sign)/10, Mathf.Floor(y*10*sign)/10, 0);

			pos=fixposition(pos);


			this.gameObject.transform.parent.localPosition = pos;
			oldPosition = pos;

		}


		if (DoubleClick()) {
			AddElement ();
		}



		//abilita il controllo touch
		if (Input.touchCount == 1) {
			Touch touch = Input.GetTouch (0);
			Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.name == this.transform.name && touch.phase == TouchPhase.Began) {
					isTouch = true;
					Navigate ();
				}
			}

			if (isDragged) {
				//calcalate delta
				Vector3 delta = Utility.ConvertPointToWorld (Input.GetTouch (0).position) - firstPositionMouse;

				float stepX = CursorElement.elementDim.x / CursorElement.dimCellGridTouched.x;
				float stepY = CursorElement.elementDim.y / CursorElement.dimCellGridTouched.y;

				float dx = delta.x / stepX;
				float dy = delta.y / stepY;

				float x = initialPositionCursor.x + (stepX * Mathf.Round (dx));
				float y = initialPositionCursor.y + (stepY * Mathf.Round (dy));

				Vector3 pos = new Vector3 (Mathf.Round (x), Mathf.Round (y), 0);

				pos=fixposition(pos);
				this.gameObject.transform.parent.localPosition = pos;
				oldPosition = pos;
			}



		} else {
			isDragged = false;

		}

		isTouch = false;




	}

	

	Vector3 fixposition(Vector3 pos){


		ControlsDesign l = GameObject.FindObjectOfType<ControlsDesign> ();

		float range;
		if (l.cg.gate.style.generator.x > l.cg.gate.style.generator.y)
			range = l.cg.gate.style.generator.x/2;
		else
			range = l.cg.gate.style.generator.y/2;

		CellGrid c=l.currentActive.findNearCell(pos, range);

		if (c) {
			
			Vector3 pivot = c.drawCoordinates;
			if (dimCellGridTouched.x % 2 == 0)
				pivot.x = (c.drawCoordinates.x +l.cg.gate.style.generator.x/2);
			
			if (dimCellGridTouched.y % 2 == 0)
				pivot.y = (c.drawCoordinates.y +l.cg.gate.style.generator.y/2);

			//verifica se lo puoi inserire
			bool tocut;
			setElementCanAdded(l.currentActive.canAdd (CursorElement.currentItem, pivot,out tocut));
			currentCellToCut = tocut;
		


			return pivot;
		
		
		} 




		return oldPosition;
			
	}

	/// <summary>
	/// Colora l'elemento selezionato in base al fatto se è possibile inserirlo o meno
	/// </summary>
	/// <param name="status">If set to <c>true</c> status.</param>
	void setElementCanAdded(bool status){
		Color color;
		if (status)
			color = Color.green;
		else
			color = Color.red;
		
		//imposta il materiale provvisorio

		Renderer[] rend = GameObject.Find ("cursorobject/Element").GetComponentsInChildren<Renderer> ();
		for (int i = 0; i < rend.Length; i++)
			rend [i].sharedMaterial.color = color;
	}
		


	/// <summary>
	/// Visualizza l'item all'interno del cursore
	/// </summary>
	/// <returns>The item.</returns>
	/// <param name="item">Item.</param>
	public void setItem(Item item){
		ControlsDesign l = GameObject.FindObjectOfType<ControlsDesign> ();
		gridLayer currentActive = l.currentActive;

		//metti la posizione del cursore nella prima cella
		Vector3 pos = new Vector3 (item.dimension.x / 2  + l.currentActive.startingPoint.x, item.dimension.y / 2  + l.currentActive.startingPoint.y, 0);


		//verifica se puoi inserire il pezzo nella cella attuale
		Vector3 pivot = new Vector3 (0, 0, 0);
		Transform o = this.transform.Find ("Element");
		if (o) {
			BoxCollider current =o.gameObject.GetComponent<BoxCollider> ();
			//prendi la posizione corrente del cursore e verifica se si trova sopra una cella (posiziona il pivot alla prima cella in basso)
			pivot = new Vector3 (
				(this.gameObject.transform.localPosition.x - current.size.x / 2) + l.cg.gate.style.generator.x  / 2 ,
				(this.gameObject.transform.localPosition.y -current.size.y / 2) +  l.cg.gate.style.generator.y / 2,
				              this.transform.localPosition.z
			              );

			CellGrid c=l.currentActive.getCellGrid(pivot);

			if (c != null) {
				//correggi la posizione per poter inserire l'oggetto
				pos = new Vector3 (c.drawCoordinates.x + l.cg.gate.style.generator.x / 2 * (item.cells.x - 1), c.drawCoordinates.y + l.cg.gate.style.generator.y / 2 * (item.cells.y - 1), this.transform.localPosition.z);

			}


		}






		// Cancello e ricreo l'elemento che sta al posto del cursore
		if (this.transform.Find ("Element"))
			DestroyImmediate (this.transform.Find ("Element").gameObject);


		//sposta il cursore
		this.gameObject.transform.localPosition = pos;

		//inserisci l'item all'interno del cursore
		GameObject element3d = (GameObject)Instantiate (item.obj);
		element3d.name = "Element";
		element3d.transform.localScale = new Vector3 (1*item.scale_x, 1*item.scale_y, 3);
		element3d.transform.parent = this.transform;

		//attacca lo script per poter spostare il cursore con il drag and drop
		element3d.AddComponent<CursorElement> ();
		element3d.GetComponent<CursorElement> ().mode = CursorElement.ModeList.FreeMove;

		//imposta il materiale provvisorio
		Material material = new Material (Shader.Find ("Standard"));
		material.color = Color.cyan;
		Renderer[] rend= element3d.GetComponentsInChildren<Renderer> ();
		for (int i = 0; i < rend.Length; i++)
			rend [i].sharedMaterial = material;


		//sposta il cursore nell'ultima posizione memorizzata
		element3d.transform.localPosition = new Vector3 (0, 0, 0);
		element3d.SetActive (true);
		if (item.rotate != null)
			element3d.transform.Rotate (item.rotate);

		BoxCollider b = element3d.AddComponent<BoxCollider> ();
		b.size = new Vector3 (item.dimension.x, item.dimension.y, 100f);
		b.isTrigger = true;

		CursorElement.dimCellGridTouched = item.cells;
		//CursorElement.generatorDim = new Vector2 (l.cg.gate.style.generator.x, l.cg.gate.style.generator.y);
		CursorElement.elementDim = item.dimension;
		setArrows ();	

		// Aggiungo il rigidbody per attivare le collisioni
		Rigidbody r = element3d.AddComponent<Rigidbody> ();
		r.isKinematic = false;
		r.useGravity = false;
		r.angularDrag = 0f;
		r.constraints = RigidbodyConstraints.FreezeAll;



		//verifica se si può aggiungere le posto indicato
		setElementCanAdded(currentActive.canAdd (item, pos));

		CursorElement.currentItem = item;
			
	}






	/// <summary>
	/// Dispone le frecce e il tasto OK attorno all'oggetto
	/// </summary>
	private void setArrows(){

		float zpos = 0;

	
		GameObject.Find ("arrow_add_lf").transform.localPosition = new Vector3 (-CursorElement.elementDim.x-50f, 0, zpos);
		GameObject.Find ("arrow_add_rg").transform.localPosition = new Vector3 (CursorElement.elementDim.x+50f, 0, zpos);
		GameObject.Find ("arrow_add_tp").transform.localPosition = new Vector3 (0, CursorElement.elementDim.y+50f, zpos);
		GameObject.Find ("arrow_add_dw").transform.localPosition = new Vector3 (0, -CursorElement.elementDim.y-50f, zpos);


	}







	/// <summary>
	/// Passete le posizioni del cursore, verifica se si trovano all'interno della griglia
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	private void verifyLimit(float x,float y){
		gridLayer currentActive = GameObject.FindObjectOfType<ControlsDesign> ().currentActive;


	}



	public void OnMouseDown(){
		Navigate ();



	}

	

	void OnMouseUp(){
		if (isMouseMove)
			isMouseMove = false;
	}

    void Navigate(){
        Navigate(mode);
    }

    void Navigate(ModeList m){
        
		switch(m){
		case (ModeList.MoveLeft):
			MoveLeft ();
			break;
		case ModeList.MoveRight:
			MoveRight ();
			break;
		case ModeList.MoveTop:
			MoveTop ();
			break;
		case ModeList.MoveDown:
			MoveDown ();
			break;
		case ModeList.AddElement:
			AddElement ();
			break;
		case ModeList.FreeMove:
			initialPositionCursor = this.gameObject.transform.parent.localPosition;
			if (!isTouch) { //escludi questo controllo in fase touch
				isMouseMove = true;
				firstPositionMouse = Utility.ConvertPointToWorld (Input.mousePosition);
			} else {
				firstPositionMouse = Utility.ConvertPointToWorld (Input.GetTouch (0).position);
				isDragged = true;
			}
			break;
		case ModeList.AddLeft:
			AddandMoveTo (new Vector2(-1,0));
			break;
		case ModeList.AddRight:
			AddandMoveTo (new Vector2(1,0));
			break;
		case ModeList.AddTop:
			AddandMoveTo (new Vector2(0,1));
			break;
		case ModeList.AddDown:
			AddandMoveTo (new Vector2(0,-1));
			break;


		}

	}





	void AddElement(){
		GameObject.FindObjectOfType<ControlsDesign> ().addCell ((finish)=>{},null,CursorElement.currentCellToCut);
	}

	void AddandMoveTo(Vector2 direction){
		GameObject.FindObjectOfType<ControlsDesign> ().addCell ((finish)=>{
			MoveNext(direction);
		},null,CursorElement.currentCellToCut);
	}


	void MoveLeft(){
		oldPosition = this.gameObject.transform.parent.localPosition;
		this.gameObject.transform.parent.Translate (new Vector3 (-CursorElement.elementDim.x/CursorElement.dimCellGridTouched.x, 0, 0));
		this.gameObject.transform.parent.localPosition = fixposition (this.gameObject.transform.parent.localPosition);
	}

	void MoveRight(){
		oldPosition = this.gameObject.transform.parent.localPosition;
		this.gameObject.transform.parent.Translate (new Vector3 (CursorElement.elementDim.x/CursorElement.dimCellGridTouched.x, 0, 0));
		this.gameObject.transform.parent.localPosition = fixposition (this.gameObject.transform.parent.localPosition);
	}

	void MoveTop(){
		oldPosition = this.gameObject.transform.parent.localPosition;
		this.gameObject.transform.parent.Translate (new Vector3 (0,CursorElement.elementDim.y/CursorElement.dimCellGridTouched.y, 0));
		this.gameObject.transform.parent.localPosition = fixposition (this.gameObject.transform.parent.localPosition);
	}

	void MoveDown(){
		oldPosition = this.gameObject.transform.parent.localPosition;
		this.gameObject.transform.parent.Translate (new Vector3 (0,-CursorElement.elementDim.y/CursorElement.dimCellGridTouched.y, 0));
		this.gameObject.transform.parent.localPosition = fixposition (this.gameObject.transform.parent.localPosition);
	}

	void MoveNext(Vector2 direction){
		oldPosition = this.gameObject.transform.parent.localPosition;
		this.gameObject.transform.parent.Translate (new Vector3 (Mathf.Abs(CursorElement.elementDim.x)*direction.x, Mathf.Abs(CursorElement.elementDim.y)*direction.y, 0));
		this.gameObject.transform.parent.localPosition = fixposition (this.gameObject.transform.parent.localPosition);
	}



}
