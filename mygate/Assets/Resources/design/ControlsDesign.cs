﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Text;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class ControlsDesign : MonoBehaviour {

	public gridLayer currentActive = null; // Layer attivo su cui si sta lavorando
	public GridManager cg;

	public Item itemSelected; // Elemento selezionato, è l'elemento che usiamo per riempire la griglia

	public MoveCamera moveCamera; // Script per il movimento della telecamera

	// Oggetto che leghiamo al cursore del mouse
	public GameObject cursorObject;
	public GameObject cursorDeleteMode;

	public bool addspecularcell = true; //definisce se nell'inserimento a doppio battente gli oggetti devono essere replicati in speculare nella anta adiacente
	private bool is_scene_rendering=false;

	//Memorizza il prezzo del cancello
	private double price;

	void Start(){

		//define dialogs for GUI
		GUIManager.InitDialogs ();
		GUIManager.showLoading ();

		//inizializzo la variabile per l'inserimento degli oggetti in speculare (solo per il doppio battente)
		addspecularcell = true;

		//collego lo script all'GateManager
		cg.oninitfinish = Init;


	}

	/// <summary>
	/// Procedura per l'inizializzazione del cancello (creazione telaio, creazione layer, etc)
	/// </summary>
	public void Init ()
	{
		

		//set default grid
		GameObject g=(GameObject)cg.layers[0];
		currentActive = g.GetComponent<gridLayer> ();

		
		//imposta il colore di default
		if(Library.ColorsAvaible.Count>0){
			ItemColor firstColor=(ItemColor)Library.ColorsAvaible[0];
			setColorLayer(firstColor.id,0);
			setColorLayer(firstColor.id,1);
			setColorLayer(firstColor.id,2);
			setColorFrame(firstColor.id);

		}
		GUIManager.createGUI(); //crea la gui
		//currentActive.showGrid(true);
	


		//carica il cancello (se è necessario)
		if(cg.gate.id>0){
			GUIManager.setLoadingMessage("Caricamento cancello");
			getGatefromJSON(cg.gate.configuration,(s)=>{
				if(s){
					activateLayer(0);
					Application.ExternalCall("UnityReady");
					GUIManager.hideLoading ();
				}else
					GUIManager.setLoadingMessage("Errore durante il caricamento del cancello",true);
			});

		}else{
			
			activateLayer(0);
			Application.ExternalCall("UnityReady");
			GUIManager.hideLoading ();

		}



		GUIManager.showHelp(LanguageManager.HELP_WELCOME_TITLE,LanguageManager.HELP_WELCOME); 	

	}



	public void setInitialCamera(Vector3 position){
		//inquadra l'interno cancello
		moveCamera.puntoPivot=position;
		moveCamera.puntoPivot.z = 0;

		moveCamera.initialCameraPos = Camera.main.transform.position;
		moveCamera.initialCameraRot = Camera.main.transform.rotation;
	}





	#region LAYER MANAGER


	/// <summary>
	/// Visualizza/Nasconde un layer
	/// </summary>
	/// <param name="arr">Array (layer_id;status)</param>
	public void setVisibilityLayer(ArrayList arr){
		setVisibilityLayer ((int)arr [0], (bool)arr [1]);
	}



	public void setVisibilityLayer(int layer_id,bool state){
		for (int i=0; i< cg.layers.Count; i++) {
			GameObject go = cg.layers [i] as GameObject;
			if (i == layer_id){ 
				go.GetComponent<gridLayer> ().isActive = state;
				go.SetActive(state);
			}
		}

	}


	public void setInvertVisibilityLayer(int layer_id){
		for (int i=0; i< cg.layers.Count; i++) {
			GameObject go = cg.layers [i] as GameObject;


			if (i == layer_id){ 
				go.GetComponent<gridLayer> ().isActive = !go.GetComponent<gridLayer> ().isActive;
				go.SetActive(go.GetComponent<gridLayer> ().isActive);
			}
		}

	}

	/// <summary>
	/// Attiva la griglia passata in input e dissttiva le altre due 
	/// </summary>
	/// <param name="layerToActivate">Layer to activate.</param>
	public void activateLayer (int layerToActivate)
	{
		GUIManager.showHelp(LanguageManager.HELP_LAYER_TITLE,LanguageManager.HELP_LAYERS);
		for (int i=0; i< cg.layers.Count; i++) {
			GameObject go = cg.layers [i] as GameObject;
			if (i == layerToActivate) {
				go.GetComponent<gridLayer> ().Activate(true);
				currentActive = go.GetComponent<gridLayer> ();
				go.GetComponent<gridLayer> ().showCellGrid (true);
				//sposto il cursore sul layer
				Vector3 pos = cursorObject.transform.localPosition;
				pos.z = currentActive.Z;
				cursorObject.transform.localPosition = pos;
				//currentActive.showGrid (true);

			} else {
				go.GetComponent<gridLayer> ().Activate(false);
				go.GetComponent<gridLayer> ().showCellGrid (false);
			}
		}

	}


	/// <summary>
	/// Controlla se è styato aggiunto almeno un elemento nel layer centrale
	/// </summary>
	/// <returns><c>true</c>, if middle layer not empty was ised, <c>false</c> otherwise.</returns>
	private bool isMiddleLayerNotEmpty ()
	{
		GameObject layer = (GameObject)cg.layers [0];
		return layer.GetComponent<gridLayer> ().isNotEmptyLayer ();
	}

	/// <summary>
	/// Controlla se il layer centrale è completamente riempito
	/// </summary>
	/// <returns><c>true</c>, if middle layer not empty was ised, <c>false</c> otherwise.</returns>
	private bool isMiddleLayerFill ()
	{
		GameObject layer = (GameObject)cg.layers [0];
		return layer.GetComponent<gridLayer> ().isLayerFill ();
	}



	#endregion

	/// <summary>
	/// Gestisce la rotazione della camera sull'asse Z
	/// </summary>
	/// <param name="degree">Degree.</param>
	public void cameraMove (int degree)
	{
		moveCamera.rotateCamera(degree);
	}


	#region ELEMENT_MANAGER


	/// <summary>
	/// Imposta l'elemento selezionato sul cursore
	/// </summary>
	/// <param name="item">Item.</param>
	public void setItemSelected (Item item)
	{

		itemSelected = item;

		GUIManager.showWaitingBox ("Scaricamento oggetto", "Attendere");
		//scarica l'obj
		webservice.downloadElement (item,(value)=>{
			item.obj=value;
			setElementToAvaible(item);
			GUIManager.closeWaitingBox();
			itemChanged(item);
			//apri l'help
			GUIManager.showHelp(LanguageManager.HELP_LIBRARY_TITLE,LanguageManager.HELP_LIBRARY);


		}
		);
	}


	/// <summary>
	/// Cambio l'item selezionato
	/// </summary>
	/// <param name="item">Item.</param>
	public void itemChanged (Item item)
	{
		// Attivo il cursore 
		GUIManager.activeCursorElement();
		itemSelected = item;
		cursorObject.GetComponent<CursorElement> ().setItem (item);

		//currentActive.showGrid (true);

		//imposto il controllo in edit mode
		setEditMode();

	}


	/// <summary>
	/// Riempe la griglia del layer corrente con l'elemento selezionato
	/// </summary>
	public void fillGrid ()
	{
		GUIManager.showHelp(LanguageManager.HELP_FILL_TITLE,LanguageManager.HELP_FILL);
		GUIManager.showWaitingBox ("Procedura automatica", "Riempimento celle in corso ...");
		StartCoroutine(
			currentActive.fillGrid (itemSelected,(finish)=>{
				GUIManager.closeWaitingBox();
			})
		);
	}
	#endregion


    /// <summary>
    /// Imposto la modalità di fruizione in add
    /// </summary>
    public void setAddMode()
    {
        Ctrl.setEditMode();
        GUIManager.activeCursorElement();
        cursorDeleteMode.SetActive(false);
        GUIManager.showLibrary(true);

    }


	/// <summary>
	/// Imposto la modalità di fruizione in edit
	/// </summary>
	public void setEditMode(){
		Ctrl.setEditMode();
		GUIManager.activeCursorElement ();
		cursorDeleteMode.SetActive (false);
		GUIManager.showHelp(LanguageManager.HELP_EDIT_TITLE,LanguageManager.HELP_EDIT);

	}

	/// <summary>
	/// Imposto la modalità di fruizione in delete
	/// </summary>
	public void setDeleteMode(){
		Ctrl.setDeleteMode();
		GUIManager.disableCursorElement ();
		cursorDeleteMode.SetActive (true);
		GUIManager.showHelp(LanguageManager.HELP_DELETE_TITLE,LanguageManager.HELP_DELETE);

	}

	/// <summary>
	/// Nasconde la griglia del layer corrente
	/// </summary>
	/// <param name="visible">If set to <c>true</c> visible.</param>
	public void showGrid (bool visible)
	{
		currentActive.showGrid (visible);

	}


	public void showCellGrid(bool visible){
		for (int i = 0; i < cg.layers.Count; i++) {
			GameObject l = (GameObject)cg.layers [i];
			gridLayer g = l.GetComponent<gridLayer> ();
			g.showCellGrid (visible);
		}
	}

	/// <summary>
	/// Inverte lo stato di visualizzazione della griglia nel layer corrente
	/// </summary>
	public void invertGridShow(){
		currentActive.invertGridShow ();
	}


	/// <summary>
	/// imposta (nel caso del doppio battente) se l'inserimento/modifica deve essere specualare o meno
	/// </summary>
	/// <param name="state">If set to <c>true</c> state.</param>
	public void setSpecularMode(bool state){
		addspecularcell = state;
	}

	/// <summary>
	/// Inverte lo stato di inserimento speculare
	/// </summary>
	public void invertSpecularMode(){
		addspecularcell = !addspecularcell;
	}


	/// <summary>
	/// Chiede conferma per eliminare tutti gli elementi presenti sulla griglia corrente 
	/// </summary>
	public void eraseGrid(){
		GUIManager.showHelp(LanguageManager.HELP_ERASEGRID_TITLE,LanguageManager.HELP_ERASEGRID);
		GUIManager.showConfirmBox ("Cancellazione griglia","Sicuro di voler eliminare tutti gli elementi del livello corrente?","Camera","ControlsDesign","deleteGrid");
	}


	/// <summary>
	/// elimina tutti gli elementi presenti sulla griglia corrente 
	/// </summary>
	public void deleteGrid ()
	{

		currentActive.ClearGrid();
	}





	public void addCell(Action<bool> added,Item item=null,bool showWaiting=false){
		if(showWaiting) GUIManager.showWaitingBox ("Inserimento elemento", "Attendere",false);
		StartCoroutine (ThreadaddCell ((s)=>{
			added(s);
			if(showWaiting) GUIManager.closeWaitingBox();
		}, item));

	}


	/// <summary>
	/// Aggiunge una cella spalmandola su più CellGrid
	/// </summary>
	/// <param name="specchiato">If set to <c>true</c> specchiato.</param>
	private IEnumerator ThreadaddCell(Action<bool> added,Item item=null){

		yield return new WaitForFixedUpdate ();
		if (item == null)
			item = itemSelected;

		Vector3 pos=cursorObject.transform.localPosition;
		cg.addCellToGrid (pos,item,currentActive, false);

		if(cg.gate.misure.width_central_frame>0 && addspecularcell){
			Vector3 pos2 = calcolateSpecularPosition (pos);
			cg.addCellToGrid (pos2, item,currentActive, true);
		}
		added (true);
	}





	/// <summary>
	/// Restituisce il valore della posizione speculare
	/// </summary>
	/// <returns>The specular position.</returns>
	/// <param name="pos">Position.</param>
	private Vector3 calcolateSpecularPosition(Vector3 pos){
        pos = new Vector3((cg.gate.width_gross  - pos.x), pos.y, currentActive.Z);
		return pos;
	}


	/// <summary>
	/// trova una cella mediante coordinate
	/// </summary>
	/// <returns>The cell.</returns>
	/// <param name="pos">Position.</param>
	public GameObject searchElement(Vector3 pos,bool isSpecular =false){

		//se si sta cercando la posizione speculare
		//imposta la posizione speculare
		if(isSpecular)
			pos = calcolateSpecularPosition(pos);

		//verifica tutte le celle del layer 

		Element[] cells=currentActive.GetComponentsInChildren<Element>();
		if(cells.Length>0)
			for (int i=0; i<cells.Length; i++) {
				if(cells[i].drawCoordinate.x==pos.x && cells[i].drawCoordinate.y==pos.y && cells[i].drawCoordinate.z==pos.z){
					return cells[i].transform.gameObject;

				}
			}


		return null;
	}


	/// <summary>
	/// Controlla la validità di una configurazione del cancello
	/// </summary>
	/// <returns><c>true</c>, if valid gate was ised, <c>false</c> otherwise.</returns>
	public bool isValidGate ()
	{ 
		// Il primo layer è obbligatorio
		if (!((GameObject)cg.layers [0]).GetComponent<gridLayer> ().isNotEmptyLayer ()) {
			return false;
		}

		// Controllo la validità di ogni songolo layer usato
		if (!((GameObject)cg.layers [0]).GetComponent<gridLayer> ().isValidLayer () ||
			!((GameObject)cg.layers [1]).GetComponent<gridLayer> ().isValidLayer () ||
			!((GameObject)cg.layers [2]).GetComponent<gridLayer> ().isValidLayer ()) {
			return false;
		}

		return true;
	}




	/// <summary>
	/// Chiude MyGate
	/// </summary>
	public void cancelGate(){
		GUIManager.showConfirmBox ("Annulla le modifiche", "Continuando le modifiche fatte non verranno memorizzate", "Camera", "ControlsDesign", "closeMyGate"); 
	}

	public void closeMyGate(){
		GUIManager.showWaitingBox ("Chiusura di MyGate", "Attendere ...");
		Application.ExternalCall("closeMyGate");
		#if !UNITY_WEBGL
		Application.LoadLevel("gates");
		#endif
	}


	#region SAVING
	/// <summary>
	/// Avvia la procedura per il salvataggio finale del cancello
	/// </summary>
	public void saveGate(){

		if (!isMiddleLayerNotEmpty()) {
			GUIManager.showDialogBox ("Il tuo cancello non è correttamente configurato",LanguageManager.MESSAGE_ALERT_BEFORE_CLOSE);
			return;
		}



		GUIManager.showWaitingBox ("Salvataggio in corso", "Stiamo salvando il tuo cancello.");

		ThreadSaveGate (
			(s)=>{
				GUIManager.closeWaitingBox();
				GUIManager.showConfirmBox("Continua",LanguageManager.text(LanguageManager.MESSAGE_SAVE,price.ToString ()),"Camera","ControlsDesign","confirmOrder",LanguageManager.BT_OK_CHECKOUT,LanguageManager.BT_CONTINUE_DRAWING ); 
			},
			(e)=>{
				GUIManager.closeWaitingBox();
				GUIManager.showDialogBox("Errore","Si è verificato un errore durante il salvataggio sul database remoto");
			}
		);

	}

	/// <summary>
	/// Salva il cancello e lo riporta nella scena delle recinzioni
	/// </summary>
	public void keepingGateFromDestr(){

		ThreadSaveGate (
			(s)=>{
				GUIManager.closeWaitingBox();

				GameObject gateToFence = GameObject.Find ("Gate");

				//	DontDestroyOnLoad (gateToFence);
				if(!StaticVariables.comingFromFence)
					StaticVariables.comingFromGate = true;
				if (StaticVariables.comingFromFence) {
					gateToFence.name = "recinz";
					gateToFence.tag = "freshFence";
				}

				DontDestroyOnLoad (gateToFence);

				StaticVariables.comingFromGate = true;
				SceneManager.LoadScene (StaticVariables.scenePlan);

			},
			(e)=>{
				GUIManager.closeWaitingBox();
				GUIManager.showDialogBox("Errore","Si è verificato un errore durante il salvataggio sul database remoto");
			}
		);

	}


	public struct SavedStruct{
		public int id_gate;
		public string json;

	};

	/// <summary>
	/// prende tutti gli elementi inseriti nel cancello con i relativi riferimenti di posizione e crea un JSON (per il salvataggio)
	/// </summary>
	public void ThreadSaveGate(Action<SavedStruct> saved,Action<bool> error){

		//esplora il primo layer

		JSONObject gateJson=new JSONObject(JSONObject.Type.OBJECT);



		gateJson.AddField("user",StaticVariables.username); 
		gateJson.AddField("password",StaticVariables.password); 



		JSONObject gateDetailJson = new JSONObject (JSONObject.Type.OBJECT);


		gateDetailJson.AddField("id",cg.gate.id); 
		gateDetailJson.AddField("id_style",cg.gate.id_style);
		gateDetailJson.AddField("width",cg.gate.width);
		gateDetailJson.AddField("height",cg.gate.height);
		gateDetailJson.AddField("maxheight",cg.gate.heightmax);
		gateDetailJson.AddField("type",cg.gate.id_type);
		gateDetailJson.AddField("geometry",cg.gate.id_geometry);
		gateDetailJson.AddField("direction-open",cg.gate.opening);

		gateDetailJson.AddField("source",StaticVariables.source);


		gateJson.AddField("gate",gateDetailJson); 



		JSONObject layersJson = new JSONObject(JSONObject.Type.ARRAY);

		gateDetailJson.AddField ("layers", layersJson);

		for(int i=0;i<cg.layers.Count;i++){
			GameObject layer=(GameObject)cg.layers[i];

			if(layer==null)
				continue;


			//crea il json del layer
			JSONObject layerJson=new JSONObject(JSONObject.Type.OBJECT);

			ItemColor lc = layer.GetComponent<gridLayer> ().getColorLayer();

			layerJson.AddField("layer",layer.name);

			if (lc.id>0) {
				layerJson.AddField ("id_colorlayer", lc.id);
				layerJson.AddField ("colorlayer", ItemColor.ColorToHex (lc.color));
				layerJson.AddField ("colornamelayer", lc.name);
			}




			JSONObject cellsJson = new JSONObject(JSONObject.Type.ARRAY);

			layerJson.AddField("cells",cellsJson);


			//prendi tutti i figli del layer
			Element[] el=layer.GetComponentsInChildren<Element>();

			for(int j=0;j<el.Length;j++){

				Element c = el[j];
				//prendi tutti gli oggetti assegnat alla cella
				JSONObject cellJson=new JSONObject(JSONObject.Type.OBJECT);

				cellJson.AddField("id",c.item.id);
				cellJson.AddField("name",c.item.name);
				cellJson.AddField("x",c.drawCoordinate.x.ToString());
				cellJson.AddField("y",c.drawCoordinate.y.ToString());
				cellJson.AddField("z",c.drawCoordinate.z.ToString());
				cellJson.AddField("specular",c.is_specular);

				cellsJson.Add(cellJson);

			}

			layersJson.Add (layerJson);
		}

		//attribuisci il json al gate
		GameObject.Find ("Gate").GetComponent<ItemGate>().json=gateJson.Print();

		//salva sul webservice
		StartCoroutine(
			sendValueToDB(
				gateJson.Print (),
				false,
				(id_gate)=>{

					SavedStruct s2=new SavedStruct();

					s2.id_gate=id_gate;
					s2.json=gateJson.Print ();

					GameObject.Find ("Gate").GetComponent<ItemGate>().id=id_gate;

					saved(s2);
				},
				(e)=>{
					error(e);
				}

			)
		);

	}



	/// <summary>
	/// Invia i dati al DB remoto
	/// </summary>
	/// <returns>The value to D.</returns>
	/// <param name="json">Json.</param>
	/// <param name="screen">Screen.</param>
	/// <param name="objBase64">Object base64.</param>
	private IEnumerator sendValueToDB(String json,bool withoutScreen,Action<int> saved,Action<bool> error){

		WWWForm form = new WWWForm ();
		//crea lo screenshot
		if (!withoutScreen){
			moveCamera.resetCamera ();

		
			showGrid (false);
			yield return new WaitForEndOfFrame ();
			String screen = ScreenShootToPNG ();


			form.AddField ("image", screen);
		}

		//invia i dati

		byte[] json_byte = Encoding.UTF8.GetBytes (json);

		form.AddField( "user", StaticVariables.username );
		form.AddField( "password", StaticVariables.password );
		form.AddBinaryData ("config", json_byte,"configurazione.json","text/json");


		//form.AddField( "obj", objBase64 );


		//Hashtable headers = form.headers;
		byte[] rawData = form.data;

		string url = StaticVariables.BASE_URL+"index.php?option=com_configuratore&view=cancello&format=json&task=save";

		// Post a request to an URL with our custom headers
		WWW www = new WWW(url, form);
		yield return www;



		if (www.error == null) {
			JSONNode result = JSON.Parse (www.text);
			if (result ["saved"].AsBool) {
				cg.gate.id = result ["id"].AsInt;
				price = result ["invoice"].AsDouble;
				Library.last_is_saved = true;
				saved (cg.gate.id);
			} else
				error (true);
		} else {
			Debug.Log (www.error);
			error (true);
		}
	}

	#endregion


	/// <summary>
	/// Conferma il cancello e lo aggiunge ai "Tuoi cancellI"
	/// </summary>
	public void confirmOrder(){
		if (!isMiddleLayerFill ()) {

			GUIManager.showConfirmBox("Il tuo cancello non è correttamente configurato","Aggiungi elementi per riempire tutta la griglia centrale, se no non sarà possibile acquistarlo","Camera","ControlsDesign","ThreadConfirmOrder","ESCI DA MYGATE","OK CONTINUO");
			return;
		}
		ThreadConfirmOrder ();

	}

	public void ThreadConfirmOrder(){
		GUIManager.showWaitingBox ("Chiusura di MyGate", "Attendere ...");
		Application.ExternalCall ("confirmOrder",cg.gate.id.ToString());
		#if !UNITY_WEBGL
		Application.LoadLevel("gates");
		#endif
	}


	#region LOADING


	/// <summary>
	/// costruisce un cancello partendo dal json restituito dal DB
	/// </summary>
	public void getGatefromJSON(JSONArray configuration, Action<bool> success){



		if (configuration==null) {
			success (false);
			return;
		}




		ArrayList items = new ArrayList ();
		ArrayList elements = new ArrayList ();



		JSONArray layersJson = configuration;

		//per ogni layer prendi le celle
		for (int i=0; i<layersJson.Count; i++) {
			JSONNode layerJson=layersJson[i];
			JSONArray cellsJson=layerJson["cells"].AsArray;

			GameObject l = (GameObject)cg.layers [i];


			if(layerJson["id_colorlayer"]!=""){
				ItemColor c=searchColorFromLibrary(layerJson["id_colorlayer"].AsInt);
				if (c.id>0)
					setColorLayer (c.id, i);
			}

			if (layerJson ["id_colorframe"] != "") {
				ItemColor c=searchColorFromLibrary(layerJson["id_colorframe"].AsInt);
				if (c.id>0)
					setColorFrame (c.id);
			}




			//imposta il layer corretto

			for(int j=0;j<cellsJson.Count;j++){
				JSONNode cellJson=cellsJson[j];

				if (cellJson ["id"].AsInt == 0) //verifica se passa l'id corretto
					continue;
				//verifica se nella libreria l'oggetto è presente ed è stato instanziato l'obj
				Item item=getElementByAvaibles(cellJson["id"].AsInt);

				if (item==null) //se non crea l'item
					continue;

				//prende la cella nella griglia
				Element el=new Element();
				el.item = item;
				el.drawCoordinate=new Vector3 (cellJson ["x"].AsFloat, cellJson ["y"].AsFloat, cellJson ["z"].AsFloat);
				el.is_specular = cellJson ["specular"].AsBool;


				items.Add (item);
				elements.Add (el);

			}




		}



		downloadElements(items,0,(finish)=>{

			for(int i=0;i<items.Count;i++){
				Element e=(Element)elements[i];
				Item item=(Item)items[i];

				if (e.drawCoordinate.z == 0) {

					GameObject layer=(GameObject)cg.layers[0];
					cg.addCellToGrid(e.drawCoordinate,item,layer.GetComponent<gridLayer>(),e.is_specular);
				}

				if (e.drawCoordinate.z > 0) {
					GameObject layer=(GameObject)cg.layers[1];
					cg.addCellToGrid(e.drawCoordinate,item,layer.GetComponent<gridLayer>(),e.is_specular);
				}

				if (e.drawCoordinate.z < 0) {
					GameObject layer=(GameObject)cg.layers[2];
					cg.addCellToGrid(e.drawCoordinate,item,layer.GetComponent<gridLayer>(),e.is_specular);
				}


			}
			Application.ExternalCall("GateLoaded");
			activateLayer (0);
			success (true);


		});



	}
	/// <summary>
	/// Scarica tutti gli elementi passati
	/// </summary>
	/// <param name="items">Items.</param>
	/// <param name="index">Index.</param>
	/// <param name="finish">Finish.</param>
	private void downloadElements(ArrayList items,int index,Action<bool> finish){

		Item item=(Item)items[index];



		webservice.downloadElement (item, (value) => {

			item.obj = value;
			setElementByAvaibles(items,item);


			if(index==items.Count-1){
				finish(true);

			}else{
				index++;
				downloadElements(items,index,(finish2)=>{finish(finish2);});
			}
		},false
		);

	}

	#endregion

	/// <summary>
	/// Imposta l'elemento corrente passandogli solo l'ID
	/// </summary>
	/// <param name="id">Identifier.</param>
	public void setElement(int id){
		//forza l'edit mode
		GUIManager.activeCursorElement ();
		setItemSelected(getElementByAvaibles(id));

	}



	/// <summary>
	/// Gets the element by library of avaibles.
	/// </summary>
	/// <returns>The element by avaibles.</returns>
	/// <param name="id">Identifier.</param>
	private Item getElementByAvaibles(int id){

		for (int i=0; i<Library.ItemsAvaible.Count; i++) {
			Item item_tmp=(Item)Library.ItemsAvaible [i];
			if(item_tmp.id==id){
				return Item.Clone(item_tmp);
			}
		}

		Debug.Log ("element " +id+" not found");
		return null;
	}


	private void setElementToAvaible(Item item){
		for (int i=0; i<Library.ItemsAvaible.Count; i++) {
			Item item_tmp=(Item)Library.ItemsAvaible [i];
			if(item_tmp.id==item.id)
				item_tmp.obj = item.obj;
		}


	}


	/// <summary>
	/// Passa il valore dell'OBJ dalla libreria all'elemento (per evitare di riscaricarlo)
	/// </summary>
	/// <param name="items">Items.</param>
	/// <param name="item_origin">Item origin.</param>

	private void setElementByAvaibles(ArrayList items,Item item_origin){
		for (int i=0; i<items.Count; i++) {
			Item item_tmp=(Item)items [i];
			if(item_tmp.id==item_origin.id){
				item_tmp.obj=item_origin.obj;

			}

		}
		return ;
	}


	#region COLOR


	/// <summary>
	/// Colora un  layer con lo stesso colore
	/// </summary>
	/// <param name="param">Parameter.</param>
	public void setColorLayer(ArrayList param){
		setColorLayer (int.Parse(param [0].ToString()), int.Parse(param [1].ToString()));
	}

	public void setColorLayer(int id, int id_layer){

		//prendi il colore dalla libreria
		ItemColor colorlibrary=searchColorFromLibrary (id);

		GameObject o = (GameObject) cg.layers [id_layer];
		gridLayer l = o.GetComponent<gridLayer>();

		//definisci che il colore del layer
		l.setColorLayer (colorlibrary);

		//imposta il colore nella GUI
		GUIManager.changeColorLayer (id_layer, colorlibrary);

	}

	/// <summary>
	/// Prende il colore del layer
	/// </summary>
	/// <returns>The color layer.</returns>
	/// <param name="id_layer">Identifier layer.</param>
	public ItemColor getColorLayer(int id_layer){
		GameObject o = (GameObject) cg.layers [id_layer];
		ItemColor c = searchColorFromLibrary (o.GetComponent<gridLayer> ().getColorLayer().id);

		return c;

	}

	/// <summary>
	/// Prende il colore del frame
	/// </summary>
	public ItemColor getColorFrame(){

		return cg.gate.mat_frame;

	}


	/// <summary>
	/// Cambia il colore all telaio
	/// </summary>
	/// <param name="id">Identifier.</param>
	public void setColorFrame(int id){
		cg.gate.mat_frame = searchColorFromLibrary (id);
		cg.gate.mat_frame.setMaterial ();


		//cambia tutti gli elementi del frame
		Renderer[] objs=GameObject.Find("Frame").GetComponentsInChildren<Renderer>();
		for (int i = 0; i < objs.Length; i++) {
			objs[i].material = cg.gate.mat_frame.material;
		}


		GUIManager.changeColorFrame (cg.gate.mat_frame);

	}


	/// <summary>
	/// azzera la colorazione del layer corrente
	/// </summary>
	public void resetColorLayer(){


		//colora tutto con il colore di default
		setColorLayer (0,currentActive.numLayer);

		//deifinisci che il layer è stato colorato tutto
		currentActive.GetComponent<gridLayer> ().setColorLayer(null);

	}




	/// <summary>
	/// ricerca il colore dalla libreria di quelli disponibili
	/// </summary>
	/// <returns>The color from library.</returns>
	/// <param name="id">Identifier.</param>
	public ItemColor searchColorFromLibrary(int id){

		for (int i=0; i<Library.ColorsAvaible.Count; i++) {
			ItemColor color_tmp=(ItemColor)Library.ColorsAvaible [i];

			if(color_tmp.id==id){

				return color_tmp;
			}

		}

		return new ItemColor();

	}
	public ItemColor searchColorFromLibrary(String search){
		for (int i=0; i<Library.ColorsAvaible.Count; i++) {
			ItemColor color_tmp=(ItemColor)Library.ColorsAvaible [i];
			if(ItemColor.ColorToHex(color_tmp.color)==search || color_tmp.name==search){

				return color_tmp;
			}

		}
		return null;

	}

	#endregion

	bool is_screencapture_fase=false;

	/// <summary>
	/// Effettua il screencaputure e restituisce una PNG codificata in Base64
	/// </summary>
	/// <returns>The shoot to PN.</returns>
	private string ScreenShootToPNG()
	{

		//moveCamera.resetCamera ();



		//showCellGrid (false);
		//showGrid (false);
		GUIManager.disableCursorElement();

		Camera cam=Camera.main;
		int width=(int)cam.pixelWidth;
		int height=(int)cam.pixelHeight;
		cam.Render();
		Texture2D image = new Texture2D(width, height);
		image.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		image.Apply();

		string value = System.Convert.ToBase64String(image.EncodeToPNG());

		GUIManager.activeCursorElement();
		/*showCellGrid (true);
		showGrid (true);
		cursorObject.SetActive (true);*/

		return value;



	}



	/// <summary>
	/// Avvia il processo di cattura schermo (disabilitando la griglia)
	/// </summary>
	public void CaptureScreen(){
		Application.ExternalCall("CaptureScreen",ScreenShootToPNG ());

	}

	#region OBJ EXPORT

	/// <summary>
	/// Avvia il processo di esportazione OBJ
	/// </summary>
	public void exportToOBJ(){
		StartCoroutine (createOBJ ((value) => {

			OBJItem objitem = value; // prendi il valore dell'obj
			StartCoroutine(sendToExport(objitem));

		}));
	}


	/// <summary>
	/// Esporta il cancello in OBJ e lo salva su file
	/// </summary>
	public void exportOBJToFile(){
		StartCoroutine (createOBJ ((value) => {

			OBJItem objitem = value; // prendi il valore dell'obj


			string directory=cg.gate.id.ToString();

			System.IO.Directory.CreateDirectory(directory);

			//scrivi il file
			System.IO.File.WriteAllText(directory+"/"+cg.gate.id.ToString()+".obj", objitem.obj);
			for(int i=0;i<objitem.mtl.Count;i++){

				System.IO.File.WriteAllText(directory+"/"+objitem.mtl_name[i]+".mtl", objitem.mtl[i].ToString());	
			}

			GUIManager.showDialogBox("Procedura di esportazione","Modello OBJ creato correttamente nella cartella "+directory);


		}));

	}

	/// <summary>
	/// Invia l'OBJ (in byte) al DB remoto
	/// </summary>
	/// <returns>The to export.</returns>
	/// <param name="objitem">Objitem.</param>
	private IEnumerator sendToExport(OBJItem objitem){
		//prepara un form da inviare al server per il processo di esportazione
		WWWForm form = new WWWForm();

		byte[] obj_byte = Encoding.UTF8.GetBytes (objitem.obj);



		form.AddField ("id_gate", cg.gate.id.ToString ());
		form.AddBinaryData ("obj", obj_byte,cg.gate.id.ToString()+ ".obj","application/octet-stream");

		for(int i=0;i<objitem.mtl.Count;i++){
			byte[] mtl_byte = Encoding.UTF8.GetBytes ((string)objitem.mtl[i]);
			form.AddBinaryData ("mtl[]", mtl_byte,objitem.mtl_name[i].ToString()+ ".mtl","application/octet-stream");
		}

		byte[] rawData = form.data;

		string url = StaticVariables.BASE_URL+"index.php?option=com_configuratore&view=cancello&format=json&task=export";

		// Post a request to an URL with our custom headers
		WWW www = new WWW(url, form);
		yield return www;



		if (www.error==null) {
			JSONNode result = JSON.Parse (www.text);
			string file_path=result["file"];
			Application.ExternalCall("GateExported",file_path);

		}else
			Debug.Log (www.error);

	}

	/// <summary>
	/// Routine per la creazione OBJ
	/// </summary>
	/// <returns>The OB.</returns>
	/// <param name="finish">Finish.</param>
	private IEnumerator createOBJ(Action<OBJItem> finish){

		GameObject gate =  GameObject.Find ("Gate");

		showGrid (false);
		yield return new WaitForFixedUpdate ();

		OBJItem objString = ObjExporting.getObjString (false, gate);

		showGrid (true);
		finish(objString);

	}

	#endregion



	/// <summary>
	/// Resets the camera.
	/// </summary>
	public void resetCamera(){
		moveCamera.resetCamera ();
	}









	


	/// <summary>
	/// Accede alla schermata di configurazione
	/// </summary>
	public void goToSetting(){

		Application.LoadLevel ("gui");
	}



	public void showPrice(){
		if(isMiddleLayerNotEmpty())
			GUIManager.showDialogBox ("Richiesta preventivo", "Ad ora il tuo cancello costa " + price.ToString () + " €");
		else
			GUIManager.showDialogBox ("Richiesta preventivo", "Non è possibile valutare il tuo cancello. Inserisci almeno un elemento alla griglia");

	}
}
