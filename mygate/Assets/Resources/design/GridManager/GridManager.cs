﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {

	const int LAYER_DISTANCE = 20;
	const int FRAME_DEPTH = 40;

	public Material mat_frame;
	public Color[] colorGrids;
	public ArrayList layers ; // Lista di tutti i layers


	public ItemGate gate;
	//Gameobject che contengono il cancello
	GameObject root_go;
	GameObject frame_go;
	GameObject layer_go;

	Spline spline;



	public delegate void onInitFinish();
	public onInitFinish oninitfinish;


	public void Init(ItemGate g){


		gate = g;

		mat_frame = Resources.Load ("defaultMat") as Material;
		//create general gameobject

		root_go = new GameObject ();
		frame_go = new GameObject ();
		layer_go = new GameObject ();


		root_go.AddComponent<ItemGate> ();

		root_go.GetComponent<ItemGate> ().id = g.id;
		root_go.GetComponent<ItemGate> ().height = g.height;
		root_go.GetComponent<ItemGate> ().heightmax = g.heightmax;
		root_go.GetComponent<ItemGate> ().opening  = g.opening;
		root_go.GetComponent<ItemGate> ().date = g.date;
		root_go.GetComponent<ItemGate> ().name = g.name;

		root_go.GetComponent<ItemGate> ().width = g.width ;
		root_go.GetComponent<ItemGate> ().id_type = g.id_type ;
		root_go.GetComponent<ItemGate> ().id_style= g.id_style ;
		root_go.GetComponent<ItemGate> ().id_geometry = g.id_geometry;
		root_go.GetComponent<ItemGate> ().columnWidth = g.columnWidth;
		root_go.GetComponent<ItemGate> ().columnDepth = g.columnDepth;
		root_go.GetComponent<ItemGate> ().capitalWidth = g.capitalWidth;
		root_go.GetComponent<ItemGate> ().capitalDepth = g.capitalDepth;
		root_go.GetComponent<ItemGate> ().capitalHeight = g.capitalHeight;

		root_go.GetComponent<ItemGate> ().columnWidthL = g.columnWidthL;
		root_go.GetComponent<ItemGate> ().columnDepthL = g.columnDepthL;
		root_go.GetComponent<ItemGate> ().capitalWidthL = g.capitalWidthL;
		root_go.GetComponent<ItemGate> ().capitalDepthL = g.capitalDepthL;
		root_go.GetComponent<ItemGate> ().capitalHeightL = g.capitalHeightL;

		root_go.GetComponent<ItemGate> ().columnWidthR = g.columnWidthR;
		root_go.GetComponent<ItemGate> ().columnDepthR = g.columnDepthR;
		root_go.GetComponent<ItemGate> ().capitalWidthR = g.capitalWidthR;
		root_go.GetComponent<ItemGate> ().capitalDepthR = g.capitalDepthR;
		root_go.GetComponent<ItemGate> ().capitalHeightR = g.capitalHeightR;





		root_go.name = "Gate";
		frame_go.name = "Frame";
		frame_go.transform.SetParent (root_go.transform);
		layer_go.name = "Layer";
		layer_go.transform.SetParent (root_go.transform);

		//if colorgrids is not defined
		if (colorGrids==null) {
			Array.Resize (ref colorGrids, 3);
			colorGrids [0] = Color.red;
			colorGrids [1] = Color.blue;
			colorGrids [2] = Color.green;

		}

		//correzione forma 
		if (!gate.isGeometry()) 
			gate.heightmax = gate.height;


        //aggiunge le divisioni
        ArrayList columns = new ArrayList();
        if (gate.misure.width_central_frame > 0)
        {
            addDivision();
        }
       
        addDivision();
        // Creo il telaio
        makeFrame ();


        // Aggiunge le griglie
        regrid();
        //addGrids (columns,gate.misure.width_central_frame);

        //aggiunge un overlay che indica lo spazio "lordo" del componente
        createGrossArea();

        if(oninitfinish!=null)
			oninitfinish();

	}


    private void regrid(){
        ArrayList columns = new ArrayList();
        if (gate.misure.width_central_frame > 0)
        {
            columns.Add(0.5f);
            columns.Add(0.5f);
        }
        else
            columns.Add(1f);
        // Aggiunge le griglie
        addGrids(columns, gate.misure.width_central_frame);
        GameObject.Find("Camera").GetComponent<ControlsDesign>().activateLayer(0);
    }



	/// <summary>
	/// Aggiunge 3 gameobject per le 3 griglie del cancello
	/// </summary>
	/// <returns>The grids.</returns>
	private void addGrids(ArrayList columns, int gap){

        GameObject grid=new GameObject();
        //elimina tutte le griglie

        layers = new ArrayList ();
        if (layers.Count > 0)
            for (int i = 0; i < 3; i++)
            {
                grid = layer_go.transform.Find("grid_layer_" + (i + 1).ToString()).gameObject;
                grid.GetComponent<gridLayer>().regrid(true, false, columns, gap);
            }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                // Creo la griglia per il layer centrale
                grid = new GameObject();

                grid.transform.parent = layer_go.transform;
                grid.name = "grid_layer_" + (i + 1).ToString();
                grid.AddComponent<gridLayer>();
                switch (i)
                {
                    case (0):
                        grid.GetComponent<gridLayer>().Z = 0;
                        break;
                    case (1):
                        grid.GetComponent<gridLayer>().Z = LAYER_DISTANCE;
                        break;
                    case (2):
                        grid.GetComponent<gridLayer>().Z = -LAYER_DISTANCE;
                        break;
                }
                grid.GetComponent<gridLayer>().numLayer = i;
                grid.GetComponent<gridLayer>().gate = gate;
                grid.GetComponent<gridLayer>().hatSpline = spline;
                grid.GetComponent<gridLayer>().init();
                grid.GetComponent<gridLayer>().colorgrid = colorGrids[i];
                grid.GetComponent<gridLayer>().isActive = true;
                grid.GetComponent<gridLayer>().regrid(true, false, columns, gap);
                layers.Add(grid);
            }
        }

       
			

		


	}


    private void addDivision(){
        ArrayList columns = new ArrayList();
        columns.Add(0.5f);
        columns.Add(0.5f);

        addDivision(columns);
    }


	/// <summary>
	/// Adds the division to gate.
	/// </summary>
	private void addDivision(ArrayList columns){

        DestroyImmediate(GameObject.Find("center_right"));


		Vector3 division_dimension=new Vector3(gate.gateMaxDim.x,gate.gateMaxDim.y,gate.gateMaxDim.z);

		//verifica se il cappello ha o meno la struttura
		if(!gate.geometry.showStructure)
			division_dimension.y=gate.gateDim.y;

        float divPoint = gate.misure.area_lateral / 2 + gate.misure.width_frame;

        for (int i = 0; i < columns.Count-1;i++){
            float space = gate.gateDim.x * (float)columns[i] - (gate.misure.width_central_frame * (columns.Count - 1)) / columns.Count;

            divPoint =  divPoint + space + gate.misure.width_central_frame / 2+(gate.misure.width_central_frame / 2)*i;

            Vector3 dim = new Vector3(gate.misure.width_central_frame, division_dimension.y, FRAME_DEPTH);
            Vector3 pos = new Vector3(divPoint, division_dimension.y / 2 + gate.misure.area_bottom, 0);
            makeFrameBar(dim, pos, "center_right");


        }

	}




	/// <summary>
	/// Crea un elemento del frame
	/// </summary>
	/// <returns>The frame bar.</returns>
	/// <param name="dim">Dim.</param>
	/// <param name="pos">Position.</param>
	/// <param name="name">Name.</param>
	private GameObject makeFrameBar (Vector3 dim, Vector3 pos, string name)
	{
		
		Color color = Color.black;

		// Creo la barra
		GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
		go.transform.localScale = dim;
		go.transform.position = pos;
		go.transform.parent = frame_go.transform;
		go.GetComponent<Renderer>().material = mat_frame;
		go.name = name;

        go.AddComponent<ItemFrame>();

		return go;
	}



	/// <summary>
	/// Crea il telaio del cancello
	/// </summary>
	private void makeFrame ()
	{


        Vector3 coordsDown = new Vector3 (gate.width_gross / 2, gate.misure.height_bottom_frame / 2+gate.misure.area_bottom, 0);
        GameObject down=makeFrameBar (new Vector3 (gate.width_gross-gate.misure.area_lateral, gate.misure.height_bottom_frame, FRAME_DEPTH), coordsDown , "down");


        Vector3 coordsUp = new Vector3 (gate.width_gross / 2, gate.gateDim.y + (gate.misure.height_top_frame / 2)+gate.misure.height_bottom_frame+gate.misure.area_bottom, 0);
        GameObject up = makeFrameBar (new Vector3 (gate.width_gross- gate.misure.area_lateral, gate.misure.height_top_frame, FRAME_DEPTH), coordsUp , "up");

        // Definisco le coordinate per il montante di sinistra
        float height_frame = gate.gateDim.y + gate.misure.height_top_frame + gate.misure.height_bottom_frame;

        Vector3 coordsLeft = new Vector3 (gate.misure.area_lateral/2+gate.misure.width_frame/2, height_frame/2 +gate.misure.area_bottom , 0);
        Vector3 dimLeftMont= new Vector3 (gate.misure.width_frame, height_frame, FRAME_DEPTH);
		// Definisco le coordinate per il montante di destra
        Vector3 coordsRight = new Vector3 (gate.width_gross -gate.misure.area_lateral / 2-gate.misure.width_frame / 2, height_frame / 2 + gate.misure.area_bottom, 0);
        Vector3 dimRightMont = new Vector3 (gate.misure.width_frame, height_frame, FRAME_DEPTH);




		if (gate.id_geometry > 1)  {
			

			if (!gate.geometry.showBottomBar) {
				// Cancello il traverso superiore in quanto la cimasa è senza barra di sotto
				Destroy (up);
			}

			makeHat (frame_go, mat_frame);

			//Se il primo punto è più alto della barra in basso, aggiusta il montante in modo che si veda attaccato

			if(gate.geometry.relativePositions.Length>0){
				if(gate.geometry.relativePositions[0].y>0 && gate.geometry.showStructure){
					float y=(gate.gateMaxDim.y - gate.gateDim.y) * gate.geometry.relativePositions[0].y;
					dimLeftMont.y=dimLeftMont.y+y;
					coordsLeft.y=coordsLeft.y+y/2;
				}
			}

				//Se l'ultimo punto è più alto della barra in basso, aggiusta il montante in modo che si veda attaccato
			int last=gate.geometry.relativePositions.Length-1;
			if(gate.geometry.relativePositions.Length>0){
				if(gate.geometry.relativePositions[last].y>0 && gate.geometry.showStructure){
					float y=(gate.gateMaxDim.y - gate.gateDim.y) * gate.geometry.relativePositions[last].y;
						dimRightMont.y=dimRightMont.y+y;
						coordsRight.y=coordsRight.y+y/2;
					}
				}

			if(!gate.geometry.showStructure)
				frame_go.transform.Find ("Hat").gameObject.SetActive(false);

            if (gate.geometry.showBottomBar)
            {
                dimRightMont.y = dimRightMont.y + gate.misure.height_top_frame;
                dimLeftMont.y = dimLeftMont.y + gate.misure.height_top_frame;
                coordsLeft.y = coordsLeft.y + gate.misure.height_top_frame/2;
                coordsRight.y = coordsRight.y + gate.misure.height_top_frame/2;

            }

			
		}

        //calcolo l'altezza dei montanti laterali
       
       

		//Creo il montante di sinistra
        GameObject left=makeFrameBar (dimLeftMont, coordsLeft , "left");
		//Creo il montente di destra
        GameObject right=makeFrameBar (dimRightMont, coordsRight , "right");

		Vector3 central_point=new Vector3(left.transform.localPosition.x+(right.transform.localPosition.x-left.transform.localPosition.x)/2,(gate.heightmax+100F)/2,down.transform.localPosition.z);


		//Dimensioni cardine
		Vector3 dimJoint = new Vector3 (50, 100, 10);
		Vector3 dimKnocker = new Vector3 (80, gate.gateDim.y, 10);

		
		if (gate.opening == "destra" || gate.opening == "entrambe") {
			//creo i cardini di destra
			Vector3 coordsJointRight1 = new Vector3 (gate.gateDim.x + gate.misure.width_frame + dimJoint.x / 2, (gate.gateDim.y + gate.misure.height_top_frame + gate.misure.height_bottom_frame) - gate.misure.height_bottom_frame - dimJoint.y, 0);
			makeJoint (dimJoint, coordsJointRight1);
			Vector3 coordsJointRight2 = new Vector3 (gate.gateDim.x + gate.misure.width_frame + dimJoint.x / 2, gate.misure.height_bottom_frame + dimJoint.y, 0);
			makeJoint (dimJoint, coordsJointRight2);
		}


		//se l'apertura è solo a destra, metti il battente a sinistra
		if (gate.opening == "destra") {
			Vector3 coordsKnockerLeft = new Vector3 (- gate.misure.width_frame - dimJoint.x / 2, (gate.gateDim.y + gate.misure.height_top_frame + gate.misure.height_bottom_frame) / 2 - gate.misure.height_bottom_frame, -FRAME_DEPTH);
			makeKnocker(dimKnocker,coordsKnockerLeft);
		}

		//se l'apertura è solo a sinistra, metti il battente a sinistra
		if (gate.opening == "sinistra") {
			Vector3 coordsKnockerLeft = new Vector3 (gate.gateDim.x + gate.misure.width_frame + dimJoint.x / 2, (gate.gateDim.y + gate.misure.height_top_frame + gate.misure.height_bottom_frame) / 2 - gate.misure.height_bottom_frame, -FRAME_DEPTH);
			makeKnocker(dimKnocker,coordsKnockerLeft);
		}

		if (gate.opening == "sinistra" || gate.opening == "entrambe") {
			//creo i cardini di sinistra
			Vector3 coordsJointLeft1 = new Vector3 (- gate.misure.width_frame - dimJoint.x / 2, (gate.gateDim.y + gate.misure.height_top_frame + gate.misure.height_bottom_frame) - gate.misure.height_bottom_frame - dimJoint.y, 0);
			makeJoint (dimJoint, coordsJointLeft1);
			Vector3 coordsJointLeft2 = new Vector3 (- gate.misure.width_frame - dimJoint.x / 2, gate.misure.height_bottom_frame + dimJoint.y, 0);
			makeJoint (dimJoint, coordsJointLeft2);
		}

	}


	/// <summary>
	/// crea i cardini da collegare alle colonne
	/// </summary>
	/// <returns>The joint.</returns>
	/// <param name="dim">Dim.</param>
	/// <param name="pos">Position.</param>
	private GameObject makeJoint(Vector3 dim,Vector3 pos){
		
		// Creo la barra
		GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
		go.transform.localScale = dim ;
		go.transform.position = pos;
		go.transform.parent = frame_go.transform;
		go.GetComponent<Renderer>().material = mat_frame;
		go.name = "Cardine";

		BoxCollider box_collider = go.AddComponent<BoxCollider> (); //aggiungo un collider per poter selezionare il telaio

		return go;

	}



	/// <summary>
	/// crea il battente in caso di apertura solo da un lato
	/// </summary>
	/// <returns>The knocker.</returns>
	/// <param name="dim">Dim.</param>
	/// <param name="pos">Position.</param>
	private GameObject makeKnocker(Vector3 dim,Vector3 pos){
		// Creo la barra
		GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
		go.transform.localScale = dim ;
		go.transform.position = pos;
		go.transform.parent = frame_go.transform;
		go.GetComponent<Renderer>().material = mat_frame;
		go.name = "Battente";

		BoxCollider box_collider = go.AddComponent<BoxCollider> (); //aggiungo un collider per poter selezionare il telaio

		return go;

	}

	// Crea il cappello
	private void makeHat (GameObject parent, Material mat)
	{


		// Genero la spline della curva del cappello scelto
		GameObject cimasa = new GameObject ();
		spline=cimasa.AddComponent<Spline> ();
		cimasa.name = "Hat";
		cimasa.transform.parent = parent.transform;

		//definisci il tipo di interpolazione della curva

		switch (gate.geometry.type_curve) {
		case "spline_hermite":
			cimasa.GetComponent<Spline> ().interpolationMode = Spline.InterpolationMode.Hermite;
			break;
		case "spline_bspline":
			cimasa.GetComponent<Spline> ().interpolationMode = Spline.InterpolationMode.BSpline;
			break;
		case "linear":
			cimasa.GetComponent<Spline> ().interpolationMode = Spline.InterpolationMode.Linear;
			break;
		case "bezier":
			cimasa.GetComponent<Spline> ().interpolationMode = Spline.InterpolationMode.Bezier;
			break;

		}


		// Aggiungo i nodi alla spline
		for (int i = 0; i< gate.geometry.relativePositions.Length; i++) {
			GameObject go = cimasa.GetComponent<Spline> ().AddSplineNode ();
			go.transform.parent = parent.transform;
		}

		// Metto in posizione i nodi della spline
		SplineNode[] nodes = cimasa.GetComponent<Spline> ().SplineNodes;
		for (int i = 0; i< gate.geometry.relativePositions.Length; i++) {
            float x = gate.misure.area_lateral/2+gate.misure.width_frame+gate.gateDim.x * gate.geometry.relativePositions [i].x;
            float y = gate.gateMaxDim.y * gate.geometry.relativePositions[i].y+gate.misure.height_bottom_frame+gate.misure.area_bottom+gate.misure.height_top_frame/2;
            if (gate.geometry.showBottomBar)
                y = y + gate.misure.height_top_frame;

            nodes [i].Position = new Vector3 (x, y, 0);

		}


		cimasa.AddComponent<MeshRenderer> ();
		cimasa.transform.GetComponent<Renderer>().material = mat;

		cimasa.AddComponent<SplineMesh> ();
		cimasa.GetComponent<SplineMesh> ().spline = cimasa.GetComponent<Spline> ();

		// Numero di segmenti che compongono la curva
		cimasa.GetComponent<SplineMesh> ().segmentCount = 200;
		GameObject cubeMesh = GameObject.CreatePrimitive (PrimitiveType.Cube);
		cubeMesh.SetActive (false);
		cimasa.GetComponent<SplineMesh> ().baseMesh = cubeMesh.GetComponent<MeshFilter> ().mesh;
		cimasa.GetComponent<SplineMesh> ().updateMode = SplineMesh.UpdateMode.WhenSplineChanged;
		cimasa.GetComponent<Spline> ().interpolationMode = gate.geometry.interpolationMode;

		spline.UpdateSpline ();
		cimasa.transform.localScale = new Vector3 (1, gate.misure.width_frame, FRAME_DEPTH);



		//definisci il punto centrale se il cancello è a doppio battente
		if (gate.misure.width_central_frame>0 && gate.geometry.showStructure) {
			Vector3 VectorDivisionPoint = cimasa.GetComponent<Spline> ().GetPositionOnSpline (0.5F);	
			GameObject battente = frame_go.transform.Find ("center_right").gameObject;
            float h = VectorDivisionPoint.y-gate.misure.area_bottom-gate.misure.height_bottom_frame;
			if (h > 0){
				battente.transform.localScale = new Vector3 (battente.transform.localScale.x, h, battente.transform.localScale.z);
                battente.transform.position = new Vector3 (battente.transform.position.x, h / 2+gate.misure.area_bottom + gate.misure.height_bottom_frame, battente.transform.position.z);
			}
		}


	}



    public void updateFrame(ArrayList parameters){

        int width_center = int.Parse(ItemParam.GetParam(parameters, "width_central").value);


        Debug.Log(ItemParam.GetParam(parameters, "Button").value);

        gate.misure.width_central_frame = width_center;


        addDivision();

       
        gate.updateOtherParams();
        regrid();

    }

	
	/// <summary>
	/// Imposta la camera in modo che inquadri l'intero cancello
	/// </summary>
	public Vector3 setCameraToSeeGate(Vector3 offset)
    {
        Camera cam = Camera.main;
        cam.transform.position = new Vector3(gate.width_gross / 2 - offset.x, gate.height_gross / 2 - offset.y, - offset.z);
        cam.orthographicSize = gate.width_gross;

        return cam.transform.position;


    }

	/// <summary>
	/// Aggiunge un elemento alla griglia
	/// </summary>
	/// <param name="pos">Position.</param>
	/// <param name="item">Item.</param>
	/// <param name="layer">Layer.</param>
	/// <param name="specular">If set to <c>true</c> specular.</param>
	public void addCellToGrid(Vector3 pos,Item item,gridLayer layer,bool specular=false){
		layer.drawElement (item, pos, specular);
	}


    /// <summary>
    /// Crea un piano che indica l'area lorda della griglia
    /// </summary>
    public void createGrossArea(){
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Cube);
        plane.transform.localScale = new Vector3(gate.width_gross,1,gate.height_gross);
        plane.transform.localRotation = Quaternion.Euler(new Vector3(90f,0,0));
        plane.transform.localPosition = new Vector3(gate.width_gross/2, gate.height_gross /2, gate.misure.width_frame);
        plane.GetComponent<Renderer>().material = Resources.Load("Materials/gross_area") as Material;
        plane.name = "gross_area";
    }

}
