﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class gridLayer : MonoBehaviour {

	

	// Valore Z (profondità) a cui si trova ad operare la griglia
	public int Z;
	// Punto di partenza per disegnare la griglia, serve per muovere la griglia
	public Vector3 startingPoint;
	// Flag indicante che la griglia è attiva
	public bool isActive = false;
	//colore della griglia
	public Color colorgrid;
	// Lista di celle per comporre la griglia
	public ArrayList matrix = new ArrayList ();
	// Numero identificativo del layer
	public int numLayer;
    // Toggle associato al layer

   

	//verifica se il layer è stato colorato tutto di un unico colore (passa l'id del colore)
    public ItemGate gate;
	private ItemColor colorlayer;



	private bool is_gridshow = true;

	// Curva del cappello
	public Spline hatSpline;



	// Serie di oggetti che modellano l'albero delle griglie
	public GameObject gridViewElements;
	public GameObject gridViewGridElements;
	

	
	// Inizializzazione della glia
	public void init ()
	{
		// Uso lo starting point solo nelle coordinate X,Y
        //definisci il punto di partenza della griglia (in funzione degli spazi laterali)
        startingPoint = new Vector3(gate.misure.area_lateral / 2+gate.misure.width_frame, gate.misure.area_bottom+gate.misure.height_bottom_frame, 0);

		
        gridViewElements = new GameObject ();
		gridViewElements.name = "gridViewElements";
        gridViewElements.transform.parent = this.transform;

        gridViewGridElements = new GameObject ();
		gridViewGridElements.name = "gridViewGridElements";
        gridViewGridElements.transform.parent = this.transform;
	
		//add canvas
		gridViewGridElements.AddComponent<Canvas>();
		gridViewGridElements.AddComponent<CanvasScaler>();
		gridViewGridElements.AddComponent<GraphicRaycaster>();

	
	}


	/// <summary>
	/// Activate this instance.
	/// </summary>
	public void Activate(bool value){
		isActive = value;
		//metti tutti i box collider degli oggetti attivi o disattivi
		BoxCollider[] boxs=this.transform.GetComponentsInChildren<BoxCollider>();
		for (int i = 0; i < boxs.Length; i++)
			boxs [i].enabled = value;




	}

	#region GRID

	/// <summary>
    /// Crea la griglia per il cancello
    /// </summary>
    /// <param name="showGrid">If set to <c>true</c> show grid.</param>
    private void makeGrid (bool showGrid,ArrayList columns,int gap)
	{
		Vector3 dim = gate.style.generator;

        /**
		 * Esempio per capire come si muove la griglia:
		 * 
		 * Box della cella: -------------
		 * 					|			|
		 * 					|	°		|
		 * 					-------------
		 * Il cerchietto indica lo startingPoint, che si può muovere 
		 * all'interno del bounding box della prima cella in basso a sinistra
		 */

        // Calcolo il numero di celle che entrano nella luce del cancello
        int elements_Y = (int)Mathf.Ceil((gate.gateDim.y) / dim.y);

        Vector3 hatStartingPoint = new Vector3(startingPoint.x, (elements_Y * dim.y) + startingPoint.y, 0);

        int elements_X = 0;

        int i = 0;
        Vector3 startPointRelative=new Vector3(startingPoint.x,startingPoint.y,startingPoint.z);
        foreach(float column in columns){
            float space = gate.gateDim.x * column - (gap * (columns.Count - 1)) / columns.Count;
            elements_X = (int)Mathf.Ceil(space / dim.x);
            dim.x = space / elements_X;

            createCellGridObjs("COL"+i.ToString(),elements_Y, elements_X, dim, startPointRelative, gridViewGridElements);




            if (gate.isGeometry()){
                int elements_Yh = 0;
                Vector3 offset = new Vector3(0, gate.gateDim.y , 0);

                if (gate.geometry.showBottomBar){
                    elements_Yh = (int)Mathf.Ceil((gate.gateMaxDim.y  - hatStartingPoint.y+ gate.misure.height_top_frame) / dim.y);
                    offset.y = offset.y + gate.misure.height_top_frame;
                }
                else
                    elements_Yh = (int)Mathf.Ceil((gate.gateMaxDim.y- hatStartingPoint.y) / dim.y);

                createCellGridObjs("TOP",elements_Yh, elements_X, dim, startPointRelative+offset, gridViewGridElements, true);

            }

            startPointRelative.x = startPointRelative.x + space + gap;
            i++;



        }


        
		// Mostro o nascondo la griglia appena creata
		this.showGrid (showGrid);
	}

	 

	// 
	/// <summary>
	/// Metodo di utilità per creare gli oggetti delle celle (CellGrid)
	/// </summary>
	/// <param name="elements_Y">Elements y.</param>
	/// <param name="elements_X">Elements x.</param>
	/// <param name="dim">Dim.</param>
	/// <param name="startingPoint">Starting point.</param>
	/// <param name="gridView">Grid view.</param>
	/// <param name="isHat">If set to <c>true</c> is hat.</param>
	private void createCellGridObjs (string prename,int elements_Y, int elements_X, Vector3 dim, Vector3 startingPoint,GameObject gridView,bool isHat=false)
	{
		for (int y = 0; y < elements_Y; y++) {
			for (int x = 0; x < elements_X; x++) { 

				GameObject element = new GameObject ();

                element.name = prename+"_Y" + y.ToString() + "X" + x.ToString();

				element.AddComponent<Image> ();
				element.GetComponent<Image>().sprite=Resources.Load("btns/box", typeof(Sprite)) as Sprite;
				element.GetComponent<Image> ().color = colorgrid;
				/*element.AddComponent<BoxCollider> ();
				element.GetComponent<BoxCollider>().size=new Vector3(dim.x/2,dim.y/2,1);*/
				element.GetComponent<Image> ().rectTransform.sizeDelta = new Vector2 (dim.x, dim.y);

				Vector3 coords = new Vector3 (x * dim.x + (dim.x / 2), y * dim.y + (dim.y / 2), Z) + startingPoint;

				element.transform.position = coords;


                element.transform.parent = gridView.transform;

				

				element.AddComponent<CellGrid> ();
				//element.GetComponent<CellGrid> ().grid = this;
				element.GetComponent<CellGrid> ().drawCoordinates = coords;
				element.GetComponent<CellGrid> ().dimension = dim;
				//element.GetComponent<CellGrid> ().toZeroCoordinates = new Vector3 (-(dim.x / 2), -(dim.y / 2), -(dim.z / 2));
				element.GetComponent<CellGrid> ().coordInMatrix = new Vector3 (x, y, 0);
				element.GetComponent<CellGrid> ().isHat = isHat;

				bool toCut = false;

				bool upperHat = isUpperHat (element.GetComponent<CellGrid> (), out toCut);



				if (toCut) {
					element.GetComponent<CellGrid> ().toCut = true;
				}
				if (upperHat)
					element.GetComponent<CellGrid> ().overHat = true;


				matrix.Add (element);
			}
		}
	}




	// Controlla se una cella si trova completamente fuori dalla curva del cappello
	// Il parametro toCut indica che è una cella nella quale l'oggetto necessita di un taglio
	private bool isUpperHat (CellGrid cell, out bool toCut)
	{
		toCut = false;

		if (hatSpline==null || hatSpline.SplineNodes.Length == 0) //escludi se non è stato caricato nessun punto di controllo
			return false;

		// Calcolo i parametri t0 e t1 per la cella in questione
		// essi servono a restringere la spazialtà delle curva da controllare
		float numCellsInGate = (gate.style.generator.x / cell.dimension.x);
		float tm = 1.0f / numCellsInGate;
		float s = cell.drawCoordinates.x / gate.style.generator.x;
		float t0 = s - (tm / 2);
		float t1 = s + (tm / 2);

		// Coordinate del punto in baso a sinistra del bounding box dell'oggetto
		float posX = cell.drawCoordinates.x - (cell.dimension.x / 2);
		float posY = cell.drawCoordinates.y - (cell.dimension.y / 2);

		// Stato dei controlli per i singoli verici del bounding box (2D) della cella
		bool[] result = new bool[4];

		// Verifico se il primo vertice del bounding box si trova sopra la spline
		Vector3 point = new Vector3 (posX, posY, 0);
		float t = hatSpline.GetClosestPointParam (point, 5, t0, t1, 0.025f);
		Vector3 splinePoint = hatSpline.GetPositionOnSpline (t);
		if (splinePoint.y < point.y) {

			result [0] = true;
			toCut = true;
		}

		// Verifico se il secondo vertice del bounding box si trova sopra la spline
		point = new Vector3 (posX + cell.dimension.x, posY, 0);
		t = hatSpline.GetClosestPointParam (point, 5, t0, t1, 0.025f);
		splinePoint = hatSpline.GetPositionOnSpline (t);
		if (splinePoint.y < point.y) {
			result [1] = true;
			toCut = true;
		}

		// Verifico se il terzo vertice del bounding box si trova sopra la spline
		point = new Vector3 (posX, posY + cell.dimension.y, 0);
		t = hatSpline.GetClosestPointParam (point, 5, t0, t1, 0.025f);
		splinePoint = hatSpline.GetPositionOnSpline (t);
		if (splinePoint.y < point.y) {
			result [2] = true;
			toCut = true;
		}

		// Verifico se il quarto vertice del bounding box si trova sopra la spline
		point = new Vector3 (posX + cell.dimension.x, posY + cell.dimension.y, 0);
		t = hatSpline.GetClosestPointParam (point, 5, t0, t1, 0.025f);
		splinePoint = hatSpline.GetPositionOnSpline (t);
		if (splinePoint.y < point.y) {
			result [3] = true;
			toCut = true;
		}

		// Se almeno uno dei vertici sta dentro la curva, tutta la cella sta dentro la curva 
		for (int i = 0; i<result.Length; i++) {
			if (result [i] == false)
				return false;
		}

		return true;
	}

	// Cancello la vecchia griglia e ne creo una nuova
	public void regrid (bool immediately, bool showGrid,ArrayList columns,int gap)
	{
		destroyGrid (immediately);
         makeGrid (showGrid,columns,gap);
	}

   
	// Rende visibile o nasconde la griglia in base al flag in input
	public void showGrid (bool val)
	{
		is_gridshow = val;
		Image[] images;
		images = gridViewGridElements.GetComponentsInChildren<Image> ();
		for (int i = 0; i < images.Length; i++)
			images [i].enabled = val;
		
	}

	#endregion


	public void showCellGrid(bool val){
		gridViewGridElements.SetActive (val);
	}


	public void invertGridShow(){
		is_gridshow = !is_gridshow;
		this.showGrid (is_gridshow);

	}

	// Cancella la griglia
	void destroyGrid (bool immediately)
	{
		for (int i =0; i< matrix.Count; i++) {
			GameObject go = matrix [i] as GameObject;

			if (immediately)
				DestroyImmediate (go);
			else 
				Destroy (go);

		}
		matrix = new ArrayList ();
	}

	// Cancella l' elemento contenuto nella cella
	public void deleteElement (GameObject elementToDestroy)
	{

		Item item = elementToDestroy.GetComponent<Element> ().item;
		Vector3 pos = elementToDestroy.GetComponent<Element> ().drawCoordinate;

		//imposta le celle come non usate
		for (int i = 0; i < item.cells.x; i++){
			for (int j = 0; j < item.cells.y; j++) {
				//prendi la cella corrispondente
				float x = pos.x - (item.dimension.x / 2) + (gate.style.generator.x / 2)+gate.style.generator.x*i;
				float y = pos.y - (item.dimension.y / 2) + (gate.style.generator.y / 2)+gate.style.generator.y*j;
				CellGrid c = getCellGrid (new Vector3 (x, y, Z));
				c.used = false;

			}
		}
		// Distruggo l'oggetto
		Destroy (elementToDestroy);
	}


	// Disegna un elemento in una cella del cancello
	public bool drawElement (Item itemSelected, Vector3 pos, bool specular)
	{

		if (itemSelected==null)
			return false;

		//verifica se le celle su cui si vuole inserire l'oggetto non sono pieno
		bool used = false;
		bool isHat = false;
		bool toCut = false;
		for (int i = 0; i < itemSelected.cells.x; i++){
			for (int j = 0; j < itemSelected.cells.y; j++) {
				//prendi la cella corrispondente
				float x = pos.x - (itemSelected.dimension.x / 2) + (gate.style.generator.x / 2)+gate.style.generator.x*i;
				float y = pos.y - (itemSelected.dimension.y / 2) + (gate.style.generator.y / 2)+gate.style.generator.y*j;
				CellGrid c = getCellGrid (new Vector3 (x, y, Z));
				if (c == null) {
					return false;
				}
				if (c.used)
					used = true;
				if (c.isHat)
					isHat = true;
				if (c.toCut)
					toCut = true;

			}
		}

		if (used)
			return false;


		//procedi all'inserimento


		Vector3 dim = itemSelected.dimension;

		GameObject elementGO = (GameObject)Instantiate (itemSelected.obj);
		elementGO.SetActive (true);
		// Aggiungo il materiale all'elemento "default" che è figlio dell'oggetto 
		Component[] rederers = elementGO.transform.GetComponentsInChildren<MeshRenderer> ();
		foreach (Component comp in rederers) {

			comp.GetComponent<Renderer> ().sharedMaterial = colorlayer.material;
		}

		if (itemSelected.rotate != null)
			elementGO.transform.Rotate (itemSelected.rotate);


		pos.z = Z;
		// Setto il nome e le coordinate
		elementGO.name = itemSelected.name;
		elementGO.transform.position = pos;

		//riscalo l'oggetto in funzione delle dimensioni della griglia
		elementGO.transform.localScale=new Vector3(elementGO.transform.localScale.x*itemSelected.scale_x,elementGO.transform.localScale.y*itemSelected.scale_y,elementGO.transform.localScale.z);

		elementGO.AddComponent<Element> ();
		elementGO.GetComponent<Element> ().drawCoordinate = pos;
		elementGO.GetComponent<Element> ().item = itemSelected;

		// Se la cella ha un elemento "specchiato" rispetto alla sua normale rotazione
		if (specular) {
			elementGO.transform.Rotate (new Vector3 (0, 180, 0));
			elementGO.GetComponent<Element> ().is_specular = specular;
		}

		// Aggiungo il box collider per catturare gli eventi del mouse
		elementGO.AddComponent<BoxCollider> ();
		elementGO.GetComponent<BoxCollider> ().size = new Vector3 (dim.x, dim.y, dim.z);
        elementGO.transform.parent = gridViewElements.transform;
		


		//verifica se va tagliato l'elemento
		if (toCut) {
			// Taglio l'elememnto contenuto nella cella 
			elementGO.AddComponent<SliceMesh> ();

			// Calcolo i parametri t0 e t1 per la cella in questione
			// essi servono a restringere la spazialtà della curva per le operazioni che coivolgono la curva
			float numCellsInGate = (gate.gateDim.x / itemSelected.dimension.x);
			float t = 1.0f / numCellsInGate;
			float s = pos.x / gate.gateDim.x;
			elementGO.GetComponent<SliceMesh> ().t0 = s - (t / 2);
			elementGO.GetComponent<SliceMesh> ().t1 = s + (t / 2);

			elementGO.GetComponent<SliceMesh> ().spline = hatSpline;
		}



		//attribuisci alla cellgrid lo stato di occupato
		for (int i = 0; i < itemSelected.cells.x; i++){
			for (int j = 0; j < itemSelected.cells.y; j++) {
				//prendi la cella corrispondente
				float x = pos.x - (itemSelected.dimension.x / 2) + (gate.style.generator.x / 2)+gate.style.generator.x*i;
				float y = pos.y - (itemSelected.dimension.y / 2) + (gate.style.generator.y / 2)+gate.style.generator.y*j;

				CellGrid c = getCellGrid (new Vector3 (x, y, Z));
				if (!c)
					continue;
				c.used = true;
			}
		}





		return true;
	}






	// Riempie le celle (cells) della griglia con l'elemento selezionato
	public IEnumerator fillGrid (Item itemSelected,Action<bool> success)
	{

		yield return new WaitForFixedUpdate ();

		CellGrid[] cells;
		cells = gridViewGridElements.GetComponentsInChildren<CellGrid> ();
		for (int i = 0; i < cells.Length; i++){
			Vector3 pos = new Vector3 (cells [i].drawCoordinates.x + gate.style.generator.x/2 * (itemSelected.cells.x-1), cells [i].drawCoordinates.y + gate.style.generator.y/2 * (itemSelected.cells.y-1), Z);
			drawElement (itemSelected,pos ,false);
		}


		success (true);



	}

	


	#region COLOR
	//Colora tutto il layer
	public void setColorLayer(ItemColor color){
		colorlayer = color;

		colorlayer.setMaterial ();

		MeshRenderer[] elementslf = gridViewElements.GetComponentsInChildren<MeshRenderer> ();
		for (int i = 0; i < elementslf.Length; i++) 
			elementslf [i].material = colorlayer.material;




	}

	//prendi il colore corrente del  layer
	public ItemColor getColorLayer(){
		return colorlayer;
	}

	#endregion

	// Elimina tutte le celle (cells) della griglia corrente
	public void ClearGrid ()
	{

		for (int i=0;i<gridViewElements.transform.childCount;i++)
			deleteElement(gridViewElements.transform.GetChild(i).gameObject);
        

	}

	// Controlla la validità del cancello
	public bool isValidLayer ()
	{
		bool ceUnElemento = false;
		bool ceUnaCellaVuota = false;
		// Se non c'è neanche un elemento nel layer allora non lo considero
		for (int i =0; i< matrix.Count; i++) {
			GameObject go = matrix [i] as GameObject;
			CellGrid cg = go.GetComponent<CellGrid> ();
			if (!cg.overHat) {
				if (cg.used)
					ceUnElemento = true;
				if (!cg.used)
					ceUnaCellaVuota = true;
			}
		}


		if (ceUnElemento && ceUnaCellaVuota)
			return false;

		return true;
	}

	/// <summary>
	/// Controlla che il layer non sia vuoto
	/// </summary>
	/// <returns><c>true</c>, if not empty layer was ised, <c>false</c> otherwise.</returns>
	public bool isNotEmptyLayer ()
	{
		int nl = 0; 
		nl = gridViewElements.transform.childCount;
		//verifica se è presente una griglia destra (se è un cancello doppio battente)
		
		if (nl > 0)
			return true;

		return false;
	}

	/// <summary>
	/// Verifica che il layer ha tutte le celle occupate
	/// </summary>
	/// <returns><c>true</c>, if layer fill was ised, <c>false</c> otherwise.</returns>
	public bool isLayerFill(){
		CellGrid[] cells;
		cells=gridViewGridElements.GetComponentsInChildren<CellGrid> ();
		for (int i = 0; i < cells.Length; i++) {
			if (!cells [i].used)
				return false;
		}
		



		return true;
	}



	/// <summary>
	/// Gets the cell grid.
	/// </summary>
	/// <returns>The cell grid.</returns>
	/// <param name="drawCoordinates">Draw coordinates.</param>
	public CellGrid getCellGrid(Vector3 drawCoordinates){



		CellGrid[] cells;

		cells = gridViewGridElements.GetComponentsInChildren<CellGrid> ();

			
		for (int i = 0; i < cells.Length; i++) {
			if(Vector3.Distance(cells [i].drawCoordinates,drawCoordinates)<(0.001f)) //necessario per correggere un errore di approssimazione numerica 
				return cells [i];
			
		}


		


		return null;


	}




	/// <summary>
	/// Restituisce il valore della posizione speculare
	/// </summary>
	/// <returns>The specular position.</returns>
	/// <param name="pos">Position.</param>
	private Vector3 calcolateSpecularPosition(Vector3 pos){
		pos = new Vector3 ((gate.gateDim.x / 2) + ((gate.gateDim.x / 2) -pos.x),pos.y,Z);

		return pos;
	}


	public CellGrid findNearCell(Vector3 pos, float range){

		CellGrid[] cells;

		cells = gridViewGridElements.GetComponentsInChildren<CellGrid> ();

		for (int i = 0; i < cells.Length; i++) {
			if(VerifyroundPosition(cells [i].drawCoordinates,pos,range))
				return cells [i];
		}



		

		return null;


	}

	private bool VerifyroundPosition(Vector3 centralpoint,Vector3 pos,float range){
		if ((pos.x-range) <= centralpoint.x  && (pos.x+range) >= centralpoint.x)
		if ((pos.y-range) <= centralpoint.y && (pos.y+range) >= centralpoint.y)
			return true;

		return false;
	}


	public bool canAdd(Item itemSelected,Vector3 pos){
		bool tocut;
		return canAdd (itemSelected, pos, out tocut);
	}

	public bool canAdd(Item itemSelected,Vector3 pos,out bool cut){
		
		bool used = false;
		bool tocut=false;
		cut = tocut;
		for (int i = 0; i < itemSelected.cells.x; i++){
			for (int j = 0; j < itemSelected.cells.y; j++) {
				//prendi la cella corrispondente
				

                float x = pos.x - (itemSelected.dimension.x / 2) + (gate.style.generator.x / 2)+gate.style.generator.x*i;
				float y = pos.y - (itemSelected.dimension.y / 2) + (gate.style.generator.y / 2)+gate.style.generator.y*j;
				CellGrid c = getCellGrid (new Vector3 (x, y, Z));

				if (c == null) {
					return false;
				}
				if (c.used)
					used = true;
				if(c.toCut)
					tocut=true;


			}
		}


		cut = tocut;
		return !used;

	}
}
