﻿using UnityEngine;
using System.Collections;

/**
 * Rappresenta una cella della griglia.
 * Un elemento (Cell) può occupare più di una CellGrid
 */
public class CellGrid : MonoBehaviour
{

		// Coordinate che identificano la cella nella matrice
		public Vector3 coordInMatrix;
		// Flag indicante se la cella è usata
		public bool used = false;
		// Riferimento all'oggetto che la occupa
		public GameObject usedBy;
		// Coordinate per disegnare la cella
		public Vector3 drawCoordinates;
		// Vettore da sommare alle coordinate di disegno per avere il punto in basso a snistra frontale
		public Vector3 toZeroCoordinates;
		// Dimensione della cella
		public Vector3 dimension;
		// Flag indicante che la cella va tagliata per via del cappello che la trapassa
		public bool toCut;
		// Item della cella
		public Item item;
		// Flag indicante se la cella appartiene al cappello
		public bool isHat;
		// Flag indicante che la cella è posizionata più in alto della curva del cappello
		public bool overHat;



}
