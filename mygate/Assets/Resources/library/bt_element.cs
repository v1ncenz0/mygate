﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_element : MonoBehaviour {

	public int id_element;

	public void setElement(){
		GameObject.FindObjectOfType<ControlsDesign> ().setElement (id_element);
        GUIManager.showLibrary(false);
	
	}


	public IEnumerator setImage(string url_image){

		WWW www = new WWW(System.Uri.EscapeUriString(url_image));
		Texture2D img =  new Texture2D(80, 80);

		yield return www;

		if (www.error == "404 Not Found\r") {
			Debug.Log ("Error icon image by id element " +id_element.ToString()+": " + www.error+" url:"+url_image);
		}
		else{
			www.LoadImageIntoTexture (img);
	
			this.transform.Find("RawImage").GetComponent<Image> ().sprite =Sprite.Create(img,new Rect(0,0,img.width,img.height),new Vector2(0.5f,0.5f),10);
		}

	}



}
