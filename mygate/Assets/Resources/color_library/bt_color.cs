﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_color : MonoBehaviour {

	public ItemColor colorItem;
	public Color bg_color;
	public Texture bg_image;


	public void setColor(){
		bool is_frame=this.GetComponentInParent<params_colorlibrary> ().is_frame;

		if (is_frame)
			setColorFrame ();
		else
			setColorLayer ();
	}


	public void setColorLayer(){

		//get id layer
		int id_layer=this.GetComponentInParent<params_colorlibrary> ().id_layer;

		GameObject.FindObjectOfType<ControlsDesign>().setColorLayer(colorItem.id,id_layer);

		GUIManager.showColorLibrary (false);


	}

	public void setColorFrame(){
		GameObject.FindObjectOfType<ControlsDesign>().setColorFrame(colorItem.id);
		GUIManager.showColorLibrary (false);

	}

	public void setColorBackground(Color color){
		bg_color = color;
		this.GetComponentInChildren<RawImage> ().color = color;
	}


	public void setImage(Texture texture){
		bg_image = texture;
		this.GetComponentInChildren<RawImage> ().texture = bg_image;
	}




}
