﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class editorCalculus : MonoBehaviour {
	public Transform  pointToLook;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (pointToLook != null)
			transform.LookAt (pointToLook.position);
	}
}
