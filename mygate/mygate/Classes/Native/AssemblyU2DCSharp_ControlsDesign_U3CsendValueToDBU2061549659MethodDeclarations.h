﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<sendValueToDB>c__Iterator1
struct U3CsendValueToDBU3Ec__Iterator1_t2061549659;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<sendValueToDB>c__Iterator1::.ctor()
extern "C"  void U3CsendValueToDBU3Ec__Iterator1__ctor_m1571545700 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign/<sendValueToDB>c__Iterator1::MoveNext()
extern "C"  bool U3CsendValueToDBU3Ec__Iterator1_MoveNext_m1144247568 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<sendValueToDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CsendValueToDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1685215008 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<sendValueToDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CsendValueToDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m308513432 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<sendValueToDB>c__Iterator1::Dispose()
extern "C"  void U3CsendValueToDBU3Ec__Iterator1_Dispose_m3481523641 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<sendValueToDB>c__Iterator1::Reset()
extern "C"  void U3CsendValueToDBU3Ec__Iterator1_Reset_m3251791603 (U3CsendValueToDBU3Ec__Iterator1_t2061549659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
