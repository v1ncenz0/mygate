﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_buy
struct  bt_buy_t2549161545  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 bt_buy::id
	int32_t ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(bt_buy_t2549161545, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
