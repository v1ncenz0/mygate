﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineAnimatorCustomValue
struct SplineAnimatorCustomValue_t4274418512;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"

// System.Void SplineAnimatorCustomValue::.ctor()
extern "C"  void SplineAnimatorCustomValue__ctor_m30604037 (SplineAnimatorCustomValue_t4274418512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineAnimatorCustomValue::Update()
extern "C"  void SplineAnimatorCustomValue_Update_m1874876346 (SplineAnimatorCustomValue_t4274418512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineAnimatorCustomValue::WrapValue(System.Single,System.Single,System.Single,UnityEngine.WrapMode)
extern "C"  float SplineAnimatorCustomValue_WrapValue_m3637971973 (SplineAnimatorCustomValue_t4274418512 * __this, float ___v0, float ___start1, float ___end2, int32_t ___wMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
