﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gridLayer/<fillGrid>c__Iterator0
struct U3CfillGridU3Ec__Iterator0_t2219342100;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void gridLayer/<fillGrid>c__Iterator0::.ctor()
extern "C"  void U3CfillGridU3Ec__Iterator0__ctor_m463307827 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer/<fillGrid>c__Iterator0::MoveNext()
extern "C"  bool U3CfillGridU3Ec__Iterator0_MoveNext_m2023799973 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gridLayer/<fillGrid>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CfillGridU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m581340953 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gridLayer/<fillGrid>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CfillGridU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1241562129 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer/<fillGrid>c__Iterator0::Dispose()
extern "C"  void U3CfillGridU3Ec__Iterator0_Dispose_m125485514 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer/<fillGrid>c__Iterator0::Reset()
extern "C"  void U3CfillGridU3Ec__Iterator0_Reset_m274733604 (U3CfillGridU3Ec__Iterator0_t2219342100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
