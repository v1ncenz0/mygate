﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Spline_InterpolationMode2569420497.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hat2
struct  Hat2_t1573290449  : public Il2CppObject
{
public:
	// System.Int32 Hat2::id
	int32_t ___id_0;
	// UnityEngine.Vector2[] Hat2::relativePositions
	Vector2U5BU5D_t686124026* ___relativePositions_1;
	// Spline/InterpolationMode Hat2::interpolationMode
	int32_t ___interpolationMode_2;
	// System.Boolean Hat2::senzaBarra
	bool ___senzaBarra_3;
	// System.Boolean Hat2::strutturaVisibile
	bool ___strutturaVisibile_4;
	// System.String Hat2::tipologia_curva
	String_t* ___tipologia_curva_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_relativePositions_1() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___relativePositions_1)); }
	inline Vector2U5BU5D_t686124026* get_relativePositions_1() const { return ___relativePositions_1; }
	inline Vector2U5BU5D_t686124026** get_address_of_relativePositions_1() { return &___relativePositions_1; }
	inline void set_relativePositions_1(Vector2U5BU5D_t686124026* value)
	{
		___relativePositions_1 = value;
		Il2CppCodeGenWriteBarrier(&___relativePositions_1, value);
	}

	inline static int32_t get_offset_of_interpolationMode_2() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___interpolationMode_2)); }
	inline int32_t get_interpolationMode_2() const { return ___interpolationMode_2; }
	inline int32_t* get_address_of_interpolationMode_2() { return &___interpolationMode_2; }
	inline void set_interpolationMode_2(int32_t value)
	{
		___interpolationMode_2 = value;
	}

	inline static int32_t get_offset_of_senzaBarra_3() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___senzaBarra_3)); }
	inline bool get_senzaBarra_3() const { return ___senzaBarra_3; }
	inline bool* get_address_of_senzaBarra_3() { return &___senzaBarra_3; }
	inline void set_senzaBarra_3(bool value)
	{
		___senzaBarra_3 = value;
	}

	inline static int32_t get_offset_of_strutturaVisibile_4() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___strutturaVisibile_4)); }
	inline bool get_strutturaVisibile_4() const { return ___strutturaVisibile_4; }
	inline bool* get_address_of_strutturaVisibile_4() { return &___strutturaVisibile_4; }
	inline void set_strutturaVisibile_4(bool value)
	{
		___strutturaVisibile_4 = value;
	}

	inline static int32_t get_offset_of_tipologia_curva_5() { return static_cast<int32_t>(offsetof(Hat2_t1573290449, ___tipologia_curva_5)); }
	inline String_t* get_tipologia_curva_5() const { return ___tipologia_curva_5; }
	inline String_t** get_address_of_tipologia_curva_5() { return &___tipologia_curva_5; }
	inline void set_tipologia_curva_5(String_t* value)
	{
		___tipologia_curva_5 = value;
		Il2CppCodeGenWriteBarrier(&___tipologia_curva_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
