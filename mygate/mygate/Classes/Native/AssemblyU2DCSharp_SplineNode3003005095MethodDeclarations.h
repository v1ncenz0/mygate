﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineNode
struct SplineNode_t3003005095;
// SplineNode/NodeParameterRegister
struct NodeParameterRegister_t3732266052;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void SplineNode::.ctor()
extern "C"  void SplineNode__ctor_m2705107000 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineNode::get_Position()
extern "C"  Vector3_t2243707580  SplineNode_get_Position_m996778698 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineNode::set_Position(UnityEngine.Vector3)
extern "C"  void SplineNode_set_Position_m3905037255 (SplineNode_t3003005095 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SplineNode::get_Rotation()
extern "C"  Quaternion_t4030073918  SplineNode_get_Rotation_m2876047543 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineNode::set_Rotation(UnityEngine.Quaternion)
extern "C"  void SplineNode_set_Rotation_m1810630844 (SplineNode_t3003005095 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineNode::get_CustomValue()
extern "C"  float SplineNode_get_CustomValue_m2030325969 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineNode::set_CustomValue(System.Single)
extern "C"  void SplineNode_set_CustomValue_m3029998584 (SplineNode_t3003005095 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineNode::get_TransformedNormal()
extern "C"  Vector3_t2243707580  SplineNode_get_TransformedNormal_m3070977479 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode/NodeParameterRegister SplineNode::get_Parameters()
extern "C"  NodeParameterRegister_t3732266052 * SplineNode_get_Parameters_m1277385376 (SplineNode_t3003005095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
