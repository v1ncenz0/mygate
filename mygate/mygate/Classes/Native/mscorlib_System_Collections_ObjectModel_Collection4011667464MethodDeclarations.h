﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<FaceIndices>
struct Collection_1_t4011667464;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FaceIndices[]
struct FaceIndicesU5BU5D_t2696217939;
// System.Collections.Generic.IEnumerator`1<FaceIndices>
struct IEnumerator_1_t1945446537;
// System.Collections.Generic.IList`1<FaceIndices>
struct IList_1_t715896015;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::.ctor()
extern "C"  void Collection_1__ctor_m4293897299_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1__ctor_m4293897299(__this, method) ((  void (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1__ctor_m4293897299_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3140483806_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3140483806(__this, method) ((  bool (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3140483806_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2594407087_gshared (Collection_1_t4011667464 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2594407087(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4011667464 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2594407087_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m116541674_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m116541674(__this, method) ((  Il2CppObject * (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m116541674_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3993969519_gshared (Collection_1_t4011667464 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3993969519(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4011667464 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3993969519_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1787580663_gshared (Collection_1_t4011667464 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1787580663(__this, ___value0, method) ((  bool (*) (Collection_1_t4011667464 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1787580663_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3242614133_gshared (Collection_1_t4011667464 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3242614133(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4011667464 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3242614133_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m4234917120_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m4234917120(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m4234917120_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3954953574_gshared (Collection_1_t4011667464 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3954953574(__this, ___value0, method) ((  void (*) (Collection_1_t4011667464 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3954953574_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3381567967_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3381567967(__this, method) ((  bool (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3381567967_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m395903551_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m395903551(__this, method) ((  Il2CppObject * (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m395903551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m916298996_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m916298996(__this, method) ((  bool (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m916298996_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1247040107_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1247040107(__this, method) ((  bool (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1247040107_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3206844696_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3206844696(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t4011667464 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3206844696_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2408645765_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2408645765(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2408645765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::Add(T)
extern "C"  void Collection_1_Add_m2708247300_gshared (Collection_1_t4011667464 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2708247300(__this, ___item0, method) ((  void (*) (Collection_1_t4011667464 *, FaceIndices_t174955414 , const MethodInfo*))Collection_1_Add_m2708247300_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::Clear()
extern "C"  void Collection_1_Clear_m2506784392_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2506784392(__this, method) ((  void (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_Clear_m2506784392_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2087551470_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2087551470(__this, method) ((  void (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_ClearItems_m2087551470_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::Contains(T)
extern "C"  bool Collection_1_Contains_m503817802_gshared (Collection_1_t4011667464 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m503817802(__this, ___item0, method) ((  bool (*) (Collection_1_t4011667464 *, FaceIndices_t174955414 , const MethodInfo*))Collection_1_Contains_m503817802_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4272163744_gshared (Collection_1_t4011667464 * __this, FaceIndicesU5BU5D_t2696217939* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4272163744(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4011667464 *, FaceIndicesU5BU5D_t2696217939*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4272163744_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<FaceIndices>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4001711487_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4001711487(__this, method) ((  Il2CppObject* (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_GetEnumerator_m4001711487_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FaceIndices>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1871460046_gshared (Collection_1_t4011667464 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1871460046(__this, ___item0, method) ((  int32_t (*) (Collection_1_t4011667464 *, FaceIndices_t174955414 , const MethodInfo*))Collection_1_IndexOf_m1871460046_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m625894569_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, FaceIndices_t174955414  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m625894569(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))Collection_1_Insert_m625894569_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3325587126_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, FaceIndices_t174955414  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3325587126(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))Collection_1_InsertItem_m3325587126_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::Remove(T)
extern "C"  bool Collection_1_Remove_m3151740241_gshared (Collection_1_t4011667464 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3151740241(__this, ___item0, method) ((  bool (*) (Collection_1_t4011667464 *, FaceIndices_t174955414 , const MethodInfo*))Collection_1_Remove_m3151740241_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3974450413_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3974450413(__this, ___index0, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3974450413_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3835638753_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3835638753(__this, ___index0, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3835638753_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FaceIndices>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3972011935_gshared (Collection_1_t4011667464 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3972011935(__this, method) ((  int32_t (*) (Collection_1_t4011667464 *, const MethodInfo*))Collection_1_get_Count_m3972011935_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<FaceIndices>::get_Item(System.Int32)
extern "C"  FaceIndices_t174955414  Collection_1_get_Item_m1341387001_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1341387001(__this, ___index0, method) ((  FaceIndices_t174955414  (*) (Collection_1_t4011667464 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1341387001_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3464990218_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, FaceIndices_t174955414  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3464990218(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))Collection_1_set_Item_m3464990218_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3604436261_gshared (Collection_1_t4011667464 * __this, int32_t ___index0, FaceIndices_t174955414  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3604436261(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4011667464 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))Collection_1_SetItem_m3604436261_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2408764900_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2408764900(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2408764900_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<FaceIndices>::ConvertItem(System.Object)
extern "C"  FaceIndices_t174955414  Collection_1_ConvertItem_m40268534_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m40268534(__this /* static, unused */, ___item0, method) ((  FaceIndices_t174955414  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m40268534_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FaceIndices>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2518831552_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2518831552(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2518831552_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m759651094_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m759651094(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m759651094_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FaceIndices>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2819605089_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2819605089(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2819605089_gshared)(__this /* static, unused */, ___list0, method)
