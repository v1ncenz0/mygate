﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_scene
struct bt_scene_t1364368137;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void bt_scene::.ctor()
extern "C"  void bt_scene__ctor_m308218700 (bt_scene_t1364368137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene::setImage(System.String)
extern "C"  void bt_scene_setImage_m337757067 (bt_scene_t1364368137 * __this, String_t* ___url_image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator bt_scene::downloadImage(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * bt_scene_downloadImage_m4064462847 (bt_scene_t1364368137 * __this, String_t* ___url_image0, Action_1_t3344795111 * ___complete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene::setScene()
extern "C"  void bt_scene_setScene_m481697358 (bt_scene_t1364368137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene::setFinish()
extern "C"  void bt_scene_setFinish_m2109350837 (bt_scene_t1364368137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene::<setImage>m__0(UnityEngine.Texture2D)
extern "C"  void bt_scene_U3CsetImageU3Em__0_m3619641142 (bt_scene_t1364368137 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
