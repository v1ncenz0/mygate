﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;
// ItemColor
struct ItemColor_t1117508970;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Item
struct  Item_t2440468191  : public Il2CppObject
{
public:
	// System.Int32 Item::id
	int32_t ___id_0;
	// System.Int32 Item::type
	int32_t ___type_1;
	// UnityEngine.Vector3 Item::dimension
	Vector3_t2243707580  ___dimension_2;
	// System.Double Item::price
	double ___price_3;
	// System.Double Item::weight
	double ___weight_4;
	// System.String Item::name
	String_t* ___name_5;
	// System.String Item::iconPath
	String_t* ___iconPath_6;
	// System.Int32 Item::numPuntiSaldatura
	int32_t ___numPuntiSaldatura_7;
	// System.Int32 Item::style
	int32_t ___style_8;
	// UnityEngine.Object Item::obj
	Object_t1021602117 * ___obj_9;
	// System.String Item::obj_path
	String_t* ___obj_path_10;
	// UnityEngine.Vector3 Item::rotate
	Vector3_t2243707580  ___rotate_11;
	// ItemColor Item::color
	ItemColor_t1117508970 * ___color_12;
	// UnityEngine.Vector2 Item::cells
	Vector2_t2243707579  ___cells_13;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_dimension_2() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___dimension_2)); }
	inline Vector3_t2243707580  get_dimension_2() const { return ___dimension_2; }
	inline Vector3_t2243707580 * get_address_of_dimension_2() { return &___dimension_2; }
	inline void set_dimension_2(Vector3_t2243707580  value)
	{
		___dimension_2 = value;
	}

	inline static int32_t get_offset_of_price_3() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___price_3)); }
	inline double get_price_3() const { return ___price_3; }
	inline double* get_address_of_price_3() { return &___price_3; }
	inline void set_price_3(double value)
	{
		___price_3 = value;
	}

	inline static int32_t get_offset_of_weight_4() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___weight_4)); }
	inline double get_weight_4() const { return ___weight_4; }
	inline double* get_address_of_weight_4() { return &___weight_4; }
	inline void set_weight_4(double value)
	{
		___weight_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_iconPath_6() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___iconPath_6)); }
	inline String_t* get_iconPath_6() const { return ___iconPath_6; }
	inline String_t** get_address_of_iconPath_6() { return &___iconPath_6; }
	inline void set_iconPath_6(String_t* value)
	{
		___iconPath_6 = value;
		Il2CppCodeGenWriteBarrier(&___iconPath_6, value);
	}

	inline static int32_t get_offset_of_numPuntiSaldatura_7() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___numPuntiSaldatura_7)); }
	inline int32_t get_numPuntiSaldatura_7() const { return ___numPuntiSaldatura_7; }
	inline int32_t* get_address_of_numPuntiSaldatura_7() { return &___numPuntiSaldatura_7; }
	inline void set_numPuntiSaldatura_7(int32_t value)
	{
		___numPuntiSaldatura_7 = value;
	}

	inline static int32_t get_offset_of_style_8() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___style_8)); }
	inline int32_t get_style_8() const { return ___style_8; }
	inline int32_t* get_address_of_style_8() { return &___style_8; }
	inline void set_style_8(int32_t value)
	{
		___style_8 = value;
	}

	inline static int32_t get_offset_of_obj_9() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___obj_9)); }
	inline Object_t1021602117 * get_obj_9() const { return ___obj_9; }
	inline Object_t1021602117 ** get_address_of_obj_9() { return &___obj_9; }
	inline void set_obj_9(Object_t1021602117 * value)
	{
		___obj_9 = value;
		Il2CppCodeGenWriteBarrier(&___obj_9, value);
	}

	inline static int32_t get_offset_of_obj_path_10() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___obj_path_10)); }
	inline String_t* get_obj_path_10() const { return ___obj_path_10; }
	inline String_t** get_address_of_obj_path_10() { return &___obj_path_10; }
	inline void set_obj_path_10(String_t* value)
	{
		___obj_path_10 = value;
		Il2CppCodeGenWriteBarrier(&___obj_path_10, value);
	}

	inline static int32_t get_offset_of_rotate_11() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___rotate_11)); }
	inline Vector3_t2243707580  get_rotate_11() const { return ___rotate_11; }
	inline Vector3_t2243707580 * get_address_of_rotate_11() { return &___rotate_11; }
	inline void set_rotate_11(Vector3_t2243707580  value)
	{
		___rotate_11 = value;
	}

	inline static int32_t get_offset_of_color_12() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___color_12)); }
	inline ItemColor_t1117508970 * get_color_12() const { return ___color_12; }
	inline ItemColor_t1117508970 ** get_address_of_color_12() { return &___color_12; }
	inline void set_color_12(ItemColor_t1117508970 * value)
	{
		___color_12 = value;
		Il2CppCodeGenWriteBarrier(&___color_12, value);
	}

	inline static int32_t get_offset_of_cells_13() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___cells_13)); }
	inline Vector2_t2243707579  get_cells_13() const { return ___cells_13; }
	inline Vector2_t2243707579 * get_address_of_cells_13() { return &___cells_13; }
	inline void set_cells_13(Vector2_t2243707579  value)
	{
		___cells_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
