﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// wizard
struct wizard_t1721110033;

#include "codegen/il2cpp-codegen.h"

// System.Void wizard::.ctor()
extern "C"  void wizard__ctor_m2230030810 (wizard_t1721110033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
