﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// ItemGate
struct ItemGate_t2548129788;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Spline
struct Spline_t1260612603;
// GridManager/onInitFinish
struct onInitFinish_t901190412;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridManager
struct  GridManager_t4027472533  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material GridManager::mat_frame
	Material_t193706927 * ___mat_frame_4;
	// UnityEngine.Color[] GridManager::colorGrids
	ColorU5BU5D_t672350442* ___colorGrids_5;
	// System.Collections.ArrayList GridManager::layers
	ArrayList_t4252133567 * ___layers_6;
	// ItemGate GridManager::gate
	ItemGate_t2548129788 * ___gate_7;
	// UnityEngine.GameObject GridManager::root_go
	GameObject_t1756533147 * ___root_go_8;
	// UnityEngine.GameObject GridManager::frame_go
	GameObject_t1756533147 * ___frame_go_9;
	// UnityEngine.GameObject GridManager::layer_go
	GameObject_t1756533147 * ___layer_go_10;
	// Spline GridManager::spline
	Spline_t1260612603 * ___spline_11;
	// GridManager/onInitFinish GridManager::oninitfinish
	onInitFinish_t901190412 * ___oninitfinish_12;

public:
	inline static int32_t get_offset_of_mat_frame_4() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___mat_frame_4)); }
	inline Material_t193706927 * get_mat_frame_4() const { return ___mat_frame_4; }
	inline Material_t193706927 ** get_address_of_mat_frame_4() { return &___mat_frame_4; }
	inline void set_mat_frame_4(Material_t193706927 * value)
	{
		___mat_frame_4 = value;
		Il2CppCodeGenWriteBarrier(&___mat_frame_4, value);
	}

	inline static int32_t get_offset_of_colorGrids_5() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___colorGrids_5)); }
	inline ColorU5BU5D_t672350442* get_colorGrids_5() const { return ___colorGrids_5; }
	inline ColorU5BU5D_t672350442** get_address_of_colorGrids_5() { return &___colorGrids_5; }
	inline void set_colorGrids_5(ColorU5BU5D_t672350442* value)
	{
		___colorGrids_5 = value;
		Il2CppCodeGenWriteBarrier(&___colorGrids_5, value);
	}

	inline static int32_t get_offset_of_layers_6() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___layers_6)); }
	inline ArrayList_t4252133567 * get_layers_6() const { return ___layers_6; }
	inline ArrayList_t4252133567 ** get_address_of_layers_6() { return &___layers_6; }
	inline void set_layers_6(ArrayList_t4252133567 * value)
	{
		___layers_6 = value;
		Il2CppCodeGenWriteBarrier(&___layers_6, value);
	}

	inline static int32_t get_offset_of_gate_7() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___gate_7)); }
	inline ItemGate_t2548129788 * get_gate_7() const { return ___gate_7; }
	inline ItemGate_t2548129788 ** get_address_of_gate_7() { return &___gate_7; }
	inline void set_gate_7(ItemGate_t2548129788 * value)
	{
		___gate_7 = value;
		Il2CppCodeGenWriteBarrier(&___gate_7, value);
	}

	inline static int32_t get_offset_of_root_go_8() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___root_go_8)); }
	inline GameObject_t1756533147 * get_root_go_8() const { return ___root_go_8; }
	inline GameObject_t1756533147 ** get_address_of_root_go_8() { return &___root_go_8; }
	inline void set_root_go_8(GameObject_t1756533147 * value)
	{
		___root_go_8 = value;
		Il2CppCodeGenWriteBarrier(&___root_go_8, value);
	}

	inline static int32_t get_offset_of_frame_go_9() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___frame_go_9)); }
	inline GameObject_t1756533147 * get_frame_go_9() const { return ___frame_go_9; }
	inline GameObject_t1756533147 ** get_address_of_frame_go_9() { return &___frame_go_9; }
	inline void set_frame_go_9(GameObject_t1756533147 * value)
	{
		___frame_go_9 = value;
		Il2CppCodeGenWriteBarrier(&___frame_go_9, value);
	}

	inline static int32_t get_offset_of_layer_go_10() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___layer_go_10)); }
	inline GameObject_t1756533147 * get_layer_go_10() const { return ___layer_go_10; }
	inline GameObject_t1756533147 ** get_address_of_layer_go_10() { return &___layer_go_10; }
	inline void set_layer_go_10(GameObject_t1756533147 * value)
	{
		___layer_go_10 = value;
		Il2CppCodeGenWriteBarrier(&___layer_go_10, value);
	}

	inline static int32_t get_offset_of_spline_11() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___spline_11)); }
	inline Spline_t1260612603 * get_spline_11() const { return ___spline_11; }
	inline Spline_t1260612603 ** get_address_of_spline_11() { return &___spline_11; }
	inline void set_spline_11(Spline_t1260612603 * value)
	{
		___spline_11 = value;
		Il2CppCodeGenWriteBarrier(&___spline_11, value);
	}

	inline static int32_t get_offset_of_oninitfinish_12() { return static_cast<int32_t>(offsetof(GridManager_t4027472533, ___oninitfinish_12)); }
	inline onInitFinish_t901190412 * get_oninitfinish_12() const { return ___oninitfinish_12; }
	inline onInitFinish_t901190412 ** get_address_of_oninitfinish_12() { return &___oninitfinish_12; }
	inline void set_oninitfinish_12(onInitFinish_t901190412 * value)
	{
		___oninitfinish_12 = value;
		Il2CppCodeGenWriteBarrier(&___oninitfinish_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
