﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::Exponential(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_Exponential_m2167370122 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::Logarithm(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_Logarithm_m1122452946 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::Conjugate(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_Conjugate_m1266554665 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::Negative(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_Negative_m1626020304 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::Normalized(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_Normalized_m2765555810 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::GetSquadIntermediate(UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_GetSquadIntermediate_m3050153534 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q00, Quaternion_t4030073918  ___q11, Quaternion_t4030073918  ___q22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::GetQuatLog(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_GetQuatLog_m546556180 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::GetQuatExp(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_GetQuatExp_m1040715337 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::GetQuatConjugate(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_GetQuatConjugate_m1225887828 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion QuaternionUtilities.QuaternionUtils::GetQuatNegative(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  QuaternionUtils_GetQuatNegative_m2634741223 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
