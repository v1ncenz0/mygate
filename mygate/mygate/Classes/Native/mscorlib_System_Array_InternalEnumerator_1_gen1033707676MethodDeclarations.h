﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1033707676.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void System.Array/InternalEnumerator`1<FaceIndices>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3755500553_gshared (InternalEnumerator_1_t1033707676 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3755500553(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1033707676 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3755500553_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FaceIndices>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691498485_gshared (InternalEnumerator_1_t1033707676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691498485(__this, method) ((  void (*) (InternalEnumerator_1_t1033707676 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691498485_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FaceIndices>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445780225_gshared (InternalEnumerator_1_t1033707676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445780225(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1033707676 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445780225_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FaceIndices>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3104282918_gshared (InternalEnumerator_1_t1033707676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3104282918(__this, method) ((  void (*) (InternalEnumerator_1_t1033707676 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3104282918_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FaceIndices>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m909506189_gshared (InternalEnumerator_1_t1033707676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m909506189(__this, method) ((  bool (*) (InternalEnumerator_1_t1033707676 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m909506189_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FaceIndices>::get_Current()
extern "C"  FaceIndices_t174955414  InternalEnumerator_1_get_Current_m460620174_gshared (InternalEnumerator_1_t1033707676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m460620174(__this, method) ((  FaceIndices_t174955414  (*) (InternalEnumerator_1_t1033707676 *, const MethodInfo*))InternalEnumerator_1_get_Current_m460620174_gshared)(__this, method)
