﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>
struct KeyCollection_t2322684825;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4014198703;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2528690492.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2550354957_gshared (KeyCollection_t2322684825 * __this, Dictionary_2_t4134154350 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2550354957(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2322684825 *, Dictionary_2_t4134154350 *, const MethodInfo*))KeyCollection__ctor_m2550354957_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4045097475_gshared (KeyCollection_t2322684825 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4045097475(__this, ___item0, method) ((  void (*) (KeyCollection_t2322684825 *, Vector3_t2243707580 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4045097475_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3077342016_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3077342016(__this, method) ((  void (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3077342016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2505935981_gshared (KeyCollection_t2322684825 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2505935981(__this, ___item0, method) ((  bool (*) (KeyCollection_t2322684825 *, Vector3_t2243707580 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2505935981_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1454038272_gshared (KeyCollection_t2322684825 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1454038272(__this, ___item0, method) ((  bool (*) (KeyCollection_t2322684825 *, Vector3_t2243707580 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1454038272_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1138727570_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1138727570(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1138727570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2059008658_gshared (KeyCollection_t2322684825 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2059008658(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2322684825 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2059008658_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3098980915_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3098980915(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3098980915_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3773115990_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3773115990(__this, method) ((  bool (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3773115990_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1719759422_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1719759422(__this, method) ((  bool (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1719759422_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1187414882_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1187414882(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1187414882_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m970850892_gshared (KeyCollection_t2322684825 * __this, Vector3U5BU5D_t1172311765* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m970850892(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2322684825 *, Vector3U5BU5D_t1172311765*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m970850892_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2528690492  KeyCollection_GetEnumerator_m3190884433_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3190884433(__this, method) ((  Enumerator_t2528690492  (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_GetEnumerator_m3190884433_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2855091294_gshared (KeyCollection_t2322684825 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2855091294(__this, method) ((  int32_t (*) (KeyCollection_t2322684825 *, const MethodInfo*))KeyCollection_get_Count_m2855091294_gshared)(__this, method)
