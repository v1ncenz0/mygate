﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/MaterialData
struct  MaterialData_t3551903345  : public Il2CppObject
{
public:
	// System.String OBJ/MaterialData::name
	String_t* ___name_0;
	// UnityEngine.Color OBJ/MaterialData::ambient
	Color_t2020392075  ___ambient_1;
	// UnityEngine.Color OBJ/MaterialData::diffuse
	Color_t2020392075  ___diffuse_2;
	// UnityEngine.Color OBJ/MaterialData::specular
	Color_t2020392075  ___specular_3;
	// System.Single OBJ/MaterialData::shininess
	float ___shininess_4;
	// System.Single OBJ/MaterialData::alpha
	float ___alpha_5;
	// System.Int32 OBJ/MaterialData::illumType
	int32_t ___illumType_6;
	// System.String OBJ/MaterialData::diffuseTexPath
	String_t* ___diffuseTexPath_7;
	// System.String OBJ/MaterialData::bumpTexPath
	String_t* ___bumpTexPath_8;
	// UnityEngine.Texture2D OBJ/MaterialData::diffuseTex
	Texture2D_t3542995729 * ___diffuseTex_9;
	// UnityEngine.Texture2D OBJ/MaterialData::bumpTex
	Texture2D_t3542995729 * ___bumpTex_10;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ambient_1() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___ambient_1)); }
	inline Color_t2020392075  get_ambient_1() const { return ___ambient_1; }
	inline Color_t2020392075 * get_address_of_ambient_1() { return &___ambient_1; }
	inline void set_ambient_1(Color_t2020392075  value)
	{
		___ambient_1 = value;
	}

	inline static int32_t get_offset_of_diffuse_2() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___diffuse_2)); }
	inline Color_t2020392075  get_diffuse_2() const { return ___diffuse_2; }
	inline Color_t2020392075 * get_address_of_diffuse_2() { return &___diffuse_2; }
	inline void set_diffuse_2(Color_t2020392075  value)
	{
		___diffuse_2 = value;
	}

	inline static int32_t get_offset_of_specular_3() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___specular_3)); }
	inline Color_t2020392075  get_specular_3() const { return ___specular_3; }
	inline Color_t2020392075 * get_address_of_specular_3() { return &___specular_3; }
	inline void set_specular_3(Color_t2020392075  value)
	{
		___specular_3 = value;
	}

	inline static int32_t get_offset_of_shininess_4() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___shininess_4)); }
	inline float get_shininess_4() const { return ___shininess_4; }
	inline float* get_address_of_shininess_4() { return &___shininess_4; }
	inline void set_shininess_4(float value)
	{
		___shininess_4 = value;
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_illumType_6() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___illumType_6)); }
	inline int32_t get_illumType_6() const { return ___illumType_6; }
	inline int32_t* get_address_of_illumType_6() { return &___illumType_6; }
	inline void set_illumType_6(int32_t value)
	{
		___illumType_6 = value;
	}

	inline static int32_t get_offset_of_diffuseTexPath_7() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___diffuseTexPath_7)); }
	inline String_t* get_diffuseTexPath_7() const { return ___diffuseTexPath_7; }
	inline String_t** get_address_of_diffuseTexPath_7() { return &___diffuseTexPath_7; }
	inline void set_diffuseTexPath_7(String_t* value)
	{
		___diffuseTexPath_7 = value;
		Il2CppCodeGenWriteBarrier(&___diffuseTexPath_7, value);
	}

	inline static int32_t get_offset_of_bumpTexPath_8() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___bumpTexPath_8)); }
	inline String_t* get_bumpTexPath_8() const { return ___bumpTexPath_8; }
	inline String_t** get_address_of_bumpTexPath_8() { return &___bumpTexPath_8; }
	inline void set_bumpTexPath_8(String_t* value)
	{
		___bumpTexPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___bumpTexPath_8, value);
	}

	inline static int32_t get_offset_of_diffuseTex_9() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___diffuseTex_9)); }
	inline Texture2D_t3542995729 * get_diffuseTex_9() const { return ___diffuseTex_9; }
	inline Texture2D_t3542995729 ** get_address_of_diffuseTex_9() { return &___diffuseTex_9; }
	inline void set_diffuseTex_9(Texture2D_t3542995729 * value)
	{
		___diffuseTex_9 = value;
		Il2CppCodeGenWriteBarrier(&___diffuseTex_9, value);
	}

	inline static int32_t get_offset_of_bumpTex_10() { return static_cast<int32_t>(offsetof(MaterialData_t3551903345, ___bumpTex_10)); }
	inline Texture2D_t3542995729 * get_bumpTex_10() const { return ___bumpTex_10; }
	inline Texture2D_t3542995729 ** get_address_of_bumpTex_10() { return &___bumpTex_10; }
	inline void set_bumpTex_10(Texture2D_t3542995729 * value)
	{
		___bumpTex_10 = value;
		Il2CppCodeGenWriteBarrier(&___bumpTex_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
