﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NodeParameters
struct  NodeParameters_t819515452  : public Il2CppObject
{
public:
	// System.Double NodeParameters::position
	double ___position_0;
	// System.Double NodeParameters::length
	double ___length_1;
	// Spline NodeParameters::spline
	Spline_t1260612603 * ___spline_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(NodeParameters_t819515452, ___position_0)); }
	inline double get_position_0() const { return ___position_0; }
	inline double* get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(double value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(NodeParameters_t819515452, ___length_1)); }
	inline double get_length_1() const { return ___length_1; }
	inline double* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(double value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(NodeParameters_t819515452, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
