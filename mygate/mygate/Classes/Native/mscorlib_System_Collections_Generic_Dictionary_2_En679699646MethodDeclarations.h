﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m221704236(__this, ___dictionary0, method) ((  void (*) (Enumerator_t679699646 *, Dictionary_2_t3654642240 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3794574633(__this, method) ((  Il2CppObject * (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1579460153(__this, method) ((  void (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3047764726(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2809790063(__this, method) ((  Il2CppObject * (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3545830607(__this, method) ((  Il2CppObject * (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::MoveNext()
#define Enumerator_MoveNext_m2999501705(__this, method) ((  bool (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::get_Current()
#define Enumerator_get_Current_m1605361077(__this, method) ((  KeyValuePair_2_t1411987462  (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3681840556(__this, method) ((  Spline_t1260612603 * (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1135587564(__this, method) ((  NodeParameters_t819515452 * (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::Reset()
#define Enumerator_Reset_m911878542(__this, method) ((  void (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::VerifyState()
#define Enumerator_VerifyState_m1006442959(__this, method) ((  void (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m654554281(__this, method) ((  void (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Spline,NodeParameters>::Dispose()
#define Enumerator_Dispose_m1622445268(__this, method) ((  void (*) (Enumerator_t679699646 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
