﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellGrid[]
struct CellGridU5BU5D_t3919062097;
// Item
struct Item_t2440468191;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// gridLayer
struct gridLayer_t4027367017;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gridLayer/<fillGrid>c__Iterator0
struct  U3CfillGridU3Ec__Iterator0_t2219342100  : public Il2CppObject
{
public:
	// CellGrid[] gridLayer/<fillGrid>c__Iterator0::<cells>__0
	CellGridU5BU5D_t3919062097* ___U3CcellsU3E__0_0;
	// Item gridLayer/<fillGrid>c__Iterator0::itemSelected
	Item_t2440468191 * ___itemSelected_1;
	// System.Action`1<System.Boolean> gridLayer/<fillGrid>c__Iterator0::success
	Action_1_t3627374100 * ___success_2;
	// gridLayer gridLayer/<fillGrid>c__Iterator0::$this
	gridLayer_t4027367017 * ___U24this_3;
	// System.Object gridLayer/<fillGrid>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean gridLayer/<fillGrid>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 gridLayer/<fillGrid>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcellsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___U3CcellsU3E__0_0)); }
	inline CellGridU5BU5D_t3919062097* get_U3CcellsU3E__0_0() const { return ___U3CcellsU3E__0_0; }
	inline CellGridU5BU5D_t3919062097** get_address_of_U3CcellsU3E__0_0() { return &___U3CcellsU3E__0_0; }
	inline void set_U3CcellsU3E__0_0(CellGridU5BU5D_t3919062097* value)
	{
		___U3CcellsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcellsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_itemSelected_1() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___itemSelected_1)); }
	inline Item_t2440468191 * get_itemSelected_1() const { return ___itemSelected_1; }
	inline Item_t2440468191 ** get_address_of_itemSelected_1() { return &___itemSelected_1; }
	inline void set_itemSelected_1(Item_t2440468191 * value)
	{
		___itemSelected_1 = value;
		Il2CppCodeGenWriteBarrier(&___itemSelected_1, value);
	}

	inline static int32_t get_offset_of_success_2() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___success_2)); }
	inline Action_1_t3627374100 * get_success_2() const { return ___success_2; }
	inline Action_1_t3627374100 ** get_address_of_success_2() { return &___success_2; }
	inline void set_success_2(Action_1_t3627374100 * value)
	{
		___success_2 = value;
		Il2CppCodeGenWriteBarrier(&___success_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___U24this_3)); }
	inline gridLayer_t4027367017 * get_U24this_3() const { return ___U24this_3; }
	inline gridLayer_t4027367017 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(gridLayer_t4027367017 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CfillGridU3Ec__Iterator0_t2219342100, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
