﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<getGatefromJSON>c__AnonStorey7
struct U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<getGatefromJSON>c__AnonStorey7::.ctor()
extern "C"  void U3CgetGatefromJSONU3Ec__AnonStorey7__ctor_m2283927643 (U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<getGatefromJSON>c__AnonStorey7::<>m__0(System.Boolean)
extern "C"  void U3CgetGatefromJSONU3Ec__AnonStorey7_U3CU3Em__0_m306028339 (U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556 * __this, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
