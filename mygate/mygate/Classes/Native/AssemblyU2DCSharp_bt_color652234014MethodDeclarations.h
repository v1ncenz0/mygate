﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_color
struct bt_color_t652234014;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void bt_color::.ctor()
extern "C"  void bt_color__ctor_m330231723 (bt_color_t652234014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_color::setColor()
extern "C"  void bt_color_setColor_m3733734574 (bt_color_t652234014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_color::setColorLayer()
extern "C"  void bt_color_setColorLayer_m2182225507 (bt_color_t652234014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_color::setColorFrame()
extern "C"  void bt_color_setColorFrame_m799895429 (bt_color_t652234014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_color::setColorBackground(UnityEngine.Color)
extern "C"  void bt_color_setColorBackground_m2052062726 (bt_color_t652234014 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_color::setImage(UnityEngine.Texture)
extern "C"  void bt_color_setImage_m1931566848 (bt_color_t652234014 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
