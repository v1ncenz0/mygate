﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Item
struct Item_t2440468191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellGrid
struct  CellGrid_t1919063536  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 CellGrid::coordInMatrix
	Vector3_t2243707580  ___coordInMatrix_2;
	// System.Boolean CellGrid::used
	bool ___used_3;
	// UnityEngine.GameObject CellGrid::usedBy
	GameObject_t1756533147 * ___usedBy_4;
	// UnityEngine.Vector3 CellGrid::drawCoordinates
	Vector3_t2243707580  ___drawCoordinates_5;
	// UnityEngine.Vector3 CellGrid::toZeroCoordinates
	Vector3_t2243707580  ___toZeroCoordinates_6;
	// UnityEngine.Vector3 CellGrid::dimension
	Vector3_t2243707580  ___dimension_7;
	// System.Boolean CellGrid::toCut
	bool ___toCut_8;
	// Item CellGrid::item
	Item_t2440468191 * ___item_9;
	// System.Boolean CellGrid::isHat
	bool ___isHat_10;
	// System.Boolean CellGrid::overHat
	bool ___overHat_11;

public:
	inline static int32_t get_offset_of_coordInMatrix_2() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___coordInMatrix_2)); }
	inline Vector3_t2243707580  get_coordInMatrix_2() const { return ___coordInMatrix_2; }
	inline Vector3_t2243707580 * get_address_of_coordInMatrix_2() { return &___coordInMatrix_2; }
	inline void set_coordInMatrix_2(Vector3_t2243707580  value)
	{
		___coordInMatrix_2 = value;
	}

	inline static int32_t get_offset_of_used_3() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___used_3)); }
	inline bool get_used_3() const { return ___used_3; }
	inline bool* get_address_of_used_3() { return &___used_3; }
	inline void set_used_3(bool value)
	{
		___used_3 = value;
	}

	inline static int32_t get_offset_of_usedBy_4() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___usedBy_4)); }
	inline GameObject_t1756533147 * get_usedBy_4() const { return ___usedBy_4; }
	inline GameObject_t1756533147 ** get_address_of_usedBy_4() { return &___usedBy_4; }
	inline void set_usedBy_4(GameObject_t1756533147 * value)
	{
		___usedBy_4 = value;
		Il2CppCodeGenWriteBarrier(&___usedBy_4, value);
	}

	inline static int32_t get_offset_of_drawCoordinates_5() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___drawCoordinates_5)); }
	inline Vector3_t2243707580  get_drawCoordinates_5() const { return ___drawCoordinates_5; }
	inline Vector3_t2243707580 * get_address_of_drawCoordinates_5() { return &___drawCoordinates_5; }
	inline void set_drawCoordinates_5(Vector3_t2243707580  value)
	{
		___drawCoordinates_5 = value;
	}

	inline static int32_t get_offset_of_toZeroCoordinates_6() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___toZeroCoordinates_6)); }
	inline Vector3_t2243707580  get_toZeroCoordinates_6() const { return ___toZeroCoordinates_6; }
	inline Vector3_t2243707580 * get_address_of_toZeroCoordinates_6() { return &___toZeroCoordinates_6; }
	inline void set_toZeroCoordinates_6(Vector3_t2243707580  value)
	{
		___toZeroCoordinates_6 = value;
	}

	inline static int32_t get_offset_of_dimension_7() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___dimension_7)); }
	inline Vector3_t2243707580  get_dimension_7() const { return ___dimension_7; }
	inline Vector3_t2243707580 * get_address_of_dimension_7() { return &___dimension_7; }
	inline void set_dimension_7(Vector3_t2243707580  value)
	{
		___dimension_7 = value;
	}

	inline static int32_t get_offset_of_toCut_8() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___toCut_8)); }
	inline bool get_toCut_8() const { return ___toCut_8; }
	inline bool* get_address_of_toCut_8() { return &___toCut_8; }
	inline void set_toCut_8(bool value)
	{
		___toCut_8 = value;
	}

	inline static int32_t get_offset_of_item_9() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___item_9)); }
	inline Item_t2440468191 * get_item_9() const { return ___item_9; }
	inline Item_t2440468191 ** get_address_of_item_9() { return &___item_9; }
	inline void set_item_9(Item_t2440468191 * value)
	{
		___item_9 = value;
		Il2CppCodeGenWriteBarrier(&___item_9, value);
	}

	inline static int32_t get_offset_of_isHat_10() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___isHat_10)); }
	inline bool get_isHat_10() const { return ___isHat_10; }
	inline bool* get_address_of_isHat_10() { return &___isHat_10; }
	inline void set_isHat_10(bool value)
	{
		___isHat_10 = value;
	}

	inline static int32_t get_offset_of_overHat_11() { return static_cast<int32_t>(offsetof(CellGrid_t1919063536, ___overHat_11)); }
	inline bool get_overHat_11() const { return ___overHat_11; }
	inline bool* get_address_of_overHat_11() { return &___overHat_11; }
	inline void set_overHat_11(bool value)
	{
		___overHat_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
