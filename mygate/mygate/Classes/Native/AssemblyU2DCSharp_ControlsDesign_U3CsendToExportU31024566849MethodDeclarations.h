﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<sendToExport>c__Iterator2
struct U3CsendToExportU3Ec__Iterator2_t1024566849;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<sendToExport>c__Iterator2::.ctor()
extern "C"  void U3CsendToExportU3Ec__Iterator2__ctor_m2068155772 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign/<sendToExport>c__Iterator2::MoveNext()
extern "C"  bool U3CsendToExportU3Ec__Iterator2_MoveNext_m3777212740 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<sendToExport>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CsendToExportU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m194899506 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<sendToExport>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CsendToExportU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3651527690 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<sendToExport>c__Iterator2::Dispose()
extern "C"  void U3CsendToExportU3Ec__Iterator2_Dispose_m4179411107 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<sendToExport>c__Iterator2::Reset()
extern "C"  void U3CsendToExportU3Ec__Iterator2_Reset_m3611704193 (U3CsendToExportU3Ec__Iterator2_t1024566849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
