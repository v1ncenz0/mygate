﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<addCell>c__AnonStorey5
struct  U3CaddCellU3Ec__AnonStorey5_t681471284  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> ControlsDesign/<addCell>c__AnonStorey5::added
	Action_1_t3627374100 * ___added_0;
	// System.Boolean ControlsDesign/<addCell>c__AnonStorey5::showWaiting
	bool ___showWaiting_1;

public:
	inline static int32_t get_offset_of_added_0() { return static_cast<int32_t>(offsetof(U3CaddCellU3Ec__AnonStorey5_t681471284, ___added_0)); }
	inline Action_1_t3627374100 * get_added_0() const { return ___added_0; }
	inline Action_1_t3627374100 ** get_address_of_added_0() { return &___added_0; }
	inline void set_added_0(Action_1_t3627374100 * value)
	{
		___added_0 = value;
		Il2CppCodeGenWriteBarrier(&___added_0, value);
	}

	inline static int32_t get_offset_of_showWaiting_1() { return static_cast<int32_t>(offsetof(U3CaddCellU3Ec__AnonStorey5_t681471284, ___showWaiting_1)); }
	inline bool get_showWaiting_1() const { return ___showWaiting_1; }
	inline bool* get_address_of_showWaiting_1() { return &___showWaiting_1; }
	inline void set_showWaiting_1(bool value)
	{
		___showWaiting_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
