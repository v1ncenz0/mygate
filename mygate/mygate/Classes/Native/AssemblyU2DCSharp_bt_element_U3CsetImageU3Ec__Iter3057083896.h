﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// bt_element
struct bt_element_t1221534273;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_element/<setImage>c__Iterator0
struct  U3CsetImageU3Ec__Iterator0_t3057083896  : public Il2CppObject
{
public:
	// System.String bt_element/<setImage>c__Iterator0::url_image
	String_t* ___url_image_0;
	// UnityEngine.WWW bt_element/<setImage>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// UnityEngine.Texture2D bt_element/<setImage>c__Iterator0::<img>__1
	Texture2D_t3542995729 * ___U3CimgU3E__1_2;
	// bt_element bt_element/<setImage>c__Iterator0::$this
	bt_element_t1221534273 * ___U24this_3;
	// System.Object bt_element/<setImage>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean bt_element/<setImage>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 bt_element/<setImage>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_image_0() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___url_image_0)); }
	inline String_t* get_url_image_0() const { return ___url_image_0; }
	inline String_t** get_address_of_url_image_0() { return &___url_image_0; }
	inline void set_url_image_0(String_t* value)
	{
		___url_image_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_image_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CimgU3E__1_2() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U3CimgU3E__1_2)); }
	inline Texture2D_t3542995729 * get_U3CimgU3E__1_2() const { return ___U3CimgU3E__1_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimgU3E__1_2() { return &___U3CimgU3E__1_2; }
	inline void set_U3CimgU3E__1_2(Texture2D_t3542995729 * value)
	{
		___U3CimgU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimgU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U24this_3)); }
	inline bt_element_t1221534273 * get_U24this_3() const { return ___U24this_3; }
	inline bt_element_t1221534273 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(bt_element_t1221534273 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CsetImageU3Ec__Iterator0_t3057083896, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
