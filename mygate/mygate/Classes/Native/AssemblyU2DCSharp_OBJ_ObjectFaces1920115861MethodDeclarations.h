﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/ObjectFaces
struct ObjectFaces_t1920115861;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void OBJ/ObjectFaces::.ctor()
extern "C"  void ObjectFaces__ctor_m3015728212 (ObjectFaces_t1920115861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/ObjectFaces::.ctor(System.String)
extern "C"  void ObjectFaces__ctor_m2610641582 (ObjectFaces_t1920115861 * __this, String_t* ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
