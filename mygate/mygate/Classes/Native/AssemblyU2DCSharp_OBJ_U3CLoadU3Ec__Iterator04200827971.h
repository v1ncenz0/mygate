﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// OBJ
struct OBJ_t3241282509;
// System.Object
struct Il2CppObject;
// OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3
struct U3CLoadU3Ec__AnonStorey3_t2764713728;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/<Load>c__Iterator0
struct  U3CLoadU3Ec__Iterator0_t4200827971  : public Il2CppObject
{
public:
	// System.String OBJ/<Load>c__Iterator0::path
	String_t* ___path_0;
	// UnityEngine.WWW OBJ/<Load>c__Iterator0::<loader>__0
	WWW_t2919945039 * ___U3CloaderU3E__0_1;
	// System.String[] OBJ/<Load>c__Iterator0::<lines>__1
	StringU5BU5D_t1642385972* ___U3ClinesU3E__1_2;
	// System.Action`1<System.Boolean> OBJ/<Load>c__Iterator0::is_finish
	Action_1_t3627374100 * ___is_finish_3;
	// OBJ OBJ/<Load>c__Iterator0::$this
	OBJ_t3241282509 * ___U24this_4;
	// System.Object OBJ/<Load>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean OBJ/<Load>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 OBJ/<Load>c__Iterator0::$PC
	int32_t ___U24PC_7;
	// OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3 OBJ/<Load>c__Iterator0::$locvar0
	U3CLoadU3Ec__AnonStorey3_t2764713728 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_U3CloaderU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U3CloaderU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CloaderU3E__0_1() const { return ___U3CloaderU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CloaderU3E__0_1() { return &___U3CloaderU3E__0_1; }
	inline void set_U3CloaderU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CloaderU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloaderU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3ClinesU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U3ClinesU3E__1_2)); }
	inline StringU5BU5D_t1642385972* get_U3ClinesU3E__1_2() const { return ___U3ClinesU3E__1_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3ClinesU3E__1_2() { return &___U3ClinesU3E__1_2; }
	inline void set_U3ClinesU3E__1_2(StringU5BU5D_t1642385972* value)
	{
		___U3ClinesU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClinesU3E__1_2, value);
	}

	inline static int32_t get_offset_of_is_finish_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___is_finish_3)); }
	inline Action_1_t3627374100 * get_is_finish_3() const { return ___is_finish_3; }
	inline Action_1_t3627374100 ** get_address_of_is_finish_3() { return &___is_finish_3; }
	inline void set_is_finish_3(Action_1_t3627374100 * value)
	{
		___is_finish_3 = value;
		Il2CppCodeGenWriteBarrier(&___is_finish_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U24this_4)); }
	inline OBJ_t3241282509 * get_U24this_4() const { return ___U24this_4; }
	inline OBJ_t3241282509 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(OBJ_t3241282509 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t4200827971, ___U24locvar0_8)); }
	inline U3CLoadU3Ec__AnonStorey3_t2764713728 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CLoadU3Ec__AnonStorey3_t2764713728 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CLoadU3Ec__AnonStorey3_t2764713728 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
