﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Spline>
struct List_1_t629733735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BranchingSpline
struct  BranchingSpline_t3375729607  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Spline> BranchingSpline::splines
	List_1_t629733735 * ___splines_2;
	// System.Int32 BranchingSpline::recoursionCounter
	int32_t ___recoursionCounter_3;

public:
	inline static int32_t get_offset_of_splines_2() { return static_cast<int32_t>(offsetof(BranchingSpline_t3375729607, ___splines_2)); }
	inline List_1_t629733735 * get_splines_2() const { return ___splines_2; }
	inline List_1_t629733735 ** get_address_of_splines_2() { return &___splines_2; }
	inline void set_splines_2(List_1_t629733735 * value)
	{
		___splines_2 = value;
		Il2CppCodeGenWriteBarrier(&___splines_2, value);
	}

	inline static int32_t get_offset_of_recoursionCounter_3() { return static_cast<int32_t>(offsetof(BranchingSpline_t3375729607, ___recoursionCounter_3)); }
	inline int32_t get_recoursionCounter_3() const { return ___recoursionCounter_3; }
	inline int32_t* get_address_of_recoursionCounter_3() { return &___recoursionCounter_3; }
	inline void set_recoursionCounter_3(int32_t value)
	{
		___recoursionCounter_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
