﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceIndices
struct  FaceIndices_t174955414 
{
public:
	// System.Int32 FaceIndices::vi
	int32_t ___vi_0;
	// System.Int32 FaceIndices::vu
	int32_t ___vu_1;
	// System.Int32 FaceIndices::vn
	int32_t ___vn_2;

public:
	inline static int32_t get_offset_of_vi_0() { return static_cast<int32_t>(offsetof(FaceIndices_t174955414, ___vi_0)); }
	inline int32_t get_vi_0() const { return ___vi_0; }
	inline int32_t* get_address_of_vi_0() { return &___vi_0; }
	inline void set_vi_0(int32_t value)
	{
		___vi_0 = value;
	}

	inline static int32_t get_offset_of_vu_1() { return static_cast<int32_t>(offsetof(FaceIndices_t174955414, ___vu_1)); }
	inline int32_t get_vu_1() const { return ___vu_1; }
	inline int32_t* get_address_of_vu_1() { return &___vu_1; }
	inline void set_vu_1(int32_t value)
	{
		___vu_1 = value;
	}

	inline static int32_t get_offset_of_vn_2() { return static_cast<int32_t>(offsetof(FaceIndices_t174955414, ___vn_2)); }
	inline int32_t get_vn_2() const { return ___vn_2; }
	inline int32_t* get_address_of_vn_2() { return &___vn_2; }
	inline void set_vn_2(int32_t value)
	{
		___vn_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
