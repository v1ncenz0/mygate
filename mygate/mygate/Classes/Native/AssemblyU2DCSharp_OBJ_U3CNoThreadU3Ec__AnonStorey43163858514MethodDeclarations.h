﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/<NoThread>c__AnonStorey4
struct U3CNoThreadU3Ec__AnonStorey4_t3163858514;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/<NoThread>c__AnonStorey4::.ctor()
extern "C"  void U3CNoThreadU3Ec__AnonStorey4__ctor_m2204276749 (U3CNoThreadU3Ec__AnonStorey4_t3163858514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<NoThread>c__AnonStorey4::<>m__0(System.Boolean)
extern "C"  void U3CNoThreadU3Ec__AnonStorey4_U3CU3Em__0_m1032812173 (U3CNoThreadU3Ec__AnonStorey4_t3163858514 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
