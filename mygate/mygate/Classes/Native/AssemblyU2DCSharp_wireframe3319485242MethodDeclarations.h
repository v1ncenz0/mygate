﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// wireframe
struct wireframe_t3319485242;

#include "codegen/il2cpp-codegen.h"

// System.Void wireframe::.ctor()
extern "C"  void wireframe__ctor_m3928774319 (wireframe_t3319485242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wireframe::Start()
extern "C"  void wireframe_Start_m2665801515 (wireframe_t3319485242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wireframe::SetActive()
extern "C"  void wireframe_SetActive_m3060823753 (wireframe_t3319485242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wireframe::SetDeactive()
extern "C"  void wireframe_SetDeactive_m2027806204 (wireframe_t3319485242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
