﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_login
struct bt_login_t335186348;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_login::.ctor()
extern "C"  void bt_login__ctor_m1129279677 (bt_login_t335186348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_login::login()
extern "C"  void bt_login_login_m2362080554 (bt_login_t335186348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_login::<login>m__0(System.Boolean)
extern "C"  void bt_login_U3CloginU3Em__0_m4030042144 (bt_login_t335186348 * __this, bool ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
