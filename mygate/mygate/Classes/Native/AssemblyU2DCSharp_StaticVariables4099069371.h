﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ItemGate
struct ItemGate_t2548129788;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticVariables
struct  StaticVariables_t4099069371  : public Il2CppObject
{
public:

public:
};

struct StaticVariables_t4099069371_StaticFields
{
public:
	// System.String StaticVariables::BASE_URL
	String_t* ___BASE_URL_0;
	// System.String StaticVariables::source
	String_t* ___source_1;
	// System.String StaticVariables::username
	String_t* ___username_2;
	// System.String StaticVariables::password
	String_t* ___password_3;
	// System.Int32 StaticVariables::id_user
	int32_t ___id_user_4;
	// ItemGate StaticVariables::gate
	ItemGate_t2548129788 * ___gate_5;

public:
	inline static int32_t get_offset_of_BASE_URL_0() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___BASE_URL_0)); }
	inline String_t* get_BASE_URL_0() const { return ___BASE_URL_0; }
	inline String_t** get_address_of_BASE_URL_0() { return &___BASE_URL_0; }
	inline void set_BASE_URL_0(String_t* value)
	{
		___BASE_URL_0 = value;
		Il2CppCodeGenWriteBarrier(&___BASE_URL_0, value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_username_2() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___username_2)); }
	inline String_t* get_username_2() const { return ___username_2; }
	inline String_t** get_address_of_username_2() { return &___username_2; }
	inline void set_username_2(String_t* value)
	{
		___username_2 = value;
		Il2CppCodeGenWriteBarrier(&___username_2, value);
	}

	inline static int32_t get_offset_of_password_3() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___password_3)); }
	inline String_t* get_password_3() const { return ___password_3; }
	inline String_t** get_address_of_password_3() { return &___password_3; }
	inline void set_password_3(String_t* value)
	{
		___password_3 = value;
		Il2CppCodeGenWriteBarrier(&___password_3, value);
	}

	inline static int32_t get_offset_of_id_user_4() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___id_user_4)); }
	inline int32_t get_id_user_4() const { return ___id_user_4; }
	inline int32_t* get_address_of_id_user_4() { return &___id_user_4; }
	inline void set_id_user_4(int32_t value)
	{
		___id_user_4 = value;
	}

	inline static int32_t get_offset_of_gate_5() { return static_cast<int32_t>(offsetof(StaticVariables_t4099069371_StaticFields, ___gate_5)); }
	inline ItemGate_t2548129788 * get_gate_5() const { return ___gate_5; }
	inline ItemGate_t2548129788 ** get_address_of_gate_5() { return &___gate_5; }
	inline void set_gate_5(ItemGate_t2548129788 * value)
	{
		___gate_5 = value;
		Il2CppCodeGenWriteBarrier(&___gate_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
