﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice
struct  webservice_t1109616683  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 webservice::colorDownloaded
	int32_t ___colorDownloaded_14;

public:
	inline static int32_t get_offset_of_colorDownloaded_14() { return static_cast<int32_t>(offsetof(webservice_t1109616683, ___colorDownloaded_14)); }
	inline int32_t get_colorDownloaded_14() const { return ___colorDownloaded_14; }
	inline int32_t* get_address_of_colorDownloaded_14() { return &___colorDownloaded_14; }
	inline void set_colorDownloaded_14(int32_t value)
	{
		___colorDownloaded_14 = value;
	}
};

struct webservice_t1109616683_StaticFields
{
public:
	// System.Boolean webservice::isDownloaded
	bool ___isDownloaded_13;

public:
	inline static int32_t get_offset_of_isDownloaded_13() { return static_cast<int32_t>(offsetof(webservice_t1109616683_StaticFields, ___isDownloaded_13)); }
	inline bool get_isDownloaded_13() const { return ___isDownloaded_13; }
	inline bool* get_address_of_isDownloaded_13() { return &___isDownloaded_13; }
	inline void set_isDownloaded_13(bool value)
	{
		___isDownloaded_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
