﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_scroll
struct bt_scroll_t132828814;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_scroll::.ctor()
extern "C"  void bt_scroll__ctor_m4021811495 (bt_scroll_t132828814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scroll::scroll()
extern "C"  void bt_scroll_scroll_m2686067272 (bt_scroll_t132828814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
