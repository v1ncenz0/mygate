﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityBuiltins1429886615.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_SplineAnimator2697441160.h"
#include "AssemblyU2DCSharp_SplineAnimatorClosestPoint4003263689.h"
#include "AssemblyU2DCSharp_SplineAnimatorCustomValue4274418512.h"
#include "AssemblyU2DCSharp_SplineAutoAlign1803875161.h"
#include "AssemblyU2DCSharp_SplineMeshModifierExample1909698667.h"
#include "AssemblyU2DCSharp_SplineGravitySimulator873170475.h"
#include "AssemblyU2DCSharp_BranchingSpline3375729607.h"
#include "AssemblyU2DCSharp_BranchingSpline_BranchingControl3837965438.h"
#include "AssemblyU2DCSharp_BranchingSplinePath1574214842.h"
#include "AssemblyU2DCSharp_BranchingSplinePath_Direction1768093872.h"
#include "AssemblyU2DCSharp_BranchingSplineParameter1831757700.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "AssemblyU2DCSharp_Spline_SegmentParameter3125294186.h"
#include "AssemblyU2DCSharp_Spline_TangentMode4224395836.h"
#include "AssemblyU2DCSharp_Spline_NormalMode4231820426.h"
#include "AssemblyU2DCSharp_Spline_RotationMode3093810083.h"
#include "AssemblyU2DCSharp_Spline_InterpolationMode2569420497.h"
#include "AssemblyU2DCSharp_Spline_UpdateMode1661020696.h"
#include "AssemblyU2DCSharp_Spline_DistanceFunction3700963585.h"
#include "AssemblyU2DCSharp_Spline_LengthData3176416758.h"
#include "AssemblyU2DCSharp_Spline_U3CGetClosestPointParamU31228364937.h"
#include "AssemblyU2DCSharp_Spline_U3CGetClosestPointParamTo1916003713.h"
#include "AssemblyU2DCSharp_Spline_U3CGetClosestPointParamToP873487574.h"
#include "AssemblyU2DCSharp_SplineInterpolator4279526764.h"
#include "AssemblyU2DCSharp_HermiteInterpolator3138331103.h"
#include "AssemblyU2DCSharp_BezierInterpolator2292175410.h"
#include "AssemblyU2DCSharp_BSplineInterpolator1920804950.h"
#include "AssemblyU2DCSharp_LinearInterpolator4145168420.h"
#include "AssemblyU2DCSharp_SplineSegment1083783318.h"
#include "AssemblyU2DCSharp_SplineCurveScaleModifier1538173695.h"
#include "AssemblyU2DCSharp_SplineSineScaleModifier862254749.h"
#include "AssemblyU2DCSharp_SplineTwistModifier3543911261.h"
#include "AssemblyU2DCSharp_SplineMesh1719246168.h"
#include "AssemblyU2DCSharp_SplineMesh_MeshData1171048948.h"
#include "AssemblyU2DCSharp_SplineMesh_UVMode511492427.h"
#include "AssemblyU2DCSharp_SplineMesh_SplitMode1084303880.h"
#include "AssemblyU2DCSharp_SplineMesh_UpdateMode201935517.h"
#include "AssemblyU2DCSharp_SplineMeshModifier3237667319.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"
#include "AssemblyU2DCSharp_SplineNode_NodeParameterRegister3732266052.h"
#include "AssemblyU2DCSharp_NodeParameters819515452.h"
#include "AssemblyU2DCSharp_QuaternionUtilities_QuaternionUtil89730617.h"
#include "AssemblyU2DCSharp_Ctrl4006552079.h"
#include "AssemblyU2DCSharp_GUIManager2551693622.h"
#include "AssemblyU2DCSharp_ScrollSnapRect671131207.h"
#include "AssemblyU2DCSharp_bt_scroll132828814.h"
#include "AssemblyU2DCSharp_bt_scroll_ModeList2967208204.h"
#include "AssemblyU2DCSharp_bt_scroll_TypeList1536841447.h"
#include "AssemblyU2DCSharp_changeScroll2128272085.h"
#include "AssemblyU2DCSharp_loadingPanel1472697062.h"
#include "AssemblyU2DCSharp_param_confirmbox505336669.h"
#include "AssemblyU2DCSharp_tooltip3401351055.h"
#include "AssemblyU2DCSharp_wireframe3319485242.h"
#include "AssemblyU2DCSharp_toolbar3757582107.h"
#include "AssemblyU2DCSharp_bt_color652234014.h"
#include "AssemblyU2DCSharp_ControlsDesign1600104368.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CsetItemSelecte1954323636.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CaddCellU3Ec__An681471284.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CThreadaddCellU1030518217.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CThreadSaveGate3479513232.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CsendValueToDBU2061549659.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CgetGatefromJSO4022664556.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CdownloadElemen2465517567.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CsendToExportU31024566849.h"
#include "AssemblyU2DCSharp_ControlsDesign_U3CcreateOBJU3Ec_3024815716.h"
#include "AssemblyU2DCSharp_CursorElement1656362794.h"
#include "AssemblyU2DCSharp_CursorElement_ModeList3850132356.h"
#include "AssemblyU2DCSharp_CursorElement_U3CAddandMoveToU3E3043472538.h"
#include "AssemblyU2DCSharp_CursorEraser4121456910.h"
#include "AssemblyU2DCSharp_CellGrid1919063536.h"
#include "AssemblyU2DCSharp_GridManager4027472533.h"
#include "AssemblyU2DCSharp_GridManager_onInitFinish901190412.h"
#include "AssemblyU2DCSharp_gridLayer4027367017.h"
#include "AssemblyU2DCSharp_gridLayer_U3CfillGridU3Ec__Itera2219342100.h"
#include "AssemblyU2DCSharp_SetGatePosition2001167784.h"
#include "AssemblyU2DCSharp_init_main2270456518.h"
#include "AssemblyU2DCSharp_init_main_U3CStartU3Ec__AnonStor3487247048.h"
#include "AssemblyU2DCSharp_bt_buy2549161545.h"
#include "AssemblyU2DCSharp_bt_continue_download4085380521.h"
#include "AssemblyU2DCSharp_bt_detail551551266.h"
#include "AssemblyU2DCSharp_bt_mygate3681173754.h"
#include "AssemblyU2DCSharp_bt_new3792772879.h"
#include "AssemblyU2DCSharp_bt_return_login1162689017.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1711[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Array_t1396575355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (UnityBuiltins_t1429886615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (SplineAnimator_t2697441160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[5] = 
{
	SplineAnimator_t2697441160::get_offset_of_spline_2(),
	SplineAnimator_t2697441160::get_offset_of_wrapMode_3(),
	SplineAnimator_t2697441160::get_offset_of_speed_4(),
	SplineAnimator_t2697441160::get_offset_of_offSet_5(),
	SplineAnimator_t2697441160::get_offset_of_passedTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (SplineAnimatorClosestPoint_t4003263689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[6] = 
{
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_spline_2(),
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_wMode_3(),
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_target_4(),
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_iterations_5(),
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_diff_6(),
	SplineAnimatorClosestPoint_t4003263689::get_offset_of_offset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (SplineAnimatorCustomValue_t4274418512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[5] = 
{
	SplineAnimatorCustomValue_t4274418512::get_offset_of_spline_2(),
	SplineAnimatorCustomValue_t4274418512::get_offset_of_wrapMode_3(),
	SplineAnimatorCustomValue_t4274418512::get_offset_of_speed_4(),
	SplineAnimatorCustomValue_t4274418512::get_offset_of_offSet_5(),
	SplineAnimatorCustomValue_t4274418512::get_offset_of_passedTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (SplineAutoAlign_t1803875161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[4] = 
{
	SplineAutoAlign_t1803875161::get_offset_of_raycastLayers_2(),
	SplineAutoAlign_t1803875161::get_offset_of_offset_3(),
	SplineAutoAlign_t1803875161::get_offset_of_ignoreTags_4(),
	SplineAutoAlign_t1803875161::get_offset_of_raycastDirection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (SplineMeshModifierExample_t1909698667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (SplineGravitySimulator_t873170475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[3] = 
{
	SplineGravitySimulator_t873170475::get_offset_of_spline_2(),
	SplineGravitySimulator_t873170475::get_offset_of_gravityConstant_3(),
	SplineGravitySimulator_t873170475::get_offset_of_iterations_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (BranchingSpline_t3375729607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	BranchingSpline_t3375729607::get_offset_of_splines_2(),
	BranchingSpline_t3375729607::get_offset_of_recoursionCounter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (BranchingController_t3837965438), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (BranchingSplinePath_t1574214842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[2] = 
{
	BranchingSplinePath_t1574214842::get_offset_of_spline_0(),
	BranchingSplinePath_t1574214842::get_offset_of_direction_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (Direction_t1768093872)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1726[3] = 
{
	Direction_t1768093872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (BranchingSplineParameter_t1831757700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	BranchingSplineParameter_t1831757700::get_offset_of_parameter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (Spline_t1260612603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[18] = 
{
	Spline_t1260612603::get_offset_of_splineNodesArray_2(),
	Spline_t1260612603::get_offset_of_splineNodesInternal_3(),
	Spline_t1260612603::get_offset_of_interpolationMode_4(),
	Spline_t1260612603::get_offset_of_rotationMode_5(),
	Spline_t1260612603::get_offset_of_tangentMode_6(),
	Spline_t1260612603::get_offset_of_normalMode_7(),
	Spline_t1260612603::get_offset_of_updateMode_8(),
	Spline_t1260612603::get_offset_of_deltaFrames_9(),
	Spline_t1260612603::get_offset_of_deltaTime_10(),
	Spline_t1260612603::get_offset_of_updateFrame_11(),
	Spline_t1260612603::get_offset_of_updateTime_12(),
	Spline_t1260612603::get_offset_of_perNodeTension_13(),
	Spline_t1260612603::get_offset_of_tension_14(),
	Spline_t1260612603::get_offset_of_normal_15(),
	Spline_t1260612603::get_offset_of_autoClose_16(),
	Spline_t1260612603::get_offset_of_interpolationAccuracy_17(),
	Spline_t1260612603::get_offset_of_lengthData_18(),
	Spline_t1260612603::get_offset_of_splineInterpolator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (SegmentParameter_t3125294186)+ sizeof (Il2CppObject), sizeof(SegmentParameter_t3125294186 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	SegmentParameter_t3125294186::get_offset_of_normalizedParam_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SegmentParameter_t3125294186::get_offset_of_normalizedIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (TangentMode_t4224395836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1730[4] = 
{
	TangentMode_t4224395836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (NormalMode_t4231820426)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	NormalMode_t4231820426::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (RotationMode_t3093810083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1732[4] = 
{
	RotationMode_t3093810083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (InterpolationMode_t2569420497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1733[6] = 
{
	InterpolationMode_t2569420497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (UpdateMode_t1661020696)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1734[5] = 
{
	UpdateMode_t1661020696::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (DistanceFunction_t3700963585), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (LengthData_t3176416758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[3] = 
{
	LengthData_t3176416758::get_offset_of_subSegmentLength_0(),
	LengthData_t3176416758::get_offset_of_subSegmentPosition_1(),
	LengthData_t3176416758::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[1] = 
{
	U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937::get_offset_of_point_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[1] = 
{
	U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713::get_offset_of_ray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574::get_offset_of_plane_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (SplineInterpolator_t4279526764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[2] = 
{
	SplineInterpolator_t4279526764::get_offset_of_coefficientMatrix_0(),
	SplineInterpolator_t4279526764::get_offset_of_nodeIndices_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (HermiteInterpolator_t3138331103), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (BezierInterpolator_t2292175410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (BSplineInterpolator_t1920804950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (LinearInterpolator_t4145168420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (SplineSegment_t1083783318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[3] = 
{
	SplineSegment_t1083783318::get_offset_of_parentSpline_0(),
	SplineSegment_t1083783318::get_offset_of_startNode_1(),
	SplineSegment_t1083783318::get_offset_of_endNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (SplineCurveScaleModifier_t1538173695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[1] = 
{
	SplineCurveScaleModifier_t1538173695::get_offset_of_scaleCurve_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (SplineSineScaleModifier_t862254749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[4] = 
{
	SplineSineScaleModifier_t862254749::get_offset_of_frequency_2(),
	SplineSineScaleModifier_t862254749::get_offset_of_offset_3(),
	SplineSineScaleModifier_t862254749::get_offset_of_sinMultiplicator_4(),
	SplineSineScaleModifier_t862254749::get_offset_of_sinOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (SplineTwistModifier_t3543911261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[3] = 
{
	SplineTwistModifier_t3543911261::get_offset_of_twistCount_2(),
	SplineTwistModifier_t3543911261::get_offset_of_twistOffset_3(),
	SplineTwistModifier_t3543911261::get_offset_of_rotationQuaternion_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (SplineMesh_t1719246168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[23] = 
{
	SplineMesh_t1719246168::get_offset_of_spline_2(),
	SplineMesh_t1719246168::get_offset_of_updateMode_3(),
	SplineMesh_t1719246168::get_offset_of_deltaFrames_4(),
	SplineMesh_t1719246168::get_offset_of_deltaTime_5(),
	SplineMesh_t1719246168::get_offset_of_updateFrame_6(),
	SplineMesh_t1719246168::get_offset_of_updateTime_7(),
	SplineMesh_t1719246168::get_offset_of_startBaseMesh_8(),
	SplineMesh_t1719246168::get_offset_of_baseMesh_9(),
	SplineMesh_t1719246168::get_offset_of_endBaseMesh_10(),
	SplineMesh_t1719246168::get_offset_of_segmentCount_11(),
	SplineMesh_t1719246168::get_offset_of_uvMode_12(),
	SplineMesh_t1719246168::get_offset_of_uvScale_13(),
	SplineMesh_t1719246168::get_offset_of_xyScale_14(),
	SplineMesh_t1719246168::get_offset_of_highAccuracy_15(),
	SplineMesh_t1719246168::get_offset_of_splitMode_16(),
	SplineMesh_t1719246168::get_offset_of_segmentStart_17(),
	SplineMesh_t1719246168::get_offset_of_segmentEnd_18(),
	SplineMesh_t1719246168::get_offset_of_splineSegment_19(),
	SplineMesh_t1719246168::get_offset_of_meshDataStart_20(),
	SplineMesh_t1719246168::get_offset_of_meshDataBase_21(),
	SplineMesh_t1719246168::get_offset_of_meshDataEnd_22(),
	SplineMesh_t1719246168::get_offset_of_meshDataNew_23(),
	SplineMesh_t1719246168::get_offset_of_bentMesh_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (MeshData_t1171048948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[11] = 
{
	MeshData_t1171048948::get_offset_of_vertices_0(),
	MeshData_t1171048948::get_offset_of_uvCoord_1(),
	MeshData_t1171048948::get_offset_of_normals_2(),
	MeshData_t1171048948::get_offset_of_tangents_3(),
	MeshData_t1171048948::get_offset_of_triangles_4(),
	MeshData_t1171048948::get_offset_of_bounds_5(),
	MeshData_t1171048948::get_offset_of_currentTriangleIndex_6(),
	MeshData_t1171048948::get_offset_of_currentVertexIndex_7(),
	MeshData_t1171048948::get_offset_of_HasNormals_8(),
	MeshData_t1171048948::get_offset_of_HasTangents_9(),
	MeshData_t1171048948::get_offset_of_referencedMesh_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (UVMode_t511492427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1751[4] = 
{
	UVMode_t511492427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (SplitMode_t1084303880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1752[4] = 
{
	SplitMode_t1084303880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (UpdateMode_t201935517)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1753[6] = 
{
	UpdateMode_t201935517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (SplineMeshModifier_t3237667319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (SplineNode_t3003005095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[4] = 
{
	SplineNode_t3003005095::get_offset_of_customValue_2(),
	SplineNode_t3003005095::get_offset_of_tension_3(),
	SplineNode_t3003005095::get_offset_of_normal_4(),
	SplineNode_t3003005095::get_offset_of_parameters_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (NodeParameterRegister_t3732266052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (NodeParameters_t819515452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[3] = 
{
	NodeParameters_t819515452::get_offset_of_position_0(),
	NodeParameters_t819515452::get_offset_of_length_1(),
	NodeParameters_t819515452::get_offset_of_spline_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (QuaternionUtils_t89730617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (Ctrl_t4006552079), -1, sizeof(Ctrl_t4006552079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	Ctrl_t4006552079_StaticFields::get_offset_of_inDeleteMode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (GUIManager_t2551693622), -1, sizeof(GUIManager_t2551693622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1760[16] = 
{
	GUIManager_t2551693622_StaticFields::get_offset_of_cameracontrols_2(),
	GUIManager_t2551693622_StaticFields::get_offset_of_library_3(),
	GUIManager_t2551693622_StaticFields::get_offset_of_layermanager_4(),
	GUIManager_t2551693622_StaticFields::get_offset_of_colorlibrary_5(),
	GUIManager_t2551693622_StaticFields::get_offset_of_toolbar_6(),
	GUIManager_t2551693622_StaticFields::get_offset_of_statusbar_7(),
	GUIManager_t2551693622_StaticFields::get_offset_of_library_scenes_8(),
	GUIManager_t2551693622_StaticFields::get_offset_of_help_9(),
	GUIManager_t2551693622_StaticFields::get_offset_of_confirmBox_10(),
	GUIManager_t2551693622_StaticFields::get_offset_of_confirmBoxObjectName_11(),
	GUIManager_t2551693622_StaticFields::get_offset_of_confirmBoxComponentName_12(),
	GUIManager_t2551693622_StaticFields::get_offset_of_confirmBoxCommandName_13(),
	GUIManager_t2551693622_StaticFields::get_offset_of_dialogBox_14(),
	GUIManager_t2551693622_StaticFields::get_offset_of_waitingBox_15(),
	GUIManager_t2551693622_StaticFields::get_offset_of_loadingPanel_16(),
	GUIManager_t2551693622_StaticFields::get_offset_of_detailBox_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (ScrollSnapRect_t671131207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[25] = 
{
	ScrollSnapRect_t671131207::get_offset_of_startingPage_2(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdTime_3(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdDistance_4(),
	ScrollSnapRect_t671131207::get_offset_of_decelerationRate_5(),
	ScrollSnapRect_t671131207::get_offset_of_prevButton_6(),
	ScrollSnapRect_t671131207::get_offset_of_nextButton_7(),
	ScrollSnapRect_t671131207::get_offset_of_unselectedPage_8(),
	ScrollSnapRect_t671131207::get_offset_of_selectedPage_9(),
	ScrollSnapRect_t671131207::get_offset_of_pageSelectionIcons_10(),
	ScrollSnapRect_t671131207::get_offset_of__fastSwipeThresholdMaxLimit_11(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectComponent_12(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectRect_13(),
	ScrollSnapRect_t671131207::get_offset_of__container_14(),
	ScrollSnapRect_t671131207::get_offset_of__horizontal_15(),
	ScrollSnapRect_t671131207::get_offset_of__pageCount_16(),
	ScrollSnapRect_t671131207::get_offset_of__currentPage_17(),
	ScrollSnapRect_t671131207::get_offset_of__lerp_18(),
	ScrollSnapRect_t671131207::get_offset_of__lerpTo_19(),
	ScrollSnapRect_t671131207::get_offset_of__pagePositions_20(),
	ScrollSnapRect_t671131207::get_offset_of__dragging_21(),
	ScrollSnapRect_t671131207::get_offset_of__timeStamp_22(),
	ScrollSnapRect_t671131207::get_offset_of__startPosition_23(),
	ScrollSnapRect_t671131207::get_offset_of__showPageSelection_24(),
	ScrollSnapRect_t671131207::get_offset_of__previousPageSelectionIndex_25(),
	ScrollSnapRect_t671131207::get_offset_of__pageSelectionImages_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (bt_scroll_t132828814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[4] = 
{
	bt_scroll_t132828814::get_offset_of_scrollview_2(),
	bt_scroll_t132828814::get_offset_of_delta_3(),
	bt_scroll_t132828814::get_offset_of_mode_4(),
	bt_scroll_t132828814::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (ModeList_t2967208204)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1763[3] = 
{
	ModeList_t2967208204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (TypeList_t1536841447)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1764[3] = 
{
	TypeList_t1536841447::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (changeScroll_t2128272085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[1] = 
{
	changeScroll_t2128272085::get_offset_of_textObj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (loadingPanel_t1472697062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[1] = 
{
	loadingPanel_t1472697062::get_offset_of_bt_back_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (param_confirmbox_t505336669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[2] = 
{
	param_confirmbox_t505336669::get_offset_of_commandName_2(),
	param_confirmbox_t505336669::get_offset_of_objectName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (tooltip_t3401351055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (wireframe_t3319485242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[1] = 
{
	wireframe_t3319485242::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (toolbar_t3757582107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (bt_color_t652234014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[3] = 
{
	bt_color_t652234014::get_offset_of_colorItem_2(),
	bt_color_t652234014::get_offset_of_bg_color_3(),
	bt_color_t652234014::get_offset_of_bg_image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (ControlsDesign_t1600104368), -1, sizeof(ControlsDesign_t1600104368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1772[12] = 
{
	ControlsDesign_t1600104368::get_offset_of_currentActive_2(),
	ControlsDesign_t1600104368::get_offset_of_cg_3(),
	ControlsDesign_t1600104368::get_offset_of_itemSelected_4(),
	ControlsDesign_t1600104368::get_offset_of_moveCamera_5(),
	ControlsDesign_t1600104368::get_offset_of_cursorObject_6(),
	ControlsDesign_t1600104368::get_offset_of_cursorDeleteMode_7(),
	ControlsDesign_t1600104368::get_offset_of_addspecularcell_8(),
	ControlsDesign_t1600104368::get_offset_of_is_scene_rendering_9(),
	ControlsDesign_t1600104368::get_offset_of_price_10(),
	ControlsDesign_t1600104368::get_offset_of_is_screencapture_fase_11(),
	ControlsDesign_t1600104368_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	ControlsDesign_t1600104368_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[2] = 
{
	U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636::get_offset_of_item_0(),
	U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (U3CaddCellU3Ec__AnonStorey5_t681471284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	U3CaddCellU3Ec__AnonStorey5_t681471284::get_offset_of_added_0(),
	U3CaddCellU3Ec__AnonStorey5_t681471284::get_offset_of_showWaiting_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (U3CThreadaddCellU3Ec__Iterator0_t1030518217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[7] = 
{
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_item_0(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_U3CposU3E__0_1(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_added_2(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_U24this_3(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_U24current_4(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_U24disposing_5(),
	U3CThreadaddCellU3Ec__Iterator0_t1030518217::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[2] = 
{
	U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232::get_offset_of_saved_0(),
	U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232::get_offset_of_error_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CsendValueToDBU3Ec__Iterator1_t2061549659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[14] = 
{
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3CformU3E__0_0(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_withoutScreen_1(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3CscreenU3E__1_2(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_json_3(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3Cjson_byteU3E__2_4(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3CrawDataU3E__3_5(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3CurlU3E__4_6(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U3CwwwU3E__5_7(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_saved_8(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_error_9(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U24this_10(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U24current_11(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U24disposing_12(),
	U3CsendValueToDBU3Ec__Iterator1_t2061549659::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[4] = 
{
	U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556::get_offset_of_items_0(),
	U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556::get_offset_of_elements_1(),
	U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556::get_offset_of_success_2(),
	U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (U3CdownloadElementsU3Ec__AnonStorey8_t2465517567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[5] = 
{
	U3CdownloadElementsU3Ec__AnonStorey8_t2465517567::get_offset_of_item_0(),
	U3CdownloadElementsU3Ec__AnonStorey8_t2465517567::get_offset_of_items_1(),
	U3CdownloadElementsU3Ec__AnonStorey8_t2465517567::get_offset_of_index_2(),
	U3CdownloadElementsU3Ec__AnonStorey8_t2465517567::get_offset_of_finish_3(),
	U3CdownloadElementsU3Ec__AnonStorey8_t2465517567::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (U3CsendToExportU3Ec__Iterator2_t1024566849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[10] = 
{
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U3CformU3E__0_0(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_objitem_1(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U3Cobj_byteU3E__1_2(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U3CrawDataU3E__2_3(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U3CurlU3E__3_4(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U3CwwwU3E__4_5(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U24this_6(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U24current_7(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U24disposing_8(),
	U3CsendToExportU3Ec__Iterator2_t1024566849::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (U3CcreateOBJU3Ec__Iterator3_t3024815716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[7] = 
{
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U3CgateU3E__0_0(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U3CobjStringU3E__1_1(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_finish_2(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U24this_3(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U24current_4(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U24disposing_5(),
	U3CcreateOBJU3Ec__Iterator3_t3024815716::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (CursorElement_t1656362794), -1, sizeof(CursorElement_t1656362794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1782[16] = 
{
	CursorElement_t1656362794_StaticFields::get_offset_of_dimCellGridTouched_2(),
	CursorElement_t1656362794_StaticFields::get_offset_of_elementDim_3(),
	CursorElement_t1656362794::get_offset_of_mode_4(),
	CursorElement_t1656362794_StaticFields::get_offset_of_currentItem_5(),
	CursorElement_t1656362794_StaticFields::get_offset_of_currentCellToCut_6(),
	CursorElement_t1656362794::get_offset_of_isInit_7(),
	CursorElement_t1656362794::get_offset_of_isMouseMove_8(),
	CursorElement_t1656362794::get_offset_of_isTouch_9(),
	CursorElement_t1656362794::get_offset_of_firstPositionMouse_10(),
	CursorElement_t1656362794::get_offset_of_initialPositionCursor_11(),
	CursorElement_t1656362794::get_offset_of_isDragged_12(),
	CursorElement_t1656362794::get_offset_of_oldPosition_13(),
	CursorElement_t1656362794::get_offset_of_clicked_14(),
	CursorElement_t1656362794::get_offset_of_clicktime_15(),
	CursorElement_t1656362794::get_offset_of_clickdelay_16(),
	CursorElement_t1656362794_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ModeList_t3850132356)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1783[12] = 
{
	ModeList_t3850132356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (U3CAddandMoveToU3Ec__AnonStorey0_t3043472538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[2] = 
{
	U3CAddandMoveToU3Ec__AnonStorey0_t3043472538::get_offset_of_direction_0(),
	U3CAddandMoveToU3Ec__AnonStorey0_t3043472538::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (CursorEraser_t4121456910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (CellGrid_t1919063536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[10] = 
{
	CellGrid_t1919063536::get_offset_of_coordInMatrix_2(),
	CellGrid_t1919063536::get_offset_of_used_3(),
	CellGrid_t1919063536::get_offset_of_usedBy_4(),
	CellGrid_t1919063536::get_offset_of_drawCoordinates_5(),
	CellGrid_t1919063536::get_offset_of_toZeroCoordinates_6(),
	CellGrid_t1919063536::get_offset_of_dimension_7(),
	CellGrid_t1919063536::get_offset_of_toCut_8(),
	CellGrid_t1919063536::get_offset_of_item_9(),
	CellGrid_t1919063536::get_offset_of_isHat_10(),
	CellGrid_t1919063536::get_offset_of_overHat_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (GridManager_t4027472533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[11] = 
{
	0,
	0,
	GridManager_t4027472533::get_offset_of_mat_frame_4(),
	GridManager_t4027472533::get_offset_of_colorGrids_5(),
	GridManager_t4027472533::get_offset_of_layers_6(),
	GridManager_t4027472533::get_offset_of_gate_7(),
	GridManager_t4027472533::get_offset_of_root_go_8(),
	GridManager_t4027472533::get_offset_of_frame_go_9(),
	GridManager_t4027472533::get_offset_of_layer_go_10(),
	GridManager_t4027472533::get_offset_of_spline_11(),
	GridManager_t4027472533::get_offset_of_oninitfinish_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (onInitFinish_t901190412), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (gridLayer_t4027367017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[31] = 
{
	0,
	gridLayer_t4027367017::get_offset_of_gate_3(),
	gridLayer_t4027367017::get_offset_of_maxOffsetDim_4(),
	gridLayer_t4027367017::get_offset_of_actualPosition_5(),
	gridLayer_t4027367017::get_offset_of_actualPositionRight_6(),
	gridLayer_t4027367017::get_offset_of_hatStartingPoint_7(),
	gridLayer_t4027367017::get_offset_of_divisorGO_8(),
	gridLayer_t4027367017::get_offset_of_divisor_offset_9(),
	gridLayer_t4027367017::get_offset_of_hasDivFrame_10(),
	gridLayer_t4027367017::get_offset_of_Z_11(),
	gridLayer_t4027367017::get_offset_of_startingPoint_12(),
	gridLayer_t4027367017::get_offset_of_startingPointRight_13(),
	gridLayer_t4027367017::get_offset_of_isActive_14(),
	gridLayer_t4027367017::get_offset_of_colorgrid_15(),
	gridLayer_t4027367017::get_offset_of_matrix_16(),
	gridLayer_t4027367017::get_offset_of_numLayer_17(),
	gridLayer_t4027367017::get_offset_of_colorlayer_18(),
	gridLayer_t4027367017::get_offset_of_is_gridshow_19(),
	gridLayer_t4027367017::get_offset_of_hatSpline_20(),
	gridLayer_t4027367017::get_offset_of_gridView_21(),
	gridLayer_t4027367017::get_offset_of_gridViewElements_22(),
	gridLayer_t4027367017::get_offset_of_gridViewGridElements_23(),
	gridLayer_t4027367017::get_offset_of_gridViewRight_24(),
	gridLayer_t4027367017::get_offset_of_gridViewElementsRight_25(),
	gridLayer_t4027367017::get_offset_of_gridViewGridElementsRight_26(),
	gridLayer_t4027367017::get_offset_of_gridViewHat_27(),
	gridLayer_t4027367017::get_offset_of_gridViewHatElements_28(),
	gridLayer_t4027367017::get_offset_of_gridViewHatGridElements_29(),
	gridLayer_t4027367017::get_offset_of_gridViewHatRight_30(),
	gridLayer_t4027367017::get_offset_of_gridViewHatElementsRight_31(),
	gridLayer_t4027367017::get_offset_of_gridViewHatGridElementsRight_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (U3CfillGridU3Ec__Iterator0_t2219342100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[7] = 
{
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_U3CcellsU3E__0_0(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_itemSelected_1(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_success_2(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_U24this_3(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_U24current_4(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_U24disposing_5(),
	U3CfillGridU3Ec__Iterator0_t2219342100::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (SetGatePosition_t2001167784), -1, sizeof(SetGatePosition_t2001167784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1791[11] = 
{
	SetGatePosition_t2001167784_StaticFields::get_offset_of_camera_position_2(),
	SetGatePosition_t2001167784_StaticFields::get_offset_of_camera_rotation_3(),
	SetGatePosition_t2001167784::get_offset_of_lamp_cad_4(),
	SetGatePosition_t2001167784::get_offset_of_lamp_render_5(),
	SetGatePosition_t2001167784::get_offset_of_column_right_6(),
	SetGatePosition_t2001167784::get_offset_of_column_left_7(),
	SetGatePosition_t2001167784::get_offset_of_wall_right_8(),
	SetGatePosition_t2001167784::get_offset_of_wall_left_9(),
	SetGatePosition_t2001167784::get_offset_of_pavimento_10(),
	SetGatePosition_t2001167784::get_offset_of_sfondo_11(),
	SetGatePosition_t2001167784::get_offset_of_sfondo_giardino_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (init_main_t2270456518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (U3CStartU3Ec__AnonStorey0_t3487247048), -1, sizeof(U3CStartU3Ec__AnonStorey0_t3487247048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	U3CStartU3Ec__AnonStorey0_t3487247048::get_offset_of_wb_0(),
	U3CStartU3Ec__AnonStorey0_t3487247048::get_offset_of_U24this_1(),
	U3CStartU3Ec__AnonStorey0_t3487247048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (bt_buy_t2549161545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[1] = 
{
	bt_buy_t2549161545::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (bt_continue_download_t4085380521), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (bt_detail_t551551266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	bt_detail_t551551266::get_offset_of_gate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (bt_mygate_t3681173754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	bt_mygate_t3681173754::get_offset_of_gate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (bt_new_t3792772879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (bt_return_login_t1162689017), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
