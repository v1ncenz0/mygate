﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ
struct OBJ_t3241282509;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// OBJ/MaterialData
struct MaterialData_t3551903345;
// System.String[]
struct StringU5BU5D_t1642385972;
// FaceIndices[]
struct FaceIndicesU5BU5D_t2696217939;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OBJ_MaterialData3551903345.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void OBJ::.ctor()
extern "C"  void OBJ__ctor_m953133084 (OBJ_t3241282509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::Start()
extern "C"  void OBJ_Start_m3954378196 (OBJ_t3241282509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::Update()
extern "C"  void OBJ_Update_m3769210929 (OBJ_t3241282509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::StartLoadCoroutine(System.Action`1<System.Boolean>)
extern "C"  void OBJ_StartLoadCoroutine_m1399635413 (OBJ_t3241282509 * __this, Action_1_t3627374100 * ___is_finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OBJ::Load(System.String,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * OBJ_Load_m1850466763 (OBJ_t3241282509 * __this, String_t* ___path0, Action_1_t3627374100 * ___is_finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OBJ::AfterFileParsing()
extern "C"  Il2CppObject * OBJ_AfterFileParsing_m807220446 (OBJ_t3241282509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW OBJ::GetTextureLoader(OBJ/MaterialData,System.String)
extern "C"  WWW_t2919945039 * OBJ_GetTextureLoader_m2184169802 (OBJ_t3241282509 * __this, MaterialData_t3551903345 * ___m0, String_t* ___texpath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::CreateThread(System.String[])
extern "C"  void OBJ_CreateThread_m165454036 (OBJ_t3241282509 * __this, StringU5BU5D_t1642385972* ___fileLines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::NoThread(System.String[],System.Action`1<System.Boolean>)
extern "C"  void OBJ_NoThread_m952044404 (OBJ_t3241282509 * __this, StringU5BU5D_t1642385972* ___fileLines0, Action_1_t3627374100 * ___is_finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::ReadObjData(System.Action`1<System.Boolean>)
extern "C"  void OBJ_ReadObjData_m490913632 (OBJ_t3241282509 * __this, Action_1_t3627374100 * ___is_finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::GetFaceIndicesByOneFaceLine(FaceIndices[],System.String[],System.Boolean)
extern "C"  void OBJ_GetFaceIndicesByOneFaceLine_m2790562327 (OBJ_t3241282509 * __this, FaceIndicesU5BU5D_t2696217939* ___faces0, StringU5BU5D_t1642385972* ___p1, bool ___isFaceIndexPlus2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::ElementsCount(System.String)
extern "C"  void OBJ_ElementsCount_m3124116970 (OBJ_t3241282509 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::SetGeometryData(System.String[])
extern "C"  void OBJ_SetGeometryData_m29613802 (OBJ_t3241282509 * __this, StringU5BU5D_t1642385972* ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OBJ::cf(System.String)
extern "C"  float OBJ_cf_m2656479489 (OBJ_t3241282509 * __this, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OBJ::ci(System.String)
extern "C"  int32_t OBJ_ci_m2356191302 (OBJ_t3241282509 * __this, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OBJ::get_hasMaterials()
extern "C"  bool OBJ_get_hasMaterials_m3054809367 (OBJ_t3241282509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::SetMaterialData(System.String)
extern "C"  void OBJ_SetMaterialData_m3656112267 (OBJ_t3241282509 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material OBJ::GetMaterial(OBJ/MaterialData)
extern "C"  Material_t193706927 * OBJ_GetMaterial_m3262691333 (OBJ_t3241282509 * __this, MaterialData_t3551903345 * ___md0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::BumpParameter(OBJ/MaterialData,System.String[])
extern "C"  void OBJ_BumpParameter_m4110330738 (OBJ_t3241282509 * __this, MaterialData_t3551903345 * ___m0, StringU5BU5D_t1642385972* ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color OBJ::gc(System.String[])
extern "C"  Color_t2020392075  OBJ_gc_m4225199761 (OBJ_t3241282509 * __this, StringU5BU5D_t1642385972* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ::Build(System.Action`1<System.Boolean>)
extern "C"  void OBJ_Build_m3746516471 (OBJ_t3241282509 * __this, Action_1_t3627374100 * ___is_finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
