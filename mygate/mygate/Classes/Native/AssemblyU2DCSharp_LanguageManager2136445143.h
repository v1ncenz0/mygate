﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageManager
struct  LanguageManager_t2136445143  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct LanguageManager_t2136445143_StaticFields
{
public:
	// System.String LanguageManager::HELP_WELCOME_TITLE
	String_t* ___HELP_WELCOME_TITLE_2;
	// System.String LanguageManager::HELP_WELCOME
	String_t* ___HELP_WELCOME_3;
	// System.String LanguageManager::HELP_EDIT_TITLE
	String_t* ___HELP_EDIT_TITLE_4;
	// System.String LanguageManager::HELP_EDIT
	String_t* ___HELP_EDIT_5;
	// System.String LanguageManager::HELP_DELETE_TITLE
	String_t* ___HELP_DELETE_TITLE_6;
	// System.String LanguageManager::HELP_DELETE
	String_t* ___HELP_DELETE_7;
	// System.String LanguageManager::HELP_FILL_TITLE
	String_t* ___HELP_FILL_TITLE_8;
	// System.String LanguageManager::HELP_FILL
	String_t* ___HELP_FILL_9;
	// System.String LanguageManager::HELP_ERASEGRID_TITLE
	String_t* ___HELP_ERASEGRID_TITLE_10;
	// System.String LanguageManager::HELP_ERASEGRID
	String_t* ___HELP_ERASEGRID_11;
	// System.String LanguageManager::HELP_LAYER_TITLE
	String_t* ___HELP_LAYER_TITLE_12;
	// System.String LanguageManager::HELP_LAYERS
	String_t* ___HELP_LAYERS_13;
	// System.String LanguageManager::HELP_LIBRARY_TITLE
	String_t* ___HELP_LIBRARY_TITLE_14;
	// System.String LanguageManager::HELP_LIBRARY
	String_t* ___HELP_LIBRARY_15;
	// System.String LanguageManager::MESSAGE_SAVE
	String_t* ___MESSAGE_SAVE_16;
	// System.String LanguageManager::BT_OK_CHECKOUT
	String_t* ___BT_OK_CHECKOUT_17;
	// System.String LanguageManager::BT_CONTINUE_DRAWING
	String_t* ___BT_CONTINUE_DRAWING_18;
	// System.String LanguageManager::MESSAGE_ALERT_BEFORE_CLOSE
	String_t* ___MESSAGE_ALERT_BEFORE_CLOSE_19;
	// System.String LanguageManager::BT_OK
	String_t* ___BT_OK_20;
	// System.String LanguageManager::BT_QUIT
	String_t* ___BT_QUIT_21;

public:
	inline static int32_t get_offset_of_HELP_WELCOME_TITLE_2() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_WELCOME_TITLE_2)); }
	inline String_t* get_HELP_WELCOME_TITLE_2() const { return ___HELP_WELCOME_TITLE_2; }
	inline String_t** get_address_of_HELP_WELCOME_TITLE_2() { return &___HELP_WELCOME_TITLE_2; }
	inline void set_HELP_WELCOME_TITLE_2(String_t* value)
	{
		___HELP_WELCOME_TITLE_2 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_WELCOME_TITLE_2, value);
	}

	inline static int32_t get_offset_of_HELP_WELCOME_3() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_WELCOME_3)); }
	inline String_t* get_HELP_WELCOME_3() const { return ___HELP_WELCOME_3; }
	inline String_t** get_address_of_HELP_WELCOME_3() { return &___HELP_WELCOME_3; }
	inline void set_HELP_WELCOME_3(String_t* value)
	{
		___HELP_WELCOME_3 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_WELCOME_3, value);
	}

	inline static int32_t get_offset_of_HELP_EDIT_TITLE_4() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_EDIT_TITLE_4)); }
	inline String_t* get_HELP_EDIT_TITLE_4() const { return ___HELP_EDIT_TITLE_4; }
	inline String_t** get_address_of_HELP_EDIT_TITLE_4() { return &___HELP_EDIT_TITLE_4; }
	inline void set_HELP_EDIT_TITLE_4(String_t* value)
	{
		___HELP_EDIT_TITLE_4 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_EDIT_TITLE_4, value);
	}

	inline static int32_t get_offset_of_HELP_EDIT_5() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_EDIT_5)); }
	inline String_t* get_HELP_EDIT_5() const { return ___HELP_EDIT_5; }
	inline String_t** get_address_of_HELP_EDIT_5() { return &___HELP_EDIT_5; }
	inline void set_HELP_EDIT_5(String_t* value)
	{
		___HELP_EDIT_5 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_EDIT_5, value);
	}

	inline static int32_t get_offset_of_HELP_DELETE_TITLE_6() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_DELETE_TITLE_6)); }
	inline String_t* get_HELP_DELETE_TITLE_6() const { return ___HELP_DELETE_TITLE_6; }
	inline String_t** get_address_of_HELP_DELETE_TITLE_6() { return &___HELP_DELETE_TITLE_6; }
	inline void set_HELP_DELETE_TITLE_6(String_t* value)
	{
		___HELP_DELETE_TITLE_6 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_DELETE_TITLE_6, value);
	}

	inline static int32_t get_offset_of_HELP_DELETE_7() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_DELETE_7)); }
	inline String_t* get_HELP_DELETE_7() const { return ___HELP_DELETE_7; }
	inline String_t** get_address_of_HELP_DELETE_7() { return &___HELP_DELETE_7; }
	inline void set_HELP_DELETE_7(String_t* value)
	{
		___HELP_DELETE_7 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_DELETE_7, value);
	}

	inline static int32_t get_offset_of_HELP_FILL_TITLE_8() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_FILL_TITLE_8)); }
	inline String_t* get_HELP_FILL_TITLE_8() const { return ___HELP_FILL_TITLE_8; }
	inline String_t** get_address_of_HELP_FILL_TITLE_8() { return &___HELP_FILL_TITLE_8; }
	inline void set_HELP_FILL_TITLE_8(String_t* value)
	{
		___HELP_FILL_TITLE_8 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_FILL_TITLE_8, value);
	}

	inline static int32_t get_offset_of_HELP_FILL_9() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_FILL_9)); }
	inline String_t* get_HELP_FILL_9() const { return ___HELP_FILL_9; }
	inline String_t** get_address_of_HELP_FILL_9() { return &___HELP_FILL_9; }
	inline void set_HELP_FILL_9(String_t* value)
	{
		___HELP_FILL_9 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_FILL_9, value);
	}

	inline static int32_t get_offset_of_HELP_ERASEGRID_TITLE_10() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_ERASEGRID_TITLE_10)); }
	inline String_t* get_HELP_ERASEGRID_TITLE_10() const { return ___HELP_ERASEGRID_TITLE_10; }
	inline String_t** get_address_of_HELP_ERASEGRID_TITLE_10() { return &___HELP_ERASEGRID_TITLE_10; }
	inline void set_HELP_ERASEGRID_TITLE_10(String_t* value)
	{
		___HELP_ERASEGRID_TITLE_10 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_ERASEGRID_TITLE_10, value);
	}

	inline static int32_t get_offset_of_HELP_ERASEGRID_11() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_ERASEGRID_11)); }
	inline String_t* get_HELP_ERASEGRID_11() const { return ___HELP_ERASEGRID_11; }
	inline String_t** get_address_of_HELP_ERASEGRID_11() { return &___HELP_ERASEGRID_11; }
	inline void set_HELP_ERASEGRID_11(String_t* value)
	{
		___HELP_ERASEGRID_11 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_ERASEGRID_11, value);
	}

	inline static int32_t get_offset_of_HELP_LAYER_TITLE_12() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_LAYER_TITLE_12)); }
	inline String_t* get_HELP_LAYER_TITLE_12() const { return ___HELP_LAYER_TITLE_12; }
	inline String_t** get_address_of_HELP_LAYER_TITLE_12() { return &___HELP_LAYER_TITLE_12; }
	inline void set_HELP_LAYER_TITLE_12(String_t* value)
	{
		___HELP_LAYER_TITLE_12 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_LAYER_TITLE_12, value);
	}

	inline static int32_t get_offset_of_HELP_LAYERS_13() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_LAYERS_13)); }
	inline String_t* get_HELP_LAYERS_13() const { return ___HELP_LAYERS_13; }
	inline String_t** get_address_of_HELP_LAYERS_13() { return &___HELP_LAYERS_13; }
	inline void set_HELP_LAYERS_13(String_t* value)
	{
		___HELP_LAYERS_13 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_LAYERS_13, value);
	}

	inline static int32_t get_offset_of_HELP_LIBRARY_TITLE_14() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_LIBRARY_TITLE_14)); }
	inline String_t* get_HELP_LIBRARY_TITLE_14() const { return ___HELP_LIBRARY_TITLE_14; }
	inline String_t** get_address_of_HELP_LIBRARY_TITLE_14() { return &___HELP_LIBRARY_TITLE_14; }
	inline void set_HELP_LIBRARY_TITLE_14(String_t* value)
	{
		___HELP_LIBRARY_TITLE_14 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_LIBRARY_TITLE_14, value);
	}

	inline static int32_t get_offset_of_HELP_LIBRARY_15() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___HELP_LIBRARY_15)); }
	inline String_t* get_HELP_LIBRARY_15() const { return ___HELP_LIBRARY_15; }
	inline String_t** get_address_of_HELP_LIBRARY_15() { return &___HELP_LIBRARY_15; }
	inline void set_HELP_LIBRARY_15(String_t* value)
	{
		___HELP_LIBRARY_15 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_LIBRARY_15, value);
	}

	inline static int32_t get_offset_of_MESSAGE_SAVE_16() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___MESSAGE_SAVE_16)); }
	inline String_t* get_MESSAGE_SAVE_16() const { return ___MESSAGE_SAVE_16; }
	inline String_t** get_address_of_MESSAGE_SAVE_16() { return &___MESSAGE_SAVE_16; }
	inline void set_MESSAGE_SAVE_16(String_t* value)
	{
		___MESSAGE_SAVE_16 = value;
		Il2CppCodeGenWriteBarrier(&___MESSAGE_SAVE_16, value);
	}

	inline static int32_t get_offset_of_BT_OK_CHECKOUT_17() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___BT_OK_CHECKOUT_17)); }
	inline String_t* get_BT_OK_CHECKOUT_17() const { return ___BT_OK_CHECKOUT_17; }
	inline String_t** get_address_of_BT_OK_CHECKOUT_17() { return &___BT_OK_CHECKOUT_17; }
	inline void set_BT_OK_CHECKOUT_17(String_t* value)
	{
		___BT_OK_CHECKOUT_17 = value;
		Il2CppCodeGenWriteBarrier(&___BT_OK_CHECKOUT_17, value);
	}

	inline static int32_t get_offset_of_BT_CONTINUE_DRAWING_18() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___BT_CONTINUE_DRAWING_18)); }
	inline String_t* get_BT_CONTINUE_DRAWING_18() const { return ___BT_CONTINUE_DRAWING_18; }
	inline String_t** get_address_of_BT_CONTINUE_DRAWING_18() { return &___BT_CONTINUE_DRAWING_18; }
	inline void set_BT_CONTINUE_DRAWING_18(String_t* value)
	{
		___BT_CONTINUE_DRAWING_18 = value;
		Il2CppCodeGenWriteBarrier(&___BT_CONTINUE_DRAWING_18, value);
	}

	inline static int32_t get_offset_of_MESSAGE_ALERT_BEFORE_CLOSE_19() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___MESSAGE_ALERT_BEFORE_CLOSE_19)); }
	inline String_t* get_MESSAGE_ALERT_BEFORE_CLOSE_19() const { return ___MESSAGE_ALERT_BEFORE_CLOSE_19; }
	inline String_t** get_address_of_MESSAGE_ALERT_BEFORE_CLOSE_19() { return &___MESSAGE_ALERT_BEFORE_CLOSE_19; }
	inline void set_MESSAGE_ALERT_BEFORE_CLOSE_19(String_t* value)
	{
		___MESSAGE_ALERT_BEFORE_CLOSE_19 = value;
		Il2CppCodeGenWriteBarrier(&___MESSAGE_ALERT_BEFORE_CLOSE_19, value);
	}

	inline static int32_t get_offset_of_BT_OK_20() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___BT_OK_20)); }
	inline String_t* get_BT_OK_20() const { return ___BT_OK_20; }
	inline String_t** get_address_of_BT_OK_20() { return &___BT_OK_20; }
	inline void set_BT_OK_20(String_t* value)
	{
		___BT_OK_20 = value;
		Il2CppCodeGenWriteBarrier(&___BT_OK_20, value);
	}

	inline static int32_t get_offset_of_BT_QUIT_21() { return static_cast<int32_t>(offsetof(LanguageManager_t2136445143_StaticFields, ___BT_QUIT_21)); }
	inline String_t* get_BT_QUIT_21() const { return ___BT_QUIT_21; }
	inline String_t** get_address_of_BT_QUIT_21() { return &___BT_QUIT_21; }
	inline void set_BT_QUIT_21(String_t* value)
	{
		___BT_QUIT_21 = value;
		Il2CppCodeGenWriteBarrier(&___BT_QUIT_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
