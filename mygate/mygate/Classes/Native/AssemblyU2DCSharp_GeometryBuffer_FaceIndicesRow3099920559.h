﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<FaceIndices>
struct List_1_t3839043842;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeometryBuffer/FaceIndicesRow
struct  FaceIndicesRow_t3099920559  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<FaceIndices> GeometryBuffer/FaceIndicesRow::faceIndices
	List_1_t3839043842 * ___faceIndices_0;

public:
	inline static int32_t get_offset_of_faceIndices_0() { return static_cast<int32_t>(offsetof(FaceIndicesRow_t3099920559, ___faceIndices_0)); }
	inline List_1_t3839043842 * get_faceIndices_0() const { return ___faceIndices_0; }
	inline List_1_t3839043842 ** get_address_of_faceIndices_0() { return &___faceIndices_0; }
	inline void set_faceIndices_0(List_1_t3839043842 * value)
	{
		___faceIndices_0 = value;
		Il2CppCodeGenWriteBarrier(&___faceIndices_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
