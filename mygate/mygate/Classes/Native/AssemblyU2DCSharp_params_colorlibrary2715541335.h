﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// params_colorlibrary
struct  params_colorlibrary_t2715541335  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 params_colorlibrary::id_layer
	int32_t ___id_layer_2;
	// System.Boolean params_colorlibrary::is_frame
	bool ___is_frame_3;

public:
	inline static int32_t get_offset_of_id_layer_2() { return static_cast<int32_t>(offsetof(params_colorlibrary_t2715541335, ___id_layer_2)); }
	inline int32_t get_id_layer_2() const { return ___id_layer_2; }
	inline int32_t* get_address_of_id_layer_2() { return &___id_layer_2; }
	inline void set_id_layer_2(int32_t value)
	{
		___id_layer_2 = value;
	}

	inline static int32_t get_offset_of_is_frame_3() { return static_cast<int32_t>(offsetof(params_colorlibrary_t2715541335, ___is_frame_3)); }
	inline bool get_is_frame_3() const { return ___is_frame_3; }
	inline bool* get_address_of_is_frame_3() { return &___is_frame_3; }
	inline void set_is_frame_3(bool value)
	{
		___is_frame_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
