﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline/<GetClosestPointParamToRay>c__AnonStorey1
struct  U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713  : public Il2CppObject
{
public:
	// UnityEngine.Ray Spline/<GetClosestPointParamToRay>c__AnonStorey1::ray
	Ray_t2469606224  ___ray_0;

public:
	inline static int32_t get_offset_of_ray_0() { return static_cast<int32_t>(offsetof(U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713, ___ray_0)); }
	inline Ray_t2469606224  get_ray_0() const { return ___ray_0; }
	inline Ray_t2469606224 * get_address_of_ray_0() { return &___ray_0; }
	inline void set_ray_0(Ray_t2469606224  value)
	{
		___ray_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
