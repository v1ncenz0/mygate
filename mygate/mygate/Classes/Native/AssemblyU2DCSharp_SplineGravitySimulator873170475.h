﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineGravitySimulator
struct  SplineGravitySimulator_t873170475  : public MonoBehaviour_t1158329972
{
public:
	// Spline SplineGravitySimulator::spline
	Spline_t1260612603 * ___spline_2;
	// System.Single SplineGravitySimulator::gravityConstant
	float ___gravityConstant_3;
	// System.Int32 SplineGravitySimulator::iterations
	int32_t ___iterations_4;

public:
	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(SplineGravitySimulator_t873170475, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}

	inline static int32_t get_offset_of_gravityConstant_3() { return static_cast<int32_t>(offsetof(SplineGravitySimulator_t873170475, ___gravityConstant_3)); }
	inline float get_gravityConstant_3() const { return ___gravityConstant_3; }
	inline float* get_address_of_gravityConstant_3() { return &___gravityConstant_3; }
	inline void set_gravityConstant_3(float value)
	{
		___gravityConstant_3 = value;
	}

	inline static int32_t get_offset_of_iterations_4() { return static_cast<int32_t>(offsetof(SplineGravitySimulator_t873170475, ___iterations_4)); }
	inline int32_t get_iterations_4() const { return ___iterations_4; }
	inline int32_t* get_address_of_iterations_4() { return &___iterations_4; }
	inline void set_iterations_4(int32_t value)
	{
		___iterations_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
