﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<UnityEngine.Sprite>
struct Action_1_t111393165;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// init_start/<downloadImage>c__Iterator0
struct  U3CdownloadImageU3Ec__Iterator0_t621683668  : public Il2CppObject
{
public:
	// UnityEngine.Sprite init_start/<downloadImage>c__Iterator0::<image>__0
	Sprite_t309593783 * ___U3CimageU3E__0_0;
	// System.String init_start/<downloadImage>c__Iterator0::image_path
	String_t* ___image_path_1;
	// UnityEngine.WWW init_start/<downloadImage>c__Iterator0::<www>__1
	WWW_t2919945039 * ___U3CwwwU3E__1_2;
	// System.Action`1<UnityEngine.Sprite> init_start/<downloadImage>c__Iterator0::finish
	Action_1_t111393165 * ___finish_3;
	// System.Object init_start/<downloadImage>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean init_start/<downloadImage>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 init_start/<downloadImage>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CimageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___U3CimageU3E__0_0)); }
	inline Sprite_t309593783 * get_U3CimageU3E__0_0() const { return ___U3CimageU3E__0_0; }
	inline Sprite_t309593783 ** get_address_of_U3CimageU3E__0_0() { return &___U3CimageU3E__0_0; }
	inline void set_U3CimageU3E__0_0(Sprite_t309593783 * value)
	{
		___U3CimageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimageU3E__0_0, value);
	}

	inline static int32_t get_offset_of_image_path_1() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___image_path_1)); }
	inline String_t* get_image_path_1() const { return ___image_path_1; }
	inline String_t** get_address_of_image_path_1() { return &___image_path_1; }
	inline void set_image_path_1(String_t* value)
	{
		___image_path_1 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___U3CwwwU3E__1_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__1_2() const { return ___U3CwwwU3E__1_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__1_2() { return &___U3CwwwU3E__1_2; }
	inline void set_U3CwwwU3E__1_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_2, value);
	}

	inline static int32_t get_offset_of_finish_3() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___finish_3)); }
	inline Action_1_t111393165 * get_finish_3() const { return ___finish_3; }
	inline Action_1_t111393165 ** get_address_of_finish_3() { return &___finish_3; }
	inline void set_finish_3(Action_1_t111393165 * value)
	{
		___finish_3 = value;
		Il2CppCodeGenWriteBarrier(&___finish_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t621683668, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
