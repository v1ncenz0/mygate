﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline/<GetClosestPointParamToRay>c__AnonStorey1
struct U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Spline/<GetClosestPointParamToRay>c__AnonStorey1::.ctor()
extern "C"  void U3CGetClosestPointParamToRayU3Ec__AnonStorey1__ctor_m3736930996 (U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline/<GetClosestPointParamToRay>c__AnonStorey1::<>m__0(UnityEngine.Vector3)
extern "C"  float U3CGetClosestPointParamToRayU3Ec__AnonStorey1_U3CU3Em__0_m1791287762 (U3CGetClosestPointParamToRayU3Ec__AnonStorey1_t1916003713 * __this, Vector3_t2243707580  ___splinePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
