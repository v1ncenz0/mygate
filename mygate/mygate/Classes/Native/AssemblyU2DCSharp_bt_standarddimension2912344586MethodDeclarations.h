﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_standarddimension
struct bt_standarddimension_t2912344586;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_standarddimension::.ctor()
extern "C"  void bt_standarddimension__ctor_m4012741321 (bt_standarddimension_t2912344586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_standarddimension::changeWidth()
extern "C"  void bt_standarddimension_changeWidth_m1809889305 (bt_standarddimension_t2912344586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_standarddimension::changeHeight()
extern "C"  void bt_standarddimension_changeHeight_m1076824470 (bt_standarddimension_t2912344586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_standarddimension::updateTextField()
extern "C"  void bt_standarddimension_updateTextField_m2922295443 (bt_standarddimension_t2912344586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
