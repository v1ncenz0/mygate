﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineAnimatorCustomValue
struct  SplineAnimatorCustomValue_t4274418512  : public MonoBehaviour_t1158329972
{
public:
	// Spline SplineAnimatorCustomValue::spline
	Spline_t1260612603 * ___spline_2;
	// UnityEngine.WrapMode SplineAnimatorCustomValue::wrapMode
	int32_t ___wrapMode_3;
	// System.Single SplineAnimatorCustomValue::speed
	float ___speed_4;
	// System.Single SplineAnimatorCustomValue::offSet
	float ___offSet_5;
	// System.Single SplineAnimatorCustomValue::passedTime
	float ___passedTime_6;

public:
	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(SplineAnimatorCustomValue_t4274418512, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}

	inline static int32_t get_offset_of_wrapMode_3() { return static_cast<int32_t>(offsetof(SplineAnimatorCustomValue_t4274418512, ___wrapMode_3)); }
	inline int32_t get_wrapMode_3() const { return ___wrapMode_3; }
	inline int32_t* get_address_of_wrapMode_3() { return &___wrapMode_3; }
	inline void set_wrapMode_3(int32_t value)
	{
		___wrapMode_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(SplineAnimatorCustomValue_t4274418512, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_offSet_5() { return static_cast<int32_t>(offsetof(SplineAnimatorCustomValue_t4274418512, ___offSet_5)); }
	inline float get_offSet_5() const { return ___offSet_5; }
	inline float* get_address_of_offSet_5() { return &___offSet_5; }
	inline void set_offSet_5(float value)
	{
		___offSet_5 = value;
	}

	inline static int32_t get_offset_of_passedTime_6() { return static_cast<int32_t>(offsetof(SplineAnimatorCustomValue_t4274418512, ___passedTime_6)); }
	inline float get_passedTime_6() const { return ___passedTime_6; }
	inline float* get_address_of_passedTime_6() { return &___passedTime_6; }
	inline void set_passedTime_6(float value)
	{
		___passedTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
