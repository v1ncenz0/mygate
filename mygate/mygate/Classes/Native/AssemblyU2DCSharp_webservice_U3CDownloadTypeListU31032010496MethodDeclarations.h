﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<DownloadTypeList>c__Iterator1
struct U3CDownloadTypeListU3Ec__Iterator1_t1032010496;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<DownloadTypeList>c__Iterator1::.ctor()
extern "C"  void U3CDownloadTypeListU3Ec__Iterator1__ctor_m2836048809 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<DownloadTypeList>c__Iterator1::MoveNext()
extern "C"  bool U3CDownloadTypeListU3Ec__Iterator1_MoveNext_m640900031 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadTypeList>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadTypeListU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3041070427 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadTypeList>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadTypeListU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m158961539 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadTypeList>c__Iterator1::Dispose()
extern "C"  void U3CDownloadTypeListU3Ec__Iterator1_Dispose_m3618673002 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadTypeList>c__Iterator1::Reset()
extern "C"  void U3CDownloadTypeListU3Ec__Iterator1_Reset_m2887655084 (U3CDownloadTypeListU3Ec__Iterator1_t1032010496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
