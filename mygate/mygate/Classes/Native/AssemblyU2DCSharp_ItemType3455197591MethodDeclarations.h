﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemType
struct ItemType_t3455197591;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ItemMisure
struct ItemMisure_t2657053000;
// ItemStyle
struct ItemStyle_t3530326308;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemStyle3530326308.h"

// System.Void ItemType::.ctor()
extern "C"  void ItemType__ctor_m1138872586 (ItemType_t3455197591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemType::downloadImage(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemType_downloadImage_m1459399054 (ItemType_t3455197591 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemMisure ItemType::getMisure(System.Int32)
extern "C"  ItemMisure_t2657053000 * ItemType_getMisure_m77942903 (ItemType_t3455197591 * __this, int32_t ___gross_width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemType::CalcolateNetDimension(System.Int32,System.Int32,ItemStyle,System.Int32&,System.Int32&,System.Int32&)
extern "C"  void ItemType_CalcolateNetDimension_m1575897782 (ItemType_t3455197591 * __this, int32_t ___gross_width0, int32_t ___gross_height1, ItemStyle_t3530326308 * ___style2, int32_t* ___net_width3, int32_t* ___net_height4, int32_t* ___net_height_max5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList ItemType::getStandardWidth(System.Int32)
extern "C"  ArrayList_t4252133567 * ItemType_getStandardWidth_m2205772662 (ItemType_t3455197591 * __this, int32_t ___mindim0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList ItemType::getStandardHeight(System.Int32,System.Int32)
extern "C"  ArrayList_t4252133567 * ItemType_getStandardHeight_m296010916 (ItemType_t3455197591 * __this, int32_t ___mindim0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
