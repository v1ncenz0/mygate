﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.Vector3,UnityEngine.Vector3>
struct Transform_1_t1051541262;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3695180410_gshared (Transform_1_t1051541262 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3695180410(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1051541262 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3695180410_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.Vector3,UnityEngine.Vector3>::Invoke(TKey,TValue)
extern "C"  Vector3_t2243707580  Transform_1_Invoke_m737164386_gshared (Transform_1_t1051541262 * __this, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m737164386(__this, ___key0, ___value1, method) ((  Vector3_t2243707580  (*) (Transform_1_t1051541262 *, int32_t, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m737164386_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.Vector3,UnityEngine.Vector3>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3716252971_gshared (Transform_1_t1051541262 * __this, int32_t ___key0, Vector3_t2243707580  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m3716252971(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1051541262 *, int32_t, Vector3_t2243707580 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3716252971_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.Vector3,UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  Transform_1_EndInvoke_m4003285840_gshared (Transform_1_t1051541262 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m4003285840(__this, ___result0, method) ((  Vector3_t2243707580  (*) (Transform_1_t1051541262 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m4003285840_gshared)(__this, ___result0, method)
