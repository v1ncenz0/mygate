﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HermiteInterpolator
struct HermiteInterpolator_t3138331103;
// Spline
struct Spline_t1260612603;
// System.Collections.Generic.IList`1<SplineNode>
struct IList_1_t3543945696;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t2784648181;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t2617450533;
// SplineNode
struct SplineNode_t3003005095;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"

// System.Void HermiteInterpolator::.ctor()
extern "C"  void HermiteInterpolator__ctor_m2160259562 (HermiteInterpolator_t3138331103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HermiteInterpolator::InterpolateVector(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  Vector3_t2243707580  HermiteInterpolator_InterpolateVector_m1882853066 (HermiteInterpolator_t3138331103 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HermiteInterpolator::InterpolateVector(System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<UnityEngine.Vector3>,System.Int32)
extern "C"  Vector3_t2243707580  HermiteInterpolator_InterpolateVector_m709721761 (HermiteInterpolator_t3138331103 * __this, double ___t0, int32_t ___index1, bool ___autoClose2, Il2CppObject* ___nodes3, int32_t ___derivationOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HermiteInterpolator::InterpolateValue(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  float HermiteInterpolator_InterpolateValue_m1894191940 (HermiteInterpolator_t3138331103 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HermiteInterpolator::InterpolateValue(System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<System.Single>,System.Int32)
extern "C"  float HermiteInterpolator_InterpolateValue_m256514123 (HermiteInterpolator_t3138331103 * __this, double ___t0, int32_t ___index1, bool ___autoClose2, Il2CppObject* ___nodes3, int32_t ___derivationOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HermiteInterpolator::InterpolateRotation(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  Quaternion_t4030073918  HermiteInterpolator_InterpolateRotation_m756473707 (HermiteInterpolator_t3138331103 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HermiteInterpolator::RecalcVectors(Spline,SplineNode,SplineNode,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void HermiteInterpolator_RecalcVectors_m4083790735 (HermiteInterpolator_t3138331103 * __this, Spline_t1260612603 * ___spline0, SplineNode_t3003005095 * ___node01, SplineNode_t3003005095 * ___node12, Vector3_t2243707580 * ___P23, Vector3_t2243707580 * ___P34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HermiteInterpolator::RecalcVectors(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void HermiteInterpolator_RecalcVectors_m917820074 (HermiteInterpolator_t3138331103 * __this, Vector3_t2243707580  ___P00, Vector3_t2243707580  ___P11, Vector3_t2243707580 * ___P22, Vector3_t2243707580 * ___P33, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HermiteInterpolator::RecalcScalars(Spline,SplineNode,SplineNode,System.Single&,System.Single&)
extern "C"  void HermiteInterpolator_RecalcScalars_m257846508 (HermiteInterpolator_t3138331103 * __this, Spline_t1260612603 * ___spline0, SplineNode_t3003005095 * ___node01, SplineNode_t3003005095 * ___node12, float* ___P23, float* ___P34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HermiteInterpolator::RecalcScalars(System.Single,System.Single,System.Single&,System.Single&)
extern "C"  void HermiteInterpolator_RecalcScalars_m3987019505 (HermiteInterpolator_t3138331103 * __this, float ___P00, float ___P11, float* ___P22, float* ___P33, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HermiteInterpolator::RecalcRotations(UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void HermiteInterpolator_RecalcRotations_m4135489219 (HermiteInterpolator_t3138331103 * __this, Quaternion_t4030073918  ___Q00, Quaternion_t4030073918  ___Q11, Quaternion_t4030073918 * ___Q22, Quaternion_t4030073918 * ___Q33, const MethodInfo* method) IL2CPP_METHOD_ATTR;
