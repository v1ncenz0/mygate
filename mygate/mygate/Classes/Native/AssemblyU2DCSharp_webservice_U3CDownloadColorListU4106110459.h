﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// webservice
struct webservice_t1109616683;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<DownloadColorList>c__Iterator3
struct  U3CDownloadColorListU3Ec__Iterator3_t4106110459  : public Il2CppObject
{
public:
	// UnityEngine.WWW webservice/<DownloadColorList>c__Iterator3::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// System.Collections.ArrayList webservice/<DownloadColorList>c__Iterator3::<colors>__1
	ArrayList_t4252133567 * ___U3CcolorsU3E__1_1;
	// System.Action`1<System.Boolean> webservice/<DownloadColorList>c__Iterator3::finish
	Action_1_t3627374100 * ___finish_2;
	// webservice webservice/<DownloadColorList>c__Iterator3::$this
	webservice_t1109616683 * ___U24this_3;
	// System.Object webservice/<DownloadColorList>c__Iterator3::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean webservice/<DownloadColorList>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 webservice/<DownloadColorList>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CcolorsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U3CcolorsU3E__1_1)); }
	inline ArrayList_t4252133567 * get_U3CcolorsU3E__1_1() const { return ___U3CcolorsU3E__1_1; }
	inline ArrayList_t4252133567 ** get_address_of_U3CcolorsU3E__1_1() { return &___U3CcolorsU3E__1_1; }
	inline void set_U3CcolorsU3E__1_1(ArrayList_t4252133567 * value)
	{
		___U3CcolorsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcolorsU3E__1_1, value);
	}

	inline static int32_t get_offset_of_finish_2() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___finish_2)); }
	inline Action_1_t3627374100 * get_finish_2() const { return ___finish_2; }
	inline Action_1_t3627374100 ** get_address_of_finish_2() { return &___finish_2; }
	inline void set_finish_2(Action_1_t3627374100 * value)
	{
		___finish_2 = value;
		Il2CppCodeGenWriteBarrier(&___finish_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U24this_3)); }
	inline webservice_t1109616683 * get_U24this_3() const { return ___U24this_3; }
	inline webservice_t1109616683 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(webservice_t1109616683 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDownloadColorListU3Ec__Iterator3_t4106110459, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
