﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/MaterialData
struct MaterialData_t3551903345;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/MaterialData::.ctor()
extern "C"  void MaterialData__ctor_m3709838330 (MaterialData_t3551903345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
