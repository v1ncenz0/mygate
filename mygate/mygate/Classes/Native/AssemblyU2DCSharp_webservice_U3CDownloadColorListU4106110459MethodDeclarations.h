﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<DownloadColorList>c__Iterator3
struct U3CDownloadColorListU3Ec__Iterator3_t4106110459;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<DownloadColorList>c__Iterator3::.ctor()
extern "C"  void U3CDownloadColorListU3Ec__Iterator3__ctor_m2506807278 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<DownloadColorList>c__Iterator3::MoveNext()
extern "C"  bool U3CDownloadColorListU3Ec__Iterator3_MoveNext_m3913275026 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadColorList>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadColorListU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3089546146 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadColorList>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadColorListU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1121217818 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadColorList>c__Iterator3::Dispose()
extern "C"  void U3CDownloadColorListU3Ec__Iterator3_Dispose_m2664441049 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadColorList>c__Iterator3::Reset()
extern "C"  void U3CDownloadColorListU3Ec__Iterator3_Reset_m3338483567 (U3CDownloadColorListU3Ec__Iterator3_t4106110459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
