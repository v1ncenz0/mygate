﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetGatePosition
struct  SetGatePosition_t2001167784  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SetGatePosition::lamp_cad
	GameObject_t1756533147 * ___lamp_cad_4;
	// UnityEngine.GameObject SetGatePosition::lamp_render
	GameObject_t1756533147 * ___lamp_render_5;
	// UnityEngine.GameObject SetGatePosition::column_right
	GameObject_t1756533147 * ___column_right_6;
	// UnityEngine.GameObject SetGatePosition::column_left
	GameObject_t1756533147 * ___column_left_7;
	// UnityEngine.GameObject SetGatePosition::wall_right
	GameObject_t1756533147 * ___wall_right_8;
	// UnityEngine.GameObject SetGatePosition::wall_left
	GameObject_t1756533147 * ___wall_left_9;
	// UnityEngine.GameObject SetGatePosition::pavimento
	GameObject_t1756533147 * ___pavimento_10;
	// UnityEngine.GameObject SetGatePosition::sfondo
	GameObject_t1756533147 * ___sfondo_11;
	// UnityEngine.GameObject SetGatePosition::sfondo_giardino
	GameObject_t1756533147 * ___sfondo_giardino_12;

public:
	inline static int32_t get_offset_of_lamp_cad_4() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___lamp_cad_4)); }
	inline GameObject_t1756533147 * get_lamp_cad_4() const { return ___lamp_cad_4; }
	inline GameObject_t1756533147 ** get_address_of_lamp_cad_4() { return &___lamp_cad_4; }
	inline void set_lamp_cad_4(GameObject_t1756533147 * value)
	{
		___lamp_cad_4 = value;
		Il2CppCodeGenWriteBarrier(&___lamp_cad_4, value);
	}

	inline static int32_t get_offset_of_lamp_render_5() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___lamp_render_5)); }
	inline GameObject_t1756533147 * get_lamp_render_5() const { return ___lamp_render_5; }
	inline GameObject_t1756533147 ** get_address_of_lamp_render_5() { return &___lamp_render_5; }
	inline void set_lamp_render_5(GameObject_t1756533147 * value)
	{
		___lamp_render_5 = value;
		Il2CppCodeGenWriteBarrier(&___lamp_render_5, value);
	}

	inline static int32_t get_offset_of_column_right_6() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___column_right_6)); }
	inline GameObject_t1756533147 * get_column_right_6() const { return ___column_right_6; }
	inline GameObject_t1756533147 ** get_address_of_column_right_6() { return &___column_right_6; }
	inline void set_column_right_6(GameObject_t1756533147 * value)
	{
		___column_right_6 = value;
		Il2CppCodeGenWriteBarrier(&___column_right_6, value);
	}

	inline static int32_t get_offset_of_column_left_7() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___column_left_7)); }
	inline GameObject_t1756533147 * get_column_left_7() const { return ___column_left_7; }
	inline GameObject_t1756533147 ** get_address_of_column_left_7() { return &___column_left_7; }
	inline void set_column_left_7(GameObject_t1756533147 * value)
	{
		___column_left_7 = value;
		Il2CppCodeGenWriteBarrier(&___column_left_7, value);
	}

	inline static int32_t get_offset_of_wall_right_8() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___wall_right_8)); }
	inline GameObject_t1756533147 * get_wall_right_8() const { return ___wall_right_8; }
	inline GameObject_t1756533147 ** get_address_of_wall_right_8() { return &___wall_right_8; }
	inline void set_wall_right_8(GameObject_t1756533147 * value)
	{
		___wall_right_8 = value;
		Il2CppCodeGenWriteBarrier(&___wall_right_8, value);
	}

	inline static int32_t get_offset_of_wall_left_9() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___wall_left_9)); }
	inline GameObject_t1756533147 * get_wall_left_9() const { return ___wall_left_9; }
	inline GameObject_t1756533147 ** get_address_of_wall_left_9() { return &___wall_left_9; }
	inline void set_wall_left_9(GameObject_t1756533147 * value)
	{
		___wall_left_9 = value;
		Il2CppCodeGenWriteBarrier(&___wall_left_9, value);
	}

	inline static int32_t get_offset_of_pavimento_10() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___pavimento_10)); }
	inline GameObject_t1756533147 * get_pavimento_10() const { return ___pavimento_10; }
	inline GameObject_t1756533147 ** get_address_of_pavimento_10() { return &___pavimento_10; }
	inline void set_pavimento_10(GameObject_t1756533147 * value)
	{
		___pavimento_10 = value;
		Il2CppCodeGenWriteBarrier(&___pavimento_10, value);
	}

	inline static int32_t get_offset_of_sfondo_11() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___sfondo_11)); }
	inline GameObject_t1756533147 * get_sfondo_11() const { return ___sfondo_11; }
	inline GameObject_t1756533147 ** get_address_of_sfondo_11() { return &___sfondo_11; }
	inline void set_sfondo_11(GameObject_t1756533147 * value)
	{
		___sfondo_11 = value;
		Il2CppCodeGenWriteBarrier(&___sfondo_11, value);
	}

	inline static int32_t get_offset_of_sfondo_giardino_12() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784, ___sfondo_giardino_12)); }
	inline GameObject_t1756533147 * get_sfondo_giardino_12() const { return ___sfondo_giardino_12; }
	inline GameObject_t1756533147 ** get_address_of_sfondo_giardino_12() { return &___sfondo_giardino_12; }
	inline void set_sfondo_giardino_12(GameObject_t1756533147 * value)
	{
		___sfondo_giardino_12 = value;
		Il2CppCodeGenWriteBarrier(&___sfondo_giardino_12, value);
	}
};

struct SetGatePosition_t2001167784_StaticFields
{
public:
	// UnityEngine.Vector3 SetGatePosition::camera_position
	Vector3_t2243707580  ___camera_position_2;
	// UnityEngine.Quaternion SetGatePosition::camera_rotation
	Quaternion_t4030073918  ___camera_rotation_3;

public:
	inline static int32_t get_offset_of_camera_position_2() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784_StaticFields, ___camera_position_2)); }
	inline Vector3_t2243707580  get_camera_position_2() const { return ___camera_position_2; }
	inline Vector3_t2243707580 * get_address_of_camera_position_2() { return &___camera_position_2; }
	inline void set_camera_position_2(Vector3_t2243707580  value)
	{
		___camera_position_2 = value;
	}

	inline static int32_t get_offset_of_camera_rotation_3() { return static_cast<int32_t>(offsetof(SetGatePosition_t2001167784_StaticFields, ___camera_rotation_3)); }
	inline Quaternion_t4030073918  get_camera_rotation_3() const { return ___camera_rotation_3; }
	inline Quaternion_t4030073918 * get_address_of_camera_rotation_3() { return &___camera_rotation_3; }
	inline void set_camera_rotation_3(Quaternion_t4030073918  value)
	{
		___camera_rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
