﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// gridLayer
struct gridLayer_t4027367017;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemLayer
struct  ItemLayer_t674074160  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.ArrayList ItemLayer::items
	ArrayList_t4252133567 * ___items_2;
	// System.Collections.ArrayList ItemLayer::cells
	ArrayList_t4252133567 * ___cells_3;
	// gridLayer ItemLayer::grid
	gridLayer_t4027367017 * ___grid_4;

public:
	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(ItemLayer_t674074160, ___items_2)); }
	inline ArrayList_t4252133567 * get_items_2() const { return ___items_2; }
	inline ArrayList_t4252133567 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(ArrayList_t4252133567 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier(&___items_2, value);
	}

	inline static int32_t get_offset_of_cells_3() { return static_cast<int32_t>(offsetof(ItemLayer_t674074160, ___cells_3)); }
	inline ArrayList_t4252133567 * get_cells_3() const { return ___cells_3; }
	inline ArrayList_t4252133567 ** get_address_of_cells_3() { return &___cells_3; }
	inline void set_cells_3(ArrayList_t4252133567 * value)
	{
		___cells_3 = value;
		Il2CppCodeGenWriteBarrier(&___cells_3, value);
	}

	inline static int32_t get_offset_of_grid_4() { return static_cast<int32_t>(offsetof(ItemLayer_t674074160, ___grid_4)); }
	inline gridLayer_t4027367017 * get_grid_4() const { return ___grid_4; }
	inline gridLayer_t4027367017 ** get_address_of_grid_4() { return &___grid_4; }
	inline void set_grid_4(gridLayer_t4027367017 * value)
	{
		___grid_4 = value;
		Il2CppCodeGenWriteBarrier(&___grid_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
