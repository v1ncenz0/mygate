﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SplineMeshModifier3237667319.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineSineScaleModifier
struct  SplineSineScaleModifier_t862254749  : public SplineMeshModifier_t3237667319
{
public:
	// System.Single SplineSineScaleModifier::frequency
	float ___frequency_2;
	// System.Single SplineSineScaleModifier::offset
	float ___offset_3;
	// System.Single SplineSineScaleModifier::sinMultiplicator
	float ___sinMultiplicator_4;
	// System.Single SplineSineScaleModifier::sinOffset
	float ___sinOffset_5;

public:
	inline static int32_t get_offset_of_frequency_2() { return static_cast<int32_t>(offsetof(SplineSineScaleModifier_t862254749, ___frequency_2)); }
	inline float get_frequency_2() const { return ___frequency_2; }
	inline float* get_address_of_frequency_2() { return &___frequency_2; }
	inline void set_frequency_2(float value)
	{
		___frequency_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(SplineSineScaleModifier_t862254749, ___offset_3)); }
	inline float get_offset_3() const { return ___offset_3; }
	inline float* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(float value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_sinMultiplicator_4() { return static_cast<int32_t>(offsetof(SplineSineScaleModifier_t862254749, ___sinMultiplicator_4)); }
	inline float get_sinMultiplicator_4() const { return ___sinMultiplicator_4; }
	inline float* get_address_of_sinMultiplicator_4() { return &___sinMultiplicator_4; }
	inline void set_sinMultiplicator_4(float value)
	{
		___sinMultiplicator_4 = value;
	}

	inline static int32_t get_offset_of_sinOffset_5() { return static_cast<int32_t>(offsetof(SplineSineScaleModifier_t862254749, ___sinOffset_5)); }
	inline float get_sinOffset_5() const { return ___sinOffset_5; }
	inline float* get_address_of_sinOffset_5() { return &___sinOffset_5; }
	inline void set_sinOffset_5(float value)
	{
		___sinOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
