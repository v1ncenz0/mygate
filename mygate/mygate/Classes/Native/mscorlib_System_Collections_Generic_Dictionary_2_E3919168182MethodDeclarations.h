﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2120762099(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3919168182 *, Dictionary_2_t2599143480 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m766562386(__this, method) ((  Il2CppObject * (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2456106020(__this, method) ((  void (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2238706313(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2434698428(__this, method) ((  Il2CppObject * (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m414053278(__this, method) ((  Il2CppObject * (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::MoveNext()
#define Enumerator_MoveNext_m2635252044(__this, method) ((  bool (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::get_Current()
#define Enumerator_get_Current_m2653269820(__this, method) ((  KeyValuePair_2_t356488702  (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1992562039(__this, method) ((  String_t* (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4153748815(__this, method) ((  BumpParamDef_t684364218 * (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::Reset()
#define Enumerator_Reset_m1074221905(__this, method) ((  void (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::VerifyState()
#define Enumerator_VerifyState_m1460505884(__this, method) ((  void (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3102728244(__this, method) ((  void (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OBJ/BumpParamDef>::Dispose()
#define Enumerator_Dispose_m2572860063(__this, method) ((  void (*) (Enumerator_t3919168182 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
