﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library
struct  Library_t4121003545  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Library_t4121003545_StaticFields
{
public:
	// System.Collections.ArrayList Library::ItemsAvaible
	ArrayList_t4252133567 * ___ItemsAvaible_2;
	// System.Collections.ArrayList Library::ColorsAvaible
	ArrayList_t4252133567 * ___ColorsAvaible_3;
	// System.Collections.ArrayList Library::ScenesAvaible
	ArrayList_t4252133567 * ___ScenesAvaible_4;
	// System.Collections.ArrayList Library::StylesAvaible
	ArrayList_t4252133567 * ___StylesAvaible_5;
	// System.Collections.ArrayList Library::TypesAvaible
	ArrayList_t4252133567 * ___TypesAvaible_6;
	// System.Collections.ArrayList Library::GeometriesAvaible
	ArrayList_t4252133567 * ___GeometriesAvaible_7;
	// System.Collections.ArrayList Library::GatesAvaible
	ArrayList_t4252133567 * ___GatesAvaible_8;
	// System.Boolean Library::last_is_saved
	bool ___last_is_saved_9;

public:
	inline static int32_t get_offset_of_ItemsAvaible_2() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___ItemsAvaible_2)); }
	inline ArrayList_t4252133567 * get_ItemsAvaible_2() const { return ___ItemsAvaible_2; }
	inline ArrayList_t4252133567 ** get_address_of_ItemsAvaible_2() { return &___ItemsAvaible_2; }
	inline void set_ItemsAvaible_2(ArrayList_t4252133567 * value)
	{
		___ItemsAvaible_2 = value;
		Il2CppCodeGenWriteBarrier(&___ItemsAvaible_2, value);
	}

	inline static int32_t get_offset_of_ColorsAvaible_3() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___ColorsAvaible_3)); }
	inline ArrayList_t4252133567 * get_ColorsAvaible_3() const { return ___ColorsAvaible_3; }
	inline ArrayList_t4252133567 ** get_address_of_ColorsAvaible_3() { return &___ColorsAvaible_3; }
	inline void set_ColorsAvaible_3(ArrayList_t4252133567 * value)
	{
		___ColorsAvaible_3 = value;
		Il2CppCodeGenWriteBarrier(&___ColorsAvaible_3, value);
	}

	inline static int32_t get_offset_of_ScenesAvaible_4() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___ScenesAvaible_4)); }
	inline ArrayList_t4252133567 * get_ScenesAvaible_4() const { return ___ScenesAvaible_4; }
	inline ArrayList_t4252133567 ** get_address_of_ScenesAvaible_4() { return &___ScenesAvaible_4; }
	inline void set_ScenesAvaible_4(ArrayList_t4252133567 * value)
	{
		___ScenesAvaible_4 = value;
		Il2CppCodeGenWriteBarrier(&___ScenesAvaible_4, value);
	}

	inline static int32_t get_offset_of_StylesAvaible_5() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___StylesAvaible_5)); }
	inline ArrayList_t4252133567 * get_StylesAvaible_5() const { return ___StylesAvaible_5; }
	inline ArrayList_t4252133567 ** get_address_of_StylesAvaible_5() { return &___StylesAvaible_5; }
	inline void set_StylesAvaible_5(ArrayList_t4252133567 * value)
	{
		___StylesAvaible_5 = value;
		Il2CppCodeGenWriteBarrier(&___StylesAvaible_5, value);
	}

	inline static int32_t get_offset_of_TypesAvaible_6() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___TypesAvaible_6)); }
	inline ArrayList_t4252133567 * get_TypesAvaible_6() const { return ___TypesAvaible_6; }
	inline ArrayList_t4252133567 ** get_address_of_TypesAvaible_6() { return &___TypesAvaible_6; }
	inline void set_TypesAvaible_6(ArrayList_t4252133567 * value)
	{
		___TypesAvaible_6 = value;
		Il2CppCodeGenWriteBarrier(&___TypesAvaible_6, value);
	}

	inline static int32_t get_offset_of_GeometriesAvaible_7() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___GeometriesAvaible_7)); }
	inline ArrayList_t4252133567 * get_GeometriesAvaible_7() const { return ___GeometriesAvaible_7; }
	inline ArrayList_t4252133567 ** get_address_of_GeometriesAvaible_7() { return &___GeometriesAvaible_7; }
	inline void set_GeometriesAvaible_7(ArrayList_t4252133567 * value)
	{
		___GeometriesAvaible_7 = value;
		Il2CppCodeGenWriteBarrier(&___GeometriesAvaible_7, value);
	}

	inline static int32_t get_offset_of_GatesAvaible_8() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___GatesAvaible_8)); }
	inline ArrayList_t4252133567 * get_GatesAvaible_8() const { return ___GatesAvaible_8; }
	inline ArrayList_t4252133567 ** get_address_of_GatesAvaible_8() { return &___GatesAvaible_8; }
	inline void set_GatesAvaible_8(ArrayList_t4252133567 * value)
	{
		___GatesAvaible_8 = value;
		Il2CppCodeGenWriteBarrier(&___GatesAvaible_8, value);
	}

	inline static int32_t get_offset_of_last_is_saved_9() { return static_cast<int32_t>(offsetof(Library_t4121003545_StaticFields, ___last_is_saved_9)); }
	inline bool get_last_is_saved_9() const { return ___last_is_saved_9; }
	inline bool* get_address_of_last_is_saved_9() { return &___last_is_saved_9; }
	inline void set_last_is_saved_9(bool value)
	{
		___last_is_saved_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
