﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3941036653.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3559988393_gshared (Enumerator_t3941036653 * __this, Dictionary_2_t1251533215 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3559988393(__this, ___host0, method) ((  void (*) (Enumerator_t3941036653 *, Dictionary_2_t1251533215 *, const MethodInfo*))Enumerator__ctor_m3559988393_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m238116670_gshared (Enumerator_t3941036653 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m238116670(__this, method) ((  Il2CppObject * (*) (Enumerator_t3941036653 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m238116670_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3572558464_gshared (Enumerator_t3941036653 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3572558464(__this, method) ((  void (*) (Enumerator_t3941036653 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3572558464_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m436088697_gshared (Enumerator_t3941036653 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m436088697(__this, method) ((  void (*) (Enumerator_t3941036653 *, const MethodInfo*))Enumerator_Dispose_m436088697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m275791404_gshared (Enumerator_t3941036653 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m275791404(__this, method) ((  bool (*) (Enumerator_t3941036653 *, const MethodInfo*))Enumerator_MoveNext_m275791404_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Vector3>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1919590136_gshared (Enumerator_t3941036653 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1919590136(__this, method) ((  int32_t (*) (Enumerator_t3941036653 *, const MethodInfo*))Enumerator_get_Current_m1919590136_gshared)(__this, method)
