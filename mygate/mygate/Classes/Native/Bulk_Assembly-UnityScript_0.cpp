﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// events
struct events_t192635521;
// System.String
struct String_t;
// Events
struct Events_t192668321;
// System.Object
struct Il2CppObject;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_events192635521.h"
#include "AssemblyU2DUnityScript_events192635521MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DUnityScript_Events192668321.h"
#include "AssemblyU2DUnityScript_Events192668321MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityBuiltins1429886615MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void events::.ctor()
extern "C"  void events__ctor_m1762963057 (events_t192635521 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void events::Start()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1592916905;
extern const uint32_t events_Start_m3135376041_MetadataUsageId;
extern "C"  void events_Start_m3135376041 (events_t192635521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (events_Start_m3135376041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Application_ExternalCall_m3669435144(NULL /*static, unused*/, _stringLiteral1592916905, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void events::init(System.String)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Array_t1396575355_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral1507727252;
extern Il2CppCodeGenString* _stringLiteral1507694452;
extern Il2CppCodeGenString* _stringLiteral61941342;
extern Il2CppCodeGenString* _stringLiteral442046435;
extern const uint32_t events_init_m3029265161_MetadataUsageId;
extern "C"  void events_init_m3029265161 (events_t192635521 * __this, String_t* ___cmd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (events_init_m3029265161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Array_t1396575355 * V_1 = NULL;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Array_t1396575355 * L_1 = (Array_t1396575355 *)il2cpp_codegen_object_new(Array_t1396575355_il2cpp_TypeInfo_var);
		Array__ctor_m4252655432(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(_stringLiteral372029308);
		Il2CppChar L_3 = String_get_Chars_m4230566705(_stringLiteral372029308, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		V_2 = L_2;
		String_t* L_4 = ___cmd0;
		CharU5BU5D_t1328083999* L_5 = V_2;
		NullCheck(L_4);
		StringU5BU5D_t1642385972* L_6 = String_Split_m3326265864(L_4, L_5, /*hidden argument*/NULL);
		Array_t1396575355 * L_7 = Array_op_Implicit_m1130146437(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_3 = 0;
		goto IL_0047;
	}

IL_0035:
	{
		ArrayList_t4252133567 * L_8 = V_0;
		Array_t1396575355 * L_9 = V_1;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		Il2CppObject * L_11 = Array_get_Item_m3001262786(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_11);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_13 = V_3;
		Array_t1396575355 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Array_get_length_m754110913(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1507727252, /*hidden argument*/NULL);
		NullCheck(L_16);
		Component_t3819376471 * L_17 = GameObject_GetComponent_m1488468710(L_16, _stringLiteral1507694452, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_18 = V_0;
		NullCheck(L_17);
		Component_SendMessage_m913946877(L_17, _stringLiteral61941342, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1507727252, /*hidden argument*/NULL);
		NullCheck(L_19);
		Component_t3819376471 * L_20 = GameObject_GetComponent_m1488468710(L_19, _stringLiteral1507694452, /*hidden argument*/NULL);
		NullCheck(L_20);
		Component_SendMessage_m3615678587(L_20, _stringLiteral442046435, /*hidden argument*/NULL);
		return;
	}
}
// System.Void events::Main()
extern "C"  void events_Main_m871052604 (events_t192635521 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Events::.ctor()
extern "C"  void Events__ctor_m1141878417 (Events_t192668321 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::Start()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1592916905;
extern const uint32_t Events_Start_m1985659401_MetadataUsageId;
extern "C"  void Events_Start_m1985659401 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_Start_m1985659401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Application_ExternalCall_m3669435144(NULL /*static, unused*/, _stringLiteral1592916905, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setEditMode()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral2589505489;
extern const uint32_t Events_setEditMode_m2634528918_MetadataUsageId;
extern "C"  void Events_setEditMode_m2634528918 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setEditMode_m2634528918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral2589505489, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setDeleteMode()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral12991126;
extern const uint32_t Events_setDeleteMode_m334080453_MetadataUsageId;
extern "C"  void Events_setDeleteMode_m334080453 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setDeleteMode_m334080453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral12991126, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::commandLayer(System.Object)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern const uint32_t Events_commandLayer_m3803116123_MetadataUsageId;
extern "C"  void Events_commandLayer_m3803116123 (Events_t192668321 * __this, Il2CppObject * ___cmd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_commandLayer_m3803116123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Component_t3819376471 * G_B2_1 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	Component_t3819376471 * G_B1_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___cmd0;
		Il2CppObject * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		if (((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)))
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B1_0, L_4, /*hidden argument*/NULL);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		NullCheck(G_B2_1);
		Component_SendMessage_m3615678587(G_B2_1, ((String_t*)CastclassSealed(G_B2_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::commandAction(System.Object)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral1406331569;
extern const uint32_t Events_commandAction_m1454428482_MetadataUsageId;
extern "C"  void Events_commandAction_m1454428482 (Events_t192668321 * __this, Il2CppObject * ___cmd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_commandAction_m1454428482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Component_t3819376471 * G_B2_1 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	Component_t3819376471 * G_B1_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral1406331569, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___cmd0;
		Il2CppObject * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		if (((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)))
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B1_0, L_4, /*hidden argument*/NULL);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		NullCheck(G_B2_1);
		Component_SendMessage_m3615678587(G_B2_1, ((String_t*)CastclassSealed(G_B2_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::changeLayer(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral4003926112;
extern Il2CppCodeGenString* _stringLiteral284853037;
extern Il2CppCodeGenString* _stringLiteral350501615;
extern const uint32_t Events_changeLayer_m1113474337_MetadataUsageId;
extern "C"  void Events_changeLayer_m1113474337 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_changeLayer_m1113474337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___id0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral4003926112, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3819376471 * L_6 = GameObject_GetComponent_m1488468710(L_5, _stringLiteral2328182886, /*hidden argument*/NULL);
		bool L_7 = ((bool)0);
		Il2CppObject * L_8 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		Component_SendMessage_m913946877(L_6, _stringLiteral284853037, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_9);
		Component_t3819376471 * L_10 = GameObject_GetComponent_m1488468710(L_9, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_10);
		Component_SendMessage_m3615678587(L_10, _stringLiteral350501615, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setElement(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1257019010;
extern const uint32_t Events_setElement_m3367502596_MetadataUsageId;
extern "C"  void Events_setElement_m3367502596 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setElement_m3367502596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___id0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral1257019010, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setColor(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3457030254;
extern const uint32_t Events_setColor_m226370423_MetadataUsageId;
extern "C"  void Events_setColor_m226370423 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setColor_m226370423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___id0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral3457030254, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setColorHex(System.String)
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3775528629;
extern const uint32_t Events_setColorHex_m389828153_MetadataUsageId;
extern "C"  void Events_setColorHex_m389828153 (Events_t192668321 * __this, String_t* ___hex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setColorHex_m389828153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		String_t* L_2 = ___hex0;
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral3775528629, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setColorFrame(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3997508330;
extern const uint32_t Events_setColorFrame_m166263492_MetadataUsageId;
extern "C"  void Events_setColorFrame_m166263492 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setColorFrame_m166263492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___id0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral3997508330, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setColorLayer(System.String)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Array_t1396575355_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029335;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral2868473570;
extern const uint32_t Events_setColorLayer_m18571855_MetadataUsageId;
extern "C"  void Events_setColorLayer_m18571855 (Events_t192668321 * __this, String_t* ___cmd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setColorLayer_m18571855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Array_t1396575355 * V_1 = NULL;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Array_t1396575355 * L_1 = (Array_t1396575355 *)il2cpp_codegen_object_new(Array_t1396575355_il2cpp_TypeInfo_var);
		Array__ctor_m4252655432(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(_stringLiteral372029335);
		Il2CppChar L_3 = String_get_Chars_m4230566705(_stringLiteral372029335, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		V_2 = L_2;
		String_t* L_4 = ___cmd0;
		CharU5BU5D_t1328083999* L_5 = V_2;
		NullCheck(L_4);
		StringU5BU5D_t1642385972* L_6 = String_Split_m3326265864(L_4, L_5, /*hidden argument*/NULL);
		Array_t1396575355 * L_7 = Array_op_Implicit_m1130146437(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_3 = 0;
		goto IL_0047;
	}

IL_0035:
	{
		ArrayList_t4252133567 * L_8 = V_0;
		Array_t1396575355 * L_9 = V_1;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		Il2CppObject * L_11 = Array_get_Item_m3001262786(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_11);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_13 = V_3;
		Array_t1396575355 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Array_get_length_m754110913(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_16);
		Component_t3819376471 * L_17 = GameObject_GetComponent_m1488468710(L_16, _stringLiteral2328182886, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_18 = V_0;
		NullCheck(L_17);
		Component_SendMessage_m913946877(L_17, _stringLiteral2868473570, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::resetColorLayer()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3976281315;
extern const uint32_t Events_resetColorLayer_m2071387886_MetadataUsageId;
extern "C"  void Events_resetColorLayer_m2071387886 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_resetColorLayer_m2071387886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3976281315, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::getColorLayer(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral70156782;
extern const uint32_t Events_getColorLayer_m1025765032_MetadataUsageId;
extern "C"  void Events_getColorLayer_m1025765032 (Events_t192668321 * __this, int32_t ___id_layer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_getColorLayer_m1025765032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___id_layer0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral70156782, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::getColorFrame()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1199192574;
extern const uint32_t Events_getColorFrame_m3384491399_MetadataUsageId;
extern "C"  void Events_getColorFrame_m3384491399 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_getColorFrame_m3384491399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral1199192574, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::moveCamera(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1510523156;
extern const uint32_t Events_moveCamera_m402994146_MetadataUsageId;
extern "C"  void Events_moveCamera_m402994146 (Events_t192668321 * __this, int32_t ___degree0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_moveCamera_m402994146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		int32_t L_2 = ___degree0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Component_SendMessage_m913946877(L_1, _stringLiteral1510523156, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::resetCamera()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3745461938;
extern const uint32_t Events_resetCamera_m2930362505_MetadataUsageId;
extern "C"  void Events_resetCamera_m2930362505 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_resetCamera_m2930362505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3745461938, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::navigation(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029335;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral3921520404;
extern const uint32_t Events_navigation_m1350350129_MetadataUsageId;
extern "C"  void Events_navigation_m1350350129 (Events_t192668321 * __this, String_t* ___txt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_navigation_m1350350129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	{
		String_t* L_0 = ___txt0;
		NullCheck(L_0);
		String_t* L_1 = String_ToString_m3226660001(L_0, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(_stringLiteral372029335);
		Il2CppChar L_3 = String_get_Chars_m4230566705(_stringLiteral372029335, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		NullCheck(L_1);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3819376471 * L_6 = GameObject_GetComponent_m1488468710(L_5, _stringLiteral3921520404, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 0;
		String_t* L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		StringU5BU5D_t1642385972* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 1;
		String_t* L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		int32_t L_13 = UnityBuiltins_parseInt_m2481732514(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_6);
		Component_SendMessage_m913946877(L_6, L_9, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setSceneVisible(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1046827532;
extern Il2CppCodeGenString* _stringLiteral2328499301;
extern const uint32_t Events_setSceneVisible_m2812210446_MetadataUsageId;
extern "C"  void Events_setSceneVisible_m2812210446 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setSceneVisible_m2812210446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = ___visible0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m1488468710(L_1, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_2);
		Component_SendMessage_m3615678587(L_2, _stringLiteral1046827532, /*hidden argument*/NULL);
		goto IL_004a;
	}

IL_002c:
	{
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_3);
		Component_t3819376471 * L_4 = GameObject_GetComponent_m1488468710(L_3, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_SendMessage_m3615678587(L_4, _stringLiteral2328499301, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Events::setSpecularMode(System.Int32)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral952256316;
extern const uint32_t Events_setSpecularMode_m820216658_MetadataUsageId;
extern "C"  void Events_setSpecularMode_m820216658 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setSpecularMode_m820216658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = ___visible0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000b;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_000b:
	{
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m1488468710(L_1, _stringLiteral2328182886, /*hidden argument*/NULL);
		bool L_3 = V_0;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Component_SendMessage_m913946877(L_2, _stringLiteral952256316, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::invertSpecularMode()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1637942932;
extern const uint32_t Events_invertSpecularMode_m3455852047_MetadataUsageId;
extern "C"  void Events_invertSpecularMode_m3455852047 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_invertSpecularMode_m3455852047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral1637942932, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setVisibilityLayer(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029335;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1959959831;
extern const uint32_t Events_setVisibilityLayer_m4014999546_MetadataUsageId;
extern "C"  void Events_setVisibilityLayer_m4014999546 (Events_t192668321 * __this, String_t* ___txt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setVisibilityLayer_m4014999546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	ArrayList_t4252133567 * V_1 = NULL;
	bool V_2 = false;
	{
		String_t* L_0 = ___txt0;
		NullCheck(L_0);
		String_t* L_1 = String_ToString_m3226660001(L_0, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(_stringLiteral372029335);
		Il2CppChar L_3 = String_get_Chars_m4230566705(_stringLiteral372029335, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		NullCheck(L_1);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_4;
		ArrayList_t4252133567 * L_5 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = (bool)0;
		StringU5BU5D_t1642385972* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = 1;
		String_t* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		int32_t L_9 = UnityBuiltins_parseInt_m2481732514(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0038;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_0038:
	{
		ArrayList_t4252133567 * L_10 = V_1;
		StringU5BU5D_t1642385972* L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		String_t* L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		int32_t L_14 = UnityBuiltins_parseInt_m2481732514(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_10);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_10, L_16);
		ArrayList_t4252133567 * L_17 = V_1;
		bool L_18 = V_2;
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_17, L_20);
		GameObject_t1756533147 * L_21 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_21);
		Component_t3819376471 * L_22 = GameObject_GetComponent_m1488468710(L_21, _stringLiteral2328182886, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_23 = V_1;
		NullCheck(L_22);
		Component_SendMessage_m913946877(L_22, _stringLiteral1959959831, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::noGrid(System.Int32)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral2645078417;
extern const uint32_t Events_noGrid_m558178953_MetadataUsageId;
extern "C"  void Events_noGrid_m558178953 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_noGrid_m558178953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = ___visible0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000b;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_000b:
	{
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m1488468710(L_1, _stringLiteral2328182886, /*hidden argument*/NULL);
		bool L_3 = V_0;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Component_SendMessage_m913946877(L_2, _stringLiteral2645078417, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::invertGrid()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3432924985;
extern const uint32_t Events_invertGrid_m2987816943_MetadataUsageId;
extern "C"  void Events_invertGrid_m2987816943 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_invertGrid_m2987816943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3432924985, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::fillGrid()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral33736975;
extern const uint32_t Events_fillGrid_m2499476784_MetadataUsageId;
extern "C"  void Events_fillGrid_m2499476784 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_fillGrid_m2499476784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral33736975, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::eraseGrid()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1722368100;
extern const uint32_t Events_eraseGrid_m4232299893_MetadataUsageId;
extern "C"  void Events_eraseGrid_m4232299893 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_eraseGrid_m4232299893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral1722368100, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::captureScreen()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral230325260;
extern const uint32_t Events_captureScreen_m1162566373_MetadataUsageId;
extern "C"  void Events_captureScreen_m1162566373 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_captureScreen_m1162566373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral230325260, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::saveGate()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3147317804;
extern const uint32_t Events_saveGate_m1664254519_MetadataUsageId;
extern "C"  void Events_saveGate_m1664254519 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_saveGate_m1664254519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3147317804, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::exportToOBJ()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3153563626;
extern const uint32_t Events_exportToOBJ_m2776330801_MetadataUsageId;
extern "C"  void Events_exportToOBJ_m2776330801 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_exportToOBJ_m2776330801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3153563626, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::setSaveWithScreen(System.Int32)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral971891097;
extern const uint32_t Events_setSaveWithScreen_m3163682793_MetadataUsageId;
extern "C"  void Events_setSaveWithScreen_m3163682793 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_setSaveWithScreen_m3163682793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = ___visible0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000b;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_000b:
	{
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m1488468710(L_1, _stringLiteral2328182886, /*hidden argument*/NULL);
		bool L_3 = V_0;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Component_SendMessage_m913946877(L_2, _stringLiteral971891097, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::invertSceneForRender()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral987505885;
extern const uint32_t Events_invertSceneForRender_m2728888782_MetadataUsageId;
extern "C"  void Events_invertSceneForRender_m2728888782 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_invertSceneForRender_m2728888782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral987505885, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::showSceneForRender()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral1046827532;
extern const uint32_t Events_showSceneForRender_m186653017_MetadataUsageId;
extern "C"  void Events_showSceneForRender_m186653017 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_showSceneForRender_m186653017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral1046827532, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::showCAD()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral2327423365;
extern const uint32_t Events_showCAD_m1715081090_MetadataUsageId;
extern "C"  void Events_showCAD_m1715081090 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_showCAD_m1715081090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral2327423365, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::init(System.String)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Array_t1396575355_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral1507727252;
extern Il2CppCodeGenString* _stringLiteral1507694452;
extern Il2CppCodeGenString* _stringLiteral61941342;
extern Il2CppCodeGenString* _stringLiteral442046435;
extern const uint32_t Events_init_m3829042537_MetadataUsageId;
extern "C"  void Events_init_m3829042537 (Events_t192668321 * __this, String_t* ___cmd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_init_m3829042537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Array_t1396575355 * V_1 = NULL;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Array_t1396575355 * L_1 = (Array_t1396575355 *)il2cpp_codegen_object_new(Array_t1396575355_il2cpp_TypeInfo_var);
		Array__ctor_m4252655432(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(_stringLiteral372029308);
		Il2CppChar L_3 = String_get_Chars_m4230566705(_stringLiteral372029308, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		V_2 = L_2;
		String_t* L_4 = ___cmd0;
		CharU5BU5D_t1328083999* L_5 = V_2;
		NullCheck(L_4);
		StringU5BU5D_t1642385972* L_6 = String_Split_m3326265864(L_4, L_5, /*hidden argument*/NULL);
		Array_t1396575355 * L_7 = Array_op_Implicit_m1130146437(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_3 = 0;
		goto IL_0047;
	}

IL_0035:
	{
		ArrayList_t4252133567 * L_8 = V_0;
		Array_t1396575355 * L_9 = V_1;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		Il2CppObject * L_11 = Array_get_Item_m3001262786(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_11);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_13 = V_3;
		Array_t1396575355 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Array_get_length_m754110913(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1507727252, /*hidden argument*/NULL);
		NullCheck(L_16);
		Component_t3819376471 * L_17 = GameObject_GetComponent_m1488468710(L_16, _stringLiteral1507694452, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_18 = V_0;
		NullCheck(L_17);
		Component_SendMessage_m913946877(L_17, _stringLiteral61941342, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1507727252, /*hidden argument*/NULL);
		NullCheck(L_19);
		Component_t3819376471 * L_20 = GameObject_GetComponent_m1488468710(L_19, _stringLiteral1507694452, /*hidden argument*/NULL);
		NullCheck(L_20);
		Component_SendMessage_m3615678587(L_20, _stringLiteral442046435, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::showPrice()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral309011196;
extern const uint32_t Events_showPrice_m3589980459_MetadataUsageId;
extern "C"  void Events_showPrice_m3589980459 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_showPrice_m3589980459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral309011196, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::back()
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral2328182886;
extern Il2CppCodeGenString* _stringLiteral3012669769;
extern const uint32_t Events_back_m2714829062_MetadataUsageId;
extern "C"  void Events_back_m2714829062 (Events_t192668321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Events_back_m2714829062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3675648761, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_1 = GameObject_GetComponent_m1488468710(L_0, _stringLiteral2328182886, /*hidden argument*/NULL);
		NullCheck(L_1);
		Component_SendMessage_m3615678587(L_1, _stringLiteral3012669769, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Events::Main()
extern "C"  void Events_Main_m1659229276 (Events_t192668321 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
