﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemColor
struct ItemColor_t1117508970;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ItemColor::.ctor()
extern "C"  void ItemColor__ctor_m1191453907 (ItemColor_t1117508970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ItemColor::ColorToHex(UnityEngine.Color32)
extern "C"  String_t* ItemColor_ColorToHex_m3115257336 (Il2CppObject * __this /* static, unused */, Color32_t874517518  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ItemColor::HexToColor(System.String)
extern "C"  Color_t2020392075  ItemColor_HexToColor_m1223902139 (Il2CppObject * __this /* static, unused */, String_t* ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemColor::downloadTextureColor(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemColor_downloadTextureColor_m2503157958 (ItemColor_t1117508970 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemColor::setMaterial()
extern "C"  void ItemColor_setMaterial_m1660929528 (ItemColor_t1117508970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
