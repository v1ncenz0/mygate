﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BranchingSplinePath_Direction1768093872.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BranchingSplinePath
struct  BranchingSplinePath_t1574214842  : public Il2CppObject
{
public:
	// Spline BranchingSplinePath::spline
	Spline_t1260612603 * ___spline_0;
	// BranchingSplinePath/Direction BranchingSplinePath::direction
	int32_t ___direction_1;

public:
	inline static int32_t get_offset_of_spline_0() { return static_cast<int32_t>(offsetof(BranchingSplinePath_t1574214842, ___spline_0)); }
	inline Spline_t1260612603 * get_spline_0() const { return ___spline_0; }
	inline Spline_t1260612603 ** get_address_of_spline_0() { return &___spline_0; }
	inline void set_spline_0(Spline_t1260612603 * value)
	{
		___spline_0 = value;
		Il2CppCodeGenWriteBarrier(&___spline_0, value);
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(BranchingSplinePath_t1574214842, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
