﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline/LengthData
struct LengthData_t3176416758;
// Spline
struct Spline_t1260612603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"

// System.Void Spline/LengthData::.ctor()
extern "C"  void LengthData__ctor_m1523479797 (LengthData_t3176416758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline/LengthData::Calculate(Spline)
extern "C"  void LengthData_Calculate_m3666047610 (LengthData_t3176416758 * __this, Spline_t1260612603 * ___spline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline/LengthData::SetupSplinePositions(Spline)
extern "C"  void LengthData_SetupSplinePositions_m1969339086 (LengthData_t3176416758 * __this, Spline_t1260612603 * ___spline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
