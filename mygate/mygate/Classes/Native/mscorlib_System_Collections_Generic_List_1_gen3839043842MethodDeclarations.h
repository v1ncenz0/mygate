﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FaceIndices>
struct List_1_t3839043842;
// System.Collections.Generic.IEnumerable`1<FaceIndices>
struct IEnumerable_1_t467082459;
// FaceIndices[]
struct FaceIndicesU5BU5D_t2696217939;
// System.Collections.Generic.IEnumerator`1<FaceIndices>
struct IEnumerator_1_t1945446537;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<FaceIndices>
struct ICollection_1_t1127030719;
// System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>
struct ReadOnlyCollection_1_t360741106;
// System.Predicate`1<FaceIndices>
struct Predicate_1_t2912892825;
// System.Comparison`1<FaceIndices>
struct Comparison_1_t1436694265;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3373773516.h"

// System.Void System.Collections.Generic.List`1<FaceIndices>::.ctor()
extern "C"  void List_1__ctor_m397473847_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1__ctor_m397473847(__this, method) ((  void (*) (List_1_t3839043842 *, const MethodInfo*))List_1__ctor_m397473847_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3724712755_gshared (List_1_t3839043842 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3724712755(__this, ___collection0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3724712755_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3185759345_gshared (List_1_t3839043842 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3185759345(__this, ___capacity0, method) ((  void (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1__ctor_m3185759345_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1635052775_gshared (List_1_t3839043842 * __this, FaceIndicesU5BU5D_t2696217939* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1635052775(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3839043842 *, FaceIndicesU5BU5D_t2696217939*, int32_t, const MethodInfo*))List_1__ctor_m1635052775_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::.cctor()
extern "C"  void List_1__cctor_m52791867_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m52791867(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m52791867_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FaceIndices>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835327296_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835327296(__this, method) ((  Il2CppObject* (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835327296_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m980235286_gshared (List_1_t3839043842 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m980235286(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3839043842 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m980235286_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FaceIndices>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3707383833_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3707383833(__this, method) ((  Il2CppObject * (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3707383833_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1299903190_gshared (List_1_t3839043842 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1299903190(__this, ___item0, method) ((  int32_t (*) (List_1_t3839043842 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1299903190_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2258198880_gshared (List_1_t3839043842 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2258198880(__this, ___item0, method) ((  bool (*) (List_1_t3839043842 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2258198880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1651227300_gshared (List_1_t3839043842 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1651227300(__this, ___item0, method) ((  int32_t (*) (List_1_t3839043842 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1651227300_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1911413257_gshared (List_1_t3839043842 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1911413257(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3839043842 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1911413257_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m833642561_gshared (List_1_t3839043842 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m833642561(__this, ___item0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m833642561_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1895948573_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1895948573(__this, method) ((  bool (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1895948573_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3973950334_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3973950334(__this, method) ((  bool (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3973950334_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FaceIndices>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1172265558_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1172265558(__this, method) ((  Il2CppObject * (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1172265558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4248818861_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4248818861(__this, method) ((  bool (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4248818861_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4021715554_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4021715554(__this, method) ((  bool (*) (List_1_t3839043842 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4021715554_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m396973421_gshared (List_1_t3839043842 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m396973421(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m396973421_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3317357984_gshared (List_1_t3839043842 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3317357984(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3839043842 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3317357984_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Add(T)
extern "C"  void List_1_Add_m1712192411_gshared (List_1_t3839043842 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define List_1_Add_m1712192411(__this, ___item0, method) ((  void (*) (List_1_t3839043842 *, FaceIndices_t174955414 , const MethodInfo*))List_1_Add_m1712192411_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1436239612_gshared (List_1_t3839043842 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1436239612(__this, ___newCount0, method) ((  void (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1436239612_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2811320529_gshared (List_1_t3839043842 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2811320529(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3839043842 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2811320529_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3385744300_gshared (List_1_t3839043842 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3385744300(__this, ___collection0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3385744300_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3805944396_gshared (List_1_t3839043842 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3805944396(__this, ___enumerable0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3805944396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m122284537_gshared (List_1_t3839043842 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m122284537(__this, ___collection0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m122284537_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FaceIndices>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t360741106 * List_1_AsReadOnly_m258382858_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m258382858(__this, method) ((  ReadOnlyCollection_1_t360741106 * (*) (List_1_t3839043842 *, const MethodInfo*))List_1_AsReadOnly_m258382858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Clear()
extern "C"  void List_1_Clear_m3103561881_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_Clear_m3103561881(__this, method) ((  void (*) (List_1_t3839043842 *, const MethodInfo*))List_1_Clear_m3103561881_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::Contains(T)
extern "C"  bool List_1_Contains_m3722895323_gshared (List_1_t3839043842 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define List_1_Contains_m3722895323(__this, ___item0, method) ((  bool (*) (List_1_t3839043842 *, FaceIndices_t174955414 , const MethodInfo*))List_1_Contains_m3722895323_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1927605077_gshared (List_1_t3839043842 * __this, FaceIndicesU5BU5D_t2696217939* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1927605077(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3839043842 *, FaceIndicesU5BU5D_t2696217939*, int32_t, const MethodInfo*))List_1_CopyTo_m1927605077_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<FaceIndices>::Find(System.Predicate`1<T>)
extern "C"  FaceIndices_t174955414  List_1_Find_m1865965047_gshared (List_1_t3839043842 * __this, Predicate_1_t2912892825 * ___match0, const MethodInfo* method);
#define List_1_Find_m1865965047(__this, ___match0, method) ((  FaceIndices_t174955414  (*) (List_1_t3839043842 *, Predicate_1_t2912892825 *, const MethodInfo*))List_1_Find_m1865965047_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3831000638_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2912892825 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3831000638(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2912892825 *, const MethodInfo*))List_1_CheckMatch_m3831000638_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2190628691_gshared (List_1_t3839043842 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2912892825 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2190628691(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3839043842 *, int32_t, int32_t, Predicate_1_t2912892825 *, const MethodInfo*))List_1_GetIndex_m2190628691_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FaceIndices>::GetEnumerator()
extern "C"  Enumerator_t3373773516  List_1_GetEnumerator_m135192202_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m135192202(__this, method) ((  Enumerator_t3373773516  (*) (List_1_t3839043842 *, const MethodInfo*))List_1_GetEnumerator_m135192202_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FaceIndices>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t3839043842 * List_1_GetRange_m3719615772_gshared (List_1_t3839043842 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m3719615772(__this, ___index0, ___count1, method) ((  List_1_t3839043842 * (*) (List_1_t3839043842 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m3719615772_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2053814177_gshared (List_1_t3839043842 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2053814177(__this, ___item0, method) ((  int32_t (*) (List_1_t3839043842 *, FaceIndices_t174955414 , const MethodInfo*))List_1_IndexOf_m2053814177_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2246225738_gshared (List_1_t3839043842 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2246225738(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3839043842 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2246225738_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3404732977_gshared (List_1_t3839043842 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3404732977(__this, ___index0, method) ((  void (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3404732977_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3284627280_gshared (List_1_t3839043842 * __this, int32_t ___index0, FaceIndices_t174955414  ___item1, const MethodInfo* method);
#define List_1_Insert_m3284627280(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3839043842 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))List_1_Insert_m3284627280_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3496443511_gshared (List_1_t3839043842 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3496443511(__this, ___collection0, method) ((  void (*) (List_1_t3839043842 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3496443511_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FaceIndices>::Remove(T)
extern "C"  bool List_1_Remove_m321396790_gshared (List_1_t3839043842 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define List_1_Remove_m321396790(__this, ___item0, method) ((  bool (*) (List_1_t3839043842 *, FaceIndices_t174955414 , const MethodInfo*))List_1_Remove_m321396790_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m482072966_gshared (List_1_t3839043842 * __this, Predicate_1_t2912892825 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m482072966(__this, ___match0, method) ((  int32_t (*) (List_1_t3839043842 *, Predicate_1_t2912892825 *, const MethodInfo*))List_1_RemoveAll_m482072966_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m786572500_gshared (List_1_t3839043842 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m786572500(__this, ___index0, method) ((  void (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_RemoveAt_m786572500_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Reverse()
extern "C"  void List_1_Reverse_m983964120_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_Reverse_m983964120(__this, method) ((  void (*) (List_1_t3839043842 *, const MethodInfo*))List_1_Reverse_m983964120_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Sort()
extern "C"  void List_1_Sort_m270931052_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_Sort_m270931052(__this, method) ((  void (*) (List_1_t3839043842 *, const MethodInfo*))List_1_Sort_m270931052_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3159034003_gshared (List_1_t3839043842 * __this, Comparison_1_t1436694265 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3159034003(__this, ___comparison0, method) ((  void (*) (List_1_t3839043842 *, Comparison_1_t1436694265 *, const MethodInfo*))List_1_Sort_m3159034003_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FaceIndices>::ToArray()
extern "C"  FaceIndicesU5BU5D_t2696217939* List_1_ToArray_m465631019_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_ToArray_m465631019(__this, method) ((  FaceIndicesU5BU5D_t2696217939* (*) (List_1_t3839043842 *, const MethodInfo*))List_1_ToArray_m465631019_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::TrimExcess()
extern "C"  void List_1_TrimExcess_m132463541_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m132463541(__this, method) ((  void (*) (List_1_t3839043842 *, const MethodInfo*))List_1_TrimExcess_m132463541_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2576117931_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2576117931(__this, method) ((  int32_t (*) (List_1_t3839043842 *, const MethodInfo*))List_1_get_Capacity_m2576117931_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m120979724_gshared (List_1_t3839043842 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m120979724(__this, ___value0, method) ((  void (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_set_Capacity_m120979724_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FaceIndices>::get_Count()
extern "C"  int32_t List_1_get_Count_m1656673535_gshared (List_1_t3839043842 * __this, const MethodInfo* method);
#define List_1_get_Count_m1656673535(__this, method) ((  int32_t (*) (List_1_t3839043842 *, const MethodInfo*))List_1_get_Count_m1656673535_gshared)(__this, method)
// T System.Collections.Generic.List`1<FaceIndices>::get_Item(System.Int32)
extern "C"  FaceIndices_t174955414  List_1_get_Item_m2326024674_gshared (List_1_t3839043842 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2326024674(__this, ___index0, method) ((  FaceIndices_t174955414  (*) (List_1_t3839043842 *, int32_t, const MethodInfo*))List_1_get_Item_m2326024674_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FaceIndices>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1109406293_gshared (List_1_t3839043842 * __this, int32_t ___index0, FaceIndices_t174955414  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1109406293(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3839043842 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))List_1_set_Item_m1109406293_gshared)(__this, ___index0, ___value1, method)
