﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LinearInterpolator
struct LinearInterpolator_t4145168420;

#include "codegen/il2cpp-codegen.h"

// System.Void LinearInterpolator::.ctor()
extern "C"  void LinearInterpolator__ctor_m463961201 (LinearInterpolator_t4145168420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
