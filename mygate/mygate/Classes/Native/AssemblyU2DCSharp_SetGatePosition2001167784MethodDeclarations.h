﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SetGatePosition
struct SetGatePosition_t2001167784;

#include "codegen/il2cpp-codegen.h"

// System.Void SetGatePosition::.ctor()
extern "C"  void SetGatePosition__ctor_m818740491 (SetGatePosition_t2001167784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetGatePosition::Start()
extern "C"  void SetGatePosition_Start_m1206518191 (SetGatePosition_t2001167784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetGatePosition::setPosition()
extern "C"  void SetGatePosition_setPosition_m3985550820 (SetGatePosition_t2001167784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetGatePosition::Reset()
extern "C"  void SetGatePosition_Reset_m3216071336 (SetGatePosition_t2001167784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetGatePosition::setSceneForRender(System.Boolean)
extern "C"  void SetGatePosition_setSceneForRender_m1584082905 (SetGatePosition_t2001167784 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
