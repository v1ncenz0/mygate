﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_gates
struct init_gates_t3190639043;

#include "codegen/il2cpp-codegen.h"

// System.Void init_gates::.ctor()
extern "C"  void init_gates__ctor_m3200899908 (init_gates_t3190639043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::Start()
extern "C"  void init_gates_Start_m2371372112 (init_gates_t3190639043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::InitDownloadGates()
extern "C"  void init_gates_InitDownloadGates_m2478288978 (init_gates_t3190639043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::downloadGates()
extern "C"  void init_gates_downloadGates_m4016511440 (init_gates_t3190639043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::populateList(System.Int32)
extern "C"  void init_gates_populateList_m1004220059 (init_gates_t3190639043 * __this, int32_t ___downloaded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::<Start>m__0(System.Boolean)
extern "C"  void init_gates_U3CStartU3Em__0_m1080007048 (init_gates_t3190639043 * __this, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_gates::<downloadGates>m__1(System.Int32)
extern "C"  void init_gates_U3CdownloadGatesU3Em__1_m377640421 (init_gates_t3190639043 * __this, int32_t ___downloaded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
