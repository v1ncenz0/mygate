﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// webservice
struct webservice_t1109616683;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<PoolDownloadColor>c__AnonStorey9
struct  U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> webservice/<PoolDownloadColor>c__AnonStorey9::finish
	Action_1_t3627374100 * ___finish_0;
	// webservice webservice/<PoolDownloadColor>c__AnonStorey9::$this
	webservice_t1109616683 * ___U24this_1;

public:
	inline static int32_t get_offset_of_finish_0() { return static_cast<int32_t>(offsetof(U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197, ___finish_0)); }
	inline Action_1_t3627374100 * get_finish_0() const { return ___finish_0; }
	inline Action_1_t3627374100 ** get_address_of_finish_0() { return &___finish_0; }
	inline void set_finish_0(Action_1_t3627374100 * value)
	{
		___finish_0 = value;
		Il2CppCodeGenWriteBarrier(&___finish_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197, ___U24this_1)); }
	inline webservice_t1109616683 * get_U24this_1() const { return ___U24this_1; }
	inline webservice_t1109616683 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(webservice_t1109616683 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
