﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineAnimator
struct SplineAnimator_t2697441160;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"

// System.Void SplineAnimator::.ctor()
extern "C"  void SplineAnimator__ctor_m281578273 (SplineAnimator_t2697441160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineAnimator::Update()
extern "C"  void SplineAnimator_Update_m2196516530 (SplineAnimator_t2697441160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineAnimator::WrapValue(System.Single,System.Single,System.Single,UnityEngine.WrapMode)
extern "C"  float SplineAnimator_WrapValue_m1920101189 (SplineAnimator_t2697441160 * __this, float ___v0, float ___start1, float ___end2, int32_t ___wMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
