﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<PoolDownloadColor>c__AnonStorey9
struct U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<PoolDownloadColor>c__AnonStorey9::.ctor()
extern "C"  void U3CPoolDownloadColorU3Ec__AnonStorey9__ctor_m1950251130 (U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<PoolDownloadColor>c__AnonStorey9::<>m__0(System.Boolean)
extern "C"  void U3CPoolDownloadColorU3Ec__AnonStorey9_U3CU3Em__0_m2449525328 (U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197 * __this, bool ___textureDownloaded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<PoolDownloadColor>c__AnonStorey9::<>m__1(System.Boolean)
extern "C"  void U3CPoolDownloadColorU3Ec__AnonStorey9_U3CU3Em__1_m48925173 (U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197 * __this, bool ___finishDownloadTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
