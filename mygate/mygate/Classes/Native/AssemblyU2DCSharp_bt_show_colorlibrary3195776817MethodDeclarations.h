﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_show_colorlibrary
struct bt_show_colorlibrary_t3195776817;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_show_colorlibrary::.ctor()
extern "C"  void bt_show_colorlibrary__ctor_m2198385592 (bt_show_colorlibrary_t3195776817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_show_colorlibrary::showColorLibrary()
extern "C"  void bt_show_colorlibrary_showColorLibrary_m1978905829 (bt_show_colorlibrary_t3195776817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
