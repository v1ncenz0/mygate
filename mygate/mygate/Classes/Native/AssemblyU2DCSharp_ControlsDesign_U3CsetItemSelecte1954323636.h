﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Item
struct Item_t2440468191;
// ControlsDesign
struct ControlsDesign_t1600104368;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<setItemSelected>c__AnonStorey4
struct  U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636  : public Il2CppObject
{
public:
	// Item ControlsDesign/<setItemSelected>c__AnonStorey4::item
	Item_t2440468191 * ___item_0;
	// ControlsDesign ControlsDesign/<setItemSelected>c__AnonStorey4::$this
	ControlsDesign_t1600104368 * ___U24this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636, ___item_0)); }
	inline Item_t2440468191 * get_item_0() const { return ___item_0; }
	inline Item_t2440468191 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_t2440468191 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636, ___U24this_1)); }
	inline ControlsDesign_t1600104368 * get_U24this_1() const { return ___U24this_1; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ControlsDesign_t1600104368 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
