﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<createOBJ>c__Iterator3
struct U3CcreateOBJU3Ec__Iterator3_t3024815716;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<createOBJ>c__Iterator3::.ctor()
extern "C"  void U3CcreateOBJU3Ec__Iterator3__ctor_m1464331261 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign/<createOBJ>c__Iterator3::MoveNext()
extern "C"  bool U3CcreateOBJU3Ec__Iterator3_MoveNext_m332177895 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<createOBJ>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcreateOBJU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1246534439 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<createOBJ>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcreateOBJU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3792143983 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<createOBJ>c__Iterator3::Dispose()
extern "C"  void U3CcreateOBJU3Ec__Iterator3_Dispose_m3721089638 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<createOBJ>c__Iterator3::Reset()
extern "C"  void U3CcreateOBJU3Ec__Iterator3_Reset_m4042208860 (U3CcreateOBJU3Ec__Iterator3_t3024815716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
