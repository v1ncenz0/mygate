﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_Dictionary_2_g3654642240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineNode/NodeParameterRegister
struct  NodeParameterRegister_t3732266052  : public Dictionary_2_t3654642240
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
