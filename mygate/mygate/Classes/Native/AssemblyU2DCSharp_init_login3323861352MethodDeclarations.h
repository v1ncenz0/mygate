﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_login
struct init_login_t3323861352;

#include "codegen/il2cpp-codegen.h"

// System.Void init_login::.ctor()
extern "C"  void init_login__ctor_m1284431853 (init_login_t3323861352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_login::Start()
extern "C"  void init_login_Start_m229286069 (init_login_t3323861352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_login::<Start>m__0(System.Boolean)
extern "C"  void init_login_U3CStartU3Em__0_m960785237 (Il2CppObject * __this /* static, unused */, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
