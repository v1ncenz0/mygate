﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemScene
struct  ItemScene_t1151284345  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemScene::id
	int32_t ___id_2;
	// System.String ItemScene::name
	String_t* ___name_3;
	// UnityEngine.Texture ItemScene::image_bg
	Texture_t2243626319 * ___image_bg_4;
	// UnityEngine.Texture ItemScene::image_floor
	Texture_t2243626319 * ___image_floor_5;
	// UnityEngine.Texture ItemScene::texture_wall
	Texture_t2243626319 * ___texture_wall_6;
	// UnityEngine.Texture ItemScene::texture_column
	Texture_t2243626319 * ___texture_column_7;
	// System.String ItemScene::image_bg_path
	String_t* ___image_bg_path_8;
	// System.String ItemScene::image_floor_path
	String_t* ___image_floor_path_9;
	// System.String ItemScene::texture_wall_path
	String_t* ___texture_wall_path_10;
	// System.String ItemScene::texture_column_path
	String_t* ___texture_column_path_11;
	// System.Int32 ItemScene::camera_rotate
	int32_t ___camera_rotate_12;
	// UnityEngine.Vector3 ItemScene::lamp_rotate
	Vector3_t2243707580  ___lamp_rotate_13;
	// System.Single ItemScene::lamp_intensity
	float ___lamp_intensity_14;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_image_bg_4() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___image_bg_4)); }
	inline Texture_t2243626319 * get_image_bg_4() const { return ___image_bg_4; }
	inline Texture_t2243626319 ** get_address_of_image_bg_4() { return &___image_bg_4; }
	inline void set_image_bg_4(Texture_t2243626319 * value)
	{
		___image_bg_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_bg_4, value);
	}

	inline static int32_t get_offset_of_image_floor_5() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___image_floor_5)); }
	inline Texture_t2243626319 * get_image_floor_5() const { return ___image_floor_5; }
	inline Texture_t2243626319 ** get_address_of_image_floor_5() { return &___image_floor_5; }
	inline void set_image_floor_5(Texture_t2243626319 * value)
	{
		___image_floor_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_floor_5, value);
	}

	inline static int32_t get_offset_of_texture_wall_6() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___texture_wall_6)); }
	inline Texture_t2243626319 * get_texture_wall_6() const { return ___texture_wall_6; }
	inline Texture_t2243626319 ** get_address_of_texture_wall_6() { return &___texture_wall_6; }
	inline void set_texture_wall_6(Texture_t2243626319 * value)
	{
		___texture_wall_6 = value;
		Il2CppCodeGenWriteBarrier(&___texture_wall_6, value);
	}

	inline static int32_t get_offset_of_texture_column_7() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___texture_column_7)); }
	inline Texture_t2243626319 * get_texture_column_7() const { return ___texture_column_7; }
	inline Texture_t2243626319 ** get_address_of_texture_column_7() { return &___texture_column_7; }
	inline void set_texture_column_7(Texture_t2243626319 * value)
	{
		___texture_column_7 = value;
		Il2CppCodeGenWriteBarrier(&___texture_column_7, value);
	}

	inline static int32_t get_offset_of_image_bg_path_8() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___image_bg_path_8)); }
	inline String_t* get_image_bg_path_8() const { return ___image_bg_path_8; }
	inline String_t** get_address_of_image_bg_path_8() { return &___image_bg_path_8; }
	inline void set_image_bg_path_8(String_t* value)
	{
		___image_bg_path_8 = value;
		Il2CppCodeGenWriteBarrier(&___image_bg_path_8, value);
	}

	inline static int32_t get_offset_of_image_floor_path_9() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___image_floor_path_9)); }
	inline String_t* get_image_floor_path_9() const { return ___image_floor_path_9; }
	inline String_t** get_address_of_image_floor_path_9() { return &___image_floor_path_9; }
	inline void set_image_floor_path_9(String_t* value)
	{
		___image_floor_path_9 = value;
		Il2CppCodeGenWriteBarrier(&___image_floor_path_9, value);
	}

	inline static int32_t get_offset_of_texture_wall_path_10() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___texture_wall_path_10)); }
	inline String_t* get_texture_wall_path_10() const { return ___texture_wall_path_10; }
	inline String_t** get_address_of_texture_wall_path_10() { return &___texture_wall_path_10; }
	inline void set_texture_wall_path_10(String_t* value)
	{
		___texture_wall_path_10 = value;
		Il2CppCodeGenWriteBarrier(&___texture_wall_path_10, value);
	}

	inline static int32_t get_offset_of_texture_column_path_11() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___texture_column_path_11)); }
	inline String_t* get_texture_column_path_11() const { return ___texture_column_path_11; }
	inline String_t** get_address_of_texture_column_path_11() { return &___texture_column_path_11; }
	inline void set_texture_column_path_11(String_t* value)
	{
		___texture_column_path_11 = value;
		Il2CppCodeGenWriteBarrier(&___texture_column_path_11, value);
	}

	inline static int32_t get_offset_of_camera_rotate_12() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___camera_rotate_12)); }
	inline int32_t get_camera_rotate_12() const { return ___camera_rotate_12; }
	inline int32_t* get_address_of_camera_rotate_12() { return &___camera_rotate_12; }
	inline void set_camera_rotate_12(int32_t value)
	{
		___camera_rotate_12 = value;
	}

	inline static int32_t get_offset_of_lamp_rotate_13() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___lamp_rotate_13)); }
	inline Vector3_t2243707580  get_lamp_rotate_13() const { return ___lamp_rotate_13; }
	inline Vector3_t2243707580 * get_address_of_lamp_rotate_13() { return &___lamp_rotate_13; }
	inline void set_lamp_rotate_13(Vector3_t2243707580  value)
	{
		___lamp_rotate_13 = value;
	}

	inline static int32_t get_offset_of_lamp_intensity_14() { return static_cast<int32_t>(offsetof(ItemScene_t1151284345, ___lamp_intensity_14)); }
	inline float get_lamp_intensity_14() const { return ___lamp_intensity_14; }
	inline float* get_address_of_lamp_intensity_14() { return &___lamp_intensity_14; }
	inline void set_lamp_intensity_14(float value)
	{
		___lamp_intensity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
