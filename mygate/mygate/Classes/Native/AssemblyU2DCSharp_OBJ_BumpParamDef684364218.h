﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/BumpParamDef
struct  BumpParamDef_t684364218  : public Il2CppObject
{
public:
	// System.String OBJ/BumpParamDef::optionName
	String_t* ___optionName_0;
	// System.String OBJ/BumpParamDef::valueType
	String_t* ___valueType_1;
	// System.Int32 OBJ/BumpParamDef::valueNumMin
	int32_t ___valueNumMin_2;
	// System.Int32 OBJ/BumpParamDef::valueNumMax
	int32_t ___valueNumMax_3;

public:
	inline static int32_t get_offset_of_optionName_0() { return static_cast<int32_t>(offsetof(BumpParamDef_t684364218, ___optionName_0)); }
	inline String_t* get_optionName_0() const { return ___optionName_0; }
	inline String_t** get_address_of_optionName_0() { return &___optionName_0; }
	inline void set_optionName_0(String_t* value)
	{
		___optionName_0 = value;
		Il2CppCodeGenWriteBarrier(&___optionName_0, value);
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(BumpParamDef_t684364218, ___valueType_1)); }
	inline String_t* get_valueType_1() const { return ___valueType_1; }
	inline String_t** get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(String_t* value)
	{
		___valueType_1 = value;
		Il2CppCodeGenWriteBarrier(&___valueType_1, value);
	}

	inline static int32_t get_offset_of_valueNumMin_2() { return static_cast<int32_t>(offsetof(BumpParamDef_t684364218, ___valueNumMin_2)); }
	inline int32_t get_valueNumMin_2() const { return ___valueNumMin_2; }
	inline int32_t* get_address_of_valueNumMin_2() { return &___valueNumMin_2; }
	inline void set_valueNumMin_2(int32_t value)
	{
		___valueNumMin_2 = value;
	}

	inline static int32_t get_offset_of_valueNumMax_3() { return static_cast<int32_t>(offsetof(BumpParamDef_t684364218, ___valueNumMax_3)); }
	inline int32_t get_valueNumMax_3() const { return ___valueNumMax_3; }
	inline int32_t* get_address_of_valueNumMax_3() { return &___valueNumMax_3; }
	inline void set_valueNumMax_3(int32_t value)
	{
		___valueNumMax_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
