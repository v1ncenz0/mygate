﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utility
struct Utility_t109704896;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Utility::.ctor()
extern "C"  void Utility__ctor_m2866738967 (Utility_t109704896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Utility::GCD(System.Int32,System.Int32)
extern "C"  int32_t Utility_GCD_m2603423149 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Utility::GCDofList(System.Int32[])
extern "C"  int32_t Utility_GCDofList_m2184019961 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utility::Base64Encode(System.String)
extern "C"  String_t* Utility_Base64Encode_m2525403017 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utility::Base64Decode(System.String)
extern "C"  String_t* Utility_Base64Decode_m366177107 (Il2CppObject * __this /* static, unused */, String_t* ___base64EncodedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utility::convertIntToBool(System.String)
extern "C"  bool Utility_convertIntToBool_m3355948606 (Il2CppObject * __this /* static, unused */, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Utility::ConvertPointToWorld(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Utility_ConvertPointToWorld_m2430895868 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utility::ConvertDBDate(System.String)
extern "C"  String_t* Utility_ConvertDBDate_m3407435645 (Il2CppObject * __this /* static, unused */, String_t* ___dbdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utility::ConvertHtmlToText(System.String)
extern "C"  String_t* Utility_ConvertHtmlToText_m1972177572 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
