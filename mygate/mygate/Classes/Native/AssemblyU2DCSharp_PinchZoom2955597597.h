﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PinchZoom
struct  PinchZoom_t2955597597  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PinchZoom::perspectiveZoomSpeed
	float ___perspectiveZoomSpeed_2;
	// System.Single PinchZoom::orthoZoomSpeed
	float ___orthoZoomSpeed_3;

public:
	inline static int32_t get_offset_of_perspectiveZoomSpeed_2() { return static_cast<int32_t>(offsetof(PinchZoom_t2955597597, ___perspectiveZoomSpeed_2)); }
	inline float get_perspectiveZoomSpeed_2() const { return ___perspectiveZoomSpeed_2; }
	inline float* get_address_of_perspectiveZoomSpeed_2() { return &___perspectiveZoomSpeed_2; }
	inline void set_perspectiveZoomSpeed_2(float value)
	{
		___perspectiveZoomSpeed_2 = value;
	}

	inline static int32_t get_offset_of_orthoZoomSpeed_3() { return static_cast<int32_t>(offsetof(PinchZoom_t2955597597, ___orthoZoomSpeed_3)); }
	inline float get_orthoZoomSpeed_3() const { return ___orthoZoomSpeed_3; }
	inline float* get_address_of_orthoZoomSpeed_3() { return &___orthoZoomSpeed_3; }
	inline void set_orthoZoomSpeed_3(float value)
	{
		___orthoZoomSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
