﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1853968839(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t643048857 *, Vector3_t2243707580 , List_1_t1440998580 *, const MethodInfo*))KeyValuePair_2__ctor_m3944620164_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::get_Key()
#define KeyValuePair_2_get_Key_m1540998805(__this, method) ((  Vector3_t2243707580  (*) (KeyValuePair_2_t643048857 *, const MethodInfo*))KeyValuePair_2_get_Key_m2722691310_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1780643624(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t643048857 *, Vector3_t2243707580 , const MethodInfo*))KeyValuePair_2_set_Key_m1626646011_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::get_Value()
#define KeyValuePair_2_get_Value_m4119334965(__this, method) ((  List_1_t1440998580 * (*) (KeyValuePair_2_t643048857 *, const MethodInfo*))KeyValuePair_2_get_Value_m2121445166_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4234199568(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t643048857 *, List_1_t1440998580 *, const MethodInfo*))KeyValuePair_2_set_Value_m2031621555_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::ToString()
#define KeyValuePair_2_ToString_m1733449456(__this, method) ((  String_t* (*) (KeyValuePair_2_t643048857 *, const MethodInfo*))KeyValuePair_2_ToString_m3365230589_gshared)(__this, method)
