﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Spline_InterpolationMode2569420497.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemGeometry
struct  ItemGeometry_t2838964733  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemGeometry::id
	int32_t ___id_2;
	// System.String ItemGeometry::name
	String_t* ___name_3;
	// UnityEngine.Sprite ItemGeometry::image
	Sprite_t309593783 * ___image_4;
	// System.String ItemGeometry::image_path
	String_t* ___image_path_5;
	// UnityEngine.Vector2[] ItemGeometry::relativePositions
	Vector2U5BU5D_t686124026* ___relativePositions_6;
	// Spline/InterpolationMode ItemGeometry::interpolationMode
	int32_t ___interpolationMode_7;
	// System.Boolean ItemGeometry::showBottomBar
	bool ___showBottomBar_8;
	// System.Boolean ItemGeometry::showStructure
	bool ___showStructure_9;
	// System.String ItemGeometry::type_curve
	String_t* ___type_curve_10;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___image_4)); }
	inline Sprite_t309593783 * get_image_4() const { return ___image_4; }
	inline Sprite_t309593783 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Sprite_t309593783 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_image_path_5() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___image_path_5)); }
	inline String_t* get_image_path_5() const { return ___image_path_5; }
	inline String_t** get_address_of_image_path_5() { return &___image_path_5; }
	inline void set_image_path_5(String_t* value)
	{
		___image_path_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_5, value);
	}

	inline static int32_t get_offset_of_relativePositions_6() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___relativePositions_6)); }
	inline Vector2U5BU5D_t686124026* get_relativePositions_6() const { return ___relativePositions_6; }
	inline Vector2U5BU5D_t686124026** get_address_of_relativePositions_6() { return &___relativePositions_6; }
	inline void set_relativePositions_6(Vector2U5BU5D_t686124026* value)
	{
		___relativePositions_6 = value;
		Il2CppCodeGenWriteBarrier(&___relativePositions_6, value);
	}

	inline static int32_t get_offset_of_interpolationMode_7() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___interpolationMode_7)); }
	inline int32_t get_interpolationMode_7() const { return ___interpolationMode_7; }
	inline int32_t* get_address_of_interpolationMode_7() { return &___interpolationMode_7; }
	inline void set_interpolationMode_7(int32_t value)
	{
		___interpolationMode_7 = value;
	}

	inline static int32_t get_offset_of_showBottomBar_8() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___showBottomBar_8)); }
	inline bool get_showBottomBar_8() const { return ___showBottomBar_8; }
	inline bool* get_address_of_showBottomBar_8() { return &___showBottomBar_8; }
	inline void set_showBottomBar_8(bool value)
	{
		___showBottomBar_8 = value;
	}

	inline static int32_t get_offset_of_showStructure_9() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___showStructure_9)); }
	inline bool get_showStructure_9() const { return ___showStructure_9; }
	inline bool* get_address_of_showStructure_9() { return &___showStructure_9; }
	inline void set_showStructure_9(bool value)
	{
		___showStructure_9 = value;
	}

	inline static int32_t get_offset_of_type_curve_10() { return static_cast<int32_t>(offsetof(ItemGeometry_t2838964733, ___type_curve_10)); }
	inline String_t* get_type_curve_10() const { return ___type_curve_10; }
	inline String_t** get_address_of_type_curve_10() { return &___type_curve_10; }
	inline void set_type_curve_10(String_t* value)
	{
		___type_curve_10 = value;
		Il2CppCodeGenWriteBarrier(&___type_curve_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
