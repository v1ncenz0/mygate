﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineAutoAlign
struct  SplineAutoAlign_t1803875161  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.LayerMask SplineAutoAlign::raycastLayers
	LayerMask_t3188175821  ___raycastLayers_2;
	// System.Single SplineAutoAlign::offset
	float ___offset_3;
	// System.String[] SplineAutoAlign::ignoreTags
	StringU5BU5D_t1642385972* ___ignoreTags_4;
	// UnityEngine.Vector3 SplineAutoAlign::raycastDirection
	Vector3_t2243707580  ___raycastDirection_5;

public:
	inline static int32_t get_offset_of_raycastLayers_2() { return static_cast<int32_t>(offsetof(SplineAutoAlign_t1803875161, ___raycastLayers_2)); }
	inline LayerMask_t3188175821  get_raycastLayers_2() const { return ___raycastLayers_2; }
	inline LayerMask_t3188175821 * get_address_of_raycastLayers_2() { return &___raycastLayers_2; }
	inline void set_raycastLayers_2(LayerMask_t3188175821  value)
	{
		___raycastLayers_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(SplineAutoAlign_t1803875161, ___offset_3)); }
	inline float get_offset_3() const { return ___offset_3; }
	inline float* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(float value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_ignoreTags_4() { return static_cast<int32_t>(offsetof(SplineAutoAlign_t1803875161, ___ignoreTags_4)); }
	inline StringU5BU5D_t1642385972* get_ignoreTags_4() const { return ___ignoreTags_4; }
	inline StringU5BU5D_t1642385972** get_address_of_ignoreTags_4() { return &___ignoreTags_4; }
	inline void set_ignoreTags_4(StringU5BU5D_t1642385972* value)
	{
		___ignoreTags_4 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreTags_4, value);
	}

	inline static int32_t get_offset_of_raycastDirection_5() { return static_cast<int32_t>(offsetof(SplineAutoAlign_t1803875161, ___raycastDirection_5)); }
	inline Vector3_t2243707580  get_raycastDirection_5() const { return ___raycastDirection_5; }
	inline Vector3_t2243707580 * get_address_of_raycastDirection_5() { return &___raycastDirection_5; }
	inline void set_raycastDirection_5(Vector3_t2243707580  value)
	{
		___raycastDirection_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
