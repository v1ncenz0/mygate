﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GeometryBuffer
struct GeometryBuffer_t1734374890;
// System.Threading.Thread
struct Thread_t241561612;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<OBJ/MaterialData>
struct List_1_t2921024477;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ
struct  OBJ_t3241282509  : public MonoBehaviour_t1158329972
{
public:
	// System.String OBJ::objPath
	String_t* ___objPath_2;
	// System.Boolean OBJ::loadOnStart
	bool ___loadOnStart_3;
	// System.String OBJ::basepath
	String_t* ___basepath_26;
	// System.String OBJ::mtllib
	String_t* ___mtllib_27;
	// GeometryBuffer OBJ::buffer
	GeometryBuffer_t1734374890 * ___buffer_28;
	// System.Boolean OBJ::isDebug
	bool ___isDebug_29;
	// System.Boolean OBJ::isFileParsingCompleted
	bool ___isFileParsingCompleted_30;
	// System.Boolean OBJ::isAfterFileParsingCoroutineStarted
	bool ___isAfterFileParsingCoroutineStarted_31;
	// System.Boolean OBJ::finish
	bool ___finish_32;
	// System.Threading.Thread OBJ::_fileParserThread
	Thread_t241561612 * ____fileParserThread_33;
	// System.String[] OBJ::_fileLines
	StringU5BU5D_t1642385972* ____fileLines_34;
	// System.Collections.Generic.List`1<OBJ/MaterialData> OBJ::materialData
	List_1_t2921024477 * ___materialData_35;

public:
	inline static int32_t get_offset_of_objPath_2() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___objPath_2)); }
	inline String_t* get_objPath_2() const { return ___objPath_2; }
	inline String_t** get_address_of_objPath_2() { return &___objPath_2; }
	inline void set_objPath_2(String_t* value)
	{
		___objPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___objPath_2, value);
	}

	inline static int32_t get_offset_of_loadOnStart_3() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___loadOnStart_3)); }
	inline bool get_loadOnStart_3() const { return ___loadOnStart_3; }
	inline bool* get_address_of_loadOnStart_3() { return &___loadOnStart_3; }
	inline void set_loadOnStart_3(bool value)
	{
		___loadOnStart_3 = value;
	}

	inline static int32_t get_offset_of_basepath_26() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___basepath_26)); }
	inline String_t* get_basepath_26() const { return ___basepath_26; }
	inline String_t** get_address_of_basepath_26() { return &___basepath_26; }
	inline void set_basepath_26(String_t* value)
	{
		___basepath_26 = value;
		Il2CppCodeGenWriteBarrier(&___basepath_26, value);
	}

	inline static int32_t get_offset_of_mtllib_27() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___mtllib_27)); }
	inline String_t* get_mtllib_27() const { return ___mtllib_27; }
	inline String_t** get_address_of_mtllib_27() { return &___mtllib_27; }
	inline void set_mtllib_27(String_t* value)
	{
		___mtllib_27 = value;
		Il2CppCodeGenWriteBarrier(&___mtllib_27, value);
	}

	inline static int32_t get_offset_of_buffer_28() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___buffer_28)); }
	inline GeometryBuffer_t1734374890 * get_buffer_28() const { return ___buffer_28; }
	inline GeometryBuffer_t1734374890 ** get_address_of_buffer_28() { return &___buffer_28; }
	inline void set_buffer_28(GeometryBuffer_t1734374890 * value)
	{
		___buffer_28 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_28, value);
	}

	inline static int32_t get_offset_of_isDebug_29() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___isDebug_29)); }
	inline bool get_isDebug_29() const { return ___isDebug_29; }
	inline bool* get_address_of_isDebug_29() { return &___isDebug_29; }
	inline void set_isDebug_29(bool value)
	{
		___isDebug_29 = value;
	}

	inline static int32_t get_offset_of_isFileParsingCompleted_30() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___isFileParsingCompleted_30)); }
	inline bool get_isFileParsingCompleted_30() const { return ___isFileParsingCompleted_30; }
	inline bool* get_address_of_isFileParsingCompleted_30() { return &___isFileParsingCompleted_30; }
	inline void set_isFileParsingCompleted_30(bool value)
	{
		___isFileParsingCompleted_30 = value;
	}

	inline static int32_t get_offset_of_isAfterFileParsingCoroutineStarted_31() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___isAfterFileParsingCoroutineStarted_31)); }
	inline bool get_isAfterFileParsingCoroutineStarted_31() const { return ___isAfterFileParsingCoroutineStarted_31; }
	inline bool* get_address_of_isAfterFileParsingCoroutineStarted_31() { return &___isAfterFileParsingCoroutineStarted_31; }
	inline void set_isAfterFileParsingCoroutineStarted_31(bool value)
	{
		___isAfterFileParsingCoroutineStarted_31 = value;
	}

	inline static int32_t get_offset_of_finish_32() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___finish_32)); }
	inline bool get_finish_32() const { return ___finish_32; }
	inline bool* get_address_of_finish_32() { return &___finish_32; }
	inline void set_finish_32(bool value)
	{
		___finish_32 = value;
	}

	inline static int32_t get_offset_of__fileParserThread_33() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ____fileParserThread_33)); }
	inline Thread_t241561612 * get__fileParserThread_33() const { return ____fileParserThread_33; }
	inline Thread_t241561612 ** get_address_of__fileParserThread_33() { return &____fileParserThread_33; }
	inline void set__fileParserThread_33(Thread_t241561612 * value)
	{
		____fileParserThread_33 = value;
		Il2CppCodeGenWriteBarrier(&____fileParserThread_33, value);
	}

	inline static int32_t get_offset_of__fileLines_34() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ____fileLines_34)); }
	inline StringU5BU5D_t1642385972* get__fileLines_34() const { return ____fileLines_34; }
	inline StringU5BU5D_t1642385972** get_address_of__fileLines_34() { return &____fileLines_34; }
	inline void set__fileLines_34(StringU5BU5D_t1642385972* value)
	{
		____fileLines_34 = value;
		Il2CppCodeGenWriteBarrier(&____fileLines_34, value);
	}

	inline static int32_t get_offset_of_materialData_35() { return static_cast<int32_t>(offsetof(OBJ_t3241282509, ___materialData_35)); }
	inline List_1_t2921024477 * get_materialData_35() const { return ___materialData_35; }
	inline List_1_t2921024477 ** get_address_of_materialData_35() { return &___materialData_35; }
	inline void set_materialData_35(List_1_t2921024477 * value)
	{
		___materialData_35 = value;
		Il2CppCodeGenWriteBarrier(&___materialData_35, value);
	}
};

struct OBJ_t3241282509_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OBJ::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OBJ::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_37;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_36() { return static_cast<int32_t>(offsetof(OBJ_t3241282509_StaticFields, ___U3CU3Ef__switchU24map0_36)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_36() const { return ___U3CU3Ef__switchU24map0_36; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_36() { return &___U3CU3Ef__switchU24map0_36; }
	inline void set_U3CU3Ef__switchU24map0_36(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_37() { return static_cast<int32_t>(offsetof(OBJ_t3241282509_StaticFields, ___U3CU3Ef__switchU24map1_37)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_37() const { return ___U3CU3Ef__switchU24map1_37; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_37() { return &___U3CU3Ef__switchU24map1_37; }
	inline void set_U3CU3Ef__switchU24map1_37(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
