﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineMesh/MeshData
struct  MeshData_t1171048948  : public Il2CppObject
{
public:
	// UnityEngine.Vector3[] SplineMesh/MeshData::vertices
	Vector3U5BU5D_t1172311765* ___vertices_0;
	// UnityEngine.Vector2[] SplineMesh/MeshData::uvCoord
	Vector2U5BU5D_t686124026* ___uvCoord_1;
	// UnityEngine.Vector3[] SplineMesh/MeshData::normals
	Vector3U5BU5D_t1172311765* ___normals_2;
	// UnityEngine.Vector4[] SplineMesh/MeshData::tangents
	Vector4U5BU5D_t1658499504* ___tangents_3;
	// System.Int32[] SplineMesh/MeshData::triangles
	Int32U5BU5D_t3030399641* ___triangles_4;
	// UnityEngine.Bounds SplineMesh/MeshData::bounds
	Bounds_t3033363703  ___bounds_5;
	// System.Int32 SplineMesh/MeshData::currentTriangleIndex
	int32_t ___currentTriangleIndex_6;
	// System.Int32 SplineMesh/MeshData::currentVertexIndex
	int32_t ___currentVertexIndex_7;
	// System.Boolean SplineMesh/MeshData::HasNormals
	bool ___HasNormals_8;
	// System.Boolean SplineMesh/MeshData::HasTangents
	bool ___HasTangents_9;
	// UnityEngine.Mesh SplineMesh/MeshData::referencedMesh
	Mesh_t1356156583 * ___referencedMesh_10;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___vertices_0)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_0() const { return ___vertices_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_0, value);
	}

	inline static int32_t get_offset_of_uvCoord_1() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___uvCoord_1)); }
	inline Vector2U5BU5D_t686124026* get_uvCoord_1() const { return ___uvCoord_1; }
	inline Vector2U5BU5D_t686124026** get_address_of_uvCoord_1() { return &___uvCoord_1; }
	inline void set_uvCoord_1(Vector2U5BU5D_t686124026* value)
	{
		___uvCoord_1 = value;
		Il2CppCodeGenWriteBarrier(&___uvCoord_1, value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___normals_2)); }
	inline Vector3U5BU5D_t1172311765* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1172311765** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1172311765* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier(&___normals_2, value);
	}

	inline static int32_t get_offset_of_tangents_3() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___tangents_3)); }
	inline Vector4U5BU5D_t1658499504* get_tangents_3() const { return ___tangents_3; }
	inline Vector4U5BU5D_t1658499504** get_address_of_tangents_3() { return &___tangents_3; }
	inline void set_tangents_3(Vector4U5BU5D_t1658499504* value)
	{
		___tangents_3 = value;
		Il2CppCodeGenWriteBarrier(&___tangents_3, value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___triangles_4)); }
	inline Int32U5BU5D_t3030399641* get_triangles_4() const { return ___triangles_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(Int32U5BU5D_t3030399641* value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_4, value);
	}

	inline static int32_t get_offset_of_bounds_5() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___bounds_5)); }
	inline Bounds_t3033363703  get_bounds_5() const { return ___bounds_5; }
	inline Bounds_t3033363703 * get_address_of_bounds_5() { return &___bounds_5; }
	inline void set_bounds_5(Bounds_t3033363703  value)
	{
		___bounds_5 = value;
	}

	inline static int32_t get_offset_of_currentTriangleIndex_6() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___currentTriangleIndex_6)); }
	inline int32_t get_currentTriangleIndex_6() const { return ___currentTriangleIndex_6; }
	inline int32_t* get_address_of_currentTriangleIndex_6() { return &___currentTriangleIndex_6; }
	inline void set_currentTriangleIndex_6(int32_t value)
	{
		___currentTriangleIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentVertexIndex_7() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___currentVertexIndex_7)); }
	inline int32_t get_currentVertexIndex_7() const { return ___currentVertexIndex_7; }
	inline int32_t* get_address_of_currentVertexIndex_7() { return &___currentVertexIndex_7; }
	inline void set_currentVertexIndex_7(int32_t value)
	{
		___currentVertexIndex_7 = value;
	}

	inline static int32_t get_offset_of_HasNormals_8() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___HasNormals_8)); }
	inline bool get_HasNormals_8() const { return ___HasNormals_8; }
	inline bool* get_address_of_HasNormals_8() { return &___HasNormals_8; }
	inline void set_HasNormals_8(bool value)
	{
		___HasNormals_8 = value;
	}

	inline static int32_t get_offset_of_HasTangents_9() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___HasTangents_9)); }
	inline bool get_HasTangents_9() const { return ___HasTangents_9; }
	inline bool* get_address_of_HasTangents_9() { return &___HasTangents_9; }
	inline void set_HasTangents_9(bool value)
	{
		___HasTangents_9 = value;
	}

	inline static int32_t get_offset_of_referencedMesh_10() { return static_cast<int32_t>(offsetof(MeshData_t1171048948, ___referencedMesh_10)); }
	inline Mesh_t1356156583 * get_referencedMesh_10() const { return ___referencedMesh_10; }
	inline Mesh_t1356156583 ** get_address_of_referencedMesh_10() { return &___referencedMesh_10; }
	inline void set_referencedMesh_10(Mesh_t1356156583 * value)
	{
		___referencedMesh_10 = value;
		Il2CppCodeGenWriteBarrier(&___referencedMesh_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
