﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_main
struct init_main_t2270456518;
// ItemGate
struct ItemGate_t2548129788;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemGate2548129788.h"

// System.Void init_main::.ctor()
extern "C"  void init_main__ctor_m3302480237 (init_main_t2270456518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_main::Start()
extern "C"  void init_main_Start_m3557503885 (init_main_t2270456518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_main::InitGate(ItemGate)
extern "C"  void init_main_InitGate_m3512787592 (init_main_t2270456518 * __this, ItemGate_t2548129788 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
