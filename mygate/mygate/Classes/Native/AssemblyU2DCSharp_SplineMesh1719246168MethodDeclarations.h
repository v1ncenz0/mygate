﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineMesh
struct SplineMesh_t1719246168;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// SplineMesh/MeshData
struct MeshData_t1171048948;
// SplineMeshModifier[]
struct SplineMeshModifierU5BU5D_t2295569486;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SplineMesh_MeshData1171048948.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void SplineMesh::.ctor()
extern "C"  void SplineMesh__ctor_m2860245489 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh SplineMesh::get_BentMesh()
extern "C"  Mesh_t1356156583 * SplineMesh_get_BentMesh_m4213038565 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SplineMesh::get_IsSubSegment()
extern "C"  bool SplineMesh_get_IsSubSegment_m1996847173 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::Start()
extern "C"  void SplineMesh_Start_m1535962873 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::OnEnable()
extern "C"  void SplineMesh_OnEnable_m914384217 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::LateUpdate()
extern "C"  void SplineMesh_LateUpdate_m1319904886 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::UpdateMesh()
extern "C"  void SplineMesh_UpdateMesh_m3254206143 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::BendMesh(System.Single,System.Single,SplineMesh/MeshData,SplineMesh/MeshData,SplineMeshModifier[])
extern "C"  void SplineMesh_BendMesh_m2378768670 (SplineMesh_t1719246168 * __this, float ___param00, float ___param11, MeshData_t1171048948 * ___meshDataBase2, MeshData_t1171048948 * ___meshDataNew3, SplineMeshModifierU5BU5D_t2295569486* ___meshModiefiers4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineMesh::FastRotation(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  SplineMesh_FastRotation_m2449383305 (SplineMesh_t1719246168 * __this, Quaternion_t4030073918  ___rotation0, Vector3_t2243707580  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::SetupMesh()
extern "C"  void SplineMesh_SetupMesh_m609221837 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh::SetupMeshBuffers()
extern "C"  void SplineMesh_SetupMeshBuffers_m3521694424 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh SplineMesh::ReturnMeshReference()
extern "C"  Mesh_t1356156583 * SplineMesh_ReturnMeshReference_m2214954792 (SplineMesh_t1719246168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
