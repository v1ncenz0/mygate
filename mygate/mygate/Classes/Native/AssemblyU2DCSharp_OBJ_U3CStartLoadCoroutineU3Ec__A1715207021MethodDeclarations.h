﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/<StartLoadCoroutine>c__AnonStorey2
struct U3CStartLoadCoroutineU3Ec__AnonStorey2_t1715207021;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/<StartLoadCoroutine>c__AnonStorey2::.ctor()
extern "C"  void U3CStartLoadCoroutineU3Ec__AnonStorey2__ctor_m2811956658 (U3CStartLoadCoroutineU3Ec__AnonStorey2_t1715207021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<StartLoadCoroutine>c__AnonStorey2::<>m__0(System.Boolean)
extern "C"  void U3CStartLoadCoroutineU3Ec__AnonStorey2_U3CU3Em__0_m3602911536 (U3CStartLoadCoroutineU3Ec__AnonStorey2_t1715207021 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
