﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2750251834.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2682092669_gshared (InternalEnumerator_1_t2750251834 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2682092669(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2750251834 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2682092669_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m292109977_gshared (InternalEnumerator_1_t2750251834 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m292109977(__this, method) ((  void (*) (InternalEnumerator_1_t2750251834 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m292109977_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197881777_gshared (InternalEnumerator_1_t2750251834 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197881777(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2750251834 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197881777_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3767298454_gshared (InternalEnumerator_1_t2750251834 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3767298454(__this, method) ((  void (*) (InternalEnumerator_1_t2750251834 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3767298454_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1079024249_gshared (InternalEnumerator_1_t2750251834 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1079024249(__this, method) ((  bool (*) (InternalEnumerator_1_t2750251834 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1079024249_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1891499572  InternalEnumerator_1_get_Current_m914179788_gshared (InternalEnumerator_1_t2750251834 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m914179788(__this, method) ((  KeyValuePair_2_t1891499572  (*) (InternalEnumerator_1_t2750251834 *, const MethodInfo*))InternalEnumerator_1_get_Current_m914179788_gshared)(__this, method)
