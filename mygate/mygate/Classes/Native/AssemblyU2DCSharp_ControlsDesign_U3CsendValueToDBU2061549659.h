﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ControlsDesign
struct ControlsDesign_t1600104368;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<sendValueToDB>c__Iterator1
struct  U3CsendValueToDBU3Ec__Iterator1_t2061549659  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm ControlsDesign/<sendValueToDB>c__Iterator1::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// System.Boolean ControlsDesign/<sendValueToDB>c__Iterator1::withoutScreen
	bool ___withoutScreen_1;
	// System.String ControlsDesign/<sendValueToDB>c__Iterator1::<screen>__1
	String_t* ___U3CscreenU3E__1_2;
	// System.String ControlsDesign/<sendValueToDB>c__Iterator1::json
	String_t* ___json_3;
	// System.Byte[] ControlsDesign/<sendValueToDB>c__Iterator1::<json_byte>__2
	ByteU5BU5D_t3397334013* ___U3Cjson_byteU3E__2_4;
	// System.Byte[] ControlsDesign/<sendValueToDB>c__Iterator1::<rawData>__3
	ByteU5BU5D_t3397334013* ___U3CrawDataU3E__3_5;
	// System.String ControlsDesign/<sendValueToDB>c__Iterator1::<url>__4
	String_t* ___U3CurlU3E__4_6;
	// UnityEngine.WWW ControlsDesign/<sendValueToDB>c__Iterator1::<www>__5
	WWW_t2919945039 * ___U3CwwwU3E__5_7;
	// System.Action`1<System.Int32> ControlsDesign/<sendValueToDB>c__Iterator1::saved
	Action_1_t1873676830 * ___saved_8;
	// System.Action`1<System.Boolean> ControlsDesign/<sendValueToDB>c__Iterator1::error
	Action_1_t3627374100 * ___error_9;
	// ControlsDesign ControlsDesign/<sendValueToDB>c__Iterator1::$this
	ControlsDesign_t1600104368 * ___U24this_10;
	// System.Object ControlsDesign/<sendValueToDB>c__Iterator1::$current
	Il2CppObject * ___U24current_11;
	// System.Boolean ControlsDesign/<sendValueToDB>c__Iterator1::$disposing
	bool ___U24disposing_12;
	// System.Int32 ControlsDesign/<sendValueToDB>c__Iterator1::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_withoutScreen_1() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___withoutScreen_1)); }
	inline bool get_withoutScreen_1() const { return ___withoutScreen_1; }
	inline bool* get_address_of_withoutScreen_1() { return &___withoutScreen_1; }
	inline void set_withoutScreen_1(bool value)
	{
		___withoutScreen_1 = value;
	}

	inline static int32_t get_offset_of_U3CscreenU3E__1_2() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3CscreenU3E__1_2)); }
	inline String_t* get_U3CscreenU3E__1_2() const { return ___U3CscreenU3E__1_2; }
	inline String_t** get_address_of_U3CscreenU3E__1_2() { return &___U3CscreenU3E__1_2; }
	inline void set_U3CscreenU3E__1_2(String_t* value)
	{
		___U3CscreenU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenU3E__1_2, value);
	}

	inline static int32_t get_offset_of_json_3() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___json_3)); }
	inline String_t* get_json_3() const { return ___json_3; }
	inline String_t** get_address_of_json_3() { return &___json_3; }
	inline void set_json_3(String_t* value)
	{
		___json_3 = value;
		Il2CppCodeGenWriteBarrier(&___json_3, value);
	}

	inline static int32_t get_offset_of_U3Cjson_byteU3E__2_4() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3Cjson_byteU3E__2_4)); }
	inline ByteU5BU5D_t3397334013* get_U3Cjson_byteU3E__2_4() const { return ___U3Cjson_byteU3E__2_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3Cjson_byteU3E__2_4() { return &___U3Cjson_byteU3E__2_4; }
	inline void set_U3Cjson_byteU3E__2_4(ByteU5BU5D_t3397334013* value)
	{
		___U3Cjson_byteU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cjson_byteU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CrawDataU3E__3_5() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3CrawDataU3E__3_5)); }
	inline ByteU5BU5D_t3397334013* get_U3CrawDataU3E__3_5() const { return ___U3CrawDataU3E__3_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CrawDataU3E__3_5() { return &___U3CrawDataU3E__3_5; }
	inline void set_U3CrawDataU3E__3_5(ByteU5BU5D_t3397334013* value)
	{
		___U3CrawDataU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrawDataU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__4_6() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3CurlU3E__4_6)); }
	inline String_t* get_U3CurlU3E__4_6() const { return ___U3CurlU3E__4_6; }
	inline String_t** get_address_of_U3CurlU3E__4_6() { return &___U3CurlU3E__4_6; }
	inline void set_U3CurlU3E__4_6(String_t* value)
	{
		___U3CurlU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__4_6, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__5_7() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U3CwwwU3E__5_7)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__5_7() const { return ___U3CwwwU3E__5_7; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__5_7() { return &___U3CwwwU3E__5_7; }
	inline void set_U3CwwwU3E__5_7(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__5_7, value);
	}

	inline static int32_t get_offset_of_saved_8() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___saved_8)); }
	inline Action_1_t1873676830 * get_saved_8() const { return ___saved_8; }
	inline Action_1_t1873676830 ** get_address_of_saved_8() { return &___saved_8; }
	inline void set_saved_8(Action_1_t1873676830 * value)
	{
		___saved_8 = value;
		Il2CppCodeGenWriteBarrier(&___saved_8, value);
	}

	inline static int32_t get_offset_of_error_9() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___error_9)); }
	inline Action_1_t3627374100 * get_error_9() const { return ___error_9; }
	inline Action_1_t3627374100 ** get_address_of_error_9() { return &___error_9; }
	inline void set_error_9(Action_1_t3627374100 * value)
	{
		___error_9 = value;
		Il2CppCodeGenWriteBarrier(&___error_9, value);
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U24this_10)); }
	inline ControlsDesign_t1600104368 * get_U24this_10() const { return ___U24this_10; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(ControlsDesign_t1600104368 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_10, value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CsendValueToDBU3Ec__Iterator1_t2061549659, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
