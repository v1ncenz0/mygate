﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Library
struct Library_t4121003545;
// ItemStyle
struct ItemStyle_t3530326308;
// ItemType
struct ItemType_t3455197591;
// ItemGeometry
struct ItemGeometry_t2838964733;

#include "codegen/il2cpp-codegen.h"

// System.Void Library::.ctor()
extern "C"  void Library__ctor_m3369429054 (Library_t4121003545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemStyle Library::searchStyle(System.Int32)
extern "C"  ItemStyle_t3530326308 * Library_searchStyle_m3621244569 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemType Library::searchType(System.Int32)
extern "C"  ItemType_t3455197591 * Library_searchType_m1953839349 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemGeometry Library::searchGeometry(System.Int32)
extern "C"  ItemGeometry_t2838964733 * Library_searchGeometry_m1151063477 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Library::.cctor()
extern "C"  void Library__cctor_m3409900707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
