﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>[]
struct KeyValuePair_2U5BU5D_t2451174760;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>>
struct IEnumerator_1_t779369560;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityEngine.Vector3>
struct KeyCollection_t3735030986;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>
struct ValueCollection_t4249560354;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23303845733.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2571557917.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::.ctor()
extern "C"  void Dictionary_2__ctor_m2666701246_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2666701246(__this, method) ((  void (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2__ctor_m2666701246_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2173977898_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2173977898(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2173977898_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1639499558_gshared (Dictionary_2_t1251533215 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1639499558(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1251533215 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1639499558_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m255911468_gshared (Dictionary_2_t1251533215 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m255911468(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1251533215 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m255911468_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1304782239_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1304782239(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1304782239_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1106352160_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1106352160(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1106352160_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3817667641_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3817667641(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3817667641_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1105083853_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1105083853(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1105083853_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1293840988_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1293840988(__this, ___key0, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1293840988_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2763595911_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2763595911(__this, method) ((  bool (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2763595911_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2334579539_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2334579539(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2334579539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m722843841_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m722843841(__this, method) ((  bool (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m722843841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1836719158_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2_t3303845733  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1836719158(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1251533215 *, KeyValuePair_2_t3303845733 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1836719158_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2331635550_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2_t3303845733  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2331635550(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1251533215 *, KeyValuePair_2_t3303845733 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2331635550_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078281658_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2U5BU5D_t2451174760* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078281658(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1251533215 *, KeyValuePair_2U5BU5D_t2451174760*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078281658_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3009633267_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2_t3303845733  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3009633267(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1251533215 *, KeyValuePair_2_t3303845733 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3009633267_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2276449399_gshared (Dictionary_2_t1251533215 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2276449399(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2276449399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m357662352_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m357662352(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m357662352_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m180040113_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m180040113(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m180040113_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m613524494_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m613524494(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m613524494_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1136918783_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1136918783(__this, method) ((  int32_t (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_get_Count_m1136918783_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::get_Item(TKey)
extern "C"  Vector3_t2243707580  Dictionary_2_get_Item_m293669754_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m293669754(__this, ___key0, method) ((  Vector3_t2243707580  (*) (Dictionary_2_t1251533215 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m293669754_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4044971107_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4044971107(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1251533215 *, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_set_Item_m4044971107_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2282046723_gshared (Dictionary_2_t1251533215 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2282046723(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1251533215 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2282046723_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m127325082_gshared (Dictionary_2_t1251533215 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m127325082(__this, ___size0, method) ((  void (*) (Dictionary_2_t1251533215 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m127325082_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3588600884_gshared (Dictionary_2_t1251533215 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3588600884(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3588600884_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3303845733  Dictionary_2_make_pair_m2138728610_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2138728610(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3303845733  (*) (Il2CppObject * /* static, unused */, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_make_pair_m2138728610_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m683266652_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m683266652(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_pick_key_m683266652_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::pick_value(TKey,TValue)
extern "C"  Vector3_t2243707580  Dictionary_2_pick_value_m959662044_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m959662044(__this /* static, unused */, ___key0, ___value1, method) ((  Vector3_t2243707580  (*) (Il2CppObject * /* static, unused */, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_pick_value_m959662044_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3165305031_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2U5BU5D_t2451174760* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3165305031(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1251533215 *, KeyValuePair_2U5BU5D_t2451174760*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3165305031_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::Resize()
extern "C"  void Dictionary_2_Resize_m3637103409_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3637103409(__this, method) ((  void (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_Resize_m3637103409_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m287562006_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m287562006(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1251533215 *, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_Add_m287562006_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::Clear()
extern "C"  void Dictionary_2_Clear_m4139558314_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m4139558314(__this, method) ((  void (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_Clear_m4139558314_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2596986338_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2596986338(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1251533215 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2596986338_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3681460242_gshared (Dictionary_2_t1251533215 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3681460242(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1251533215 *, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_ContainsValue_m3681460242_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3772259227_gshared (Dictionary_2_t1251533215 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3772259227(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1251533215 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3772259227_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m675495699_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m675495699(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m675495699_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3579313782_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3579313782(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1251533215 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3579313782_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2586254771_gshared (Dictionary_2_t1251533215 * __this, int32_t ___key0, Vector3_t2243707580 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2586254771(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1251533215 *, int32_t, Vector3_t2243707580 *, const MethodInfo*))Dictionary_2_TryGetValue_m2586254771_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::get_Keys()
extern "C"  KeyCollection_t3735030986 * Dictionary_2_get_Keys_m376525272_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m376525272(__this, method) ((  KeyCollection_t3735030986 * (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_get_Keys_m376525272_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::get_Values()
extern "C"  ValueCollection_t4249560354 * Dictionary_2_get_Values_m45477912_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m45477912(__this, method) ((  ValueCollection_t4249560354 * (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_get_Values_m45477912_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1581095441_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1581095441(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1581095441_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::ToTValue(System.Object)
extern "C"  Vector3_t2243707580  Dictionary_2_ToTValue_m3789677873_gshared (Dictionary_2_t1251533215 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3789677873(__this, ___value0, method) ((  Vector3_t2243707580  (*) (Dictionary_2_t1251533215 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3789677873_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m4039232075_gshared (Dictionary_2_t1251533215 * __this, KeyValuePair_2_t3303845733  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m4039232075(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1251533215 *, KeyValuePair_2_t3303845733 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m4039232075_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t2571557917  Dictionary_2_GetEnumerator_m135950656_gshared (Dictionary_2_t1251533215 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m135950656(__this, method) ((  Enumerator_t2571557917  (*) (Dictionary_2_t1251533215 *, const MethodInfo*))Dictionary_2_GetEnumerator_m135950656_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3752296985_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3752296985(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3752296985_gshared)(__this /* static, unused */, ___key0, ___value1, method)
