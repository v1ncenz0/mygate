﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CursorElement/<AddandMoveTo>c__AnonStorey0
struct U3CAddandMoveToU3Ec__AnonStorey0_t3043472538;

#include "codegen/il2cpp-codegen.h"

// System.Void CursorElement/<AddandMoveTo>c__AnonStorey0::.ctor()
extern "C"  void U3CAddandMoveToU3Ec__AnonStorey0__ctor_m3951870373 (U3CAddandMoveToU3Ec__AnonStorey0_t3043472538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement/<AddandMoveTo>c__AnonStorey0::<>m__0(System.Boolean)
extern "C"  void U3CAddandMoveToU3Ec__AnonStorey0_U3CU3Em__0_m320949765 (U3CAddandMoveToU3Ec__AnonStorey0_t3043472538 * __this, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
