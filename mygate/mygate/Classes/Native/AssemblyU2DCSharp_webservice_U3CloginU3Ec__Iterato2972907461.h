﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<login>c__Iterator5
struct  U3CloginU3Ec__Iterator5_t2972907461  : public Il2CppObject
{
public:
	// System.String webservice/<login>c__Iterator5::username
	String_t* ___username_0;
	// System.String webservice/<login>c__Iterator5::password
	String_t* ___password_1;
	// UnityEngine.WWW webservice/<login>c__Iterator5::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_2;
	// System.Action`1<System.Boolean> webservice/<login>c__Iterator5::success
	Action_1_t3627374100 * ___success_3;
	// System.Object webservice/<login>c__Iterator5::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean webservice/<login>c__Iterator5::$disposing
	bool ___U24disposing_5;
	// System.Int32 webservice/<login>c__Iterator5::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_username_0() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___username_0)); }
	inline String_t* get_username_0() const { return ___username_0; }
	inline String_t** get_address_of_username_0() { return &___username_0; }
	inline void set_username_0(String_t* value)
	{
		___username_0 = value;
		Il2CppCodeGenWriteBarrier(&___username_0, value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier(&___password_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___U3CwwwU3E__0_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_2, value);
	}

	inline static int32_t get_offset_of_success_3() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___success_3)); }
	inline Action_1_t3627374100 * get_success_3() const { return ___success_3; }
	inline Action_1_t3627374100 ** get_address_of_success_3() { return &___success_3; }
	inline void set_success_3(Action_1_t3627374100 * value)
	{
		___success_3 = value;
		Il2CppCodeGenWriteBarrier(&___success_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CloginU3Ec__Iterator5_t2972907461, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
