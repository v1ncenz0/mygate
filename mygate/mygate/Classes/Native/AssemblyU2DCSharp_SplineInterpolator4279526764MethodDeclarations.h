﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineInterpolator
struct SplineInterpolator_t4279526764;
// System.Double[]
struct DoubleU5BU5D_t1889952540;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t2784648181;
// Spline
struct Spline_t1260612603;
// System.Collections.Generic.IList`1<SplineNode>
struct IList_1_t3543945696;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t2617450533;
// System.Collections.Generic.IList`1<UnityEngine.Quaternion>
struct IList_1_t276047223;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void SplineInterpolator::.ctor()
extern "C"  void SplineInterpolator__ctor_m2360003061 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::.ctor(System.Double[],System.Int32[])
extern "C"  void SplineInterpolator__ctor_m2046973470 (SplineInterpolator_t4279526764 * __this, DoubleU5BU5D_t1889952540* ___coefficientMatrix0, Int32U5BU5D_t3030399641* ___nodeIndices1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double[] SplineInterpolator::get_CoefficientMatrix()
extern "C"  DoubleU5BU5D_t1889952540* SplineInterpolator_get_CoefficientMatrix_m2377832705 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::set_CoefficientMatrix(System.Double[])
extern "C"  void SplineInterpolator_set_CoefficientMatrix_m873043482 (SplineInterpolator_t4279526764 * __this, DoubleU5BU5D_t1889952540* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] SplineInterpolator::get_NodeIndices()
extern "C"  Int32U5BU5D_t3030399641* SplineInterpolator_get_NodeIndices_m2404690591 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::set_NodeIndices(System.Int32[])
extern "C"  void SplineInterpolator_set_NodeIndices_m3876301596 (SplineInterpolator_t4279526764 * __this, Int32U5BU5D_t3030399641* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 SplineInterpolator::get_CoefficientMatrix4x4()
extern "C"  Matrix4x4_t2933234003  SplineInterpolator_get_CoefficientMatrix4x4_m948161981 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::set_CoefficientMatrix4x4(UnityEngine.Matrix4x4)
extern "C"  void SplineInterpolator_set_CoefficientMatrix4x4_m1599670094 (SplineInterpolator_t4279526764 * __this, Matrix4x4_t2933234003  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineInterpolator::InterpolateVector(System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<UnityEngine.Vector3>,System.Int32)
extern "C"  Vector3_t2243707580  SplineInterpolator_InterpolateVector_m1324876374 (SplineInterpolator_t4279526764 * __this, double ___t0, int32_t ___index1, bool ___autoClose2, Il2CppObject* ___nodes3, int32_t ___derivationOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineInterpolator::InterpolateVector(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  Vector3_t2243707580  SplineInterpolator_InterpolateVector_m2762014499 (SplineInterpolator_t4279526764 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineInterpolator::InterpolateValue(System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<System.Single>,System.Int32)
extern "C"  float SplineInterpolator_InterpolateValue_m1171633446 (SplineInterpolator_t4279526764 * __this, double ___t0, int32_t ___index1, bool ___autoClose2, Il2CppObject* ___nodes3, int32_t ___derivationOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineInterpolator::InterpolateValue(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  float SplineInterpolator_InterpolateValue_m1809058693 (SplineInterpolator_t4279526764 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SplineInterpolator::InterpolateRotation(System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<UnityEngine.Quaternion>,System.Int32)
extern "C"  Quaternion_t4030073918  SplineInterpolator_InterpolateRotation_m2794906347 (SplineInterpolator_t4279526764 * __this, double ___t0, int32_t ___index1, bool ___autoClose2, Il2CppObject* ___nodes3, int32_t ___derivationOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SplineInterpolator::InterpolateRotation(Spline,System.Double,System.Int32,System.Boolean,System.Collections.Generic.IList`1<SplineNode>,System.Int32)
extern "C"  Quaternion_t4030073918  SplineInterpolator_InterpolateRotation_m2376921578 (SplineInterpolator_t4279526764 * __this, Spline_t1260612603 * ___spline0, double ___t1, int32_t ___index2, bool ___autoClose3, Il2CppObject* ___nodes4, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineInterpolator::InterpolateVector(System.Double,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  Vector3_t2243707580  SplineInterpolator_InterpolateVector_m3117966060 (SplineInterpolator_t4279526764 * __this, double ___t0, Vector3_t2243707580  ___v01, Vector3_t2243707580  ___v12, Vector3_t2243707580  ___v23, Vector3_t2243707580  ___v34, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineInterpolator::InterpolateValue(System.Double,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  float SplineInterpolator_InterpolateValue_m2806153284 (SplineInterpolator_t4279526764 * __this, double ___t0, float ___v01, float ___v12, float ___v23, float ___v34, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SplineInterpolator::InterpolateRotation(System.Double,UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Int32)
extern "C"  Quaternion_t4030073918  SplineInterpolator_InterpolateRotation_m3398574721 (SplineInterpolator_t4279526764 * __this, double ___t0, Quaternion_t4030073918  ___q01, Quaternion_t4030073918  ___q12, Quaternion_t4030073918  ___q23, Quaternion_t4030073918  ___q34, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SplineInterpolator::GetNodeIndex(System.Boolean,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t SplineInterpolator_GetNodeIndex_m2178906789 (SplineInterpolator_t4279526764 * __this, bool ___autoClose0, int32_t ___arrayLength1, int32_t ___index2, int32_t ___offset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::GetCoefficients(System.Double,System.Single&,System.Single&,System.Single&,System.Single&,System.Int32)
extern "C"  void SplineInterpolator_GetCoefficients_m1171169320 (SplineInterpolator_t4279526764 * __this, double ___t0, float* ___b01, float* ___b12, float* ___b23, float* ___b34, int32_t ___derivationOrder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::GetCoefficients(System.Double,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void SplineInterpolator_GetCoefficients_m3556050235 (SplineInterpolator_t4279526764 * __this, double ___t0, float* ___b01, float* ___b12, float* ___b23, float* ___b34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::GetCoefficientsFirstDerivative(System.Double,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void SplineInterpolator_GetCoefficientsFirstDerivative_m1241288012 (SplineInterpolator_t4279526764 * __this, double ___t0, float* ___b01, float* ___b12, float* ___b23, float* ___b34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::GetCoefficientsSecondDerivative(System.Double,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void SplineInterpolator_GetCoefficientsSecondDerivative_m4194273662 (SplineInterpolator_t4279526764 * __this, double ___t0, float* ___b01, float* ___b12, float* ___b23, float* ___b34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::CheckMatrix(System.Double[])
extern "C"  void SplineInterpolator_CheckMatrix_m366371876 (SplineInterpolator_t4279526764 * __this, DoubleU5BU5D_t1889952540* ___coefficientMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::CheckIndices(System.Int32[])
extern "C"  void SplineInterpolator_CheckIndices_m2168140015 (SplineInterpolator_t4279526764 * __this, Int32U5BU5D_t3030399641* ___nodeIndices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
