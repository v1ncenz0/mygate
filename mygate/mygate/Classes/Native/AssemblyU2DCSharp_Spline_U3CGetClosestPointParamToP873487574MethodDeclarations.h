﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline/<GetClosestPointParamToPlane>c__AnonStorey2
struct U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Spline/<GetClosestPointParamToPlane>c__AnonStorey2::.ctor()
extern "C"  void U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2__ctor_m3062412063 (U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline/<GetClosestPointParamToPlane>c__AnonStorey2::<>m__0(UnityEngine.Vector3)
extern "C"  float U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_U3CU3Em__0_m2487034899 (U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574 * __this, Vector3_t2243707580  ___splinePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
