﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_start
struct init_start_t1142197987;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Action`1<UnityEngine.Sprite>
struct Action_1_t111393165;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void init_start::.ctor()
extern "C"  void init_start__ctor_m2950883688 (init_start_t1142197987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::Start()
extern "C"  void init_start_Start_m2907018180 (init_start_t1142197987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::InitGUI()
extern "C"  void init_start_InitGUI_m3974044271 (init_start_t1142197987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator init_start::downloadImage(System.String,System.Action`1<UnityEngine.Sprite>)
extern "C"  Il2CppObject * init_start_downloadImage_m1846719933 (init_start_t1142197987 * __this, String_t* ___image_path0, Action_1_t111393165 * ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::NextPanel()
extern "C"  void init_start_NextPanel_m1686996839 (init_start_t1142197987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::PrevPanel()
extern "C"  void init_start_PrevPanel_m3772056749 (init_start_t1142197987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::MoveToPanel(System.Int32)
extern "C"  void init_start_MoveToPanel_m890291695 (init_start_t1142197987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start::<Start>m__0(System.Boolean)
extern "C"  void init_start_U3CStartU3Em__0_m4053461412 (init_start_t1142197987 * __this, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
