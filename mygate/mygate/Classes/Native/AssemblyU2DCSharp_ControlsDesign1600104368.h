﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// gridLayer
struct gridLayer_t4027367017;
// GridManager
struct GridManager_t4027472533;
// Item
struct Item_t2440468191;
// MoveCamera
struct MoveCamera_t3193441886;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign
struct  ControlsDesign_t1600104368  : public MonoBehaviour_t1158329972
{
public:
	// gridLayer ControlsDesign::currentActive
	gridLayer_t4027367017 * ___currentActive_2;
	// GridManager ControlsDesign::cg
	GridManager_t4027472533 * ___cg_3;
	// Item ControlsDesign::itemSelected
	Item_t2440468191 * ___itemSelected_4;
	// MoveCamera ControlsDesign::moveCamera
	MoveCamera_t3193441886 * ___moveCamera_5;
	// UnityEngine.GameObject ControlsDesign::cursorObject
	GameObject_t1756533147 * ___cursorObject_6;
	// UnityEngine.GameObject ControlsDesign::cursorDeleteMode
	GameObject_t1756533147 * ___cursorDeleteMode_7;
	// System.Boolean ControlsDesign::addspecularcell
	bool ___addspecularcell_8;
	// System.Boolean ControlsDesign::is_scene_rendering
	bool ___is_scene_rendering_9;
	// System.Double ControlsDesign::price
	double ___price_10;
	// System.Boolean ControlsDesign::is_screencapture_fase
	bool ___is_screencapture_fase_11;

public:
	inline static int32_t get_offset_of_currentActive_2() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___currentActive_2)); }
	inline gridLayer_t4027367017 * get_currentActive_2() const { return ___currentActive_2; }
	inline gridLayer_t4027367017 ** get_address_of_currentActive_2() { return &___currentActive_2; }
	inline void set_currentActive_2(gridLayer_t4027367017 * value)
	{
		___currentActive_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentActive_2, value);
	}

	inline static int32_t get_offset_of_cg_3() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___cg_3)); }
	inline GridManager_t4027472533 * get_cg_3() const { return ___cg_3; }
	inline GridManager_t4027472533 ** get_address_of_cg_3() { return &___cg_3; }
	inline void set_cg_3(GridManager_t4027472533 * value)
	{
		___cg_3 = value;
		Il2CppCodeGenWriteBarrier(&___cg_3, value);
	}

	inline static int32_t get_offset_of_itemSelected_4() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___itemSelected_4)); }
	inline Item_t2440468191 * get_itemSelected_4() const { return ___itemSelected_4; }
	inline Item_t2440468191 ** get_address_of_itemSelected_4() { return &___itemSelected_4; }
	inline void set_itemSelected_4(Item_t2440468191 * value)
	{
		___itemSelected_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemSelected_4, value);
	}

	inline static int32_t get_offset_of_moveCamera_5() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___moveCamera_5)); }
	inline MoveCamera_t3193441886 * get_moveCamera_5() const { return ___moveCamera_5; }
	inline MoveCamera_t3193441886 ** get_address_of_moveCamera_5() { return &___moveCamera_5; }
	inline void set_moveCamera_5(MoveCamera_t3193441886 * value)
	{
		___moveCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___moveCamera_5, value);
	}

	inline static int32_t get_offset_of_cursorObject_6() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___cursorObject_6)); }
	inline GameObject_t1756533147 * get_cursorObject_6() const { return ___cursorObject_6; }
	inline GameObject_t1756533147 ** get_address_of_cursorObject_6() { return &___cursorObject_6; }
	inline void set_cursorObject_6(GameObject_t1756533147 * value)
	{
		___cursorObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___cursorObject_6, value);
	}

	inline static int32_t get_offset_of_cursorDeleteMode_7() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___cursorDeleteMode_7)); }
	inline GameObject_t1756533147 * get_cursorDeleteMode_7() const { return ___cursorDeleteMode_7; }
	inline GameObject_t1756533147 ** get_address_of_cursorDeleteMode_7() { return &___cursorDeleteMode_7; }
	inline void set_cursorDeleteMode_7(GameObject_t1756533147 * value)
	{
		___cursorDeleteMode_7 = value;
		Il2CppCodeGenWriteBarrier(&___cursorDeleteMode_7, value);
	}

	inline static int32_t get_offset_of_addspecularcell_8() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___addspecularcell_8)); }
	inline bool get_addspecularcell_8() const { return ___addspecularcell_8; }
	inline bool* get_address_of_addspecularcell_8() { return &___addspecularcell_8; }
	inline void set_addspecularcell_8(bool value)
	{
		___addspecularcell_8 = value;
	}

	inline static int32_t get_offset_of_is_scene_rendering_9() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___is_scene_rendering_9)); }
	inline bool get_is_scene_rendering_9() const { return ___is_scene_rendering_9; }
	inline bool* get_address_of_is_scene_rendering_9() { return &___is_scene_rendering_9; }
	inline void set_is_scene_rendering_9(bool value)
	{
		___is_scene_rendering_9 = value;
	}

	inline static int32_t get_offset_of_price_10() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___price_10)); }
	inline double get_price_10() const { return ___price_10; }
	inline double* get_address_of_price_10() { return &___price_10; }
	inline void set_price_10(double value)
	{
		___price_10 = value;
	}

	inline static int32_t get_offset_of_is_screencapture_fase_11() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368, ___is_screencapture_fase_11)); }
	inline bool get_is_screencapture_fase_11() const { return ___is_screencapture_fase_11; }
	inline bool* get_address_of_is_screencapture_fase_11() { return &___is_screencapture_fase_11; }
	inline void set_is_screencapture_fase_11(bool value)
	{
		___is_screencapture_fase_11 = value;
	}
};

struct ControlsDesign_t1600104368_StaticFields
{
public:
	// System.Action`1<System.Boolean> ControlsDesign::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_12;
	// System.Action`1<System.Boolean> ControlsDesign::<>f__am$cache1
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(ControlsDesign_t1600104368_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
