﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineSegment
struct SplineSegment_t1083783318;
// Spline
struct Spline_t1260612603;
// SplineNode
struct SplineNode_t3003005095;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"

// System.Void SplineSegment::.ctor(Spline,SplineNode,SplineNode)
extern "C"  void SplineSegment__ctor_m582665040 (SplineSegment_t1083783318 * __this, Spline_t1260612603 * ___pSpline0, SplineNode_t3003005095 * ___sNode1, SplineNode_t3003005095 * ___eNode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Spline SplineSegment::get_ParentSpline()
extern "C"  Spline_t1260612603 * SplineSegment_get_ParentSpline_m2942269419 (SplineSegment_t1083783318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode SplineSegment::get_StartNode()
extern "C"  SplineNode_t3003005095 * SplineSegment_get_StartNode_m2051308142 (SplineSegment_t1083783318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode SplineSegment::get_EndNode()
extern "C"  SplineNode_t3003005095 * SplineSegment_get_EndNode_m2538562245 (SplineSegment_t1083783318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineSegment::get_Length()
extern "C"  float SplineSegment_get_Length_m3039957622 (SplineSegment_t1083783318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineSegment::get_NormalizedLength()
extern "C"  float SplineSegment_get_NormalizedLength_m2523049243 (SplineSegment_t1083783318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineSegment::ConvertSegmentToSplineParamter(System.Single)
extern "C"  float SplineSegment_ConvertSegmentToSplineParamter_m2813667068 (SplineSegment_t1083783318 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineSegment::ConvertSplineToSegmentParamter(System.Single)
extern "C"  float SplineSegment_ConvertSplineToSegmentParamter_m71178168 (SplineSegment_t1083783318 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineSegment::ClampParameterToSegment(System.Single)
extern "C"  float SplineSegment_ClampParameterToSegment_m2452667620 (SplineSegment_t1083783318 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SplineSegment::IsParameterInRange(System.Single)
extern "C"  bool SplineSegment_IsParameterInRange_m2656264675 (SplineSegment_t1083783318 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
