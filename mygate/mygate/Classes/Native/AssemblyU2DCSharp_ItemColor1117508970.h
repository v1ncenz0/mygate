﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemColor
struct  ItemColor_t1117508970  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemColor::id
	int32_t ___id_2;
	// System.String ItemColor::name
	String_t* ___name_3;
	// UnityEngine.Color ItemColor::color
	Color_t2020392075  ___color_4;
	// UnityEngine.Texture ItemColor::texture
	Texture_t2243626319 * ___texture_5;
	// System.String ItemColor::texture_path
	String_t* ___texture_path_6;
	// System.String ItemColor::image_path
	String_t* ___image_path_7;
	// System.Boolean ItemColor::selected
	bool ___selected_8;
	// UnityEngine.Material ItemColor::material
	Material_t193706927 * ___material_9;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___color_4)); }
	inline Color_t2020392075  get_color_4() const { return ___color_4; }
	inline Color_t2020392075 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t2020392075  value)
	{
		___color_4 = value;
	}

	inline static int32_t get_offset_of_texture_5() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___texture_5)); }
	inline Texture_t2243626319 * get_texture_5() const { return ___texture_5; }
	inline Texture_t2243626319 ** get_address_of_texture_5() { return &___texture_5; }
	inline void set_texture_5(Texture_t2243626319 * value)
	{
		___texture_5 = value;
		Il2CppCodeGenWriteBarrier(&___texture_5, value);
	}

	inline static int32_t get_offset_of_texture_path_6() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___texture_path_6)); }
	inline String_t* get_texture_path_6() const { return ___texture_path_6; }
	inline String_t** get_address_of_texture_path_6() { return &___texture_path_6; }
	inline void set_texture_path_6(String_t* value)
	{
		___texture_path_6 = value;
		Il2CppCodeGenWriteBarrier(&___texture_path_6, value);
	}

	inline static int32_t get_offset_of_image_path_7() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___image_path_7)); }
	inline String_t* get_image_path_7() const { return ___image_path_7; }
	inline String_t** get_address_of_image_path_7() { return &___image_path_7; }
	inline void set_image_path_7(String_t* value)
	{
		___image_path_7 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_7, value);
	}

	inline static int32_t get_offset_of_selected_8() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___selected_8)); }
	inline bool get_selected_8() const { return ___selected_8; }
	inline bool* get_address_of_selected_8() { return &___selected_8; }
	inline void set_selected_8(bool value)
	{
		___selected_8 = value;
	}

	inline static int32_t get_offset_of_material_9() { return static_cast<int32_t>(offsetof(ItemColor_t1117508970, ___material_9)); }
	inline Material_t193706927 * get_material_9() const { return ___material_9; }
	inline Material_t193706927 ** get_address_of_material_9() { return &___material_9; }
	inline void set_material_9(Material_t193706927 * value)
	{
		___material_9 = value;
		Il2CppCodeGenWriteBarrier(&___material_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
