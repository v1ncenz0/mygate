﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3
struct U3CLoadU3Ec__AnonStorey3_t2764713728;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3::.ctor()
extern "C"  void U3CLoadU3Ec__AnonStorey3__ctor_m2742972369 (U3CLoadU3Ec__AnonStorey3_t2764713728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3::<>m__0(System.Boolean)
extern "C"  void U3CLoadU3Ec__AnonStorey3_U3CU3Em__0_m239300761 (U3CLoadU3Ec__AnonStorey3_t2764713728 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
