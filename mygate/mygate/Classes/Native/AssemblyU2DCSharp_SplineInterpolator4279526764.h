﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Double[]
struct DoubleU5BU5D_t1889952540;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineInterpolator
struct  SplineInterpolator_t4279526764  : public Il2CppObject
{
public:
	// System.Double[] SplineInterpolator::coefficientMatrix
	DoubleU5BU5D_t1889952540* ___coefficientMatrix_0;
	// System.Int32[] SplineInterpolator::nodeIndices
	Int32U5BU5D_t3030399641* ___nodeIndices_1;

public:
	inline static int32_t get_offset_of_coefficientMatrix_0() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___coefficientMatrix_0)); }
	inline DoubleU5BU5D_t1889952540* get_coefficientMatrix_0() const { return ___coefficientMatrix_0; }
	inline DoubleU5BU5D_t1889952540** get_address_of_coefficientMatrix_0() { return &___coefficientMatrix_0; }
	inline void set_coefficientMatrix_0(DoubleU5BU5D_t1889952540* value)
	{
		___coefficientMatrix_0 = value;
		Il2CppCodeGenWriteBarrier(&___coefficientMatrix_0, value);
	}

	inline static int32_t get_offset_of_nodeIndices_1() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___nodeIndices_1)); }
	inline Int32U5BU5D_t3030399641* get_nodeIndices_1() const { return ___nodeIndices_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_nodeIndices_1() { return &___nodeIndices_1; }
	inline void set_nodeIndices_1(Int32U5BU5D_t3030399641* value)
	{
		___nodeIndices_1 = value;
		Il2CppCodeGenWriteBarrier(&___nodeIndices_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
