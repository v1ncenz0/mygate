﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// SplineNode
struct SplineNode_t3003005095;
// Spline
struct Spline_t1260612603;
// BranchingSplinePath
struct BranchingSplinePath_t1574214842;
// SplineSegment
struct SplineSegment_t1083783318;
// SplineMeshModifier
struct SplineMeshModifier_t3237667319;
// SplineMesh/MeshData
struct MeshData_t1171048948;
// NodeParameters
struct NodeParameters_t819515452;
// Element
struct Element_t2605769808;
// CellGrid
struct CellGrid_t1919063536;
// JSONObject
struct JSONObject_t1971882247;
// GeometryBuffer/ObjectData
struct ObjectData_t4054693732;
// GeometryBuffer/GroupData
struct GroupData_t2204692426;
// GeometryBuffer/FaceIndicesRow
struct FaceIndicesRow_t3099920559;
// OBJ/MaterialData
struct MaterialData_t3551903345;
// OBJ/ObjectFaces
struct ObjectFaces_t1920115861;
// OBJ/BumpParamDef
struct BumpParamDef_t684364218;
// SimpleJSON.JSONNode
struct JSONNode_t1250409636;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "AssemblyU2DCSharp_BranchingSplinePath1574214842.h"
#include "AssemblyU2DCSharp_SplineSegment1083783318.h"
#include "AssemblyU2DCSharp_SplineMeshModifier3237667319.h"
#include "AssemblyU2DCSharp_SplineMesh_MeshData1171048948.h"
#include "AssemblyU2DCSharp_NodeParameters819515452.h"
#include "AssemblyU2DCSharp_Element2605769808.h"
#include "AssemblyU2DCSharp_CellGrid1919063536.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "AssemblyU2DCSharp_GeometryBuffer_ObjectData4054693732.h"
#include "AssemblyU2DCSharp_GeometryBuffer_GroupData2204692426.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"
#include "AssemblyU2DCSharp_GeometryBuffer_FaceIndicesRow3099920559.h"
#include "AssemblyU2DCSharp_OBJ_MaterialData3551903345.h"
#include "AssemblyU2DCSharp_OBJ_ObjectFaces1920115861.h"
#include "AssemblyU2DCSharp_OBJ_BumpParamDef684364218.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode1250409636.h"

#pragma once
// SplineNode[]
struct SplineNodeU5BU5D_t2523538654  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SplineNode_t3003005095 * m_Items[1];

public:
	inline SplineNode_t3003005095 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SplineNode_t3003005095 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SplineNode_t3003005095 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SplineNode_t3003005095 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SplineNode_t3003005095 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SplineNode_t3003005095 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Spline[]
struct SplineU5BU5D_t2562733498  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Spline_t1260612603 * m_Items[1];

public:
	inline Spline_t1260612603 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Spline_t1260612603 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Spline_t1260612603 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Spline_t1260612603 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Spline_t1260612603 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Spline_t1260612603 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BranchingSplinePath[]
struct BranchingSplinePathU5BU5D_t900974367  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BranchingSplinePath_t1574214842 * m_Items[1];

public:
	inline BranchingSplinePath_t1574214842 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline BranchingSplinePath_t1574214842 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, BranchingSplinePath_t1574214842 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline BranchingSplinePath_t1574214842 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline BranchingSplinePath_t1574214842 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, BranchingSplinePath_t1574214842 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SplineSegment[]
struct SplineSegmentU5BU5D_t3814610003  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SplineSegment_t1083783318 * m_Items[1];

public:
	inline SplineSegment_t1083783318 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SplineSegment_t1083783318 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SplineSegment_t1083783318 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SplineSegment_t1083783318 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SplineSegment_t1083783318 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SplineSegment_t1083783318 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SplineMeshModifier[]
struct SplineMeshModifierU5BU5D_t2295569486  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SplineMeshModifier_t3237667319 * m_Items[1];

public:
	inline SplineMeshModifier_t3237667319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SplineMeshModifier_t3237667319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SplineMeshModifier_t3237667319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SplineMeshModifier_t3237667319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SplineMeshModifier_t3237667319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SplineMeshModifier_t3237667319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SplineMesh/MeshData[]
struct MeshDataU5BU5D_t910502845  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MeshData_t1171048948 * m_Items[1];

public:
	inline MeshData_t1171048948 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MeshData_t1171048948 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MeshData_t1171048948 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MeshData_t1171048948 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MeshData_t1171048948 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MeshData_t1171048948 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// NodeParameters[]
struct NodeParametersU5BU5D_t3889699669  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NodeParameters_t819515452 * m_Items[1];

public:
	inline NodeParameters_t819515452 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NodeParameters_t819515452 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NodeParameters_t819515452 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NodeParameters_t819515452 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NodeParameters_t819515452 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NodeParameters_t819515452 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Element[]
struct ElementU5BU5D_t3301830769  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Element_t2605769808 * m_Items[1];

public:
	inline Element_t2605769808 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Element_t2605769808 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Element_t2605769808 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Element_t2605769808 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Element_t2605769808 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Element_t2605769808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CellGrid[]
struct CellGridU5BU5D_t3919062097  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CellGrid_t1919063536 * m_Items[1];

public:
	inline CellGrid_t1919063536 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CellGrid_t1919063536 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CellGrid_t1919063536 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CellGrid_t1919063536 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CellGrid_t1919063536 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CellGrid_t1919063536 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSONObject[]
struct JSONObjectU5BU5D_t2270799614  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSONObject_t1971882247 * m_Items[1];

public:
	inline JSONObject_t1971882247 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline JSONObject_t1971882247 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, JSONObject_t1971882247 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline JSONObject_t1971882247 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline JSONObject_t1971882247 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, JSONObject_t1971882247 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GeometryBuffer/ObjectData[]
struct ObjectDataU5BU5D_t3041995405  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectData_t4054693732 * m_Items[1];

public:
	inline ObjectData_t4054693732 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ObjectData_t4054693732 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ObjectData_t4054693732 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ObjectData_t4054693732 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ObjectData_t4054693732 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ObjectData_t4054693732 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GeometryBuffer/GroupData[]
struct GroupDataU5BU5D_t365360591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GroupData_t2204692426 * m_Items[1];

public:
	inline GroupData_t2204692426 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GroupData_t2204692426 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GroupData_t2204692426 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GroupData_t2204692426 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GroupData_t2204692426 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GroupData_t2204692426 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FaceIndices[]
struct FaceIndicesU5BU5D_t2696217939  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FaceIndices_t174955414  m_Items[1];

public:
	inline FaceIndices_t174955414  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FaceIndices_t174955414 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FaceIndices_t174955414  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline FaceIndices_t174955414  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FaceIndices_t174955414 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FaceIndices_t174955414  value)
	{
		m_Items[index] = value;
	}
};
// GeometryBuffer/FaceIndicesRow[]
struct FaceIndicesRowU5BU5D_t1592994742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FaceIndicesRow_t3099920559 * m_Items[1];

public:
	inline FaceIndicesRow_t3099920559 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FaceIndicesRow_t3099920559 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FaceIndicesRow_t3099920559 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FaceIndicesRow_t3099920559 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FaceIndicesRow_t3099920559 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FaceIndicesRow_t3099920559 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OBJ/MaterialData[]
struct MaterialDataU5BU5D_t1968663724  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MaterialData_t3551903345 * m_Items[1];

public:
	inline MaterialData_t3551903345 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MaterialData_t3551903345 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MaterialData_t3551903345 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MaterialData_t3551903345 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MaterialData_t3551903345 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MaterialData_t3551903345 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OBJ/ObjectFaces[]
struct ObjectFacesU5BU5D_t2042303864  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectFaces_t1920115861 * m_Items[1];

public:
	inline ObjectFaces_t1920115861 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ObjectFaces_t1920115861 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ObjectFaces_t1920115861 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ObjectFaces_t1920115861 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ObjectFaces_t1920115861 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ObjectFaces_t1920115861 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OBJ/BumpParamDef[]
struct BumpParamDefU5BU5D_t3173408287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BumpParamDef_t684364218 * m_Items[1];

public:
	inline BumpParamDef_t684364218 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline BumpParamDef_t684364218 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, BumpParamDef_t684364218 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline BumpParamDef_t684364218 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline BumpParamDef_t684364218 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, BumpParamDef_t684364218 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SimpleJSON.JSONNode[]
struct JSONNodeU5BU5D_t3483773005  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSONNode_t1250409636 * m_Items[1];

public:
	inline JSONNode_t1250409636 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline JSONNode_t1250409636 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, JSONNode_t1250409636 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline JSONNode_t1250409636 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline JSONNode_t1250409636 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, JSONNode_t1250409636 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
