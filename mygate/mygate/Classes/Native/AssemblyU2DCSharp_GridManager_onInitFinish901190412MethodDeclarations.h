﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GridManager/onInitFinish
struct onInitFinish_t901190412;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void GridManager/onInitFinish::.ctor(System.Object,System.IntPtr)
extern "C"  void onInitFinish__ctor_m3892945171 (onInitFinish_t901190412 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager/onInitFinish::Invoke()
extern "C"  void onInitFinish_Invoke_m1177533551 (onInitFinish_t901190412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GridManager/onInitFinish::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * onInitFinish_BeginInvoke_m57095278 (onInitFinish_t901190412 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager/onInitFinish::EndInvoke(System.IAsyncResult)
extern "C"  void onInitFinish_EndInvoke_m238209101 (onInitFinish_t901190412 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
