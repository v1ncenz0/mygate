﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ctrl
struct  Ctrl_t4006552079  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Ctrl_t4006552079_StaticFields
{
public:
	// System.Boolean Ctrl::inDeleteMode
	bool ___inDeleteMode_2;

public:
	inline static int32_t get_offset_of_inDeleteMode_2() { return static_cast<int32_t>(offsetof(Ctrl_t4006552079_StaticFields, ___inDeleteMode_2)); }
	inline bool get_inDeleteMode_2() const { return ___inDeleteMode_2; }
	inline bool* get_address_of_inDeleteMode_2() { return &___inDeleteMode_2; }
	inline void set_inDeleteMode_2(bool value)
	{
		___inDeleteMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
