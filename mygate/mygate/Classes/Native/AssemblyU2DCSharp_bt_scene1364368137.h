﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemScene
struct ItemScene_t1151284345;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_scene
struct  bt_scene_t1364368137  : public MonoBehaviour_t1158329972
{
public:
	// ItemScene bt_scene::scene
	ItemScene_t1151284345 * ___scene_2;
	// System.Int32 bt_scene::finishstatus
	int32_t ___finishstatus_3;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(bt_scene_t1364368137, ___scene_2)); }
	inline ItemScene_t1151284345 * get_scene_2() const { return ___scene_2; }
	inline ItemScene_t1151284345 ** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(ItemScene_t1151284345 * value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier(&___scene_2, value);
	}

	inline static int32_t get_offset_of_finishstatus_3() { return static_cast<int32_t>(offsetof(bt_scene_t1364368137, ___finishstatus_3)); }
	inline int32_t get_finishstatus_3() const { return ___finishstatus_3; }
	inline int32_t* get_address_of_finishstatus_3() { return &___finishstatus_3; }
	inline void set_finishstatus_3(int32_t value)
	{
		___finishstatus_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
