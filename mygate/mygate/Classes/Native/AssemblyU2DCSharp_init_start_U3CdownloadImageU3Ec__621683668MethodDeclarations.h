﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_start/<downloadImage>c__Iterator0
struct U3CdownloadImageU3Ec__Iterator0_t621683668;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void init_start/<downloadImage>c__Iterator0::.ctor()
extern "C"  void U3CdownloadImageU3Ec__Iterator0__ctor_m2874952361 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean init_start/<downloadImage>c__Iterator0::MoveNext()
extern "C"  bool U3CdownloadImageU3Ec__Iterator0_MoveNext_m3869621407 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object init_start/<downloadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3345004327 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object init_start/<downloadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m967900895 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start/<downloadImage>c__Iterator0::Dispose()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Dispose_m1942905918 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_start/<downloadImage>c__Iterator0::Reset()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Reset_m2075213384 (U3CdownloadImageU3Ec__Iterator0_t621683668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
