﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline/DistanceFunction
struct DistanceFunction_t3700963585;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Spline/DistanceFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void DistanceFunction__ctor_m1785812560 (DistanceFunction_t3700963585 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline/DistanceFunction::Invoke(UnityEngine.Vector3)
extern "C"  float DistanceFunction_Invoke_m3514536151 (DistanceFunction_t3700963585 * __this, Vector3_t2243707580  ___splinePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Spline/DistanceFunction::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DistanceFunction_BeginInvoke_m2762029776 (DistanceFunction_t3700963585 * __this, Vector3_t2243707580  ___splinePos0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline/DistanceFunction::EndInvoke(System.IAsyncResult)
extern "C"  float DistanceFunction_EndInvoke_m2213370572 (DistanceFunction_t3700963585 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
