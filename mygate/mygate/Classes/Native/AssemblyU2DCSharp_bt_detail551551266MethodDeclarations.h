﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_detail
struct bt_detail_t551551266;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_detail::.ctor()
extern "C"  void bt_detail__ctor_m3366971379 (bt_detail_t551551266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_detail::openDetail()
extern "C"  void bt_detail_openDetail_m4218772444 (bt_detail_t551551266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_detail::closeDetail()
extern "C"  void bt_detail_closeDetail_m3045616140 (bt_detail_t551551266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_detail::<openDetail>m__0(System.Boolean)
extern "C"  void bt_detail_U3CopenDetailU3Em__0_m769503862 (bt_detail_t551551266 * __this, bool ___img_downloaded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
