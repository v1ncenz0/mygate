﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_bt_scroll_ModeList2967208204.h"
#include "AssemblyU2DCSharp_bt_scroll_TypeList1536841447.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_scroll
struct  bt_scroll_t132828814  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject bt_scroll::scrollview
	GameObject_t1756533147 * ___scrollview_2;
	// System.Single bt_scroll::delta
	float ___delta_3;
	// bt_scroll/ModeList bt_scroll::mode
	int32_t ___mode_4;
	// bt_scroll/TypeList bt_scroll::type
	int32_t ___type_5;

public:
	inline static int32_t get_offset_of_scrollview_2() { return static_cast<int32_t>(offsetof(bt_scroll_t132828814, ___scrollview_2)); }
	inline GameObject_t1756533147 * get_scrollview_2() const { return ___scrollview_2; }
	inline GameObject_t1756533147 ** get_address_of_scrollview_2() { return &___scrollview_2; }
	inline void set_scrollview_2(GameObject_t1756533147 * value)
	{
		___scrollview_2 = value;
		Il2CppCodeGenWriteBarrier(&___scrollview_2, value);
	}

	inline static int32_t get_offset_of_delta_3() { return static_cast<int32_t>(offsetof(bt_scroll_t132828814, ___delta_3)); }
	inline float get_delta_3() const { return ___delta_3; }
	inline float* get_address_of_delta_3() { return &___delta_3; }
	inline void set_delta_3(float value)
	{
		___delta_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(bt_scroll_t132828814, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(bt_scroll_t132828814, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
