﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2456756948(__this, ___l0, method) ((  void (*) (Enumerator_t2003771365 *, List_1_t2469041691 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2496769430(__this, method) ((  void (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1929739178(__this, method) ((  Il2CppObject * (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::Dispose()
#define Enumerator_Dispose_m3853746087(__this, method) ((  void (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::VerifyState()
#define Enumerator_VerifyState_m3752847778(__this, method) ((  void (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::MoveNext()
#define Enumerator_MoveNext_m3120356301(__this, method) ((  bool (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GeometryBuffer/FaceIndicesRow>::get_Current()
#define Enumerator_get_Current_m1275544117(__this, method) ((  FaceIndicesRow_t3099920559 * (*) (Enumerator_t2003771365 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
