﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ControlsDesign
struct ControlsDesign_t1600104368;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<getGatefromJSON>c__AnonStorey7
struct  U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556  : public Il2CppObject
{
public:
	// System.Collections.ArrayList ControlsDesign/<getGatefromJSON>c__AnonStorey7::items
	ArrayList_t4252133567 * ___items_0;
	// System.Collections.ArrayList ControlsDesign/<getGatefromJSON>c__AnonStorey7::elements
	ArrayList_t4252133567 * ___elements_1;
	// System.Action`1<System.Boolean> ControlsDesign/<getGatefromJSON>c__AnonStorey7::success
	Action_1_t3627374100 * ___success_2;
	// ControlsDesign ControlsDesign/<getGatefromJSON>c__AnonStorey7::$this
	ControlsDesign_t1600104368 * ___U24this_3;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556, ___items_0)); }
	inline ArrayList_t4252133567 * get_items_0() const { return ___items_0; }
	inline ArrayList_t4252133567 ** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ArrayList_t4252133567 * value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier(&___items_0, value);
	}

	inline static int32_t get_offset_of_elements_1() { return static_cast<int32_t>(offsetof(U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556, ___elements_1)); }
	inline ArrayList_t4252133567 * get_elements_1() const { return ___elements_1; }
	inline ArrayList_t4252133567 ** get_address_of_elements_1() { return &___elements_1; }
	inline void set_elements_1(ArrayList_t4252133567 * value)
	{
		___elements_1 = value;
		Il2CppCodeGenWriteBarrier(&___elements_1, value);
	}

	inline static int32_t get_offset_of_success_2() { return static_cast<int32_t>(offsetof(U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556, ___success_2)); }
	inline Action_1_t3627374100 * get_success_2() const { return ___success_2; }
	inline Action_1_t3627374100 ** get_address_of_success_2() { return &___success_2; }
	inline void set_success_2(Action_1_t3627374100 * value)
	{
		___success_2 = value;
		Il2CppCodeGenWriteBarrier(&___success_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CgetGatefromJSONU3Ec__AnonStorey7_t4022664556, ___U24this_3)); }
	inline ControlsDesign_t1600104368 * get_U24this_3() const { return ___U24this_3; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ControlsDesign_t1600104368 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
