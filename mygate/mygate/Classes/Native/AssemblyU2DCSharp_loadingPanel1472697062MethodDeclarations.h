﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// loadingPanel
struct loadingPanel_t1472697062;

#include "codegen/il2cpp-codegen.h"

// System.Void loadingPanel::.ctor()
extern "C"  void loadingPanel__ctor_m2467080847 (loadingPanel_t1472697062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void loadingPanel::GoBack()
extern "C"  void loadingPanel_GoBack_m2501827548 (loadingPanel_t1472697062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void loadingPanel::showError()
extern "C"  void loadingPanel_showError_m396398720 (loadingPanel_t1472697062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
