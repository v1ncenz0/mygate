﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineCurveScaleModifier
struct SplineCurveScaleModifier_t1538173695;
// SplineMesh
struct SplineMesh_t1719246168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_SplineMesh1719246168.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// System.Void SplineCurveScaleModifier::.ctor()
extern "C"  void SplineCurveScaleModifier__ctor_m437081794 (SplineCurveScaleModifier_t1538173695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineCurveScaleModifier::ModifyVertex(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineCurveScaleModifier_ModifyVertex_m3185117926 (SplineCurveScaleModifier_t1538173695 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___vertex1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SplineCurveScaleModifier::ModifyUV(SplineMesh,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  SplineCurveScaleModifier_ModifyUV_m4111032857 (SplineCurveScaleModifier_t1538173695 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector2_t2243707579  ___uvCoord1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineCurveScaleModifier::ModifyNormal(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineCurveScaleModifier_ModifyNormal_m2304353401 (SplineCurveScaleModifier_t1538173695 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___normal1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 SplineCurveScaleModifier::ModifyTangent(SplineMesh,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  SplineCurveScaleModifier_ModifyTangent_m345613829 (SplineCurveScaleModifier_t1538173695 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector4_t2243707581  ___tangent1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
