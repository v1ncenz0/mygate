﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_buy
struct bt_buy_t2549161545;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_buy::.ctor()
extern "C"  void bt_buy__ctor_m2901687660 (bt_buy_t2549161545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_buy::goToBuy()
extern "C"  void bt_buy_goToBuy_m629598249 (bt_buy_t2549161545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
