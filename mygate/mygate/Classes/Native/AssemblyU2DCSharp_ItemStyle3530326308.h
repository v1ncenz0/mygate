﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemStyle
struct  ItemStyle_t3530326308  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemStyle::id
	int32_t ___id_2;
	// System.String ItemStyle::name
	String_t* ___name_3;
	// UnityEngine.Sprite ItemStyle::image
	Sprite_t309593783 * ___image_4;
	// System.String ItemStyle::image_path
	String_t* ___image_path_5;
	// UnityEngine.Vector2 ItemStyle::generator
	Vector2_t2243707579  ___generator_6;
	// System.Collections.ArrayList ItemStyle::elements
	ArrayList_t4252133567 * ___elements_7;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___image_4)); }
	inline Sprite_t309593783 * get_image_4() const { return ___image_4; }
	inline Sprite_t309593783 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Sprite_t309593783 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_image_path_5() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___image_path_5)); }
	inline String_t* get_image_path_5() const { return ___image_path_5; }
	inline String_t** get_address_of_image_path_5() { return &___image_path_5; }
	inline void set_image_path_5(String_t* value)
	{
		___image_path_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_5, value);
	}

	inline static int32_t get_offset_of_generator_6() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___generator_6)); }
	inline Vector2_t2243707579  get_generator_6() const { return ___generator_6; }
	inline Vector2_t2243707579 * get_address_of_generator_6() { return &___generator_6; }
	inline void set_generator_6(Vector2_t2243707579  value)
	{
		___generator_6 = value;
	}

	inline static int32_t get_offset_of_elements_7() { return static_cast<int32_t>(offsetof(ItemStyle_t3530326308, ___elements_7)); }
	inline ArrayList_t4252133567 * get_elements_7() const { return ___elements_7; }
	inline ArrayList_t4252133567 ** get_address_of_elements_7() { return &___elements_7; }
	inline void set_elements_7(ArrayList_t4252133567 * value)
	{
		___elements_7 = value;
		Il2CppCodeGenWriteBarrier(&___elements_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
