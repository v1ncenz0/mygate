﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_back_gates
struct bt_back_gates_t3731273459;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_back_gates::.ctor()
extern "C"  void bt_back_gates__ctor_m847378006 (bt_back_gates_t3731273459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_back_gates::goToGates()
extern "C"  void bt_back_gates_goToGates_m1363679777 (bt_back_gates_t3731273459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
