﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineMeshModifier
struct SplineMeshModifier_t3237667319;

#include "codegen/il2cpp-codegen.h"

// System.Void SplineMeshModifier::.ctor()
extern "C"  void SplineMeshModifier__ctor_m2526488916 (SplineMeshModifier_t3237667319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
