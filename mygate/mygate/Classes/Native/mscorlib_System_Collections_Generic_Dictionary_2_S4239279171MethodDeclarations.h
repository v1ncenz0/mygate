﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>
struct ShimEnumerator_t4239279171;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2496836611_gshared (ShimEnumerator_t4239279171 * __this, Dictionary_2_t4134154350 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2496836611(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4239279171 *, Dictionary_2_t4134154350 *, const MethodInfo*))ShimEnumerator__ctor_m2496836611_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3448010494_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3448010494(__this, method) ((  bool (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_MoveNext_m3448010494_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m953076558_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m953076558(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_get_Entry_m953076558_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1916645267_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1916645267(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_get_Key_m1916645267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2519664987_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2519664987(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_get_Value_m2519664987_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4293776181_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4293776181(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_get_Current_m4293776181_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Vector3,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2756762081_gshared (ShimEnumerator_t4239279171 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2756762081(__this, method) ((  void (*) (ShimEnumerator_t4239279171 *, const MethodInfo*))ShimEnumerator_Reset_m2756762081_gshared)(__this, method)
