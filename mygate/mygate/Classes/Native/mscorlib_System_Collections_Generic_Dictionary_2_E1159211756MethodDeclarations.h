﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1159211756.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3707044332_gshared (Enumerator_t1159211756 * __this, Dictionary_2_t4134154350 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3707044332(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1159211756 *, Dictionary_2_t4134154350 *, const MethodInfo*))Enumerator__ctor_m3707044332_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m359647425_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m359647425(__this, method) ((  Il2CppObject * (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m359647425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3678245697_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3678245697(__this, method) ((  void (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3678245697_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3603310410_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3603310410(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3603310410_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1384610131_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1384610131(__this, method) ((  Il2CppObject * (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1384610131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3475591035_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3475591035(__this, method) ((  Il2CppObject * (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3475591035_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m850524225_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m850524225(__this, method) ((  bool (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_MoveNext_m850524225_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1891499572  Enumerator_get_Current_m3161629733_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3161629733(__this, method) ((  KeyValuePair_2_t1891499572  (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_get_Current_m3161629733_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::get_CurrentKey()
extern "C"  Vector3_t2243707580  Enumerator_get_CurrentKey_m2950231828_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2950231828(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_get_CurrentKey_m2950231828_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m965660436_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m965660436(__this, method) ((  Il2CppObject * (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_get_CurrentValue_m965660436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m4021617514_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_Reset_m4021617514(__this, method) ((  void (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_Reset_m4021617514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3966567443_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3966567443(__this, method) ((  void (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_VerifyState_m3966567443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2762011769_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2762011769(__this, method) ((  void (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_VerifyCurrent_m2762011769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m776557428_gshared (Enumerator_t1159211756 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m776557428(__this, method) ((  void (*) (Enumerator_t1159211756 *, const MethodInfo*))Enumerator_Dispose_m776557428_gshared)(__this, method)
