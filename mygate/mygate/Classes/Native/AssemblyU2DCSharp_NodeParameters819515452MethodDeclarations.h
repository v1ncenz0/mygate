﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NodeParameters
struct NodeParameters_t819515452;
// Spline
struct Spline_t1260612603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"

// System.Void NodeParameters::.ctor(Spline,System.Single,System.Single)
extern "C"  void NodeParameters__ctor_m3388517344 (NodeParameters_t819515452 * __this, Spline_t1260612603 * ___spline0, float ___position1, float ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NodeParameters::get_PosInSpline()
extern "C"  float NodeParameters_get_PosInSpline_m3318096330 (NodeParameters_t819515452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NodeParameters::get_Length()
extern "C"  float NodeParameters_get_Length_m286728136 (NodeParameters_t819515452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NodeParameters::Reset()
extern "C"  void NodeParameters_Reset_m3697488780 (NodeParameters_t819515452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
