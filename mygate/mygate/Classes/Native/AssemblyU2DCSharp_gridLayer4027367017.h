﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemGate
struct ItemGate_t2548129788;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// ItemColor
struct ItemColor_t1117508970;
// Spline
struct Spline_t1260612603;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gridLayer
struct  gridLayer_t4027367017  : public MonoBehaviour_t1158329972
{
public:
	// ItemGate gridLayer::gate
	ItemGate_t2548129788 * ___gate_3;
	// UnityEngine.Vector2 gridLayer::maxOffsetDim
	Vector2_t2243707579  ___maxOffsetDim_4;
	// UnityEngine.Vector2 gridLayer::actualPosition
	Vector2_t2243707579  ___actualPosition_5;
	// UnityEngine.Vector2 gridLayer::actualPositionRight
	Vector2_t2243707579  ___actualPositionRight_6;
	// UnityEngine.Vector3 gridLayer::hatStartingPoint
	Vector3_t2243707580  ___hatStartingPoint_7;
	// UnityEngine.GameObject gridLayer::divisorGO
	GameObject_t1756533147 * ___divisorGO_8;
	// System.Single gridLayer::divisor_offset
	float ___divisor_offset_9;
	// System.Boolean gridLayer::hasDivFrame
	bool ___hasDivFrame_10;
	// System.Int32 gridLayer::Z
	int32_t ___Z_11;
	// UnityEngine.Vector3 gridLayer::startingPoint
	Vector3_t2243707580  ___startingPoint_12;
	// UnityEngine.Vector3 gridLayer::startingPointRight
	Vector3_t2243707580  ___startingPointRight_13;
	// System.Boolean gridLayer::isActive
	bool ___isActive_14;
	// UnityEngine.Color gridLayer::colorgrid
	Color_t2020392075  ___colorgrid_15;
	// System.Collections.ArrayList gridLayer::matrix
	ArrayList_t4252133567 * ___matrix_16;
	// System.Int32 gridLayer::numLayer
	int32_t ___numLayer_17;
	// ItemColor gridLayer::colorlayer
	ItemColor_t1117508970 * ___colorlayer_18;
	// System.Boolean gridLayer::is_gridshow
	bool ___is_gridshow_19;
	// Spline gridLayer::hatSpline
	Spline_t1260612603 * ___hatSpline_20;
	// UnityEngine.GameObject gridLayer::gridView
	GameObject_t1756533147 * ___gridView_21;
	// UnityEngine.GameObject gridLayer::gridViewElements
	GameObject_t1756533147 * ___gridViewElements_22;
	// UnityEngine.GameObject gridLayer::gridViewGridElements
	GameObject_t1756533147 * ___gridViewGridElements_23;
	// UnityEngine.GameObject gridLayer::gridViewRight
	GameObject_t1756533147 * ___gridViewRight_24;
	// UnityEngine.GameObject gridLayer::gridViewElementsRight
	GameObject_t1756533147 * ___gridViewElementsRight_25;
	// UnityEngine.GameObject gridLayer::gridViewGridElementsRight
	GameObject_t1756533147 * ___gridViewGridElementsRight_26;
	// UnityEngine.GameObject gridLayer::gridViewHat
	GameObject_t1756533147 * ___gridViewHat_27;
	// UnityEngine.GameObject gridLayer::gridViewHatElements
	GameObject_t1756533147 * ___gridViewHatElements_28;
	// UnityEngine.GameObject gridLayer::gridViewHatGridElements
	GameObject_t1756533147 * ___gridViewHatGridElements_29;
	// UnityEngine.GameObject gridLayer::gridViewHatRight
	GameObject_t1756533147 * ___gridViewHatRight_30;
	// UnityEngine.GameObject gridLayer::gridViewHatElementsRight
	GameObject_t1756533147 * ___gridViewHatElementsRight_31;
	// UnityEngine.GameObject gridLayer::gridViewHatGridElementsRight
	GameObject_t1756533147 * ___gridViewHatGridElementsRight_32;

public:
	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gate_3)); }
	inline ItemGate_t2548129788 * get_gate_3() const { return ___gate_3; }
	inline ItemGate_t2548129788 ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(ItemGate_t2548129788 * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_maxOffsetDim_4() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___maxOffsetDim_4)); }
	inline Vector2_t2243707579  get_maxOffsetDim_4() const { return ___maxOffsetDim_4; }
	inline Vector2_t2243707579 * get_address_of_maxOffsetDim_4() { return &___maxOffsetDim_4; }
	inline void set_maxOffsetDim_4(Vector2_t2243707579  value)
	{
		___maxOffsetDim_4 = value;
	}

	inline static int32_t get_offset_of_actualPosition_5() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___actualPosition_5)); }
	inline Vector2_t2243707579  get_actualPosition_5() const { return ___actualPosition_5; }
	inline Vector2_t2243707579 * get_address_of_actualPosition_5() { return &___actualPosition_5; }
	inline void set_actualPosition_5(Vector2_t2243707579  value)
	{
		___actualPosition_5 = value;
	}

	inline static int32_t get_offset_of_actualPositionRight_6() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___actualPositionRight_6)); }
	inline Vector2_t2243707579  get_actualPositionRight_6() const { return ___actualPositionRight_6; }
	inline Vector2_t2243707579 * get_address_of_actualPositionRight_6() { return &___actualPositionRight_6; }
	inline void set_actualPositionRight_6(Vector2_t2243707579  value)
	{
		___actualPositionRight_6 = value;
	}

	inline static int32_t get_offset_of_hatStartingPoint_7() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___hatStartingPoint_7)); }
	inline Vector3_t2243707580  get_hatStartingPoint_7() const { return ___hatStartingPoint_7; }
	inline Vector3_t2243707580 * get_address_of_hatStartingPoint_7() { return &___hatStartingPoint_7; }
	inline void set_hatStartingPoint_7(Vector3_t2243707580  value)
	{
		___hatStartingPoint_7 = value;
	}

	inline static int32_t get_offset_of_divisorGO_8() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___divisorGO_8)); }
	inline GameObject_t1756533147 * get_divisorGO_8() const { return ___divisorGO_8; }
	inline GameObject_t1756533147 ** get_address_of_divisorGO_8() { return &___divisorGO_8; }
	inline void set_divisorGO_8(GameObject_t1756533147 * value)
	{
		___divisorGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___divisorGO_8, value);
	}

	inline static int32_t get_offset_of_divisor_offset_9() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___divisor_offset_9)); }
	inline float get_divisor_offset_9() const { return ___divisor_offset_9; }
	inline float* get_address_of_divisor_offset_9() { return &___divisor_offset_9; }
	inline void set_divisor_offset_9(float value)
	{
		___divisor_offset_9 = value;
	}

	inline static int32_t get_offset_of_hasDivFrame_10() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___hasDivFrame_10)); }
	inline bool get_hasDivFrame_10() const { return ___hasDivFrame_10; }
	inline bool* get_address_of_hasDivFrame_10() { return &___hasDivFrame_10; }
	inline void set_hasDivFrame_10(bool value)
	{
		___hasDivFrame_10 = value;
	}

	inline static int32_t get_offset_of_Z_11() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___Z_11)); }
	inline int32_t get_Z_11() const { return ___Z_11; }
	inline int32_t* get_address_of_Z_11() { return &___Z_11; }
	inline void set_Z_11(int32_t value)
	{
		___Z_11 = value;
	}

	inline static int32_t get_offset_of_startingPoint_12() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___startingPoint_12)); }
	inline Vector3_t2243707580  get_startingPoint_12() const { return ___startingPoint_12; }
	inline Vector3_t2243707580 * get_address_of_startingPoint_12() { return &___startingPoint_12; }
	inline void set_startingPoint_12(Vector3_t2243707580  value)
	{
		___startingPoint_12 = value;
	}

	inline static int32_t get_offset_of_startingPointRight_13() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___startingPointRight_13)); }
	inline Vector3_t2243707580  get_startingPointRight_13() const { return ___startingPointRight_13; }
	inline Vector3_t2243707580 * get_address_of_startingPointRight_13() { return &___startingPointRight_13; }
	inline void set_startingPointRight_13(Vector3_t2243707580  value)
	{
		___startingPointRight_13 = value;
	}

	inline static int32_t get_offset_of_isActive_14() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___isActive_14)); }
	inline bool get_isActive_14() const { return ___isActive_14; }
	inline bool* get_address_of_isActive_14() { return &___isActive_14; }
	inline void set_isActive_14(bool value)
	{
		___isActive_14 = value;
	}

	inline static int32_t get_offset_of_colorgrid_15() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___colorgrid_15)); }
	inline Color_t2020392075  get_colorgrid_15() const { return ___colorgrid_15; }
	inline Color_t2020392075 * get_address_of_colorgrid_15() { return &___colorgrid_15; }
	inline void set_colorgrid_15(Color_t2020392075  value)
	{
		___colorgrid_15 = value;
	}

	inline static int32_t get_offset_of_matrix_16() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___matrix_16)); }
	inline ArrayList_t4252133567 * get_matrix_16() const { return ___matrix_16; }
	inline ArrayList_t4252133567 ** get_address_of_matrix_16() { return &___matrix_16; }
	inline void set_matrix_16(ArrayList_t4252133567 * value)
	{
		___matrix_16 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_16, value);
	}

	inline static int32_t get_offset_of_numLayer_17() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___numLayer_17)); }
	inline int32_t get_numLayer_17() const { return ___numLayer_17; }
	inline int32_t* get_address_of_numLayer_17() { return &___numLayer_17; }
	inline void set_numLayer_17(int32_t value)
	{
		___numLayer_17 = value;
	}

	inline static int32_t get_offset_of_colorlayer_18() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___colorlayer_18)); }
	inline ItemColor_t1117508970 * get_colorlayer_18() const { return ___colorlayer_18; }
	inline ItemColor_t1117508970 ** get_address_of_colorlayer_18() { return &___colorlayer_18; }
	inline void set_colorlayer_18(ItemColor_t1117508970 * value)
	{
		___colorlayer_18 = value;
		Il2CppCodeGenWriteBarrier(&___colorlayer_18, value);
	}

	inline static int32_t get_offset_of_is_gridshow_19() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___is_gridshow_19)); }
	inline bool get_is_gridshow_19() const { return ___is_gridshow_19; }
	inline bool* get_address_of_is_gridshow_19() { return &___is_gridshow_19; }
	inline void set_is_gridshow_19(bool value)
	{
		___is_gridshow_19 = value;
	}

	inline static int32_t get_offset_of_hatSpline_20() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___hatSpline_20)); }
	inline Spline_t1260612603 * get_hatSpline_20() const { return ___hatSpline_20; }
	inline Spline_t1260612603 ** get_address_of_hatSpline_20() { return &___hatSpline_20; }
	inline void set_hatSpline_20(Spline_t1260612603 * value)
	{
		___hatSpline_20 = value;
		Il2CppCodeGenWriteBarrier(&___hatSpline_20, value);
	}

	inline static int32_t get_offset_of_gridView_21() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridView_21)); }
	inline GameObject_t1756533147 * get_gridView_21() const { return ___gridView_21; }
	inline GameObject_t1756533147 ** get_address_of_gridView_21() { return &___gridView_21; }
	inline void set_gridView_21(GameObject_t1756533147 * value)
	{
		___gridView_21 = value;
		Il2CppCodeGenWriteBarrier(&___gridView_21, value);
	}

	inline static int32_t get_offset_of_gridViewElements_22() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewElements_22)); }
	inline GameObject_t1756533147 * get_gridViewElements_22() const { return ___gridViewElements_22; }
	inline GameObject_t1756533147 ** get_address_of_gridViewElements_22() { return &___gridViewElements_22; }
	inline void set_gridViewElements_22(GameObject_t1756533147 * value)
	{
		___gridViewElements_22 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewElements_22, value);
	}

	inline static int32_t get_offset_of_gridViewGridElements_23() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewGridElements_23)); }
	inline GameObject_t1756533147 * get_gridViewGridElements_23() const { return ___gridViewGridElements_23; }
	inline GameObject_t1756533147 ** get_address_of_gridViewGridElements_23() { return &___gridViewGridElements_23; }
	inline void set_gridViewGridElements_23(GameObject_t1756533147 * value)
	{
		___gridViewGridElements_23 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewGridElements_23, value);
	}

	inline static int32_t get_offset_of_gridViewRight_24() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewRight_24)); }
	inline GameObject_t1756533147 * get_gridViewRight_24() const { return ___gridViewRight_24; }
	inline GameObject_t1756533147 ** get_address_of_gridViewRight_24() { return &___gridViewRight_24; }
	inline void set_gridViewRight_24(GameObject_t1756533147 * value)
	{
		___gridViewRight_24 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewRight_24, value);
	}

	inline static int32_t get_offset_of_gridViewElementsRight_25() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewElementsRight_25)); }
	inline GameObject_t1756533147 * get_gridViewElementsRight_25() const { return ___gridViewElementsRight_25; }
	inline GameObject_t1756533147 ** get_address_of_gridViewElementsRight_25() { return &___gridViewElementsRight_25; }
	inline void set_gridViewElementsRight_25(GameObject_t1756533147 * value)
	{
		___gridViewElementsRight_25 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewElementsRight_25, value);
	}

	inline static int32_t get_offset_of_gridViewGridElementsRight_26() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewGridElementsRight_26)); }
	inline GameObject_t1756533147 * get_gridViewGridElementsRight_26() const { return ___gridViewGridElementsRight_26; }
	inline GameObject_t1756533147 ** get_address_of_gridViewGridElementsRight_26() { return &___gridViewGridElementsRight_26; }
	inline void set_gridViewGridElementsRight_26(GameObject_t1756533147 * value)
	{
		___gridViewGridElementsRight_26 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewGridElementsRight_26, value);
	}

	inline static int32_t get_offset_of_gridViewHat_27() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHat_27)); }
	inline GameObject_t1756533147 * get_gridViewHat_27() const { return ___gridViewHat_27; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHat_27() { return &___gridViewHat_27; }
	inline void set_gridViewHat_27(GameObject_t1756533147 * value)
	{
		___gridViewHat_27 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHat_27, value);
	}

	inline static int32_t get_offset_of_gridViewHatElements_28() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHatElements_28)); }
	inline GameObject_t1756533147 * get_gridViewHatElements_28() const { return ___gridViewHatElements_28; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHatElements_28() { return &___gridViewHatElements_28; }
	inline void set_gridViewHatElements_28(GameObject_t1756533147 * value)
	{
		___gridViewHatElements_28 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHatElements_28, value);
	}

	inline static int32_t get_offset_of_gridViewHatGridElements_29() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHatGridElements_29)); }
	inline GameObject_t1756533147 * get_gridViewHatGridElements_29() const { return ___gridViewHatGridElements_29; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHatGridElements_29() { return &___gridViewHatGridElements_29; }
	inline void set_gridViewHatGridElements_29(GameObject_t1756533147 * value)
	{
		___gridViewHatGridElements_29 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHatGridElements_29, value);
	}

	inline static int32_t get_offset_of_gridViewHatRight_30() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHatRight_30)); }
	inline GameObject_t1756533147 * get_gridViewHatRight_30() const { return ___gridViewHatRight_30; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHatRight_30() { return &___gridViewHatRight_30; }
	inline void set_gridViewHatRight_30(GameObject_t1756533147 * value)
	{
		___gridViewHatRight_30 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHatRight_30, value);
	}

	inline static int32_t get_offset_of_gridViewHatElementsRight_31() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHatElementsRight_31)); }
	inline GameObject_t1756533147 * get_gridViewHatElementsRight_31() const { return ___gridViewHatElementsRight_31; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHatElementsRight_31() { return &___gridViewHatElementsRight_31; }
	inline void set_gridViewHatElementsRight_31(GameObject_t1756533147 * value)
	{
		___gridViewHatElementsRight_31 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHatElementsRight_31, value);
	}

	inline static int32_t get_offset_of_gridViewHatGridElementsRight_32() { return static_cast<int32_t>(offsetof(gridLayer_t4027367017, ___gridViewHatGridElementsRight_32)); }
	inline GameObject_t1756533147 * get_gridViewHatGridElementsRight_32() const { return ___gridViewHatGridElementsRight_32; }
	inline GameObject_t1756533147 ** get_address_of_gridViewHatGridElementsRight_32() { return &___gridViewHatGridElementsRight_32; }
	inline void set_gridViewHatGridElementsRight_32(GameObject_t1756533147 * value)
	{
		___gridViewHatGridElementsRight_32 = value;
		Il2CppCodeGenWriteBarrier(&___gridViewHatGridElementsRight_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
