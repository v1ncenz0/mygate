﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline
struct Spline_t1260612603;
// SplineNode[]
struct SplineNodeU5BU5D_t2523538654;
// SplineSegment[]
struct SplineSegmentU5BU5D_t3814610003;
// SplineSegment
struct SplineSegment_t1083783318;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SplineNode
struct SplineNode_t3003005095;
// Spline/DistanceFunction
struct DistanceFunction_t3700963585;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Spline_SegmentParameter3125294186.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "AssemblyU2DCSharp_Spline_DistanceFunction3700963585.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Spline::.ctor()
extern "C"  void Spline__ctor_m894960208 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::get_Length()
extern "C"  float Spline_get_Length_m2666030433 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Spline::get_AutoClose()
extern "C"  bool Spline_get_AutoClose_m1011689996 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::get_NodesPerSegment()
extern "C"  int32_t Spline_get_NodesPerSegment_m3393263892 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::get_SegmentCount()
extern "C"  int32_t Spline_get_SegmentCount_m270115081 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Spline::get_HasBeenUpdated()
extern "C"  bool Spline_get_HasBeenUpdated_m2966535552 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::get_UpdateFrame()
extern "C"  int32_t Spline_get_UpdateFrame_m2712067009 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::get_ControlNodeCount()
extern "C"  int32_t Spline_get_ControlNodeCount_m574301893 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Spline::get_InvertedAccuracy()
extern "C"  double Spline_get_InvertedAccuracy_m2363431598 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Spline::get_IsBezier()
extern "C"  bool Spline_get_IsBezier_m974423430 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Spline::get_HasNodes()
extern "C"  bool Spline_get_HasNodes_m2787075340 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode[] Spline::get_SplineNodes()
extern "C"  SplineNodeU5BU5D_t2523538654* Spline_get_SplineNodes_m3528958249 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode[] Spline::get_SegmentNodes()
extern "C"  SplineNodeU5BU5D_t2523538654* Spline_get_SegmentNodes_m2319015795 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineSegment[] Spline::get_SplineSegments()
extern "C"  SplineSegmentU5BU5D_t3814610003* Spline_get_SplineSegments_m3861480849 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::OnEnable()
extern "C"  void Spline_OnEnable_m302717884 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::LateUpdate()
extern "C"  void Spline_LateUpdate_m1639623627 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::UpdateSpline()
extern "C"  void Spline_UpdateSpline_m2474060938 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetPositionOnSpline(System.Single)
extern "C"  Vector3_t2243707580  Spline_GetPositionOnSpline_m2119374910 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetTangentToSpline(System.Single)
extern "C"  Vector3_t2243707580  Spline_GetTangentToSpline_m2732324982 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetNormalToSpline(System.Single)
extern "C"  Vector3_t2243707580  Spline_GetNormalToSpline_m641548806 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetCurvatureOfSpline(System.Single)
extern "C"  Vector3_t2243707580  Spline_GetCurvatureOfSpline_m1806795538 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Spline::GetOrientationOnSpline(System.Single)
extern "C"  Quaternion_t4030073918  Spline_GetOrientationOnSpline_m3681912419 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetCustomValueOnSpline(System.Single)
extern "C"  float Spline_GetCustomValueOnSpline_m4031913051 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetPositionInternal(Spline/SegmentParameter)
extern "C"  Vector3_t2243707580  Spline_GetPositionInternal_m1339181056 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetTangentInternal(Spline/SegmentParameter)
extern "C"  Vector3_t2243707580  Spline_GetTangentInternal_m3484399004 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetNormalInternal(Spline/SegmentParameter)
extern "C"  Vector3_t2243707580  Spline_GetNormalInternal_m1918477260 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Spline::GetCurvatureInternal(Spline/SegmentParameter)
extern "C"  Vector3_t2243707580  Spline_GetCurvatureInternal_m253558928 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetValueInternal(Spline/SegmentParameter)
extern "C"  float Spline_GetValueInternal_m2585206216 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Spline::GetRotationInternal(Spline/SegmentParameter)
extern "C"  Quaternion_t4030073918  Spline_GetRotationInternal_m1557068727 (Spline_t1260612603 * __this, SegmentParameter_t3125294186  ___sParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineSegment Spline::GetSplineSegment(System.Single)
extern "C"  SplineSegment_t1083783318 * Spline_GetSplineSegment_m586134332 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::ConvertNormalizedParameterToDistance(System.Single)
extern "C"  float Spline_ConvertNormalizedParameterToDistance_m3956265628 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::ConvertDistanceToNormalizedParameter(System.Single)
extern "C"  float Spline_ConvertDistanceToNormalizedParameter_m2287745164 (Spline_t1260612603 * __this, float ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Spline::AddSplineNode()
extern "C"  GameObject_t1756533147 * Spline_AddSplineNode_m368565765 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Spline::AddSplineNode(System.Single)
extern "C"  GameObject_t1756533147 * Spline_AddSplineNode_m4206971706 (Spline_t1260612603 * __this, float ___normalizedParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Spline::AddSplineNode(SplineNode)
extern "C"  GameObject_t1756533147 * Spline_AddSplineNode_m2386603498 (Spline_t1260612603 * __this, SplineNode_t3003005095 * ___precedingNode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::RemoveSplineNode(UnityEngine.GameObject)
extern "C"  void Spline_RemoveSplineNode_m1043454069 (Spline_t1260612603 * __this, GameObject_t1756533147 * ___gObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::RemoveSplineNode(SplineNode)
extern "C"  void Spline_RemoveSplineNode_m208762060 (Spline_t1260612603 * __this, SplineNode_t3003005095 * ___splineNode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Spline/SegmentParameter Spline::RecalculateParameter(System.Double)
extern "C"  SegmentParameter_t3125294186  Spline_RecalculateParameter_m741432761 (Spline_t1260612603 * __this, double ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode Spline::GetNode(System.Int32,System.Int32)
extern "C"  SplineNode_t3003005095 * Spline_GetNode_m3779691980 (Spline_t1260612603 * __this, int32_t ___idxNode0, int32_t ___idxOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::ReparameterizeCurve()
extern "C"  void Spline_ReparameterizeCurve_m3044801657 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::MaxNodeIndex()
extern "C"  int32_t Spline_MaxNodeIndex_m2297636602 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spline::GetRelevantNodeCount(System.Int32)
extern "C"  int32_t Spline_GetRelevantNodeCount_m4031276479 (Spline_t1260612603 * __this, int32_t ___nodeCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Spline::EnoughNodes(System.Int32)
extern "C"  bool Spline_EnoughNodes_m1774450354 (Spline_t1260612603 * __this, int32_t ___nodeCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetClosestPointParam(UnityEngine.Vector3,System.Int32,System.Single,System.Single,System.Single)
extern "C"  float Spline_GetClosestPointParam_m1912992189 (Spline_t1260612603 * __this, Vector3_t2243707580  ___point0, int32_t ___iterations1, float ___start2, float ___end3, float ___step4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetClosestPointParamToRay(UnityEngine.Ray,System.Int32,System.Single,System.Single,System.Single)
extern "C"  float Spline_GetClosestPointParamToRay_m3212039726 (Spline_t1260612603 * __this, Ray_t2469606224  ___ray0, int32_t ___iterations1, float ___start2, float ___end3, float ___step4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetClosestPointParamToPlane(UnityEngine.Plane,System.Int32,System.Single,System.Single,System.Single)
extern "C"  float Spline_GetClosestPointParamToPlane_m2606949838 (Spline_t1260612603 * __this, Plane_t3727654732  ___plane0, int32_t ___iterations1, float ___start2, float ___end3, float ___step4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetClosestPointParamIntern(Spline/DistanceFunction,System.Int32,System.Single,System.Single,System.Single)
extern "C"  float Spline_GetClosestPointParamIntern_m2820984149 (Spline_t1260612603 * __this, DistanceFunction_t3700963585 * ___distFnc0, int32_t ___iterations1, float ___start2, float ___end3, float ___step4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetClosestPointParamOnSegmentIntern(Spline/DistanceFunction,System.Single,System.Single,System.Single)
extern "C"  float Spline_GetClosestPointParamOnSegmentIntern_m413341722 (Spline_t1260612603 * __this, DistanceFunction_t3700963585 * ___distFnc0, float ___start1, float ___end2, float ___step3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::OnDrawGizmos()
extern "C"  void Spline_OnDrawGizmos_m974251562 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::OnDrawGizmosSelected()
extern "C"  void Spline_OnDrawGizmosSelected_m1538033891 (Spline_t1260612603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spline::DrawSplineGizmo(UnityEngine.Color)
extern "C"  void Spline_DrawSplineGizmo_m166909947 (Spline_t1260612603 * __this, Color_t2020392075  ___curveColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline::GetSizeMultiplier(SplineNode)
extern "C"  float Spline_GetSizeMultiplier_m1511616869 (Spline_t1260612603 * __this, SplineNode_t3003005095 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Spline::GetSegmentLengthInternal(System.Int32,System.Double,System.Double,System.Double)
extern "C"  double Spline_GetSegmentLengthInternal_m3113690866 (Spline_t1260612603 * __this, int32_t ___idxFirstPoint0, double ___startValue1, double ___endValue2, double ___step3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
