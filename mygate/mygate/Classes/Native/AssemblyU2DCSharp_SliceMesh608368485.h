﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliceMesh
struct  SliceMesh_t608368485  : public MonoBehaviour_t1158329972
{
public:
	// Spline SliceMesh::spline
	Spline_t1260612603 * ___spline_2;
	// System.Single SliceMesh::t0
	float ___t0_3;
	// System.Single SliceMesh::t1
	float ___t1_4;

public:
	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(SliceMesh_t608368485, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}

	inline static int32_t get_offset_of_t0_3() { return static_cast<int32_t>(offsetof(SliceMesh_t608368485, ___t0_3)); }
	inline float get_t0_3() const { return ___t0_3; }
	inline float* get_address_of_t0_3() { return &___t0_3; }
	inline void set_t0_3(float value)
	{
		___t0_3 = value;
	}

	inline static int32_t get_offset_of_t1_4() { return static_cast<int32_t>(offsetof(SliceMesh_t608368485, ___t1_4)); }
	inline float get_t1_4() const { return ___t1_4; }
	inline float* get_address_of_t1_4() { return &___t1_4; }
	inline void set_t1_4(float value)
	{
		___t1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
