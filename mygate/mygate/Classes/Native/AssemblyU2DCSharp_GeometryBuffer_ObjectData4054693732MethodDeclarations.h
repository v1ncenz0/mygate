﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GeometryBuffer/ObjectData
struct ObjectData_t4054693732;

#include "codegen/il2cpp-codegen.h"

// System.Void GeometryBuffer/ObjectData::.ctor()
extern "C"  void ObjectData__ctor_m3201256431 (ObjectData_t4054693732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
