﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Item
struct Item_t2440468191;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_CursorElement_ModeList3850132356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CursorElement
struct  CursorElement_t1656362794  : public MonoBehaviour_t1158329972
{
public:
	// CursorElement/ModeList CursorElement::mode
	int32_t ___mode_4;
	// System.Boolean CursorElement::isInit
	bool ___isInit_7;
	// System.Boolean CursorElement::isMouseMove
	bool ___isMouseMove_8;
	// System.Boolean CursorElement::isTouch
	bool ___isTouch_9;
	// UnityEngine.Vector3 CursorElement::firstPositionMouse
	Vector3_t2243707580  ___firstPositionMouse_10;
	// UnityEngine.Vector3 CursorElement::initialPositionCursor
	Vector3_t2243707580  ___initialPositionCursor_11;
	// System.Boolean CursorElement::isDragged
	bool ___isDragged_12;
	// UnityEngine.Vector3 CursorElement::oldPosition
	Vector3_t2243707580  ___oldPosition_13;
	// System.Single CursorElement::clicked
	float ___clicked_14;
	// System.Single CursorElement::clicktime
	float ___clicktime_15;
	// System.Single CursorElement::clickdelay
	float ___clickdelay_16;

public:
	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_isInit_7() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___isInit_7)); }
	inline bool get_isInit_7() const { return ___isInit_7; }
	inline bool* get_address_of_isInit_7() { return &___isInit_7; }
	inline void set_isInit_7(bool value)
	{
		___isInit_7 = value;
	}

	inline static int32_t get_offset_of_isMouseMove_8() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___isMouseMove_8)); }
	inline bool get_isMouseMove_8() const { return ___isMouseMove_8; }
	inline bool* get_address_of_isMouseMove_8() { return &___isMouseMove_8; }
	inline void set_isMouseMove_8(bool value)
	{
		___isMouseMove_8 = value;
	}

	inline static int32_t get_offset_of_isTouch_9() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___isTouch_9)); }
	inline bool get_isTouch_9() const { return ___isTouch_9; }
	inline bool* get_address_of_isTouch_9() { return &___isTouch_9; }
	inline void set_isTouch_9(bool value)
	{
		___isTouch_9 = value;
	}

	inline static int32_t get_offset_of_firstPositionMouse_10() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___firstPositionMouse_10)); }
	inline Vector3_t2243707580  get_firstPositionMouse_10() const { return ___firstPositionMouse_10; }
	inline Vector3_t2243707580 * get_address_of_firstPositionMouse_10() { return &___firstPositionMouse_10; }
	inline void set_firstPositionMouse_10(Vector3_t2243707580  value)
	{
		___firstPositionMouse_10 = value;
	}

	inline static int32_t get_offset_of_initialPositionCursor_11() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___initialPositionCursor_11)); }
	inline Vector3_t2243707580  get_initialPositionCursor_11() const { return ___initialPositionCursor_11; }
	inline Vector3_t2243707580 * get_address_of_initialPositionCursor_11() { return &___initialPositionCursor_11; }
	inline void set_initialPositionCursor_11(Vector3_t2243707580  value)
	{
		___initialPositionCursor_11 = value;
	}

	inline static int32_t get_offset_of_isDragged_12() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___isDragged_12)); }
	inline bool get_isDragged_12() const { return ___isDragged_12; }
	inline bool* get_address_of_isDragged_12() { return &___isDragged_12; }
	inline void set_isDragged_12(bool value)
	{
		___isDragged_12 = value;
	}

	inline static int32_t get_offset_of_oldPosition_13() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___oldPosition_13)); }
	inline Vector3_t2243707580  get_oldPosition_13() const { return ___oldPosition_13; }
	inline Vector3_t2243707580 * get_address_of_oldPosition_13() { return &___oldPosition_13; }
	inline void set_oldPosition_13(Vector3_t2243707580  value)
	{
		___oldPosition_13 = value;
	}

	inline static int32_t get_offset_of_clicked_14() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___clicked_14)); }
	inline float get_clicked_14() const { return ___clicked_14; }
	inline float* get_address_of_clicked_14() { return &___clicked_14; }
	inline void set_clicked_14(float value)
	{
		___clicked_14 = value;
	}

	inline static int32_t get_offset_of_clicktime_15() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___clicktime_15)); }
	inline float get_clicktime_15() const { return ___clicktime_15; }
	inline float* get_address_of_clicktime_15() { return &___clicktime_15; }
	inline void set_clicktime_15(float value)
	{
		___clicktime_15 = value;
	}

	inline static int32_t get_offset_of_clickdelay_16() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794, ___clickdelay_16)); }
	inline float get_clickdelay_16() const { return ___clickdelay_16; }
	inline float* get_address_of_clickdelay_16() { return &___clickdelay_16; }
	inline void set_clickdelay_16(float value)
	{
		___clickdelay_16 = value;
	}
};

struct CursorElement_t1656362794_StaticFields
{
public:
	// UnityEngine.Vector2 CursorElement::dimCellGridTouched
	Vector2_t2243707579  ___dimCellGridTouched_2;
	// UnityEngine.Vector3 CursorElement::elementDim
	Vector3_t2243707580  ___elementDim_3;
	// Item CursorElement::currentItem
	Item_t2440468191 * ___currentItem_5;
	// System.Boolean CursorElement::currentCellToCut
	bool ___currentCellToCut_6;
	// System.Action`1<System.Boolean> CursorElement::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_dimCellGridTouched_2() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794_StaticFields, ___dimCellGridTouched_2)); }
	inline Vector2_t2243707579  get_dimCellGridTouched_2() const { return ___dimCellGridTouched_2; }
	inline Vector2_t2243707579 * get_address_of_dimCellGridTouched_2() { return &___dimCellGridTouched_2; }
	inline void set_dimCellGridTouched_2(Vector2_t2243707579  value)
	{
		___dimCellGridTouched_2 = value;
	}

	inline static int32_t get_offset_of_elementDim_3() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794_StaticFields, ___elementDim_3)); }
	inline Vector3_t2243707580  get_elementDim_3() const { return ___elementDim_3; }
	inline Vector3_t2243707580 * get_address_of_elementDim_3() { return &___elementDim_3; }
	inline void set_elementDim_3(Vector3_t2243707580  value)
	{
		___elementDim_3 = value;
	}

	inline static int32_t get_offset_of_currentItem_5() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794_StaticFields, ___currentItem_5)); }
	inline Item_t2440468191 * get_currentItem_5() const { return ___currentItem_5; }
	inline Item_t2440468191 ** get_address_of_currentItem_5() { return &___currentItem_5; }
	inline void set_currentItem_5(Item_t2440468191 * value)
	{
		___currentItem_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_5, value);
	}

	inline static int32_t get_offset_of_currentCellToCut_6() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794_StaticFields, ___currentCellToCut_6)); }
	inline bool get_currentCellToCut_6() const { return ___currentCellToCut_6; }
	inline bool* get_address_of_currentCellToCut_6() { return &___currentCellToCut_6; }
	inline void set_currentCellToCut_6(bool value)
	{
		___currentCellToCut_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(CursorElement_t1656362794_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
