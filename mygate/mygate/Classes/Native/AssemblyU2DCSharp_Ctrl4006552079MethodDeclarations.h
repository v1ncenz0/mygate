﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ctrl
struct Ctrl_t4006552079;

#include "codegen/il2cpp-codegen.h"

// System.Void Ctrl::.ctor()
extern "C"  void Ctrl__ctor_m415933796 (Ctrl_t4006552079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ctrl::Update()
extern "C"  void Ctrl_Update_m2930431571 (Ctrl_t4006552079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ctrl::setEditMode()
extern "C"  void Ctrl_setEditMode_m341290251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ctrl::setDeleteMode()
extern "C"  void Ctrl_setDeleteMode_m1705595336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ctrl::.cctor()
extern "C"  void Ctrl__cctor_m1646827233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
