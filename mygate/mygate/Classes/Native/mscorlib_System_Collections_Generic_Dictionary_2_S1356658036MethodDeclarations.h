﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>
struct ShimEnumerator_t1356658036;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3431099424_gshared (ShimEnumerator_t1356658036 * __this, Dictionary_2_t1251533215 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3431099424(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1356658036 *, Dictionary_2_t1251533215 *, const MethodInfo*))ShimEnumerator__ctor_m3431099424_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3809291801_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3809291801(__this, method) ((  bool (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_MoveNext_m3809291801_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2849841413_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2849841413(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_get_Entry_m2849841413_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2442335216_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2442335216(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_get_Key_m2442335216_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1742106106_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1742106106(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_get_Value_m1742106106_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m210980734_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m210980734(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_get_Current_m210980734_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.Vector3>::Reset()
extern "C"  void ShimEnumerator_Reset_m622771266_gshared (ShimEnumerator_t1356658036 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m622771266(__this, method) ((  void (*) (ShimEnumerator_t1356658036 *, const MethodInfo*))ShimEnumerator_Reset_m622771266_gshared)(__this, method)
