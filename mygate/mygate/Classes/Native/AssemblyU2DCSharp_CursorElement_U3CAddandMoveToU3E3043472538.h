﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CursorElement
struct CursorElement_t1656362794;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CursorElement/<AddandMoveTo>c__AnonStorey0
struct  U3CAddandMoveToU3Ec__AnonStorey0_t3043472538  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 CursorElement/<AddandMoveTo>c__AnonStorey0::direction
	Vector2_t2243707579  ___direction_0;
	// CursorElement CursorElement/<AddandMoveTo>c__AnonStorey0::$this
	CursorElement_t1656362794 * ___U24this_1;

public:
	inline static int32_t get_offset_of_direction_0() { return static_cast<int32_t>(offsetof(U3CAddandMoveToU3Ec__AnonStorey0_t3043472538, ___direction_0)); }
	inline Vector2_t2243707579  get_direction_0() const { return ___direction_0; }
	inline Vector2_t2243707579 * get_address_of_direction_0() { return &___direction_0; }
	inline void set_direction_0(Vector2_t2243707579  value)
	{
		___direction_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddandMoveToU3Ec__AnonStorey0_t3043472538, ___U24this_1)); }
	inline CursorElement_t1656362794 * get_U24this_1() const { return ___U24this_1; }
	inline CursorElement_t1656362794 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CursorElement_t1656362794 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
