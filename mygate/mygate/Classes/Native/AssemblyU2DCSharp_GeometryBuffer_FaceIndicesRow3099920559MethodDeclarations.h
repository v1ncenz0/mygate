﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GeometryBuffer/FaceIndicesRow
struct FaceIndicesRow_t3099920559;

#include "codegen/il2cpp-codegen.h"

// System.Void GeometryBuffer/FaceIndicesRow::.ctor()
extern "C"  void FaceIndicesRow__ctor_m1901239332 (FaceIndicesRow_t3099920559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
