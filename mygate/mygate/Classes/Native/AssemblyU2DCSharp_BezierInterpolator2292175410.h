﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SplineInterpolator4279526764.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BezierInterpolator
struct  BezierInterpolator_t2292175410  : public SplineInterpolator_t4279526764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
