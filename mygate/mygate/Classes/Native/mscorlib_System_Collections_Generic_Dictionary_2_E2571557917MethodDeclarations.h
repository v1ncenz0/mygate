﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2571557917.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23303845733.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3259537371_gshared (Enumerator_t2571557917 * __this, Dictionary_2_t1251533215 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3259537371(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2571557917 *, Dictionary_2_t1251533215 *, const MethodInfo*))Enumerator__ctor_m3259537371_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3172023274_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3172023274(__this, method) ((  Il2CppObject * (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3172023274_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m374200428_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m374200428(__this, method) ((  void (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m374200428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1053062465_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1053062465(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1053062465_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1295800932_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1295800932(__this, method) ((  Il2CppObject * (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1295800932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m716313350_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m716313350(__this, method) ((  Il2CppObject * (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m716313350_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2272884836_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2272884836(__this, method) ((  bool (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_MoveNext_m2272884836_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::get_Current()
extern "C"  KeyValuePair_2_t3303845733  Enumerator_get_Current_m1434609380_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1434609380(__this, method) ((  KeyValuePair_2_t3303845733  (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_get_Current_m1434609380_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1635107391_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1635107391(__this, method) ((  int32_t (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_get_CurrentKey_m1635107391_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::get_CurrentValue()
extern "C"  Vector3_t2243707580  Enumerator_get_CurrentValue_m3705057799_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3705057799(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_get_CurrentValue_m3705057799_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::Reset()
extern "C"  void Enumerator_Reset_m1784401753_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1784401753(__this, method) ((  void (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_Reset_m1784401753_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2106023556_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2106023556(__this, method) ((  void (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_VerifyState_m2106023556_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m702157852_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m702157852(__this, method) ((  void (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_VerifyCurrent_m702157852_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m3538521975_gshared (Enumerator_t2571557917 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3538521975(__this, method) ((  void (*) (Enumerator_t2571557917 *, const MethodInfo*))Enumerator_Dispose_m3538521975_gshared)(__this, method)
