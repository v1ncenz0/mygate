﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "AssemblyU2DCSharp_SplineMeshModifier3237667319.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineCurveScaleModifier
struct  SplineCurveScaleModifier_t1538173695  : public SplineMeshModifier_t3237667319
{
public:
	// UnityEngine.AnimationCurve SplineCurveScaleModifier::scaleCurve
	AnimationCurve_t3306541151 * ___scaleCurve_2;

public:
	inline static int32_t get_offset_of_scaleCurve_2() { return static_cast<int32_t>(offsetof(SplineCurveScaleModifier_t1538173695, ___scaleCurve_2)); }
	inline AnimationCurve_t3306541151 * get_scaleCurve_2() const { return ___scaleCurve_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_scaleCurve_2() { return &___scaleCurve_2; }
	inline void set_scaleCurve_2(AnimationCurve_t3306541151 * value)
	{
		___scaleCurve_2 = value;
		Il2CppCodeGenWriteBarrier(&___scaleCurve_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
