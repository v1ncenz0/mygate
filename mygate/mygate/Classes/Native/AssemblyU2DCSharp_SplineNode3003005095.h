﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SplineNode/NodeParameterRegister
struct NodeParameterRegister_t3732266052;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineNode
struct  SplineNode_t3003005095  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SplineNode::customValue
	float ___customValue_2;
	// System.Single SplineNode::tension
	float ___tension_3;
	// UnityEngine.Vector3 SplineNode::normal
	Vector3_t2243707580  ___normal_4;
	// SplineNode/NodeParameterRegister SplineNode::parameters
	NodeParameterRegister_t3732266052 * ___parameters_5;

public:
	inline static int32_t get_offset_of_customValue_2() { return static_cast<int32_t>(offsetof(SplineNode_t3003005095, ___customValue_2)); }
	inline float get_customValue_2() const { return ___customValue_2; }
	inline float* get_address_of_customValue_2() { return &___customValue_2; }
	inline void set_customValue_2(float value)
	{
		___customValue_2 = value;
	}

	inline static int32_t get_offset_of_tension_3() { return static_cast<int32_t>(offsetof(SplineNode_t3003005095, ___tension_3)); }
	inline float get_tension_3() const { return ___tension_3; }
	inline float* get_address_of_tension_3() { return &___tension_3; }
	inline void set_tension_3(float value)
	{
		___tension_3 = value;
	}

	inline static int32_t get_offset_of_normal_4() { return static_cast<int32_t>(offsetof(SplineNode_t3003005095, ___normal_4)); }
	inline Vector3_t2243707580  get_normal_4() const { return ___normal_4; }
	inline Vector3_t2243707580 * get_address_of_normal_4() { return &___normal_4; }
	inline void set_normal_4(Vector3_t2243707580  value)
	{
		___normal_4 = value;
	}

	inline static int32_t get_offset_of_parameters_5() { return static_cast<int32_t>(offsetof(SplineNode_t3003005095, ___parameters_5)); }
	inline NodeParameterRegister_t3732266052 * get_parameters_5() const { return ___parameters_5; }
	inline NodeParameterRegister_t3732266052 ** get_address_of_parameters_5() { return &___parameters_5; }
	inline void set_parameters_5(NodeParameterRegister_t3732266052 * value)
	{
		___parameters_5 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
