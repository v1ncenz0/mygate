﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Events
struct Events_t192668321;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Events::.ctor()
extern "C"  void Events__ctor_m1141878417 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::Start()
extern "C"  void Events_Start_m1985659401 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setEditMode()
extern "C"  void Events_setEditMode_m2634528918 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setDeleteMode()
extern "C"  void Events_setDeleteMode_m334080453 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::commandLayer(System.Object)
extern "C"  void Events_commandLayer_m3803116123 (Events_t192668321 * __this, Il2CppObject * ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::commandAction(System.Object)
extern "C"  void Events_commandAction_m1454428482 (Events_t192668321 * __this, Il2CppObject * ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::changeLayer(System.Int32)
extern "C"  void Events_changeLayer_m1113474337 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setElement(System.Int32)
extern "C"  void Events_setElement_m3367502596 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setColor(System.Int32)
extern "C"  void Events_setColor_m226370423 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setColorHex(System.String)
extern "C"  void Events_setColorHex_m389828153 (Events_t192668321 * __this, String_t* ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setColorFrame(System.Int32)
extern "C"  void Events_setColorFrame_m166263492 (Events_t192668321 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setColorLayer(System.String)
extern "C"  void Events_setColorLayer_m18571855 (Events_t192668321 * __this, String_t* ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::resetColorLayer()
extern "C"  void Events_resetColorLayer_m2071387886 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::getColorLayer(System.Int32)
extern "C"  void Events_getColorLayer_m1025765032 (Events_t192668321 * __this, int32_t ___id_layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::getColorFrame()
extern "C"  void Events_getColorFrame_m3384491399 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::moveCamera(System.Int32)
extern "C"  void Events_moveCamera_m402994146 (Events_t192668321 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::resetCamera()
extern "C"  void Events_resetCamera_m2930362505 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::navigation(System.String)
extern "C"  void Events_navigation_m1350350129 (Events_t192668321 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setSceneVisible(System.Int32)
extern "C"  void Events_setSceneVisible_m2812210446 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setSpecularMode(System.Int32)
extern "C"  void Events_setSpecularMode_m820216658 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::invertSpecularMode()
extern "C"  void Events_invertSpecularMode_m3455852047 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setVisibilityLayer(System.String)
extern "C"  void Events_setVisibilityLayer_m4014999546 (Events_t192668321 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::noGrid(System.Int32)
extern "C"  void Events_noGrid_m558178953 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::invertGrid()
extern "C"  void Events_invertGrid_m2987816943 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::fillGrid()
extern "C"  void Events_fillGrid_m2499476784 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::eraseGrid()
extern "C"  void Events_eraseGrid_m4232299893 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::captureScreen()
extern "C"  void Events_captureScreen_m1162566373 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::saveGate()
extern "C"  void Events_saveGate_m1664254519 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::exportToOBJ()
extern "C"  void Events_exportToOBJ_m2776330801 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::setSaveWithScreen(System.Int32)
extern "C"  void Events_setSaveWithScreen_m3163682793 (Events_t192668321 * __this, int32_t ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::invertSceneForRender()
extern "C"  void Events_invertSceneForRender_m2728888782 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::showSceneForRender()
extern "C"  void Events_showSceneForRender_m186653017 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::showCAD()
extern "C"  void Events_showCAD_m1715081090 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::init(System.String)
extern "C"  void Events_init_m3829042537 (Events_t192668321 * __this, String_t* ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::showPrice()
extern "C"  void Events_showPrice_m3589980459 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::back()
extern "C"  void Events_back_m2714829062 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Events::Main()
extern "C"  void Events_Main_m1659229276 (Events_t192668321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
