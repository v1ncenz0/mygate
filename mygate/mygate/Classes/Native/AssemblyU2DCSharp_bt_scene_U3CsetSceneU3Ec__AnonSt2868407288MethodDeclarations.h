﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_scene/<setScene>c__AnonStorey1
struct U3CsetSceneU3Ec__AnonStorey1_t2868407288;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void bt_scene/<setScene>c__AnonStorey1::.ctor()
extern "C"  void U3CsetSceneU3Ec__AnonStorey1__ctor_m307537627 (U3CsetSceneU3Ec__AnonStorey1_t2868407288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene/<setScene>c__AnonStorey1::<>m__0(UnityEngine.Texture2D)
extern "C"  void U3CsetSceneU3Ec__AnonStorey1_U3CU3Em__0_m3991771740 (U3CsetSceneU3Ec__AnonStorey1_t2868407288 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene/<setScene>c__AnonStorey1::<>m__1(UnityEngine.Texture2D)
extern "C"  void U3CsetSceneU3Ec__AnonStorey1_U3CU3Em__1_m1236327483 (U3CsetSceneU3Ec__AnonStorey1_t2868407288 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
