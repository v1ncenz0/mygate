﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Double[]
struct DoubleU5BU5D_t1889952540;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline/LengthData
struct  LengthData_t3176416758  : public Il2CppObject
{
public:
	// System.Double[] Spline/LengthData::subSegmentLength
	DoubleU5BU5D_t1889952540* ___subSegmentLength_0;
	// System.Double[] Spline/LengthData::subSegmentPosition
	DoubleU5BU5D_t1889952540* ___subSegmentPosition_1;
	// System.Double Spline/LengthData::length
	double ___length_2;

public:
	inline static int32_t get_offset_of_subSegmentLength_0() { return static_cast<int32_t>(offsetof(LengthData_t3176416758, ___subSegmentLength_0)); }
	inline DoubleU5BU5D_t1889952540* get_subSegmentLength_0() const { return ___subSegmentLength_0; }
	inline DoubleU5BU5D_t1889952540** get_address_of_subSegmentLength_0() { return &___subSegmentLength_0; }
	inline void set_subSegmentLength_0(DoubleU5BU5D_t1889952540* value)
	{
		___subSegmentLength_0 = value;
		Il2CppCodeGenWriteBarrier(&___subSegmentLength_0, value);
	}

	inline static int32_t get_offset_of_subSegmentPosition_1() { return static_cast<int32_t>(offsetof(LengthData_t3176416758, ___subSegmentPosition_1)); }
	inline DoubleU5BU5D_t1889952540* get_subSegmentPosition_1() const { return ___subSegmentPosition_1; }
	inline DoubleU5BU5D_t1889952540** get_address_of_subSegmentPosition_1() { return &___subSegmentPosition_1; }
	inline void set_subSegmentPosition_1(DoubleU5BU5D_t1889952540* value)
	{
		___subSegmentPosition_1 = value;
		Il2CppCodeGenWriteBarrier(&___subSegmentPosition_1, value);
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(LengthData_t3176416758, ___length_2)); }
	inline double get_length_2() const { return ___length_2; }
	inline double* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(double value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
