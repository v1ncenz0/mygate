﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<GeometryBuffer/ObjectData>
struct List_1_t3423814864;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// GeometryBuffer/ObjectData
struct ObjectData_t4054693732;
// GeometryBuffer/GroupData
struct GroupData_t2204692426;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeometryBuffer
struct  GeometryBuffer_t1734374890  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<GeometryBuffer/ObjectData> GeometryBuffer::objects
	List_1_t3423814864 * ___objects_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GeometryBuffer::vertices
	List_1_t1612828712 * ___vertices_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> GeometryBuffer::uvs
	List_1_t1612828711 * ___uvs_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GeometryBuffer::normals
	List_1_t1612828712 * ___normals_3;
	// System.Int32 GeometryBuffer::unnamedGroupIndex
	int32_t ___unnamedGroupIndex_4;
	// GeometryBuffer/ObjectData GeometryBuffer::current
	ObjectData_t4054693732 * ___current_5;
	// GeometryBuffer/GroupData GeometryBuffer::curgr
	GroupData_t2204692426 * ___curgr_6;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___objects_0)); }
	inline List_1_t3423814864 * get_objects_0() const { return ___objects_0; }
	inline List_1_t3423814864 ** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(List_1_t3423814864 * value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier(&___objects_0, value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___vertices_1)); }
	inline List_1_t1612828712 * get_vertices_1() const { return ___vertices_1; }
	inline List_1_t1612828712 ** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(List_1_t1612828712 * value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_1, value);
	}

	inline static int32_t get_offset_of_uvs_2() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___uvs_2)); }
	inline List_1_t1612828711 * get_uvs_2() const { return ___uvs_2; }
	inline List_1_t1612828711 ** get_address_of_uvs_2() { return &___uvs_2; }
	inline void set_uvs_2(List_1_t1612828711 * value)
	{
		___uvs_2 = value;
		Il2CppCodeGenWriteBarrier(&___uvs_2, value);
	}

	inline static int32_t get_offset_of_normals_3() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___normals_3)); }
	inline List_1_t1612828712 * get_normals_3() const { return ___normals_3; }
	inline List_1_t1612828712 ** get_address_of_normals_3() { return &___normals_3; }
	inline void set_normals_3(List_1_t1612828712 * value)
	{
		___normals_3 = value;
		Il2CppCodeGenWriteBarrier(&___normals_3, value);
	}

	inline static int32_t get_offset_of_unnamedGroupIndex_4() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___unnamedGroupIndex_4)); }
	inline int32_t get_unnamedGroupIndex_4() const { return ___unnamedGroupIndex_4; }
	inline int32_t* get_address_of_unnamedGroupIndex_4() { return &___unnamedGroupIndex_4; }
	inline void set_unnamedGroupIndex_4(int32_t value)
	{
		___unnamedGroupIndex_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___current_5)); }
	inline ObjectData_t4054693732 * get_current_5() const { return ___current_5; }
	inline ObjectData_t4054693732 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(ObjectData_t4054693732 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier(&___current_5, value);
	}

	inline static int32_t get_offset_of_curgr_6() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890, ___curgr_6)); }
	inline GroupData_t2204692426 * get_curgr_6() const { return ___curgr_6; }
	inline GroupData_t2204692426 ** get_address_of_curgr_6() { return &___curgr_6; }
	inline void set_curgr_6(GroupData_t2204692426 * value)
	{
		___curgr_6 = value;
		Il2CppCodeGenWriteBarrier(&___curgr_6, value);
	}
};

struct GeometryBuffer_t1734374890_StaticFields
{
public:
	// System.Int32 GeometryBuffer::MAX_VERTICES_LIMIT_FOR_A_MESH
	int32_t ___MAX_VERTICES_LIMIT_FOR_A_MESH_7;

public:
	inline static int32_t get_offset_of_MAX_VERTICES_LIMIT_FOR_A_MESH_7() { return static_cast<int32_t>(offsetof(GeometryBuffer_t1734374890_StaticFields, ___MAX_VERTICES_LIMIT_FOR_A_MESH_7)); }
	inline int32_t get_MAX_VERTICES_LIMIT_FOR_A_MESH_7() const { return ___MAX_VERTICES_LIMIT_FOR_A_MESH_7; }
	inline int32_t* get_address_of_MAX_VERTICES_LIMIT_FOR_A_MESH_7() { return &___MAX_VERTICES_LIMIT_FOR_A_MESH_7; }
	inline void set_MAX_VERTICES_LIMIT_FOR_A_MESH_7(int32_t value)
	{
		___MAX_VERTICES_LIMIT_FOR_A_MESH_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
