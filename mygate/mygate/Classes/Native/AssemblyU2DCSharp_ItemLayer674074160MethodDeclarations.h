﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemLayer
struct ItemLayer_t674074160;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemLayer::.ctor()
extern "C"  void ItemLayer__ctor_m70466477 (ItemLayer_t674074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
