﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<ThreadaddCell>c__Iterator0
struct U3CThreadaddCellU3Ec__Iterator0_t1030518217;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<ThreadaddCell>c__Iterator0::.ctor()
extern "C"  void U3CThreadaddCellU3Ec__Iterator0__ctor_m1552852426 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign/<ThreadaddCell>c__Iterator0::MoveNext()
extern "C"  bool U3CThreadaddCellU3Ec__Iterator0_MoveNext_m171441982 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<ThreadaddCell>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CThreadaddCellU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m746383458 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ControlsDesign/<ThreadaddCell>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CThreadaddCellU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1391524378 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<ThreadaddCell>c__Iterator0::Dispose()
extern "C"  void U3CThreadaddCellU3Ec__Iterator0_Dispose_m2126418195 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<ThreadaddCell>c__Iterator0::Reset()
extern "C"  void U3CThreadaddCellU3Ec__Iterator0_Reset_m475345337 (U3CThreadaddCellU3Ec__Iterator0_t1030518217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
