﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Hat2
struct Hat2_t1573290449;

#include "codegen/il2cpp-codegen.h"

// System.Void Hat2::.ctor()
extern "C"  void Hat2__ctor_m2555243212 (Hat2_t1573290449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
