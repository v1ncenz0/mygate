﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BranchingSpline
struct BranchingSpline_t3375729607;
// BranchingSplineParameter
struct BranchingSplineParameter_t1831757700;
// BranchingSpline/BranchingController
struct BranchingController_t3837965438;
// BranchingSplinePath
struct BranchingSplinePath_t1574214842;
// SplineNode
struct SplineNode_t3003005095;
// Spline
struct Spline_t1260612603;
// SplineNode[]
struct SplineNodeU5BU5D_t2523538654;
// System.Collections.Generic.IList`1<Spline>
struct IList_1_t1801553204;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BranchingSplineParameter1831757700.h"
#include "AssemblyU2DCSharp_BranchingSpline_BranchingControl3837965438.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_SplineNode3003005095.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "AssemblyU2DCSharp_BranchingSplinePath_Direction1768093872.h"

// System.Void BranchingSpline::.ctor()
extern "C"  void BranchingSpline__ctor_m3448647788 (BranchingSpline_t3375729607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BranchingSpline::Advance(BranchingSplineParameter,System.Single,BranchingSpline/BranchingController)
extern "C"  bool BranchingSpline_Advance_m2297184111 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, float ___distanceOffset1, BranchingController_t3837965438 * ___bController2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BranchingSpline::GetPosition(BranchingSplineParameter)
extern "C"  Vector3_t2243707580  BranchingSpline_GetPosition_m4032225291 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion BranchingSpline::GetOrientation(BranchingSplineParameter)
extern "C"  Quaternion_t4030073918  BranchingSpline_GetOrientation_m2447530346 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BranchingSpline::GetTangent(BranchingSplineParameter)
extern "C"  Vector3_t2243707580  BranchingSpline_GetTangent_m912461813 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BranchingSpline::GetCustomValue(BranchingSplineParameter)
extern "C"  float BranchingSpline_GetCustomValue_m3838744712 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BranchingSpline::GetNormal(BranchingSplineParameter)
extern "C"  Vector3_t2243707580  BranchingSpline_GetNormal_m1778978149 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BranchingSplinePath BranchingSpline::ChoseSpline(SplineNode,BranchingSplineParameter,BranchingSpline/BranchingController,System.Boolean)
extern "C"  BranchingSplinePath_t1574214842 * BranchingSpline_ChoseSpline_m2907943358 (BranchingSpline_t3375729607 * __this, SplineNode_t3003005095 * ___switchNode0, BranchingSplineParameter_t1831757700 * ___currentPath1, BranchingController_t3837965438 * ___bController2, bool ___positiveValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode BranchingSpline::IsOnSplineNode(System.Single,Spline)
extern "C"  SplineNode_t3003005095 * BranchingSpline_IsOnSplineNode_m2469271380 (BranchingSpline_t3375729607 * __this, float ___param0, Spline_t1260612603 * ___spline1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SplineNode[] BranchingSpline::GetAdjacentSegmentNodes(Spline,SplineNode)
extern "C"  SplineNodeU5BU5D_t2523538654* BranchingSpline_GetAdjacentSegmentNodes_m3720321004 (BranchingSpline_t3375729607 * __this, Spline_t1260612603 * ___spline0, SplineNode_t3003005095 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BranchingSpline::ForwardOnSpline(BranchingSplinePath/Direction,System.Single)
extern "C"  bool BranchingSpline_ForwardOnSpline_m1334507572 (BranchingSpline_t3375729607 * __this, int32_t ___direction0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BranchingSpline::IsMiddleNode(Spline,SplineNode)
extern "C"  bool BranchingSpline_IsMiddleNode_m391890625 (BranchingSpline_t3375729607 * __this, Spline_t1260612603 * ___spline0, SplineNode_t3003005095 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BranchingSpline::get_SplinesAvailable()
extern "C"  bool BranchingSpline_get_SplinesAvailable_m2570089152 (BranchingSpline_t3375729607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Spline> BranchingSpline::GetSplinesForNode(SplineNode)
extern "C"  Il2CppObject* BranchingSpline_GetSplinesForNode_m3001449889 (BranchingSpline_t3375729607 * __this, SplineNode_t3003005095 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BranchingSpline::CheckParameter(BranchingSplineParameter)
extern "C"  void BranchingSpline_CheckParameter_m1147539705 (BranchingSpline_t3375729607 * __this, BranchingSplineParameter_t1831757700 * ___bParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
