﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BezierInterpolator
struct BezierInterpolator_t2292175410;

#include "codegen/il2cpp-codegen.h"

// System.Void BezierInterpolator::.ctor()
extern "C"  void BezierInterpolator__ctor_m3633131203 (BezierInterpolator_t2292175410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
