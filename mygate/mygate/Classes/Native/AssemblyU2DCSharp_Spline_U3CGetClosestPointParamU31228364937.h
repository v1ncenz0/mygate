﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline/<GetClosestPointParam>c__AnonStorey0
struct  U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Spline/<GetClosestPointParam>c__AnonStorey0::point
	Vector3_t2243707580  ___point_0;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937, ___point_0)); }
	inline Vector3_t2243707580  get_point_0() const { return ___point_0; }
	inline Vector3_t2243707580 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector3_t2243707580  value)
	{
		___point_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
