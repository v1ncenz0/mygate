﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Vector3,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct Transform_1_t868377499;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Vector3,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4240600310_gshared (Transform_1_t868377499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m4240600310(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t868377499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4240600310_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Vector3,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1891499572  Transform_1_Invoke_m2472163702_gshared (Transform_1_t868377499 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2472163702(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1891499572  (*) (Transform_1_t868377499 *, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2472163702_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Vector3,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1571510559_gshared (Transform_1_t868377499 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1571510559(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t868377499 *, Vector3_t2243707580 , Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1571510559_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Vector3,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1891499572  Transform_1_EndInvoke_m1655521100_gshared (Transform_1_t868377499 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1655521100(__this, ___result0, method) ((  KeyValuePair_2_t1891499572  (*) (Transform_1_t868377499 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1655521100_gshared)(__this, ___result0, method)
