﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemGate
struct ItemGate_t2548129788;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_detail
struct  bt_detail_t551551266  : public MonoBehaviour_t1158329972
{
public:
	// ItemGate bt_detail::gate
	ItemGate_t2548129788 * ___gate_2;

public:
	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(bt_detail_t551551266, ___gate_2)); }
	inline ItemGate_t2548129788 * get_gate_2() const { return ___gate_2; }
	inline ItemGate_t2548129788 ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(ItemGate_t2548129788 * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier(&___gate_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
