﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SliceMesh
struct SliceMesh_t608368485;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SliceMesh::.ctor()
extern "C"  void SliceMesh__ctor_m1156676378 (SliceMesh_t608368485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliceMesh::Start()
extern "C"  void SliceMesh_Start_m2447066410 (SliceMesh_t608368485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliceMesh::SliceIt()
extern "C"  void SliceMesh_SliceIt_m2136787001 (SliceMesh_t608368485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SliceMesh::isExternalPoint(UnityEngine.Vector3,UnityEngine.Vector3&)
extern "C"  bool SliceMesh_isExternalPoint_m783341117 (SliceMesh_t608368485 * __this, Vector3_t2243707580  ___point0, Vector3_t2243707580 * ___newPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
