﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CursorEraser
struct CursorEraser_t4121456910;

#include "codegen/il2cpp-codegen.h"

// System.Void CursorEraser::.ctor()
extern "C"  void CursorEraser__ctor_m1449729255 (CursorEraser_t4121456910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorEraser::Start()
extern "C"  void CursorEraser_Start_m4004336219 (CursorEraser_t4121456910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorEraser::Update()
extern "C"  void CursorEraser_Update_m826338584 (CursorEraser_t4121456910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
