﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/<Load>c__Iterator0
struct U3CLoadU3Ec__Iterator0_t4200827971;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/<Load>c__Iterator0::.ctor()
extern "C"  void U3CLoadU3Ec__Iterator0__ctor_m249424570 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OBJ/<Load>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadU3Ec__Iterator0_MoveNext_m3581960650 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBJ/<Load>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3064038354 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBJ/<Load>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4196096778 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<Load>c__Iterator0::Dispose()
extern "C"  void U3CLoadU3Ec__Iterator0_Dispose_m3686689097 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<Load>c__Iterator0::Reset()
extern "C"  void U3CLoadU3Ec__Iterator0_Reset_m1972470771 (U3CLoadU3Ec__Iterator0_t4200827971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
