﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineNode/NodeParameterRegister
struct NodeParameterRegister_t3732266052;
// NodeParameters
struct NodeParameters_t819515452;
// Spline
struct Spline_t1260612603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"

// System.Void SplineNode/NodeParameterRegister::.ctor()
extern "C"  void NodeParameterRegister__ctor_m193685841 (NodeParameterRegister_t3732266052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NodeParameters SplineNode/NodeParameterRegister::get_Item(Spline)
extern "C"  NodeParameters_t819515452 * NodeParameterRegister_get_Item_m1310526601 (NodeParameterRegister_t3732266052 * __this, Spline_t1260612603 * ___spline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
