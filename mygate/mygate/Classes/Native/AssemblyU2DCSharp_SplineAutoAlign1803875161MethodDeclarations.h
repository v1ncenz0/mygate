﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineAutoAlign
struct SplineAutoAlign_t1803875161;

#include "codegen/il2cpp-codegen.h"

// System.Void SplineAutoAlign::.ctor()
extern "C"  void SplineAutoAlign__ctor_m2968440230 (SplineAutoAlign_t1803875161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineAutoAlign::AutoAlign()
extern "C"  void SplineAutoAlign_AutoAlign_m1964233830 (SplineAutoAlign_t1803875161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
