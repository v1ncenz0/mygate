﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_init_gates3190639043.h"
#include "AssemblyU2DCSharp_bt_enable_layer839870442.h"
#include "AssemblyU2DCSharp_bt_show_colorlibrary3195776817.h"
#include "AssemblyU2DCSharp_bt_show_layer441805848.h"
#include "AssemblyU2DCSharp_params_colorlibrary2715541335.h"
#include "AssemblyU2DCSharp_bt_element1221534273.h"
#include "AssemblyU2DCSharp_bt_element_U3CsetImageU3Ec__Iter3057083896.h"
#include "AssemblyU2DCSharp_bt_scene1364368137.h"
#include "AssemblyU2DCSharp_bt_scene_U3CdownloadImageU3Ec__I3077030272.h"
#include "AssemblyU2DCSharp_bt_scene_U3CsetSceneU3Ec__AnonSt2868407288.h"
#include "AssemblyU2DCSharp_bt_login335186348.h"
#include "AssemblyU2DCSharp_bt_register2424814550.h"
#include "AssemblyU2DCSharp_init_login3323861352.h"
#include "AssemblyU2DCSharp_startWebGL4008935213.h"
#include "AssemblyU2DCSharp_bt_back_gates3731273459.h"
#include "AssemblyU2DCSharp_bt_opening174438555.h"
#include "AssemblyU2DCSharp_bt_standarddimension2912344586.h"
#include "AssemblyU2DCSharp_bt_start1297110715.h"
#include "AssemblyU2DCSharp_bt_wizard652226372.h"
#include "AssemblyU2DCSharp_init_start1142197987.h"
#include "AssemblyU2DCSharp_init_start_U3CdownloadImageU3Ec__621683668.h"
#include "AssemblyU2DCSharp_wizard1721110033.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "AssemblyU2DCSharp_JSONObject_Type1314578890.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONContents3850664647.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound865402053.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1259369279.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Ite1149809410.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__Ite716304657.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec4037879552.h"
#include "AssemblyU2DCSharp_LanguageManager2136445143.h"
#include "AssemblyU2DCSharp_Library4121003545.h"
#include "AssemblyU2DCSharp_ManageCoroutines3089936288.h"
#include "AssemblyU2DCSharp_MoveCamera3193441886.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"
#include "AssemblyU2DCSharp_GeometryBuffer1734374890.h"
#include "AssemblyU2DCSharp_GeometryBuffer_ObjectData4054693732.h"
#include "AssemblyU2DCSharp_GeometryBuffer_GroupData2204692426.h"
#include "AssemblyU2DCSharp_GeometryBuffer_FaceIndicesRow3099920559.h"
#include "AssemblyU2DCSharp_OBJ3241282509.h"
#include "AssemblyU2DCSharp_OBJ_ObjectFaces1920115861.h"
#include "AssemblyU2DCSharp_OBJ_MaterialData3551903345.h"
#include "AssemblyU2DCSharp_OBJ_BumpParamDef684364218.h"
#include "AssemblyU2DCSharp_OBJ_U3CStartLoadCoroutineU3Ec__A1715207021.h"
#include "AssemblyU2DCSharp_OBJ_U3CLoadU3Ec__Iterator04200827971.h"
#include "AssemblyU2DCSharp_OBJ_U3CLoadU3Ec__Iterator0_U3CLo2764713728.h"
#include "AssemblyU2DCSharp_OBJ_U3CAfterFileParsingU3Ec__Ite2191380560.h"
#include "AssemblyU2DCSharp_OBJ_U3CNoThreadU3Ec__AnonStorey43163858514.h"
#include "AssemblyU2DCSharp_OBJ_U3CReadObjDataU3Ec__AnonStor1414577351.h"
#include "AssemblyU2DCSharp_OBJItem2734286650.h"
#include "AssemblyU2DCSharp_ObjExporterScript792682685.h"
#include "AssemblyU2DCSharp_ObjExporter2277678484.h"
#include "AssemblyU2DCSharp_PinchZoom2955597597.h"
#include "AssemblyU2DCSharp_SliceMesh608368485.h"
#include "AssemblyU2DCSharp_StaticVariables4099069371.h"
#include "AssemblyU2DCSharp_Utility109704896.h"
#include "AssemblyU2DCSharp_webservice1109616683.h"
#include "AssemblyU2DCSharp_webservice_U3CInitInterfaceU3Ec_3794987368.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadStyleListU1849560148.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadTypeListU31032010496.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadGeometriesL809774299.h"
#include "AssemblyU2DCSharp_webservice_U3CPoolDownloadColorU1682189197.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadColorListU4106110459.h"
#include "AssemblyU2DCSharp_webservice_U3CdownloadElementU3E3800062637.h"
#include "AssemblyU2DCSharp_webservice_U3CdownloadElementU3E4203347164.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadScenesU3Ec1121704572.h"
#include "AssemblyU2DCSharp_webservice_U3CloginU3Ec__Iterato2972907461.h"
#include "AssemblyU2DCSharp_webservice_U3CDownloadGatesU3Ec_4257982057.h"
#include "AssemblyU2DCSharp_webservice_U3CdownloadGateFromDB3768071003.h"
#include "AssemblyU2DCSharp_Element2605769808.h"
#include "AssemblyU2DCSharp_Hat21573290449.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_ItemColor1117508970.h"
#include "AssemblyU2DCSharp_ItemColor_U3CdownloadTextureColo2545635092.h"
#include "AssemblyU2DCSharp_ItemGate2548129788.h"
#include "AssemblyU2DCSharp_ItemGate_U3CdownloadImageU3Ec__I1853153015.h"
#include "AssemblyU2DCSharp_ItemGeometry2838964733.h"
#include "AssemblyU2DCSharp_ItemGeometry_U3CdownloadImageU3E3125289710.h"
#include "AssemblyU2DCSharp_ItemLayer674074160.h"
#include "AssemblyU2DCSharp_ItemMisure2657053000.h"
#include "AssemblyU2DCSharp_ItemScene1151284345.h"
#include "AssemblyU2DCSharp_ItemStyle3530326308.h"
#include "AssemblyU2DCSharp_ItemStyle_U3CdownloadImageU3Ec__I141980167.h"
#include "AssemblyU2DCSharp_ItemType3455197591.h"
#include "AssemblyU2DCSharp_ItemType_U3CdownloadImageU3Ec__I1504783650.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONBinaryTag3856753551.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode1250409636.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode_U3CU3Ec__Iter803137928.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode_U3CU3Ec__Ite3532021283.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray3986483147.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray_U3CU3Ec__It4017688111.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray_U3CGetEnume1167240694.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass1609506608.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CRemoveU32562212806.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CU3Ec__Ite466495836.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CGetEnumer201440115.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONData531041784.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONLazyCreator3575446586.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSON3971185768.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (init_gates_t3190639043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[3] = 
{
	init_gates_t3190639043::get_offset_of_start_2(),
	init_gates_t3190639043::get_offset_of_limit_3(),
	init_gates_t3190639043::get_offset_of_wb_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (bt_enable_layer_t839870442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	bt_enable_layer_t839870442::get_offset_of_layer_color_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (bt_show_colorlibrary_t3195776817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	bt_show_colorlibrary_t3195776817::get_offset_of_id_layer_2(),
	bt_show_colorlibrary_t3195776817::get_offset_of_is_frame_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (bt_show_layer_t441805848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	bt_show_layer_t441805848::get_offset_of_id_layer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (params_colorlibrary_t2715541335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[2] = 
{
	params_colorlibrary_t2715541335::get_offset_of_id_layer_2(),
	params_colorlibrary_t2715541335::get_offset_of_is_frame_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (bt_element_t1221534273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	bt_element_t1221534273::get_offset_of_id_element_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (U3CsetImageU3Ec__Iterator0_t3057083896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[7] = 
{
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_url_image_0(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U3CwwwU3E__0_1(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U3CimgU3E__1_2(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U24this_3(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U24current_4(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U24disposing_5(),
	U3CsetImageU3Ec__Iterator0_t3057083896::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (bt_scene_t1364368137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	bt_scene_t1364368137::get_offset_of_scene_2(),
	bt_scene_t1364368137::get_offset_of_finishstatus_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t3077030272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[8] = 
{
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_url_image_0(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U3CwwwU3E__0_1(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U3CimgU3E__1_2(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_complete_3(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U24this_4(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U24current_5(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U24disposing_6(),
	U3CdownloadImageU3Ec__Iterator0_t3077030272::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (U3CsetSceneU3Ec__AnonStorey1_t2868407288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[6] = 
{
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_bg_0(),
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_wall_left_1(),
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_wall_right_2(),
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_column_left_3(),
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_column_right_4(),
	U3CsetSceneU3Ec__AnonStorey1_t2868407288::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (bt_login_t335186348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[1] = 
{
	bt_login_t335186348::get_offset_of_message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (bt_register_t2424814550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (init_login_t3323861352), -1, sizeof(init_login_t3323861352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	init_login_t3323861352_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (startWebGL_t4008935213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (bt_back_gates_t3731273459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (bt_opening_t174438555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (bt_standarddimension_t2912344586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	bt_standarddimension_t2912344586::get_offset_of_textfield_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (bt_start_t1297110715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (bt_wizard_t652226372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	bt_wizard_t652226372::get_offset_of_id_2(),
	bt_wizard_t652226372::get_offset_of_textureReplica_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (init_start_t1142197987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	init_start_t1142197987::get_offset_of_countStylesImage_2(),
	init_start_t1142197987::get_offset_of_pnl_3(),
	init_start_t1142197987::get_offset_of_opening_dropdown_4(),
	init_start_t1142197987::get_offset_of_currentIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t621683668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[7] = 
{
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_U3CimageU3E__0_0(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_image_path_1(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_U3CwwwU3E__1_2(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_finish_3(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_U24current_4(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_U24disposing_5(),
	U3CdownloadImageU3Ec__Iterator0_t621683668::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (wizard_t1721110033), -1, sizeof(wizard_t1721110033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1822[6] = 
{
	wizard_t1721110033_StaticFields::get_offset_of_id_type_2(),
	wizard_t1721110033_StaticFields::get_offset_of_id_style_3(),
	wizard_t1721110033_StaticFields::get_offset_of_id_geometry_4(),
	wizard_t1721110033_StaticFields::get_offset_of_width_gross_5(),
	wizard_t1721110033_StaticFields::get_offset_of_height_gross_6(),
	wizard_t1721110033_StaticFields::get_offset_of_opening_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (JSONObject_t1971882247), -1, sizeof(JSONObject_t1971882247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1823[13] = 
{
	0,
	0,
	0,
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_WHITESPACE_4(),
	JSONObject_t1971882247::get_offset_of_type_5(),
	JSONObject_t1971882247::get_offset_of_list_6(),
	JSONObject_t1971882247::get_offset_of_keys_7(),
	JSONObject_t1971882247::get_offset_of_str_8(),
	JSONObject_t1971882247::get_offset_of_n_9(),
	JSONObject_t1971882247::get_offset_of_b_10(),
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_printWatch_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Type_t1314578890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[8] = 
{
	Type_t1314578890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (AddJSONContents_t3850664647), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (FieldNotFound_t865402053), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (GetFieldResponse_t1259369279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (U3CBakeAsyncU3Ec__Iterator0_t1149809410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[6] = 
{
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24locvar0_0(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U3CsU3E__0_1(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24this_2(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24disposing_4(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (U3CPrintAsyncU3Ec__Iterator1_t716304657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[9] = 
{
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar0_2(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CeU3E__1_3(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar1_4(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24this_5(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24current_6(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24disposing_7(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (U3CStringifyAsyncU3Ec__Iterator2_t4037879552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[19] = 
{
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar0_1(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_builder_2(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_pretty_3(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__0_4(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CkeyU3E__1_5(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CobjU3E__2_6(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar1_7(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__3_8(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar2_9(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__4_10(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar3_11(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__5_12(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar4_13(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24this_14(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24current_15(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24disposing_16(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (LanguageManager_t2136445143), -1, sizeof(LanguageManager_t2136445143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[20] = 
{
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_WELCOME_TITLE_2(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_WELCOME_3(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_EDIT_TITLE_4(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_EDIT_5(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_DELETE_TITLE_6(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_DELETE_7(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_FILL_TITLE_8(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_FILL_9(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_ERASEGRID_TITLE_10(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_ERASEGRID_11(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_LAYER_TITLE_12(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_LAYERS_13(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_LIBRARY_TITLE_14(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_HELP_LIBRARY_15(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_MESSAGE_SAVE_16(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_BT_OK_CHECKOUT_17(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_BT_CONTINUE_DRAWING_18(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_MESSAGE_ALERT_BEFORE_CLOSE_19(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_BT_OK_20(),
	LanguageManager_t2136445143_StaticFields::get_offset_of_BT_QUIT_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Library_t4121003545), -1, sizeof(Library_t4121003545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[8] = 
{
	Library_t4121003545_StaticFields::get_offset_of_ItemsAvaible_2(),
	Library_t4121003545_StaticFields::get_offset_of_ColorsAvaible_3(),
	Library_t4121003545_StaticFields::get_offset_of_ScenesAvaible_4(),
	Library_t4121003545_StaticFields::get_offset_of_StylesAvaible_5(),
	Library_t4121003545_StaticFields::get_offset_of_TypesAvaible_6(),
	Library_t4121003545_StaticFields::get_offset_of_GeometriesAvaible_7(),
	Library_t4121003545_StaticFields::get_offset_of_GatesAvaible_8(),
	Library_t4121003545_StaticFields::get_offset_of_last_is_saved_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ManageCoroutines_t3089936288), -1, sizeof(ManageCoroutines_t3089936288_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	ManageCoroutines_t3089936288_StaticFields::get_offset_of_inProgress_2(),
	ManageCoroutines_t3089936288_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (MoveCamera_t3193441886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[35] = 
{
	MoveCamera_t3193441886::get_offset_of_turnSpeed_2(),
	MoveCamera_t3193441886::get_offset_of_panSpeed_3(),
	MoveCamera_t3193441886::get_offset_of_zoomSpeed_4(),
	MoveCamera_t3193441886::get_offset_of_zoomSpeedManual_5(),
	MoveCamera_t3193441886::get_offset_of_turnSpeed2_6(),
	MoveCamera_t3193441886::get_offset_of_mouseOrigin_7(),
	MoveCamera_t3193441886::get_offset_of_minFov_8(),
	MoveCamera_t3193441886::get_offset_of_maxFov_9(),
	MoveCamera_t3193441886::get_offset_of_sensitivity_10(),
	MoveCamera_t3193441886::get_offset_of_ZoomAmount_11(),
	MoveCamera_t3193441886::get_offset_of_MaxToClamp_12(),
	MoveCamera_t3193441886::get_offset_of_ROTSpeed_13(),
	MoveCamera_t3193441886::get_offset_of_manualZoomFactor_14(),
	MoveCamera_t3193441886::get_offset_of_frame_15(),
	MoveCamera_t3193441886::get_offset_of_initialTransform_16(),
	MoveCamera_t3193441886::get_offset_of_lastRotation_17(),
	MoveCamera_t3193441886::get_offset_of_lastTraslation_18(),
	MoveCamera_t3193441886::get_offset_of_puntoPivot_19(),
	MoveCamera_t3193441886::get_offset_of_lastVector_20(),
	MoveCamera_t3193441886::get_offset_of_mouseLeftDown_21(),
	MoveCamera_t3193441886::get_offset_of_mouseRightDown_22(),
	MoveCamera_t3193441886::get_offset_of_mouseMiddleDown_23(),
	MoveCamera_t3193441886::get_offset_of_isRotating_24(),
	MoveCamera_t3193441886::get_offset_of_initialCameraPos_25(),
	MoveCamera_t3193441886::get_offset_of_initialCameraRot_26(),
	MoveCamera_t3193441886::get_offset_of_lastCameraPos_27(),
	MoveCamera_t3193441886::get_offset_of_lastCameraRot_28(),
	MoveCamera_t3193441886::get_offset_of_manualRotation_29(),
	MoveCamera_t3193441886::get_offset_of_enableKeyboardMouse_30(),
	MoveCamera_t3193441886::get_offset_of_lastClick_31(),
	MoveCamera_t3193441886::get_offset_of_targetAngle_32(),
	MoveCamera_t3193441886::get_offset_of_targetAngle2_33(),
	0,
	MoveCamera_t3193441886::get_offset_of_rDistance_35(),
	MoveCamera_t3193441886::get_offset_of_rSpeed_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (FaceIndices_t174955414)+ sizeof (Il2CppObject), sizeof(FaceIndices_t174955414 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1835[3] = 
{
	FaceIndices_t174955414::get_offset_of_vi_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FaceIndices_t174955414::get_offset_of_vu_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FaceIndices_t174955414::get_offset_of_vn_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (GeometryBuffer_t1734374890), -1, sizeof(GeometryBuffer_t1734374890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[8] = 
{
	GeometryBuffer_t1734374890::get_offset_of_objects_0(),
	GeometryBuffer_t1734374890::get_offset_of_vertices_1(),
	GeometryBuffer_t1734374890::get_offset_of_uvs_2(),
	GeometryBuffer_t1734374890::get_offset_of_normals_3(),
	GeometryBuffer_t1734374890::get_offset_of_unnamedGroupIndex_4(),
	GeometryBuffer_t1734374890::get_offset_of_current_5(),
	GeometryBuffer_t1734374890::get_offset_of_curgr_6(),
	GeometryBuffer_t1734374890_StaticFields::get_offset_of_MAX_VERTICES_LIMIT_FOR_A_MESH_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ObjectData_t4054693732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[8] = 
{
	ObjectData_t4054693732::get_offset_of_name_0(),
	ObjectData_t4054693732::get_offset_of_groups_1(),
	ObjectData_t4054693732::get_offset_of_allFacesRow_2(),
	ObjectData_t4054693732::get_offset_of_vertices_3(),
	ObjectData_t4054693732::get_offset_of_uvs_4(),
	ObjectData_t4054693732::get_offset_of_normals_5(),
	ObjectData_t4054693732::get_offset_of_triangles_6(),
	ObjectData_t4054693732::get_offset_of_normalCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (GroupData_t2204692426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[3] = 
{
	GroupData_t2204692426::get_offset_of_name_0(),
	GroupData_t2204692426::get_offset_of_materialName_1(),
	GroupData_t2204692426::get_offset_of_faces_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (FaceIndicesRow_t3099920559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[1] = 
{
	FaceIndicesRow_t3099920559::get_offset_of_faceIndices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (OBJ_t3241282509), -1, sizeof(OBJ_t3241282509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[36] = 
{
	OBJ_t3241282509::get_offset_of_objPath_2(),
	OBJ_t3241282509::get_offset_of_loadOnStart_3(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	OBJ_t3241282509::get_offset_of_basepath_26(),
	OBJ_t3241282509::get_offset_of_mtllib_27(),
	OBJ_t3241282509::get_offset_of_buffer_28(),
	OBJ_t3241282509::get_offset_of_isDebug_29(),
	OBJ_t3241282509::get_offset_of_isFileParsingCompleted_30(),
	OBJ_t3241282509::get_offset_of_isAfterFileParsingCoroutineStarted_31(),
	OBJ_t3241282509::get_offset_of_finish_32(),
	OBJ_t3241282509::get_offset_of__fileParserThread_33(),
	OBJ_t3241282509::get_offset_of__fileLines_34(),
	OBJ_t3241282509::get_offset_of_materialData_35(),
	OBJ_t3241282509_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_36(),
	OBJ_t3241282509_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (ObjectFaces_t1920115861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[5] = 
{
	ObjectFaces_t1920115861::get_offset_of_obj_0(),
	ObjectFaces_t1920115861::get_offset_of_faces_1(),
	ObjectFaces_t1920115861::get_offset_of_verts_2(),
	ObjectFaces_t1920115861::get_offset_of_normals_3(),
	ObjectFaces_t1920115861::get_offset_of_uvs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (MaterialData_t3551903345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[11] = 
{
	MaterialData_t3551903345::get_offset_of_name_0(),
	MaterialData_t3551903345::get_offset_of_ambient_1(),
	MaterialData_t3551903345::get_offset_of_diffuse_2(),
	MaterialData_t3551903345::get_offset_of_specular_3(),
	MaterialData_t3551903345::get_offset_of_shininess_4(),
	MaterialData_t3551903345::get_offset_of_alpha_5(),
	MaterialData_t3551903345::get_offset_of_illumType_6(),
	MaterialData_t3551903345::get_offset_of_diffuseTexPath_7(),
	MaterialData_t3551903345::get_offset_of_bumpTexPath_8(),
	MaterialData_t3551903345::get_offset_of_diffuseTex_9(),
	MaterialData_t3551903345::get_offset_of_bumpTex_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (BumpParamDef_t684364218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[4] = 
{
	BumpParamDef_t684364218::get_offset_of_optionName_0(),
	BumpParamDef_t684364218::get_offset_of_valueType_1(),
	BumpParamDef_t684364218::get_offset_of_valueNumMin_2(),
	BumpParamDef_t684364218::get_offset_of_valueNumMax_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (U3CStartLoadCoroutineU3Ec__AnonStorey2_t1715207021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	U3CStartLoadCoroutineU3Ec__AnonStorey2_t1715207021::get_offset_of_is_finish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U3CLoadU3Ec__Iterator0_t4200827971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[9] = 
{
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_path_0(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U3CloaderU3E__0_1(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U3ClinesU3E__1_2(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_is_finish_3(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U24this_4(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U24current_5(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U24disposing_6(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U24PC_7(),
	U3CLoadU3Ec__Iterator0_t4200827971::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CLoadU3Ec__AnonStorey3_t2764713728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	U3CLoadU3Ec__AnonStorey3_t2764713728::get_offset_of_is_finish_0(),
	U3CLoadU3Ec__AnonStorey3_t2764713728::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (U3CAfterFileParsingU3Ec__Iterator1_t2191380560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[3] = 
{
	U3CAfterFileParsingU3Ec__Iterator1_t2191380560::get_offset_of_U24current_0(),
	U3CAfterFileParsingU3Ec__Iterator1_t2191380560::get_offset_of_U24disposing_1(),
	U3CAfterFileParsingU3Ec__Iterator1_t2191380560::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CNoThreadU3Ec__AnonStorey4_t3163858514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	U3CNoThreadU3Ec__AnonStorey4_t3163858514::get_offset_of_is_finish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (U3CReadObjDataU3Ec__AnonStorey5_t1414577351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[1] = 
{
	U3CReadObjDataU3Ec__AnonStorey5_t1414577351::get_offset_of_is_finish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (OBJItem_t2734286650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[3] = 
{
	OBJItem_t2734286650::get_offset_of_obj_0(),
	OBJItem_t2734286650::get_offset_of_mtl_1(),
	OBJItem_t2734286650::get_offset_of_mtl_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (ObjExporterScript_t792682685), -1, sizeof(ObjExporterScript_t792682685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[4] = 
{
	ObjExporterScript_t792682685_StaticFields::get_offset_of_StartIndex_0(),
	ObjExporterScript_t792682685_StaticFields::get_offset_of_materialsname_1(),
	ObjExporterScript_t792682685_StaticFields::get_offset_of_materialsncolor_2(),
	ObjExporterScript_t792682685_StaticFields::get_offset_of_totalVertex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ObjExporter_t2277678484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (PinchZoom_t2955597597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	PinchZoom_t2955597597::get_offset_of_perspectiveZoomSpeed_2(),
	PinchZoom_t2955597597::get_offset_of_orthoZoomSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (SliceMesh_t608368485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[3] = 
{
	SliceMesh_t608368485::get_offset_of_spline_2(),
	SliceMesh_t608368485::get_offset_of_t0_3(),
	SliceMesh_t608368485::get_offset_of_t1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (StaticVariables_t4099069371), -1, sizeof(StaticVariables_t4099069371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[6] = 
{
	StaticVariables_t4099069371_StaticFields::get_offset_of_BASE_URL_0(),
	StaticVariables_t4099069371_StaticFields::get_offset_of_source_1(),
	StaticVariables_t4099069371_StaticFields::get_offset_of_username_2(),
	StaticVariables_t4099069371_StaticFields::get_offset_of_password_3(),
	StaticVariables_t4099069371_StaticFields::get_offset_of_id_user_4(),
	StaticVariables_t4099069371_StaticFields::get_offset_of_gate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (Utility_t109704896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (webservice_t1109616683), -1, sizeof(webservice_t1109616683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1857[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	webservice_t1109616683_StaticFields::get_offset_of_isDownloaded_13(),
	webservice_t1109616683::get_offset_of_colorDownloaded_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[2] = 
{
	U3CInitInterfaceU3Ec__AnonStorey8_t3794987368::get_offset_of_finish_0(),
	U3CInitInterfaceU3Ec__AnonStorey8_t3794987368::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (U3CDownloadStyleListU3Ec__Iterator0_t1849560148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[7] = 
{
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U3CstylesU3E__1_1(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_finish_2(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U24this_3(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U24current_4(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U24disposing_5(),
	U3CDownloadStyleListU3Ec__Iterator0_t1849560148::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (U3CDownloadTypeListU3Ec__Iterator1_t1032010496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[7] = 
{
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U3CtypesU3E__1_1(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_finish_2(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U24this_3(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U24current_4(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U24disposing_5(),
	U3CDownloadTypeListU3Ec__Iterator1_t1032010496::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[7] = 
{
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U3CgeometriesU3E__1_1(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_finish_2(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U24this_3(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U24current_4(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U24disposing_5(),
	U3CDownloadGeometriesListU3Ec__Iterator2_t809774299::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[2] = 
{
	U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197::get_offset_of_finish_0(),
	U3CPoolDownloadColorU3Ec__AnonStorey9_t1682189197::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (U3CDownloadColorListU3Ec__Iterator3_t4106110459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[7] = 
{
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U3CcolorsU3E__1_1(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_finish_2(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U24this_3(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U24current_4(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U24disposing_5(),
	U3CDownloadColorListU3Ec__Iterator3_t4106110459::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (U3CdownloadElementU3Ec__AnonStoreyB_t3800062637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[1] = 
{
	U3CdownloadElementU3Ec__AnonStoreyB_t3800062637::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (U3CdownloadElementU3Ec__AnonStoreyA_t4203347164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[2] = 
{
	U3CdownloadElementU3Ec__AnonStoreyA_t4203347164::get_offset_of_g_0(),
	U3CdownloadElementU3Ec__AnonStoreyA_t4203347164::get_offset_of_U3CU3Ef__refU2411_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (U3CDownloadScenesU3Ec__Iterator4_t1121704572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[7] = 
{
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U3CscenesU3E__1_1(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_finish_2(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U24this_3(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U24current_4(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U24disposing_5(),
	U3CDownloadScenesU3Ec__Iterator4_t1121704572::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (U3CloginU3Ec__Iterator5_t2972907461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[7] = 
{
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_username_0(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_password_1(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_U3CwwwU3E__0_2(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_success_3(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_U24current_4(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_U24disposing_5(),
	U3CloginU3Ec__Iterator5_t2972907461::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (U3CDownloadGatesU3Ec__Iterator6_t4257982057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[9] = 
{
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_start_0(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_limit_1(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U3CwwwU3E__0_2(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U3CgatesU3E__1_3(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_finish_4(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U24this_5(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U24current_6(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U24disposing_7(),
	U3CDownloadGatesU3Ec__Iterator6_t4257982057::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[7] = 
{
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_id_0(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_U3CwwwU3E__0_1(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_error_2(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_finish_3(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_U24current_4(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_U24disposing_5(),
	U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (Element_t2605769808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[3] = 
{
	Element_t2605769808::get_offset_of_drawCoordinate_2(),
	Element_t2605769808::get_offset_of_item_3(),
	Element_t2605769808::get_offset_of_is_specular_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (Hat2_t1573290449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[6] = 
{
	Hat2_t1573290449::get_offset_of_id_0(),
	Hat2_t1573290449::get_offset_of_relativePositions_1(),
	Hat2_t1573290449::get_offset_of_interpolationMode_2(),
	Hat2_t1573290449::get_offset_of_senzaBarra_3(),
	Hat2_t1573290449::get_offset_of_strutturaVisibile_4(),
	Hat2_t1573290449::get_offset_of_tipologia_curva_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (Item_t2440468191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[14] = 
{
	Item_t2440468191::get_offset_of_id_0(),
	Item_t2440468191::get_offset_of_type_1(),
	Item_t2440468191::get_offset_of_dimension_2(),
	Item_t2440468191::get_offset_of_price_3(),
	Item_t2440468191::get_offset_of_weight_4(),
	Item_t2440468191::get_offset_of_name_5(),
	Item_t2440468191::get_offset_of_iconPath_6(),
	Item_t2440468191::get_offset_of_numPuntiSaldatura_7(),
	Item_t2440468191::get_offset_of_style_8(),
	Item_t2440468191::get_offset_of_obj_9(),
	Item_t2440468191::get_offset_of_obj_path_10(),
	Item_t2440468191::get_offset_of_rotate_11(),
	Item_t2440468191::get_offset_of_color_12(),
	Item_t2440468191::get_offset_of_cells_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (ItemColor_t1117508970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[8] = 
{
	ItemColor_t1117508970::get_offset_of_id_2(),
	ItemColor_t1117508970::get_offset_of_name_3(),
	ItemColor_t1117508970::get_offset_of_color_4(),
	ItemColor_t1117508970::get_offset_of_texture_5(),
	ItemColor_t1117508970::get_offset_of_texture_path_6(),
	ItemColor_t1117508970::get_offset_of_image_path_7(),
	ItemColor_t1117508970::get_offset_of_selected_8(),
	ItemColor_t1117508970::get_offset_of_material_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CdownloadTextureColorU3Ec__Iterator0_t2545635092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_U3CwwwU3E__0_0(),
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_finish_1(),
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_U24this_2(),
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_U24current_3(),
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_U24disposing_4(),
	U3CdownloadTextureColorU3Ec__Iterator0_t2545635092::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (ItemGate_t2548129788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[21] = 
{
	ItemGate_t2548129788::get_offset_of_id_2(),
	ItemGate_t2548129788::get_offset_of_name_3(),
	ItemGate_t2548129788::get_offset_of_image_4(),
	ItemGate_t2548129788::get_offset_of_image_path_5(),
	ItemGate_t2548129788::get_offset_of_date_6(),
	ItemGate_t2548129788::get_offset_of_id_type_7(),
	ItemGate_t2548129788::get_offset_of_id_style_8(),
	ItemGate_t2548129788::get_offset_of_id_geometry_9(),
	ItemGate_t2548129788::get_offset_of_width_10(),
	ItemGate_t2548129788::get_offset_of_height_11(),
	ItemGate_t2548129788::get_offset_of_heightmax_12(),
	ItemGate_t2548129788::get_offset_of_opening_13(),
	ItemGate_t2548129788::get_offset_of_configuration_14(),
	ItemGate_t2548129788::get_offset_of_type_15(),
	ItemGate_t2548129788::get_offset_of_style_16(),
	ItemGate_t2548129788::get_offset_of_geometry_17(),
	ItemGate_t2548129788::get_offset_of_misure_18(),
	ItemGate_t2548129788::get_offset_of_gateDim_19(),
	ItemGate_t2548129788::get_offset_of_gateMaxDim_20(),
	ItemGate_t2548129788::get_offset_of_centralPoint_21(),
	ItemGate_t2548129788::get_offset_of_mat_frame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t1853153015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[6] = 
{
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_U3CwwwU3E__0_0(),
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_finish_1(),
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_U24this_2(),
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_U24current_3(),
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_U24disposing_4(),
	U3CdownloadImageU3Ec__Iterator0_t1853153015::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (ItemGeometry_t2838964733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[9] = 
{
	ItemGeometry_t2838964733::get_offset_of_id_2(),
	ItemGeometry_t2838964733::get_offset_of_name_3(),
	ItemGeometry_t2838964733::get_offset_of_image_4(),
	ItemGeometry_t2838964733::get_offset_of_image_path_5(),
	ItemGeometry_t2838964733::get_offset_of_relativePositions_6(),
	ItemGeometry_t2838964733::get_offset_of_interpolationMode_7(),
	ItemGeometry_t2838964733::get_offset_of_showBottomBar_8(),
	ItemGeometry_t2838964733::get_offset_of_showStructure_9(),
	ItemGeometry_t2838964733::get_offset_of_type_curve_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t3125289710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[6] = 
{
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_U3CwwwU3E__0_0(),
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_finish_1(),
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_U24this_2(),
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_U24current_3(),
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_U24disposing_4(),
	U3CdownloadImageU3Ec__Iterator0_t3125289710::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (ItemLayer_t674074160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[3] = 
{
	ItemLayer_t674074160::get_offset_of_items_2(),
	ItemLayer_t674074160::get_offset_of_cells_3(),
	ItemLayer_t674074160::get_offset_of_grid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (ItemMisure_t2657053000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[11] = 
{
	ItemMisure_t2657053000::get_offset_of_id_2(),
	ItemMisure_t2657053000::get_offset_of_min_width_3(),
	ItemMisure_t2657053000::get_offset_of_max_width_4(),
	ItemMisure_t2657053000::get_offset_of_area_lateral_5(),
	ItemMisure_t2657053000::get_offset_of_area_bottom_6(),
	ItemMisure_t2657053000::get_offset_of_area_top_7(),
	ItemMisure_t2657053000::get_offset_of_width_frame_8(),
	ItemMisure_t2657053000::get_offset_of_width_central_frame_9(),
	ItemMisure_t2657053000::get_offset_of_height_top_frame_10(),
	ItemMisure_t2657053000::get_offset_of_height_bottom_frame_11(),
	ItemMisure_t2657053000::get_offset_of_factor_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (ItemScene_t1151284345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[13] = 
{
	ItemScene_t1151284345::get_offset_of_id_2(),
	ItemScene_t1151284345::get_offset_of_name_3(),
	ItemScene_t1151284345::get_offset_of_image_bg_4(),
	ItemScene_t1151284345::get_offset_of_image_floor_5(),
	ItemScene_t1151284345::get_offset_of_texture_wall_6(),
	ItemScene_t1151284345::get_offset_of_texture_column_7(),
	ItemScene_t1151284345::get_offset_of_image_bg_path_8(),
	ItemScene_t1151284345::get_offset_of_image_floor_path_9(),
	ItemScene_t1151284345::get_offset_of_texture_wall_path_10(),
	ItemScene_t1151284345::get_offset_of_texture_column_path_11(),
	ItemScene_t1151284345::get_offset_of_camera_rotate_12(),
	ItemScene_t1151284345::get_offset_of_lamp_rotate_13(),
	ItemScene_t1151284345::get_offset_of_lamp_intensity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (ItemStyle_t3530326308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[6] = 
{
	ItemStyle_t3530326308::get_offset_of_id_2(),
	ItemStyle_t3530326308::get_offset_of_name_3(),
	ItemStyle_t3530326308::get_offset_of_image_4(),
	ItemStyle_t3530326308::get_offset_of_image_path_5(),
	ItemStyle_t3530326308::get_offset_of_generator_6(),
	ItemStyle_t3530326308::get_offset_of_elements_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t141980167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[6] = 
{
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_U3CwwwU3E__0_0(),
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_finish_1(),
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_U24this_2(),
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_U24current_3(),
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_U24disposing_4(),
	U3CdownloadImageU3Ec__Iterator0_t141980167::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ItemType_t3455197591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[7] = 
{
	ItemType_t3455197591::get_offset_of_id_2(),
	ItemType_t3455197591::get_offset_of_name_3(),
	ItemType_t3455197591::get_offset_of_image_4(),
	ItemType_t3455197591::get_offset_of_image_path_5(),
	ItemType_t3455197591::get_offset_of_opening_6(),
	ItemType_t3455197591::get_offset_of_description_7(),
	ItemType_t3455197591::get_offset_of_misure_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (U3CdownloadImageU3Ec__Iterator0_t1504783650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[6] = 
{
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_U3CwwwU3E__0_0(),
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_finish_1(),
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_U24this_2(),
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_U24current_3(),
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_U24disposing_4(),
	U3CdownloadImageU3Ec__Iterator0_t1504783650::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (JSONBinaryTag_t3856753551)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[8] = 
{
	JSONBinaryTag_t3856753551::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (JSONNode_t1250409636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (U3CU3Ec__Iterator0_t803137928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[3] = 
{
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (U3CU3Ec__Iterator1_t3532021283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[8] = 
{
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U3CCU3E__0_1(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U3CDU3E__1_3(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (JSONArray_t3986483147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	JSONArray_t3986483147::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (U3CU3Ec__Iterator0_t4017688111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[6] = 
{
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U3CNU3E__0_1(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t1167240694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U3CNU3E__0_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (JSONClass_t1609506608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[1] = 
{
	JSONClass_t1609506608::get_offset_of_m_Dict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3CRemoveU3Ec__AnonStorey2_t2562212806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	U3CRemoveU3Ec__AnonStorey2_t2562212806::get_offset_of_aNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (U3CU3Ec__Iterator0_t466495836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[6] = 
{
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U3CNU3E__0_1(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t201440115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U3CNU3E__0_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (JSONData_t531041784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	JSONData_t531041784::get_offset_of_m_Data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (JSONLazyCreator_t3575446586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[2] = 
{
	JSONLazyCreator_t3575446586::get_offset_of_m_Node_0(),
	JSONLazyCreator_t3575446586::get_offset_of_m_Key_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (JSON_t3971185768), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
