﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2938065979.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2673668873_gshared (Enumerator_t2938065979 * __this, Dictionary_2_t1251533215 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2673668873(__this, ___host0, method) ((  void (*) (Enumerator_t2938065979 *, Dictionary_2_t1251533215 *, const MethodInfo*))Enumerator__ctor_m2673668873_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2076376076_gshared (Enumerator_t2938065979 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2076376076(__this, method) ((  Il2CppObject * (*) (Enumerator_t2938065979 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2076376076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3532897834_gshared (Enumerator_t2938065979 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3532897834(__this, method) ((  void (*) (Enumerator_t2938065979 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3532897834_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m183454441_gshared (Enumerator_t2938065979 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m183454441(__this, method) ((  void (*) (Enumerator_t2938065979 *, const MethodInfo*))Enumerator_Dispose_m183454441_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2341706654_gshared (Enumerator_t2938065979 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2341706654(__this, method) ((  bool (*) (Enumerator_t2938065979 *, const MethodInfo*))Enumerator_MoveNext_m2341706654_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m2684114648_gshared (Enumerator_t2938065979 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2684114648(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t2938065979 *, const MethodInfo*))Enumerator_get_Current_m2684114648_gshared)(__this, method)
