﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GeometryBuffer/GroupData
struct GroupData_t2204692426;

#include "codegen/il2cpp-codegen.h"

// System.Void GeometryBuffer/GroupData::.ctor()
extern "C"  void GroupData__ctor_m2539669057 (GroupData_t2204692426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer/GroupData::get_isEmpty()
extern "C"  bool GroupData_get_isEmpty_m3643082963 (GroupData_t2204692426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
