﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_start
struct bt_start_t1297110715;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_start::.ctor()
extern "C"  void bt_start__ctor_m285536332 (bt_start_t1297110715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_start::goToMain()
extern "C"  void bt_start_goToMain_m671558272 (bt_start_t1297110715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
