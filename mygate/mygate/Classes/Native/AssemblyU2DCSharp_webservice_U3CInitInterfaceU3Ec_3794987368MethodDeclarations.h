﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<InitInterface>c__AnonStorey8
struct U3CInitInterfaceU3Ec__AnonStorey8_t3794987368;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<InitInterface>c__AnonStorey8::.ctor()
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8__ctor_m4032579287 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__0(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__0_m458405719 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___FinishStyleList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__1(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__1_m2352772860 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___FinishTypeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__2(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__2_m885698701 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___FinishGeometriesList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__3(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__3_m2780065842 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___FinishColorList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__4(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__4_m2664480707 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___finishAllDownloadImages0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<InitInterface>c__AnonStorey8::<>m__5(System.Boolean)
extern "C"  void U3CInitInterfaceU3Ec__AnonStorey8_U3CU3Em__5_m263880552 (U3CInitInterfaceU3Ec__AnonStorey8_t3794987368 * __this, bool ___finishScene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
