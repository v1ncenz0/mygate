﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<ThreadSaveGate>c__AnonStorey6
struct  U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232  : public Il2CppObject
{
public:
	// System.Action`1<System.Int32> ControlsDesign/<ThreadSaveGate>c__AnonStorey6::saved
	Action_1_t1873676830 * ___saved_0;
	// System.Action`1<System.Boolean> ControlsDesign/<ThreadSaveGate>c__AnonStorey6::error
	Action_1_t3627374100 * ___error_1;

public:
	inline static int32_t get_offset_of_saved_0() { return static_cast<int32_t>(offsetof(U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232, ___saved_0)); }
	inline Action_1_t1873676830 * get_saved_0() const { return ___saved_0; }
	inline Action_1_t1873676830 ** get_address_of_saved_0() { return &___saved_0; }
	inline void set_saved_0(Action_1_t1873676830 * value)
	{
		___saved_0 = value;
		Il2CppCodeGenWriteBarrier(&___saved_0, value);
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232, ___error_1)); }
	inline Action_1_t3627374100 * get_error_1() const { return ___error_1; }
	inline Action_1_t3627374100 ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Action_1_t3627374100 * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier(&___error_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
