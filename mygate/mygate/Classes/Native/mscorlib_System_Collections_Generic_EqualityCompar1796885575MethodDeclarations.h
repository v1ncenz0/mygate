﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FaceIndices>
struct DefaultComparer_t1796885575;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FaceIndices>::.ctor()
extern "C"  void DefaultComparer__ctor_m1445819092_gshared (DefaultComparer_t1796885575 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1445819092(__this, method) ((  void (*) (DefaultComparer_t1796885575 *, const MethodInfo*))DefaultComparer__ctor_m1445819092_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FaceIndices>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m34403509_gshared (DefaultComparer_t1796885575 * __this, FaceIndices_t174955414  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m34403509(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1796885575 *, FaceIndices_t174955414 , const MethodInfo*))DefaultComparer_GetHashCode_m34403509_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FaceIndices>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3488816421_gshared (DefaultComparer_t1796885575 * __this, FaceIndices_t174955414  ___x0, FaceIndices_t174955414  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3488816421(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1796885575 *, FaceIndices_t174955414 , FaceIndices_t174955414 , const MethodInfo*))DefaultComparer_Equals_m3488816421_gshared)(__this, ___x0, ___y1, method)
