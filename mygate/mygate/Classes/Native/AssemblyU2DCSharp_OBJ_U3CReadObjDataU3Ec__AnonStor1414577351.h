﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/<ReadObjData>c__AnonStorey5
struct  U3CReadObjDataU3Ec__AnonStorey5_t1414577351  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> OBJ/<ReadObjData>c__AnonStorey5::is_finish
	Action_1_t3627374100 * ___is_finish_0;

public:
	inline static int32_t get_offset_of_is_finish_0() { return static_cast<int32_t>(offsetof(U3CReadObjDataU3Ec__AnonStorey5_t1414577351, ___is_finish_0)); }
	inline Action_1_t3627374100 * get_is_finish_0() const { return ___is_finish_0; }
	inline Action_1_t3627374100 ** get_address_of_is_finish_0() { return &___is_finish_0; }
	inline void set_is_finish_0(Action_1_t3627374100 * value)
	{
		___is_finish_0 = value;
		Il2CppCodeGenWriteBarrier(&___is_finish_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
