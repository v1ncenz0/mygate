﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemMisure
struct ItemMisure_t2657053000;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemMisure::.ctor()
extern "C"  void ItemMisure__ctor_m3840382475 (ItemMisure_t2657053000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
