﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign
struct ControlsDesign_t1600104368;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// Item
struct Item_t2440468191;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.String
struct String_t;
// SimpleJSON.JSONArray
struct JSONArray_t3986483147;
// ItemColor
struct ItemColor_t1117508970;
// OBJItem
struct OBJItem_t2734286650;
// System.Action`1<OBJItem>
struct Action_1_t2536086032;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray3986483147.h"
#include "AssemblyU2DCSharp_OBJItem2734286650.h"

// System.Void ControlsDesign::.ctor()
extern "C"  void ControlsDesign__ctor_m1007887185 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::Start()
extern "C"  void ControlsDesign_Start_m3239624785 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::Init()
extern "C"  void ControlsDesign_Init_m946222235 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setInitialCamera(UnityEngine.Vector3)
extern "C"  void ControlsDesign_setInitialCamera_m2553095157 (ControlsDesign_t1600104368 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setVisibilityLayer(System.Collections.ArrayList)
extern "C"  void ControlsDesign_setVisibilityLayer_m1691979837 (ControlsDesign_t1600104368 * __this, ArrayList_t4252133567 * ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setVisibilityLayer(System.Int32,System.Boolean)
extern "C"  void ControlsDesign_setVisibilityLayer_m2572899862 (ControlsDesign_t1600104368 * __this, int32_t ___layer_id0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setInvertVisibilityLayer(System.Int32)
extern "C"  void ControlsDesign_setInvertVisibilityLayer_m3455857049 (ControlsDesign_t1600104368 * __this, int32_t ___layer_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::activateLayer(System.Int32)
extern "C"  void ControlsDesign_activateLayer_m635649420 (ControlsDesign_t1600104368 * __this, int32_t ___layerToActivate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign::isMiddleLayerNotEmpty()
extern "C"  bool ControlsDesign_isMiddleLayerNotEmpty_m2049357965 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign::isMiddleLayerFill()
extern "C"  bool ControlsDesign_isMiddleLayerFill_m1297563134 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::cameraMove(System.Int32)
extern "C"  void ControlsDesign_cameraMove_m2618160916 (ControlsDesign_t1600104368 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setItemSelected(Item)
extern "C"  void ControlsDesign_setItemSelected_m4048310084 (ControlsDesign_t1600104368 * __this, Item_t2440468191 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::itemChanged(Item)
extern "C"  void ControlsDesign_itemChanged_m3568007299 (ControlsDesign_t1600104368 * __this, Item_t2440468191 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::fillGrid()
extern "C"  void ControlsDesign_fillGrid_m4284212242 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setEditMode()
extern "C"  void ControlsDesign_setEditMode_m4040372048 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setDeleteMode()
extern "C"  void ControlsDesign_setDeleteMode_m1990844589 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::showGrid(System.Boolean)
extern "C"  void ControlsDesign_showGrid_m118924973 (ControlsDesign_t1600104368 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::showCellGrid(System.Boolean)
extern "C"  void ControlsDesign_showCellGrid_m1571019799 (ControlsDesign_t1600104368 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::invertGridShow()
extern "C"  void ControlsDesign_invertGridShow_m4140784766 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setSpecularMode(System.Boolean)
extern "C"  void ControlsDesign_setSpecularMode_m3111075588 (ControlsDesign_t1600104368 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::invertSpecularMode()
extern "C"  void ControlsDesign_invertSpecularMode_m3667743799 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::eraseGrid()
extern "C"  void ControlsDesign_eraseGrid_m2176074381 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::deleteGrid()
extern "C"  void ControlsDesign_deleteGrid_m3103376160 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::addCell(System.Action`1<System.Boolean>,Item,System.Boolean)
extern "C"  void ControlsDesign_addCell_m3245497709 (ControlsDesign_t1600104368 * __this, Action_1_t3627374100 * ___added0, Item_t2440468191 * ___item1, bool ___showWaiting2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ControlsDesign::ThreadaddCell(System.Action`1<System.Boolean>,Item)
extern "C"  Il2CppObject * ControlsDesign_ThreadaddCell_m4200333358 (ControlsDesign_t1600104368 * __this, Action_1_t3627374100 * ___added0, Item_t2440468191 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ControlsDesign::calcolateSpecularPosition(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  ControlsDesign_calcolateSpecularPosition_m2424140282 (ControlsDesign_t1600104368 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ControlsDesign::searchElement(UnityEngine.Vector3,System.Boolean)
extern "C"  GameObject_t1756533147 * ControlsDesign_searchElement_m2028733776 (ControlsDesign_t1600104368 * __this, Vector3_t2243707580  ___pos0, bool ___isSpecular1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ControlsDesign::isValidGate()
extern "C"  bool ControlsDesign_isValidGate_m2999041940 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::cancelGate()
extern "C"  void ControlsDesign_cancelGate_m562820422 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::closeMyGate()
extern "C"  void ControlsDesign_closeMyGate_m161139726 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::saveGate()
extern "C"  void ControlsDesign_saveGate_m3301007695 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::ThreadSaveGate(System.Action`1<System.Int32>,System.Action`1<System.Boolean>)
extern "C"  void ControlsDesign_ThreadSaveGate_m3282362943 (ControlsDesign_t1600104368 * __this, Action_1_t1873676830 * ___saved0, Action_1_t3627374100 * ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ControlsDesign::sendValueToDB(System.String,System.Boolean,System.Action`1<System.Int32>,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ControlsDesign_sendValueToDB_m256654198 (ControlsDesign_t1600104368 * __this, String_t* ___json0, bool ___withoutScreen1, Action_1_t1873676830 * ___saved2, Action_1_t3627374100 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::confirmOrder()
extern "C"  void ControlsDesign_confirmOrder_m2085207233 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::ThreadConfirmOrder()
extern "C"  void ControlsDesign_ThreadConfirmOrder_m169787487 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::getGatefromJSON(SimpleJSON.JSONArray,System.Action`1<System.Boolean>)
extern "C"  void ControlsDesign_getGatefromJSON_m486559692 (ControlsDesign_t1600104368 * __this, JSONArray_t3986483147 * ___configuration0, Action_1_t3627374100 * ___success1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::downloadElements(System.Collections.ArrayList,System.Int32,System.Action`1<System.Boolean>)
extern "C"  void ControlsDesign_downloadElements_m1659884447 (ControlsDesign_t1600104368 * __this, ArrayList_t4252133567 * ___items0, int32_t ___index1, Action_1_t3627374100 * ___finish2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setElement(System.Int32)
extern "C"  void ControlsDesign_setElement_m756224042 (ControlsDesign_t1600104368 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item ControlsDesign::getElementByAvaibles(System.Int32)
extern "C"  Item_t2440468191 * ControlsDesign_getElementByAvaibles_m2267237610 (ControlsDesign_t1600104368 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setElementToAvaible(Item)
extern "C"  void ControlsDesign_setElementToAvaible_m3899228895 (ControlsDesign_t1600104368 * __this, Item_t2440468191 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setElementByAvaibles(System.Collections.ArrayList,Item)
extern "C"  void ControlsDesign_setElementByAvaibles_m1236695999 (ControlsDesign_t1600104368 * __this, ArrayList_t4252133567 * ___items0, Item_t2440468191 * ___item_origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setColorLayer(System.Collections.ArrayList)
extern "C"  void ControlsDesign_setColorLayer_m1770413744 (ControlsDesign_t1600104368 * __this, ArrayList_t4252133567 * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setColorLayer(System.Int32,System.Int32)
extern "C"  void ControlsDesign_setColorLayer_m2177753157 (ControlsDesign_t1600104368 * __this, int32_t ___id0, int32_t ___id_layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemColor ControlsDesign::getColorLayer(System.Int32)
extern "C"  ItemColor_t1117508970 * ControlsDesign_getColorLayer_m1022060743 (ControlsDesign_t1600104368 * __this, int32_t ___id_layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemColor ControlsDesign::getColorFrame()
extern "C"  ItemColor_t1117508970 * ControlsDesign_getColorFrame_m432155866 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::setColorFrame(System.Int32)
extern "C"  void ControlsDesign_setColorFrame_m2871938286 (ControlsDesign_t1600104368 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::resetColorLayer()
extern "C"  void ControlsDesign_resetColorLayer_m2823512804 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemColor ControlsDesign::searchColorFromLibrary(System.Int32)
extern "C"  ItemColor_t1117508970 * ControlsDesign_searchColorFromLibrary_m2631474029 (ControlsDesign_t1600104368 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemColor ControlsDesign::searchColorFromLibrary(System.String)
extern "C"  ItemColor_t1117508970 * ControlsDesign_searchColorFromLibrary_m698875444 (ControlsDesign_t1600104368 * __this, String_t* ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ControlsDesign::ScreenShootToPNG()
extern "C"  String_t* ControlsDesign_ScreenShootToPNG_m2357710681 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::CaptureScreen()
extern "C"  void ControlsDesign_CaptureScreen_m2705239789 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::exportToOBJ()
extern "C"  void ControlsDesign_exportToOBJ_m4288863689 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::exportOBJToFile()
extern "C"  void ControlsDesign_exportOBJToFile_m1290058983 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ControlsDesign::sendToExport(OBJItem)
extern "C"  Il2CppObject * ControlsDesign_sendToExport_m402447214 (ControlsDesign_t1600104368 * __this, OBJItem_t2734286650 * ___objitem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ControlsDesign::createOBJ(System.Action`1<OBJItem>)
extern "C"  Il2CppObject * ControlsDesign_createOBJ_m1082920934 (ControlsDesign_t1600104368 * __this, Action_1_t2536086032 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::resetCamera()
extern "C"  void ControlsDesign_resetCamera_m1552374793 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::showSceneForRender()
extern "C"  void ControlsDesign_showSceneForRender_m2957815529 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::showCAD()
extern "C"  void ControlsDesign_showCAD_m2077577340 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::invertSceneForRender()
extern "C"  void ControlsDesign_invertSceneForRender_m919667608 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::goToSetting()
extern "C"  void ControlsDesign_goToSetting_m3218160994 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::showPrice()
extern "C"  void ControlsDesign_showPrice_m1520849811 (ControlsDesign_t1600104368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<Init>m__0(System.Boolean)
extern "C"  void ControlsDesign_U3CInitU3Em__0_m818335747 (ControlsDesign_t1600104368 * __this, bool ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<fillGrid>m__1(System.Boolean)
extern "C"  void ControlsDesign_U3CfillGridU3Em__1_m4231047283 (Il2CppObject * __this /* static, unused */, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<saveGate>m__2(System.Int32)
extern "C"  void ControlsDesign_U3CsaveGateU3Em__2_m262954895 (ControlsDesign_t1600104368 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<saveGate>m__3(System.Boolean)
extern "C"  void ControlsDesign_U3CsaveGateU3Em__3_m328263184 (Il2CppObject * __this /* static, unused */, bool ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<exportToOBJ>m__4(OBJItem)
extern "C"  void ControlsDesign_U3CexportToOBJU3Em__4_m4246151606 (ControlsDesign_t1600104368 * __this, OBJItem_t2734286650 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign::<exportOBJToFile>m__5(OBJItem)
extern "C"  void ControlsDesign_U3CexportOBJToFileU3Em__5_m4205024883 (ControlsDesign_t1600104368 * __this, OBJItem_t2734286650 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
