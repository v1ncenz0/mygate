﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// OBJ/<Load>c__Iterator0
struct U3CLoadU3Ec__Iterator0_t4200827971;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3
struct  U3CLoadU3Ec__AnonStorey3_t2764713728  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3::is_finish
	Action_1_t3627374100 * ___is_finish_0;
	// OBJ/<Load>c__Iterator0 OBJ/<Load>c__Iterator0/<Load>c__AnonStorey3::<>f__ref$0
	U3CLoadU3Ec__Iterator0_t4200827971 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_is_finish_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__AnonStorey3_t2764713728, ___is_finish_0)); }
	inline Action_1_t3627374100 * get_is_finish_0() const { return ___is_finish_0; }
	inline Action_1_t3627374100 ** get_address_of_is_finish_0() { return &___is_finish_0; }
	inline void set_is_finish_0(Action_1_t3627374100 * value)
	{
		___is_finish_0 = value;
		Il2CppCodeGenWriteBarrier(&___is_finish_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__AnonStorey3_t2764713728, ___U3CU3Ef__refU240_1)); }
	inline U3CLoadU3Ec__Iterator0_t4200827971 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CLoadU3Ec__Iterator0_t4200827971 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CLoadU3Ec__Iterator0_t4200827971 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
