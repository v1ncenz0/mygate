﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManageCoroutines
struct ManageCoroutines_t3089936288;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ManageCoroutines::.ctor()
extern "C"  void ManageCoroutines__ctor_m2566500875 (ManageCoroutines_t3089936288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageCoroutines::lauchCoroutines(System.Collections.IEnumerator)
extern "C"  void ManageCoroutines_lauchCoroutines_m2559485174 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___coroutine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
