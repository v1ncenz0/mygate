﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<DownloadGeometriesList>c__Iterator2
struct U3CDownloadGeometriesListU3Ec__Iterator2_t809774299;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<DownloadGeometriesList>c__Iterator2::.ctor()
extern "C"  void U3CDownloadGeometriesListU3Ec__Iterator2__ctor_m4273873092 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<DownloadGeometriesList>c__Iterator2::MoveNext()
extern "C"  bool U3CDownloadGeometriesListU3Ec__Iterator2_MoveNext_m1524865704 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadGeometriesList>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadGeometriesListU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3276703414 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadGeometriesList>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadGeometriesListU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m518292814 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadGeometriesList>c__Iterator2::Dispose()
extern "C"  void U3CDownloadGeometriesListU3Ec__Iterator2_Dispose_m964780413 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadGeometriesList>c__Iterator2::Reset()
extern "C"  void U3CDownloadGeometriesListU3Ec__Iterator2_Reset_m702948255 (U3CDownloadGeometriesListU3Ec__Iterator2_t809774299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
