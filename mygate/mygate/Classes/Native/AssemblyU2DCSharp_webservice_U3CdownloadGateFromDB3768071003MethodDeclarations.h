﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<downloadGateFromDB>c__Iterator7
struct U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<downloadGateFromDB>c__Iterator7::.ctor()
extern "C"  void U3CdownloadGateFromDBU3Ec__Iterator7__ctor_m3209751024 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<downloadGateFromDB>c__Iterator7::MoveNext()
extern "C"  bool U3CdownloadGateFromDBU3Ec__Iterator7_MoveNext_m1542802120 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<downloadGateFromDB>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadGateFromDBU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4014918230 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<downloadGateFromDB>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadGateFromDBU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m4239244078 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<downloadGateFromDB>c__Iterator7::Dispose()
extern "C"  void U3CdownloadGateFromDBU3Ec__Iterator7_Dispose_m2775971573 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<downloadGateFromDB>c__Iterator7::Reset()
extern "C"  void U3CdownloadGateFromDBU3Ec__Iterator7_Reset_m1987844067 (U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
