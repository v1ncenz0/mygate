﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spline/<GetClosestPointParam>c__AnonStorey0
struct U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Spline/<GetClosestPointParam>c__AnonStorey0::.ctor()
extern "C"  void U3CGetClosestPointParamU3Ec__AnonStorey0__ctor_m542083542 (U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Spline/<GetClosestPointParam>c__AnonStorey0::<>m__0(UnityEngine.Vector3)
extern "C"  float U3CGetClosestPointParamU3Ec__AnonStorey0_U3CU3Em__0_m2133537820 (U3CGetClosestPointParamU3Ec__AnonStorey0_t1228364937 * __this, Vector3_t2243707580  ___splinePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
