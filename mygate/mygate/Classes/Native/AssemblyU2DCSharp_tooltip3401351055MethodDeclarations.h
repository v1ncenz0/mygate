﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// tooltip
struct tooltip_t3401351055;

#include "codegen/il2cpp-codegen.h"

// System.Void tooltip::.ctor()
extern "C"  void tooltip__ctor_m4201827002 (tooltip_t3401351055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tooltip::Start()
extern "C"  void tooltip_Start_m3194571926 (tooltip_t3401351055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tooltip::Update()
extern "C"  void tooltip_Update_m3517127503 (tooltip_t3401351055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
