﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>
struct ValueCollection_t4249560354;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4014198703;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2938065979.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m465808632_gshared (ValueCollection_t4249560354 * __this, Dictionary_2_t1251533215 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m465808632(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4249560354 *, Dictionary_2_t1251533215 *, const MethodInfo*))ValueCollection__ctor_m465808632_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1669945658_gshared (ValueCollection_t4249560354 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1669945658(__this, ___item0, method) ((  void (*) (ValueCollection_t4249560354 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1669945658_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4259986355_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4259986355(__this, method) ((  void (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4259986355_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2097493694_gshared (ValueCollection_t4249560354 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2097493694(__this, ___item0, method) ((  bool (*) (ValueCollection_t4249560354 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2097493694_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1674072923_gshared (ValueCollection_t4249560354 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1674072923(__this, ___item0, method) ((  bool (*) (ValueCollection_t4249560354 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1674072923_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m957054517_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m957054517(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m957054517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1259413909_gshared (ValueCollection_t4249560354 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1259413909(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4249560354 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1259413909_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4124274694_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4124274694(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4124274694_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2857575343_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2857575343(__this, method) ((  bool (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2857575343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1481662225_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1481662225(__this, method) ((  bool (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1481662225_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m72072369_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m72072369(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m72072369_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2830292475_gshared (ValueCollection_t4249560354 * __this, Vector3U5BU5D_t1172311765* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2830292475(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4249560354 *, Vector3U5BU5D_t1172311765*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2830292475_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t2938065979  ValueCollection_GetEnumerator_m4242309760_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4242309760(__this, method) ((  Enumerator_t2938065979  (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_GetEnumerator_m4242309760_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Vector3>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3371609621_gshared (ValueCollection_t4249560354 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3371609621(__this, method) ((  int32_t (*) (ValueCollection_t4249560354 *, const MethodInfo*))ValueCollection_get_Count_m3371609621_gshared)(__this, method)
