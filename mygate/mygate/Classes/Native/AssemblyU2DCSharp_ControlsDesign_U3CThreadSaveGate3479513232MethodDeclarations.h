﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<ThreadSaveGate>c__AnonStorey6
struct U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<ThreadSaveGate>c__AnonStorey6::.ctor()
extern "C"  void U3CThreadSaveGateU3Ec__AnonStorey6__ctor_m895791481 (U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<ThreadSaveGate>c__AnonStorey6::<>m__0(System.Int32)
extern "C"  void U3CThreadSaveGateU3Ec__AnonStorey6_U3CU3Em__0_m892764983 (U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232 * __this, int32_t ___id_gate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<ThreadSaveGate>c__AnonStorey6::<>m__1(System.Boolean)
extern "C"  void U3CThreadSaveGateU3Ec__AnonStorey6_U3CU3Em__1_m3058869352 (U3CThreadSaveGateU3Ec__AnonStorey6_t3479513232 * __this, bool ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
