﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2528690492.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3914828424_gshared (Enumerator_t2528690492 * __this, Dictionary_2_t4134154350 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3914828424(__this, ___host0, method) ((  void (*) (Enumerator_t2528690492 *, Dictionary_2_t4134154350 *, const MethodInfo*))Enumerator__ctor_m3914828424_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2994005831_gshared (Enumerator_t2528690492 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2994005831(__this, method) ((  Il2CppObject * (*) (Enumerator_t2528690492 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2994005831_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m984752899_gshared (Enumerator_t2528690492 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m984752899(__this, method) ((  void (*) (Enumerator_t2528690492 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m984752899_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3906195268_gshared (Enumerator_t2528690492 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3906195268(__this, method) ((  void (*) (Enumerator_t2528690492 *, const MethodInfo*))Enumerator_Dispose_m3906195268_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2391508615_gshared (Enumerator_t2528690492 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2391508615(__this, method) ((  bool (*) (Enumerator_t2528690492 *, const MethodInfo*))Enumerator_MoveNext_m2391508615_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Vector3,System.Object>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m2593706109_gshared (Enumerator_t2528690492 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2593706109(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t2528690492 *, const MethodInfo*))Enumerator_get_Current_m2593706109_gshared)(__this, method)
