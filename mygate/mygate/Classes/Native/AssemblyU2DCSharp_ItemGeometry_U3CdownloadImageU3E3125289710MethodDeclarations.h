﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemGeometry/<downloadImage>c__Iterator0
struct U3CdownloadImageU3Ec__Iterator0_t3125289710;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemGeometry/<downloadImage>c__Iterator0::.ctor()
extern "C"  void U3CdownloadImageU3Ec__Iterator0__ctor_m36257295 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemGeometry/<downloadImage>c__Iterator0::MoveNext()
extern "C"  bool U3CdownloadImageU3Ec__Iterator0_MoveNext_m3879553249 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemGeometry/<downloadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2647776989 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemGeometry/<downloadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m342282341 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGeometry/<downloadImage>c__Iterator0::Dispose()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Dispose_m2841044580 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGeometry/<downloadImage>c__Iterator0::Reset()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Reset_m3533942238 (U3CdownloadImageU3Ec__Iterator0_t3125289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
