﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2186542558(__this, ___l0, method) ((  void (*) (Enumerator_t2455754151 *, List_1_t2921024477 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1935631564(__this, method) ((  void (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m845906058(__this, method) ((  Il2CppObject * (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::Dispose()
#define Enumerator_Dispose_m4233526325(__this, method) ((  void (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::VerifyState()
#define Enumerator_VerifyState_m3464357872(__this, method) ((  void (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::MoveNext()
#define Enumerator_MoveNext_m2923175167(__this, method) ((  bool (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OBJ/MaterialData>::get_Current()
#define Enumerator_get_Current_m3096128707(__this, method) ((  MaterialData_t3551903345 * (*) (Enumerator_t2455754151 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
