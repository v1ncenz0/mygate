﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BranchingSplineParameter
struct BranchingSplineParameter_t1831757700;
// Spline
struct Spline_t1260612603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"

// System.Void BranchingSplineParameter::.ctor()
extern "C"  void BranchingSplineParameter__ctor_m1073277763 (BranchingSplineParameter_t1831757700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BranchingSplineParameter::.ctor(Spline,System.Single)
extern "C"  void BranchingSplineParameter__ctor_m3307922803 (BranchingSplineParameter_t1831757700 * __this, Spline_t1260612603 * ___spline0, float ___parameter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BranchingSplineParameter::.ctor(Spline,System.Single,System.Boolean)
extern "C"  void BranchingSplineParameter__ctor_m2181141206 (BranchingSplineParameter_t1831757700 * __this, Spline_t1260612603 * ___spline0, float ___parameter1, bool ___forward2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BranchingSplineParameter::get_Forward()
extern "C"  bool BranchingSplineParameter_get_Forward_m2469365797 (BranchingSplineParameter_t1831757700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
