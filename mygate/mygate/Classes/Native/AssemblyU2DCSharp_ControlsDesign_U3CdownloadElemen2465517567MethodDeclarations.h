﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<downloadElements>c__AnonStorey8
struct U3CdownloadElementsU3Ec__AnonStorey8_t2465517567;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ControlsDesign/<downloadElements>c__AnonStorey8::.ctor()
extern "C"  void U3CdownloadElementsU3Ec__AnonStorey8__ctor_m415134998 (U3CdownloadElementsU3Ec__AnonStorey8_t2465517567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<downloadElements>c__AnonStorey8::<>m__0(UnityEngine.GameObject)
extern "C"  void U3CdownloadElementsU3Ec__AnonStorey8_U3CU3Em__0_m89138833 (U3CdownloadElementsU3Ec__AnonStorey8_t2465517567 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<downloadElements>c__AnonStorey8::<>m__1(System.Boolean)
extern "C"  void U3CdownloadElementsU3Ec__AnonStorey8_U3CU3Em__1_m1067632079 (U3CdownloadElementsU3Ec__AnonStorey8_t2465517567 * __this, bool ___finish20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
