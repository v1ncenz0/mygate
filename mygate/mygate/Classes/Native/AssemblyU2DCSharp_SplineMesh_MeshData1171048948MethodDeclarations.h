﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineMesh/MeshData
struct MeshData_t1171048948;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// SplineMesh/MeshData[]
struct MeshDataU5BU5D_t910502845;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "AssemblyU2DCSharp_SplineMesh_MeshData1171048948.h"

// System.Void SplineMesh/MeshData::.ctor(UnityEngine.Mesh)
extern "C"  void MeshData__ctor_m3790623037 (MeshData_t1171048948 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh/MeshData::.ctor(SplineMesh/MeshData,System.Int32,SplineMesh/MeshData[])
extern "C"  void MeshData__ctor_m2344056098 (MeshData_t1171048948 * __this, MeshData_t1171048948 * ___mData0, int32_t ___segmentCount1, MeshDataU5BU5D_t910502845* ___additionalMeshes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SplineMesh/MeshData::get_VertexCount()
extern "C"  int32_t MeshData_get_VertexCount_m3924184865 (MeshData_t1171048948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SplineMesh/MeshData::get_TriangleCount()
extern "C"  int32_t MeshData_get_TriangleCount_m4012592819 (MeshData_t1171048948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SplineMesh/MeshData::Suits(SplineMesh/MeshData,System.Int32,SplineMesh/MeshData[])
extern "C"  bool MeshData_Suits_m73688262 (MeshData_t1171048948 * __this, MeshData_t1171048948 * ___mData0, int32_t ___segmentCount1, MeshDataU5BU5D_t910502845* ___additionalMeshes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SplineMesh/MeshData::ReferencesMesh(UnityEngine.Mesh)
extern "C"  bool MeshData_ReferencesMesh_m2241446076 (MeshData_t1171048948 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh/MeshData::Reset()
extern "C"  void MeshData_Reset_m3222860864 (MeshData_t1171048948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineMesh/MeshData::AssignToMesh(UnityEngine.Mesh)
extern "C"  void MeshData_AssignToMesh_m1099556324 (MeshData_t1171048948 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
