﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline/SegmentParameter
struct  SegmentParameter_t3125294186 
{
public:
	// System.Double Spline/SegmentParameter::normalizedParam
	double ___normalizedParam_0;
	// System.Int32 Spline/SegmentParameter::normalizedIndex
	int32_t ___normalizedIndex_1;

public:
	inline static int32_t get_offset_of_normalizedParam_0() { return static_cast<int32_t>(offsetof(SegmentParameter_t3125294186, ___normalizedParam_0)); }
	inline double get_normalizedParam_0() const { return ___normalizedParam_0; }
	inline double* get_address_of_normalizedParam_0() { return &___normalizedParam_0; }
	inline void set_normalizedParam_0(double value)
	{
		___normalizedParam_0 = value;
	}

	inline static int32_t get_offset_of_normalizedIndex_1() { return static_cast<int32_t>(offsetof(SegmentParameter_t3125294186, ___normalizedIndex_1)); }
	inline int32_t get_normalizedIndex_1() const { return ___normalizedIndex_1; }
	inline int32_t* get_address_of_normalizedIndex_1() { return &___normalizedIndex_1; }
	inline void set_normalizedIndex_1(int32_t value)
	{
		___normalizedIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
