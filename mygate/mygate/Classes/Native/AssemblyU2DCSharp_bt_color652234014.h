﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemColor
struct ItemColor_t1117508970;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_color
struct  bt_color_t652234014  : public MonoBehaviour_t1158329972
{
public:
	// ItemColor bt_color::colorItem
	ItemColor_t1117508970 * ___colorItem_2;
	// UnityEngine.Color bt_color::bg_color
	Color_t2020392075  ___bg_color_3;
	// UnityEngine.Texture bt_color::bg_image
	Texture_t2243626319 * ___bg_image_4;

public:
	inline static int32_t get_offset_of_colorItem_2() { return static_cast<int32_t>(offsetof(bt_color_t652234014, ___colorItem_2)); }
	inline ItemColor_t1117508970 * get_colorItem_2() const { return ___colorItem_2; }
	inline ItemColor_t1117508970 ** get_address_of_colorItem_2() { return &___colorItem_2; }
	inline void set_colorItem_2(ItemColor_t1117508970 * value)
	{
		___colorItem_2 = value;
		Il2CppCodeGenWriteBarrier(&___colorItem_2, value);
	}

	inline static int32_t get_offset_of_bg_color_3() { return static_cast<int32_t>(offsetof(bt_color_t652234014, ___bg_color_3)); }
	inline Color_t2020392075  get_bg_color_3() const { return ___bg_color_3; }
	inline Color_t2020392075 * get_address_of_bg_color_3() { return &___bg_color_3; }
	inline void set_bg_color_3(Color_t2020392075  value)
	{
		___bg_color_3 = value;
	}

	inline static int32_t get_offset_of_bg_image_4() { return static_cast<int32_t>(offsetof(bt_color_t652234014, ___bg_image_4)); }
	inline Texture_t2243626319 * get_bg_image_4() const { return ___bg_image_4; }
	inline Texture_t2243626319 ** get_address_of_bg_image_4() { return &___bg_image_4; }
	inline void set_bg_image_4(Texture_t2243626319 * value)
	{
		___bg_image_4 = value;
		Il2CppCodeGenWriteBarrier(&___bg_image_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
