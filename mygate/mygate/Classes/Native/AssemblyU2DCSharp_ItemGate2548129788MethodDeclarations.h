﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemGate
struct ItemGate_t2548129788;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void ItemGate::.ctor()
extern "C"  void ItemGate__ctor_m21906373 (ItemGate_t2548129788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemGate::downloadImage(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemGate_downloadImage_m3223701209 (ItemGate_t2548129788 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGate::loadFromLibrary()
extern "C"  void ItemGate_loadFromLibrary_m2651880020 (ItemGate_t2548129788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGate::updateOtherParams()
extern "C"  void ItemGate_updateOtherParams_m3079665516 (ItemGate_t2548129788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 ItemGate::calcolateDimensionGross()
extern "C"  Vector2_t2243707579  ItemGate_calcolateDimensionGross_m2347644748 (ItemGate_t2548129788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemGate::isGeometry()
extern "C"  bool ItemGate_isGeometry_m2255642389 (ItemGate_t2548129788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
