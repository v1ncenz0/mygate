﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_wizard
struct  bt_wizard_t652226372  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 bt_wizard::id
	int32_t ___id_2;
	// UnityEngine.GameObject bt_wizard::textureReplica
	GameObject_t1756533147 * ___textureReplica_3;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(bt_wizard_t652226372, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_textureReplica_3() { return static_cast<int32_t>(offsetof(bt_wizard_t652226372, ___textureReplica_3)); }
	inline GameObject_t1756533147 * get_textureReplica_3() const { return ___textureReplica_3; }
	inline GameObject_t1756533147 ** get_address_of_textureReplica_3() { return &___textureReplica_3; }
	inline void set_textureReplica_3(GameObject_t1756533147 * value)
	{
		___textureReplica_3 = value;
		Il2CppCodeGenWriteBarrier(&___textureReplica_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
