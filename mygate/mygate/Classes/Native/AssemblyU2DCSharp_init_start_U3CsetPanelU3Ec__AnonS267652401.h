﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// init_start/<setPanel>c__AnonStorey1`1<System.Object>
struct  U3CsetPanelU3Ec__AnonStorey1_1_t267652401  : public Il2CppObject
{
public:
	// UnityEngine.GameObject init_start/<setPanel>c__AnonStorey1`1::bt
	GameObject_t1756533147 * ___bt_0;
	// T init_start/<setPanel>c__AnonStorey1`1::c
	Il2CppObject * ___c_1;

public:
	inline static int32_t get_offset_of_bt_0() { return static_cast<int32_t>(offsetof(U3CsetPanelU3Ec__AnonStorey1_1_t267652401, ___bt_0)); }
	inline GameObject_t1756533147 * get_bt_0() const { return ___bt_0; }
	inline GameObject_t1756533147 ** get_address_of_bt_0() { return &___bt_0; }
	inline void set_bt_0(GameObject_t1756533147 * value)
	{
		___bt_0 = value;
		Il2CppCodeGenWriteBarrier(&___bt_0, value);
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(U3CsetPanelU3Ec__AnonStorey1_1_t267652401, ___c_1)); }
	inline Il2CppObject * get_c_1() const { return ___c_1; }
	inline Il2CppObject ** get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(Il2CppObject * value)
	{
		___c_1 = value;
		Il2CppCodeGenWriteBarrier(&___c_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
