﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// webservice
struct webservice_t1109616683;
// init_main
struct init_main_t2270456518;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// init_main/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t3487247048  : public Il2CppObject
{
public:
	// webservice init_main/<Start>c__AnonStorey0::wb
	webservice_t1109616683 * ___wb_0;
	// init_main init_main/<Start>c__AnonStorey0::$this
	init_main_t2270456518 * ___U24this_1;

public:
	inline static int32_t get_offset_of_wb_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3487247048, ___wb_0)); }
	inline webservice_t1109616683 * get_wb_0() const { return ___wb_0; }
	inline webservice_t1109616683 ** get_address_of_wb_0() { return &___wb_0; }
	inline void set_wb_0(webservice_t1109616683 * value)
	{
		___wb_0 = value;
		Il2CppCodeGenWriteBarrier(&___wb_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3487247048, ___U24this_1)); }
	inline init_main_t2270456518 * get_U24this_1() const { return ___U24this_1; }
	inline init_main_t2270456518 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(init_main_t2270456518 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

struct U3CStartU3Ec__AnonStorey0_t3487247048_StaticFields
{
public:
	// System.Action`1<System.String> init_main/<Start>c__AnonStorey0::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3487247048_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
