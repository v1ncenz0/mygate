﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineMeshModifierExample
struct SplineMeshModifierExample_t1909698667;
// SplineMesh
struct SplineMesh_t1719246168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_SplineMesh1719246168.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void SplineMeshModifierExample::.ctor()
extern "C"  void SplineMeshModifierExample__ctor_m843313548 (SplineMeshModifierExample_t1909698667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineMeshModifierExample::ModifyVertex(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineMeshModifierExample_ModifyVertex_m1096696656 (SplineMeshModifierExample_t1909698667 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___vertex1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineMeshModifierExample::ModifyNormal(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineMeshModifierExample_ModifyNormal_m4028814301 (SplineMeshModifierExample_t1909698667 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___normal1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 SplineMeshModifierExample::ModifyTangent(SplineMesh,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  SplineMeshModifierExample_ModifyTangent_m491221601 (SplineMeshModifierExample_t1909698667 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector4_t2243707581  ___tangent1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SplineMeshModifierExample::ModifyUV(SplineMesh,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  SplineMeshModifierExample_ModifyUV_m2860563545 (SplineMeshModifierExample_t1909698667 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector2_t2243707579  ___uvCoord1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
