﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GeometryBuffer
struct GeometryBuffer_t1734374890;
// System.String
struct String_t;
// GeometryBuffer/FaceIndicesRow
struct FaceIndicesRow_t3099920559;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t2108486189;
// GeometryBuffer/ObjectData
struct ObjectData_t4054693732;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>
struct Dictionary_2_t2885703635;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>
struct Dictionary_2_t1251533215;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_GeometryBuffer_FaceIndicesRow3099920559.h"
#include "AssemblyU2DCSharp_GeometryBuffer_ObjectData4054693732.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void GeometryBuffer::.ctor()
extern "C"  void GeometryBuffer__ctor_m1326262817 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushObject(System.String)
extern "C"  void GeometryBuffer_PushObject_m2300331576 (GeometryBuffer_t1734374890 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushGroup(System.String)
extern "C"  void GeometryBuffer_PushGroup_m3398090876 (GeometryBuffer_t1734374890 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushMaterialName(System.String)
extern "C"  void GeometryBuffer_PushMaterialName_m3037003365 (GeometryBuffer_t1734374890 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushVertex(UnityEngine.Vector3)
extern "C"  void GeometryBuffer_PushVertex_m584175552 (GeometryBuffer_t1734374890 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushUV(UnityEngine.Vector2)
extern "C"  void GeometryBuffer_PushUV_m1416883622 (GeometryBuffer_t1734374890 * __this, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushNormal(UnityEngine.Vector3)
extern "C"  void GeometryBuffer_PushNormal_m2021874081 (GeometryBuffer_t1734374890 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PushFaceRow(GeometryBuffer/FaceIndicesRow)
extern "C"  void GeometryBuffer_PushFaceRow_m1558056977 (GeometryBuffer_t1734374890 * __this, FaceIndicesRow_t3099920559 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::Trace()
extern "C"  void GeometryBuffer_Trace_m589360368 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GeometryBuffer::get_numObjects()
extern "C"  int32_t GeometryBuffer_get_numObjects_m926137318 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::get_isEmpty()
extern "C"  bool GeometryBuffer_get_isEmpty_m2228850899 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::get_hasUVs()
extern "C"  bool GeometryBuffer_get_hasUVs_m1331414346 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::get_hasNormals()
extern "C"  bool GeometryBuffer_get_hasNormals_m2163924762 (GeometryBuffer_t1734374890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::PopulateMeshes(UnityEngine.GameObject[],System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>)
extern "C"  void GeometryBuffer_PopulateMeshes_m3698467521 (GeometryBuffer_t1734374890 * __this, GameObjectU5BU5D_t3057952154* ___gs0, Dictionary_2_t2108486189 * ___mats1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::HasObjectUV(GeometryBuffer/ObjectData)
extern "C"  bool GeometryBuffer_HasObjectUV_m1165816705 (GeometryBuffer_t1734374890 * __this, ObjectData_t4054693732 * ___od0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::HasObjectNormals(GeometryBuffer/ObjectData)
extern "C"  bool GeometryBuffer_HasObjectNormals_m3182962132 (GeometryBuffer_t1734374890 * __this, ObjectData_t4054693732 * ___od0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GeometryBuffer::CountTotalObjectFaces(GeometryBuffer/ObjectData)
extern "C"  int32_t GeometryBuffer_CountTotalObjectFaces_m3138691021 (GeometryBuffer_t1734374890 * __this, ObjectData_t4054693732 * ___od0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GeometryBuffer::AddVertexToList(System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>,System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,FaceIndices)
extern "C"  bool GeometryBuffer_AddVertexToList_m3122635951 (GeometryBuffer_t1734374890 * __this, Dictionary_2_t2885703635 * ___vertDic0, Dictionary_2_t1251533215 * ___vertOrderedDic1, List_1_t1440998580 * ___tempTriangles2, List_1_t1612828711 * ___tempUvs3, List_1_t1612828712 * ___tempNormals4, FaceIndices_t174955414  ___face5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GeometryBuffer::.cctor()
extern "C"  void GeometryBuffer__cctor_m3123717540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
