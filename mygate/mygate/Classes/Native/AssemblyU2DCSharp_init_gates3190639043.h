﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// webservice
struct webservice_t1109616683;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// init_gates
struct  init_gates_t3190639043  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 init_gates::start
	int32_t ___start_2;
	// System.Int32 init_gates::limit
	int32_t ___limit_3;
	// webservice init_gates::wb
	webservice_t1109616683 * ___wb_4;

public:
	inline static int32_t get_offset_of_start_2() { return static_cast<int32_t>(offsetof(init_gates_t3190639043, ___start_2)); }
	inline int32_t get_start_2() const { return ___start_2; }
	inline int32_t* get_address_of_start_2() { return &___start_2; }
	inline void set_start_2(int32_t value)
	{
		___start_2 = value;
	}

	inline static int32_t get_offset_of_limit_3() { return static_cast<int32_t>(offsetof(init_gates_t3190639043, ___limit_3)); }
	inline int32_t get_limit_3() const { return ___limit_3; }
	inline int32_t* get_address_of_limit_3() { return &___limit_3; }
	inline void set_limit_3(int32_t value)
	{
		___limit_3 = value;
	}

	inline static int32_t get_offset_of_wb_4() { return static_cast<int32_t>(offsetof(init_gates_t3190639043, ___wb_4)); }
	inline webservice_t1109616683 * get_wb_4() const { return ___wb_4; }
	inline webservice_t1109616683 ** get_address_of_wb_4() { return &___wb_4; }
	inline void set_wb_4(webservice_t1109616683 * value)
	{
		___wb_4 = value;
		Il2CppCodeGenWriteBarrier(&___wb_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
