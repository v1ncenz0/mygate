﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1456340358;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>[]
struct KeyValuePair_2U5BU5D_t2620219005;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct IEnumerator_1_t3661990695;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector3,System.Object>
struct KeyCollection_t2322684825;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector3,System.Object>
struct ValueCollection_t2837214193;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1159211756.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2178610434_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2178610434(__this, method) ((  void (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2__ctor_m2178610434_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1180196707_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1180196707(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1180196707_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3999861877_gshared (Dictionary_2_t4134154350 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3999861877(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4134154350 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3999861877_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m542283243_gshared (Dictionary_2_t4134154350 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m542283243(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4134154350 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m542283243_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3731360370_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3731360370(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3731360370_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3316888531_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3316888531(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3316888531_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3481667988_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3481667988(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3481667988_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3691184188_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3691184188(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3691184188_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3048635743_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3048635743(__this, ___key0, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3048635743_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2945193498_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2945193498(__this, method) ((  bool (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2945193498_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3779156442_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3779156442(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3779156442_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2822582452_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2822582452(__this, method) ((  bool (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2822582452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2430645851_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2_t1891499572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2430645851(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4134154350 *, KeyValuePair_2_t1891499572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2430645851_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1168165081_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2_t1891499572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1168165081(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4134154350 *, KeyValuePair_2_t1891499572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1168165081_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3812467023_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2U5BU5D_t2620219005* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3812467023(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4134154350 *, KeyValuePair_2U5BU5D_t2620219005*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3812467023_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3763652566_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2_t1891499572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3763652566(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4134154350 *, KeyValuePair_2_t1891499572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3763652566_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3860995546_gshared (Dictionary_2_t4134154350 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3860995546(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3860995546_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592503861_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592503861(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592503861_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1113832796_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1113832796(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1113832796_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3980817759_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3980817759(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3980817759_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2979874962_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2979874962(__this, method) ((  int32_t (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_get_Count_m2979874962_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m585706047_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m585706047(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_get_Item_m585706047_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1666851200_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1666851200(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1666851200_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2418763790_gshared (Dictionary_2_t4134154350 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2418763790(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4134154350 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2418763790_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2821920421_gshared (Dictionary_2_t4134154350 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2821920421(__this, ___size0, method) ((  void (*) (Dictionary_2_t4134154350 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2821920421_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1459368471_gshared (Dictionary_2_t4134154350 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1459368471(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1459368471_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1891499572  Dictionary_2_make_pair_m128022557_gshared (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m128022557(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1891499572  (*) (Il2CppObject * /* static, unused */, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m128022557_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::pick_key(TKey,TValue)
extern "C"  Vector3_t2243707580  Dictionary_2_pick_key_m1207135077_gshared (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1207135077(__this /* static, unused */, ___key0, ___value1, method) ((  Vector3_t2243707580  (*) (Il2CppObject * /* static, unused */, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1207135077_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m246360973_gshared (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m246360973(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m246360973_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1422240060_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2U5BU5D_t2620219005* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1422240060(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4134154350 *, KeyValuePair_2U5BU5D_t2620219005*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1422240060_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1339823172_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1339823172(__this, method) ((  void (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_Resize_m1339823172_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2483083761_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2483083761(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2483083761_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2092253141_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2092253141(__this, method) ((  void (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_Clear_m2092253141_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m719750399_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m719750399(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_ContainsKey_m719750399_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3983713655_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3983713655(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3983713655_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3526172102_gshared (Dictionary_2_t4134154350 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3526172102(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4134154350 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3526172102_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3695248632_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3695248632(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3695248632_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m4246675945_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m4246675945(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , const MethodInfo*))Dictionary_2_Remove_m4246675945_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1337464124_gshared (Dictionary_2_t4134154350 * __this, Vector3_t2243707580  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1337464124(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4134154350 *, Vector3_t2243707580 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1337464124_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::get_Keys()
extern "C"  KeyCollection_t2322684825 * Dictionary_2_get_Keys_m202322123_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m202322123(__this, method) ((  KeyCollection_t2322684825 * (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_get_Keys_m202322123_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::get_Values()
extern "C"  ValueCollection_t2837214193 * Dictionary_2_get_Values_m1086848843_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1086848843(__this, method) ((  ValueCollection_t2837214193 * (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_get_Values_m1086848843_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::ToTKey(System.Object)
extern "C"  Vector3_t2243707580  Dictionary_2_ToTKey_m3278166686_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3278166686(__this, ___key0, method) ((  Vector3_t2243707580  (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3278166686_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m4055254206_gshared (Dictionary_2_t4134154350 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4055254206(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t4134154350 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4055254206_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3104162316_gshared (Dictionary_2_t4134154350 * __this, KeyValuePair_2_t1891499572  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3104162316(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4134154350 *, KeyValuePair_2_t1891499572 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3104162316_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1159211756  Dictionary_2_GetEnumerator_m1751708881_gshared (Dictionary_2_t4134154350 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1751708881(__this, method) ((  Enumerator_t1159211756  (*) (Dictionary_2_t4134154350 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1751708881_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3233565398_gshared (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3233565398(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3233565398_gshared)(__this /* static, unused */, ___key0, ___value1, method)
