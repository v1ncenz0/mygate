﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,System.Object>
struct Dictionary_2_t4134154350;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1525719818.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1700120030_gshared (Enumerator_t1525719818 * __this, Dictionary_2_t4134154350 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1700120030(__this, ___host0, method) ((  void (*) (Enumerator_t1525719818 *, Dictionary_2_t4134154350 *, const MethodInfo*))Enumerator__ctor_m1700120030_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1006470943_gshared (Enumerator_t1525719818 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1006470943(__this, method) ((  Il2CppObject * (*) (Enumerator_t1525719818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1006470943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m342544931_gshared (Enumerator_t1525719818 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m342544931(__this, method) ((  void (*) (Enumerator_t1525719818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m342544931_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2937147778_gshared (Enumerator_t1525719818 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2937147778(__this, method) ((  void (*) (Enumerator_t1525719818 *, const MethodInfo*))Enumerator_Dispose_m2937147778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4071702087_gshared (Enumerator_t1525719818 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4071702087(__this, method) ((  bool (*) (Enumerator_t1525719818 *, const MethodInfo*))Enumerator_MoveNext_m4071702087_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1837101021_gshared (Enumerator_t1525719818 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1837101021(__this, method) ((  Il2CppObject * (*) (Enumerator_t1525719818 *, const MethodInfo*))Enumerator_get_Current_m1837101021_gshared)(__this, method)
