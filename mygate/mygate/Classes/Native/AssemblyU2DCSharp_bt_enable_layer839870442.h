﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemColor
struct ItemColor_t1117508970;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_enable_layer
struct  bt_enable_layer_t839870442  : public MonoBehaviour_t1158329972
{
public:
	// ItemColor bt_enable_layer::layer_color
	ItemColor_t1117508970 * ___layer_color_2;

public:
	inline static int32_t get_offset_of_layer_color_2() { return static_cast<int32_t>(offsetof(bt_enable_layer_t839870442, ___layer_color_2)); }
	inline ItemColor_t1117508970 * get_layer_color_2() const { return ___layer_color_2; }
	inline ItemColor_t1117508970 ** get_address_of_layer_color_2() { return &___layer_color_2; }
	inline void set_layer_color_2(ItemColor_t1117508970 * value)
	{
		___layer_color_2 = value;
		Il2CppCodeGenWriteBarrier(&___layer_color_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
