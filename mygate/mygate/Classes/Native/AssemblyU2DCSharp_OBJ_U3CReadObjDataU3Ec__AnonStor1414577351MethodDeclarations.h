﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/<ReadObjData>c__AnonStorey5
struct U3CReadObjDataU3Ec__AnonStorey5_t1414577351;

#include "codegen/il2cpp-codegen.h"

// System.Void OBJ/<ReadObjData>c__AnonStorey5::.ctor()
extern "C"  void U3CReadObjDataU3Ec__AnonStorey5__ctor_m3051062070 (U3CReadObjDataU3Ec__AnonStorey5_t1414577351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBJ/<ReadObjData>c__AnonStorey5::<>m__0(System.Boolean)
extern "C"  void U3CReadObjDataU3Ec__AnonStorey5_U3CU3Em__0_m2464783400 (U3CReadObjDataU3Ec__AnonStorey5_t1414577351 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
