﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// toolbar
struct toolbar_t3757582107;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void toolbar::.ctor()
extern "C"  void toolbar__ctor_m1344610454 (toolbar_t3757582107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void toolbar::EditMode()
extern "C"  void toolbar_EditMode_m3251707441 (toolbar_t3757582107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void toolbar::DeleteMode()
extern "C"  void toolbar_DeleteMode_m1077525364 (toolbar_t3757582107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void toolbar::setToggle(System.String,System.Boolean)
extern "C"  void toolbar_setToggle_m955304583 (toolbar_t3757582107 * __this, String_t* ___bt_name0, bool ___enable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
