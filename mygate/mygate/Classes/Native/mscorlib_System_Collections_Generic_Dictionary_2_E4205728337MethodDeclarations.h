﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1159211756MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m491847637(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4205728337 *, Dictionary_2_t2885703635 *, const MethodInfo*))Enumerator__ctor_m3707044332_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1033403384(__this, method) ((  Il2CppObject * (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m359647425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2459984232(__this, method) ((  void (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3678245697_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4126629863(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3603310410_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2507239866(__this, method) ((  Il2CppObject * (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1384610131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1467160676(__this, method) ((  Il2CppObject * (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3475591035_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m728816948(__this, method) ((  bool (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_MoveNext_m850524225_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m1899905672(__this, method) ((  KeyValuePair_2_t643048857  (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_get_Current_m3161629733_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1600598449(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_get_CurrentKey_m2950231828_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2362756433(__this, method) ((  List_1_t1440998580 * (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_get_CurrentValue_m965660436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::Reset()
#define Enumerator_Reset_m1196860467(__this, method) ((  void (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_Reset_m4021617514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m3315003100(__this, method) ((  void (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_VerifyState_m3966567443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2200644800(__this, method) ((  void (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_VerifyCurrent_m2762011769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m4002908109(__this, method) ((  void (*) (Enumerator_t4205728337 *, const MethodInfo*))Enumerator_Dispose_m776557428_gshared)(__this, method)
