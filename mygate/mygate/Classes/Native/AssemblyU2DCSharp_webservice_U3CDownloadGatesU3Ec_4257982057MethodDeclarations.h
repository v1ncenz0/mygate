﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<DownloadGates>c__Iterator6
struct U3CDownloadGatesU3Ec__Iterator6_t4257982057;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<DownloadGates>c__Iterator6::.ctor()
extern "C"  void U3CDownloadGatesU3Ec__Iterator6__ctor_m1445987916 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<DownloadGates>c__Iterator6::MoveNext()
extern "C"  bool U3CDownloadGatesU3Ec__Iterator6_MoveNext_m780117648 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadGates>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadGatesU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1748583260 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadGates>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadGatesU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3821988820 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadGates>c__Iterator6::Dispose()
extern "C"  void U3CDownloadGatesU3Ec__Iterator6_Dispose_m813319179 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadGates>c__Iterator6::Reset()
extern "C"  void U3CDownloadGatesU3Ec__Iterator6_Reset_m614538373 (U3CDownloadGatesU3Ec__Iterator6_t4257982057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
