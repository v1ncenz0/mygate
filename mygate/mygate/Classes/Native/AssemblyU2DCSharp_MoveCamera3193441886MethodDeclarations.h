﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveCamera
struct MoveCamera_t3193441886;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveCamera::.ctor()
extern "C"  void MoveCamera__ctor_m3742198839 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::Start()
extern "C"  void MoveCamera_Start_m2614974443 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::Update()
extern "C"  void MoveCamera_Update_m2357030312 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::rotateCamera(System.Int32)
extern "C"  void MoveCamera_rotateCamera_m2825396440 (MoveCamera_t3193441886 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::goLeftFor30Degree()
extern "C"  void MoveCamera_goLeftFor30Degree_m3478655472 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::goRightFor30Degree()
extern "C"  void MoveCamera_goRightFor30Degree_m2958619467 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::goInFront()
extern "C"  void MoveCamera_goInFront_m2904176181 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::goToBack()
extern "C"  void MoveCamera_goToBack_m4211329935 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::Rotate()
extern "C"  void MoveCamera_Rotate_m1683161066 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::Rotate2()
extern "C"  void MoveCamera_Rotate2_m2014046366 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::panup(System.Single)
extern "C"  void MoveCamera_panup_m464268524 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::pandw(System.Single)
extern "C"  void MoveCamera_pandw_m1725043716 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::pandx(System.Single)
extern "C"  void MoveCamera_pandx_m1794096041 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::pansx(System.Single)
extern "C"  void MoveCamera_pansx_m3872725182 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::rotatesx(System.Single)
extern "C"  void MoveCamera_rotatesx_m822058662 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::rotatedx(System.Single)
extern "C"  void MoveCamera_rotatedx_m411739795 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::zoomIN(System.Single)
extern "C"  void MoveCamera_zoomIN_m1117741010 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::zoomOUT(System.Single)
extern "C"  void MoveCamera_zoomOUT_m3175961351 (MoveCamera_t3193441886 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::resetCamera(System.Boolean)
extern "C"  void MoveCamera_resetCamera_m291253408 (MoveCamera_t3193441886 * __this, bool ___saveLastCameraPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCamera::lastCamera()
extern "C"  void MoveCamera_lastCamera_m2455429698 (MoveCamera_t3193441886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
