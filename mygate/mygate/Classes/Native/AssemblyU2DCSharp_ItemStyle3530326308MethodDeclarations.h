﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemStyle
struct ItemStyle_t3530326308;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemStyle::.ctor()
extern "C"  void ItemStyle__ctor_m1756803737 (ItemStyle_t3530326308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemStyle::downloadImage(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemStyle_downloadImage_m664145617 (ItemStyle_t3530326308 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemStyle::setCellsElements()
extern "C"  void ItemStyle_setCellsElements_m1158627615 (ItemStyle_t3530326308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
