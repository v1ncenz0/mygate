﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemGate/<downloadImage>c__Iterator0
struct U3CdownloadImageU3Ec__Iterator0_t1853153015;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemGate/<downloadImage>c__Iterator0::.ctor()
extern "C"  void U3CdownloadImageU3Ec__Iterator0__ctor_m4157440272 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemGate/<downloadImage>c__Iterator0::MoveNext()
extern "C"  bool U3CdownloadImageU3Ec__Iterator0_MoveNext_m3704132628 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemGate/<downloadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3125177020 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemGate/<downloadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4058035124 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGate/<downloadImage>c__Iterator0::Dispose()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Dispose_m3352121597 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemGate/<downloadImage>c__Iterator0::Reset()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Reset_m3362583975 (U3CdownloadImageU3Ec__Iterator0_t1853153015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
