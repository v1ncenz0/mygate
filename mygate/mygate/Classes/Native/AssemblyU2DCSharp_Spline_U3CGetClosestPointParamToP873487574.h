﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline/<GetClosestPointParamToPlane>c__AnonStorey2
struct  U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574  : public Il2CppObject
{
public:
	// UnityEngine.Plane Spline/<GetClosestPointParamToPlane>c__AnonStorey2::plane
	Plane_t3727654732  ___plane_0;

public:
	inline static int32_t get_offset_of_plane_0() { return static_cast<int32_t>(offsetof(U3CGetClosestPointParamToPlaneU3Ec__AnonStorey2_t873487574, ___plane_0)); }
	inline Plane_t3727654732  get_plane_0() const { return ___plane_0; }
	inline Plane_t3727654732 * get_address_of_plane_0() { return &___plane_0; }
	inline void set_plane_0(Plane_t3727654732  value)
	{
		___plane_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
