﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Item
struct Item_t2440468191;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ControlsDesign
struct ControlsDesign_t1600104368;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<ThreadaddCell>c__Iterator0
struct  U3CThreadaddCellU3Ec__Iterator0_t1030518217  : public Il2CppObject
{
public:
	// Item ControlsDesign/<ThreadaddCell>c__Iterator0::item
	Item_t2440468191 * ___item_0;
	// UnityEngine.Vector3 ControlsDesign/<ThreadaddCell>c__Iterator0::<pos>__0
	Vector3_t2243707580  ___U3CposU3E__0_1;
	// System.Action`1<System.Boolean> ControlsDesign/<ThreadaddCell>c__Iterator0::added
	Action_1_t3627374100 * ___added_2;
	// ControlsDesign ControlsDesign/<ThreadaddCell>c__Iterator0::$this
	ControlsDesign_t1600104368 * ___U24this_3;
	// System.Object ControlsDesign/<ThreadaddCell>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean ControlsDesign/<ThreadaddCell>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ControlsDesign/<ThreadaddCell>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___item_0)); }
	inline Item_t2440468191 * get_item_0() const { return ___item_0; }
	inline Item_t2440468191 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_t2440468191 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_U3CposU3E__0_1() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___U3CposU3E__0_1)); }
	inline Vector3_t2243707580  get_U3CposU3E__0_1() const { return ___U3CposU3E__0_1; }
	inline Vector3_t2243707580 * get_address_of_U3CposU3E__0_1() { return &___U3CposU3E__0_1; }
	inline void set_U3CposU3E__0_1(Vector3_t2243707580  value)
	{
		___U3CposU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_added_2() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___added_2)); }
	inline Action_1_t3627374100 * get_added_2() const { return ___added_2; }
	inline Action_1_t3627374100 ** get_address_of_added_2() { return &___added_2; }
	inline void set_added_2(Action_1_t3627374100 * value)
	{
		___added_2 = value;
		Il2CppCodeGenWriteBarrier(&___added_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___U24this_3)); }
	inline ControlsDesign_t1600104368 * get_U24this_3() const { return ___U24this_3; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ControlsDesign_t1600104368 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CThreadaddCellU3Ec__Iterator0_t1030518217, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
