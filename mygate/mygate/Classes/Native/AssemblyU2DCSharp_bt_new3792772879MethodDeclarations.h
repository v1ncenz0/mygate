﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_new
struct bt_new_t3792772879;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_new::.ctor()
extern "C"  void bt_new__ctor_m2335336626 (bt_new_t3792772879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_new::goToWizard()
extern "C"  void bt_new_goToWizard_m3009975580 (bt_new_t3792772879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
