﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_register
struct bt_register_t2424814550;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_register::.ctor()
extern "C"  void bt_register__ctor_m2443543091 (bt_register_t2424814550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_register::goToRegistering()
extern "C"  void bt_register_goToRegistering_m3641792189 (bt_register_t2424814550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
