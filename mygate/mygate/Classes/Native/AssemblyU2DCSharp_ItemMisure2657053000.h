﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemMisure
struct  ItemMisure_t2657053000  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemMisure::id
	int32_t ___id_2;
	// System.Int32 ItemMisure::min_width
	int32_t ___min_width_3;
	// System.Int32 ItemMisure::max_width
	int32_t ___max_width_4;
	// System.Int32 ItemMisure::area_lateral
	int32_t ___area_lateral_5;
	// System.Int32 ItemMisure::area_bottom
	int32_t ___area_bottom_6;
	// System.Int32 ItemMisure::area_top
	int32_t ___area_top_7;
	// System.Int32 ItemMisure::width_frame
	int32_t ___width_frame_8;
	// System.Int32 ItemMisure::width_central_frame
	int32_t ___width_central_frame_9;
	// System.Int32 ItemMisure::height_top_frame
	int32_t ___height_top_frame_10;
	// System.Int32 ItemMisure::height_bottom_frame
	int32_t ___height_bottom_frame_11;
	// System.Single ItemMisure::factor
	float ___factor_12;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_min_width_3() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___min_width_3)); }
	inline int32_t get_min_width_3() const { return ___min_width_3; }
	inline int32_t* get_address_of_min_width_3() { return &___min_width_3; }
	inline void set_min_width_3(int32_t value)
	{
		___min_width_3 = value;
	}

	inline static int32_t get_offset_of_max_width_4() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___max_width_4)); }
	inline int32_t get_max_width_4() const { return ___max_width_4; }
	inline int32_t* get_address_of_max_width_4() { return &___max_width_4; }
	inline void set_max_width_4(int32_t value)
	{
		___max_width_4 = value;
	}

	inline static int32_t get_offset_of_area_lateral_5() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___area_lateral_5)); }
	inline int32_t get_area_lateral_5() const { return ___area_lateral_5; }
	inline int32_t* get_address_of_area_lateral_5() { return &___area_lateral_5; }
	inline void set_area_lateral_5(int32_t value)
	{
		___area_lateral_5 = value;
	}

	inline static int32_t get_offset_of_area_bottom_6() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___area_bottom_6)); }
	inline int32_t get_area_bottom_6() const { return ___area_bottom_6; }
	inline int32_t* get_address_of_area_bottom_6() { return &___area_bottom_6; }
	inline void set_area_bottom_6(int32_t value)
	{
		___area_bottom_6 = value;
	}

	inline static int32_t get_offset_of_area_top_7() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___area_top_7)); }
	inline int32_t get_area_top_7() const { return ___area_top_7; }
	inline int32_t* get_address_of_area_top_7() { return &___area_top_7; }
	inline void set_area_top_7(int32_t value)
	{
		___area_top_7 = value;
	}

	inline static int32_t get_offset_of_width_frame_8() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___width_frame_8)); }
	inline int32_t get_width_frame_8() const { return ___width_frame_8; }
	inline int32_t* get_address_of_width_frame_8() { return &___width_frame_8; }
	inline void set_width_frame_8(int32_t value)
	{
		___width_frame_8 = value;
	}

	inline static int32_t get_offset_of_width_central_frame_9() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___width_central_frame_9)); }
	inline int32_t get_width_central_frame_9() const { return ___width_central_frame_9; }
	inline int32_t* get_address_of_width_central_frame_9() { return &___width_central_frame_9; }
	inline void set_width_central_frame_9(int32_t value)
	{
		___width_central_frame_9 = value;
	}

	inline static int32_t get_offset_of_height_top_frame_10() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___height_top_frame_10)); }
	inline int32_t get_height_top_frame_10() const { return ___height_top_frame_10; }
	inline int32_t* get_address_of_height_top_frame_10() { return &___height_top_frame_10; }
	inline void set_height_top_frame_10(int32_t value)
	{
		___height_top_frame_10 = value;
	}

	inline static int32_t get_offset_of_height_bottom_frame_11() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___height_bottom_frame_11)); }
	inline int32_t get_height_bottom_frame_11() const { return ___height_bottom_frame_11; }
	inline int32_t* get_address_of_height_bottom_frame_11() { return &___height_bottom_frame_11; }
	inline void set_height_bottom_frame_11(int32_t value)
	{
		___height_bottom_frame_11 = value;
	}

	inline static int32_t get_offset_of_factor_12() { return static_cast<int32_t>(offsetof(ItemMisure_t2657053000, ___factor_12)); }
	inline float get_factor_12() const { return ___factor_12; }
	inline float* get_address_of_factor_12() { return &___factor_12; }
	inline void set_factor_12(float value)
	{
		___factor_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
