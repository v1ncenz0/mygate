﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_scene/<downloadImage>c__Iterator0
struct U3CdownloadImageU3Ec__Iterator0_t3077030272;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_scene/<downloadImage>c__Iterator0::.ctor()
extern "C"  void U3CdownloadImageU3Ec__Iterator0__ctor_m2425520755 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean bt_scene/<downloadImage>c__Iterator0::MoveNext()
extern "C"  bool U3CdownloadImageU3Ec__Iterator0_MoveNext_m456793137 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bt_scene/<downloadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2755144477 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bt_scene/<downloadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2152171045 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene/<downloadImage>c__Iterator0::Dispose()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Dispose_m3384157086 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_scene/<downloadImage>c__Iterator0::Reset()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Reset_m3870300116 (U3CdownloadImageU3Ec__Iterator0_t3077030272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
