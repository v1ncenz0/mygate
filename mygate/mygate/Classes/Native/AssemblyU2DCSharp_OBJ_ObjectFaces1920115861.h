﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJ/ObjectFaces
struct  ObjectFaces_t1920115861  : public Il2CppObject
{
public:
	// System.String OBJ/ObjectFaces::obj
	String_t* ___obj_0;
	// System.Int32 OBJ/ObjectFaces::faces
	int32_t ___faces_1;
	// System.Int32 OBJ/ObjectFaces::verts
	int32_t ___verts_2;
	// System.Int32 OBJ/ObjectFaces::normals
	int32_t ___normals_3;
	// System.Int32 OBJ/ObjectFaces::uvs
	int32_t ___uvs_4;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(ObjectFaces_t1920115861, ___obj_0)); }
	inline String_t* get_obj_0() const { return ___obj_0; }
	inline String_t** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(String_t* value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_faces_1() { return static_cast<int32_t>(offsetof(ObjectFaces_t1920115861, ___faces_1)); }
	inline int32_t get_faces_1() const { return ___faces_1; }
	inline int32_t* get_address_of_faces_1() { return &___faces_1; }
	inline void set_faces_1(int32_t value)
	{
		___faces_1 = value;
	}

	inline static int32_t get_offset_of_verts_2() { return static_cast<int32_t>(offsetof(ObjectFaces_t1920115861, ___verts_2)); }
	inline int32_t get_verts_2() const { return ___verts_2; }
	inline int32_t* get_address_of_verts_2() { return &___verts_2; }
	inline void set_verts_2(int32_t value)
	{
		___verts_2 = value;
	}

	inline static int32_t get_offset_of_normals_3() { return static_cast<int32_t>(offsetof(ObjectFaces_t1920115861, ___normals_3)); }
	inline int32_t get_normals_3() const { return ___normals_3; }
	inline int32_t* get_address_of_normals_3() { return &___normals_3; }
	inline void set_normals_3(int32_t value)
	{
		___normals_3 = value;
	}

	inline static int32_t get_offset_of_uvs_4() { return static_cast<int32_t>(offsetof(ObjectFaces_t1920115861, ___uvs_4)); }
	inline int32_t get_uvs_4() const { return ___uvs_4; }
	inline int32_t* get_address_of_uvs_4() { return &___uvs_4; }
	inline void set_uvs_4(int32_t value)
	{
		___uvs_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
