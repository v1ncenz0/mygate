﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_element/<setImage>c__Iterator0
struct U3CsetImageU3Ec__Iterator0_t3057083896;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_element/<setImage>c__Iterator0::.ctor()
extern "C"  void U3CsetImageU3Ec__Iterator0__ctor_m287586245 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean bt_element/<setImage>c__Iterator0::MoveNext()
extern "C"  bool U3CsetImageU3Ec__Iterator0_MoveNext_m613500143 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bt_element/<setImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CsetImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m255382195 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bt_element/<setImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CsetImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1711408395 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_element/<setImage>c__Iterator0::Dispose()
extern "C"  void U3CsetImageU3Ec__Iterator0_Dispose_m2794225882 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_element/<setImage>c__Iterator0::Reset()
extern "C"  void U3CsetImageU3Ec__Iterator0_Reset_m2809920088 (U3CsetImageU3Ec__Iterator0_t3057083896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
