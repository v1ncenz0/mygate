﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjExporterScript
struct ObjExporterScript_t792682685;
// System.String
struct String_t;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ObjExporterScript::.ctor()
extern "C"  void ObjExporterScript__ctor_m923899170 (ObjExporterScript_t792682685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporterScript::Start()
extern "C"  void ObjExporterScript_Start_m3681608962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporterScript::End()
extern "C"  void ObjExporterScript_End_m272269495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ObjExporterScript::MeshToString(UnityEngine.MeshFilter,UnityEngine.Transform)
extern "C"  String_t* ObjExporterScript_MeshToString_m2945575735 (Il2CppObject * __this /* static, unused */, MeshFilter_t3026937449 * ___mf0, Transform_t3275118058 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporterScript::addMaterial(System.String,UnityEngine.Color)
extern "C"  void ObjExporterScript_addMaterial_m3668252712 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporterScript::.cctor()
extern "C"  void ObjExporterScript__cctor_m3069986823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
