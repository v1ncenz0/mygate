﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// changeScroll
struct changeScroll_t2128272085;

#include "codegen/il2cpp-codegen.h"

// System.Void changeScroll::.ctor()
extern "C"  void changeScroll__ctor_m476420552 (changeScroll_t2128272085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void changeScroll::changeValue()
extern "C"  void changeScroll_changeValue_m3055043897 (changeScroll_t2128272085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
