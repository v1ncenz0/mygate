﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CursorElement
struct CursorElement_t1656362794;
// Item
struct Item_t2440468191;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void CursorElement::.ctor()
extern "C"  void CursorElement__ctor_m2885971199 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::Start()
extern "C"  void CursorElement_Start_m3587659803 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CursorElement::DoubleClick()
extern "C"  bool CursorElement_DoubleClick_m3630390140 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::Update()
extern "C"  void CursorElement_Update_m2672223476 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CursorElement::fixposition(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CursorElement_fixposition_m1433105828 (CursorElement_t1656362794 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::setElementCanAdded(System.Boolean)
extern "C"  void CursorElement_setElementCanAdded_m3881359016 (CursorElement_t1656362794 * __this, bool ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::setItem(Item)
extern "C"  void CursorElement_setItem_m412567113 (CursorElement_t1656362794 * __this, Item_t2440468191 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::setArrows()
extern "C"  void CursorElement_setArrows_m3722984487 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::verifyLimit(System.Single,System.Single)
extern "C"  void CursorElement_verifyLimit_m3042901653 (CursorElement_t1656362794 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::OnMouseDown()
extern "C"  void CursorElement_OnMouseDown_m2915834101 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::OnMouseUp()
extern "C"  void CursorElement_OnMouseUp_m3206290792 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::Navigate()
extern "C"  void CursorElement_Navigate_m810301590 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::AddElement()
extern "C"  void CursorElement_AddElement_m1324243752 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::AddandMoveTo(UnityEngine.Vector2)
extern "C"  void CursorElement_AddandMoveTo_m3824872939 (CursorElement_t1656362794 * __this, Vector2_t2243707579  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::MoveLeft()
extern "C"  void CursorElement_MoveLeft_m4195030861 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::MoveRight()
extern "C"  void CursorElement_MoveRight_m3265237132 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::MoveTop()
extern "C"  void CursorElement_MoveTop_m2877805373 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::MoveDown()
extern "C"  void CursorElement_MoveDown_m2787265504 (CursorElement_t1656362794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::MoveNext(UnityEngine.Vector2)
extern "C"  void CursorElement_MoveNext_m3357496201 (CursorElement_t1656362794 * __this, Vector2_t2243707579  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::.cctor()
extern "C"  void CursorElement__cctor_m4045414144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CursorElement::<AddElement>m__0(System.Boolean)
extern "C"  void CursorElement_U3CAddElementU3Em__0_m490356386 (Il2CppObject * __this /* static, unused */, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
