﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3944620164_gshared (KeyValuePair_2_t1891499572 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3944620164(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1891499572 *, Vector3_t2243707580 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3944620164_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::get_Key()
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Key_m2722691310_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2722691310(__this, method) ((  Vector3_t2243707580  (*) (KeyValuePair_2_t1891499572 *, const MethodInfo*))KeyValuePair_2_get_Key_m2722691310_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1626646011_gshared (KeyValuePair_2_t1891499572 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1626646011(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1891499572 *, Vector3_t2243707580 , const MethodInfo*))KeyValuePair_2_set_Key_m1626646011_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2121445166_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2121445166(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1891499572 *, const MethodInfo*))KeyValuePair_2_get_Value_m2121445166_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2031621555_gshared (KeyValuePair_2_t1891499572 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2031621555(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1891499572 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2031621555_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3365230589_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3365230589(__this, method) ((  String_t* (*) (KeyValuePair_2_t1891499572 *, const MethodInfo*))KeyValuePair_2_ToString_m3365230589_gshared)(__this, method)
