﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_element
struct bt_element_t1221534273;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void bt_element::.ctor()
extern "C"  void bt_element__ctor_m3646025536 (bt_element_t1221534273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_element::setElement()
extern "C"  void bt_element_setElement_m3120080398 (bt_element_t1221534273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator bt_element::setImage(System.String)
extern "C"  Il2CppObject * bt_element_setImage_m833519893 (bt_element_t1221534273 * __this, String_t* ___url_image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
