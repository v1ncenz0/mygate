﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// events
struct events_t192635521;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void events::.ctor()
extern "C"  void events__ctor_m1762963057 (events_t192635521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void events::Start()
extern "C"  void events_Start_m3135376041 (events_t192635521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void events::init(System.String)
extern "C"  void events_init_m3029265161 (events_t192635521 * __this, String_t* ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void events::Main()
extern "C"  void events_Main_m871052604 (events_t192635521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
