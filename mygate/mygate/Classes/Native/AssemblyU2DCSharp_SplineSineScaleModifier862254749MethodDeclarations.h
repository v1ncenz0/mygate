﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineSineScaleModifier
struct SplineSineScaleModifier_t862254749;
// SplineMesh
struct SplineMesh_t1719246168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_SplineMesh1719246168.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// System.Void SplineSineScaleModifier::.ctor()
extern "C"  void SplineSineScaleModifier__ctor_m3473274618 (SplineSineScaleModifier_t862254749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineSineScaleModifier::ModifyVertex(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineSineScaleModifier_ModifyVertex_m138688542 (SplineSineScaleModifier_t862254749 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___vertex1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SplineSineScaleModifier::ModifyUV(SplineMesh,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  SplineSineScaleModifier_ModifyUV_m1343240683 (SplineSineScaleModifier_t862254749 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector2_t2243707579  ___uvCoord1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineSineScaleModifier::ModifyNormal(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineSineScaleModifier_ModifyNormal_m1985474095 (SplineSineScaleModifier_t862254749 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___normal1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 SplineSineScaleModifier::ModifyTangent(SplineMesh,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  SplineSineScaleModifier_ModifyTangent_m1628714675 (SplineSineScaleModifier_t862254749 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector4_t2243707581  ___tangent1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
