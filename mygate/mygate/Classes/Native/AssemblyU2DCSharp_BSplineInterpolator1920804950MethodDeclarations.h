﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BSplineInterpolator
struct BSplineInterpolator_t1920804950;

#include "codegen/il2cpp-codegen.h"

// System.Void BSplineInterpolator::.ctor()
extern "C"  void BSplineInterpolator__ctor_m373522963 (BSplineInterpolator_t1920804950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
