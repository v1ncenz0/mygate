﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_mygate
struct bt_mygate_t3681173754;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_mygate::.ctor()
extern "C"  void bt_mygate__ctor_m1143771641 (bt_mygate_t3681173754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_mygate::OpenMyGate()
extern "C"  void bt_mygate_OpenMyGate_m1371108124 (bt_mygate_t3681173754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
