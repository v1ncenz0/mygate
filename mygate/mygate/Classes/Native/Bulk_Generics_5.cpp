﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.List`1<FaceIndices>
struct List_1_t3839043842;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3758245971;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t867319046;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23303845733.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23303845733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21891499572MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3373773516.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3373773516MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3839043842.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3758245971.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867319046.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1350990071((KeyValuePair_2_t3749587448 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2726037047((KeyValuePair_2_t3749587448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3201181706_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2__ctor_m3201181706(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1435832840(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1350990071_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Key_m1350990071(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3690000728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2726037047_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Value_m2726037047(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1391611625_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1391611625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_ToString_m1391611625(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3014120993_gshared (KeyValuePair_2_t3303845733 * __this, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m313903746((KeyValuePair_2_t3303845733 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector3_t2243707580  L_1 = ___value1;
		KeyValuePair_2_set_Value_m997861426((KeyValuePair_2_t3303845733 *)__this, (Vector3_t2243707580 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3014120993_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	KeyValuePair_2__ctor_m3014120993(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m166250727_gshared (KeyValuePair_2_t3303845733 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m166250727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	return KeyValuePair_2_get_Key_m166250727(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m313903746_gshared (KeyValuePair_2_t3303845733 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m313903746_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	KeyValuePair_2_set_Key_m313903746(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Value_m3216406343_gshared (KeyValuePair_2_t3303845733 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Value_m3216406343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3216406343(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m997861426_gshared (KeyValuePair_2_t3303845733 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m997861426_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	KeyValuePair_2_set_Value_m997861426(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector3>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m4267512256_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4267512256_gshared (KeyValuePair_2_t3303845733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4267512256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m166250727((KeyValuePair_2_t3303845733 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m166250727((KeyValuePair_2_t3303845733 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Vector3_t2243707580  L_8 = KeyValuePair_2_get_Value_m3216406343((KeyValuePair_2_t3303845733 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		Vector3_t2243707580  L_9 = KeyValuePair_2_get_Value_m3216406343((KeyValuePair_2_t3303845733 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Vector3_t2243707580 )L_9;
		String_t* L_10 = Vector3_ToString_m3857189970((Vector3_t2243707580 *)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4267512256_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3303845733 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3303845733 *>(__this + 1);
	return KeyValuePair_2_ToString_m4267512256(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1222844869((KeyValuePair_2_t1174980068 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m965533293((KeyValuePair_2_t1174980068 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4040336782_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2__ctor_m4040336782(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2113318928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1222844869_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Key_m1222844869(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1916631176(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m965533293_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Value_m965533293(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1739958171_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1739958171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m1253164328((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_ToString_m1739958171(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1307112735((KeyValuePair_2_t3716250094 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1921288671((KeyValuePair_2_t3716250094 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1877755778_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2__ctor_m1877755778(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1454531804(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1307112735_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Key_m1307112735(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3699669100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1921288671_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Value_m1921288671(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1394661909_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1394661909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_ToString_m1394661909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1640124561_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m744486900((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1416408204((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1640124561_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2__ctor_m1640124561(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2561166459(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m744486900_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Key_m744486900(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Value_m499643803(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1416408204_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Value_m1416408204(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2613351884_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2613351884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_ToString_m2613351884(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3944620164_gshared (KeyValuePair_2_t1891499572 * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___key0;
		KeyValuePair_2_set_Key_m1626646011((KeyValuePair_2_t1891499572 *)__this, (Vector3_t2243707580 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2031621555((KeyValuePair_2_t1891499572 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3944620164_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	KeyValuePair_2__ctor_m3944620164(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::get_Key()
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Key_m2722691310_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Key_m2722691310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2722691310(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1626646011_gshared (KeyValuePair_2_t1891499572 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1626646011_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	KeyValuePair_2_set_Key_m1626646011(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2121445166_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2121445166_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2121445166(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2031621555_gshared (KeyValuePair_2_t1891499572 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2031621555_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	KeyValuePair_2_set_Value_m2031621555(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m3365230589_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3365230589_gshared (KeyValuePair_2_t1891499572 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3365230589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Vector3_t2243707580  L_2 = KeyValuePair_2_get_Key_m2722691310((KeyValuePair_2_t1891499572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		Vector3_t2243707580  L_3 = KeyValuePair_2_get_Key_m2722691310((KeyValuePair_2_t1891499572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Vector3_t2243707580 )L_3;
		String_t* L_4 = Vector3_ToString_m3857189970((Vector3_t2243707580 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m2121445166((KeyValuePair_2_t1891499572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m2121445166((KeyValuePair_2_t1891499572 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3365230589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1891499572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1891499572 *>(__this + 1);
	return KeyValuePair_2_ToString_m3365230589(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2553205409_gshared (Enumerator_t3373773516 * __this, List_1_t3839043842 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3839043842 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3839043842 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2553205409_AdjustorThunk (Il2CppObject * __this, List_1_t3839043842 * ___l0, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	Enumerator__ctor_m2553205409(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1577766193_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m767115607((Enumerator_t3373773516 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1577766193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1577766193(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<FaceIndices>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m767115607((Enumerator_t3373773516 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		FaceIndices_t174955414  L_2 = (FaceIndices_t174955414 )__this->get_current_3();
		FaceIndices_t174955414  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3387245145(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::Dispose()
extern "C"  void Enumerator_Dispose_m632182510_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3839043842 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m632182510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	Enumerator_Dispose_m632182510(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m767115607_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m767115607_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m767115607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3839043842 * L_0 = (List_1_t3839043842 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3373773516  L_1 = (*(Enumerator_t3373773516 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3839043842 * L_7 = (List_1_t3839043842 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m767115607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	Enumerator_VerifyState_m767115607(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<FaceIndices>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3916965222_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m767115607((Enumerator_t3373773516 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3839043842 * L_2 = (List_1_t3839043842 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3839043842 * L_4 = (List_1_t3839043842 *)__this->get_l_0();
		NullCheck(L_4);
		FaceIndicesU5BU5D_t2696217939* L_5 = (FaceIndicesU5BU5D_t2696217939*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		FaceIndices_t174955414  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3916965222_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	return Enumerator_MoveNext_m3916965222(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<FaceIndices>::get_Current()
extern "C"  FaceIndices_t174955414  Enumerator_get_Current_m892207596_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method)
{
	{
		FaceIndices_t174955414  L_0 = (FaceIndices_t174955414 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  FaceIndices_t174955414  Enumerator_get_Current_m892207596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3373773516 * _thisAdjusted = reinterpret_cast<Enumerator_t3373773516 *>(__this + 1);
	return Enumerator_get_Current_m892207596(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1614742070_gshared (Enumerator_t975728254 * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1440998580 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1614742070_AdjustorThunk (Il2CppObject * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator__ctor_m1614742070(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1016756388(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2154261170(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1440998580 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1274756239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_Dispose_m1274756239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2167629240_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2167629240_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2167629240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t975728254  L_1 = (*(Enumerator_t975728254 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1440998580 * L_7 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2167629240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_VerifyState_m2167629240(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4282865897_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1440998580 * L_4 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4282865897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_MoveNext_m4282865897(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1062633493_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1062633493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_get_Current_m1062633493(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3769601633_gshared (Enumerator_t1593300101 * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2058570427 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3769601633_AdjustorThunk (Il2CppObject * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator__ctor_m3769601633(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3440386353(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2853089017(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2058570427 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3736175406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_Dispose_m3736175406(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m825848279_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m825848279_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m825848279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1593300101  L_1 = (*(Enumerator_t1593300101 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m825848279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_VerifyState_m825848279(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2058570427 * L_4 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m44995089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_MoveNext_m44995089(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3108634708_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3108634708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_get_Current_m3108634708(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3021143890_gshared (Enumerator_t3292975645 * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3758245971 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3758245971 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3021143890_AdjustorThunk (Il2CppObject * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator__ctor_m3021143890(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m610822832(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t94157543  L_2 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		CustomAttributeNamedArgument_t94157543  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1278092846(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3704913451_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3758245971 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3704913451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_Dispose_m3704913451(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m739025304_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m739025304_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m739025304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3758245971 * L_0 = (List_1_t3758245971 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3292975645  L_1 = (*(Enumerator_t3292975645 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3758245971 * L_7 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m739025304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_VerifyState_m739025304(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m598197344_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3758245971 * L_2 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3758245971 * L_4 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t94157543  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m598197344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_MoveNext_m598197344(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_get_Current_m3860473239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3421311553_gshared (Enumerator_t402048720 * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	{
		List_1_t867319046 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t867319046 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3421311553_AdjustorThunk (Il2CppObject * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator__ctor_m3421311553(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1436660297(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t1498197914  L_2 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		CustomAttributeTypedArgument_t1498197914  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m355114893(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3434518394_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t867319046 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3434518394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_Dispose_m3434518394(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m435841047_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m435841047_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m435841047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t867319046 * L_0 = (List_1_t867319046 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t402048720  L_1 = (*(Enumerator_t402048720 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t867319046 * L_7 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m435841047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_VerifyState_m435841047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1792725673_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t867319046 * L_2 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t867319046 * L_4 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t1498197914  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1792725673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_MoveNext_m1792725673(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_get_Current_m1371324410(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
