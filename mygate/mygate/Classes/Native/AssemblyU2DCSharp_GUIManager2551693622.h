﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIManager
struct  GUIManager_t2551693622  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct GUIManager_t2551693622_StaticFields
{
public:
	// UnityEngine.GameObject GUIManager::cameracontrols
	GameObject_t1756533147 * ___cameracontrols_2;
	// UnityEngine.GameObject GUIManager::library
	GameObject_t1756533147 * ___library_3;
	// UnityEngine.GameObject GUIManager::layermanager
	GameObject_t1756533147 * ___layermanager_4;
	// UnityEngine.GameObject GUIManager::colorlibrary
	GameObject_t1756533147 * ___colorlibrary_5;
	// UnityEngine.GameObject GUIManager::toolbar
	GameObject_t1756533147 * ___toolbar_6;
	// UnityEngine.GameObject GUIManager::statusbar
	GameObject_t1756533147 * ___statusbar_7;
	// UnityEngine.GameObject GUIManager::library_scenes
	GameObject_t1756533147 * ___library_scenes_8;
	// UnityEngine.GameObject GUIManager::help
	GameObject_t1756533147 * ___help_9;
	// UnityEngine.GameObject GUIManager::confirmBox
	GameObject_t1756533147 * ___confirmBox_10;
	// System.String GUIManager::confirmBoxObjectName
	String_t* ___confirmBoxObjectName_11;
	// System.String GUIManager::confirmBoxComponentName
	String_t* ___confirmBoxComponentName_12;
	// System.String GUIManager::confirmBoxCommandName
	String_t* ___confirmBoxCommandName_13;
	// UnityEngine.GameObject GUIManager::dialogBox
	GameObject_t1756533147 * ___dialogBox_14;
	// UnityEngine.GameObject GUIManager::waitingBox
	GameObject_t1756533147 * ___waitingBox_15;
	// UnityEngine.GameObject GUIManager::loadingPanel
	GameObject_t1756533147 * ___loadingPanel_16;
	// UnityEngine.GameObject GUIManager::detailBox
	GameObject_t1756533147 * ___detailBox_17;

public:
	inline static int32_t get_offset_of_cameracontrols_2() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___cameracontrols_2)); }
	inline GameObject_t1756533147 * get_cameracontrols_2() const { return ___cameracontrols_2; }
	inline GameObject_t1756533147 ** get_address_of_cameracontrols_2() { return &___cameracontrols_2; }
	inline void set_cameracontrols_2(GameObject_t1756533147 * value)
	{
		___cameracontrols_2 = value;
		Il2CppCodeGenWriteBarrier(&___cameracontrols_2, value);
	}

	inline static int32_t get_offset_of_library_3() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___library_3)); }
	inline GameObject_t1756533147 * get_library_3() const { return ___library_3; }
	inline GameObject_t1756533147 ** get_address_of_library_3() { return &___library_3; }
	inline void set_library_3(GameObject_t1756533147 * value)
	{
		___library_3 = value;
		Il2CppCodeGenWriteBarrier(&___library_3, value);
	}

	inline static int32_t get_offset_of_layermanager_4() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___layermanager_4)); }
	inline GameObject_t1756533147 * get_layermanager_4() const { return ___layermanager_4; }
	inline GameObject_t1756533147 ** get_address_of_layermanager_4() { return &___layermanager_4; }
	inline void set_layermanager_4(GameObject_t1756533147 * value)
	{
		___layermanager_4 = value;
		Il2CppCodeGenWriteBarrier(&___layermanager_4, value);
	}

	inline static int32_t get_offset_of_colorlibrary_5() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___colorlibrary_5)); }
	inline GameObject_t1756533147 * get_colorlibrary_5() const { return ___colorlibrary_5; }
	inline GameObject_t1756533147 ** get_address_of_colorlibrary_5() { return &___colorlibrary_5; }
	inline void set_colorlibrary_5(GameObject_t1756533147 * value)
	{
		___colorlibrary_5 = value;
		Il2CppCodeGenWriteBarrier(&___colorlibrary_5, value);
	}

	inline static int32_t get_offset_of_toolbar_6() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___toolbar_6)); }
	inline GameObject_t1756533147 * get_toolbar_6() const { return ___toolbar_6; }
	inline GameObject_t1756533147 ** get_address_of_toolbar_6() { return &___toolbar_6; }
	inline void set_toolbar_6(GameObject_t1756533147 * value)
	{
		___toolbar_6 = value;
		Il2CppCodeGenWriteBarrier(&___toolbar_6, value);
	}

	inline static int32_t get_offset_of_statusbar_7() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___statusbar_7)); }
	inline GameObject_t1756533147 * get_statusbar_7() const { return ___statusbar_7; }
	inline GameObject_t1756533147 ** get_address_of_statusbar_7() { return &___statusbar_7; }
	inline void set_statusbar_7(GameObject_t1756533147 * value)
	{
		___statusbar_7 = value;
		Il2CppCodeGenWriteBarrier(&___statusbar_7, value);
	}

	inline static int32_t get_offset_of_library_scenes_8() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___library_scenes_8)); }
	inline GameObject_t1756533147 * get_library_scenes_8() const { return ___library_scenes_8; }
	inline GameObject_t1756533147 ** get_address_of_library_scenes_8() { return &___library_scenes_8; }
	inline void set_library_scenes_8(GameObject_t1756533147 * value)
	{
		___library_scenes_8 = value;
		Il2CppCodeGenWriteBarrier(&___library_scenes_8, value);
	}

	inline static int32_t get_offset_of_help_9() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___help_9)); }
	inline GameObject_t1756533147 * get_help_9() const { return ___help_9; }
	inline GameObject_t1756533147 ** get_address_of_help_9() { return &___help_9; }
	inline void set_help_9(GameObject_t1756533147 * value)
	{
		___help_9 = value;
		Il2CppCodeGenWriteBarrier(&___help_9, value);
	}

	inline static int32_t get_offset_of_confirmBox_10() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___confirmBox_10)); }
	inline GameObject_t1756533147 * get_confirmBox_10() const { return ___confirmBox_10; }
	inline GameObject_t1756533147 ** get_address_of_confirmBox_10() { return &___confirmBox_10; }
	inline void set_confirmBox_10(GameObject_t1756533147 * value)
	{
		___confirmBox_10 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBox_10, value);
	}

	inline static int32_t get_offset_of_confirmBoxObjectName_11() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___confirmBoxObjectName_11)); }
	inline String_t* get_confirmBoxObjectName_11() const { return ___confirmBoxObjectName_11; }
	inline String_t** get_address_of_confirmBoxObjectName_11() { return &___confirmBoxObjectName_11; }
	inline void set_confirmBoxObjectName_11(String_t* value)
	{
		___confirmBoxObjectName_11 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBoxObjectName_11, value);
	}

	inline static int32_t get_offset_of_confirmBoxComponentName_12() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___confirmBoxComponentName_12)); }
	inline String_t* get_confirmBoxComponentName_12() const { return ___confirmBoxComponentName_12; }
	inline String_t** get_address_of_confirmBoxComponentName_12() { return &___confirmBoxComponentName_12; }
	inline void set_confirmBoxComponentName_12(String_t* value)
	{
		___confirmBoxComponentName_12 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBoxComponentName_12, value);
	}

	inline static int32_t get_offset_of_confirmBoxCommandName_13() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___confirmBoxCommandName_13)); }
	inline String_t* get_confirmBoxCommandName_13() const { return ___confirmBoxCommandName_13; }
	inline String_t** get_address_of_confirmBoxCommandName_13() { return &___confirmBoxCommandName_13; }
	inline void set_confirmBoxCommandName_13(String_t* value)
	{
		___confirmBoxCommandName_13 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBoxCommandName_13, value);
	}

	inline static int32_t get_offset_of_dialogBox_14() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___dialogBox_14)); }
	inline GameObject_t1756533147 * get_dialogBox_14() const { return ___dialogBox_14; }
	inline GameObject_t1756533147 ** get_address_of_dialogBox_14() { return &___dialogBox_14; }
	inline void set_dialogBox_14(GameObject_t1756533147 * value)
	{
		___dialogBox_14 = value;
		Il2CppCodeGenWriteBarrier(&___dialogBox_14, value);
	}

	inline static int32_t get_offset_of_waitingBox_15() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___waitingBox_15)); }
	inline GameObject_t1756533147 * get_waitingBox_15() const { return ___waitingBox_15; }
	inline GameObject_t1756533147 ** get_address_of_waitingBox_15() { return &___waitingBox_15; }
	inline void set_waitingBox_15(GameObject_t1756533147 * value)
	{
		___waitingBox_15 = value;
		Il2CppCodeGenWriteBarrier(&___waitingBox_15, value);
	}

	inline static int32_t get_offset_of_loadingPanel_16() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___loadingPanel_16)); }
	inline GameObject_t1756533147 * get_loadingPanel_16() const { return ___loadingPanel_16; }
	inline GameObject_t1756533147 ** get_address_of_loadingPanel_16() { return &___loadingPanel_16; }
	inline void set_loadingPanel_16(GameObject_t1756533147 * value)
	{
		___loadingPanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___loadingPanel_16, value);
	}

	inline static int32_t get_offset_of_detailBox_17() { return static_cast<int32_t>(offsetof(GUIManager_t2551693622_StaticFields, ___detailBox_17)); }
	inline GameObject_t1756533147 * get_detailBox_17() const { return ___detailBox_17; }
	inline GameObject_t1756533147 ** get_address_of_detailBox_17() { return &___detailBox_17; }
	inline void set_detailBox_17(GameObject_t1756533147 * value)
	{
		___detailBox_17 = value;
		Il2CppCodeGenWriteBarrier(&___detailBox_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
