﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<downloadElement>c__AnonStoreyB
struct U3CdownloadElementU3Ec__AnonStoreyB_t3800062637;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<downloadElement>c__AnonStoreyB::.ctor()
extern "C"  void U3CdownloadElementU3Ec__AnonStoreyB__ctor_m2503623732 (U3CdownloadElementU3Ec__AnonStoreyB_t3800062637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
