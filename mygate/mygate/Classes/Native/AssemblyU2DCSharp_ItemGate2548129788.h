﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// SimpleJSON.JSONArray
struct JSONArray_t3986483147;
// ItemType
struct ItemType_t3455197591;
// ItemStyle
struct ItemStyle_t3530326308;
// ItemGeometry
struct ItemGeometry_t2838964733;
// ItemMisure
struct ItemMisure_t2657053000;
// ItemColor
struct ItemColor_t1117508970;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemGate
struct  ItemGate_t2548129788  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemGate::id
	int32_t ___id_2;
	// System.String ItemGate::name
	String_t* ___name_3;
	// UnityEngine.Sprite ItemGate::image
	Sprite_t309593783 * ___image_4;
	// System.String ItemGate::image_path
	String_t* ___image_path_5;
	// System.String ItemGate::date
	String_t* ___date_6;
	// System.Int32 ItemGate::id_type
	int32_t ___id_type_7;
	// System.Int32 ItemGate::id_style
	int32_t ___id_style_8;
	// System.Int32 ItemGate::id_geometry
	int32_t ___id_geometry_9;
	// System.Int32 ItemGate::width
	int32_t ___width_10;
	// System.Int32 ItemGate::height
	int32_t ___height_11;
	// System.Int32 ItemGate::heightmax
	int32_t ___heightmax_12;
	// System.String ItemGate::opening
	String_t* ___opening_13;
	// SimpleJSON.JSONArray ItemGate::configuration
	JSONArray_t3986483147 * ___configuration_14;
	// ItemType ItemGate::type
	ItemType_t3455197591 * ___type_15;
	// ItemStyle ItemGate::style
	ItemStyle_t3530326308 * ___style_16;
	// ItemGeometry ItemGate::geometry
	ItemGeometry_t2838964733 * ___geometry_17;
	// ItemMisure ItemGate::misure
	ItemMisure_t2657053000 * ___misure_18;
	// UnityEngine.Vector3 ItemGate::gateDim
	Vector3_t2243707580  ___gateDim_19;
	// UnityEngine.Vector3 ItemGate::gateMaxDim
	Vector3_t2243707580  ___gateMaxDim_20;
	// UnityEngine.Vector3 ItemGate::centralPoint
	Vector3_t2243707580  ___centralPoint_21;
	// ItemColor ItemGate::mat_frame
	ItemColor_t1117508970 * ___mat_frame_22;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___image_4)); }
	inline Sprite_t309593783 * get_image_4() const { return ___image_4; }
	inline Sprite_t309593783 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Sprite_t309593783 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_image_path_5() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___image_path_5)); }
	inline String_t* get_image_path_5() const { return ___image_path_5; }
	inline String_t** get_address_of_image_path_5() { return &___image_path_5; }
	inline void set_image_path_5(String_t* value)
	{
		___image_path_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_5, value);
	}

	inline static int32_t get_offset_of_date_6() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___date_6)); }
	inline String_t* get_date_6() const { return ___date_6; }
	inline String_t** get_address_of_date_6() { return &___date_6; }
	inline void set_date_6(String_t* value)
	{
		___date_6 = value;
		Il2CppCodeGenWriteBarrier(&___date_6, value);
	}

	inline static int32_t get_offset_of_id_type_7() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___id_type_7)); }
	inline int32_t get_id_type_7() const { return ___id_type_7; }
	inline int32_t* get_address_of_id_type_7() { return &___id_type_7; }
	inline void set_id_type_7(int32_t value)
	{
		___id_type_7 = value;
	}

	inline static int32_t get_offset_of_id_style_8() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___id_style_8)); }
	inline int32_t get_id_style_8() const { return ___id_style_8; }
	inline int32_t* get_address_of_id_style_8() { return &___id_style_8; }
	inline void set_id_style_8(int32_t value)
	{
		___id_style_8 = value;
	}

	inline static int32_t get_offset_of_id_geometry_9() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___id_geometry_9)); }
	inline int32_t get_id_geometry_9() const { return ___id_geometry_9; }
	inline int32_t* get_address_of_id_geometry_9() { return &___id_geometry_9; }
	inline void set_id_geometry_9(int32_t value)
	{
		___id_geometry_9 = value;
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___width_10)); }
	inline int32_t get_width_10() const { return ___width_10; }
	inline int32_t* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(int32_t value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___height_11)); }
	inline int32_t get_height_11() const { return ___height_11; }
	inline int32_t* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(int32_t value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_heightmax_12() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___heightmax_12)); }
	inline int32_t get_heightmax_12() const { return ___heightmax_12; }
	inline int32_t* get_address_of_heightmax_12() { return &___heightmax_12; }
	inline void set_heightmax_12(int32_t value)
	{
		___heightmax_12 = value;
	}

	inline static int32_t get_offset_of_opening_13() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___opening_13)); }
	inline String_t* get_opening_13() const { return ___opening_13; }
	inline String_t** get_address_of_opening_13() { return &___opening_13; }
	inline void set_opening_13(String_t* value)
	{
		___opening_13 = value;
		Il2CppCodeGenWriteBarrier(&___opening_13, value);
	}

	inline static int32_t get_offset_of_configuration_14() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___configuration_14)); }
	inline JSONArray_t3986483147 * get_configuration_14() const { return ___configuration_14; }
	inline JSONArray_t3986483147 ** get_address_of_configuration_14() { return &___configuration_14; }
	inline void set_configuration_14(JSONArray_t3986483147 * value)
	{
		___configuration_14 = value;
		Il2CppCodeGenWriteBarrier(&___configuration_14, value);
	}

	inline static int32_t get_offset_of_type_15() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___type_15)); }
	inline ItemType_t3455197591 * get_type_15() const { return ___type_15; }
	inline ItemType_t3455197591 ** get_address_of_type_15() { return &___type_15; }
	inline void set_type_15(ItemType_t3455197591 * value)
	{
		___type_15 = value;
		Il2CppCodeGenWriteBarrier(&___type_15, value);
	}

	inline static int32_t get_offset_of_style_16() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___style_16)); }
	inline ItemStyle_t3530326308 * get_style_16() const { return ___style_16; }
	inline ItemStyle_t3530326308 ** get_address_of_style_16() { return &___style_16; }
	inline void set_style_16(ItemStyle_t3530326308 * value)
	{
		___style_16 = value;
		Il2CppCodeGenWriteBarrier(&___style_16, value);
	}

	inline static int32_t get_offset_of_geometry_17() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___geometry_17)); }
	inline ItemGeometry_t2838964733 * get_geometry_17() const { return ___geometry_17; }
	inline ItemGeometry_t2838964733 ** get_address_of_geometry_17() { return &___geometry_17; }
	inline void set_geometry_17(ItemGeometry_t2838964733 * value)
	{
		___geometry_17 = value;
		Il2CppCodeGenWriteBarrier(&___geometry_17, value);
	}

	inline static int32_t get_offset_of_misure_18() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___misure_18)); }
	inline ItemMisure_t2657053000 * get_misure_18() const { return ___misure_18; }
	inline ItemMisure_t2657053000 ** get_address_of_misure_18() { return &___misure_18; }
	inline void set_misure_18(ItemMisure_t2657053000 * value)
	{
		___misure_18 = value;
		Il2CppCodeGenWriteBarrier(&___misure_18, value);
	}

	inline static int32_t get_offset_of_gateDim_19() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___gateDim_19)); }
	inline Vector3_t2243707580  get_gateDim_19() const { return ___gateDim_19; }
	inline Vector3_t2243707580 * get_address_of_gateDim_19() { return &___gateDim_19; }
	inline void set_gateDim_19(Vector3_t2243707580  value)
	{
		___gateDim_19 = value;
	}

	inline static int32_t get_offset_of_gateMaxDim_20() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___gateMaxDim_20)); }
	inline Vector3_t2243707580  get_gateMaxDim_20() const { return ___gateMaxDim_20; }
	inline Vector3_t2243707580 * get_address_of_gateMaxDim_20() { return &___gateMaxDim_20; }
	inline void set_gateMaxDim_20(Vector3_t2243707580  value)
	{
		___gateMaxDim_20 = value;
	}

	inline static int32_t get_offset_of_centralPoint_21() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___centralPoint_21)); }
	inline Vector3_t2243707580  get_centralPoint_21() const { return ___centralPoint_21; }
	inline Vector3_t2243707580 * get_address_of_centralPoint_21() { return &___centralPoint_21; }
	inline void set_centralPoint_21(Vector3_t2243707580  value)
	{
		___centralPoint_21 = value;
	}

	inline static int32_t get_offset_of_mat_frame_22() { return static_cast<int32_t>(offsetof(ItemGate_t2548129788, ___mat_frame_22)); }
	inline ItemColor_t1117508970 * get_mat_frame_22() const { return ___mat_frame_22; }
	inline ItemColor_t1117508970 ** get_address_of_mat_frame_22() { return &___mat_frame_22; }
	inline void set_mat_frame_22(ItemColor_t1117508970 * value)
	{
		___mat_frame_22 = value;
		Il2CppCodeGenWriteBarrier(&___mat_frame_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
