﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BranchingSpline/BranchingController
struct BranchingController_t3837965438;
// System.Object
struct Il2CppObject;
// BranchingSplinePath
struct BranchingSplinePath_t1574214842;
// BranchingSplineParameter
struct BranchingSplineParameter_t1831757700;
// System.Collections.Generic.List`1<BranchingSplinePath>
struct List_1_t943335974;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_BranchingSplineParameter1831757700.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void BranchingSpline/BranchingController::.ctor(System.Object,System.IntPtr)
extern "C"  void BranchingController__ctor_m252919681 (BranchingController_t3837965438 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BranchingSplinePath BranchingSpline/BranchingController::Invoke(BranchingSplineParameter,System.Collections.Generic.List`1<BranchingSplinePath>)
extern "C"  BranchingSplinePath_t1574214842 * BranchingController_Invoke_m4024889080 (BranchingController_t3837965438 * __this, BranchingSplineParameter_t1831757700 * ___currentParameter0, List_1_t943335974 * ___possiblePaths1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BranchingSpline/BranchingController::BeginInvoke(BranchingSplineParameter,System.Collections.Generic.List`1<BranchingSplinePath>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BranchingController_BeginInvoke_m1431005242 (BranchingController_t3837965438 * __this, BranchingSplineParameter_t1831757700 * ___currentParameter0, List_1_t943335974 * ___possiblePaths1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BranchingSplinePath BranchingSpline/BranchingController::EndInvoke(System.IAsyncResult)
extern "C"  BranchingSplinePath_t1574214842 * BranchingController_EndInvoke_m2797913060 (BranchingController_t3837965438 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
