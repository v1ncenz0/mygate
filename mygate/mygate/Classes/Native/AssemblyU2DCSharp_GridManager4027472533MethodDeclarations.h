﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GridManager
struct GridManager_t4027472533;
// ItemGate
struct ItemGate_t2548129788;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t193706927;
// Item
struct Item_t2440468191;
// gridLayer
struct gridLayer_t4027367017;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemGate2548129788.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_gridLayer4027367017.h"

// System.Void GridManager::.ctor()
extern "C"  void GridManager__ctor_m2435300606 (GridManager_t4027472533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::Init(ItemGate)
extern "C"  void GridManager_Init_m2052463946 (GridManager_t4027472533 * __this, ItemGate_t2548129788 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::addGrids(UnityEngine.GameObject)
extern "C"  void GridManager_addGrids_m915925870 (GridManager_t4027472533 * __this, GameObject_t1756533147 * ___division0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GridManager::addDivision()
extern "C"  GameObject_t1756533147 * GridManager_addDivision_m2614171975 (GridManager_t4027472533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GridManager::makeFrameBar(UnityEngine.Vector3,UnityEngine.Vector3,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  GameObject_t1756533147 * GridManager_makeFrameBar_m3822825596 (GridManager_t4027472533 * __this, Vector3_t2243707580  ___dim0, Vector3_t2243707580  ___pos1, String_t* ___name2, int32_t ___larghezzaSezione3, int32_t ___altezzaSezione4, int32_t ___lunghezza5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::makeFrame()
extern "C"  void GridManager_makeFrame_m3393706387 (GridManager_t4027472533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GridManager::makeJoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  GameObject_t1756533147 * GridManager_makeJoint_m2387715439 (GridManager_t4027472533 * __this, Vector3_t2243707580  ___dim0, Vector3_t2243707580  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GridManager::makeKnocker(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  GameObject_t1756533147 * GridManager_makeKnocker_m2687416168 (GridManager_t4027472533 * __this, Vector3_t2243707580  ___dim0, Vector3_t2243707580  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::makeHat(UnityEngine.GameObject,UnityEngine.Material)
extern "C"  void GridManager_makeHat_m50856619 (GridManager_t4027472533 * __this, GameObject_t1756533147 * ___parent0, Material_t193706927 * ___mat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::makeCompensation()
extern "C"  void GridManager_makeCompensation_m1275448892 (GridManager_t4027472533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GridManager::setCameraToSeeGate(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  GridManager_setCameraToSeeGate_m623705223 (GridManager_t4027472533 * __this, Vector3_t2243707580  ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GridManager::addCellToGrid(UnityEngine.Vector3,Item,gridLayer,System.Boolean)
extern "C"  void GridManager_addCellToGrid_m4161830442 (GridManager_t4027472533 * __this, Vector3_t2243707580  ___pos0, Item_t2440468191 * ___item1, gridLayer_t4027367017 * ___layer2, bool ___specular3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
