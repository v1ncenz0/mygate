﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjExporterScript
struct  ObjExporterScript_t792682685  : public Il2CppObject
{
public:

public:
};

struct ObjExporterScript_t792682685_StaticFields
{
public:
	// System.Int32 ObjExporterScript::StartIndex
	int32_t ___StartIndex_0;
	// System.Collections.ArrayList ObjExporterScript::materialsname
	ArrayList_t4252133567 * ___materialsname_1;
	// System.Collections.ArrayList ObjExporterScript::materialsncolor
	ArrayList_t4252133567 * ___materialsncolor_2;
	// System.Int32 ObjExporterScript::totalVertex
	int32_t ___totalVertex_3;

public:
	inline static int32_t get_offset_of_StartIndex_0() { return static_cast<int32_t>(offsetof(ObjExporterScript_t792682685_StaticFields, ___StartIndex_0)); }
	inline int32_t get_StartIndex_0() const { return ___StartIndex_0; }
	inline int32_t* get_address_of_StartIndex_0() { return &___StartIndex_0; }
	inline void set_StartIndex_0(int32_t value)
	{
		___StartIndex_0 = value;
	}

	inline static int32_t get_offset_of_materialsname_1() { return static_cast<int32_t>(offsetof(ObjExporterScript_t792682685_StaticFields, ___materialsname_1)); }
	inline ArrayList_t4252133567 * get_materialsname_1() const { return ___materialsname_1; }
	inline ArrayList_t4252133567 ** get_address_of_materialsname_1() { return &___materialsname_1; }
	inline void set_materialsname_1(ArrayList_t4252133567 * value)
	{
		___materialsname_1 = value;
		Il2CppCodeGenWriteBarrier(&___materialsname_1, value);
	}

	inline static int32_t get_offset_of_materialsncolor_2() { return static_cast<int32_t>(offsetof(ObjExporterScript_t792682685_StaticFields, ___materialsncolor_2)); }
	inline ArrayList_t4252133567 * get_materialsncolor_2() const { return ___materialsncolor_2; }
	inline ArrayList_t4252133567 ** get_address_of_materialsncolor_2() { return &___materialsncolor_2; }
	inline void set_materialsncolor_2(ArrayList_t4252133567 * value)
	{
		___materialsncolor_2 = value;
		Il2CppCodeGenWriteBarrier(&___materialsncolor_2, value);
	}

	inline static int32_t get_offset_of_totalVertex_3() { return static_cast<int32_t>(offsetof(ObjExporterScript_t792682685_StaticFields, ___totalVertex_3)); }
	inline int32_t get_totalVertex_3() const { return ___totalVertex_3; }
	inline int32_t* get_address_of_totalVertex_3() { return &___totalVertex_3; }
	inline void set_totalVertex_3(int32_t value)
	{
		___totalVertex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
