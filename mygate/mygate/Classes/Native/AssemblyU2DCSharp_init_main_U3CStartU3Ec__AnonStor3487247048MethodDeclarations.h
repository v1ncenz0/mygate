﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_main/<Start>c__AnonStorey0
struct U3CStartU3Ec__AnonStorey0_t3487247048;
// ItemGate
struct ItemGate_t2548129788;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemGate2548129788.h"
#include "mscorlib_System_String2029220233.h"

// System.Void init_main/<Start>c__AnonStorey0::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey0__ctor_m1421659439 (U3CStartU3Ec__AnonStorey0_t3487247048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_main/<Start>c__AnonStorey0::<>m__0(System.Boolean)
extern "C"  void U3CStartU3Ec__AnonStorey0_U3CU3Em__0_m1131016039 (U3CStartU3Ec__AnonStorey0_t3487247048 * __this, bool ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_main/<Start>c__AnonStorey0::<>m__1(ItemGate)
extern "C"  void U3CStartU3Ec__AnonStorey0_U3CU3Em__1_m2954744991 (U3CStartU3Ec__AnonStorey0_t3487247048 * __this, ItemGate_t2548129788 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void init_main/<Start>c__AnonStorey0::<>m__2(System.String)
extern "C"  void U3CStartU3Ec__AnonStorey0_U3CU3Em__2_m656594870 (Il2CppObject * __this /* static, unused */, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
