﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<DownloadScenes>c__Iterator4
struct U3CDownloadScenesU3Ec__Iterator4_t1121704572;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<DownloadScenes>c__Iterator4::.ctor()
extern "C"  void U3CDownloadScenesU3Ec__Iterator4__ctor_m3341367395 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<DownloadScenes>c__Iterator4::MoveNext()
extern "C"  bool U3CDownloadScenesU3Ec__Iterator4_MoveNext_m578012085 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadScenes>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadScenesU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3626803789 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<DownloadScenes>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadScenesU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1374668965 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadScenes>c__Iterator4::Dispose()
extern "C"  void U3CDownloadScenesU3Ec__Iterator4_Dispose_m4281322782 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<DownloadScenes>c__Iterator4::Reset()
extern "C"  void U3CDownloadScenesU3Ec__Iterator4_Reset_m2979189920 (U3CDownloadScenesU3Ec__Iterator4_t1121704572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
