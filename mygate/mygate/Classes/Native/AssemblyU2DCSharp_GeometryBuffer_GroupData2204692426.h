﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<GeometryBuffer/FaceIndicesRow>
struct List_1_t2469041691;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeometryBuffer/GroupData
struct  GroupData_t2204692426  : public Il2CppObject
{
public:
	// System.String GeometryBuffer/GroupData::name
	String_t* ___name_0;
	// System.String GeometryBuffer/GroupData::materialName
	String_t* ___materialName_1;
	// System.Collections.Generic.List`1<GeometryBuffer/FaceIndicesRow> GeometryBuffer/GroupData::faces
	List_1_t2469041691 * ___faces_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GroupData_t2204692426, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_materialName_1() { return static_cast<int32_t>(offsetof(GroupData_t2204692426, ___materialName_1)); }
	inline String_t* get_materialName_1() const { return ___materialName_1; }
	inline String_t** get_address_of_materialName_1() { return &___materialName_1; }
	inline void set_materialName_1(String_t* value)
	{
		___materialName_1 = value;
		Il2CppCodeGenWriteBarrier(&___materialName_1, value);
	}

	inline static int32_t get_offset_of_faces_2() { return static_cast<int32_t>(offsetof(GroupData_t2204692426, ___faces_2)); }
	inline List_1_t2469041691 * get_faces_2() const { return ___faces_2; }
	inline List_1_t2469041691 ** get_address_of_faces_2() { return &___faces_2; }
	inline void set_faces_2(List_1_t2469041691 * value)
	{
		___faces_2 = value;
		Il2CppCodeGenWriteBarrier(&___faces_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
