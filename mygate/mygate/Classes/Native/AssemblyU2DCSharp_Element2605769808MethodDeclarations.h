﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Element
struct Element_t2605769808;

#include "codegen/il2cpp-codegen.h"

// System.Void Element::.ctor()
extern "C"  void Element__ctor_m3274598753 (Element_t2605769808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Element::OnMouseOver()
extern "C"  void Element_OnMouseOver_m4223792551 (Element_t2605769808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Element::removeSpecular()
extern "C"  void Element_removeSpecular_m1271295498 (Element_t2605769808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Element::Update()
extern "C"  void Element_Update_m2358624190 (Element_t2605769808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
