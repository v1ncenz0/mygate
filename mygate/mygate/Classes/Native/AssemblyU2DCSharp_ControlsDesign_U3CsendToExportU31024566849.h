﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// OBJItem
struct OBJItem_t2734286650;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// ControlsDesign
struct ControlsDesign_t1600104368;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<sendToExport>c__Iterator2
struct  U3CsendToExportU3Ec__Iterator2_t1024566849  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm ControlsDesign/<sendToExport>c__Iterator2::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// OBJItem ControlsDesign/<sendToExport>c__Iterator2::objitem
	OBJItem_t2734286650 * ___objitem_1;
	// System.Byte[] ControlsDesign/<sendToExport>c__Iterator2::<obj_byte>__1
	ByteU5BU5D_t3397334013* ___U3Cobj_byteU3E__1_2;
	// System.Byte[] ControlsDesign/<sendToExport>c__Iterator2::<rawData>__2
	ByteU5BU5D_t3397334013* ___U3CrawDataU3E__2_3;
	// System.String ControlsDesign/<sendToExport>c__Iterator2::<url>__3
	String_t* ___U3CurlU3E__3_4;
	// UnityEngine.WWW ControlsDesign/<sendToExport>c__Iterator2::<www>__4
	WWW_t2919945039 * ___U3CwwwU3E__4_5;
	// ControlsDesign ControlsDesign/<sendToExport>c__Iterator2::$this
	ControlsDesign_t1600104368 * ___U24this_6;
	// System.Object ControlsDesign/<sendToExport>c__Iterator2::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean ControlsDesign/<sendToExport>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 ControlsDesign/<sendToExport>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_objitem_1() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___objitem_1)); }
	inline OBJItem_t2734286650 * get_objitem_1() const { return ___objitem_1; }
	inline OBJItem_t2734286650 ** get_address_of_objitem_1() { return &___objitem_1; }
	inline void set_objitem_1(OBJItem_t2734286650 * value)
	{
		___objitem_1 = value;
		Il2CppCodeGenWriteBarrier(&___objitem_1, value);
	}

	inline static int32_t get_offset_of_U3Cobj_byteU3E__1_2() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U3Cobj_byteU3E__1_2)); }
	inline ByteU5BU5D_t3397334013* get_U3Cobj_byteU3E__1_2() const { return ___U3Cobj_byteU3E__1_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3Cobj_byteU3E__1_2() { return &___U3Cobj_byteU3E__1_2; }
	inline void set_U3Cobj_byteU3E__1_2(ByteU5BU5D_t3397334013* value)
	{
		___U3Cobj_byteU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cobj_byteU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CrawDataU3E__2_3() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U3CrawDataU3E__2_3)); }
	inline ByteU5BU5D_t3397334013* get_U3CrawDataU3E__2_3() const { return ___U3CrawDataU3E__2_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CrawDataU3E__2_3() { return &___U3CrawDataU3E__2_3; }
	inline void set_U3CrawDataU3E__2_3(ByteU5BU5D_t3397334013* value)
	{
		___U3CrawDataU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrawDataU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__3_4() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U3CurlU3E__3_4)); }
	inline String_t* get_U3CurlU3E__3_4() const { return ___U3CurlU3E__3_4; }
	inline String_t** get_address_of_U3CurlU3E__3_4() { return &___U3CurlU3E__3_4; }
	inline void set_U3CurlU3E__3_4(String_t* value)
	{
		___U3CurlU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_5() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U3CwwwU3E__4_5)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__4_5() const { return ___U3CwwwU3E__4_5; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__4_5() { return &___U3CwwwU3E__4_5; }
	inline void set_U3CwwwU3E__4_5(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U24this_6)); }
	inline ControlsDesign_t1600104368 * get_U24this_6() const { return ___U24this_6; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ControlsDesign_t1600104368 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CsendToExportU3Ec__Iterator2_t1024566849, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
