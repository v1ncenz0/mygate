﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineTwistModifier
struct SplineTwistModifier_t3543911261;
// SplineMesh
struct SplineMesh_t1719246168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_SplineMesh1719246168.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// System.Void SplineTwistModifier::.ctor()
extern "C"  void SplineTwistModifier__ctor_m1688409964 (SplineTwistModifier_t3543911261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineTwistModifier::ModifyVertex(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineTwistModifier_ModifyVertex_m227160152 (SplineTwistModifier_t3543911261 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___vertex1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SplineTwistModifier::ModifyUV(SplineMesh,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  SplineTwistModifier_ModifyUV_m726366147 (SplineTwistModifier_t3543911261 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector2_t2243707579  ___uvCoord1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineTwistModifier::ModifyNormal(SplineMesh,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SplineTwistModifier_ModifyNormal_m1960520895 (SplineTwistModifier_t3543911261 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector3_t2243707580  ___normal1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 SplineTwistModifier::ModifyTangent(SplineMesh,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  SplineTwistModifier_ModifyTangent_m1741823827 (SplineTwistModifier_t3543911261 * __this, SplineMesh_t1719246168 * ___splineMesh0, Vector4_t2243707581  ___tangent1, float ___splineParam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
