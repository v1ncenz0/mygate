﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBJ/BumpParamDef
struct BumpParamDef_t684364218;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void OBJ/BumpParamDef::.ctor(System.String,System.String,System.Int32,System.Int32)
extern "C"  void BumpParamDef__ctor_m3936092137 (BumpParamDef_t684364218 * __this, String_t* ___name0, String_t* ___type1, int32_t ___numMin2, int32_t ___numMax3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
