﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_show_layer
struct bt_show_layer_t441805848;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_show_layer::.ctor()
extern "C"  void bt_show_layer__ctor_m793523601 (bt_show_layer_t441805848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_show_layer::invertLayer()
extern "C"  void bt_show_layer_invertLayer_m3888668858 (bt_show_layer_t441805848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_show_layer::invertFrame()
extern "C"  void bt_show_layer_invertFrame_m671308274 (bt_show_layer_t441805848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
