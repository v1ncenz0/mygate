﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineAnimatorClosestPoint
struct  SplineAnimatorClosestPoint_t4003263689  : public MonoBehaviour_t1158329972
{
public:
	// Spline SplineAnimatorClosestPoint::spline
	Spline_t1260612603 * ___spline_2;
	// UnityEngine.WrapMode SplineAnimatorClosestPoint::wMode
	int32_t ___wMode_3;
	// UnityEngine.Transform SplineAnimatorClosestPoint::target
	Transform_t3275118058 * ___target_4;
	// System.Int32 SplineAnimatorClosestPoint::iterations
	int32_t ___iterations_5;
	// System.Single SplineAnimatorClosestPoint::diff
	float ___diff_6;
	// System.Single SplineAnimatorClosestPoint::offset
	float ___offset_7;

public:
	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}

	inline static int32_t get_offset_of_wMode_3() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___wMode_3)); }
	inline int32_t get_wMode_3() const { return ___wMode_3; }
	inline int32_t* get_address_of_wMode_3() { return &___wMode_3; }
	inline void set_wMode_3(int32_t value)
	{
		___wMode_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___target_4)); }
	inline Transform_t3275118058 * get_target_4() const { return ___target_4; }
	inline Transform_t3275118058 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3275118058 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier(&___target_4, value);
	}

	inline static int32_t get_offset_of_iterations_5() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___iterations_5)); }
	inline int32_t get_iterations_5() const { return ___iterations_5; }
	inline int32_t* get_address_of_iterations_5() { return &___iterations_5; }
	inline void set_iterations_5(int32_t value)
	{
		___iterations_5 = value;
	}

	inline static int32_t get_offset_of_diff_6() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___diff_6)); }
	inline float get_diff_6() const { return ___diff_6; }
	inline float* get_address_of_diff_6() { return &___diff_6; }
	inline void set_diff_6(float value)
	{
		___diff_6 = value;
	}

	inline static int32_t get_offset_of_offset_7() { return static_cast<int32_t>(offsetof(SplineAnimatorClosestPoint_t4003263689, ___offset_7)); }
	inline float get_offset_7() const { return ___offset_7; }
	inline float* get_address_of_offset_7() { return &___offset_7; }
	inline void set_offset_7(float value)
	{
		___offset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
