﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>
struct ReadOnlyCollection_1_t360741106;
// System.Collections.Generic.IList`1<FaceIndices>
struct IList_1_t715896015;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FaceIndices[]
struct FaceIndicesU5BU5D_t2696217939;
// System.Collections.Generic.IEnumerator`1<FaceIndices>
struct IEnumerator_1_t1945446537;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2057871682_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2057871682(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2057871682_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m988278226_gshared (ReadOnlyCollection_1_t360741106 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m988278226(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m988278226_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4191747414_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4191747414(__this, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4191747414_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1116397995_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, FaceIndices_t174955414  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1116397995(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1116397995_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4178549427_gshared (ReadOnlyCollection_1_t360741106 * __this, FaceIndices_t174955414  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4178549427(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4178549427_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3767401495_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3767401495(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3767401495_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  FaceIndices_t174955414  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2597432199_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2597432199(__this, ___index0, method) ((  FaceIndices_t174955414  (*) (ReadOnlyCollection_1_t360741106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2597432199_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m824694512_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, FaceIndices_t174955414  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m824694512(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m824694512_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3023564504_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3023564504(__this, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3023564504_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2839127917_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2839127917(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2839127917_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m10710860_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m10710860(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m10710860_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1259157041_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1259157041(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t360741106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1259157041_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1478419133_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1478419133(__this, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1478419133_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1899119261_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1899119261(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1899119261_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2771801303_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2771801303(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t360741106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2771801303_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1079945826_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1079945826(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1079945826_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1593692144_gshared (ReadOnlyCollection_1_t360741106 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1593692144(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1593692144_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1827961808_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1827961808(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1827961808_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1046798361_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1046798361(__this, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1046798361_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1981829469_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1981829469(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1981829469_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2269886194_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2269886194(__this, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2269886194_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m41745605_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m41745605(__this, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m41745605_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m804159702_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m804159702(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t360741106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m804159702_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1463760495_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1463760495(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1463760495_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3985818992_gshared (ReadOnlyCollection_1_t360741106 * __this, FaceIndices_t174955414  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3985818992(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t360741106 *, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3985818992_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3730480766_gshared (ReadOnlyCollection_1_t360741106 * __this, FaceIndicesU5BU5D_t2696217939* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3730480766(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t360741106 *, FaceIndicesU5BU5D_t2696217939*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3730480766_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3086075297_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3086075297(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3086075297_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1369583660_gshared (ReadOnlyCollection_1_t360741106 * __this, FaceIndices_t174955414  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1369583660(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t360741106 *, FaceIndices_t174955414 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1369583660_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m859422081_gshared (ReadOnlyCollection_1_t360741106 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m859422081(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t360741106 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m859422081_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FaceIndices>::get_Item(System.Int32)
extern "C"  FaceIndices_t174955414  ReadOnlyCollection_1_get_Item_m820998243_gshared (ReadOnlyCollection_1_t360741106 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m820998243(__this, ___index0, method) ((  FaceIndices_t174955414  (*) (ReadOnlyCollection_1_t360741106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m820998243_gshared)(__this, ___index0, method)
