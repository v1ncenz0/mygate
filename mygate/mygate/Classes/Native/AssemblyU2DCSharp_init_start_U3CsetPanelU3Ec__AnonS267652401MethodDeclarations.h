﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// init_start/<setPanel>c__AnonStorey1`1<System.Object>
struct U3CsetPanelU3Ec__AnonStorey1_1_t267652401;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"

// System.Void init_start/<setPanel>c__AnonStorey1`1<System.Object>::.ctor()
extern "C"  void U3CsetPanelU3Ec__AnonStorey1_1__ctor_m3659554114_gshared (U3CsetPanelU3Ec__AnonStorey1_1_t267652401 * __this, const MethodInfo* method);
#define U3CsetPanelU3Ec__AnonStorey1_1__ctor_m3659554114(__this, method) ((  void (*) (U3CsetPanelU3Ec__AnonStorey1_1_t267652401 *, const MethodInfo*))U3CsetPanelU3Ec__AnonStorey1_1__ctor_m3659554114_gshared)(__this, method)
// System.Void init_start/<setPanel>c__AnonStorey1`1<System.Object>::<>m__0(UnityEngine.Sprite)
extern "C"  void U3CsetPanelU3Ec__AnonStorey1_1_U3CU3Em__0_m3907243633_gshared (U3CsetPanelU3Ec__AnonStorey1_1_t267652401 * __this, Sprite_t309593783 * ___img_downloaded0, const MethodInfo* method);
#define U3CsetPanelU3Ec__AnonStorey1_1_U3CU3Em__0_m3907243633(__this, ___img_downloaded0, method) ((  void (*) (U3CsetPanelU3Ec__AnonStorey1_1_t267652401 *, Sprite_t309593783 *, const MethodInfo*))U3CsetPanelU3Ec__AnonStorey1_1_U3CU3Em__0_m3907243633_gshared)(__this, ___img_downloaded0, method)
