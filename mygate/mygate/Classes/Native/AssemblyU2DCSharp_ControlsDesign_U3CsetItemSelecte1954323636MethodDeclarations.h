﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<setItemSelected>c__AnonStorey4
struct U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ControlsDesign/<setItemSelected>c__AnonStorey4::.ctor()
extern "C"  void U3CsetItemSelectedU3Ec__AnonStorey4__ctor_m2698913929 (U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<setItemSelected>c__AnonStorey4::<>m__0(UnityEngine.GameObject)
extern "C"  void U3CsetItemSelectedU3Ec__AnonStorey4_U3CU3Em__0_m925508280 (U3CsetItemSelectedU3Ec__AnonStorey4_t1954323636 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
