﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_opening
struct bt_opening_t174438555;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_opening::.ctor()
extern "C"  void bt_opening__ctor_m2202488184 (bt_opening_t174438555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_opening::changeOpening()
extern "C"  void bt_opening_changeOpening_m2306656536 (bt_opening_t174438555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
