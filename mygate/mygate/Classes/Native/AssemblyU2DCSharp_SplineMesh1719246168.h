﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// SplineMesh/MeshData
struct MeshData_t1171048948;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SplineMesh_UpdateMode201935517.h"
#include "AssemblyU2DCSharp_SplineMesh_UVMode511492427.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_SplineMesh_SplitMode1084303880.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineMesh
struct  SplineMesh_t1719246168  : public MonoBehaviour_t1158329972
{
public:
	// Spline SplineMesh::spline
	Spline_t1260612603 * ___spline_2;
	// SplineMesh/UpdateMode SplineMesh::updateMode
	int32_t ___updateMode_3;
	// System.Int32 SplineMesh::deltaFrames
	int32_t ___deltaFrames_4;
	// System.Single SplineMesh::deltaTime
	float ___deltaTime_5;
	// System.Int32 SplineMesh::updateFrame
	int32_t ___updateFrame_6;
	// System.Single SplineMesh::updateTime
	float ___updateTime_7;
	// UnityEngine.Mesh SplineMesh::startBaseMesh
	Mesh_t1356156583 * ___startBaseMesh_8;
	// UnityEngine.Mesh SplineMesh::baseMesh
	Mesh_t1356156583 * ___baseMesh_9;
	// UnityEngine.Mesh SplineMesh::endBaseMesh
	Mesh_t1356156583 * ___endBaseMesh_10;
	// System.Int32 SplineMesh::segmentCount
	int32_t ___segmentCount_11;
	// SplineMesh/UVMode SplineMesh::uvMode
	int32_t ___uvMode_12;
	// UnityEngine.Vector2 SplineMesh::uvScale
	Vector2_t2243707579  ___uvScale_13;
	// UnityEngine.Vector2 SplineMesh::xyScale
	Vector2_t2243707579  ___xyScale_14;
	// System.Boolean SplineMesh::highAccuracy
	bool ___highAccuracy_15;
	// SplineMesh/SplitMode SplineMesh::splitMode
	int32_t ___splitMode_16;
	// System.Single SplineMesh::segmentStart
	float ___segmentStart_17;
	// System.Single SplineMesh::segmentEnd
	float ___segmentEnd_18;
	// System.Int32 SplineMesh::splineSegment
	int32_t ___splineSegment_19;
	// SplineMesh/MeshData SplineMesh::meshDataStart
	MeshData_t1171048948 * ___meshDataStart_20;
	// SplineMesh/MeshData SplineMesh::meshDataBase
	MeshData_t1171048948 * ___meshDataBase_21;
	// SplineMesh/MeshData SplineMesh::meshDataEnd
	MeshData_t1171048948 * ___meshDataEnd_22;
	// SplineMesh/MeshData SplineMesh::meshDataNew
	MeshData_t1171048948 * ___meshDataNew_23;
	// UnityEngine.Mesh SplineMesh::bentMesh
	Mesh_t1356156583 * ___bentMesh_24;

public:
	inline static int32_t get_offset_of_spline_2() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___spline_2)); }
	inline Spline_t1260612603 * get_spline_2() const { return ___spline_2; }
	inline Spline_t1260612603 ** get_address_of_spline_2() { return &___spline_2; }
	inline void set_spline_2(Spline_t1260612603 * value)
	{
		___spline_2 = value;
		Il2CppCodeGenWriteBarrier(&___spline_2, value);
	}

	inline static int32_t get_offset_of_updateMode_3() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___updateMode_3)); }
	inline int32_t get_updateMode_3() const { return ___updateMode_3; }
	inline int32_t* get_address_of_updateMode_3() { return &___updateMode_3; }
	inline void set_updateMode_3(int32_t value)
	{
		___updateMode_3 = value;
	}

	inline static int32_t get_offset_of_deltaFrames_4() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___deltaFrames_4)); }
	inline int32_t get_deltaFrames_4() const { return ___deltaFrames_4; }
	inline int32_t* get_address_of_deltaFrames_4() { return &___deltaFrames_4; }
	inline void set_deltaFrames_4(int32_t value)
	{
		___deltaFrames_4 = value;
	}

	inline static int32_t get_offset_of_deltaTime_5() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___deltaTime_5)); }
	inline float get_deltaTime_5() const { return ___deltaTime_5; }
	inline float* get_address_of_deltaTime_5() { return &___deltaTime_5; }
	inline void set_deltaTime_5(float value)
	{
		___deltaTime_5 = value;
	}

	inline static int32_t get_offset_of_updateFrame_6() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___updateFrame_6)); }
	inline int32_t get_updateFrame_6() const { return ___updateFrame_6; }
	inline int32_t* get_address_of_updateFrame_6() { return &___updateFrame_6; }
	inline void set_updateFrame_6(int32_t value)
	{
		___updateFrame_6 = value;
	}

	inline static int32_t get_offset_of_updateTime_7() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___updateTime_7)); }
	inline float get_updateTime_7() const { return ___updateTime_7; }
	inline float* get_address_of_updateTime_7() { return &___updateTime_7; }
	inline void set_updateTime_7(float value)
	{
		___updateTime_7 = value;
	}

	inline static int32_t get_offset_of_startBaseMesh_8() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___startBaseMesh_8)); }
	inline Mesh_t1356156583 * get_startBaseMesh_8() const { return ___startBaseMesh_8; }
	inline Mesh_t1356156583 ** get_address_of_startBaseMesh_8() { return &___startBaseMesh_8; }
	inline void set_startBaseMesh_8(Mesh_t1356156583 * value)
	{
		___startBaseMesh_8 = value;
		Il2CppCodeGenWriteBarrier(&___startBaseMesh_8, value);
	}

	inline static int32_t get_offset_of_baseMesh_9() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___baseMesh_9)); }
	inline Mesh_t1356156583 * get_baseMesh_9() const { return ___baseMesh_9; }
	inline Mesh_t1356156583 ** get_address_of_baseMesh_9() { return &___baseMesh_9; }
	inline void set_baseMesh_9(Mesh_t1356156583 * value)
	{
		___baseMesh_9 = value;
		Il2CppCodeGenWriteBarrier(&___baseMesh_9, value);
	}

	inline static int32_t get_offset_of_endBaseMesh_10() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___endBaseMesh_10)); }
	inline Mesh_t1356156583 * get_endBaseMesh_10() const { return ___endBaseMesh_10; }
	inline Mesh_t1356156583 ** get_address_of_endBaseMesh_10() { return &___endBaseMesh_10; }
	inline void set_endBaseMesh_10(Mesh_t1356156583 * value)
	{
		___endBaseMesh_10 = value;
		Il2CppCodeGenWriteBarrier(&___endBaseMesh_10, value);
	}

	inline static int32_t get_offset_of_segmentCount_11() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___segmentCount_11)); }
	inline int32_t get_segmentCount_11() const { return ___segmentCount_11; }
	inline int32_t* get_address_of_segmentCount_11() { return &___segmentCount_11; }
	inline void set_segmentCount_11(int32_t value)
	{
		___segmentCount_11 = value;
	}

	inline static int32_t get_offset_of_uvMode_12() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___uvMode_12)); }
	inline int32_t get_uvMode_12() const { return ___uvMode_12; }
	inline int32_t* get_address_of_uvMode_12() { return &___uvMode_12; }
	inline void set_uvMode_12(int32_t value)
	{
		___uvMode_12 = value;
	}

	inline static int32_t get_offset_of_uvScale_13() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___uvScale_13)); }
	inline Vector2_t2243707579  get_uvScale_13() const { return ___uvScale_13; }
	inline Vector2_t2243707579 * get_address_of_uvScale_13() { return &___uvScale_13; }
	inline void set_uvScale_13(Vector2_t2243707579  value)
	{
		___uvScale_13 = value;
	}

	inline static int32_t get_offset_of_xyScale_14() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___xyScale_14)); }
	inline Vector2_t2243707579  get_xyScale_14() const { return ___xyScale_14; }
	inline Vector2_t2243707579 * get_address_of_xyScale_14() { return &___xyScale_14; }
	inline void set_xyScale_14(Vector2_t2243707579  value)
	{
		___xyScale_14 = value;
	}

	inline static int32_t get_offset_of_highAccuracy_15() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___highAccuracy_15)); }
	inline bool get_highAccuracy_15() const { return ___highAccuracy_15; }
	inline bool* get_address_of_highAccuracy_15() { return &___highAccuracy_15; }
	inline void set_highAccuracy_15(bool value)
	{
		___highAccuracy_15 = value;
	}

	inline static int32_t get_offset_of_splitMode_16() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___splitMode_16)); }
	inline int32_t get_splitMode_16() const { return ___splitMode_16; }
	inline int32_t* get_address_of_splitMode_16() { return &___splitMode_16; }
	inline void set_splitMode_16(int32_t value)
	{
		___splitMode_16 = value;
	}

	inline static int32_t get_offset_of_segmentStart_17() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___segmentStart_17)); }
	inline float get_segmentStart_17() const { return ___segmentStart_17; }
	inline float* get_address_of_segmentStart_17() { return &___segmentStart_17; }
	inline void set_segmentStart_17(float value)
	{
		___segmentStart_17 = value;
	}

	inline static int32_t get_offset_of_segmentEnd_18() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___segmentEnd_18)); }
	inline float get_segmentEnd_18() const { return ___segmentEnd_18; }
	inline float* get_address_of_segmentEnd_18() { return &___segmentEnd_18; }
	inline void set_segmentEnd_18(float value)
	{
		___segmentEnd_18 = value;
	}

	inline static int32_t get_offset_of_splineSegment_19() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___splineSegment_19)); }
	inline int32_t get_splineSegment_19() const { return ___splineSegment_19; }
	inline int32_t* get_address_of_splineSegment_19() { return &___splineSegment_19; }
	inline void set_splineSegment_19(int32_t value)
	{
		___splineSegment_19 = value;
	}

	inline static int32_t get_offset_of_meshDataStart_20() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___meshDataStart_20)); }
	inline MeshData_t1171048948 * get_meshDataStart_20() const { return ___meshDataStart_20; }
	inline MeshData_t1171048948 ** get_address_of_meshDataStart_20() { return &___meshDataStart_20; }
	inline void set_meshDataStart_20(MeshData_t1171048948 * value)
	{
		___meshDataStart_20 = value;
		Il2CppCodeGenWriteBarrier(&___meshDataStart_20, value);
	}

	inline static int32_t get_offset_of_meshDataBase_21() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___meshDataBase_21)); }
	inline MeshData_t1171048948 * get_meshDataBase_21() const { return ___meshDataBase_21; }
	inline MeshData_t1171048948 ** get_address_of_meshDataBase_21() { return &___meshDataBase_21; }
	inline void set_meshDataBase_21(MeshData_t1171048948 * value)
	{
		___meshDataBase_21 = value;
		Il2CppCodeGenWriteBarrier(&___meshDataBase_21, value);
	}

	inline static int32_t get_offset_of_meshDataEnd_22() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___meshDataEnd_22)); }
	inline MeshData_t1171048948 * get_meshDataEnd_22() const { return ___meshDataEnd_22; }
	inline MeshData_t1171048948 ** get_address_of_meshDataEnd_22() { return &___meshDataEnd_22; }
	inline void set_meshDataEnd_22(MeshData_t1171048948 * value)
	{
		___meshDataEnd_22 = value;
		Il2CppCodeGenWriteBarrier(&___meshDataEnd_22, value);
	}

	inline static int32_t get_offset_of_meshDataNew_23() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___meshDataNew_23)); }
	inline MeshData_t1171048948 * get_meshDataNew_23() const { return ___meshDataNew_23; }
	inline MeshData_t1171048948 ** get_address_of_meshDataNew_23() { return &___meshDataNew_23; }
	inline void set_meshDataNew_23(MeshData_t1171048948 * value)
	{
		___meshDataNew_23 = value;
		Il2CppCodeGenWriteBarrier(&___meshDataNew_23, value);
	}

	inline static int32_t get_offset_of_bentMesh_24() { return static_cast<int32_t>(offsetof(SplineMesh_t1719246168, ___bentMesh_24)); }
	inline Mesh_t1356156583 * get_bentMesh_24() const { return ___bentMesh_24; }
	inline Mesh_t1356156583 ** get_address_of_bentMesh_24() { return &___bentMesh_24; }
	inline void set_bentMesh_24(Mesh_t1356156583 * value)
	{
		___bentMesh_24 = value;
		Il2CppCodeGenWriteBarrier(&___bentMesh_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
