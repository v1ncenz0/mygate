﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// param_confirmbox
struct  param_confirmbox_t505336669  : public MonoBehaviour_t1158329972
{
public:
	// System.String param_confirmbox::commandName
	String_t* ___commandName_2;
	// System.String param_confirmbox::objectName
	String_t* ___objectName_3;

public:
	inline static int32_t get_offset_of_commandName_2() { return static_cast<int32_t>(offsetof(param_confirmbox_t505336669, ___commandName_2)); }
	inline String_t* get_commandName_2() const { return ___commandName_2; }
	inline String_t** get_address_of_commandName_2() { return &___commandName_2; }
	inline void set_commandName_2(String_t* value)
	{
		___commandName_2 = value;
		Il2CppCodeGenWriteBarrier(&___commandName_2, value);
	}

	inline static int32_t get_offset_of_objectName_3() { return static_cast<int32_t>(offsetof(param_confirmbox_t505336669, ___objectName_3)); }
	inline String_t* get_objectName_3() const { return ___objectName_3; }
	inline String_t** get_address_of_objectName_3() { return &___objectName_3; }
	inline void set_objectName_3(String_t* value)
	{
		___objectName_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
