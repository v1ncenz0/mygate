﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Item
struct Item_t2440468191;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Item2440468191.h"

// System.Void Item::.ctor()
extern "C"  void Item__ctor_m2013999946 (Item_t2440468191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item Item::Clone(Item)
extern "C"  Item_t2440468191 * Item_Clone_m3010848088 (Il2CppObject * __this /* static, unused */, Item_t2440468191 * ___fromClone0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
