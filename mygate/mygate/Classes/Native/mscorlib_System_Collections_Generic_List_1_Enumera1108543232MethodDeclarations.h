﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m452107845(__this, ___l0, method) ((  void (*) (Enumerator_t1108543232 *, List_1_t1573813558 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2374625781(__this, method) ((  void (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1192447841(__this, method) ((  Il2CppObject * (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::Dispose()
#define Enumerator_Dispose_m902914302(__this, method) ((  void (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::VerifyState()
#define Enumerator_VerifyState_m832660615(__this, method) ((  void (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::MoveNext()
#define Enumerator_MoveNext_m2910636600(__this, method) ((  bool (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GeometryBuffer/GroupData>::get_Current()
#define Enumerator_get_Current_m1490086682(__this, method) ((  GroupData_t2204692426 * (*) (Enumerator_t1108543232 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
