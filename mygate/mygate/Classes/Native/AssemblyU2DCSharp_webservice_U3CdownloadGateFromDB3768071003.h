﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<ItemGate>
struct Action_1_t2349929170;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<downloadGateFromDB>c__Iterator7
struct  U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003  : public Il2CppObject
{
public:
	// System.Int32 webservice/<downloadGateFromDB>c__Iterator7::id
	int32_t ___id_0;
	// UnityEngine.WWW webservice/<downloadGateFromDB>c__Iterator7::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Action`1<System.String> webservice/<downloadGateFromDB>c__Iterator7::error
	Action_1_t1831019615 * ___error_2;
	// System.Action`1<ItemGate> webservice/<downloadGateFromDB>c__Iterator7::finish
	Action_1_t2349929170 * ___finish_3;
	// System.Object webservice/<downloadGateFromDB>c__Iterator7::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean webservice/<downloadGateFromDB>c__Iterator7::$disposing
	bool ___U24disposing_5;
	// System.Int32 webservice/<downloadGateFromDB>c__Iterator7::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___error_2)); }
	inline Action_1_t1831019615 * get_error_2() const { return ___error_2; }
	inline Action_1_t1831019615 ** get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(Action_1_t1831019615 * value)
	{
		___error_2 = value;
		Il2CppCodeGenWriteBarrier(&___error_2, value);
	}

	inline static int32_t get_offset_of_finish_3() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___finish_3)); }
	inline Action_1_t2349929170 * get_finish_3() const { return ___finish_3; }
	inline Action_1_t2349929170 ** get_address_of_finish_3() { return &___finish_3; }
	inline void set_finish_3(Action_1_t2349929170 * value)
	{
		___finish_3 = value;
		Il2CppCodeGenWriteBarrier(&___finish_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CdownloadGateFromDBU3Ec__Iterator7_t3768071003, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
