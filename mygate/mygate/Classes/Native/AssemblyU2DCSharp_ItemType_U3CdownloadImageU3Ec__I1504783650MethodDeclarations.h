﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemType/<downloadImage>c__Iterator0
struct U3CdownloadImageU3Ec__Iterator0_t1504783650;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemType/<downloadImage>c__Iterator0::.ctor()
extern "C"  void U3CdownloadImageU3Ec__Iterator0__ctor_m2164177861 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemType/<downloadImage>c__Iterator0::MoveNext()
extern "C"  bool U3CdownloadImageU3Ec__Iterator0_MoveNext_m2037137227 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemType/<downloadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2918923467 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemType/<downloadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3566465475 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemType/<downloadImage>c__Iterator0::Dispose()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Dispose_m647492924 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemType/<downloadImage>c__Iterator0::Reset()
extern "C"  void U3CdownloadImageU3Ec__Iterator0_Reset_m1983733910 (U3CdownloadImageU3Ec__Iterator0_t1504783650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
