﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<login>c__Iterator5
struct U3CloginU3Ec__Iterator5_t2972907461;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<login>c__Iterator5::.ctor()
extern "C"  void U3CloginU3Ec__Iterator5__ctor_m3986381414 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean webservice/<login>c__Iterator5::MoveNext()
extern "C"  bool U3CloginU3Ec__Iterator5_MoveNext_m3259074642 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<login>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CloginU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1274128158 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object webservice/<login>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CloginU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m4151048230 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<login>c__Iterator5::Dispose()
extern "C"  void U3CloginU3Ec__Iterator5_Dispose_m1441132695 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<login>c__Iterator5::Reset()
extern "C"  void U3CloginU3Ec__Iterator5_Reset_m1651606237 (U3CloginU3Ec__Iterator5_t2972907461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
