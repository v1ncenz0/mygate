﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ManageCoroutines
struct ManageCoroutines_t3089936288;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageCoroutines
struct  ManageCoroutines_t3089936288  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct ManageCoroutines_t3089936288_StaticFields
{
public:
	// System.Boolean ManageCoroutines::inProgress
	bool ___inProgress_2;
	// ManageCoroutines ManageCoroutines::instance
	ManageCoroutines_t3089936288 * ___instance_3;

public:
	inline static int32_t get_offset_of_inProgress_2() { return static_cast<int32_t>(offsetof(ManageCoroutines_t3089936288_StaticFields, ___inProgress_2)); }
	inline bool get_inProgress_2() const { return ___inProgress_2; }
	inline bool* get_address_of_inProgress_2() { return &___inProgress_2; }
	inline void set_inProgress_2(bool value)
	{
		___inProgress_2 = value;
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(ManageCoroutines_t3089936288_StaticFields, ___instance_3)); }
	inline ManageCoroutines_t3089936288 * get_instance_3() const { return ___instance_3; }
	inline ManageCoroutines_t3089936288 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(ManageCoroutines_t3089936288 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
