﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlsDesign/<addCell>c__AnonStorey5
struct U3CaddCellU3Ec__AnonStorey5_t681471284;

#include "codegen/il2cpp-codegen.h"

// System.Void ControlsDesign/<addCell>c__AnonStorey5::.ctor()
extern "C"  void U3CaddCellU3Ec__AnonStorey5__ctor_m2018315157 (U3CaddCellU3Ec__AnonStorey5_t681471284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlsDesign/<addCell>c__AnonStorey5::<>m__0(System.Boolean)
extern "C"  void U3CaddCellU3Ec__AnonStorey5_U3CU3Em__0_m2065618997 (U3CaddCellU3Ec__AnonStorey5_t681471284 * __this, bool ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
