﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PinchZoom
struct PinchZoom_t2955597597;

#include "codegen/il2cpp-codegen.h"

// System.Void PinchZoom::.ctor()
extern "C"  void PinchZoom__ctor_m2917082734 (PinchZoom_t2955597597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchZoom::Update()
extern "C"  void PinchZoom_Update_m4095497769 (PinchZoom_t2955597597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
