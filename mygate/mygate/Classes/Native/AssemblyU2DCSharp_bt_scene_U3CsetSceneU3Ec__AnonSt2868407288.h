﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// bt_scene
struct bt_scene_t1364368137;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_scene/<setScene>c__AnonStorey1
struct  U3CsetSceneU3Ec__AnonStorey1_t2868407288  : public Il2CppObject
{
public:
	// UnityEngine.GameObject bt_scene/<setScene>c__AnonStorey1::bg
	GameObject_t1756533147 * ___bg_0;
	// UnityEngine.GameObject bt_scene/<setScene>c__AnonStorey1::wall_left
	GameObject_t1756533147 * ___wall_left_1;
	// UnityEngine.GameObject bt_scene/<setScene>c__AnonStorey1::wall_right
	GameObject_t1756533147 * ___wall_right_2;
	// UnityEngine.GameObject bt_scene/<setScene>c__AnonStorey1::column_left
	GameObject_t1756533147 * ___column_left_3;
	// UnityEngine.GameObject bt_scene/<setScene>c__AnonStorey1::column_right
	GameObject_t1756533147 * ___column_right_4;
	// bt_scene bt_scene/<setScene>c__AnonStorey1::$this
	bt_scene_t1364368137 * ___U24this_5;

public:
	inline static int32_t get_offset_of_bg_0() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___bg_0)); }
	inline GameObject_t1756533147 * get_bg_0() const { return ___bg_0; }
	inline GameObject_t1756533147 ** get_address_of_bg_0() { return &___bg_0; }
	inline void set_bg_0(GameObject_t1756533147 * value)
	{
		___bg_0 = value;
		Il2CppCodeGenWriteBarrier(&___bg_0, value);
	}

	inline static int32_t get_offset_of_wall_left_1() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___wall_left_1)); }
	inline GameObject_t1756533147 * get_wall_left_1() const { return ___wall_left_1; }
	inline GameObject_t1756533147 ** get_address_of_wall_left_1() { return &___wall_left_1; }
	inline void set_wall_left_1(GameObject_t1756533147 * value)
	{
		___wall_left_1 = value;
		Il2CppCodeGenWriteBarrier(&___wall_left_1, value);
	}

	inline static int32_t get_offset_of_wall_right_2() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___wall_right_2)); }
	inline GameObject_t1756533147 * get_wall_right_2() const { return ___wall_right_2; }
	inline GameObject_t1756533147 ** get_address_of_wall_right_2() { return &___wall_right_2; }
	inline void set_wall_right_2(GameObject_t1756533147 * value)
	{
		___wall_right_2 = value;
		Il2CppCodeGenWriteBarrier(&___wall_right_2, value);
	}

	inline static int32_t get_offset_of_column_left_3() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___column_left_3)); }
	inline GameObject_t1756533147 * get_column_left_3() const { return ___column_left_3; }
	inline GameObject_t1756533147 ** get_address_of_column_left_3() { return &___column_left_3; }
	inline void set_column_left_3(GameObject_t1756533147 * value)
	{
		___column_left_3 = value;
		Il2CppCodeGenWriteBarrier(&___column_left_3, value);
	}

	inline static int32_t get_offset_of_column_right_4() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___column_right_4)); }
	inline GameObject_t1756533147 * get_column_right_4() const { return ___column_right_4; }
	inline GameObject_t1756533147 ** get_address_of_column_right_4() { return &___column_right_4; }
	inline void set_column_right_4(GameObject_t1756533147 * value)
	{
		___column_right_4 = value;
		Il2CppCodeGenWriteBarrier(&___column_right_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsetSceneU3Ec__AnonStorey1_t2868407288, ___U24this_5)); }
	inline bt_scene_t1364368137 * get_U24this_5() const { return ___U24this_5; }
	inline bt_scene_t1364368137 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(bt_scene_t1364368137 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
