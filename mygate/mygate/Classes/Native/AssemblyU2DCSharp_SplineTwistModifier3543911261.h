﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SplineMeshModifier3237667319.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineTwistModifier
struct  SplineTwistModifier_t3543911261  : public SplineMeshModifier_t3237667319
{
public:
	// System.Single SplineTwistModifier::twistCount
	float ___twistCount_2;
	// System.Single SplineTwistModifier::twistOffset
	float ___twistOffset_3;
	// UnityEngine.Quaternion SplineTwistModifier::rotationQuaternion
	Quaternion_t4030073918  ___rotationQuaternion_4;

public:
	inline static int32_t get_offset_of_twistCount_2() { return static_cast<int32_t>(offsetof(SplineTwistModifier_t3543911261, ___twistCount_2)); }
	inline float get_twistCount_2() const { return ___twistCount_2; }
	inline float* get_address_of_twistCount_2() { return &___twistCount_2; }
	inline void set_twistCount_2(float value)
	{
		___twistCount_2 = value;
	}

	inline static int32_t get_offset_of_twistOffset_3() { return static_cast<int32_t>(offsetof(SplineTwistModifier_t3543911261, ___twistOffset_3)); }
	inline float get_twistOffset_3() const { return ___twistOffset_3; }
	inline float* get_address_of_twistOffset_3() { return &___twistOffset_3; }
	inline void set_twistOffset_3(float value)
	{
		___twistOffset_3 = value;
	}

	inline static int32_t get_offset_of_rotationQuaternion_4() { return static_cast<int32_t>(offsetof(SplineTwistModifier_t3543911261, ___rotationQuaternion_4)); }
	inline Quaternion_t4030073918  get_rotationQuaternion_4() const { return ___rotationQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_rotationQuaternion_4() { return &___rotationQuaternion_4; }
	inline void set_rotationQuaternion_4(Quaternion_t4030073918  value)
	{
		___rotationQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
