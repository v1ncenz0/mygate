﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Spline
struct Spline_t1260612603;
// SplineNode
struct SplineNode_t3003005095;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineSegment
struct  SplineSegment_t1083783318  : public Il2CppObject
{
public:
	// Spline SplineSegment::parentSpline
	Spline_t1260612603 * ___parentSpline_0;
	// SplineNode SplineSegment::startNode
	SplineNode_t3003005095 * ___startNode_1;
	// SplineNode SplineSegment::endNode
	SplineNode_t3003005095 * ___endNode_2;

public:
	inline static int32_t get_offset_of_parentSpline_0() { return static_cast<int32_t>(offsetof(SplineSegment_t1083783318, ___parentSpline_0)); }
	inline Spline_t1260612603 * get_parentSpline_0() const { return ___parentSpline_0; }
	inline Spline_t1260612603 ** get_address_of_parentSpline_0() { return &___parentSpline_0; }
	inline void set_parentSpline_0(Spline_t1260612603 * value)
	{
		___parentSpline_0 = value;
		Il2CppCodeGenWriteBarrier(&___parentSpline_0, value);
	}

	inline static int32_t get_offset_of_startNode_1() { return static_cast<int32_t>(offsetof(SplineSegment_t1083783318, ___startNode_1)); }
	inline SplineNode_t3003005095 * get_startNode_1() const { return ___startNode_1; }
	inline SplineNode_t3003005095 ** get_address_of_startNode_1() { return &___startNode_1; }
	inline void set_startNode_1(SplineNode_t3003005095 * value)
	{
		___startNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___startNode_1, value);
	}

	inline static int32_t get_offset_of_endNode_2() { return static_cast<int32_t>(offsetof(SplineSegment_t1083783318, ___endNode_2)); }
	inline SplineNode_t3003005095 * get_endNode_2() const { return ___endNode_2; }
	inline SplineNode_t3003005095 ** get_address_of_endNode_2() { return &___endNode_2; }
	inline void set_endNode_2(SplineNode_t3003005095 * value)
	{
		___endNode_2 = value;
		Il2CppCodeGenWriteBarrier(&___endNode_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
