﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// bt_scene
struct bt_scene_t1364368137;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_scene/<downloadImage>c__Iterator0
struct  U3CdownloadImageU3Ec__Iterator0_t3077030272  : public Il2CppObject
{
public:
	// System.String bt_scene/<downloadImage>c__Iterator0::url_image
	String_t* ___url_image_0;
	// UnityEngine.WWW bt_scene/<downloadImage>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// UnityEngine.Texture2D bt_scene/<downloadImage>c__Iterator0::<img>__1
	Texture2D_t3542995729 * ___U3CimgU3E__1_2;
	// System.Action`1<UnityEngine.Texture2D> bt_scene/<downloadImage>c__Iterator0::complete
	Action_1_t3344795111 * ___complete_3;
	// bt_scene bt_scene/<downloadImage>c__Iterator0::$this
	bt_scene_t1364368137 * ___U24this_4;
	// System.Object bt_scene/<downloadImage>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean bt_scene/<downloadImage>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 bt_scene/<downloadImage>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_url_image_0() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___url_image_0)); }
	inline String_t* get_url_image_0() const { return ___url_image_0; }
	inline String_t** get_address_of_url_image_0() { return &___url_image_0; }
	inline void set_url_image_0(String_t* value)
	{
		___url_image_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_image_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CimgU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U3CimgU3E__1_2)); }
	inline Texture2D_t3542995729 * get_U3CimgU3E__1_2() const { return ___U3CimgU3E__1_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimgU3E__1_2() { return &___U3CimgU3E__1_2; }
	inline void set_U3CimgU3E__1_2(Texture2D_t3542995729 * value)
	{
		___U3CimgU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimgU3E__1_2, value);
	}

	inline static int32_t get_offset_of_complete_3() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___complete_3)); }
	inline Action_1_t3344795111 * get_complete_3() const { return ___complete_3; }
	inline Action_1_t3344795111 ** get_address_of_complete_3() { return &___complete_3; }
	inline void set_complete_3(Action_1_t3344795111 * value)
	{
		___complete_3 = value;
		Il2CppCodeGenWriteBarrier(&___complete_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U24this_4)); }
	inline bt_scene_t1364368137 * get_U24this_4() const { return ___U24this_4; }
	inline bt_scene_t1364368137 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(bt_scene_t1364368137 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CdownloadImageU3Ec__Iterator0_t3077030272, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
