﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUIManager
struct GUIManager_t2551693622;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// ItemColor
struct ItemColor_t1117508970;
// ItemGate
struct ItemGate_t2548129788;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ItemColor1117508970.h"
#include "AssemblyU2DCSharp_ItemGate2548129788.h"

// System.Void GUIManager::.ctor()
extern "C"  void GUIManager__ctor_m3507475335 (GUIManager_t2551693622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::InitDialogs()
extern "C"  void GUIManager_InitDialogs_m2060842450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::LoadGUIWindow(UnityEngine.GameObject&,System.String)
extern "C"  void GUIManager_LoadGUIWindow_m3972814928 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 ** ___obj0, String_t* ___file1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::createGUI()
extern "C"  void GUIManager_createGUI_m2431563642 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showColorLibrary(System.Boolean)
extern "C"  void GUIManager_showColorLibrary_m1055191893 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideColorLibrary()
extern "C"  void GUIManager_hideColorLibrary_m238762221 (GUIManager_t2551693622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::changeColorLayer(System.Int32,ItemColor)
extern "C"  void GUIManager_changeColorLayer_m2370090846 (Il2CppObject * __this /* static, unused */, int32_t ___id_layer0, ItemColor_t1117508970 * ___colorItem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::changeColorFrame(ItemColor)
extern "C"  void GUIManager_changeColorFrame_m2319159457 (Il2CppObject * __this /* static, unused */, ItemColor_t1117508970 * ___colorItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showConfirmBox(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GUIManager_showConfirmBox_m2848504621 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___objectname2, String_t* ___componentname3, String_t* ___commandname4, String_t* ___lbl_ok_button5, String_t* ___lbl_cancel_button6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideConfirmBox()
extern "C"  void GUIManager_hideConfirmBox_m4283432104 (GUIManager_t2551693622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::okConfirmBox()
extern "C"  void GUIManager_okConfirmBox_m3134001614 (GUIManager_t2551693622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showDialogBox(System.String,System.String)
extern "C"  void GUIManager_showDialogBox_m3835081365 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::okDialogBox()
extern "C"  void GUIManager_okDialogBox_m1534817770 (GUIManager_t2551693622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showWaitingBox(System.String,System.String,System.Boolean)
extern "C"  void GUIManager_showWaitingBox_m1032830737 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, bool ___hideCursor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::closeWaitingBox()
extern "C"  void GUIManager_closeWaitingBox_m3985774795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showLibraryScenes()
extern "C"  void GUIManager_showLibraryScenes_m2111511712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideLibraryScenes()
extern "C"  void GUIManager_hideLibraryScenes_m697014289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::disableCursorElement()
extern "C"  void GUIManager_disableCursorElement_m1572332619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::activeCursorElement()
extern "C"  void GUIManager_activeCursorElement_m524446163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hidePalettes()
extern "C"  void GUIManager_hidePalettes_m3191162533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showPalettes()
extern "C"  void GUIManager_showPalettes_m52551892 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideCameraControl()
extern "C"  void GUIManager_hideCameraControl_m3276137669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showCameraControl()
extern "C"  void GUIManager_showCameraControl_m3953011894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::setEditMode()
extern "C"  void GUIManager_setEditMode_m3503765598 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::setDeleteMode()
extern "C"  void GUIManager_setDeleteMode_m1703316539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showGrid()
extern "C"  void GUIManager_showGrid_m3864997388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideGrid()
extern "C"  void GUIManager_hideGrid_m2851105479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showCAD()
extern "C"  void GUIManager_showCAD_m4154621086 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showRender()
extern "C"  void GUIManager_showRender_m3895258168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::disableToolbarForSceneRendering()
extern "C"  void GUIManager_disableToolbarForSceneRendering_m2965155345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::enableToolbarForSceneRendering()
extern "C"  void GUIManager_enableToolbarForSceneRendering_m603813416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showLoading()
extern "C"  void GUIManager_showLoading_m338435982 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::hideLoading()
extern "C"  void GUIManager_hideLoading_m723936221 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::setLoadingMessage(System.String,System.Boolean)
extern "C"  void GUIManager_setLoadingMessage_m1576393529 (Il2CppObject * __this /* static, unused */, String_t* ___message0, bool ___isError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::updateInvoice(System.Double)
extern "C"  void GUIManager_updateInvoice_m3225187925 (Il2CppObject * __this /* static, unused */, double ___price0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::showHelp(System.String,System.String)
extern "C"  void GUIManager_showHelp_m2709579541 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::closeHelp()
extern "C"  void GUIManager_closeHelp_m2232504146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::openDetailBox(ItemGate)
extern "C"  void GUIManager_openDetailBox_m4292466589 (Il2CppObject * __this /* static, unused */, ItemGate_t2548129788 * ___gate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIManager::closeDetailBox()
extern "C"  void GUIManager_closeDetailBox_m275793825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
