﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// params_colorlibrary
struct params_colorlibrary_t2715541335;

#include "codegen/il2cpp-codegen.h"

// System.Void params_colorlibrary::.ctor()
extern "C"  void params_colorlibrary__ctor_m222693812 (params_colorlibrary_t2715541335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
