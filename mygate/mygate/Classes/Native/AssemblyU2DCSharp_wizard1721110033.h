﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wizard
struct  wizard_t1721110033  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct wizard_t1721110033_StaticFields
{
public:
	// System.Int32 wizard::id_type
	int32_t ___id_type_2;
	// System.Int32 wizard::id_style
	int32_t ___id_style_3;
	// System.Int32 wizard::id_geometry
	int32_t ___id_geometry_4;
	// System.Int32 wizard::width_gross
	int32_t ___width_gross_5;
	// System.Int32 wizard::height_gross
	int32_t ___height_gross_6;
	// System.String wizard::opening
	String_t* ___opening_7;

public:
	inline static int32_t get_offset_of_id_type_2() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___id_type_2)); }
	inline int32_t get_id_type_2() const { return ___id_type_2; }
	inline int32_t* get_address_of_id_type_2() { return &___id_type_2; }
	inline void set_id_type_2(int32_t value)
	{
		___id_type_2 = value;
	}

	inline static int32_t get_offset_of_id_style_3() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___id_style_3)); }
	inline int32_t get_id_style_3() const { return ___id_style_3; }
	inline int32_t* get_address_of_id_style_3() { return &___id_style_3; }
	inline void set_id_style_3(int32_t value)
	{
		___id_style_3 = value;
	}

	inline static int32_t get_offset_of_id_geometry_4() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___id_geometry_4)); }
	inline int32_t get_id_geometry_4() const { return ___id_geometry_4; }
	inline int32_t* get_address_of_id_geometry_4() { return &___id_geometry_4; }
	inline void set_id_geometry_4(int32_t value)
	{
		___id_geometry_4 = value;
	}

	inline static int32_t get_offset_of_width_gross_5() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___width_gross_5)); }
	inline int32_t get_width_gross_5() const { return ___width_gross_5; }
	inline int32_t* get_address_of_width_gross_5() { return &___width_gross_5; }
	inline void set_width_gross_5(int32_t value)
	{
		___width_gross_5 = value;
	}

	inline static int32_t get_offset_of_height_gross_6() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___height_gross_6)); }
	inline int32_t get_height_gross_6() const { return ___height_gross_6; }
	inline int32_t* get_address_of_height_gross_6() { return &___height_gross_6; }
	inline void set_height_gross_6(int32_t value)
	{
		___height_gross_6 = value;
	}

	inline static int32_t get_offset_of_opening_7() { return static_cast<int32_t>(offsetof(wizard_t1721110033_StaticFields, ___opening_7)); }
	inline String_t* get_opening_7() const { return ___opening_7; }
	inline String_t** get_address_of_opening_7() { return &___opening_7; }
	inline void set_opening_7(String_t* value)
	{
		___opening_7 = value;
		Il2CppCodeGenWriteBarrier(&___opening_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
