﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveCamera
struct  MoveCamera_t3193441886  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MoveCamera::turnSpeed
	float ___turnSpeed_2;
	// System.Single MoveCamera::panSpeed
	float ___panSpeed_3;
	// System.Single MoveCamera::zoomSpeed
	float ___zoomSpeed_4;
	// System.Single MoveCamera::zoomSpeedManual
	float ___zoomSpeedManual_5;
	// System.Single MoveCamera::turnSpeed2
	float ___turnSpeed2_6;
	// UnityEngine.Vector3 MoveCamera::mouseOrigin
	Vector3_t2243707580  ___mouseOrigin_7;
	// System.Single MoveCamera::minFov
	float ___minFov_8;
	// System.Single MoveCamera::maxFov
	float ___maxFov_9;
	// System.Single MoveCamera::sensitivity
	float ___sensitivity_10;
	// System.Single MoveCamera::ZoomAmount
	float ___ZoomAmount_11;
	// System.Single MoveCamera::MaxToClamp
	float ___MaxToClamp_12;
	// System.Single MoveCamera::ROTSpeed
	float ___ROTSpeed_13;
	// System.Single MoveCamera::manualZoomFactor
	float ___manualZoomFactor_14;
	// UnityEngine.GameObject MoveCamera::frame
	GameObject_t1756533147 * ___frame_15;
	// UnityEngine.Transform MoveCamera::initialTransform
	Transform_t3275118058 * ___initialTransform_16;
	// System.Single MoveCamera::lastRotation
	float ___lastRotation_17;
	// UnityEngine.Vector3 MoveCamera::lastTraslation
	Vector3_t2243707580  ___lastTraslation_18;
	// UnityEngine.Vector3 MoveCamera::puntoPivot
	Vector3_t2243707580  ___puntoPivot_19;
	// UnityEngine.Vector3 MoveCamera::lastVector
	Vector3_t2243707580  ___lastVector_20;
	// System.Boolean MoveCamera::mouseLeftDown
	bool ___mouseLeftDown_21;
	// System.Boolean MoveCamera::mouseRightDown
	bool ___mouseRightDown_22;
	// System.Boolean MoveCamera::mouseMiddleDown
	bool ___mouseMiddleDown_23;
	// System.Boolean MoveCamera::isRotating
	bool ___isRotating_24;
	// UnityEngine.Vector3 MoveCamera::initialCameraPos
	Vector3_t2243707580  ___initialCameraPos_25;
	// UnityEngine.Quaternion MoveCamera::initialCameraRot
	Quaternion_t4030073918  ___initialCameraRot_26;
	// UnityEngine.Vector3 MoveCamera::lastCameraPos
	Vector3_t2243707580  ___lastCameraPos_27;
	// UnityEngine.Quaternion MoveCamera::lastCameraRot
	Quaternion_t4030073918  ___lastCameraRot_28;
	// System.Boolean MoveCamera::manualRotation
	bool ___manualRotation_29;
	// System.Boolean MoveCamera::enableKeyboardMouse
	bool ___enableKeyboardMouse_30;
	// System.String MoveCamera::lastClick
	String_t* ___lastClick_31;
	// System.Single MoveCamera::targetAngle
	float ___targetAngle_32;
	// System.Single MoveCamera::targetAngle2
	float ___targetAngle2_33;
	// System.Single MoveCamera::rDistance
	float ___rDistance_35;
	// System.Single MoveCamera::rSpeed
	float ___rSpeed_36;

public:
	inline static int32_t get_offset_of_turnSpeed_2() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___turnSpeed_2)); }
	inline float get_turnSpeed_2() const { return ___turnSpeed_2; }
	inline float* get_address_of_turnSpeed_2() { return &___turnSpeed_2; }
	inline void set_turnSpeed_2(float value)
	{
		___turnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_panSpeed_3() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___panSpeed_3)); }
	inline float get_panSpeed_3() const { return ___panSpeed_3; }
	inline float* get_address_of_panSpeed_3() { return &___panSpeed_3; }
	inline void set_panSpeed_3(float value)
	{
		___panSpeed_3 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_4() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___zoomSpeed_4)); }
	inline float get_zoomSpeed_4() const { return ___zoomSpeed_4; }
	inline float* get_address_of_zoomSpeed_4() { return &___zoomSpeed_4; }
	inline void set_zoomSpeed_4(float value)
	{
		___zoomSpeed_4 = value;
	}

	inline static int32_t get_offset_of_zoomSpeedManual_5() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___zoomSpeedManual_5)); }
	inline float get_zoomSpeedManual_5() const { return ___zoomSpeedManual_5; }
	inline float* get_address_of_zoomSpeedManual_5() { return &___zoomSpeedManual_5; }
	inline void set_zoomSpeedManual_5(float value)
	{
		___zoomSpeedManual_5 = value;
	}

	inline static int32_t get_offset_of_turnSpeed2_6() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___turnSpeed2_6)); }
	inline float get_turnSpeed2_6() const { return ___turnSpeed2_6; }
	inline float* get_address_of_turnSpeed2_6() { return &___turnSpeed2_6; }
	inline void set_turnSpeed2_6(float value)
	{
		___turnSpeed2_6 = value;
	}

	inline static int32_t get_offset_of_mouseOrigin_7() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___mouseOrigin_7)); }
	inline Vector3_t2243707580  get_mouseOrigin_7() const { return ___mouseOrigin_7; }
	inline Vector3_t2243707580 * get_address_of_mouseOrigin_7() { return &___mouseOrigin_7; }
	inline void set_mouseOrigin_7(Vector3_t2243707580  value)
	{
		___mouseOrigin_7 = value;
	}

	inline static int32_t get_offset_of_minFov_8() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___minFov_8)); }
	inline float get_minFov_8() const { return ___minFov_8; }
	inline float* get_address_of_minFov_8() { return &___minFov_8; }
	inline void set_minFov_8(float value)
	{
		___minFov_8 = value;
	}

	inline static int32_t get_offset_of_maxFov_9() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___maxFov_9)); }
	inline float get_maxFov_9() const { return ___maxFov_9; }
	inline float* get_address_of_maxFov_9() { return &___maxFov_9; }
	inline void set_maxFov_9(float value)
	{
		___maxFov_9 = value;
	}

	inline static int32_t get_offset_of_sensitivity_10() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___sensitivity_10)); }
	inline float get_sensitivity_10() const { return ___sensitivity_10; }
	inline float* get_address_of_sensitivity_10() { return &___sensitivity_10; }
	inline void set_sensitivity_10(float value)
	{
		___sensitivity_10 = value;
	}

	inline static int32_t get_offset_of_ZoomAmount_11() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___ZoomAmount_11)); }
	inline float get_ZoomAmount_11() const { return ___ZoomAmount_11; }
	inline float* get_address_of_ZoomAmount_11() { return &___ZoomAmount_11; }
	inline void set_ZoomAmount_11(float value)
	{
		___ZoomAmount_11 = value;
	}

	inline static int32_t get_offset_of_MaxToClamp_12() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___MaxToClamp_12)); }
	inline float get_MaxToClamp_12() const { return ___MaxToClamp_12; }
	inline float* get_address_of_MaxToClamp_12() { return &___MaxToClamp_12; }
	inline void set_MaxToClamp_12(float value)
	{
		___MaxToClamp_12 = value;
	}

	inline static int32_t get_offset_of_ROTSpeed_13() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___ROTSpeed_13)); }
	inline float get_ROTSpeed_13() const { return ___ROTSpeed_13; }
	inline float* get_address_of_ROTSpeed_13() { return &___ROTSpeed_13; }
	inline void set_ROTSpeed_13(float value)
	{
		___ROTSpeed_13 = value;
	}

	inline static int32_t get_offset_of_manualZoomFactor_14() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___manualZoomFactor_14)); }
	inline float get_manualZoomFactor_14() const { return ___manualZoomFactor_14; }
	inline float* get_address_of_manualZoomFactor_14() { return &___manualZoomFactor_14; }
	inline void set_manualZoomFactor_14(float value)
	{
		___manualZoomFactor_14 = value;
	}

	inline static int32_t get_offset_of_frame_15() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___frame_15)); }
	inline GameObject_t1756533147 * get_frame_15() const { return ___frame_15; }
	inline GameObject_t1756533147 ** get_address_of_frame_15() { return &___frame_15; }
	inline void set_frame_15(GameObject_t1756533147 * value)
	{
		___frame_15 = value;
		Il2CppCodeGenWriteBarrier(&___frame_15, value);
	}

	inline static int32_t get_offset_of_initialTransform_16() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___initialTransform_16)); }
	inline Transform_t3275118058 * get_initialTransform_16() const { return ___initialTransform_16; }
	inline Transform_t3275118058 ** get_address_of_initialTransform_16() { return &___initialTransform_16; }
	inline void set_initialTransform_16(Transform_t3275118058 * value)
	{
		___initialTransform_16 = value;
		Il2CppCodeGenWriteBarrier(&___initialTransform_16, value);
	}

	inline static int32_t get_offset_of_lastRotation_17() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastRotation_17)); }
	inline float get_lastRotation_17() const { return ___lastRotation_17; }
	inline float* get_address_of_lastRotation_17() { return &___lastRotation_17; }
	inline void set_lastRotation_17(float value)
	{
		___lastRotation_17 = value;
	}

	inline static int32_t get_offset_of_lastTraslation_18() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastTraslation_18)); }
	inline Vector3_t2243707580  get_lastTraslation_18() const { return ___lastTraslation_18; }
	inline Vector3_t2243707580 * get_address_of_lastTraslation_18() { return &___lastTraslation_18; }
	inline void set_lastTraslation_18(Vector3_t2243707580  value)
	{
		___lastTraslation_18 = value;
	}

	inline static int32_t get_offset_of_puntoPivot_19() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___puntoPivot_19)); }
	inline Vector3_t2243707580  get_puntoPivot_19() const { return ___puntoPivot_19; }
	inline Vector3_t2243707580 * get_address_of_puntoPivot_19() { return &___puntoPivot_19; }
	inline void set_puntoPivot_19(Vector3_t2243707580  value)
	{
		___puntoPivot_19 = value;
	}

	inline static int32_t get_offset_of_lastVector_20() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastVector_20)); }
	inline Vector3_t2243707580  get_lastVector_20() const { return ___lastVector_20; }
	inline Vector3_t2243707580 * get_address_of_lastVector_20() { return &___lastVector_20; }
	inline void set_lastVector_20(Vector3_t2243707580  value)
	{
		___lastVector_20 = value;
	}

	inline static int32_t get_offset_of_mouseLeftDown_21() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___mouseLeftDown_21)); }
	inline bool get_mouseLeftDown_21() const { return ___mouseLeftDown_21; }
	inline bool* get_address_of_mouseLeftDown_21() { return &___mouseLeftDown_21; }
	inline void set_mouseLeftDown_21(bool value)
	{
		___mouseLeftDown_21 = value;
	}

	inline static int32_t get_offset_of_mouseRightDown_22() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___mouseRightDown_22)); }
	inline bool get_mouseRightDown_22() const { return ___mouseRightDown_22; }
	inline bool* get_address_of_mouseRightDown_22() { return &___mouseRightDown_22; }
	inline void set_mouseRightDown_22(bool value)
	{
		___mouseRightDown_22 = value;
	}

	inline static int32_t get_offset_of_mouseMiddleDown_23() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___mouseMiddleDown_23)); }
	inline bool get_mouseMiddleDown_23() const { return ___mouseMiddleDown_23; }
	inline bool* get_address_of_mouseMiddleDown_23() { return &___mouseMiddleDown_23; }
	inline void set_mouseMiddleDown_23(bool value)
	{
		___mouseMiddleDown_23 = value;
	}

	inline static int32_t get_offset_of_isRotating_24() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___isRotating_24)); }
	inline bool get_isRotating_24() const { return ___isRotating_24; }
	inline bool* get_address_of_isRotating_24() { return &___isRotating_24; }
	inline void set_isRotating_24(bool value)
	{
		___isRotating_24 = value;
	}

	inline static int32_t get_offset_of_initialCameraPos_25() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___initialCameraPos_25)); }
	inline Vector3_t2243707580  get_initialCameraPos_25() const { return ___initialCameraPos_25; }
	inline Vector3_t2243707580 * get_address_of_initialCameraPos_25() { return &___initialCameraPos_25; }
	inline void set_initialCameraPos_25(Vector3_t2243707580  value)
	{
		___initialCameraPos_25 = value;
	}

	inline static int32_t get_offset_of_initialCameraRot_26() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___initialCameraRot_26)); }
	inline Quaternion_t4030073918  get_initialCameraRot_26() const { return ___initialCameraRot_26; }
	inline Quaternion_t4030073918 * get_address_of_initialCameraRot_26() { return &___initialCameraRot_26; }
	inline void set_initialCameraRot_26(Quaternion_t4030073918  value)
	{
		___initialCameraRot_26 = value;
	}

	inline static int32_t get_offset_of_lastCameraPos_27() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastCameraPos_27)); }
	inline Vector3_t2243707580  get_lastCameraPos_27() const { return ___lastCameraPos_27; }
	inline Vector3_t2243707580 * get_address_of_lastCameraPos_27() { return &___lastCameraPos_27; }
	inline void set_lastCameraPos_27(Vector3_t2243707580  value)
	{
		___lastCameraPos_27 = value;
	}

	inline static int32_t get_offset_of_lastCameraRot_28() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastCameraRot_28)); }
	inline Quaternion_t4030073918  get_lastCameraRot_28() const { return ___lastCameraRot_28; }
	inline Quaternion_t4030073918 * get_address_of_lastCameraRot_28() { return &___lastCameraRot_28; }
	inline void set_lastCameraRot_28(Quaternion_t4030073918  value)
	{
		___lastCameraRot_28 = value;
	}

	inline static int32_t get_offset_of_manualRotation_29() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___manualRotation_29)); }
	inline bool get_manualRotation_29() const { return ___manualRotation_29; }
	inline bool* get_address_of_manualRotation_29() { return &___manualRotation_29; }
	inline void set_manualRotation_29(bool value)
	{
		___manualRotation_29 = value;
	}

	inline static int32_t get_offset_of_enableKeyboardMouse_30() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___enableKeyboardMouse_30)); }
	inline bool get_enableKeyboardMouse_30() const { return ___enableKeyboardMouse_30; }
	inline bool* get_address_of_enableKeyboardMouse_30() { return &___enableKeyboardMouse_30; }
	inline void set_enableKeyboardMouse_30(bool value)
	{
		___enableKeyboardMouse_30 = value;
	}

	inline static int32_t get_offset_of_lastClick_31() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___lastClick_31)); }
	inline String_t* get_lastClick_31() const { return ___lastClick_31; }
	inline String_t** get_address_of_lastClick_31() { return &___lastClick_31; }
	inline void set_lastClick_31(String_t* value)
	{
		___lastClick_31 = value;
		Il2CppCodeGenWriteBarrier(&___lastClick_31, value);
	}

	inline static int32_t get_offset_of_targetAngle_32() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___targetAngle_32)); }
	inline float get_targetAngle_32() const { return ___targetAngle_32; }
	inline float* get_address_of_targetAngle_32() { return &___targetAngle_32; }
	inline void set_targetAngle_32(float value)
	{
		___targetAngle_32 = value;
	}

	inline static int32_t get_offset_of_targetAngle2_33() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___targetAngle2_33)); }
	inline float get_targetAngle2_33() const { return ___targetAngle2_33; }
	inline float* get_address_of_targetAngle2_33() { return &___targetAngle2_33; }
	inline void set_targetAngle2_33(float value)
	{
		___targetAngle2_33 = value;
	}

	inline static int32_t get_offset_of_rDistance_35() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___rDistance_35)); }
	inline float get_rDistance_35() const { return ___rDistance_35; }
	inline float* get_address_of_rDistance_35() { return &___rDistance_35; }
	inline void set_rDistance_35(float value)
	{
		___rDistance_35 = value;
	}

	inline static int32_t get_offset_of_rSpeed_36() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___rSpeed_36)); }
	inline float get_rSpeed_36() const { return ___rSpeed_36; }
	inline float* get_address_of_rSpeed_36() { return &___rSpeed_36; }
	inline void set_rSpeed_36(float value)
	{
		___rSpeed_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
