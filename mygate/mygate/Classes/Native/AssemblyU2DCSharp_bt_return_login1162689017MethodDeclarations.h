﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_return_login
struct bt_return_login_t1162689017;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_return_login::.ctor()
extern "C"  void bt_return_login__ctor_m2959495454 (bt_return_login_t1162689017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_return_login::goToLogin()
extern "C"  void bt_return_login_goToLogin_m3148645902 (bt_return_login_t1162689017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
