﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<GeometryBuffer/GroupData>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2880912879(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t647662541 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<GeometryBuffer/GroupData>::Invoke(T)
#define Predicate_1_Invoke_m3626749967(__this, ___obj0, method) ((  bool (*) (Predicate_1_t647662541 *, GroupData_t2204692426 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<GeometryBuffer/GroupData>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2084075470(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t647662541 *, GroupData_t2204692426 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<GeometryBuffer/GroupData>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m708252773(__this, ___result0, method) ((  bool (*) (Predicate_1_t647662541 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
