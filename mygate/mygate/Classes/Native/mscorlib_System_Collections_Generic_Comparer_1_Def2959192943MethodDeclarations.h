﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<FaceIndices>
struct DefaultComparer_t2959192943;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<FaceIndices>::.ctor()
extern "C"  void DefaultComparer__ctor_m2837366796_gshared (DefaultComparer_t2959192943 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2837366796(__this, method) ((  void (*) (DefaultComparer_t2959192943 *, const MethodInfo*))DefaultComparer__ctor_m2837366796_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<FaceIndices>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2893933007_gshared (DefaultComparer_t2959192943 * __this, FaceIndices_t174955414  ___x0, FaceIndices_t174955414  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2893933007(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2959192943 *, FaceIndices_t174955414 , FaceIndices_t174955414 , const MethodInfo*))DefaultComparer_Compare_m2893933007_gshared)(__this, ___x0, ___y1, method)
