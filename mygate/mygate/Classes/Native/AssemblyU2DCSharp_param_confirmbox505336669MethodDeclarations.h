﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// param_confirmbox
struct param_confirmbox_t505336669;

#include "codegen/il2cpp-codegen.h"

// System.Void param_confirmbox::.ctor()
extern "C"  void param_confirmbox__ctor_m2571575458 (param_confirmbox_t505336669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
