﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// init_start
struct  init_start_t1142197987  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 init_start::countStylesImage
	int32_t ___countStylesImage_2;
	// UnityEngine.GameObject[] init_start::pnl
	GameObjectU5BU5D_t3057952154* ___pnl_3;
	// UnityEngine.GameObject init_start::opening_dropdown
	GameObject_t1756533147 * ___opening_dropdown_4;
	// System.Int32 init_start::currentIndex
	int32_t ___currentIndex_5;

public:
	inline static int32_t get_offset_of_countStylesImage_2() { return static_cast<int32_t>(offsetof(init_start_t1142197987, ___countStylesImage_2)); }
	inline int32_t get_countStylesImage_2() const { return ___countStylesImage_2; }
	inline int32_t* get_address_of_countStylesImage_2() { return &___countStylesImage_2; }
	inline void set_countStylesImage_2(int32_t value)
	{
		___countStylesImage_2 = value;
	}

	inline static int32_t get_offset_of_pnl_3() { return static_cast<int32_t>(offsetof(init_start_t1142197987, ___pnl_3)); }
	inline GameObjectU5BU5D_t3057952154* get_pnl_3() const { return ___pnl_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_pnl_3() { return &___pnl_3; }
	inline void set_pnl_3(GameObjectU5BU5D_t3057952154* value)
	{
		___pnl_3 = value;
		Il2CppCodeGenWriteBarrier(&___pnl_3, value);
	}

	inline static int32_t get_offset_of_opening_dropdown_4() { return static_cast<int32_t>(offsetof(init_start_t1142197987, ___opening_dropdown_4)); }
	inline GameObject_t1756533147 * get_opening_dropdown_4() const { return ___opening_dropdown_4; }
	inline GameObject_t1756533147 ** get_address_of_opening_dropdown_4() { return &___opening_dropdown_4; }
	inline void set_opening_dropdown_4(GameObject_t1756533147 * value)
	{
		___opening_dropdown_4 = value;
		Il2CppCodeGenWriteBarrier(&___opening_dropdown_4, value);
	}

	inline static int32_t get_offset_of_currentIndex_5() { return static_cast<int32_t>(offsetof(init_start_t1142197987, ___currentIndex_5)); }
	inline int32_t get_currentIndex_5() const { return ___currentIndex_5; }
	inline int32_t* get_address_of_currentIndex_5() { return &___currentIndex_5; }
	inline void set_currentIndex_5(int32_t value)
	{
		___currentIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
