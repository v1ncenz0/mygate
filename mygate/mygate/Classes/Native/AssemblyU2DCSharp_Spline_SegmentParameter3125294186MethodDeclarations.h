﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline_SegmentParameter3125294186.h"

// System.Void Spline/SegmentParameter::.ctor(System.Int32,System.Double)
extern "C"  void SegmentParameter__ctor_m305160768 (SegmentParameter_t3125294186 * __this, int32_t ___index0, double ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
