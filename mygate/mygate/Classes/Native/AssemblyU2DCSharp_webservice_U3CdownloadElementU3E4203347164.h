﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// webservice/<downloadElement>c__AnonStoreyB
struct U3CdownloadElementU3Ec__AnonStoreyB_t3800062637;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<downloadElement>c__AnonStoreyA
struct  U3CdownloadElementU3Ec__AnonStoreyA_t4203347164  : public Il2CppObject
{
public:
	// UnityEngine.GameObject webservice/<downloadElement>c__AnonStoreyA::g
	GameObject_t1756533147 * ___g_0;
	// webservice/<downloadElement>c__AnonStoreyB webservice/<downloadElement>c__AnonStoreyA::<>f__ref$11
	U3CdownloadElementU3Ec__AnonStoreyB_t3800062637 * ___U3CU3Ef__refU2411_1;

public:
	inline static int32_t get_offset_of_g_0() { return static_cast<int32_t>(offsetof(U3CdownloadElementU3Ec__AnonStoreyA_t4203347164, ___g_0)); }
	inline GameObject_t1756533147 * get_g_0() const { return ___g_0; }
	inline GameObject_t1756533147 ** get_address_of_g_0() { return &___g_0; }
	inline void set_g_0(GameObject_t1756533147 * value)
	{
		___g_0 = value;
		Il2CppCodeGenWriteBarrier(&___g_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2411_1() { return static_cast<int32_t>(offsetof(U3CdownloadElementU3Ec__AnonStoreyA_t4203347164, ___U3CU3Ef__refU2411_1)); }
	inline U3CdownloadElementU3Ec__AnonStoreyB_t3800062637 * get_U3CU3Ef__refU2411_1() const { return ___U3CU3Ef__refU2411_1; }
	inline U3CdownloadElementU3Ec__AnonStoreyB_t3800062637 ** get_address_of_U3CU3Ef__refU2411_1() { return &___U3CU3Ef__refU2411_1; }
	inline void set_U3CU3Ef__refU2411_1(U3CdownloadElementU3Ec__AnonStoreyB_t3800062637 * value)
	{
		___U3CU3Ef__refU2411_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2411_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
