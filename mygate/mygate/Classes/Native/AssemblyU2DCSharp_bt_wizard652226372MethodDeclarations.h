﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_wizard
struct bt_wizard_t652226372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void bt_wizard::.ctor()
extern "C"  void bt_wizard__ctor_m2646632749 (bt_wizard_t652226372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_wizard::setParameter(System.String)
extern "C"  void bt_wizard_setParameter_m1395886942 (bt_wizard_t652226372 * __this, String_t* ___param_name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
