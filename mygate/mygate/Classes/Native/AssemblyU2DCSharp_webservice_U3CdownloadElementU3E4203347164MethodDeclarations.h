﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice/<downloadElement>c__AnonStoreyA
struct U3CdownloadElementU3Ec__AnonStoreyA_t4203347164;

#include "codegen/il2cpp-codegen.h"

// System.Void webservice/<downloadElement>c__AnonStoreyA::.ctor()
extern "C"  void U3CdownloadElementU3Ec__AnonStoreyA__ctor_m2157276451 (U3CdownloadElementU3Ec__AnonStoreyA_t4203347164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice/<downloadElement>c__AnonStoreyA::<>m__0(System.Boolean)
extern "C"  void U3CdownloadElementU3Ec__AnonStoreyA_U3CU3Em__0_m3008633059 (U3CdownloadElementU3Ec__AnonStoreyA_t4203347164 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
