﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BranchingSplinePath
struct BranchingSplinePath_t1574214842;
// Spline
struct Spline_t1260612603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Spline1260612603.h"
#include "AssemblyU2DCSharp_BranchingSplinePath_Direction1768093872.h"

// System.Void BranchingSplinePath::.ctor()
extern "C"  void BranchingSplinePath__ctor_m463196791 (BranchingSplinePath_t1574214842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BranchingSplinePath::.ctor(Spline)
extern "C"  void BranchingSplinePath__ctor_m459464182 (BranchingSplinePath_t1574214842 * __this, Spline_t1260612603 * ___spline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BranchingSplinePath::.ctor(Spline,BranchingSplinePath/Direction)
extern "C"  void BranchingSplinePath__ctor_m598277986 (BranchingSplinePath_t1574214842 * __this, Spline_t1260612603 * ___spline0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
