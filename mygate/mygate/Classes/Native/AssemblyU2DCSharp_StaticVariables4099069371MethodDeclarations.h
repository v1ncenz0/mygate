﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticVariables
struct StaticVariables_t4099069371;

#include "codegen/il2cpp-codegen.h"

// System.Void StaticVariables::.ctor()
extern "C"  void StaticVariables__ctor_m1608172322 (StaticVariables_t4099069371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticVariables::.cctor()
extern "C"  void StaticVariables__cctor_m199389169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
