﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// webservice/<downloadElement>c__AnonStoreyB
struct  U3CdownloadElementU3Ec__AnonStoreyB_t3800062637  : public Il2CppObject
{
public:
	// System.Action`1<UnityEngine.GameObject> webservice/<downloadElement>c__AnonStoreyB::callback
	Action_1_t1558332529 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CdownloadElementU3Ec__AnonStoreyB_t3800062637, ___callback_0)); }
	inline Action_1_t1558332529 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1558332529 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1558332529 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
