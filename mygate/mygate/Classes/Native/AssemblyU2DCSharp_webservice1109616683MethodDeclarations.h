﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// webservice
struct webservice_t1109616683;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// SimpleJSON.JSONNode
struct JSONNode_t1250409636;
// Item
struct Item_t2440468191;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;
// System.String
struct String_t;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<ItemGate>
struct Action_1_t2349929170;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode1250409636.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "mscorlib_System_String2029220233.h"

// System.Void webservice::.ctor()
extern "C"  void webservice__ctor_m3567966664 (webservice_t1109616683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// webservice webservice::InitWebService()
extern "C"  webservice_t1109616683 * webservice_InitWebService_m581631819 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice::InitInterface(System.Action`1<System.Boolean>)
extern "C"  void webservice_InitInterface_m3400633264 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadStyleList(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_DownloadStyleList_m4092861544 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToStyles(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToStyles_m2449138203 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadTypeList(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_DownloadTypeList_m3392060655 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToTypes(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToTypes_m4089217878 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadGeometriesList(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_DownloadGeometriesList_m3236254251 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToGeometries(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToGeometries_m4182829183 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice::PoolDownloadColor(System.Action`1<System.Boolean>)
extern "C"  void webservice_PoolDownloadColor_m1088684616 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice::verifyDownloadTexture(System.Action`1<System.Boolean>)
extern "C"  void webservice_verifyDownloadTexture_m1008598131 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadColorList(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_DownloadColorList_m2604749094 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToColors(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToColors_m1021896793 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice::downloadElement(Item,System.Action`1<UnityEngine.GameObject>,System.Boolean)
extern "C"  void webservice_downloadElement_m2073977016 (Il2CppObject * __this /* static, unused */, Item_t2440468191 * ___item0, Action_1_t1558332529 * ___callback1, bool ___waitingform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadScenes(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_DownloadScenes_m3488684118 (webservice_t1109616683 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToScenes(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToScenes_m375949722 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::login(System.String,System.String,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * webservice_login_m1166358764 (webservice_t1109616683 * __this, String_t* ___username0, String_t* ___password1, Action_1_t3627374100 * ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::DownloadGates(System.Int32,System.Int32,System.Action`1<System.Int32>)
extern "C"  Il2CppObject * webservice_DownloadGates_m3119855555 (webservice_t1109616683 * __this, int32_t ___start0, int32_t ___limit1, Action_1_t1873676830 * ___finish2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList webservice::fromJsonToGates(SimpleJSON.JSONNode)
extern "C"  ArrayList_t4252133567 * webservice_fromJsonToGates_m280045595 (webservice_t1109616683 * __this, JSONNode_t1250409636 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator webservice::downloadGateFromDB(System.Int32,System.Action`1<ItemGate>,System.Action`1<System.String>)
extern "C"  Il2CppObject * webservice_downloadGateFromDB_m3003386792 (webservice_t1109616683 * __this, int32_t ___id0, Action_1_t2349929170 * ___finish1, Action_1_t1831019615 * ___error2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void webservice::.cctor()
extern "C"  void webservice__cctor_m2882966981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
