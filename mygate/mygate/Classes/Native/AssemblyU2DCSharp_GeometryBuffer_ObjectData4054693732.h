﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<GeometryBuffer/GroupData>
struct List_1_t1573813558;
// System.Collections.Generic.List`1<GeometryBuffer/FaceIndicesRow>
struct List_1_t2469041691;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeometryBuffer/ObjectData
struct  ObjectData_t4054693732  : public Il2CppObject
{
public:
	// System.String GeometryBuffer/ObjectData::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<GeometryBuffer/GroupData> GeometryBuffer/ObjectData::groups
	List_1_t1573813558 * ___groups_1;
	// System.Collections.Generic.List`1<GeometryBuffer/FaceIndicesRow> GeometryBuffer/ObjectData::allFacesRow
	List_1_t2469041691 * ___allFacesRow_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GeometryBuffer/ObjectData::vertices
	List_1_t1612828712 * ___vertices_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> GeometryBuffer/ObjectData::uvs
	List_1_t1612828711 * ___uvs_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GeometryBuffer/ObjectData::normals
	List_1_t1612828712 * ___normals_5;
	// System.Collections.Generic.List`1<System.Int32> GeometryBuffer/ObjectData::triangles
	List_1_t1440998580 * ___triangles_6;
	// System.Int32 GeometryBuffer/ObjectData::normalCount
	int32_t ___normalCount_7;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___groups_1)); }
	inline List_1_t1573813558 * get_groups_1() const { return ___groups_1; }
	inline List_1_t1573813558 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(List_1_t1573813558 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier(&___groups_1, value);
	}

	inline static int32_t get_offset_of_allFacesRow_2() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___allFacesRow_2)); }
	inline List_1_t2469041691 * get_allFacesRow_2() const { return ___allFacesRow_2; }
	inline List_1_t2469041691 ** get_address_of_allFacesRow_2() { return &___allFacesRow_2; }
	inline void set_allFacesRow_2(List_1_t2469041691 * value)
	{
		___allFacesRow_2 = value;
		Il2CppCodeGenWriteBarrier(&___allFacesRow_2, value);
	}

	inline static int32_t get_offset_of_vertices_3() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___vertices_3)); }
	inline List_1_t1612828712 * get_vertices_3() const { return ___vertices_3; }
	inline List_1_t1612828712 ** get_address_of_vertices_3() { return &___vertices_3; }
	inline void set_vertices_3(List_1_t1612828712 * value)
	{
		___vertices_3 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_3, value);
	}

	inline static int32_t get_offset_of_uvs_4() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___uvs_4)); }
	inline List_1_t1612828711 * get_uvs_4() const { return ___uvs_4; }
	inline List_1_t1612828711 ** get_address_of_uvs_4() { return &___uvs_4; }
	inline void set_uvs_4(List_1_t1612828711 * value)
	{
		___uvs_4 = value;
		Il2CppCodeGenWriteBarrier(&___uvs_4, value);
	}

	inline static int32_t get_offset_of_normals_5() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___normals_5)); }
	inline List_1_t1612828712 * get_normals_5() const { return ___normals_5; }
	inline List_1_t1612828712 ** get_address_of_normals_5() { return &___normals_5; }
	inline void set_normals_5(List_1_t1612828712 * value)
	{
		___normals_5 = value;
		Il2CppCodeGenWriteBarrier(&___normals_5, value);
	}

	inline static int32_t get_offset_of_triangles_6() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___triangles_6)); }
	inline List_1_t1440998580 * get_triangles_6() const { return ___triangles_6; }
	inline List_1_t1440998580 ** get_address_of_triangles_6() { return &___triangles_6; }
	inline void set_triangles_6(List_1_t1440998580 * value)
	{
		___triangles_6 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_6, value);
	}

	inline static int32_t get_offset_of_normalCount_7() { return static_cast<int32_t>(offsetof(ObjectData_t4054693732, ___normalCount_7)); }
	inline int32_t get_normalCount_7() const { return ___normalCount_7; }
	inline int32_t* get_address_of_normalCount_7() { return &___normalCount_7; }
	inline void set_normalCount_7(int32_t value)
	{
		___normalCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
