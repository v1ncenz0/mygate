﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineAnimatorClosestPoint
struct SplineAnimatorClosestPoint_t4003263689;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"

// System.Void SplineAnimatorClosestPoint::.ctor()
extern "C"  void SplineAnimatorClosestPoint__ctor_m26407690 (SplineAnimatorClosestPoint_t4003263689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineAnimatorClosestPoint::Update()
extern "C"  void SplineAnimatorClosestPoint_Update_m675379721 (SplineAnimatorClosestPoint_t4003263689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SplineAnimatorClosestPoint::WrapValue(System.Single,System.Single,System.Single,UnityEngine.WrapMode)
extern "C"  float SplineAnimatorClosestPoint_WrapValue_m373463284 (SplineAnimatorClosestPoint_t4003263689 * __this, float ___v0, float ___start1, float ___end2, int32_t ___wMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
