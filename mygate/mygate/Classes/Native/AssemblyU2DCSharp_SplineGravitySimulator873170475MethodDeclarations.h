﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineGravitySimulator
struct SplineGravitySimulator_t873170475;

#include "codegen/il2cpp-codegen.h"

// System.Void SplineGravitySimulator::.ctor()
extern "C"  void SplineGravitySimulator__ctor_m332380616 (SplineGravitySimulator_t873170475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineGravitySimulator::Start()
extern "C"  void SplineGravitySimulator_Start_m2067230476 (SplineGravitySimulator_t873170475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineGravitySimulator::FixedUpdate()
extern "C"  void SplineGravitySimulator_FixedUpdate_m924086185 (SplineGravitySimulator_t873170475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
