﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemGeometry
struct ItemGeometry_t2838964733;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemGeometry::.ctor()
extern "C"  void ItemGeometry__ctor_m866473166 (ItemGeometry_t2838964733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemGeometry::downloadImage(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemGeometry_downloadImage_m2377932734 (ItemGeometry_t2838964733 * __this, Action_1_t3627374100 * ___finish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemGeometry::isPresent()
extern "C"  bool ItemGeometry_isPresent_m884301933 (ItemGeometry_t2838964733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
