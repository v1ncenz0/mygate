﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// startWebGL
struct startWebGL_t4008935213;

#include "codegen/il2cpp-codegen.h"

// System.Void startWebGL::.ctor()
extern "C"  void startWebGL__ctor_m1883196712 (startWebGL_t4008935213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void startWebGL::Start()
extern "C"  void startWebGL_Start_m4016994968 (startWebGL_t4008935213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
