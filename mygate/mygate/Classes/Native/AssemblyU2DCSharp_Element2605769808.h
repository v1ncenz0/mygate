﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Item
struct Item_t2440468191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Element
struct  Element_t2605769808  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Element::drawCoordinate
	Vector3_t2243707580  ___drawCoordinate_2;
	// Item Element::item
	Item_t2440468191 * ___item_3;
	// System.Boolean Element::is_specular
	bool ___is_specular_4;

public:
	inline static int32_t get_offset_of_drawCoordinate_2() { return static_cast<int32_t>(offsetof(Element_t2605769808, ___drawCoordinate_2)); }
	inline Vector3_t2243707580  get_drawCoordinate_2() const { return ___drawCoordinate_2; }
	inline Vector3_t2243707580 * get_address_of_drawCoordinate_2() { return &___drawCoordinate_2; }
	inline void set_drawCoordinate_2(Vector3_t2243707580  value)
	{
		___drawCoordinate_2 = value;
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(Element_t2605769808, ___item_3)); }
	inline Item_t2440468191 * get_item_3() const { return ___item_3; }
	inline Item_t2440468191 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(Item_t2440468191 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_3, value);
	}

	inline static int32_t get_offset_of_is_specular_4() { return static_cast<int32_t>(offsetof(Element_t2605769808, ___is_specular_4)); }
	inline bool get_is_specular_4() const { return ___is_specular_4; }
	inline bool* get_address_of_is_specular_4() { return &___is_specular_4; }
	inline void set_is_specular_4(bool value)
	{
		___is_specular_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
