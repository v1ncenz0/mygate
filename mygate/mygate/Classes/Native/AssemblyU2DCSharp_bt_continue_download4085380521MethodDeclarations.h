﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_continue_download
struct bt_continue_download_t4085380521;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_continue_download::.ctor()
extern "C"  void bt_continue_download__ctor_m1074748298 (bt_continue_download_t4085380521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_continue_download::continueDownload()
extern "C"  void bt_continue_download_continueDownload_m4162361521 (bt_continue_download_t4085380521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
