﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FaceIndices>
struct List_1_t3839043842;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3373773516.h"
#include "AssemblyU2DCSharp_FaceIndices174955414.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2553205409_gshared (Enumerator_t3373773516 * __this, List_1_t3839043842 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2553205409(__this, ___l0, method) ((  void (*) (Enumerator_t3373773516 *, List_1_t3839043842 *, const MethodInfo*))Enumerator__ctor_m2553205409_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1577766193_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1577766193(__this, method) ((  void (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1577766193_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FaceIndices>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3387245145(__this, method) ((  Il2CppObject * (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3387245145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::Dispose()
extern "C"  void Enumerator_Dispose_m632182510_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m632182510(__this, method) ((  void (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_Dispose_m632182510_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FaceIndices>::VerifyState()
extern "C"  void Enumerator_VerifyState_m767115607_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m767115607(__this, method) ((  void (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_VerifyState_m767115607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FaceIndices>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3916965222_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3916965222(__this, method) ((  bool (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_MoveNext_m3916965222_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FaceIndices>::get_Current()
extern "C"  FaceIndices_t174955414  Enumerator_get_Current_m892207596_gshared (Enumerator_t3373773516 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m892207596(__this, method) ((  FaceIndices_t174955414  (*) (Enumerator_t3373773516 *, const MethodInfo*))Enumerator_get_Current_m892207596_gshared)(__this, method)
