﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bt_enable_layer
struct bt_enable_layer_t839870442;

#include "codegen/il2cpp-codegen.h"

// System.Void bt_enable_layer::.ctor()
extern "C"  void bt_enable_layer__ctor_m2784947399 (bt_enable_layer_t839870442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bt_enable_layer::setEnableLayer(System.Int32)
extern "C"  void bt_enable_layer_setEnableLayer_m644705884 (bt_enable_layer_t839870442 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
