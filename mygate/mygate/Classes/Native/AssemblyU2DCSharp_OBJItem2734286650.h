﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJItem
struct  OBJItem_t2734286650  : public Il2CppObject
{
public:
	// System.String OBJItem::obj
	String_t* ___obj_0;
	// System.Collections.ArrayList OBJItem::mtl
	ArrayList_t4252133567 * ___mtl_1;
	// System.Collections.ArrayList OBJItem::mtl_name
	ArrayList_t4252133567 * ___mtl_name_2;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(OBJItem_t2734286650, ___obj_0)); }
	inline String_t* get_obj_0() const { return ___obj_0; }
	inline String_t** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(String_t* value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_mtl_1() { return static_cast<int32_t>(offsetof(OBJItem_t2734286650, ___mtl_1)); }
	inline ArrayList_t4252133567 * get_mtl_1() const { return ___mtl_1; }
	inline ArrayList_t4252133567 ** get_address_of_mtl_1() { return &___mtl_1; }
	inline void set_mtl_1(ArrayList_t4252133567 * value)
	{
		___mtl_1 = value;
		Il2CppCodeGenWriteBarrier(&___mtl_1, value);
	}

	inline static int32_t get_offset_of_mtl_name_2() { return static_cast<int32_t>(offsetof(OBJItem_t2734286650, ___mtl_name_2)); }
	inline ArrayList_t4252133567 * get_mtl_name_2() const { return ___mtl_name_2; }
	inline ArrayList_t4252133567 ** get_address_of_mtl_name_2() { return &___mtl_name_2; }
	inline void set_mtl_name_2(ArrayList_t4252133567 * value)
	{
		___mtl_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___mtl_name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
