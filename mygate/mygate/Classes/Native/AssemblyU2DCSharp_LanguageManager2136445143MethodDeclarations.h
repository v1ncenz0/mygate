﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageManager
struct LanguageManager_t2136445143;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LanguageManager::.ctor()
extern "C"  void LanguageManager__ctor_m145132788 (LanguageManager_t2136445143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageManager::text(System.String,System.String)
extern "C"  String_t* LanguageManager_text_m3138572552 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageManager::.cctor()
extern "C"  void LanguageManager__cctor_m1495645749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
