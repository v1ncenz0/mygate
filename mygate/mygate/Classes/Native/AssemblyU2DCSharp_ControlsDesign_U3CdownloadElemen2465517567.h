﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Item
struct Item_t2440468191;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ControlsDesign
struct ControlsDesign_t1600104368;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlsDesign/<downloadElements>c__AnonStorey8
struct  U3CdownloadElementsU3Ec__AnonStorey8_t2465517567  : public Il2CppObject
{
public:
	// Item ControlsDesign/<downloadElements>c__AnonStorey8::item
	Item_t2440468191 * ___item_0;
	// System.Collections.ArrayList ControlsDesign/<downloadElements>c__AnonStorey8::items
	ArrayList_t4252133567 * ___items_1;
	// System.Int32 ControlsDesign/<downloadElements>c__AnonStorey8::index
	int32_t ___index_2;
	// System.Action`1<System.Boolean> ControlsDesign/<downloadElements>c__AnonStorey8::finish
	Action_1_t3627374100 * ___finish_3;
	// ControlsDesign ControlsDesign/<downloadElements>c__AnonStorey8::$this
	ControlsDesign_t1600104368 * ___U24this_4;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CdownloadElementsU3Ec__AnonStorey8_t2465517567, ___item_0)); }
	inline Item_t2440468191 * get_item_0() const { return ___item_0; }
	inline Item_t2440468191 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_t2440468191 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(U3CdownloadElementsU3Ec__AnonStorey8_t2465517567, ___items_1)); }
	inline ArrayList_t4252133567 * get_items_1() const { return ___items_1; }
	inline ArrayList_t4252133567 ** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(ArrayList_t4252133567 * value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier(&___items_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CdownloadElementsU3Ec__AnonStorey8_t2465517567, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_finish_3() { return static_cast<int32_t>(offsetof(U3CdownloadElementsU3Ec__AnonStorey8_t2465517567, ___finish_3)); }
	inline Action_1_t3627374100 * get_finish_3() const { return ___finish_3; }
	inline Action_1_t3627374100 ** get_address_of_finish_3() { return &___finish_3; }
	inline void set_finish_3(Action_1_t3627374100 * value)
	{
		___finish_3 = value;
		Il2CppCodeGenWriteBarrier(&___finish_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CdownloadElementsU3Ec__AnonStorey8_t2465517567, ___U24this_4)); }
	inline ControlsDesign_t1600104368 * get_U24this_4() const { return ___U24this_4; }
	inline ControlsDesign_t1600104368 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ControlsDesign_t1600104368 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
