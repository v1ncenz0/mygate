﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemType
struct  ItemType_t3455197591  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemType::id
	int32_t ___id_2;
	// System.String ItemType::name
	String_t* ___name_3;
	// UnityEngine.Sprite ItemType::image
	Sprite_t309593783 * ___image_4;
	// System.String ItemType::image_path
	String_t* ___image_path_5;
	// System.String ItemType::opening
	String_t* ___opening_6;
	// System.String ItemType::description
	String_t* ___description_7;
	// System.Collections.ArrayList ItemType::misure
	ArrayList_t4252133567 * ___misure_8;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___image_4)); }
	inline Sprite_t309593783 * get_image_4() const { return ___image_4; }
	inline Sprite_t309593783 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Sprite_t309593783 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_image_path_5() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___image_path_5)); }
	inline String_t* get_image_path_5() const { return ___image_path_5; }
	inline String_t** get_address_of_image_path_5() { return &___image_path_5; }
	inline void set_image_path_5(String_t* value)
	{
		___image_path_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_path_5, value);
	}

	inline static int32_t get_offset_of_opening_6() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___opening_6)); }
	inline String_t* get_opening_6() const { return ___opening_6; }
	inline String_t** get_address_of_opening_6() { return &___opening_6; }
	inline void set_opening_6(String_t* value)
	{
		___opening_6 = value;
		Il2CppCodeGenWriteBarrier(&___opening_6, value);
	}

	inline static int32_t get_offset_of_description_7() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___description_7)); }
	inline String_t* get_description_7() const { return ___description_7; }
	inline String_t** get_address_of_description_7() { return &___description_7; }
	inline void set_description_7(String_t* value)
	{
		___description_7 = value;
		Il2CppCodeGenWriteBarrier(&___description_7, value);
	}

	inline static int32_t get_offset_of_misure_8() { return static_cast<int32_t>(offsetof(ItemType_t3455197591, ___misure_8)); }
	inline ArrayList_t4252133567 * get_misure_8() const { return ___misure_8; }
	inline ArrayList_t4252133567 ** get_address_of_misure_8() { return &___misure_8; }
	inline void set_misure_8(ArrayList_t4252133567 * value)
	{
		___misure_8 = value;
		Il2CppCodeGenWriteBarrier(&___misure_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
