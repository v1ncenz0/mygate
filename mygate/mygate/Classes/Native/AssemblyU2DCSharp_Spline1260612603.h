﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<SplineNode>
struct List_1_t2372126227;
// Spline/LengthData
struct LengthData_t3176416758;
// SplineInterpolator
struct SplineInterpolator_t4279526764;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Spline_InterpolationMode2569420497.h"
#include "AssemblyU2DCSharp_Spline_RotationMode3093810083.h"
#include "AssemblyU2DCSharp_Spline_TangentMode4224395836.h"
#include "AssemblyU2DCSharp_Spline_NormalMode4231820426.h"
#include "AssemblyU2DCSharp_Spline_UpdateMode1661020696.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spline
struct  Spline_t1260612603  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<SplineNode> Spline::splineNodesArray
	List_1_t2372126227 * ___splineNodesArray_2;
	// System.Collections.Generic.List`1<SplineNode> Spline::splineNodesInternal
	List_1_t2372126227 * ___splineNodesInternal_3;
	// Spline/InterpolationMode Spline::interpolationMode
	int32_t ___interpolationMode_4;
	// Spline/RotationMode Spline::rotationMode
	int32_t ___rotationMode_5;
	// Spline/TangentMode Spline::tangentMode
	int32_t ___tangentMode_6;
	// Spline/NormalMode Spline::normalMode
	int32_t ___normalMode_7;
	// Spline/UpdateMode Spline::updateMode
	int32_t ___updateMode_8;
	// System.Int32 Spline::deltaFrames
	int32_t ___deltaFrames_9;
	// System.Single Spline::deltaTime
	float ___deltaTime_10;
	// System.Int32 Spline::updateFrame
	int32_t ___updateFrame_11;
	// System.Single Spline::updateTime
	float ___updateTime_12;
	// System.Boolean Spline::perNodeTension
	bool ___perNodeTension_13;
	// System.Single Spline::tension
	float ___tension_14;
	// UnityEngine.Vector3 Spline::normal
	Vector3_t2243707580  ___normal_15;
	// System.Boolean Spline::autoClose
	bool ___autoClose_16;
	// System.Int32 Spline::interpolationAccuracy
	int32_t ___interpolationAccuracy_17;
	// Spline/LengthData Spline::lengthData
	LengthData_t3176416758 * ___lengthData_18;
	// SplineInterpolator Spline::splineInterpolator
	SplineInterpolator_t4279526764 * ___splineInterpolator_19;

public:
	inline static int32_t get_offset_of_splineNodesArray_2() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___splineNodesArray_2)); }
	inline List_1_t2372126227 * get_splineNodesArray_2() const { return ___splineNodesArray_2; }
	inline List_1_t2372126227 ** get_address_of_splineNodesArray_2() { return &___splineNodesArray_2; }
	inline void set_splineNodesArray_2(List_1_t2372126227 * value)
	{
		___splineNodesArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___splineNodesArray_2, value);
	}

	inline static int32_t get_offset_of_splineNodesInternal_3() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___splineNodesInternal_3)); }
	inline List_1_t2372126227 * get_splineNodesInternal_3() const { return ___splineNodesInternal_3; }
	inline List_1_t2372126227 ** get_address_of_splineNodesInternal_3() { return &___splineNodesInternal_3; }
	inline void set_splineNodesInternal_3(List_1_t2372126227 * value)
	{
		___splineNodesInternal_3 = value;
		Il2CppCodeGenWriteBarrier(&___splineNodesInternal_3, value);
	}

	inline static int32_t get_offset_of_interpolationMode_4() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___interpolationMode_4)); }
	inline int32_t get_interpolationMode_4() const { return ___interpolationMode_4; }
	inline int32_t* get_address_of_interpolationMode_4() { return &___interpolationMode_4; }
	inline void set_interpolationMode_4(int32_t value)
	{
		___interpolationMode_4 = value;
	}

	inline static int32_t get_offset_of_rotationMode_5() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___rotationMode_5)); }
	inline int32_t get_rotationMode_5() const { return ___rotationMode_5; }
	inline int32_t* get_address_of_rotationMode_5() { return &___rotationMode_5; }
	inline void set_rotationMode_5(int32_t value)
	{
		___rotationMode_5 = value;
	}

	inline static int32_t get_offset_of_tangentMode_6() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___tangentMode_6)); }
	inline int32_t get_tangentMode_6() const { return ___tangentMode_6; }
	inline int32_t* get_address_of_tangentMode_6() { return &___tangentMode_6; }
	inline void set_tangentMode_6(int32_t value)
	{
		___tangentMode_6 = value;
	}

	inline static int32_t get_offset_of_normalMode_7() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___normalMode_7)); }
	inline int32_t get_normalMode_7() const { return ___normalMode_7; }
	inline int32_t* get_address_of_normalMode_7() { return &___normalMode_7; }
	inline void set_normalMode_7(int32_t value)
	{
		___normalMode_7 = value;
	}

	inline static int32_t get_offset_of_updateMode_8() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___updateMode_8)); }
	inline int32_t get_updateMode_8() const { return ___updateMode_8; }
	inline int32_t* get_address_of_updateMode_8() { return &___updateMode_8; }
	inline void set_updateMode_8(int32_t value)
	{
		___updateMode_8 = value;
	}

	inline static int32_t get_offset_of_deltaFrames_9() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___deltaFrames_9)); }
	inline int32_t get_deltaFrames_9() const { return ___deltaFrames_9; }
	inline int32_t* get_address_of_deltaFrames_9() { return &___deltaFrames_9; }
	inline void set_deltaFrames_9(int32_t value)
	{
		___deltaFrames_9 = value;
	}

	inline static int32_t get_offset_of_deltaTime_10() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___deltaTime_10)); }
	inline float get_deltaTime_10() const { return ___deltaTime_10; }
	inline float* get_address_of_deltaTime_10() { return &___deltaTime_10; }
	inline void set_deltaTime_10(float value)
	{
		___deltaTime_10 = value;
	}

	inline static int32_t get_offset_of_updateFrame_11() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___updateFrame_11)); }
	inline int32_t get_updateFrame_11() const { return ___updateFrame_11; }
	inline int32_t* get_address_of_updateFrame_11() { return &___updateFrame_11; }
	inline void set_updateFrame_11(int32_t value)
	{
		___updateFrame_11 = value;
	}

	inline static int32_t get_offset_of_updateTime_12() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___updateTime_12)); }
	inline float get_updateTime_12() const { return ___updateTime_12; }
	inline float* get_address_of_updateTime_12() { return &___updateTime_12; }
	inline void set_updateTime_12(float value)
	{
		___updateTime_12 = value;
	}

	inline static int32_t get_offset_of_perNodeTension_13() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___perNodeTension_13)); }
	inline bool get_perNodeTension_13() const { return ___perNodeTension_13; }
	inline bool* get_address_of_perNodeTension_13() { return &___perNodeTension_13; }
	inline void set_perNodeTension_13(bool value)
	{
		___perNodeTension_13 = value;
	}

	inline static int32_t get_offset_of_tension_14() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___tension_14)); }
	inline float get_tension_14() const { return ___tension_14; }
	inline float* get_address_of_tension_14() { return &___tension_14; }
	inline void set_tension_14(float value)
	{
		___tension_14 = value;
	}

	inline static int32_t get_offset_of_normal_15() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___normal_15)); }
	inline Vector3_t2243707580  get_normal_15() const { return ___normal_15; }
	inline Vector3_t2243707580 * get_address_of_normal_15() { return &___normal_15; }
	inline void set_normal_15(Vector3_t2243707580  value)
	{
		___normal_15 = value;
	}

	inline static int32_t get_offset_of_autoClose_16() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___autoClose_16)); }
	inline bool get_autoClose_16() const { return ___autoClose_16; }
	inline bool* get_address_of_autoClose_16() { return &___autoClose_16; }
	inline void set_autoClose_16(bool value)
	{
		___autoClose_16 = value;
	}

	inline static int32_t get_offset_of_interpolationAccuracy_17() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___interpolationAccuracy_17)); }
	inline int32_t get_interpolationAccuracy_17() const { return ___interpolationAccuracy_17; }
	inline int32_t* get_address_of_interpolationAccuracy_17() { return &___interpolationAccuracy_17; }
	inline void set_interpolationAccuracy_17(int32_t value)
	{
		___interpolationAccuracy_17 = value;
	}

	inline static int32_t get_offset_of_lengthData_18() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___lengthData_18)); }
	inline LengthData_t3176416758 * get_lengthData_18() const { return ___lengthData_18; }
	inline LengthData_t3176416758 ** get_address_of_lengthData_18() { return &___lengthData_18; }
	inline void set_lengthData_18(LengthData_t3176416758 * value)
	{
		___lengthData_18 = value;
		Il2CppCodeGenWriteBarrier(&___lengthData_18, value);
	}

	inline static int32_t get_offset_of_splineInterpolator_19() { return static_cast<int32_t>(offsetof(Spline_t1260612603, ___splineInterpolator_19)); }
	inline SplineInterpolator_t4279526764 * get_splineInterpolator_19() const { return ___splineInterpolator_19; }
	inline SplineInterpolator_t4279526764 ** get_address_of_splineInterpolator_19() { return &___splineInterpolator_19; }
	inline void set_splineInterpolator_19(SplineInterpolator_t4279526764 * value)
	{
		___splineInterpolator_19 = value;
		Il2CppCodeGenWriteBarrier(&___splineInterpolator_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
