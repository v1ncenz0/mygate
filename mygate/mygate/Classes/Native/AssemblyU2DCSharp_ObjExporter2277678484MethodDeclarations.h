﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjExporter
struct ObjExporter_t2277678484;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// OBJItem
struct OBJItem_t2734286650;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ObjExporter::.ctor()
extern "C"  void ObjExporter__ctor_m490673187 (ObjExporter_t2277678484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporter::DoExportWSubmeshes(System.String,UnityEngine.GameObject)
extern "C"  void ObjExporter_DoExportWSubmeshes_m847514114 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporter::DoExportWOSubmeshes(System.String,UnityEngine.GameObject)
extern "C"  void ObjExporter_DoExportWOSubmeshes_m1928501019 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OBJItem ObjExporter::getObjString(System.Boolean,UnityEngine.GameObject)
extern "C"  OBJItem_t2734286650 * ObjExporter_getObjString_m1239392197 (Il2CppObject * __this /* static, unused */, bool ___makeSubmeshes0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ObjExporter::createMTL(System.String,UnityEngine.Color)
extern "C"  String_t* ObjExporter_createMTL_m1825179259 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Color_t2020392075  ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporter::DoExport(System.Boolean,System.String,UnityEngine.GameObject)
extern "C"  void ObjExporter_DoExport_m3255956045 (Il2CppObject * __this /* static, unused */, bool ___makeSubmeshes0, String_t* ___fileName1, GameObject_t1756533147 * ___go2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ObjExporter::processTransform(UnityEngine.Transform,System.Boolean)
extern "C"  String_t* ObjExporter_processTransform_m1427425053 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, bool ___makeSubmeshes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjExporter::WriteToFile(System.String,System.String)
extern "C"  void ObjExporter_WriteToFile_m2668656575 (Il2CppObject * __this /* static, unused */, String_t* ___s0, String_t* ___filename1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
