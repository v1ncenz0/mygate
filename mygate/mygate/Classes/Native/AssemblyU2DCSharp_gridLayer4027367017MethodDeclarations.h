﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gridLayer
struct gridLayer_t4027367017;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// CellGrid
struct CellGrid_t1919063536;
// Item
struct Item_t2440468191;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ItemColor
struct ItemColor_t1117508970;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_CellGrid1919063536.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_ItemColor1117508970.h"

// System.Void gridLayer::.ctor()
extern "C"  void gridLayer__ctor_m1515556072 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::init()
extern "C"  void gridLayer_init_m3064169136 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::Activate(System.Boolean)
extern "C"  void gridLayer_Activate_m2412703510 (gridLayer_t4027367017 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::makeGrid(System.Boolean)
extern "C"  void gridLayer_makeGrid_m2887906497 (gridLayer_t4027367017 * __this, bool ___showGrid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::makeHatGrid()
extern "C"  void gridLayer_makeHatGrid_m4262691831 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::createCellGridObjs(System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void gridLayer_createCellGridObjs_m3553649917 (gridLayer_t4027367017 * __this, int32_t ___elements_Y0, int32_t ___elements_X1, Vector3_t2243707580  ___dim2, Vector3_t2243707580  ___startingPoint3, GameObject_t1756533147 * ___gridView4, GameObject_t1756533147 * ___gridViewRight5, bool ___isHat6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::isUpperHat(CellGrid,System.Boolean&)
extern "C"  bool gridLayer_isUpperHat_m2858079580 (gridLayer_t4027367017 * __this, CellGrid_t1919063536 * ___cell0, bool* ___toCut1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::regrid(System.Boolean,System.Boolean)
extern "C"  void gridLayer_regrid_m4021049439 (gridLayer_t4027367017 * __this, bool ___immediately0, bool ___showGrid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::moveGrid(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C"  void gridLayer_moveGrid_m52478942 (gridLayer_t4027367017 * __this, Vector2_t2243707579  ___offset0, Vector2_t2243707579  ___offsetRight1, bool ___redrawHat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::showGrid(System.Boolean)
extern "C"  void gridLayer_showGrid_m3417720814 (gridLayer_t4027367017 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::showCellGrid(System.Boolean)
extern "C"  void gridLayer_showCellGrid_m560944938 (gridLayer_t4027367017 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::invertGridShow()
extern "C"  void gridLayer_invertGridShow_m895915769 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::destroyGrid(System.Boolean)
extern "C"  void gridLayer_destroyGrid_m2939761329 (gridLayer_t4027367017 * __this, bool ___immediately0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::deleteElement(UnityEngine.GameObject)
extern "C"  void gridLayer_deleteElement_m3912575923 (gridLayer_t4027367017 * __this, GameObject_t1756533147 * ___elementToDestroy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::drawElement(Item,UnityEngine.Vector3,System.Boolean)
extern "C"  bool gridLayer_drawElement_m1947595217 (gridLayer_t4027367017 * __this, Item_t2440468191 * ___itemSelected0, Vector3_t2243707580  ___pos1, bool ___specular2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gridLayer::fillGrid(Item,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * gridLayer_fillGrid_m1225643833 (gridLayer_t4027367017 * __this, Item_t2440468191 * ___itemSelected0, Action_1_t3627374100 * ___success1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::setColorLayer(ItemColor)
extern "C"  void gridLayer_setColorLayer_m3765936824 (gridLayer_t4027367017 * __this, ItemColor_t1117508970 * ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemColor gridLayer::getColorLayer()
extern "C"  ItemColor_t1117508970 * gridLayer_getColorLayer_m2520011623 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gridLayer::ClearGrid()
extern "C"  void gridLayer_ClearGrid_m3299033337 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::isValidLayer()
extern "C"  bool gridLayer_isValidLayer_m2499213671 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::isNotEmptyLayer()
extern "C"  bool gridLayer_isNotEmptyLayer_m3233876781 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::isLayerFill()
extern "C"  bool gridLayer_isLayerFill_m2761215406 (gridLayer_t4027367017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CellGrid gridLayer::getCellGrid(UnityEngine.Vector3)
extern "C"  CellGrid_t1919063536 * gridLayer_getCellGrid_m583157328 (gridLayer_t4027367017 * __this, Vector3_t2243707580  ___drawCoordinates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 gridLayer::calcolateSpecularPosition(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  gridLayer_calcolateSpecularPosition_m4056421299 (gridLayer_t4027367017 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CellGrid gridLayer::findNearCell(UnityEngine.Vector3,System.Int32)
extern "C"  CellGrid_t1919063536 * gridLayer_findNearCell_m605300106 (gridLayer_t4027367017 * __this, Vector3_t2243707580  ___pos0, int32_t ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::VerifyroundPosition(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  bool gridLayer_VerifyroundPosition_m103705833 (gridLayer_t4027367017 * __this, Vector3_t2243707580  ___centralpoint0, Vector3_t2243707580  ___pos1, int32_t ___range2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::canAdd(Item,UnityEngine.Vector3)
extern "C"  bool gridLayer_canAdd_m1195641585 (gridLayer_t4027367017 * __this, Item_t2440468191 * ___itemSelected0, Vector3_t2243707580  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gridLayer::canAdd(Item,UnityEngine.Vector3,System.Boolean&)
extern "C"  bool gridLayer_canAdd_m1545538722 (gridLayer_t4027367017 * __this, Item_t2440468191 * ___itemSelected0, Vector3_t2243707580  ___pos1, bool* ___cut2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
